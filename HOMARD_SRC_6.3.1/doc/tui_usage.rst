Mode d'emploi pour TUI
======================
Le module HOMARD peut se lancer � partir d'instructions python. On trouvera ici la description de chacune des fonctions accessibles par l'utilisateur.

.. toctree::
   :maxdepth: 2

   tui_create_case
   tui_create_iteration
   tui_create_hypothese
   tui_create_zone
   tui_create_boundary
   tui_homard
