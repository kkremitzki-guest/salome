Mode d'emploi pour l'interface graphique
========================================
.. index:: single: cas
.. index:: single: it�ration

Activer le module HOMARD
""""""""""""""""""""""""
Pour utiliser le module HOMARD, deux fa�ons existent :

#. en activant l'onglet HOMARD dans la liste des modules,
#. en cliquant sur le bouton HOMARD dans la barre d'outils.

L'utilisateur a alors le choix entre cr�er une nouvelle �tude ou en ouvrir une qui a �t� pr�c�demment enregistr�e.

D�finir une adaptation
""""""""""""""""""""""

Une fois que HOMARD a �t� activ�, la permi�re action consiste � cr�er un cas. Il s'agit de s�lectionner le maillage initial de la suite d'adaptations envisag�e (voir :ref:`gui_create_case`). A partir de ce cas, on d�finira les it�rations successives (voir :ref:`gui_create_iteration`) � partir des hypoth�ses (voir :ref:`gui_create_hypothese`).

R�cup�rer le r�sultat d'une adaptation
""""""""""""""""""""""""""""""""""""""
Le r�sultat d'une adaptation s'obtient en s�lectionnant l'it�ration � calculer. On s�lectionne ensuite *Lancement* dans le menu HOMARD.

.. image:: images/lancement_1.png
   :align: center

Le fichier contenant le maillage produit est visible dans l'arbre d'�tudes.

Mode d'emploi de la saisie des donn�es
""""""""""""""""""""""""""""""""""""""

.. toctree::
   :maxdepth: 2

   gui_create_case
   gui_create_iteration
   gui_create_hypothese
   gui_create_zone

