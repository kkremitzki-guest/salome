.. HOMARD documentation master file, created by
   sphinx-quickstart on Tue Jan  5 08:51:14 2010.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. |logo| image:: images/HOMARD.png

Documentation du composant HOMARD |logo|
----------------------------------------

HOMARD est le composant qui permet l'adaptation de maillage au sein de la plateforme Salome.

*HOMARD est une marque d�pos�e d'EDF.*

Table des mati�res de ce mode d'emploi
""""""""""""""""""""""""""""""""""""""

.. toctree::
   :maxdepth: 2

   intro
   gui_usage
   tui_usage
   tutorials


Index et tables
"""""""""""""""

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
* :ref:`glossaire`
