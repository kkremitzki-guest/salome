EnSight Model Geometry File
part 1 portion
node id given
element id given
extents
 0.00000e+00 4.00000e+00
 0.00000e+00 4.00000e+00
 0.00000e+00 0.00000e+00
part
         1
bottom left
coordinates
        16
         1
         2
         3
         4
         6
         7
         8
         9
        11
        12
        13
        14
        16
        17
        18
        19
 0.00000e+00
 1.00000e+00
 2.00000e+00
 3.00000e+00
 0.00000e+00
 1.00000e+00
 2.00000e+00
 3.00000e+00
 0.00000e+00
 1.00000e+00
 2.00000e+00
 3.00000e+00
 0.00000e+00
 1.00000e+00
 2.00000e+00
 3.00000e+00
 0.00000e+00
 0.00000e+00
 0.00000e+00
 0.00000e+00
 1.00000e+00
 1.00000e+00
 1.00000e+00
 1.00000e+00
 2.00000e+00
 2.00000e+00
 2.00000e+00
 2.00000e+00
 3.00000e+00
 3.00000e+00
 3.00000e+00
 3.00000e+00
 0.00000e+00
 0.00000e+00
 0.00000e+00
 0.00000e+00
 0.00000e+00
 0.00000e+00
 0.00000e+00
 0.00000e+00
 0.00000e+00
 0.00000e+00
 0.00000e+00
 0.00000e+00
 0.00000e+00
 0.00000e+00
 0.00000e+00
 0.00000e+00
quad4
         4
         1
         2
         5
         6
         1         2         6         5
         2         3         7         6
         5         6        10         9
         6         7        11        10
g_quad4
         5
         1
         1
         1
         1
         1
         3         4         8         7
         7         8        12        11
         9        10        14        13
        10        11        15        14
        11        12        16        15
part
         2
bottom right
part
         3
top left
part
         4
top right
