This is the 1st description line of the EnSight6 geometry example
This is the 2nd description line of the EnSight6 geometry example
node id given
element id given
coordinates
      11
      15 4.00000e+00 0.00000e+00 0.00000e+00
      31 3.00000e+00 0.00000e+00 0.00000e+00
      20 5.00000e+00 0.00000e+00 0.00000e+00
      40 6.00000e+00 0.00000e+00 0.00000e+00
      22 5.00000e+00 1.00000e+00 0.00000e+00
      44 6.00000e+00 1.00000e+00 0.00000e+00
      55 6.00000e+00 3.00000e+00 0.00000e+00
      60 5.00000e+00 0.00000e+00 2.00000e+00
      61 6.00000e+00 0.00000e+00 2.00000e+00
      62 6.00000e+00 1.00000e+00 2.00000e+00
      63 5.00000e+00 1.00000e+00 2.00000e+00
part 1
2D uns-elements (description line for part 1)
tria3
       2
     102      15      20      22
     103      22      44      55
hexa8
       1
     104      20      40      44      22      60      61      62      63
part 2
1D uns-elements (description line for part 2)
bar2
       1
     101      31      15
part 3
3D struct-part (description line for part 3)
block iblanked
       2       3       2
 0.00000e+00 2.00000e+00 0.00000e+00 2.00000e+00 0.00000e+00 2.00000e+00
 0.00000e+00 2.00000e+00 0.00000e+00 2.00000e+00 0.00000e+00 2.00000e+00
 0.00000e+00 0.00000e+00 1.00000e+00 1.00000e+00 3.00000e+00 3.00000e+00
 0.00000e+00 0.00000e+00 1.00000e+00 1.00000e+00 3.00000e+00 3.00000e+00
 0.00000e+00 0.00000e+00 0.00000e+00 0.00000e+00 0.00000e+00 0.00000e+00
 2.00000e+00 2.00000e+00 2.00000e+00 2.00000e+00 2.00000e+00 2.00000e+00
       1       1       1       1       1       1       1       1       1       1
       1       1
