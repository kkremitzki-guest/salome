This is the 1st description line of the EnSight Gold geometry example
This is the 2nd description line of the EnSight Gold geometry example
node id given
element id given
extents
 0.00000e+00 6.00000e+00
 0.00000e+00 3.00000e+00
 0.00000e+00 2.00000e+00
part
         1
2D uns-elements (description line for part 1)
coordinates
        10
        15
        20
        40
        22
        44
        55
        60
        61
        62
        63
 4.00000e+00
 5.00000e+00
 6.00000e+00
 5.00000e+00
 6.00000e+00
 6.00000e+00
 5.00000e+00
 6.00000e+00
 6.00000e+00
 5.00000e+00
 0.00000e+00
 0.00000e+00
 0.00000e+00
 1.00000e+00
 1.00000e+00
 3.00000e+00
 0.00000e+00
 0.00000e+00
 1.00000e+00
 1.00000e+00
 0.00000e+00
 0.00000e+00
 0.00000e+00
 0.00000e+00
 0.00000e+00
 0.00000e+00
 2.00000e+00
 2.00000e+00
 2.00000e+00
 2.00000e+00
tria3
         2
       102
       103
         1         2         4
         4         5         6
hexa8
         1
       104
         2         3         5         4         7         8         9        10
part
         2
1D uns-elements (description line for part 2)
coordinates
         2
        15
        31
 4.00000e+00
 3.00000e+00
 0.00000e+00
 0.00000e+00
 0.00000e+00
 0.00000e+00
bar2
         1
       101
         2         1
part
         3
3D struct-part (description line for part 3), (IN
block iblanked
         2         3         2
 0.00000e+00
 2.00000e+00
 0.00000e+00
 2.00000e+00
 0.00000e+00
 2.00000e+00
 0.00000e+00
 2.00000e+00
 0.00000e+00
 2.00000e+00
 0.00000e+00
 2.00000e+00
 0.00000e+00
 0.00000e+00
 1.00000e+00
 1.00000e+00
 3.00000e+00
 3.00000e+00
 0.00000e+00
 0.00000e+00
 1.00000e+00
 1.00000e+00
 3.00000e+00
 3.00000e+00
 0.00000e+00
 0.00000e+00
 0.00000e+00
 0.00000e+00
 0.00000e+00
 0.00000e+00
 2.00000e+00
 2.00000e+00
 2.00000e+00
 2.00000e+00
 2.00000e+00
 2.00000e+00
         1
         1
         1
         1
         1
         1
         1
         1
         1
         1
         1
         1
