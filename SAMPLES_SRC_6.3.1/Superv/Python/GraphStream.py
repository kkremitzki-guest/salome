#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph GraphStream
#
from SuperV import *
# Graph creation 
GraphStream = StreamGraph( 'GraphStream' )
GraphStream.SetStreamParams( 0 , SUPERV.WithoutTrace , 0 )
GraphStream.SetName( 'GraphStream' )
GraphStream.SetAuthor( '' )
GraphStream.SetComment( 'Test of DataStreamPorts in FactoryNodes' )
GraphStream.Coords( 0 , 0 )

# Creation of Factory Nodes

NewDataStream = GraphStream.FNode( 'DataStreamFactory' , 'DataStreamFactory' , 'NewDataStream' )
NewDataStream.SetName( 'NewDataStream' )
NewDataStream.SetAuthor( '' )
NewDataStream.SetContainer( 'FactoryServer' )
NewDataStream.SetComment( 'NewDataStream from DataStreamFactory' )
NewDataStream.Coords( 4 , 10 )
INewDataStreamGate = NewDataStream.GetInPort( 'Gate' )
ONewDataStreamDataStream = NewDataStream.GetOutPort( 'DataStream' )
ONewDataStreamGate = NewDataStream.GetOutPort( 'Gate' )

Add = GraphStream.FNode( 'DataStreamFactory' , 'DataStreamFactory' , 'Add' )
Add.SetName( 'Add' )
Add.SetAuthor( '' )
Add.SetContainer( 'FactoryServer' )
Add.SetComment( 'Add from DataStreamFactory from DataStreamComponent' )
Add.Coords( 6 , 370 )
IAddx = Add.GetInPort( 'x' )
IAddy = Add.GetInPort( 'y' )
IAddGate = Add.GetInPort( 'Gate' )
OAddz = Add.GetOutPort( 'z' )
OAddGate = Add.GetOutPort( 'Gate' )
IAddistream = Add.GetInStreamPort( 'istream' )
IAddistream.SetParams( SUPERV.SCHENULL , SUPERV.INTERNULL , SUPERV.EXTRANULL )
OAddostream = Add.GetOutStreamPort( 'ostream' )
OAddostream.SetNumberOfValues( 0 )
OAddOStream = Add.GetOutStreamPort( 'OStream' )
OAddOStream.SetNumberOfValues( 0 )

Sub = GraphStream.FNode( 'DataStreamFactory' , 'DataStreamFactory' , 'Sub' )
Sub.SetName( 'Sub' )
Sub.SetAuthor( '' )
Sub.SetContainer( 'FactoryServer' )
Sub.SetComment( 'Sub from DataStreamFactory from DataStreamComponent' )
Sub.Coords( 234 , 216 )
ISubx = Sub.GetInPort( 'x' )
ISuby = Sub.GetInPort( 'y' )
ISubGate = Sub.GetInPort( 'Gate' )
OSubz = Sub.GetOutPort( 'z' )
OSubGate = Sub.GetOutPort( 'Gate' )
ISubistream = Sub.GetInStreamPort( 'istream' )
ISubistream.SetParams( SUPERV.SCHENULL , SUPERV.INTERNULL , SUPERV.EXTRANULL )
ISubIstream = Sub.GetInStreamPort( 'Istream' )
ISubIstream.SetParams( SUPERV.SCHENULL , SUPERV.INTERNULL , SUPERV.EXTRANULL )

Mul = GraphStream.FNode( 'DataStreamFactory' , 'DataStreamFactory' , 'Mul' )
Mul.SetName( 'Mul' )
Mul.SetAuthor( '' )
Mul.SetContainer( 'FactoryServer' )
Mul.SetComment( 'Mul from DataStreamFactory from DataStreamComponent' )
Mul.Coords( 448 , 342 )
IMulx = Mul.GetInPort( 'x' )
IMuly = Mul.GetInPort( 'y' )
IMulGate = Mul.GetInPort( 'Gate' )
OMulz = Mul.GetOutPort( 'z' )
OMulGate = Mul.GetOutPort( 'Gate' )

Div = GraphStream.FNode( 'DataStreamFactory' , 'DataStreamFactory' , 'Div' )
Div.SetName( 'Div' )
Div.SetAuthor( '' )
Div.SetContainer( 'FactoryServer' )
Div.SetComment( 'Div from DataStreamFactory from DataStreamComponent' )
Div.Coords( 678 , 214 )
IDivx = Div.GetInPort( 'x' )
IDivy = Div.GetInPort( 'y' )
IDivGate = Div.GetInPort( 'Gate' )
ODivz = Div.GetOutPort( 'z' )
ODivGate = Div.GetOutPort( 'Gate' )
ODivostream = Div.GetOutStreamPort( 'ostream' )
ODivostream.SetNumberOfValues( 0 )

# Creation of Computing Nodes
StreamAdd_ServiceinParameter = []
StreamAdd_ServiceinParameter.append( SALOME_ModuleCatalog.ServicesParameter( 'objref' , 'this' ) )
StreamAdd_ServiceinParameter.append( SALOME_ModuleCatalog.ServicesParameter( 'long' , 'x' ) )
StreamAdd_ServiceinParameter.append( SALOME_ModuleCatalog.ServicesParameter( 'long' , 'y' ) )
StreamAdd_ServiceoutParameter = []
StreamAdd_ServiceoutParameter.append( SALOME_ModuleCatalog.ServicesParameter( 'long' , 'z' ) )
StreamAdd_ServiceinStreamParameter = []
StreamAdd_ServiceinStreamParameter.append( SALOME_ModuleCatalog.ServicesDataStreamParameter( SALOME_ModuleCatalog.DATASTREAM_INTEGER , 'istream' , SALOME_ModuleCatalog.DATASTREAM_ITERATIVE ) )
StreamAdd_ServiceoutStreamParameter = []
StreamAdd_ServiceoutStreamParameter.append( SALOME_ModuleCatalog.ServicesDataStreamParameter( SALOME_ModuleCatalog.DATASTREAM_INTEGER , 'ostream' , SALOME_ModuleCatalog.DATASTREAM_ITERATIVE ) )
StreamAdd_ServiceoutStreamParameter.append( SALOME_ModuleCatalog.ServicesDataStreamParameter( SALOME_ModuleCatalog.DATASTREAM_INTEGER , 'OStream' , SALOME_ModuleCatalog.DATASTREAM_ITERATIVE ) )
StreamAdd_Service = SALOME_ModuleCatalog.Service( 'StreamAdd' , StreamAdd_ServiceinParameter , StreamAdd_ServiceoutParameter , StreamAdd_ServiceinStreamParameter , StreamAdd_ServiceoutStreamParameter , 0 , 0 )
StreamAdd = GraphStream.CNode( StreamAdd_Service )
StreamAdd.SetName( 'StreamAdd' )
StreamAdd.SetAuthor( '' )
StreamAdd.SetComment( 'Compute Node' )
StreamAdd.Coords( 448 , 9 )
IStreamAddthis = StreamAdd.GetInPort( 'this' )
IStreamAddx = StreamAdd.GetInPort( 'x' )
IStreamAddy = StreamAdd.GetInPort( 'y' )
IStreamAddGate = StreamAdd.GetInPort( 'Gate' )
OStreamAddz = StreamAdd.GetOutPort( 'z' )
OStreamAddGate = StreamAdd.GetOutPort( 'Gate' )
IStreamAddistream = StreamAdd.GetInStreamPort( 'istream' )
IStreamAddistream.SetParams( SUPERV.SCHENULL , SUPERV.INTERNULL , SUPERV.EXTRANULL )
OStreamAddostream = StreamAdd.GetOutStreamPort( 'ostream' )
OStreamAddostream.SetNumberOfValues( 0 )
OStreamAddOStream = StreamAdd.GetOutStreamPort( 'OStream' )
OStreamAddOStream.SetNumberOfValues( 0 )

# Creation of Links
LNewDataStreamDataStreamStreamAddthis = GraphStream.Link( ONewDataStreamDataStream , IStreamAddthis )

LAddzSuby = GraphStream.Link( OAddz , ISuby )
LAddzSuby.AddCoord( 1 , 185 , 310 )
LAddzSuby.AddCoord( 2 , 185 , 437 )

LAddzMuly = GraphStream.Link( OAddz , IMuly )

LAddostreamSubistream = GraphStream.StreamLink( OAddostream , ISubistream )
LAddostreamSubistream.AddCoord( 1 , 199 , 343 )
LAddostreamSubistream.AddCoord( 2 , 200 , 497 )

LAddOStreamSubIstream = GraphStream.StreamLink( OAddOStream , ISubIstream )
LAddOStreamSubIstream.AddCoord( 1 , 219 , 374 )
LAddOStreamSubIstream.AddCoord( 2 , 218 , 529 )

LSubzMulx = GraphStream.Link( OSubz , IMulx )
LSubzMulx.AddCoord( 1 , 426 , 408 )
LSubzMulx.AddCoord( 2 , 427 , 282 )

LSubzDivx = GraphStream.Link( OSubz , IDivx )

LMulzDivy = GraphStream.Link( OMulz , IDivy )
LMulzDivy.AddCoord( 1 , 648 , 309 )
LMulzDivy.AddCoord( 2 , 648 , 406 )

# Input datas
IAddx.Input( 3 )
IAddy.Input( 7 )
ISubx.Input( 1 )
IStreamAddx.Input( 1 )
IStreamAddy.Input( 2 )
IAddistream.Input( 1 )
IStreamAddistream.Input( 136159896 )

# Output Ports of the graph
#ODivz = Div.GetOutPort( 'z' )
#OStreamAddz = StreamAdd.GetOutPort( 'z' )

GraphStream.PrintPorts()
GraphStream.IsValid()
status = GraphStream.IsExecutable()
if status == 0 :
    print 'Input of port IStreamAddistream is required : Ok'
else :
    print 'Input of port IStreamAddistream is required : ERROR'
# Following input is required :
IStreamAddistream.Input( 1 )
GraphStream.IsExecutable()

GraphStream.Run()
GraphStream.DoneW()
GraphStream.PrintPorts()
GraphStream.State()

subgraphs = GraphStream.SubGraphsNumber()
i = 1
while i <= subgraphs :
    nodes = GraphStream.SubGraphsNodes( i )
    j = 0
    while j < len(nodes) :
        print 'SubGraph',i,nodes[j].Name()
        j = j + 1
    i = i + 1


substreamgraphs = GraphStream.SubStreamGraphsNumber()
i = 1
while i <= substreamgraphs :
    nodes = GraphStream.SubStreamGraphsNodes( i )
    j = 0
    while j < len(nodes) :
        print 'SubStreamGraph',i,nodes[j].Name()
        j = j + 1
    i = i + 1

GraphStream.Export('/tmp/GraphStream.xml')

GraphStream = StreamGraph( '/tmp/GraphStream.xml' )
GraphStream.Run()
GraphStream.DoneW()
GraphStream.PrintPorts()
print GraphStream.State()

subgraphs = GraphStream.SubGraphsNumber()
i = 1
while i <= subgraphs :
    nodes = GraphStream.SubGraphsNodes( i )
    j = 0
    while j < len(nodes) :
        print 'SubGraph',i,nodes[j].Name()
        j = j + 1
    i = i + 1


substreamgraphs = GraphStream.SubStreamGraphsNumber()
i = 1
while i <= substreamgraphs :
    nodes = GraphStream.SubStreamGraphsNodes( i )
    j = 0
    while j < len(nodes) :
        print 'SubStreamGraph',i,nodes[j].Name()
        j = j + 1
    i = i + 1

