#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph GraphConvertFloatCheck
#
from SuperV import *
# Graph creation 
GraphConvertFloatCheck = Graph( 'GraphConvertFloatCheck' )
GraphConvertFloatCheck.SetName( 'GraphConvertFloatCheck' )
GraphConvertFloatCheck.SetAuthor( 'JR' )
GraphConvertFloatCheck.SetComment( 'Check conversions of Float' )
GraphConvertFloatCheck.Coords( 0 , 0 )

# Creation of Factory Nodes

MiscTypes = GraphConvertFloatCheck.FNode( 'TypesCheck' , 'TypesCheck' , 'MiscTypes' )
MiscTypes.SetName( 'MiscTypes' )
MiscTypes.SetAuthor( '' )
MiscTypes.SetContainer( 'localhost/FactoryServer' )
MiscTypes.SetComment( 'MiscTypes from TypesCheck' )
MiscTypes.Coords( 284 , 28 )

# Creation of InLine Nodes
PyFloat = []
PyFloat.append( 'def Float() :   ' )
PyFloat.append( '    aFloat = 3.1415926535 ' )
PyFloat.append( '    print type(aFloat),"aFloat",aFloat ' )
PyFloat.append( '    return aFloat   ' )
PyFloat.append( ' ' )
Float = GraphConvertFloatCheck.INode( 'Float' , PyFloat )
Float.OutPort( 'OutFloat' , 'float' )
Float.SetName( 'Float' )
Float.SetAuthor( 'JR' )
Float.SetComment( 'InLine Node' )
Float.Coords( 14 , 114 )

# Creation of Links
FloatOutFloat = Float.Port( 'OutFloat' )
MiscTypesInString = GraphConvertFloatCheck.Link( FloatOutFloat , MiscTypes.Port( 'InString' ) )

MiscTypesInBool = GraphConvertFloatCheck.Link( FloatOutFloat , MiscTypes.Port( 'InBool' ) )

MiscTypesInChar = GraphConvertFloatCheck.Link( FloatOutFloat , MiscTypes.Port( 'InChar' ) )

MiscTypesInShort = GraphConvertFloatCheck.Link( FloatOutFloat , MiscTypes.Port( 'InShort' ) )

MiscTypesInLong = GraphConvertFloatCheck.Link( FloatOutFloat , MiscTypes.Port( 'InLong' ) )

MiscTypesInFloat = GraphConvertFloatCheck.Link( FloatOutFloat , MiscTypes.Port( 'InFloat' ) )

MiscTypesInDouble = GraphConvertFloatCheck.Link( FloatOutFloat , MiscTypes.Port( 'InDouble' ) )

MiscTypesInObjRef = GraphConvertFloatCheck.Link( FloatOutFloat , MiscTypes.Port( 'InObjRef' ) )

# Creation of Output variables
MiscTypesOutString = MiscTypes.Port( 'OutString' )
MiscTypesOutBool = MiscTypes.Port( 'OutBool' )
MiscTypesOutChar = MiscTypes.Port( 'OutChar' )
MiscTypesOutShort = MiscTypes.Port( 'OutShort' )
MiscTypesOutLong = MiscTypes.Port( 'OutLong' )
MiscTypesOutFloat = MiscTypes.Port( 'OutFloat' )
MiscTypesOutDouble = MiscTypes.Port( 'OutDouble' )
MiscTypesOutObjRef = MiscTypes.Port( 'OutObjRef' )

GraphConvertFloatCheck.Run()
GraphConvertFloatCheck.DoneW()
GraphConvertFloatCheck.PrintPorts()
