#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Check the Merge of two graphs
# Creation of GraphSwitch
#
from GraphSwitch import *

# Creation of GraphSwitch1
from GraphSwitch1 import *

# Merge of GraphSwitch1 in GraphSwitch
GraphSwitch.Merge( GraphSwitch1 )

# Create a variable for each node of GraphSwitch :
# IsOdd,IsOdd_1,InitLoopSwitch,InitLoop,EndOfInitLoopSwitch,EndOfInitLoop,Switch,Switch_1,EndOfSwitch,EndSwitch
exec GraphSwitch.ListNodes()

# Print Input and Output Ports values of InitLoopSwitch
InitLoopSwitch.PrintPorts()

# Print Input and Output Ports values of InitLoop
InitLoop.PrintPorts()

# Print Input and Output Ports values of GraphSwitch
GraphSwitch.PrintPorts()

# Start asynchronous execution of GraphSwitch
GraphSwitch.Run()

# Wait for completion of GraphSwitch
GraphSwitch.DoneW()

# Print the state of GraphSwitch
GraphSwitch.State()

# Print the results of GraphSwitch
GraphSwitch.PrintPorts()

# Start asynchronous execution of GraphSwitch
GraphSwitch.Run()

# Start asynchronous execution of GraphSwitch1 (parallel to the execution of GraphSwitch)
GraphSwitch1.Run()

# Wait for completion of GraphSwitch1
GraphSwitch1.DoneW()

# Print the state of GraphSwitch1
GraphSwitch.State()

# Wait for completion of GraphSwitch
GraphSwitch.DoneW()

# Print the state of GraphSwitch
GraphSwitch.State()

# Print the results of GraphSwitch
GraphSwitch.PrintPorts()

# Print the results of GraphSwitch1
GraphSwitch1.PrintPorts()

