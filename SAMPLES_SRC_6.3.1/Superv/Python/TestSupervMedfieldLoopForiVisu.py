#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph TestSupervMedfieldLoopForiVisu
#
from SuperV import *

# Graph creation of TestSupervMedfieldLoopForiVisu
def DefTestSupervMedfieldLoopForiVisu() :
    TestSupervMedfieldLoopForiVisu = Graph( 'TestSupervMedfieldLoopForiVisu' )
    TestSupervMedfieldLoopForiVisu.SetName( 'TestSupervMedfieldLoopForiVisu' )
    TestSupervMedfieldLoopForiVisu.SetAuthor( '' )
    TestSupervMedfieldLoopForiVisu.SetComment( '' )
    TestSupervMedfieldLoopForiVisu.Coords( 0 , 0 )
    
    # Creation of Factory Nodes
    
    Mul = TestSupervMedfieldLoopForiVisu.FNode( 'Calculator' , 'Calculator' , 'Mul' )
    Mul.SetName( 'Mul' )
    Mul.SetAuthor( 'LD' )
    Mul.SetContainer( 'localhost/FactoryServer' )
    Mul.SetComment( 'Multiply a field by a constant' )
    Mul.Coords( 387 , 104 )
    IMulfield1 = Mul.GetInPort( 'field1' )
    IMulx1 = Mul.GetInPort( 'x1' )
    IMulGate = Mul.GetInPort( 'Gate' )
    OMulreturn = Mul.GetOutPort( 'return' )
    OMulGate = Mul.GetOutPort( 'Gate' )
    
    # Creation of InLine Nodes
    PyInitDisplay = []
    PyInitDisplay.append( 'from LifeCycleCORBA import *   ' )
    PyInitDisplay.append( 'from VISU import *   ' )
    PyInitDisplay.append( 'def InitDisplay():   ' )
    PyInitDisplay.append( '    # initialize the ORB     ' )
    PyInitDisplay.append( '    orb = CORBA.ORB_init([], CORBA.ORB_ID)   ' )
    PyInitDisplay.append( '    # create an LifeCycleCORBA instance        ' )
    PyInitDisplay.append( '    lcc = LifeCycleCORBA(orb)                  ' )
    PyInitDisplay.append( '    # -----------------------------------------    ' )
    PyInitDisplay.append( '    # loading visu component                       ' )
    PyInitDisplay.append( '    aVisu = lcc.FindOrLoadComponent("FactoryServer","VISU")    ' )
    PyInitDisplay.append( '    print "InitDisplay --> aVisu        = ",aVisu    ' )
    PyInitDisplay.append( '    aViewManager = aVisu.GetViewManager()           ' )
    PyInitDisplay.append( '    print "InitDisplay --> aViewManager = ",aViewManager    ' )
    PyInitDisplay.append( '    # aView = aViewManager.GetCurrentView()        ' )
    PyInitDisplay.append( '    aView = aViewManager.Create3DView()             ' )
    PyInitDisplay.append( '    print "InitDisplay --> aView        = ",aView    ' )
    PyInitDisplay.append( '    return aVisu,aView   ' )
    InitDisplay = TestSupervMedfieldLoopForiVisu.INode( 'InitDisplay' , PyInitDisplay )
    InitDisplay.SetName( 'InitDisplay' )
    InitDisplay.SetAuthor( '' )
    InitDisplay.SetComment( 'Compute Node' )
    InitDisplay.Coords( 0 , 163 )
    IInitDisplayGate = InitDisplay.GetInPort( 'Gate' )
    OInitDisplayaVisu = InitDisplay.OutPort( 'aVisu' , 'objref' )
    OInitDisplayaView = InitDisplay.OutPort( 'aView' , 'objref' )
    OInitDisplayGate = InitDisplay.GetOutPort( 'Gate' )
    
    PyDisplayMed = []
    PyDisplayMed.append( 'import os  ' )
    PyDisplayMed.append( 'import SALOME_MED  ' )
    PyDisplayMed.append( 'from VISU import *  ' )
    PyDisplayMed.append( 'from LifeCycleCORBA import *  ' )
    PyDisplayMed.append( 'def DisplayMed(medfield,index,aVisu,aView):     ' )
    PyDisplayMed.append( '    # initialize the ORB                    ' )
    PyDisplayMed.append( '    orb = CORBA.ORB_init([''], CORBA.ORB_ID)	                    ' )
    PyDisplayMed.append( '    # create an LifeCycleCORBA instance                   ' )
    PyDisplayMed.append( '    lcc = LifeCycleCORBA(orb)                   ' )
    PyDisplayMed.append( '    print " "  ' )
    PyDisplayMed.append( '    print "***************************************************************"  ' )
    PyDisplayMed.append( '    calc = lcc.FindOrLoadComponent("FactoryServer", "Calculator")   ' )
    PyDisplayMed.append( '    print "-----> calc  = ",calc  ' )
    PyDisplayMed.append( '    medfilename = "/tmp/LoopGraphFile" + str(index) + ".med"  ' )
    PyDisplayMed.append( '    print "-----> Writing Med field in temporary file : ",medfilename  ' )
    PyDisplayMed.append( '    calc.writeMEDfile(medfield,medfilename)  ' )
    PyDisplayMed.append( '    print "-----> calc  = ",calc  ' )
    PyDisplayMed.append( '    print "***************************************************************"  ' )
    PyDisplayMed.append( '    myResult1   = aVisu.ImportFile(medfilename)  ' )
    PyDisplayMed.append( '    print "-----> myResult1 = ",myResult1  ' )
    PyDisplayMed.append( '    field_name = medfield.getName()  ' )
    PyDisplayMed.append( '    print "-----> field_name = ",field_name  ' )
    PyDisplayMed.append( '    mesh_name = medfield.getSupport().getMesh().getName()  ' )
    PyDisplayMed.append( '    print "-----> mesh_name = ",mesh_name  ' )
    PyDisplayMed.append( '    aMesh1      = aVisu.MeshOnEntity(myResult1,mesh_name,CELL)  ' )
    PyDisplayMed.append( '    print "-----> aMesh1 = ",aMesh1  ' )
    PyDisplayMed.append( '    aScalarMap1 = aVisu.ScalarMapOnField(myResult1,mesh_name,NODE,field_name,1)  ' )
    PyDisplayMed.append( '    print "-----> aScalarMap1 = ",aScalarMap1  ' )
    PyDisplayMed.append( '    aView.DisplayOnly(aScalarMap1)  ' )
    PyDisplayMed.append( '    aView.FitAll()  ' )
    PyDisplayMed.append( '    command = "rm " + medfilename  ' )
    PyDisplayMed.append( '    os.system(command)  ' )
    PyDisplayMed.append( '    print "-----> Delete temporary Med file : ",medfilename  ' )
    PyDisplayMed.append( '    print "***************************************************************"  ' )
    PyDisplayMed.append( '    print " "  ' )
    PyDisplayMed.append( '    return medfield ' )
    DisplayMed = TestSupervMedfieldLoopForiVisu.INode( 'DisplayMed' , PyDisplayMed )
    DisplayMed.SetName( 'DisplayMed' )
    DisplayMed.SetAuthor( '' )
    DisplayMed.SetComment( 'Compute Node' )
    DisplayMed.Coords( 594 , 105 )
    IDisplayMedINmedfield = DisplayMed.InPort( 'INmedfield' , 'objref' )
    IDisplayMedindex = DisplayMed.InPort( 'index' , 'long' )
    IDisplayMedaVisu = DisplayMed.InPort( 'aVisu' , 'objref' )
    IDisplayMedaView = DisplayMed.InPort( 'aView' , 'objref' )
    IDisplayMedGate = DisplayMed.GetInPort( 'Gate' )
    ODisplayMedOUTmedfield = DisplayMed.OutPort( 'OUTmedfield' , 'objref' )
    ODisplayMedGate = DisplayMed.GetOutPort( 'Gate' )
    
    # Creation of Loop Nodes
    PyInit = []
    PyInit.append( 'def Init(index,min,max,medfield,x1,aVisu,aView):  ' )
    PyInit.append( '    if max >= min :  ' )
    PyInit.append( '	       index = min  ' )
    PyInit.append( '    else : ' )
    PyInit.append( '        index = max  ' )
    PyInit.append( '    print "Init --> index      = ",index   ' )
    PyInit.append( '    print "         min        = ",min   ' )
    PyInit.append( '    print "         max        = ",max   ' )
    PyInit.append( '    print "         medfield   = ",medfield   ' )
    PyInit.append( '    return index,min,max,medfield,x1,aVisu,aView  ' )
    PyMoreInit = []
    PyMoreInit.append( 'def More(index,min,max,medfield,x1,aVisu,aView):  ' )
    PyMoreInit.append( '    if max >= index :   ' )
    PyMoreInit.append( '        DoLoop = 1   ' )
    PyMoreInit.append( '    else :   ' )
    PyMoreInit.append( '        DoLoop = 0   ' )
    PyMoreInit.append( '    print "More --> DoLoop      = ",DoLoop   ' )
    PyMoreInit.append( '    print "         index       = ",index   ' )
    PyMoreInit.append( '    print "         min         = ",min   ' )
    PyMoreInit.append( '    print "         max         = ",max   ' )
    PyMoreInit.append( '    print "         medfield    = ",medfield   ' )
    PyMoreInit.append( '    return DoLoop,index,min,max,medfield,x1,aVisu,aView  ' )
    PyNextInit = []
    PyNextInit.append( 'def Next(index,min,max,medfield,x1,aVisu,aView):  ' )
    PyNextInit.append( '    index = index + 1  ' )
    PyNextInit.append( '    print "Next --> index      = ",index   ' )
    PyNextInit.append( '    print "         min        = ",min   ' )
    PyNextInit.append( '    print "         max        = ",max   ' )
    PyNextInit.append( '    print "         medfield   = ",medfield   ' )
    PyNextInit.append( '    return index,min,max,medfield,x1,aVisu,aView  ' )
    Init,EndInit = TestSupervMedfieldLoopForiVisu.LNode( 'Init' , PyInit , 'More' , PyMoreInit , 'Next' , PyNextInit )
    EndInit.SetName( 'EndInit' )
    EndInit.SetAuthor( 'LD' )
    EndInit.SetComment( 'no comment' )
    EndInit.Coords( 776 , 64 )
    PyEndInit = []
    EndInit.SetPyFunction( '' , PyEndInit )
    IInitDoLoop = Init.GetInPort( 'DoLoop' )
    IInitindex = Init.InPort( 'index' , 'long' )
    IInitmin = Init.InPort( 'min' , 'long' )
    IInitmax = Init.InPort( 'max' , 'long' )
    IInitmedfield = Init.InPort( 'medfield' , 'objref' )
    IInitx1 = Init.InPort( 'x1' , 'double' )
    IInitaVisu = Init.InPort( 'aVisu' , 'objref' )
    IInitaView = Init.InPort( 'aView' , 'objref' )
    IInitGate = Init.GetInPort( 'Gate' )
    OInitDoLoop = Init.GetOutPort( 'DoLoop' )
    OInitindex = Init.GetOutPort( 'index' )
    OInitmin = Init.GetOutPort( 'min' )
    OInitmax = Init.GetOutPort( 'max' )
    OInitmedfield = Init.GetOutPort( 'medfield' )
    OInitx1 = Init.GetOutPort( 'x1' )
    OInitaVisu = Init.GetOutPort( 'aVisu' )
    OInitaView = Init.GetOutPort( 'aView' )
    IEndInitDoLoop = EndInit.GetInPort( 'DoLoop' )
    IEndInitindex = EndInit.GetInPort( 'index' )
    IEndInitmin = EndInit.GetInPort( 'min' )
    IEndInitmax = EndInit.GetInPort( 'max' )
    IEndInitmedfield = EndInit.GetInPort( 'medfield' )
    IEndInitx1 = EndInit.GetInPort( 'x1' )
    IEndInitaVisu = EndInit.GetInPort( 'aVisu' )
    IEndInitaView = EndInit.GetInPort( 'aView' )
    IEndInitGate = EndInit.GetInPort( 'Gate' )
    OEndInitDoLoop = EndInit.GetOutPort( 'DoLoop' )
    OEndInitindex = EndInit.GetOutPort( 'index' )
    OEndInitmin = EndInit.GetOutPort( 'min' )
    OEndInitmax = EndInit.GetOutPort( 'max' )
    OEndInitmedfield = EndInit.GetOutPort( 'medfield' )
    OEndInitx1 = EndInit.GetOutPort( 'x1' )
    OEndInitaVisu = EndInit.GetOutPort( 'aVisu' )
    OEndInitaView = EndInit.GetOutPort( 'aView' )
    OEndInitGate = EndInit.GetOutPort( 'Gate' )
    Init.SetName( 'Init' )
    Init.SetAuthor( 'LD' )
    Init.SetComment( 'no comment' )
    Init.Coords( 205 , 63 )
    
    # Creation of Links
    LInitDisplayaVisuInitaVisu = TestSupervMedfieldLoopForiVisu.Link( OInitDisplayaVisu , IInitaVisu )
    
    LInitDisplayaViewInitaView = TestSupervMedfieldLoopForiVisu.Link( OInitDisplayaView , IInitaView )
    
    LInitindexEndInitindex = TestSupervMedfieldLoopForiVisu.Link( OInitindex , IEndInitindex )
    
    LInitindexDisplayMedindex = TestSupervMedfieldLoopForiVisu.Link( OInitindex , IDisplayMedindex )
    LInitindexDisplayMedindex.AddCoord( 1 , 566 , 215 )
    LInitindexDisplayMedindex.AddCoord( 2 , 566 , 96 )
    
    LInitminEndInitmin = TestSupervMedfieldLoopForiVisu.Link( OInitmin , IEndInitmin )
    
    LInitmaxEndInitmax = TestSupervMedfieldLoopForiVisu.Link( OInitmax , IEndInitmax )
    
    LInitmedfieldMulfield1 = TestSupervMedfieldLoopForiVisu.Link( OInitmedfield , IMulfield1 )
    
    LInitx1EndInitx1 = TestSupervMedfieldLoopForiVisu.Link( OInitx1 , IEndInitx1 )
    
    LInitx1Mulx1 = TestSupervMedfieldLoopForiVisu.Link( OInitx1 , IMulx1 )
    
    LInitaVisuEndInitaVisu = TestSupervMedfieldLoopForiVisu.Link( OInitaVisu , IEndInitaVisu )
    
    LInitaVisuDisplayMedaVisu = TestSupervMedfieldLoopForiVisu.Link( OInitaVisu , IDisplayMedaVisu )
    LInitaVisuDisplayMedaVisu.AddCoord( 1 , 570 , 242 )
    LInitaVisuDisplayMedaVisu.AddCoord( 2 , 569 , 263 )
    LInitaVisuDisplayMedaVisu.AddCoord( 3 , 377 , 264 )
    LInitaVisuDisplayMedaVisu.AddCoord( 4 , 376 , 242 )
    
    LInitaViewEndInitaView = TestSupervMedfieldLoopForiVisu.Link( OInitaView , IEndInitaView )
    
    LInitaViewDisplayMedaView = TestSupervMedfieldLoopForiVisu.Link( OInitaView , IDisplayMedaView )
    
    LMulreturnDisplayMedINmedfield = TestSupervMedfieldLoopForiVisu.Link( OMulreturn , IDisplayMedINmedfield )
    
    LDisplayMedOUTmedfieldEndInitmedfield = TestSupervMedfieldLoopForiVisu.Link( ODisplayMedOUTmedfield , IEndInitmedfield )
    
    # Input datas
    IInitindex.Input( 0 )
    IInitmin.Input( 0 )
    IInitmax.Input( 4 )
    IInitx1.Input( 2 )
    
    # Input Ports of the graph
    #IInitmedfield = Init.GetInPort( 'medfield' )
    
    # Output Ports of the graph
    #OEndInitindex = EndInit.GetOutPort( 'index' )
    #OEndInitmin = EndInit.GetOutPort( 'min' )
    #OEndInitmax = EndInit.GetOutPort( 'max' )
    #OEndInitmedfield = EndInit.GetOutPort( 'medfield' )
    #OEndInitx1 = EndInit.GetOutPort( 'x1' )
    #OEndInitaVisu = EndInit.GetOutPort( 'aVisu' )
    #OEndInitaView = EndInit.GetOutPort( 'aView' )
    return TestSupervMedfieldLoopForiVisu


TestSupervMedfieldLoopForiVisu = DefTestSupervMedfieldLoopForiVisu()
