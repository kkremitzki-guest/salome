#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph test_superv_basic_loop
#
from SuperV import *
# Graph creation 
test_superv_basic_loop = Graph( 'test_superv_basic_loop' )
test_superv_basic_loop.SetName( 'test_superv_basic_loop' )
test_superv_basic_loop.SetAuthor( '' )
test_superv_basic_loop.SetComment( '' )
test_superv_basic_loop.Coords( 0 , 0 )

# Creation of Factory Nodes

# Creation of InLine Nodes
PyAdd = []
PyAdd.append( 'def Sum(sum,incr): ' )
PyAdd.append( '	result = sum+incr     ' )
PyAdd.append( '	return result,incr     ' )
Add = test_superv_basic_loop.INode( 'Sum' , PyAdd )
Add.InPort( 'sum' , 'long' )
Add.InPort( 'incr' , 'long' )
Add.OutPort( 'Sum' , 'long' )
Add.OutPort( 'incr' , 'long' )
Add.SetName( 'Add' )
Add.SetAuthor( '' )
Add.SetComment( 'Python function' )
Add.Coords( 220 , 40 )

# Creation of Loop Nodes
Pyfori = []
Pyfori.append( 'def Init(index,min,max,sum):   ' )
Pyfori.append( '	if max > min :   ' )
Pyfori.append( '		index = min   ' )
Pyfori.append( '	else :   ' )
Pyfori.append( '		index = max   ' )
Pyfori.append( '	#print index,min,max,sum  ' )
Pyfori.append( '	return index,min,max,sum   ' )
PyMorefori = []
PyMorefori.append( 'def More(index,min,max,sum):   ' )
PyMorefori.append( '	OutLoop = 0  ' )
PyMorefori.append( '	if max > index :   ' )
PyMorefori.append( '		OutLoop = 1  ' )
PyMorefori.append( '	#print OutLoop,index,min,max,sum  ' )
PyMorefori.append( '	return OutLoop,index,min,max,sum   ' )
PyNextfori = []
PyNextfori.append( 'def Next(index,min,max,sum):   ' )
PyNextfori.append( '	index = index + 1   ' )
PyNextfori.append( '	#print index,min,max,sum  ' )
PyNextfori.append( '	return index,min,max,sum   ' )
fori,Endfori = test_superv_basic_loop.LNode( 'Init' , Pyfori , 'More' , PyMorefori , 'Next' , PyNextfori )
Endfori.SetName( 'Endfori' )
Endfori.SetAuthor( '' )
Endfori.SetComment( '' )
Endfori.Coords( 432 , 0 )
fori.InPort( 'index' , 'long' )
fori.InPort( 'min' , 'long' )
fori.InPort( 'max' , 'long' )
fori.InPort( 'sum' , 'long' )
fori.OutPort( 'index' , 'long' )
fori.OutPort( 'min' , 'long' )
fori.OutPort( 'max' , 'long' )
fori.OutPort( 'sum' , 'long' )
fori.SetName( 'fori' )
fori.SetAuthor( '' )
fori.SetComment( '' )
fori.Coords( 12 , 0 )

# Creation of Links
AddSum = Add.Port( 'Sum' )
Endforisum = test_superv_basic_loop.Link( AddSum , Endfori.Port( 'sum' ) )

foriindex = fori.Port( 'index' )
Endforiindex = test_superv_basic_loop.Link( foriindex , Endfori.Port( 'index' ) )

Addincr = test_superv_basic_loop.Link( foriindex , Add.Port( 'incr' ) )
Addincr.AddCoord( 1 , 186 , 150 )
Addincr.AddCoord( 2 , 187 , 34 )

forimin = fori.Port( 'min' )
Endforimin = test_superv_basic_loop.Link( forimin , Endfori.Port( 'min' ) )

forimax = fori.Port( 'max' )
Endforimax = test_superv_basic_loop.Link( forimax , Endfori.Port( 'max' ) )

forisum = fori.Port( 'sum' )
Addsum = test_superv_basic_loop.Link( forisum , Add.Port( 'sum' ) )

# Creation of Input datas
foriindex = fori.Input( 'index' , 0)
forimin = fori.Input( 'min' , 0)
forimax = fori.Input( 'max' , 1001)
forisum = fori.Input( 'sum' , 0)

# Creation of Output variables
Addincr = Add.Port( 'incr' )
Endforiindex = Endfori.Port( 'index' )
Endforimin = Endfori.Port( 'min' )
Endforimax = Endfori.Port( 'max' )
Endforisum = Endfori.Port( 'sum' )

test_superv_basic_loop.Run()
test_superv_basic_loop.DoneW()
test_superv_basic_loop.PrintPorts()
