#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph aNewDataFlow_1
#
from SuperV import *

# Graph creation of aNewDataFlow_1
def DefaNewDataFlow_1() :
    aNewDataFlow_1 = Graph( 'aNewDataFlow_1' )
    aNewDataFlow_1.SetName( 'aNewDataFlow_1' )
    aNewDataFlow_1.SetAuthor( '' )
    aNewDataFlow_1.SetComment( '' )
    aNewDataFlow_1.Coords( 0 , 0 )
    
    # Creation of Factory Nodes
    
    SetStudyID = aNewDataFlow_1.FNode( 'GEOM_Superv' , 'GEOM_Superv' , 'SetStudyID' )
    SetStudyID.SetName( 'SetStudyID' )
    SetStudyID.SetAuthor( '' )
    SetStudyID.SetContainer( 'localhost/FactoryServer' )
    SetStudyID.SetComment( 'SetStudyID from GEOM_Superv' )
    SetStudyID.Coords( 9 , 118 )
    ISetStudyIDtheStudyID = SetStudyID.GetInPort( 'theStudyID' )
    ISetStudyIDGate = SetStudyID.GetInPort( 'Gate' )
    OSetStudyIDGate = SetStudyID.GetOutPort( 'Gate' )
    
    MakeSphere = aNewDataFlow_1.FNode( 'GEOM_Superv' , 'GEOM_Superv' , 'MakeSphere' )
    MakeSphere.SetName( 'MakeSphere' )
    MakeSphere.SetAuthor( '' )
    MakeSphere.SetContainer( 'localhost/FactoryServer' )
    MakeSphere.SetComment( 'MakeSphere from GEOM_Superv' )
    MakeSphere.Coords( 202 , 58 )
    IMakeSpheretheX = MakeSphere.GetInPort( 'theX' )
    IMakeSpheretheY = MakeSphere.GetInPort( 'theY' )
    IMakeSpheretheZ = MakeSphere.GetInPort( 'theZ' )
    IMakeSpheretheRadius = MakeSphere.GetInPort( 'theRadius' )
    IMakeSphereGate = MakeSphere.GetInPort( 'Gate' )
    OMakeSpherereturn = MakeSphere.GetOutPort( 'return' )
    OMakeSphereGate = MakeSphere.GetOutPort( 'Gate' )
    
    MakeCopy = aNewDataFlow_1.FNode( 'GEOM_Superv' , 'GEOM_Superv' , 'MakeCopy' )
    MakeCopy.SetName( 'MakeCopy' )
    MakeCopy.SetAuthor( '' )
    MakeCopy.SetContainer( 'localhost/FactoryServer' )
    MakeCopy.SetComment( 'MakeCopy from GEOM_Superv' )
    MakeCopy.Coords( 425 , 11 )
    IMakeCopytheOriginal = MakeCopy.GetInPort( 'theOriginal' )
    IMakeCopyGate = MakeCopy.GetInPort( 'Gate' )
    OMakeCopyreturn = MakeCopy.GetOutPort( 'return' )
    OMakeCopyGate = MakeCopy.GetOutPort( 'Gate' )
    
    TranslateDXDYDZ = aNewDataFlow_1.FNode( 'GEOM_Superv' , 'GEOM_Superv' , 'TranslateDXDYDZ' )
    TranslateDXDYDZ.SetName( 'TranslateDXDYDZ' )
    TranslateDXDYDZ.SetAuthor( '' )
    TranslateDXDYDZ.SetContainer( 'localhost/FactoryServer' )
    TranslateDXDYDZ.SetComment( 'TranslateDXDYDZ from GEOM_Superv' )
    TranslateDXDYDZ.Coords( 428 , 197 )
    ITranslateDXDYDZtheObject = TranslateDXDYDZ.GetInPort( 'theObject' )
    ITranslateDXDYDZtheDX = TranslateDXDYDZ.GetInPort( 'theDX' )
    ITranslateDXDYDZtheDY = TranslateDXDYDZ.GetInPort( 'theDY' )
    ITranslateDXDYDZtheDZ = TranslateDXDYDZ.GetInPort( 'theDZ' )
    ITranslateDXDYDZGate = TranslateDXDYDZ.GetInPort( 'Gate' )
    OTranslateDXDYDZreturn = TranslateDXDYDZ.GetOutPort( 'return' )
    OTranslateDXDYDZGate = TranslateDXDYDZ.GetOutPort( 'Gate' )
    
    MakeFuse = aNewDataFlow_1.FNode( 'GEOM_Superv' , 'GEOM_Superv' , 'MakeFuse' )
    MakeFuse.SetName( 'MakeFuse' )
    MakeFuse.SetAuthor( '' )
    MakeFuse.SetContainer( 'localhost/FactoryServer' )
    MakeFuse.SetComment( 'MakeFuse from GEOM_Superv' )
    MakeFuse.Coords( 651 , 103 )
    IMakeFusetheShape1 = MakeFuse.GetInPort( 'theShape1' )
    IMakeFusetheShape2 = MakeFuse.GetInPort( 'theShape2' )
    IMakeFuseGate = MakeFuse.GetInPort( 'Gate' )
    OMakeFusereturn = MakeFuse.GetOutPort( 'return' )
    OMakeFuseGate = MakeFuse.GetOutPort( 'Gate' )
    
    # Creation of Links
    LSetStudyIDGateMakeSphereGate = aNewDataFlow_1.Link( OSetStudyIDGate , IMakeSphereGate )
    
    LMakeSpherereturnMakeCopytheOriginal = aNewDataFlow_1.Link( OMakeSpherereturn , IMakeCopytheOriginal )
    
    LMakeSpherereturnTranslateDXDYDZtheObject = aNewDataFlow_1.Link( OMakeSpherereturn , ITranslateDXDYDZtheObject )
    
    LMakeSphereGateMakeCopyGate = aNewDataFlow_1.Link( OMakeSphereGate , IMakeCopyGate )
    
    LMakeCopyreturnMakeFusetheShape1 = aNewDataFlow_1.Link( OMakeCopyreturn , IMakeFusetheShape1 )
    
    LMakeCopyGateTranslateDXDYDZGate = aNewDataFlow_1.Link( OMakeCopyGate , ITranslateDXDYDZGate )
    LMakeCopyGateTranslateDXDYDZGate.AddCoord( 1 , 390 , 352 )
    LMakeCopyGateTranslateDXDYDZGate.AddCoord( 2 , 618 , 107 )
    
    LTranslateDXDYDZreturnMakeFusetheShape2 = aNewDataFlow_1.Link( OTranslateDXDYDZreturn , IMakeFusetheShape2 )
    
    LTranslateDXDYDZGateMakeFuseGate = aNewDataFlow_1.Link( OTranslateDXDYDZGate , IMakeFuseGate )
    
    # Input datas
    ISetStudyIDtheStudyID.Input( 1 )
    IMakeSpheretheX.Input( 0 )
    IMakeSpheretheY.Input( 0 )
    IMakeSpheretheZ.Input( 0 )
    IMakeSpheretheRadius.Input( 50 )
    ITranslateDXDYDZtheDX.Input( 80 )
    ITranslateDXDYDZtheDY.Input( 0 )
    ITranslateDXDYDZtheDZ.Input( 0 )
    
    # Output Ports of the graph
    #OMakeFusereturn = MakeFuse.GetOutPort( 'return' )
    return aNewDataFlow_1


aNewDataFlow_1 = DefaNewDataFlow_1()
