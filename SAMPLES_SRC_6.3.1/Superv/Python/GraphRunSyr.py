#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
#  File   : GraphRunSyr.py
#  Module : SuperVisionTest
#
from SuperV import *

# Warning this script has an IOR (object reference) as Input :
# You may have to redefine it with the result of :
#         SyrStruct.ComponentRef( 'FactoryServer' , 'SyrComponent' )
# See the Input Port forN.InPort( 'SyrComponent' , 'ComponentRef' )

#SyrStruct = Graph('../share/salome/resources/SyrStruct.xml')
SyrStruct = Graph('/home/data/jr_HEAD/build/share/salome/resources/SyrStruct.xml')

exec SyrStruct.ListNodes()

forN.Input('min',5)
forN.Input('max',8)

SyrStruct.Start()

endforN.State()

SyrStruct.State()

endforN.Suspend()

forN.State()

N = forN.Port( 'N' )
EndN = endwhileNotOne.Port( 'N' )
K = endforN.Port( 'K' )

forN.Resume()

while SyrStruct.IsDone() == 0 :
    endforN.SuspendedW()
    if SyrStruct.IsDone() == 0 :
        print N.ToString(),'-->',EndN.ToString(),'with',K.ToString(),'steps'
        endforN.Resume()

SyrStruct.State()

SyrStruct.PrintPorts()
