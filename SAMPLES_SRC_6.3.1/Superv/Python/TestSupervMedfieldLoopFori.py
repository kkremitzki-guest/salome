#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph TestSupervMedfieldLoopFori
#
from SuperV import *

# Graph creation of TestSupervMedfieldLoopFori
def DefTestSupervMedfieldLoopFori() :
    TestSupervMedfieldLoopFori = Graph( 'TestSupervMedfieldLoopFori' )
    TestSupervMedfieldLoopFori.SetName( 'TestSupervMedfieldLoopFori' )
    TestSupervMedfieldLoopFori.SetAuthor( '' )
    TestSupervMedfieldLoopFori.SetComment( '' )
    TestSupervMedfieldLoopFori.Coords( 0 , 0 )
    
    # Creation of Factory Nodes
    
    Mul = TestSupervMedfieldLoopFori.FNode( 'Calculator' , 'Calculator' , 'Mul' )
    Mul.SetName( 'Mul' )
    Mul.SetAuthor( 'LD' )
    Mul.SetContainer( 'localhost/FactoryServer' )
    Mul.SetComment( 'Multiply a field by a constant' )
    Mul.Coords( 429 , 46 )
    IMulfield1 = Mul.GetInPort( 'field1' )
    IMulx1 = Mul.GetInPort( 'x1' )
    IMulGate = Mul.GetInPort( 'Gate' )
    OMulreturn = Mul.GetOutPort( 'return' )
    OMulGate = Mul.GetOutPort( 'Gate' )
    
    # Creation of InLine Nodes
    PyInitDisplay = []
    PyInitDisplay.append( 'from VISU import *              ' )
    PyInitDisplay.append( 'import SALOMEDS              ' )
    PyInitDisplay.append( 'from LifeCycleCORBA import *       ' )
    PyInitDisplay.append( 'from SALOME_NamingServicePy import *          ' )
    PyInitDisplay.append( 'def InitDisplay():   ' )
    PyInitDisplay.append( '    # initialize the ORB     ' )
    PyInitDisplay.append( '    orb = CORBA.ORB_init([], CORBA.ORB_ID)   ' )
    PyInitDisplay.append( '    # create an LifeCycleCORBA instance        ' )
    PyInitDisplay.append( '    lcc = LifeCycleCORBA(orb)                  ' )
    PyInitDisplay.append( '    # -----------------------------------------          ' )
    PyInitDisplay.append( '    # Visualization parameters        ' )
    PyInitDisplay.append( '    #create a naming service instance              ' )
    PyInitDisplay.append( '    naming_service = SALOME_NamingServicePy_i(orb)              ' )
    PyInitDisplay.append( '    # get Study Manager reference              ' )
    PyInitDisplay.append( '    obj = naming_service.Resolve("myStudyManager")              ' )
    PyInitDisplay.append( '    myStudyManager = obj._narrow(SALOMEDS.StudyManager)              ' )
    PyInitDisplay.append( '    myStudy = myStudyManager.GetStudyByID(1)              ' )
    PyInitDisplay.append( '    aVisu = lcc.FindOrLoadComponent("FactoryServer", "VISU")              ' )
    PyInitDisplay.append( '    print "InitDisplay --> aVisu        = ",aVisu    ' )
    PyInitDisplay.append( '    aVisu.SetCurrentStudy(myStudy);              ' )
    PyInitDisplay.append( '    aViewManager = aVisu.GetViewManager()               ' )
    PyInitDisplay.append( '    print "InitDisplay --> aViewManager = ",aViewManager    ' )
    PyInitDisplay.append( '    aView = aViewManager.Create3DView()              ' )
    PyInitDisplay.append( '    print "InitDisplay --> aView        = ",aView    ' )
    PyInitDisplay.append( '    return aVisu,aView   ' )
    InitDisplay = TestSupervMedfieldLoopFori.INode( 'InitDisplay' , PyInitDisplay )
    InitDisplay.SetName( 'InitDisplay' )
    InitDisplay.SetAuthor( '' )
    InitDisplay.SetComment( 'Compute Node' )
    InitDisplay.Coords( 1 , 105 )
    IInitDisplayGate = InitDisplay.GetInPort( 'Gate' )
    OInitDisplayaVisu = InitDisplay.OutPort( 'aVisu' , 'objref' )
    OInitDisplayaView = InitDisplay.OutPort( 'aView' , 'objref' )
    OInitDisplayGate = InitDisplay.GetOutPort( 'Gate' )
    
    # Creation of Loop Nodes
    PyInit = []
    PyInit.append( 'def Init(index,min,max,medfield,x1,aVisu,aView):  ' )
    PyInit.append( '    if max >= min :  ' )
    PyInit.append( '	       index = min  ' )
    PyInit.append( '    else : ' )
    PyInit.append( '        index = max  ' )
    PyInit.append( '    print "Init --> index      = ",index   ' )
    PyInit.append( '    print "         min        = ",min   ' )
    PyInit.append( '    print "         max        = ",max   ' )
    PyInit.append( '    print "         medfield   = ",medfield   ' )
    PyInit.append( '    return index,min,max,medfield,x1,aVisu,aView  ' )
    PyMoreInit = []
    PyMoreInit.append( 'def More(index,min,max,medfield,x1,aVisu,aView):  ' )
    PyMoreInit.append( '    if max >= index :   ' )
    PyMoreInit.append( '        DoLoop = 1   ' )
    PyMoreInit.append( '    else :   ' )
    PyMoreInit.append( '        DoLoop = 0   ' )
    PyMoreInit.append( '    print "More --> DoLoop      = ",DoLoop   ' )
    PyMoreInit.append( '    print "         index       = ",index   ' )
    PyMoreInit.append( '    print "         min         = ",min   ' )
    PyMoreInit.append( '    print "         max         = ",max   ' )
    PyMoreInit.append( '    print "         medfield    = ",medfield   ' )
    PyMoreInit.append( '    return DoLoop,index,min,max,medfield,x1,aVisu,aView  ' )
    PyNextInit = []
    PyNextInit.append( 'def Next(index,min,max,medfield,x1,aVisu,aView):  ' )
    PyNextInit.append( '    index = index + 1  ' )
    PyNextInit.append( '    print "Next --> index      = ",index   ' )
    PyNextInit.append( '    print "         min        = ",min   ' )
    PyNextInit.append( '    print "         max        = ",max   ' )
    PyNextInit.append( '    print "         medfield   = ",medfield   ' )
    PyNextInit.append( '    return index,min,max,medfield,x1,aVisu,aView  ' )
    Init,EndInit = TestSupervMedfieldLoopFori.LNode( 'Init' , PyInit , 'More' , PyMoreInit , 'Next' , PyNextInit )
    EndInit.SetName( 'EndInit' )
    EndInit.SetAuthor( 'LD' )
    EndInit.SetComment( 'no comment' )
    EndInit.Coords( 631 , 5 )
    PyEndInit = []
    EndInit.SetPyFunction( '' , PyEndInit )
    IInitDoLoop = Init.GetInPort( 'DoLoop' )
    IInitindex = Init.InPort( 'index' , 'long' )
    IInitmin = Init.InPort( 'min' , 'long' )
    IInitmax = Init.InPort( 'max' , 'long' )
    IInitmedfield = Init.InPort( 'medfield' , 'objref' )
    IInitx1 = Init.InPort( 'x1' , 'double' )
    IInitaVisu = Init.InPort( 'aVisu' , 'objref' )
    IInitaView = Init.InPort( 'aView' , 'objref' )
    IInitGate = Init.GetInPort( 'Gate' )
    OInitDoLoop = Init.GetOutPort( 'DoLoop' )
    OInitindex = Init.GetOutPort( 'index' )
    OInitmin = Init.GetOutPort( 'min' )
    OInitmax = Init.GetOutPort( 'max' )
    OInitmedfield = Init.GetOutPort( 'medfield' )
    OInitx1 = Init.GetOutPort( 'x1' )
    OInitaVisu = Init.GetOutPort( 'aVisu' )
    OInitaView = Init.GetOutPort( 'aView' )
    IEndInitDoLoop = EndInit.GetInPort( 'DoLoop' )
    IEndInitindex = EndInit.GetInPort( 'index' )
    IEndInitmin = EndInit.GetInPort( 'min' )
    IEndInitmax = EndInit.GetInPort( 'max' )
    IEndInitmedfield = EndInit.GetInPort( 'medfield' )
    IEndInitx1 = EndInit.GetInPort( 'x1' )
    IEndInitaVisu = EndInit.GetInPort( 'aVisu' )
    IEndInitaView = EndInit.GetInPort( 'aView' )
    IEndInitGate = EndInit.GetInPort( 'Gate' )
    OEndInitDoLoop = EndInit.GetOutPort( 'DoLoop' )
    OEndInitindex = EndInit.GetOutPort( 'index' )
    OEndInitmin = EndInit.GetOutPort( 'min' )
    OEndInitmax = EndInit.GetOutPort( 'max' )
    OEndInitmedfield = EndInit.GetOutPort( 'medfield' )
    OEndInitx1 = EndInit.GetOutPort( 'x1' )
    OEndInitaVisu = EndInit.GetOutPort( 'aVisu' )
    OEndInitaView = EndInit.GetOutPort( 'aView' )
    OEndInitGate = EndInit.GetOutPort( 'Gate' )
    Init.SetName( 'Init' )
    Init.SetAuthor( 'LD' )
    Init.SetComment( 'no comment' )
    Init.Coords( 216 , 5 )
    
    # Creation of Links
    LInitDisplayaVisuInitaVisu = TestSupervMedfieldLoopFori.Link( OInitDisplayaVisu , IInitaVisu )
    
    LInitDisplayaViewInitaView = TestSupervMedfieldLoopFori.Link( OInitDisplayaView , IInitaView )
    
    LInitindexEndInitindex = TestSupervMedfieldLoopFori.Link( OInitindex , IEndInitindex )
    
    LInitminEndInitmin = TestSupervMedfieldLoopFori.Link( OInitmin , IEndInitmin )
    
    LInitmaxEndInitmax = TestSupervMedfieldLoopFori.Link( OInitmax , IEndInitmax )
    
    LInitmedfieldMulfield1 = TestSupervMedfieldLoopFori.Link( OInitmedfield , IMulfield1 )
    
    LInitx1EndInitx1 = TestSupervMedfieldLoopFori.Link( OInitx1 , IEndInitx1 )
    
    LInitx1Mulx1 = TestSupervMedfieldLoopFori.Link( OInitx1 , IMulx1 )
    
    LInitaVisuEndInitaVisu = TestSupervMedfieldLoopFori.Link( OInitaVisu , IEndInitaVisu )
    
    LInitaViewEndInitaView = TestSupervMedfieldLoopFori.Link( OInitaView , IEndInitaView )
    
    LMulreturnEndInitmedfield = TestSupervMedfieldLoopFori.Link( OMulreturn , IEndInitmedfield )
    
    # Input datas
    IInitindex.Input( 0 )
    IInitmin.Input( 0 )
    IInitmax.Input( 55 )
    IInitmedfield.Input( 'IOR:010000001f00000049444c3a53414c4f4d455f4d45442f4649454c44444f55424c453a312e300000010000000000000026000000010100000a0000003132372e302e302e3100ead00e000000fe1948673f000058e60000000021' )
    IInitx1.Input( 2 )
    
    # Output Ports of the graph
    #OEndInitindex = EndInit.GetOutPort( 'index' )
    #OEndInitmin = EndInit.GetOutPort( 'min' )
    #OEndInitmax = EndInit.GetOutPort( 'max' )
    #OEndInitmedfield = EndInit.GetOutPort( 'medfield' )
    #OEndInitx1 = EndInit.GetOutPort( 'x1' )
    #OEndInitaVisu = EndInit.GetOutPort( 'aVisu' )
    #OEndInitaView = EndInit.GetOutPort( 'aView' )
    return TestSupervMedfieldLoopFori


TestSupervMedfieldLoopFori = DefTestSupervMedfieldLoopFori()
