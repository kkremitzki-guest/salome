#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph GraphGOTOAddMemory
#
from SuperV import *

# Graph creation of GraphGOTOAddMemory
def DefGraphGOTOAddMemory() :
    GraphGOTOAddMemory = Graph( 'GraphGOTOAddMemory' )
    GraphGOTOAddMemory.SetName( 'GraphGOTOAddMemory' )
    GraphGOTOAddMemory.SetAuthor( 'JR' )
    GraphGOTOAddMemory.SetComment( 'To study memory leaks' )
    GraphGOTOAddMemory.Coords( 0 , 0 )
    
    # Creation of Factory Nodes
    
    AddWithoutSleep = GraphGOTOAddMemory.FNode( 'AddComponent' , 'AddComponent' , 'AddWithoutSleep' )
    AddWithoutSleep.SetName( 'AddWithoutSleep' )
    AddWithoutSleep.SetAuthor( '' )
    AddWithoutSleep.SetContainer( 'localhost/FactoryServer' )
    AddWithoutSleep.SetComment( 'AddWithoutSleep from AddComponent' )
    AddWithoutSleep.Coords( 297 , 138 )
    IAddWithoutSleepx = AddWithoutSleep.GetInPort( 'x' )
    IAddWithoutSleepy = AddWithoutSleep.GetInPort( 'y' )
    IAddWithoutSleepGate = AddWithoutSleep.GetInPort( 'Gate' )
    OAddWithoutSleepFuncValue = AddWithoutSleep.GetOutPort( 'FuncValue' )
    OAddWithoutSleepz = AddWithoutSleep.GetOutPort( 'z' )
    OAddWithoutSleepGate = AddWithoutSleep.GetOutPort( 'Gate' )
    
    # Creation of InLine Nodes
    PyInLine = []
    InLine = GraphGOTOAddMemory.INode( '' , PyInLine )
    InLine.SetName( 'InLine' )
    InLine.SetAuthor( '' )
    InLine.SetComment( 'Compute Node' )
    InLine.Coords( 74 , 178 )
    IInLineGate = InLine.GetInPort( 'Gate' )
    OInLineGate = InLine.GetOutPort( 'Gate' )
    
    # Creation of GOTO Nodes
    PyGoTo = []
    GoTo = GraphGOTOAddMemory.GNode( '' , PyGoTo , 'InLine' )
    GoTo.SetName( 'GoTo' )
    GoTo.SetAuthor( '' )
    GoTo.SetComment( 'Compute Node' )
    GoTo.Coords( 533 , 218 )
    IGoToGate = GoTo.GetInPort( 'Gate' )
    OGoToGate = GoTo.GetOutPort( 'Gate' )
    
    # Creation of Links
    LInLineGateAddWithoutSleepGate = GraphGOTOAddMemory.Link( OInLineGate , IAddWithoutSleepGate )
    
    LGoToGateInLineGate = GraphGOTOAddMemory.Link( OGoToGate , IInLineGate )
    LGoToGateInLineGate.AddCoord( 1 , 69 , 420 )
    LGoToGateInLineGate.AddCoord( 2 , 700 , 420 )
    
    LAddWithoutSleepGateGoToGate = GraphGOTOAddMemory.Link( OAddWithoutSleepGate , IGoToGate )
    
    # Input datas
    IAddWithoutSleepx.Input( 1 )
    IAddWithoutSleepy.Input( 2 )
    
    # Output Ports of the graph
    #OAddWithoutSleepFuncValue = AddWithoutSleep.GetOutPort( 'FuncValue' )
    #OAddWithoutSleepz = AddWithoutSleep.GetOutPort( 'z' )
    return GraphGOTOAddMemory


GraphGOTOAddMemory = DefGraphGOTOAddMemory()

GraphGOTOAddMemory.Run()
GraphGOTOAddMemory.DoneW()
GraphGOTOAddMemory.State()
GraphGOTOAddMemory.PrintPorts()
