#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph GraphLoopSwitchsBranches_1
#
from SuperV import *

# Graph creation of GraphLoopSwitchsBranches_1
def DefGraphLoopSwitchsBranches_1() :
    GraphLoopSwitchsBranches_1 = Graph( 'GraphLoopSwitchsBranches_1' )
    GraphLoopSwitchsBranches_1.SetName( 'GraphLoopSwitchsBranches_1' )
    GraphLoopSwitchsBranches_1.SetAuthor( 'JR' )
    GraphLoopSwitchsBranches_1.SetComment( '' )
    GraphLoopSwitchsBranches_1.Coords( 0 , 0 )
    
    # Creation of Factory Nodes
    
    # Creation of InLine Nodes
    PyIsOdd = []
    PyIsOdd.append( 'from time import *     ' )
    PyIsOdd.append( 'def IsOdd(a) :         ' )
    PyIsOdd.append( '    print a,"IsOdd"        ' )
    PyIsOdd.append( '    sleep( 1 )     ' )
    PyIsOdd.append( '    return a,1 ' )
    IsOdd = GraphLoopSwitchsBranches_1.INode( 'IsOdd' , PyIsOdd )
    IsOdd.SetName( 'IsOdd' )
    IsOdd.SetAuthor( '' )
    IsOdd.SetComment( 'Python function' )
    IsOdd.Coords( 476 , 2 )
    IIsOdda = IsOdd.InPort( 'a' , 'long' )
    IIsOddGate = IsOdd.GetInPort( 'Gate' )
    OIsOdda = IsOdd.OutPort( 'a' , 'long' )
    OIsOddOdd = IsOdd.OutPort( 'Odd' , 'long' )
    OIsOddGate = IsOdd.GetOutPort( 'Gate' )
    
    PyIsEven = []
    PyIsEven.append( 'from time import *     ' )
    PyIsEven.append( 'def IsEven(a) :         ' )
    PyIsEven.append( '    print a,"IsEven"        ' )
    PyIsEven.append( '    sleep( 1 )     ' )
    PyIsEven.append( '    return a,1 ' )
    IsEven = GraphLoopSwitchsBranches_1.INode( 'IsEven' , PyIsEven )
    IsEven.SetName( 'IsEven' )
    IsEven.SetAuthor( '' )
    IsEven.SetComment( 'Python function' )
    IsEven.Coords( 472 , 724 )
    IIsEvena = IsEven.InPort( 'a' , 'long' )
    IIsEvenGate = IsEven.GetInPort( 'Gate' )
    OIsEvena = IsEven.OutPort( 'a' , 'long' )
    OIsEvenEven = IsEven.OutPort( 'Even' , 'long' )
    OIsEvenGate = IsEven.GetOutPort( 'Gate' )
    
    PySwitchsCompare = []
    PySwitchsCompare.append( 'from time import *  ' )
    PySwitchsCompare.append( 'def SwitchsCompare(aOdd,Odd,aEven,Even,Index) :  ' )
    PySwitchsCompare.append( '    sleep(1)  ' )
    PySwitchsCompare.append( '    return Index ' )
    SwitchsCompare = GraphLoopSwitchsBranches_1.INode( 'SwitchsCompare' , PySwitchsCompare )
    SwitchsCompare.SetName( 'SwitchsCompare' )
    SwitchsCompare.SetAuthor( '' )
    SwitchsCompare.SetComment( 'Compute Node' )
    SwitchsCompare.Coords( 918 , 221 )
    ISwitchsCompareaOdd = SwitchsCompare.InPort( 'aOdd' , 'long' )
    ISwitchsCompareOdd = SwitchsCompare.InPort( 'Odd' , 'boolean' )
    ISwitchsCompareaEven = SwitchsCompare.InPort( 'aEven' , 'long' )
    ISwitchsCompareEven = SwitchsCompare.InPort( 'Even' , 'boolean' )
    ISwitchsCompareIndex = SwitchsCompare.InPort( 'Index' , 'long' )
    ISwitchsCompareGate = SwitchsCompare.GetInPort( 'Gate' )
    OSwitchsCompareIndex = SwitchsCompare.OutPort( 'Index' , 'long' )
    OSwitchsCompareGate = SwitchsCompare.GetOutPort( 'Gate' )
    
    PyIsNotOdd = []
    PyIsNotOdd.append( 'from time import * ' )
    PyIsNotOdd.append( 'def IsNotOdd(Even) : ' )
    PyIsNotOdd.append( '    sleep(1) ' )
    PyIsNotOdd.append( '    return Even ' )
    IsNotOdd = GraphLoopSwitchsBranches_1.INode( 'IsNotOdd' , PyIsNotOdd )
    IsNotOdd.SetName( 'IsNotOdd' )
    IsNotOdd.SetAuthor( '' )
    IsNotOdd.SetComment( 'Compute Node' )
    IsNotOdd.Coords( 477 , 145 )
    IIsNotOddEven = IsNotOdd.InPort( 'Even' , 'long' )
    IIsNotOddGate = IsNotOdd.GetInPort( 'Gate' )
    OIsNotOddEven = IsNotOdd.OutPort( 'Even' , 'long' )
    OIsNotOddGate = IsNotOdd.GetOutPort( 'Gate' )
    
    PyIsNotEven = []
    PyIsNotEven.append( 'from time import *  ' )
    PyIsNotEven.append( 'def IsNotEven(Odd) :  ' )
    PyIsNotEven.append( '    sleep(1)  ' )
    PyIsNotEven.append( '    return Odd ' )
    IsNotEven = GraphLoopSwitchsBranches_1.INode( 'IsNotEven' , PyIsNotEven )
    IsNotEven.SetName( 'IsNotEven' )
    IsNotEven.SetAuthor( '' )
    IsNotEven.SetComment( 'Compute Node' )
    IsNotEven.Coords( 477 , 570 )
    IIsNotEvenOdd = IsNotEven.InPort( 'Odd' , 'long' )
    IIsNotEvenGate = IsNotEven.GetInPort( 'Gate' )
    OIsNotEvenOdd = IsNotEven.OutPort( 'Odd' , 'long' )
    OIsNotEvenGate = IsNotEven.GetOutPort( 'Gate' )
    
    # Creation of Loop Nodes
    PyLoop = []
    PyLoop.append( 'def InitLoop(Index,Min,Max) :      ' )
    PyLoop.append( '	return Index,Min,Max     ' )
    PyMoreLoop = []
    PyMoreLoop.append( 'def MoreLoop(Index,Min,Max) :     ' )
    PyMoreLoop.append( '	if Index <= Max :   ' )
    PyMoreLoop.append( '		DoLoop = 1     ' )
    PyMoreLoop.append( '	else :     ' )
    PyMoreLoop.append( '		DoLoop = 0     ' )
    PyMoreLoop.append( '	return DoLoop,Index,Min,Max     ' )
    PyNextLoop = []
    PyNextLoop.append( 'def NextLoop(Index,Min,Max) :     ' )
    PyNextLoop.append( '	Index = Index + 1     ' )
    PyNextLoop.append( '	return Index,Min,Max     ' )
    Loop,EndOfLoop = GraphLoopSwitchsBranches_1.LNode( 'InitLoop' , PyLoop , 'MoreLoop' , PyMoreLoop , 'NextLoop' , PyNextLoop )
    EndOfLoop.SetName( 'EndOfLoop' )
    EndOfLoop.SetAuthor( '' )
    EndOfLoop.SetComment( 'Compute Node' )
    EndOfLoop.Coords( 1099 , 261 )
    PyEndOfLoop = []
    EndOfLoop.SetPyFunction( '' , PyEndOfLoop )
    ILoopDoLoop = Loop.GetInPort( 'DoLoop' )
    ILoopIndex = Loop.InPort( 'Index' , 'long' )
    ILoopMin = Loop.InPort( 'Min' , 'long' )
    ILoopMax = Loop.InPort( 'Max' , 'long' )
    ILoopGate = Loop.GetInPort( 'Gate' )
    OLoopDoLoop = Loop.GetOutPort( 'DoLoop' )
    OLoopIndex = Loop.GetOutPort( 'Index' )
    OLoopMin = Loop.GetOutPort( 'Min' )
    OLoopMax = Loop.GetOutPort( 'Max' )
    IEndOfLoopDoLoop = EndOfLoop.GetInPort( 'DoLoop' )
    IEndOfLoopIndex = EndOfLoop.GetInPort( 'Index' )
    IEndOfLoopMin = EndOfLoop.GetInPort( 'Min' )
    IEndOfLoopMax = EndOfLoop.GetInPort( 'Max' )
    IEndOfLoopGate = EndOfLoop.GetInPort( 'Gate' )
    OEndOfLoopDoLoop = EndOfLoop.GetOutPort( 'DoLoop' )
    OEndOfLoopIndex = EndOfLoop.GetOutPort( 'Index' )
    OEndOfLoopMin = EndOfLoop.GetOutPort( 'Min' )
    OEndOfLoopMax = EndOfLoop.GetOutPort( 'Max' )
    OEndOfLoopGate = EndOfLoop.GetOutPort( 'Gate' )
    Loop.SetName( 'Loop' )
    Loop.SetAuthor( '' )
    Loop.SetComment( 'Compute Node' )
    Loop.Coords( 19 , 341 )
    
    # Creation of Switch Nodes
    PySwitchOdd = []
    PySwitchOdd.append( 'from time import *    ' )
    PySwitchOdd.append( 'def SwitchOdd(a) :      ' )
    PySwitchOdd.append( '    sleep(1)  ' )
    PySwitchOdd.append( '    OddEven = 'Odd'  ' )
    PySwitchOdd.append( '    if (a&1) == 0 :  ' )
    PySwitchOdd.append( '        OddEven = 'Even'  ' )
    PySwitchOdd.append( '    return a & 1,1-(a&1),a,OddEven  ' )
    SwitchOdd,EndOfSwitchOdd = GraphLoopSwitchsBranches_1.SNode( 'SwitchOdd' , PySwitchOdd )
    EndOfSwitchOdd.SetName( 'EndOfSwitchOdd' )
    EndOfSwitchOdd.SetAuthor( '' )
    EndOfSwitchOdd.SetComment( 'Compute Node' )
    EndOfSwitchOdd.Coords( 715 , 208 )
    PyEndOfSwitchOdd = []
    PyEndOfSwitchOdd.append( 'from time import *  ' )
    PyEndOfSwitchOdd.append( 'def EndOfSwitchOdd(a,Odd,Even,OddEven) :  ' )
    PyEndOfSwitchOdd.append( '    sleep(1)  ' )
    PyEndOfSwitchOdd.append( '    return a,Odd  ' )
    EndOfSwitchOdd.SetPyFunction( 'EndOfSwitchOdd' , PyEndOfSwitchOdd )
    IEndOfSwitchOdda = EndOfSwitchOdd.InPort( 'a' , 'long' )
    IEndOfSwitchOddOdd = EndOfSwitchOdd.InPort( 'Odd' , 'boolean' )
    IEndOfSwitchOddEven = EndOfSwitchOdd.InPort( 'Even' , 'long' )
    IEndOfSwitchOddOddEven = EndOfSwitchOdd.InPort( 'OddEven' , 'boolean' )
    IEndOfSwitchOddDefault = EndOfSwitchOdd.GetInPort( 'Default' )
    OEndOfSwitchOdda = EndOfSwitchOdd.OutPort( 'a' , 'long' )
    OEndOfSwitchOddOdd = EndOfSwitchOdd.OutPort( 'Odd' , 'boolean' )
    OEndOfSwitchOddGate = EndOfSwitchOdd.GetOutPort( 'Gate' )
    SwitchOdd.SetName( 'SwitchOdd' )
    SwitchOdd.SetAuthor( '' )
    SwitchOdd.SetComment( 'Compute Node' )
    SwitchOdd.Coords( 241 , 208 )
    ISwitchOdda = SwitchOdd.InPort( 'a' , 'long' )
    ISwitchOddGate = SwitchOdd.GetInPort( 'Gate' )
    OSwitchOddOdd = SwitchOdd.OutPort( 'Odd' , 'long' )
    OSwitchOddEven = SwitchOdd.OutPort( 'Even' , 'int' )
    OSwitchOdda = SwitchOdd.OutPort( 'a' , 'int' )
    OSwitchOddOddEven = SwitchOdd.OutPort( 'OddEven' , 'string' )
    OSwitchOddDefault = SwitchOdd.GetOutPort( 'Default' )
    
    PySwitchEven = []
    PySwitchEven.append( 'from time import *      ' )
    PySwitchEven.append( 'def SwitchEven(a) :    ' )
    PySwitchEven.append( '    sleep(1)   ' )
    PySwitchEven.append( '    OddEven = 'Even'   ' )
    PySwitchEven.append( '    if (a&1) != 0 :   ' )
    PySwitchEven.append( '        OddEven = 'Odd'  ' )
    PySwitchEven.append( '    return a & 1,1-(a&1),a,OddEven   ' )
    SwitchEven,EndOfSwitchEven = GraphLoopSwitchsBranches_1.SNode( 'SwitchEven' , PySwitchEven )
    EndOfSwitchEven.SetName( 'EndOfSwitchEven' )
    EndOfSwitchEven.SetAuthor( '' )
    EndOfSwitchEven.SetComment( 'Compute Node' )
    EndOfSwitchEven.Coords( 722 , 425 )
    PyEndOfSwitchEven = []
    PyEndOfSwitchEven.append( 'from time import *  ' )
    PyEndOfSwitchEven.append( 'def EndOfSwitchEven(a,Even,Odd,OddEven) :  ' )
    PyEndOfSwitchEven.append( '    sleep(1)  ' )
    PyEndOfSwitchEven.append( '    return a,Even  ' )
    EndOfSwitchEven.SetPyFunction( 'EndOfSwitchEven' , PyEndOfSwitchEven )
    IEndOfSwitchEvena = EndOfSwitchEven.InPort( 'a' , 'long' )
    IEndOfSwitchEvenEven = EndOfSwitchEven.InPort( 'Even' , 'boolean' )
    IEndOfSwitchEvenOdd = EndOfSwitchEven.InPort( 'Odd' , 'long' )
    IEndOfSwitchEvenOddEven = EndOfSwitchEven.InPort( 'OddEven' , 'string' )
    IEndOfSwitchEvenDefault = EndOfSwitchEven.GetInPort( 'Default' )
    OEndOfSwitchEvena = EndOfSwitchEven.OutPort( 'a' , 'long' )
    OEndOfSwitchEvenEven = EndOfSwitchEven.OutPort( 'Even' , 'boolean' )
    OEndOfSwitchEvenGate = EndOfSwitchEven.GetOutPort( 'Gate' )
    SwitchEven.SetName( 'SwitchEven' )
    SwitchEven.SetAuthor( '' )
    SwitchEven.SetComment( 'Compute Node' )
    SwitchEven.Coords( 228 , 425 )
    ISwitchEvena = SwitchEven.InPort( 'a' , 'long' )
    ISwitchEvenGate = SwitchEven.GetInPort( 'Gate' )
    OSwitchEvenOdd = SwitchEven.OutPort( 'Odd' , 'long' )
    OSwitchEvenEven = SwitchEven.OutPort( 'Even' , 'int' )
    OSwitchEvena = SwitchEven.OutPort( 'a' , 'int' )
    OSwitchEvenOddEven = SwitchEven.OutPort( 'OddEven' , 'string' )
    OSwitchEvenDefault = SwitchEven.GetOutPort( 'Default' )
    
    # Creation of Links
    LIsOddaEndOfSwitchOdda = GraphLoopSwitchsBranches_1.Link( OIsOdda , IEndOfSwitchOdda )
    
    LIsOddOddEndOfSwitchOddOdd = GraphLoopSwitchsBranches_1.Link( OIsOddOdd , IEndOfSwitchOddOdd )
    
    LSwitchOddOddIsOddGate = GraphLoopSwitchsBranches_1.Link( OSwitchOddOdd , IIsOddGate )
    
    LSwitchOddEvenIsNotOddEven = GraphLoopSwitchsBranches_1.Link( OSwitchOddEven , IIsNotOddEven )
    
    LSwitchOddaIsOdda = GraphLoopSwitchsBranches_1.Link( OSwitchOdda , IIsOdda )
    
    LSwitchOddOddEvenEndOfSwitchOddOddEven = GraphLoopSwitchsBranches_1.Link( OSwitchOddOddEven , IEndOfSwitchOddOddEven )
    
    LSwitchOddDefaultEndOfSwitchOddDefault = GraphLoopSwitchsBranches_1.Link( OSwitchOddDefault , IEndOfSwitchOddDefault )
    
    LEndOfSwitchOddaSwitchsCompareaOdd = GraphLoopSwitchsBranches_1.Link( OEndOfSwitchOdda , ISwitchsCompareaOdd )
    
    LEndOfSwitchOddOddSwitchsCompareOdd = GraphLoopSwitchsBranches_1.Link( OEndOfSwitchOddOdd , ISwitchsCompareOdd )
    
    LIsEvenaEndOfSwitchEvena = GraphLoopSwitchsBranches_1.Link( OIsEvena , IEndOfSwitchEvena )
    
    LIsEvenEvenEndOfSwitchEvenEven = GraphLoopSwitchsBranches_1.Link( OIsEvenEven , IEndOfSwitchEvenEven )
    
    LLoopIndexSwitchOdda = GraphLoopSwitchsBranches_1.Link( OLoopIndex , ISwitchOdda )
    
    LLoopIndexSwitchEvena = GraphLoopSwitchsBranches_1.Link( OLoopIndex , ISwitchEvena )
    
    LLoopIndexSwitchsCompareIndex = GraphLoopSwitchsBranches_1.Link( OLoopIndex , ISwitchsCompareIndex )
    
    LLoopMinEndOfLoopMin = GraphLoopSwitchsBranches_1.Link( OLoopMin , IEndOfLoopMin )
    
    LLoopMaxEndOfLoopMax = GraphLoopSwitchsBranches_1.Link( OLoopMax , IEndOfLoopMax )
    
    LSwitchEvenOddIsNotEvenOdd = GraphLoopSwitchsBranches_1.Link( OSwitchEvenOdd , IIsNotEvenOdd )
    
    LSwitchEvenEvenIsEvenGate = GraphLoopSwitchsBranches_1.Link( OSwitchEvenEven , IIsEvenGate )
    
    LSwitchEvenaIsEvena = GraphLoopSwitchsBranches_1.Link( OSwitchEvena , IIsEvena )
    
    LSwitchEvenOddEvenEndOfSwitchEvenOddEven = GraphLoopSwitchsBranches_1.Link( OSwitchEvenOddEven , IEndOfSwitchEvenOddEven )
    
    LSwitchEvenDefaultEndOfSwitchEvenDefault = GraphLoopSwitchsBranches_1.Link( OSwitchEvenDefault , IEndOfSwitchEvenDefault )
    
    LEndOfSwitchEvenaSwitchsCompareaEven = GraphLoopSwitchsBranches_1.Link( OEndOfSwitchEvena , ISwitchsCompareaEven )
    
    LEndOfSwitchEvenEvenSwitchsCompareEven = GraphLoopSwitchsBranches_1.Link( OEndOfSwitchEvenEven , ISwitchsCompareEven )
    
    LSwitchsCompareIndexEndOfLoopIndex = GraphLoopSwitchsBranches_1.Link( OSwitchsCompareIndex , IEndOfLoopIndex )
    
    LIsNotOddEvenEndOfSwitchOddEven = GraphLoopSwitchsBranches_1.Link( OIsNotOddEven , IEndOfSwitchOddEven )
    
    LIsNotEvenOddEndOfSwitchEvenOdd = GraphLoopSwitchsBranches_1.Link( OIsNotEvenOdd , IEndOfSwitchEvenOdd )
    
    # Input datas
    ILoopIndex.Input( 0 )
    ILoopMin.Input( 0 )
    ILoopMax.Input( 23 )
    
    # Output Ports of the graph
    #OEndOfLoopIndex = EndOfLoop.GetOutPort( 'Index' )
    #OEndOfLoopMin = EndOfLoop.GetOutPort( 'Min' )
    #OEndOfLoopMax = EndOfLoop.GetOutPort( 'Max' )
    return GraphLoopSwitchsBranches_1


GraphLoopSwitchsBranches_1 = DefGraphLoopSwitchsBranches_1()
