#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph GraphUnValid
#
from SuperV import *

# Graph creation of GraphUnValid
def DefGraphUnValid() :
    GraphUnValid = Graph( 'GraphUnValid' )
    GraphUnValid.SetName( 'GraphUnValid' )
    GraphUnValid.SetAuthor( 'JR' )
    GraphUnValid.SetComment( '' )
    GraphUnValid.Coords( 0 , 0 )
    
    # Creation of Factory Nodes
    
    Add = GraphUnValid.FNode( 'AddComponent' , 'AddComponent' , 'Add' )
    Add.SetName( 'Add' )
    Add.SetAuthor( '' )
    Add.SetContainer( 'FactoryServer' )
    Add.SetComment( 'Add from AddComponent' )
    Add.Coords( 8 , 62 )
    IAddx = Add.GetInPort( 'x' )
    IAddy = Add.GetInPort( 'y' )
    IAddGate = Add.GetInPort( 'Gate' )
    OAddFuncValue = Add.GetOutPort( 'FuncValue' )
    OAddz = Add.GetOutPort( 'z' )
    OAddGate = Add.GetOutPort( 'Gate' )
    
    Sub = GraphUnValid.FNode( 'SubComponent' , 'SubComponent' , 'Sub' )
    Sub.SetName( 'Sub' )
    Sub.SetAuthor( '' )
    Sub.SetContainer( 'FactoryServer' )
    Sub.SetComment( 'Sub from SubComponent' )
    Sub.Coords( 203 , 4 )
    ISubx = Sub.GetInPort( 'x' )
    ISuby = Sub.GetInPort( 'y' )
    ISubGate = Sub.GetInPort( 'Gate' )
    OSubz = Sub.GetOutPort( 'z' )
    OSubGate = Sub.GetOutPort( 'Gate' )
    
    Mul = GraphUnValid.FNode( 'MulComponent' , 'MulComponent' , 'Mul' )
    Mul.SetName( 'Mul' )
    Mul.SetAuthor( '' )
    Mul.SetContainer( 'FactoryServer' )
    Mul.SetComment( 'Mul from MulComponent' )
    Mul.Coords( 390 , 62 )
    IMulx = Mul.GetInPort( 'x' )
    IMuly = Mul.GetInPort( 'y' )
    IMulGate = Mul.GetInPort( 'Gate' )
    OMulz = Mul.GetOutPort( 'z' )
    OMulGate = Mul.GetOutPort( 'Gate' )
    
    Div = GraphUnValid.FNode( 'DivComponent' , 'DivComponent' , 'Div' )
    Div.SetName( 'Div' )
    Div.SetAuthor( '' )
    Div.SetContainer( 'FactoryServer' )
    Div.SetComment( 'Div from DivComponent' )
    Div.Coords( 575 , 11 )
    IDivx = Div.GetInPort( 'x' )
    IDivy = Div.GetInPort( 'y' )
    IDivGate = Div.GetInPort( 'Gate' )
    ODivz = Div.GetOutPort( 'z' )
    ODivGate = Div.GetOutPort( 'Gate' )
    
    # Creation of Links
    LAddzMuly = GraphUnValid.Link( OAddz , IMuly )
    
    LAddzSuby = GraphUnValid.Link( OAddz , ISuby )
    LAddzSuby.AddCoord( 1 , 182 , 113 )
    LAddzSuby.AddCoord( 2 , 183 , 170 )
    
    LSubzDivx = GraphUnValid.Link( OSubz , IDivx )
    LSubzDivx.AddCoord( 1 , 566 , 91 )
    LSubzDivx.AddCoord( 2 , 566 , 49 )
    LSubzDivx.AddCoord( 3 , 380 , 49 )
    LSubzDivx.AddCoord( 4 , 379 , 84 )
    
    LSubzMulx = GraphUnValid.Link( OSubz , IMulx )
    LSubzMulx.AddCoord( 1 , 379 , 141 )
    LSubzMulx.AddCoord( 2 , 378 , 85 )
    
    LMulzDivy = GraphUnValid.Link( OMulz , IDivy )
    LMulzDivy.AddCoord( 1 , 565 , 120 )
    LMulzDivy.AddCoord( 2 , 565 , 141 )
    
    LMulzSubx = GraphUnValid.Link( OMulz , ISubx )
    LMulzSubx.AddCoord( 1 , 194 , 84 )
    LMulzSubx.AddCoord( 2 , 195 , 252 )
    LMulzSubx.AddCoord( 3 , 566 , 252 )
    LMulzSubx.AddCoord( 4 , 565 , 141 )
    
    # Input datas
    IAddx.Input( 3 )
    IAddy.Input( 4.5 )
    
    # Output Ports of the graph
    #OAddFuncValue = Add.GetOutPort( 'FuncValue' )
    #ODivz = Div.GetOutPort( 'z' )
    return GraphUnValid


GraphUnValid = DefGraphUnValid()
