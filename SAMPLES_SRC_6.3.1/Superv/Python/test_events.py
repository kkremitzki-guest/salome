#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph aNewDataFlow
#
from SuperV import *
# Graph creation 
aNewDataFlow = Graph( 'aNewDataFlow' )
aNewDataFlow.SetName( 'aNewDataFlow' )
aNewDataFlow.SetAuthor( '' )
aNewDataFlow.SetComment( '' )
aNewDataFlow.Coords( 0 , 0 )

# Creation of Factory Nodes

# Creation of InLine Nodes
PyFunc = []
PyFunc.append( 'def Func( A ):     ' )
PyFunc.append( '  import batchmode_visu  ' )
PyFunc.append( '  batchmode_visu.myVisu.CreateTestView()   ' )
PyFunc.append( '  batchmode_visu.myVisu.ShowTestObject()   ' )
PyFunc.append( '  return A     ' )
Func = aNewDataFlow.INode( 'Func' , PyFunc )
Func.SetName( 'Func' )
Func.SetAuthor( '' )
Func.SetComment( 'Compute Node' )
Func.Coords( 375 , 162 )
Func.InPort( 'A' , 'int' )
Func.OutPort( 'B' , 'int' )

# Creation of Loop Nodes
PyInit = []
PyInit.append( 'def Init( A ):   ' )
PyInit.append( '  return A   ' )
PyMoreInit = []
PyMoreInit.append( 'def More( A ):   ' )
PyMoreInit.append( '  if A < 10:   ' )
PyMoreInit.append( '    return 1,A   ' )
PyMoreInit.append( '  return 0,A   ' )
PyNextInit = []
PyNextInit.append( 'def Next( A ):   ' )
PyNextInit.append( '  return (A + 1)   ' )
Init,EndInit = aNewDataFlow.LNode( 'Init' , PyInit , 'More' , PyMoreInit , 'Next' , PyNextInit )
EndInit.SetName( 'EndInit' )
EndInit.SetAuthor( '' )
EndInit.SetComment( '' )
EndInit.Coords( 740 , 168 )
PyEndInit = []
EndInit.SetPyFunction( 'EndInit' , PyEndInit )
Init.SetName( 'Init' )
Init.SetAuthor( '' )
Init.SetComment( '' )
Init.Coords( 118 , 160 )
Init.InPort( 'A' , 'int' )
Init.OutPort( 'A' , 'int' )

# Creation of Links
InitA = Init.Port( 'A' )
FuncA = aNewDataFlow.Link( InitA , Func.Port( 'A' ) )

FuncB = Func.Port( 'B' )
EndInitA = aNewDataFlow.Link( FuncB , EndInit.Port( 'A' ) )

# Creation of Input datas
InitA = Init.Input( 'A' , 0)

# Creation of Output variables
EndInitA = EndInit.Port( 'A' )
