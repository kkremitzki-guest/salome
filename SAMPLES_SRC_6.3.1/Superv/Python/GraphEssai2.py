#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph GraphEssai2
#
from SuperV import *
# Graph creation 
GraphEssai2 = Graph( 'GraphEssai2' )
GraphEssai2.SetName( 'GraphEssai2' )
GraphEssai2.SetAuthor( '' )
GraphEssai2.SetComment( '' )
GraphEssai2.Coords( 0 , 0 )

# Creation of Factory Nodes

Add = GraphEssai2.FNode( 'AddComponent' , 'AddComponent' , 'Add' )
Add.SetName( 'Add' )
Add.SetAuthor( '' )
Add.SetContainer( 'localhost/FactoryServer' )
Add.SetComment( 'Add from AddComponent' )
Add.Coords( 18 , 239 )

Sub = GraphEssai2.FNode( 'SubComponent' , 'SubComponent' , 'Sub' )
Sub.SetName( 'Sub' )
Sub.SetAuthor( '' )
Sub.SetContainer( 'localhost/FactoryServer' )
Sub.SetComment( 'Sub from SubComponent' )
Sub.Coords( 232 , 269 )

Mul = GraphEssai2.FNode( 'MulComponent' , 'MulComponent' , 'Mul' )
Mul.SetName( 'Mul' )
Mul.SetAuthor( '' )
Mul.SetContainer( 'localhost/FactoryServer' )
Mul.SetComment( 'Mul from MulComponent' )
Mul.Coords( 431 , 22 )

Div = GraphEssai2.FNode( 'DivComponent' , 'DivComponent' , 'Div' )
Div.SetName( 'Div' )
Div.SetAuthor( '' )
Div.SetContainer( 'localhost/FactoryServer' )
Div.SetComment( 'Div from DivComponent' )
Div.Coords( 645 , 268 )

# Creation of Links
AddFuncValue = Add.Port( 'FuncValue' )
Mulx = GraphEssai2.Link( AddFuncValue , Mul.Port( 'x' ) )
Mulx.AddCoord( 1 , 199 , 102 )
Mulx.AddCoord( 2 , 198 , 319 )

Addz = Add.Port( 'z' )
Subx = GraphEssai2.Link( Addz , Sub.Port( 'x' ) )

Subz = Sub.Port( 'z' )
Divx = GraphEssai2.Link( Subz , Div.Port( 'x' ) )

SubOutGate = Sub.Port( 'OutGate' )
MulInGate = GraphEssai2.Link( SubOutGate , Mul.Port( 'InGate' ) )
MulInGate.AddCoord( 1 , 405 , 165 )
MulInGate.AddCoord( 2 , 405 , 412 )

Mulz = Mul.Port( 'z' )
Divy = GraphEssai2.Link( Mulz , Div.Port( 'y' ) )
Divy.AddCoord( 1 , 612 , 377 )
Divy.AddCoord( 2 , 612 , 103 )

# Creation of Input datas
Addx = Add.Input( 'x' , 1)
Addy = Add.Input( 'y' , 2)
Suby = Sub.Input( 'y' , 3)
Muly = Mul.Input( 'y' , 4)

# Creation of Output variables
Divz = Div.Port( 'z' )

GraphEssai2.Run()

GraphEssai2.DoneW()

GraphEssai2.State()

GraphEssai2.PrintPorts()

