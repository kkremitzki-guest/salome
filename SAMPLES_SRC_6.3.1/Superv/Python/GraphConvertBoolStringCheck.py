#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph GraphConvertBoolStringCheck
#
from SuperV import *
# Graph creation 
GraphConvertBoolStringCheck = Graph( 'GraphConvertBoolStringCheck' )
GraphConvertBoolStringCheck.SetName( 'GraphConvertBoolStringCheck' )
GraphConvertBoolStringCheck.SetAuthor( 'JR' )
GraphConvertBoolStringCheck.SetComment( 'Check conversions of String' )
GraphConvertBoolStringCheck.Coords( 0 , 0 )

# Creation of Factory Nodes

MiscTypes = GraphConvertBoolStringCheck.FNode( 'TypesCheck' , 'TypesCheck' , 'MiscTypes' )
MiscTypes.SetName( 'MiscTypes' )
MiscTypes.SetAuthor( '' )
MiscTypes.SetContainer( 'localhost/FactoryServer' )
MiscTypes.SetComment( 'MiscTypes from TypesCheck' )
MiscTypes.Coords( 284 , 28 )

# Creation of InLine Nodes
PyBoolString = []
PyBoolString.append( 'def StringString() :  ' )
PyBoolString.append( '    string = "1" ' )
PyBoolString.append( '    return string  ' )
PyBoolString.append( ' ' )
BoolString = GraphConvertBoolStringCheck.INode( 'StringString' , PyBoolString )
BoolString.OutPort( 'OutString' , 'string' )
BoolString.SetName( 'BoolString' )
BoolString.SetAuthor( 'JR' )
BoolString.SetComment( 'InLine Node' )
BoolString.Coords( 14 , 114 )

# Creation of Links
BoolStringOutString = BoolString.Port( 'OutString' )
MiscTypesInString = GraphConvertBoolStringCheck.Link( BoolStringOutString , MiscTypes.Port( 'InString' ) )

MiscTypesInBool = GraphConvertBoolStringCheck.Link( BoolStringOutString , MiscTypes.Port( 'InBool' ) )

MiscTypesInChar = GraphConvertBoolStringCheck.Link( BoolStringOutString , MiscTypes.Port( 'InChar' ) )

MiscTypesInShort = GraphConvertBoolStringCheck.Link( BoolStringOutString , MiscTypes.Port( 'InShort' ) )

MiscTypesInLong = GraphConvertBoolStringCheck.Link( BoolStringOutString , MiscTypes.Port( 'InLong' ) )

MiscTypesInFloat = GraphConvertBoolStringCheck.Link( BoolStringOutString , MiscTypes.Port( 'InFloat' ) )

MiscTypesInDouble = GraphConvertBoolStringCheck.Link( BoolStringOutString , MiscTypes.Port( 'InDouble' ) )

MiscTypesInObjRef = GraphConvertBoolStringCheck.Link( BoolStringOutString , MiscTypes.Port( 'InObjRef' ) )

# Creation of Output variables
MiscTypesOutString = MiscTypes.Port( 'OutString' )
MiscTypesOutBool = MiscTypes.Port( 'OutBool' )
MiscTypesOutChar = MiscTypes.Port( 'OutChar' )
MiscTypesOutShort = MiscTypes.Port( 'OutShort' )
MiscTypesOutLong = MiscTypes.Port( 'OutLong' )
MiscTypesOutFloat = MiscTypes.Port( 'OutFloat' )
MiscTypesOutDouble = MiscTypes.Port( 'OutDouble' )
MiscTypesOutObjRef = MiscTypes.Port( 'OutObjRef' )

GraphConvertBoolStringCheck.Run()
GraphConvertBoolStringCheck.DoneW()
GraphConvertBoolStringCheck.PrintPorts()
