#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph GraphPyFunctions_1
#
from SuperV import *

# Graph creation of GraphPyFunctions_1
def DefGraphPyFunctions_1() :
    GraphPyFunctions_1 = Graph( 'GraphPyFunctions_1' )
    GraphPyFunctions_1.SetName( 'GraphPyFunctions_1' )
    GraphPyFunctions_1.SetAuthor( '' )
    GraphPyFunctions_1.SetComment( '' )
    GraphPyFunctions_1.Coords( 0 , 0 )
    
    # Creation of Factory Nodes
    
    # Creation of InLine Nodes
    PyIsOdd = []
    IsOdd = GraphPyFunctions_1.INode( 'IsOdd' , PyIsOdd )
    IsOdd.SetName( 'IsOdd' )
    IsOdd.SetAuthor( '' )
    IsOdd.SetComment( 'Python function' )
    IsOdd.Coords( 388 , 50 )
    IIsOdda = IsOdd.InPort( 'a' , 'long' )
    IIsOddGate = IsOdd.GetInPort( 'Gate' )
    OIsOdda = IsOdd.OutPort( 'a' , 'long' )
    OIsOddGate = IsOdd.GetOutPort( 'Gate' )
    
    PyIsOdd_1 = []
    PyIsOdd_1.append( 'from time import *   ' )
    PyIsOdd_1.append( 'def IsOdd(a) :       ' )
    PyIsOdd_1.append( '    print a,"IsOdd"      ' )
    PyIsOdd_1.append( '    sleep( 1 )   ' )
    PyIsOdd_1.append( '    return a     ' )
    IsOdd_1 = GraphPyFunctions_1.INode( 'IsOdd' , PyIsOdd_1 )
    IsOdd_1.SetName( 'IsOdd_1' )
    IsOdd_1.SetAuthor( '' )
    IsOdd_1.SetComment( 'Python function' )
    IsOdd_1.Coords( 391 , 288 )
    IIsOdd_1a = IsOdd_1.InPort( 'a' , 'long' )
    IIsOdd_1Gate = IsOdd_1.GetInPort( 'Gate' )
    OIsOdd_1a = IsOdd_1.OutPort( 'a' , 'long' )
    OIsOdd_1Gate = IsOdd_1.GetOutPort( 'Gate' )
    
    # Creation of Loop Nodes
    PyInitLoop = []
    PyInitLoop.append( 'def InitLoop(Index,Min,Max) :      ' )
    PyInitLoop.append( '	return Index,Min,Max     ' )
    PyMoreInitLoop = []
    PyMoreInitLoop.append( 'def MoreLoop(Index,Min,Max) :     ' )
    PyMoreInitLoop.append( '	if Index <= Max :   ' )
    PyMoreInitLoop.append( '		DoLoop = 1     ' )
    PyMoreInitLoop.append( '	else :     ' )
    PyMoreInitLoop.append( '		DoLoop = 0     ' )
    PyMoreInitLoop.append( '	return DoLoop,Index,Min,Max     ' )
    PyNextInitLoop = []
    PyNextInitLoop.append( 'def NextLoop(Index,Min,Max) :     ' )
    PyNextInitLoop.append( '	Index = Index + 1     ' )
    PyNextInitLoop.append( '	return Index,Min,Max     ' )
    InitLoop,EndOfInitLoop = GraphPyFunctions_1.LNode( 'InitLoop' , PyInitLoop , 'MoreLoop' , PyMoreInitLoop , 'NextLoop' , PyNextInitLoop )
    EndOfInitLoop.SetName( 'EndOfInitLoop' )
    EndOfInitLoop.SetAuthor( '' )
    EndOfInitLoop.SetComment( 'Compute Node' )
    EndOfInitLoop.Coords( 766 , 127 )
    PyEndOfInitLoop = []
    EndOfInitLoop.SetPyFunction( 'EndInitLoop' , PyEndOfInitLoop )
    IInitLoopDoLoop = InitLoop.GetInPort( 'DoLoop' )
    IInitLoopIndex = InitLoop.InPort( 'Index' , 'long' )
    IInitLoopMin = InitLoop.InPort( 'Min' , 'long' )
    IInitLoopMax = InitLoop.InPort( 'Max' , 'long' )
    IInitLoopGate = InitLoop.GetInPort( 'Gate' )
    OInitLoopDoLoop = InitLoop.GetOutPort( 'DoLoop' )
    OInitLoopIndex = InitLoop.GetOutPort( 'Index' )
    OInitLoopMin = InitLoop.GetOutPort( 'Min' )
    OInitLoopMax = InitLoop.GetOutPort( 'Max' )
    IEndOfInitLoopDoLoop = EndOfInitLoop.GetInPort( 'DoLoop' )
    IEndOfInitLoopIndex = EndOfInitLoop.GetInPort( 'Index' )
    IEndOfInitLoopMin = EndOfInitLoop.GetInPort( 'Min' )
    IEndOfInitLoopMax = EndOfInitLoop.GetInPort( 'Max' )
    IEndOfInitLoopGate = EndOfInitLoop.GetInPort( 'Gate' )
    OEndOfInitLoopDoLoop = EndOfInitLoop.GetOutPort( 'DoLoop' )
    OEndOfInitLoopIndex = EndOfInitLoop.GetOutPort( 'Index' )
    OEndOfInitLoopMin = EndOfInitLoop.GetOutPort( 'Min' )
    OEndOfInitLoopMax = EndOfInitLoop.GetOutPort( 'Max' )
    OEndOfInitLoopGate = EndOfInitLoop.GetOutPort( 'Gate' )
    InitLoop.SetName( 'InitLoop' )
    InitLoop.SetAuthor( '' )
    InitLoop.SetComment( 'Compute Node' )
    InitLoop.Coords( 10 , 129 )
    
    PyInitLoop_1 = []
    PyMoreInitLoop_1 = []
    PyMoreInitLoop_1.append( '' )
    PyNextInitLoop_1 = []
    PyNextInitLoop_1.append( '' )
    InitLoop_1,EndOfInitLoop_1 = GraphPyFunctions_1.LNode( 'InitLoop' , PyInitLoop_1 , 'MoreLoop' , PyMoreInitLoop_1 , 'NextLoop' , PyNextInitLoop_1 )
    EndOfInitLoop_1.SetName( 'EndOfInitLoop_1' )
    EndOfInitLoop_1.SetAuthor( '' )
    EndOfInitLoop_1.SetComment( 'Compute Node' )
    EndOfInitLoop_1.Coords( 784 , 361 )
    PyEndOfInitLoop_1 = []
    EndOfInitLoop_1.SetPyFunction( 'EndInitLoop' , PyEndOfInitLoop_1 )
    IInitLoop_1DoLoop = InitLoop_1.GetInPort( 'DoLoop' )
    IInitLoop_1Index = InitLoop_1.InPort( 'Index' , 'long' )
    IInitLoop_1Min = InitLoop_1.InPort( 'Min' , 'long' )
    IInitLoop_1Max = InitLoop_1.InPort( 'Max' , 'long' )
    IInitLoop_1Gate = InitLoop_1.GetInPort( 'Gate' )
    OInitLoop_1DoLoop = InitLoop_1.GetOutPort( 'DoLoop' )
    OInitLoop_1Index = InitLoop_1.GetOutPort( 'Index' )
    OInitLoop_1Min = InitLoop_1.GetOutPort( 'Min' )
    OInitLoop_1Max = InitLoop_1.GetOutPort( 'Max' )
    IEndOfInitLoop_1DoLoop = EndOfInitLoop_1.GetInPort( 'DoLoop' )
    IEndOfInitLoop_1Index = EndOfInitLoop_1.GetInPort( 'Index' )
    IEndOfInitLoop_1Min = EndOfInitLoop_1.GetInPort( 'Min' )
    IEndOfInitLoop_1Max = EndOfInitLoop_1.GetInPort( 'Max' )
    IEndOfInitLoop_1Gate = EndOfInitLoop_1.GetInPort( 'Gate' )
    OEndOfInitLoop_1DoLoop = EndOfInitLoop_1.GetOutPort( 'DoLoop' )
    OEndOfInitLoop_1Index = EndOfInitLoop_1.GetOutPort( 'Index' )
    OEndOfInitLoop_1Min = EndOfInitLoop_1.GetOutPort( 'Min' )
    OEndOfInitLoop_1Max = EndOfInitLoop_1.GetOutPort( 'Max' )
    OEndOfInitLoop_1Gate = EndOfInitLoop_1.GetOutPort( 'Gate' )
    InitLoop_1.SetName( 'InitLoop_1' )
    InitLoop_1.SetAuthor( '' )
    InitLoop_1.SetComment( 'Compute Node' )
    InitLoop_1.Coords( 12 , 369 )
    
    # Creation of Switch Nodes
    PySwitch = []
    Switch,EndOfSwitch = GraphPyFunctions_1.SNode( 'Switch' , PySwitch )
    EndOfSwitch.SetName( 'EndOfSwitch' )
    EndOfSwitch.SetAuthor( '' )
    EndOfSwitch.SetComment( 'Compute Node' )
    EndOfSwitch.Coords( 587 , 126 )
    PyEndOfSwitch = []
    EndOfSwitch.SetPyFunction( 'EndOfSwitch_1' , PyEndOfSwitch )
    IEndOfSwitcha = EndOfSwitch.InPort( 'a' , 'long' )
    IEndOfSwitchDefault = EndOfSwitch.GetInPort( 'Default' )
    OEndOfSwitcha = EndOfSwitch.OutPort( 'a' , 'long' )
    OEndOfSwitchGate = EndOfSwitch.GetOutPort( 'Gate' )
    Switch.SetName( 'Switch' )
    Switch.SetAuthor( '' )
    Switch.SetComment( 'Compute Node' )
    Switch.Coords( 186 , 130 )
    ISwitcha = Switch.InPort( 'a' , 'long' )
    ISwitchGate = Switch.GetInPort( 'Gate' )
    OSwitchOdd = Switch.OutPort( 'Odd' , 'long' )
    OSwitchEven = Switch.OutPort( 'Even' , 'int' )
    OSwitcha = Switch.OutPort( 'a' , 'int' )
    OSwitchDefault = Switch.GetOutPort( 'Default' )
    
    PySwitch_1 = []
    PySwitch_1.append( 'from time import *  ' )
    PySwitch_1.append( 'def Switch(a) :    ' )
    PySwitch_1.append( '    if ( a & 1 ) == 0 :  ' )
    PySwitch_1.append( '        sleep(1)  ' )
    PySwitch_1.append( '    return a & 1,1-(a&1),a    ' )
    Switch_1,EndOfSwitch_1 = GraphPyFunctions_1.SNode( 'Switch' , PySwitch_1 )
    EndOfSwitch_1.SetName( 'EndOfSwitch_1' )
    EndOfSwitch_1.SetAuthor( '' )
    EndOfSwitch_1.SetComment( 'Compute Node' )
    EndOfSwitch_1.Coords( 594 , 362 )
    PyEndOfSwitch_1 = []
    EndOfSwitch_1.SetPyFunction( 'EndOfSwitch_1' , PyEndOfSwitch_1 )
    IEndOfSwitch_1a = EndOfSwitch_1.InPort( 'a' , 'long' )
    IEndOfSwitch_1Default = EndOfSwitch_1.GetInPort( 'Default' )
    OEndOfSwitch_1a = EndOfSwitch_1.OutPort( 'a' , 'long' )
    OEndOfSwitch_1Gate = EndOfSwitch_1.GetOutPort( 'Gate' )
    Switch_1.SetName( 'Switch_1' )
    Switch_1.SetAuthor( '' )
    Switch_1.SetComment( 'Compute Node' )
    Switch_1.Coords( 185 , 368 )
    ISwitch_1a = Switch_1.InPort( 'a' , 'long' )
    ISwitch_1Gate = Switch_1.GetInPort( 'Gate' )
    OSwitch_1Odd = Switch_1.OutPort( 'Odd' , 'long' )
    OSwitch_1Even = Switch_1.OutPort( 'Even' , 'int' )
    OSwitch_1a = Switch_1.OutPort( 'a' , 'int' )
    OSwitch_1Default = Switch_1.GetOutPort( 'Default' )
    
    # Creation of Links
    LIsOddaEndOfSwitcha = GraphPyFunctions_1.Link( OIsOdda , IEndOfSwitcha )
    LIsOddaEndOfSwitcha.AddCoord( 1 , 571 , 160 )
    LIsOddaEndOfSwitcha.AddCoord( 2 , 571 , 129 )
    
    LIsOdd_1aEndOfSwitch_1a = GraphPyFunctions_1.Link( OIsOdd_1a , IEndOfSwitch_1a )
    LIsOdd_1aEndOfSwitch_1a.AddCoord( 1 , 580 , 396 )
    LIsOdd_1aEndOfSwitch_1a.AddCoord( 2 , 580 , 368 )
    
    LInitLoopIndexSwitcha = GraphPyFunctions_1.Link( OInitLoopIndex , ISwitcha )
    
    LInitLoopMinEndOfInitLoopMin = GraphPyFunctions_1.Link( OInitLoopMin , IEndOfInitLoopMin )
    
    LInitLoopMaxEndOfInitLoopMax = GraphPyFunctions_1.Link( OInitLoopMax , IEndOfInitLoopMax )
    
    LInitLoop_1IndexSwitch_1a = GraphPyFunctions_1.Link( OInitLoop_1Index , ISwitch_1a )
    
    LInitLoop_1MinEndOfInitLoop_1Min = GraphPyFunctions_1.Link( OInitLoop_1Min , IEndOfInitLoop_1Min )
    
    LInitLoop_1MaxEndOfInitLoop_1Max = GraphPyFunctions_1.Link( OInitLoop_1Max , IEndOfInitLoop_1Max )
    
    LSwitchOddIsOddGate = GraphPyFunctions_1.Link( OSwitchOdd , IIsOddGate )
    
    LSwitchaIsOdda = GraphPyFunctions_1.Link( OSwitcha , IIsOdda )
    LSwitchaIsOdda.AddCoord( 1 , 366 , 131 )
    LSwitchaIsOdda.AddCoord( 2 , 365 , 223 )
    
    LSwitchDefaultEndOfSwitchDefault = GraphPyFunctions_1.Link( OSwitchDefault , IEndOfSwitchDefault )
    LSwitchDefaultEndOfSwitchDefault.AddCoord( 1 , 572 , 194 )
    LSwitchDefaultEndOfSwitchDefault.AddCoord( 2 , 571 , 257 )
    
    LEndOfSwitchaEndOfInitLoopIndex = GraphPyFunctions_1.Link( OEndOfSwitcha , IEndOfInitLoopIndex )
    
    LSwitch_1OddIsOdd_1Gate = GraphPyFunctions_1.Link( OSwitch_1Odd , IIsOdd_1Gate )
    
    LSwitch_1aIsOdd_1a = GraphPyFunctions_1.Link( OSwitch_1a , IIsOdd_1a )
    LSwitch_1aIsOdd_1a.AddCoord( 1 , 368 , 368 )
    LSwitch_1aIsOdd_1a.AddCoord( 2 , 367 , 460 )
    
    LSwitch_1DefaultEndOfSwitch_1Default = GraphPyFunctions_1.Link( OSwitch_1Default , IEndOfSwitch_1Default )
    
    LEndOfSwitch_1aEndOfInitLoop_1Index = GraphPyFunctions_1.Link( OEndOfSwitch_1a , IEndOfInitLoop_1Index )
    
    # Input datas
    IInitLoopIndex.Input( 0 )
    IInitLoopMin.Input( 0 )
    IInitLoopMax.Input( 25 )
    IInitLoop_1Index.Input( 0 )
    IInitLoop_1Min.Input( 0 )
    IInitLoop_1Max.Input( 35 )
    
    # Output Ports of the graph
    #OEndOfInitLoopIndex = EndOfInitLoop.GetOutPort( 'Index' )
    #OEndOfInitLoopMin = EndOfInitLoop.GetOutPort( 'Min' )
    #OEndOfInitLoopMax = EndOfInitLoop.GetOutPort( 'Max' )
    #OEndOfInitLoop_1Index = EndOfInitLoop_1.GetOutPort( 'Index' )
    #OEndOfInitLoop_1Min = EndOfInitLoop_1.GetOutPort( 'Min' )
    #OEndOfInitLoop_1Max = EndOfInitLoop_1.GetOutPort( 'Max' )
    #OSwitchEven = Switch.GetOutPort( 'Even' )
    #OSwitch_1Even = Switch_1.GetOutPort( 'Even' )
    return GraphPyFunctions_1


GraphPyFunctions_1 = DefGraphPyFunctions_1()
