#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph GraphLoopsCoupled_3
#
from SuperV import *

# Graph creation of GraphLoopsCoupled_3
def DefGraphLoopsCoupled_3() :
    GraphLoopsCoupled_3 = Graph( 'GraphLoopsCoupled_3' )
    GraphLoopsCoupled_3.SetName( 'GraphLoopsCoupled_3' )
    GraphLoopsCoupled_3.SetAuthor( 'JR' )
    GraphLoopsCoupled_3.SetComment( '' )
    GraphLoopsCoupled_3.Coords( 0 , 0 )
    
    # Creation of Factory Nodes
    
    # Creation of InLine Nodes
    PyAdd = []
    PyAdd.append( 'def Add(x,y) : ' )
    PyAdd.append( '	return x+y ' )
    PyAdd.append( '' )
    Add = GraphLoopsCoupled_3.INode( 'Add' , PyAdd )
    Add.SetName( 'Add' )
    Add.SetAuthor( '' )
    Add.SetComment( 'Compute Node' )
    Add.Coords( 245 , 38 )
    IAddx = Add.InPort( 'x' , 'long' )
    IAddy = Add.InPort( 'y' , 'long' )
    IAddGate = Add.GetInPort( 'Gate' )
    OAddz = Add.OutPort( 'z' , 'long' )
    OAddGate = Add.GetOutPort( 'Gate' )
    
    PyAdd_1 = []
    PyAdd_1.append( 'def Add(x,y) :  ' )
    PyAdd_1.append( '	return x+y  ' )
    PyAdd_1.append( '' )
    Add_1 = GraphLoopsCoupled_3.INode( 'Add' , PyAdd_1 )
    Add_1.SetName( 'Add_1' )
    Add_1.SetAuthor( '' )
    Add_1.SetComment( 'Compute Node' )
    Add_1.Coords( 243 , 288 )
    IAdd_1x = Add_1.InPort( 'x' , 'long' )
    IAdd_1y = Add_1.InPort( 'y' , 'long' )
    IAdd_1Gate = Add_1.GetInPort( 'Gate' )
    OAdd_1z = Add_1.OutPort( 'z' , 'long' )
    OAdd_1Gate = Add_1.GetOutPort( 'Gate' )
    
    PySub = []
    PySub.append( 'def Sub(x,y) :  ' )
    PySub.append( '	return x-y  ' )
    PySub.append( '' )
    Sub = GraphLoopsCoupled_3.INode( 'Sub' , PySub )
    Sub.SetName( 'Sub' )
    Sub.SetAuthor( '' )
    Sub.SetComment( 'Compute Node' )
    Sub.Coords( 458 , 18 )
    ISubx = Sub.InPort( 'x' , 'long' )
    ISuby = Sub.InPort( 'y' , 'long' )
    ISubGate = Sub.GetInPort( 'Gate' )
    OSubz = Sub.OutPort( 'z' , 'long' )
    OSubGate = Sub.GetOutPort( 'Gate' )
    
    PySub_1 = []
    PySub_1.append( 'def Sub(x,y) :   ' )
    PySub_1.append( '	return x-y   ' )
    PySub_1.append( '' )
    Sub_1 = GraphLoopsCoupled_3.INode( 'Sub' , PySub_1 )
    Sub_1.SetName( 'Sub_1' )
    Sub_1.SetAuthor( '' )
    Sub_1.SetComment( 'Compute Node' )
    Sub_1.Coords( 461 , 268 )
    ISub_1x = Sub_1.InPort( 'x' , 'long' )
    ISub_1y = Sub_1.InPort( 'y' , 'long' )
    ISub_1Gate = Sub_1.GetInPort( 'Gate' )
    OSub_1z = Sub_1.OutPort( 'z' , 'long' )
    OSub_1Gate = Sub_1.GetOutPort( 'Gate' )
    
    # Creation of Loop Nodes
    PyLoop = []
    PyLoop.append( '  ' )
    PyMoreLoop = []
    PyMoreLoop.append( 'def More(Index,Max,z) :    ' )
    PyMoreLoop.append( '	DoLoop = 0    ' )
    PyMoreLoop.append( '	if Index < Max :   ' )
    PyMoreLoop.append( '		DoLoop = 1    ' )
    PyMoreLoop.append( '	return DoLoop,Index,Max,z ' )
    PyMoreLoop.append( '  ' )
    PyNextLoop = []
    PyNextLoop.append( 'def Next(Index,Max,z) :    ' )
    PyNextLoop.append( '	Index = Index + 1    ' )
    PyNextLoop.append( '	return Index,Max,z  ' )
    PyNextLoop.append( '  ' )
    Loop,EndLoop = GraphLoopsCoupled_3.LNode( '' , PyLoop , 'More' , PyMoreLoop , 'Next' , PyNextLoop )
    EndLoop.SetName( 'EndLoop' )
    EndLoop.SetAuthor( '' )
    EndLoop.SetComment( 'Compute Node' )
    EndLoop.Coords( 662 , 38 )
    PyEndLoop = []
    EndLoop.SetPyFunction( 'EndLoop' , PyEndLoop )
    ILoopDoLoop = Loop.GetInPort( 'DoLoop' )
    ILoopIndex = Loop.InPort( 'Index' , 'long' )
    ILoopMax = Loop.InPort( 'Max' , 'long' )
    ILoopz = Loop.InPort( 'z' , 'long' )
    ILoopGate = Loop.GetInPort( 'Gate' )
    OLoopDoLoop = Loop.GetOutPort( 'DoLoop' )
    OLoopIndex = Loop.GetOutPort( 'Index' )
    OLoopMax = Loop.GetOutPort( 'Max' )
    OLoopz = Loop.GetOutPort( 'z' )
    IEndLoopDoLoop = EndLoop.GetInPort( 'DoLoop' )
    IEndLoopIndex = EndLoop.GetInPort( 'Index' )
    IEndLoopMax = EndLoop.GetInPort( 'Max' )
    IEndLoopz = EndLoop.GetInPort( 'z' )
    IEndLoopGate = EndLoop.GetInPort( 'Gate' )
    OEndLoopDoLoop = EndLoop.GetOutPort( 'DoLoop' )
    OEndLoopIndex = EndLoop.GetOutPort( 'Index' )
    OEndLoopMax = EndLoop.GetOutPort( 'Max' )
    OEndLoopz = EndLoop.GetOutPort( 'z' )
    OEndLoopGate = EndLoop.GetOutPort( 'Gate' )
    Loop.SetName( 'Loop' )
    Loop.SetAuthor( '' )
    Loop.SetComment( 'Compute Node' )
    Loop.Coords( 15 , 58 )
    
    PyInit = []
    PyInit.append( '  ' )
    PyMoreInit = []
    PyMoreInit.append( 'def More(Index,Max,z) :      ' )
    PyMoreInit.append( '	DoLoop = 0      ' )
    PyMoreInit.append( '	if Index < Max :     ' )
    PyMoreInit.append( '		DoLoop = 1      ' )
    PyMoreInit.append( '	return DoLoop,Index,Max,z      ' )
    PyMoreInit.append( '  ' )
    PyNextInit = []
    PyNextInit.append( 'def Next(Index,Max,z) :     ' )
    PyNextInit.append( '	Index = Index + 1     ' )
    PyNextInit.append( '	return Index,Max,z     ' )
    PyNextInit.append( '  ' )
    Init,EndInit = GraphLoopsCoupled_3.LNode( '' , PyInit , 'More' , PyMoreInit , 'Next' , PyNextInit )
    EndInit.SetName( 'EndInit' )
    EndInit.SetAuthor( '' )
    EndInit.SetComment( 'Compute Node' )
    EndInit.Coords( 670 , 288 )
    PyEndInit = []
    EndInit.SetPyFunction( 'EndInit' , PyEndInit )
    IInitDoLoop = Init.GetInPort( 'DoLoop' )
    IInitIndex = Init.InPort( 'Index' , 'long' )
    IInitMax = Init.InPort( 'Max' , 'long' )
    IInitz = Init.InPort( 'z' , 'long' )
    IInitGate = Init.GetInPort( 'Gate' )
    OInitDoLoop = Init.GetOutPort( 'DoLoop' )
    OInitIndex = Init.GetOutPort( 'Index' )
    OInitMax = Init.GetOutPort( 'Max' )
    OInitz = Init.GetOutPort( 'z' )
    IEndInitDoLoop = EndInit.GetInPort( 'DoLoop' )
    IEndInitIndex = EndInit.GetInPort( 'Index' )
    IEndInitMax = EndInit.GetInPort( 'Max' )
    IEndInitz = EndInit.GetInPort( 'z' )
    IEndInitGate = EndInit.GetInPort( 'Gate' )
    OEndInitDoLoop = EndInit.GetOutPort( 'DoLoop' )
    OEndInitIndex = EndInit.GetOutPort( 'Index' )
    OEndInitMax = EndInit.GetOutPort( 'Max' )
    OEndInitz = EndInit.GetOutPort( 'z' )
    OEndInitGate = EndInit.GetOutPort( 'Gate' )
    Init.SetName( 'Init' )
    Init.SetAuthor( '' )
    Init.SetComment( 'Compute Node' )
    Init.Coords( 10 , 308 )
    
    # Creation of Links
    LAddzSuby = GraphLoopsCoupled_3.Link( OAddz , ISuby )
    
    LAddzSub_1x = GraphLoopsCoupled_3.Link( OAddz , ISub_1x )
    
    LAdd_1zSub_1y = GraphLoopsCoupled_3.Link( OAdd_1z , ISub_1y )
    
    LAdd_1zSubx = GraphLoopsCoupled_3.Link( OAdd_1z , ISubx )
    
    LSubzEndInitz = GraphLoopsCoupled_3.Link( OSubz , IEndInitz )
    
    LSub_1zEndLoopz = GraphLoopsCoupled_3.Link( OSub_1z , IEndLoopz )
    
    LLoopIndexEndLoopIndex = GraphLoopsCoupled_3.Link( OLoopIndex , IEndLoopIndex )
    
    LLoopMaxEndLoopMax = GraphLoopsCoupled_3.Link( OLoopMax , IEndLoopMax )
    
    LLoopMaxAddx = GraphLoopsCoupled_3.Link( OLoopMax , IAddx )
    
    LLoopzAddy = GraphLoopsCoupled_3.Link( OLoopz , IAddy )
    
    LInitIndexEndInitIndex = GraphLoopsCoupled_3.Link( OInitIndex , IEndInitIndex )
    
    LInitMaxEndInitMax = GraphLoopsCoupled_3.Link( OInitMax , IEndInitMax )
    
    LInitMaxAdd_1x = GraphLoopsCoupled_3.Link( OInitMax , IAdd_1x )
    
    LInitzAdd_1y = GraphLoopsCoupled_3.Link( OInitz , IAdd_1y )
    
    # Input datas
    ILoopIndex.Input( 5 )
    ILoopMax.Input( 15 )
    ILoopz.Input( -1 )
    IInitIndex.Input( 3 )
    IInitMax.Input( 13 )
    IInitz.Input( -2 )
    
    # Output Ports of the graph
    #OEndLoopIndex = EndLoop.GetOutPort( 'Index' )
    #OEndLoopMax = EndLoop.GetOutPort( 'Max' )
    #OEndLoopz = EndLoop.GetOutPort( 'z' )
    #OEndInitIndex = EndInit.GetOutPort( 'Index' )
    #OEndInitMax = EndInit.GetOutPort( 'Max' )
    #OEndInitz = EndInit.GetOutPort( 'z' )
    return GraphLoopsCoupled_3


GraphLoopsCoupled_3 = DefGraphLoopsCoupled_3()
