#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
from SuperV import *

anXmlFile = os.getenv('DATA_DIR') + '/Superv/Graphs/GraphEssai.xml'
GraphMacroNodes = Graph( anXmlFile )

GraphMacroNodes.PrintLinks()

GraphMacroNodes.SetName('GraphMacroNodes')

anXmlFile = os.getenv('DATA_DIR') + '/Superv/Graphs/SyrStruct.xml'
Macro_SyrStruct = GraphMacroNodes.MNode( anXmlFile )

Macro_SyrStruct.PrintPorts()

GraphMacroNodes.PrintPorts()

GraphMacroNodes.PrintLinks()

GraphMacroNodes.Print()

Macro_SyrStruct.Print()

exec GraphMacroNodes.ListNodes('GraphMacroNodes')

Macro_SyrStruct.Print()

Macro_SyrStruct.IsMacro()
Macro_SyrStruct.IsFlowMacro()
Macro_SyrStruct.IsStreamMacro()

SyrStruct = Macro_SyrStruct.FlowObjRef()

SyrStruct.Print()

SyrStruct.PrintPorts()

SyrStruct.PrintLinks()

SyrStruct.IsValid()
SyrStruct.IsExecutable()

exec SyrStruct.ListNodes('SyrStruct')

m3incr.Print()

m3incr.PrintPorts()

m3incr.PrintLinks()

EndOffori.Print()

EndOffori.PrintPorts()

EndOffori.PrintLinks()

EndOfwhileEven.Print()

EndOfwhileEven.PrintPorts()

EndOfwhileEven.PrintLinks()

MSyrStruct = SyrStruct.FlowObjRef()

MSyrStruct.Print()

MSyrStruct.PrintPorts()

GraphMacroNodes.IsValid()
GraphMacroNodes.IsExecutable()

GraphMacroNodes.PrintPorts()

anXmlFile = os.getenv('DATA_DIR') + '/Superv/Graphs/SyrStruct.xml'
Macro_SyrStruct_1 = GraphMacroNodes.MNode( anXmlFile )

Macro_SyrStruct_1.Print()

Macro_SyrStruct_1.IsMacro()
Macro_SyrStruct_1.IsFlowMacro()
Macro_SyrStruct_1.IsStreamMacro()

SyrStruct_1 = Macro_SyrStruct_1.FlowObjRef()

SyrStruct_1.Print()

GraphMacroNodes.IsValid()
GraphMacroNodes.IsExecutable()

GraphMacroNodes.PrintPorts()

MSyrStruct_1 = SyrStruct_1.FlowObjRef()

MSyrStruct_1.Print()

MSyrStruct_1.PrintPorts()


anXmlFile = os.getenv('DATA_DIR') + '/Superv/Graphs/GraphMacroNodes.xml'
GraphMacroNodes.Export( anXmlFile )

SyrStruct.Name()
SyrStruct_1.Name()

GraphMacroNodes.Run()

GraphMacroNodes.DoneW()

GraphMacroNodes.State()

Macro_SyrStruct.State()

Macro_SyrStruct_1.State()





from SuperV import *

anXmlFile = os.getenv('DATA_DIR') + '/Superv/Graphs/GraphMacroNodes.xml'
GraphMacroNodes = Graph( anXmlFile )

GraphMacroNodes.Name()

GraphMacroNodes.Export( '/tmp/GraphMacroNodes.xml' )

exec GraphMacroNodes.ListNodes('GraphMacroNodes')

GraphMacroNodes.PrintPorts()

GraphMacroNodes.PrintLinks()

Macro_SyrStruct.IsMacro()
Macro_SyrStruct.IsFlowMacro()
Macro_SyrStruct.IsStreamMacro()

Macro_SyrStruct.Print()

SyrStruct = Macro_SyrStruct.FlowObjRef()

SyrStruct.Print()

SyrStruct.PrintPorts()

SyrStruct.PrintLinks()

exec SyrStruct.ListNodes('SyrStruct')

Macro_SyrStruct.IsMacro()
Macro_SyrStruct.IsFlowMacro()
Macro_SyrStruct.IsStreamMacro()

Macro_SyrStruct.IsValid()

Macro_SyrStruct.IsExecutable()

Macro_SyrStruct_1.Print()

SyrStruct_1 = Macro_SyrStruct_1.FlowObjRef()

SyrStruct_1.Print()

SyrStruct_1.PrintPorts()

SyrStruct_1.PrintLinks()

exec SyrStruct_1.ListNodes('SyrStruct_1')

MSyrStruct = SyrStruct.FlowObjRef()

MSyrStruct.Print()

MSyrStruct_1 = SyrStruct_1.FlowObjRef()

MSyrStruct_1.Print()

GraphMacroNodes.Run()

GraphMacroNodes.DoneW()

GraphMacroNodes.State()

Macro_SyrStruct.State()

Macro_SyrStruct_1.State()




from SuperV import *

from GraphMacroNodes import *

GraphMacroNodes.IsExecutable()

GraphMacroNodes.Run()

exec GraphMacroNodes.ListNodes('GraphMacroNodes')

SyrStruct = Macro_SyrStruct.FlowObjRef()

SyrStruct.IsExecutable()

GraphMacroNodes.DoneW()

GraphMacroNodes.State()

Macro_SyrStruct.State()

Macro_SyrStruct_1.State()

SyrStruct = Macro_SyrStruct.FlowObjRef()

exec SyrStruct.ListNodes('SyrStruct')

EndOfforN.State()

SyrStruct.DoneW()

SyrStruct.State()

SyrStruct_1 = Macro_SyrStruct_1.FlowObjRef()

exec SyrStruct_1.ListNodes('SyrStruct')

EndOfforN.State()

SyrStruct_1.DoneW()

SyrStruct_1.State()

SyrStruct.PrintPorts()

Macro_SyrStruct.PrintPorts()

GraphMacroNodes.PrintPorts()

GraphMacroNodes.Run()

GraphMacroNodes.DoneW()

GraphMacroNodes.State()

Macro_SyrStruct.State()

Macro_SyrStruct_1.State()




SyrStruct.Export('/tmp/SubSyrStruct.xml')


SubSyrStruct = Graph('/tmp/SubSyrStruct.xml')

SubSyrStruct.IsValid()

SubSyrStruct.IsExecutable()

SubSyrStruct.Run()

SubSyrStruct.DoneW()

SubSyrStruct.State()

SubSyrStruct.PrintPorts()





from SuperV import *

from GraphMacroNodes import *

exec GraphMacroNodes.ListNodes('GraphMacroNodes')

SyrStruct = Macro_SyrStruct.FlowObjRef()

exec SyrStruct.ListNodes('SyrStruct')

Unused = SyrComponent.InPort('Unused','long')

SyrComponent.PrintPorts()

SyrComponent.Print()

SyrStruct.IsValid()

SyrStruct.PrintPorts()

Macro_SyrStruct.PrintPorts()

GraphMacroNodes.PrintPorts()

Unused.Destroy()

GraphMacroNodes.PrintPorts()

Macro_SyrStruct.PrintPorts()

SyrStruct.PrintPorts()

GraphMacroNodes.Run()

GraphMacroNodes.DoneW()

GraphMacroNodes.State()

Macro_SyrStruct.State()

Macro_SyrStruct_1.State()
