#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph GraphGOTOMemory
#
from SuperV import *

# Graph creation of GraphGOTOMemory
def DefGraphGOTOMemory() :
    GraphGOTOMemory = Graph( 'GraphGOTOMemory' )
    GraphGOTOMemory.SetName( 'GraphGOTOMemory' )
    GraphGOTOMemory.SetAuthor( 'JR' )
    GraphGOTOMemory.SetComment( 'To study memory leaks' )
    GraphGOTOMemory.Coords( 0 , 0 )
    
    # Creation of Factory Nodes
    
    # Creation of InLine Nodes
    PyInLine = []
    InLine = GraphGOTOMemory.INode( '' , PyInLine )
    InLine.SetName( 'InLine' )
    InLine.SetAuthor( '' )
    InLine.SetComment( 'Compute Node' )
    InLine.Coords( 74 , 178 )
    IInLineGate = InLine.GetInPort( 'Gate' )
    OInLineGate = InLine.GetOutPort( 'Gate' )
    
    # Creation of GOTO Nodes
    PyGoTo = []
    GoTo = GraphGOTOMemory.GNode( '' , PyGoTo , 'InLine' )
    GoTo.SetName( 'GoTo' )
    GoTo.SetAuthor( '' )
    GoTo.SetComment( 'Compute Node' )
    GoTo.Coords( 533 , 218 )
    IGoToGate = GoTo.GetInPort( 'Gate' )
    OGoToGate = GoTo.GetOutPort( 'Gate' )
    
    # Creation of Links
    LInLineGateGoToGate = GraphGOTOMemory.Link( OInLineGate , IGoToGate )
    
    LGoToGateInLineGate = GraphGOTOMemory.Link( OGoToGate , IInLineGate )
    LGoToGateInLineGate.AddCoord( 1 , 69 , 420 )
    LGoToGateInLineGate.AddCoord( 2 , 700 , 420 )
    
    # Output Ports of the graph
    return GraphGOTOMemory


GraphGOTOMemory = DefGraphGOTOMemory()

GraphGOTOMemory.Run()
GraphGOTOMemory.DoneW()
GraphGOTOMemory.State()
GraphGOTOMemory.PrintPorts()
