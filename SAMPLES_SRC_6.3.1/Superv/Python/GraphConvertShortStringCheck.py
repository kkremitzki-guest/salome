#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph GraphConvertShortStringCheck
#
from SuperV import *
# Graph creation 
GraphConvertShortStringCheck = Graph( 'GraphConvertShortStringCheck' )
GraphConvertShortStringCheck.SetName( 'GraphConvertShortStringCheck' )
GraphConvertShortStringCheck.SetAuthor( 'JR' )
GraphConvertShortStringCheck.SetComment( 'Check conversions of String' )
GraphConvertShortStringCheck.Coords( 0 , 0 )

# Creation of Factory Nodes

MiscTypes = GraphConvertShortStringCheck.FNode( 'TypesCheck' , 'TypesCheck' , 'MiscTypes' )
MiscTypes.SetName( 'MiscTypes' )
MiscTypes.SetAuthor( '' )
MiscTypes.SetContainer( 'localhost/FactoryServer' )
MiscTypes.SetComment( 'MiscTypes from TypesCheck' )
MiscTypes.Coords( 284 , 28 )

# Creation of InLine Nodes
PyShortString = []
PyShortString.append( 'def ShortString() :   ' )
PyShortString.append( '    string = "32767"   ' )
PyShortString.append( '    return string   ' )
PyShortString.append( ' ' )
ShortString = GraphConvertShortStringCheck.INode( 'ShortString' , PyShortString )
ShortString.OutPort( 'OutString' , 'string' )
ShortString.SetName( 'ShortString' )
ShortString.SetAuthor( 'JR' )
ShortString.SetComment( 'InLine Node' )
ShortString.Coords( 14 , 114 )

# Creation of Links
ShortStringOutString = ShortString.Port( 'OutString' )
MiscTypesInString = GraphConvertShortStringCheck.Link( ShortStringOutString , MiscTypes.Port( 'InString' ) )

MiscTypesInBool = GraphConvertShortStringCheck.Link( ShortStringOutString , MiscTypes.Port( 'InBool' ) )

MiscTypesInChar = GraphConvertShortStringCheck.Link( ShortStringOutString , MiscTypes.Port( 'InChar' ) )

MiscTypesInShort = GraphConvertShortStringCheck.Link( ShortStringOutString , MiscTypes.Port( 'InShort' ) )

MiscTypesInLong = GraphConvertShortStringCheck.Link( ShortStringOutString , MiscTypes.Port( 'InLong' ) )

MiscTypesInFloat = GraphConvertShortStringCheck.Link( ShortStringOutString , MiscTypes.Port( 'InFloat' ) )

MiscTypesInDouble = GraphConvertShortStringCheck.Link( ShortStringOutString , MiscTypes.Port( 'InDouble' ) )

MiscTypesInObjRef = GraphConvertShortStringCheck.Link( ShortStringOutString , MiscTypes.Port( 'InObjRef' ) )

# Creation of Output variables
MiscTypesOutString = MiscTypes.Port( 'OutString' )
MiscTypesOutBool = MiscTypes.Port( 'OutBool' )
MiscTypesOutChar = MiscTypes.Port( 'OutChar' )
MiscTypesOutShort = MiscTypes.Port( 'OutShort' )
MiscTypesOutLong = MiscTypes.Port( 'OutLong' )
MiscTypesOutFloat = MiscTypes.Port( 'OutFloat' )
MiscTypesOutDouble = MiscTypes.Port( 'OutDouble' )
MiscTypesOutObjRef = MiscTypes.Port( 'OutObjRef' )

GraphConvertShortStringCheck.Run()
GraphConvertShortStringCheck.DoneW()
GraphConvertShortStringCheck.PrintPorts()
