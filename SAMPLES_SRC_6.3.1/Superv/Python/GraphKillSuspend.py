#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
from GraphEssai import *
import time

exec GraphEssai.ListNodes('GraphEssai')

GraphEssai.Run()
time.sleep(10)
GraphEssai.Kill()
print 'Add :',Add.State()
print 'Sub :',Sub.State()
print 'Mul :',Mul.State()
print 'Div :',Div.State()
print 'GraphEssai :',GraphEssai.State()


GraphEssai.Run()
time.sleep(5)
GraphEssai.Suspend()
print 'Add :',Add.State()
print 'Sub :',Sub.State()
print 'Mul :',Mul.State()
print 'Div :',Div.State()
print 'GraphEssai :',GraphEssai.State()
GraphEssai.Resume()
time.sleep(5)
print 'Add :',Add.State()
print 'Sub :',Sub.State()
print 'Mul :',Mul.State()
print 'Div :',Div.State()
print 'GraphEssai :',GraphEssai.State()

GraphEssai.DoneW()
GraphEssai.PrintPorts()
time.sleep(1)
print 'Add :',Add.State()
print 'Sub :',Sub.State()
print 'Mul :',Mul.State()
print 'Div :',Div.State()
print 'GraphEssai :',GraphEssai.State()

