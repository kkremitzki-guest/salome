#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph SIGFPEPython
#
from SuperV import *

# Graph creation of SIGFPEPython
def DefSIGFPEPython() :
    SIGFPEPython = Graph( 'SIGFPEPython' )
    SIGFPEPython.SetName( 'SIGFPEPython' )
    SIGFPEPython.SetAuthor( 'JR' )
    SIGFPEPython.SetComment( '' )
    SIGFPEPython.Coords( 0 , 0 )
    
    # Creation of Factory Nodes
    
    # Creation of InLine Nodes
    PySIGFPEPython = []
    PySIGFPEPython.append( 'def SIGFPEPython() :   ' )
    PySIGFPEPython.append( '	import libSIGNALSComponent_Swig  ' )
    PySIGFPEPython.append( '	s = libSIGNALSComponent_Swig.SIGNALSComponentEngine()     ' )
    PySIGFPEPython.append( '	s.SIGFPEfunc(1,0)     ' )
    PySIGFPEPython.append( '  ' )
    SIGFPEPython = SIGFPEPython.INode( 'SIGFPEPython' , PySIGFPEPython )
    SIGFPEPython.SetName( 'SIGFPEPython' )
    SIGFPEPython.SetAuthor( '' )
    SIGFPEPython.SetComment( 'Compute Node' )
    SIGFPEPython.Coords( 118 , 159 )
    ISIGFPEPythonGate = SIGFPEPython.GetInPort( 'Gate' )
    OSIGFPEPythonGate = SIGFPEPython.GetOutPort( 'Gate' )
    
    # Output Ports of the graph
    return SIGFPEPython


SIGFPEPython = DefSIGFPEPython()
