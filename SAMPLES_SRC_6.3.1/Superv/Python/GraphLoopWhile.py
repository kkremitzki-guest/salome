#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph GraphLoopWhile
#
from SuperV import *

# Graph creation of GraphLoopWhile
def DefGraphLoopWhile() :
    GraphLoopWhile = Graph( 'GraphLoopWhile' )
    GraphLoopWhile.SetName( 'GraphLoopWhile' )
    GraphLoopWhile.SetAuthor( 'JR' )
    GraphLoopWhile.SetComment( '' )
    GraphLoopWhile.Coords( 0 , 0 )
    
    # Creation of Factory Nodes
    
    # Creation of Loop Nodes
    PyLoop = []
    PyMoreLoop = []
    PyMoreLoop.append( 'import time ' )
    PyMoreLoop.append( 'def LoopWhileMore(Index,Max) :  ' )
    PyMoreLoop.append( '	time.sleep(2) ' )
    PyMoreLoop.append( '	if Index < Max :  ' )
    PyMoreLoop.append( '		DoLoop = 1  ' )
    PyMoreLoop.append( '	else :  ' )
    PyMoreLoop.append( '		DoLoop = 0  ' )
    PyMoreLoop.append( '	return DoLoop,Index,Max  ' )
    PyMoreLoop.append( '' )
    PyNextLoop = []
    PyNextLoop.append( 'def LoopWhileNext(Index,Max) :  ' )
    PyNextLoop.append( '	Index = Index + 1  ' )
    PyNextLoop.append( '	return Index,Max  ' )
    PyNextLoop.append( '' )
    Loop,EndLoop = GraphLoopWhile.LNode( '' , PyLoop , 'LoopWhileMore' , PyMoreLoop , 'LoopWhileNext' , PyNextLoop )
    EndLoop.SetName( 'EndLoop' )
    EndLoop.SetAuthor( '' )
    EndLoop.SetComment( 'Compute Node' )
    EndLoop.Coords( 414 , 232 )
    PyEndLoop = []
    EndLoop.SetPyFunction( 'EndLoop' , PyEndLoop )
    ILoopDoLoop = Loop.GetInPort( 'DoLoop' )
    ILoopIndex = Loop.InPort( 'Index' , 'long' )
    ILoopMax = Loop.InPort( 'Max' , 'long' )
    ILoopGate = Loop.GetInPort( 'Gate' )
    OLoopDoLoop = Loop.GetOutPort( 'DoLoop' )
    OLoopIndex = Loop.GetOutPort( 'Index' )
    OLoopMax = Loop.GetOutPort( 'Max' )
    IEndLoopDoLoop = EndLoop.GetInPort( 'DoLoop' )
    IEndLoopIndex = EndLoop.GetInPort( 'Index' )
    IEndLoopMax = EndLoop.GetInPort( 'Max' )
    IEndLoopGate = EndLoop.GetInPort( 'Gate' )
    OEndLoopDoLoop = EndLoop.GetOutPort( 'DoLoop' )
    OEndLoopIndex = EndLoop.GetOutPort( 'Index' )
    OEndLoopMax = EndLoop.GetOutPort( 'Max' )
    Loop.SetName( 'Loop' )
    Loop.SetAuthor( '' )
    Loop.SetComment( 'Compute Node' )
    Loop.Coords( 105 , 226 )
    
    # Creation of Links
    LLoopIndexEndLoopIndex = GraphLoopWhile.Link( OLoopIndex , IEndLoopIndex )
    
    LLoopMaxEndLoopMax = GraphLoopWhile.Link( OLoopMax , IEndLoopMax )
    
    # Input datas
    ILoopIndex.Input( 5 )
    ILoopMax.Input( 10 )
    
    # Output Ports of the graph
    #OEndLoopIndex = EndLoop.GetOutPort( 'Index' )
    #OEndLoopMax = EndLoop.GetOutPort( 'Max' )
    return GraphLoopWhile


GraphLoopWhile = DefGraphLoopWhile()
