#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph aNewDataFlow_2
#
from SuperV import *

# Graph creation of aNewDataFlow_2
def DefaNewDataFlow_2() :
    aNewDataFlow_2 = Graph( 'aNewDataFlow_2' )
    aNewDataFlow_2.SetName( 'aNewDataFlow_2' )
    aNewDataFlow_2.SetAuthor( '' )
    aNewDataFlow_2.SetComment( '' )
    aNewDataFlow_2.Coords( 0 , 0 )
    
    # Creation of Factory Nodes
    
    # Creation of InLine Nodes
    PyErrorNode = []
    PyErrorNode.append( 'def ErrorNode(a,b) ' )
    PyErrorNode.append( '  return a&b ' )
    ErrorNode = aNewDataFlow_2.INode( 'ErrorNode' , PyErrorNode )
    ErrorNode.SetName( 'ErrorNode' )
    ErrorNode.SetAuthor( '' )
    ErrorNode.SetComment( 'Compute Node' )
    ErrorNode.Coords( 0 , 0 )
    IErrorNodea = ErrorNode.InPort( 'a' , 'boolean' )
    IErrorNodeb = ErrorNode.InPort( 'b' , 'boolean' )
    IErrorNodeGate = ErrorNode.GetInPort( 'Gate' )
    OErrorNodec = ErrorNode.OutPort( 'c' , 'boolean' )
    OErrorNodeGate = ErrorNode.GetOutPort( 'Gate' )
    
    # Input datas
    IErrorNodea.Input( 1 )
    IErrorNodeb.Input( 0 )
    
    # Output Ports of the graph
    #OErrorNodec = ErrorNode.GetOutPort( 'c' )
    return aNewDataFlow_2


aNewDataFlow_2 = DefaNewDataFlow_2()
