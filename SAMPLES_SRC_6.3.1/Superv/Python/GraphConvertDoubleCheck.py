#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph GraphConvertDoubleCheck
#
from SuperV import *
# Graph creation 
GraphConvertDoubleCheck = Graph( 'GraphConvertDoubleCheck' )
GraphConvertDoubleCheck.SetName( 'GraphConvertDoubleCheck' )
GraphConvertDoubleCheck.SetAuthor( 'JR' )
GraphConvertDoubleCheck.SetComment( 'Check conversions of Double' )
GraphConvertDoubleCheck.Coords( 0 , 0 )

# Creation of Factory Nodes

MiscTypes = GraphConvertDoubleCheck.FNode( 'TypesCheck' , 'TypesCheck' , 'MiscTypes' )
MiscTypes.SetName( 'MiscTypes' )
MiscTypes.SetAuthor( '' )
MiscTypes.SetContainer( 'localhost/FactoryServer' )
MiscTypes.SetComment( 'MiscTypes from TypesCheck' )
MiscTypes.Coords( 217 , 0 )

MiscTypes_1 = GraphConvertDoubleCheck.FNode( 'TypesCheck' , 'TypesCheck' , 'MiscTypes' )
MiscTypes_1.SetName( 'MiscTypes_1' )
MiscTypes_1.SetAuthor( '' )
MiscTypes_1.SetContainer( 'localhost/FactoryServer' )
MiscTypes_1.SetComment( 'MiscTypes from TypesCheck' )
MiscTypes_1.Coords( 433 , 131 )

# Creation of InLine Nodes
PyDouble = []
PyDouble.append( 'def Double() :     ' )
PyDouble.append( '    aDouble = 3.1415926535  ' )
PyDouble.append( '    print type(aDouble),"aDouble",aDouble,"NO double in python !!! Use Strings ?"  ' )
PyDouble.append( '    return aDouble     ' )
PyDouble.append( '' )
Double = GraphConvertDoubleCheck.INode( 'Double' , PyDouble )
Double.OutPort( 'OutDouble' , 'double' )
Double.SetName( 'Double' )
Double.SetAuthor( 'JR' )
Double.SetComment( 'InLine Node' )
Double.Coords( 9 , 87 )

PyDoubleString = []
PyDoubleString.append( 'def DoubleString() :       ' )
PyDoubleString.append( '    aDouble = 3.1415926535    ' )
PyDoubleString.append( '    print type(aDouble),"aDouble",aDouble,"NO double in python !!! Use Strings ?"    ' )
PyDoubleString.append( '    aDoubleString = str( aDouble )  ' )
PyDoubleString.append( '    print type(aDoubleString),"aDoubleString",aDoubleString  ' )
PyDoubleString.append( '    return aDoubleString  ' )
PyDoubleString.append( ' ' )
DoubleString = GraphConvertDoubleCheck.INode( 'DoubleString' , PyDoubleString )
DoubleString.OutPort( 'OutDoubleString' , 'string' )
DoubleString.SetName( 'DoubleString' )
DoubleString.SetAuthor( 'JR' )
DoubleString.SetComment( 'InLine Node' )
DoubleString.Coords( 217 , 334 )

# Creation of Links
DoubleOutDouble = Double.Port( 'OutDouble' )
MiscTypesInString = GraphConvertDoubleCheck.Link( DoubleOutDouble , MiscTypes.Port( 'InString' ) )

MiscTypesInBool = GraphConvertDoubleCheck.Link( DoubleOutDouble , MiscTypes.Port( 'InBool' ) )

MiscTypesInChar = GraphConvertDoubleCheck.Link( DoubleOutDouble , MiscTypes.Port( 'InChar' ) )

MiscTypesInShort = GraphConvertDoubleCheck.Link( DoubleOutDouble , MiscTypes.Port( 'InShort' ) )

MiscTypesInLong = GraphConvertDoubleCheck.Link( DoubleOutDouble , MiscTypes.Port( 'InLong' ) )

MiscTypesInFloat = GraphConvertDoubleCheck.Link( DoubleOutDouble , MiscTypes.Port( 'InFloat' ) )

MiscTypesInDouble = GraphConvertDoubleCheck.Link( DoubleOutDouble , MiscTypes.Port( 'InDouble' ) )

MiscTypesInObjRef = GraphConvertDoubleCheck.Link( DoubleOutDouble , MiscTypes.Port( 'InObjRef' ) )

DoubleStringOutDoubleString = DoubleString.Port( 'OutDoubleString' )
MiscTypes_1InString = GraphConvertDoubleCheck.Link( DoubleStringOutDoubleString , MiscTypes_1.Port( 'InString' ) )

MiscTypes_1InBool = GraphConvertDoubleCheck.Link( DoubleStringOutDoubleString , MiscTypes_1.Port( 'InBool' ) )

MiscTypes_1InChar = GraphConvertDoubleCheck.Link( DoubleStringOutDoubleString , MiscTypes_1.Port( 'InChar' ) )

MiscTypes_1InShort = GraphConvertDoubleCheck.Link( DoubleStringOutDoubleString , MiscTypes_1.Port( 'InShort' ) )

MiscTypes_1InLong = GraphConvertDoubleCheck.Link( DoubleStringOutDoubleString , MiscTypes_1.Port( 'InLong' ) )

MiscTypes_1InFloat = GraphConvertDoubleCheck.Link( DoubleStringOutDoubleString , MiscTypes_1.Port( 'InFloat' ) )

MiscTypes_1InDouble = GraphConvertDoubleCheck.Link( DoubleStringOutDoubleString , MiscTypes_1.Port( 'InDouble' ) )

MiscTypes_1InObjRef = GraphConvertDoubleCheck.Link( DoubleStringOutDoubleString , MiscTypes_1.Port( 'InObjRef' ) )

# Creation of Output variables
MiscTypesOutString = MiscTypes.Port( 'OutString' )
MiscTypesOutBool = MiscTypes.Port( 'OutBool' )
MiscTypesOutChar = MiscTypes.Port( 'OutChar' )
MiscTypesOutShort = MiscTypes.Port( 'OutShort' )
MiscTypesOutLong = MiscTypes.Port( 'OutLong' )
MiscTypesOutFloat = MiscTypes.Port( 'OutFloat' )
MiscTypesOutDouble = MiscTypes.Port( 'OutDouble' )
MiscTypesOutObjRef = MiscTypes.Port( 'OutObjRef' )
MiscTypes_1OutString = MiscTypes_1.Port( 'OutString' )
MiscTypes_1OutBool = MiscTypes_1.Port( 'OutBool' )
MiscTypes_1OutChar = MiscTypes_1.Port( 'OutChar' )
MiscTypes_1OutShort = MiscTypes_1.Port( 'OutShort' )
MiscTypes_1OutLong = MiscTypes_1.Port( 'OutLong' )
MiscTypes_1OutFloat = MiscTypes_1.Port( 'OutFloat' )
MiscTypes_1OutDouble = MiscTypes_1.Port( 'OutDouble' )
MiscTypes_1OutObjRef = MiscTypes_1.Port( 'OutObjRef' )

GraphConvertDoubleCheck.Run()
GraphConvertDoubleCheck.DoneW()
GraphConvertDoubleCheck.PrintPorts()
