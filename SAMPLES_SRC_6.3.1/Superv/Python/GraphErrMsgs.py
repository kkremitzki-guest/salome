#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
from SuperV import *

import sys

GraphErrMsgs = Graph( "GraphErrMsgs" )

anXmlFile = os.getenv('DATA_DIR') + '/Superv/Graphs/SyrStruct.xml'

GraphErrMsgs.Import( anXmlFile )

exec GraphErrMsgs.ListNodes( 'GraphErrMsgs' )
# ==> m3,m3incr,incra,div2,incrb,SyrComponent,forN,whileNotOne,fori,whileEven,EndOfforN,EndOfwhileNotOne,EndOffori,EndOfwhileEven,ifNotEven,EndOfifNotEven

exec ifNotEven.ListPorts()
# ==> IifNotEvenSyrComponent,IifNotEvenN,IifNotEvenK,IifNotEvenGate,OifNotEvenOdd,OifNotEvenEven,OifNotEvenSyrComponent,OifNotEvenN,OifNotEvenK,OifNotEvenDefault

exec EndOfifNotEven.ListPorts()
# ==> IEndOfifNotEvenN,IEndOfifNotEvenK,IEndOfifNotEvenDefault,OEndOfifNotEvenN,OEndOfifNotEvenK,OEndOfifNotEvenGate

aLink = GraphErrMsgs.Link( OifNotEvenEven , IEndOfifNotEvenN )
if aLink != None :
    print "Error link from OifNotEvenEven to IEndOfifNotEvenN was created"
    print "GraphErrMsgs test failed"
    sys.exit(1)

exec EndOfwhileEven.ListPorts()
# ==> IEndOfwhileEvenSyrComponent,IEndOfwhileEvenN,IEndOfwhileEvenK,IEndOfwhileEvenGate,OEndOfwhileEvenSyrComponent,OEndOfwhileEvenN,OEndOfwhileEvenK,OEndOfwhileEvenGate

exec EndOffori.ListPorts()
# ==> IEndOfforii,IEndOfforiK,IEndOfforiGate,OEndOfforii,OEndOfforiK,OEndOfforiGate

aLink = GraphErrMsgs.Link( OEndOfwhileEvenK , IEndOfifNotEvenK )
aLink.destroy()

aLink = GraphErrMsgs.Link( OEndOfforiK , IEndOfifNotEvenK )
aLink.destroy()

aLink = GraphErrMsgs.Link( OifNotEvenN , IEndOfifNotEvenK )
if aLink == None :
    print "Error link from OifNotEvenN to IEndOfifNotEvenK was not created"
    print "GraphErrMsgs test failed"
    sys.exit(1)

exec whileEven.ListPorts()
# ==>IwhileEvenSyrComponent,IwhileEvenN,IwhileEvenK,IwhileEvenGate,OwhileEvenSyrComponent,OwhileEvenN,OwhileEvenK,OwhileEvenGate

aLink = GraphErrMsgs.Link( OifNotEvenK , IwhileEvenK )
aLink.destroy()

aLink = GraphErrMsgs.Link( OifNotEvenOdd , IwhileEvenK )
if aLink != None :
    print "Error link from OifNotEvenOdd to IwhileEvenK was created"
    print "GraphErrMsgs test failed"
    sys.exit(1)

aLink = GraphErrMsgs.Link( OifNotEvenDefault , IwhileEvenK )
if aLink != None :
    print "Error link from OifNotEvenDefault to IwhileEvenK was created"
    print "GraphErrMsgs test failed"
    sys.exit(1)

exec div2.ListPorts()
# ==> Idiv2anEvenInteger,Idiv2Gate,Odiv2anInteger,Odiv2Gate

aLink = GraphErrMsgs.Link( OwhileEvenN , Idiv2Gate )
if aLink != None :
    print "Error link from OwhileEvenN to Idiv2Gate was created"
    print "GraphErrMsgs test failed"
    sys.exit(1)

print "GraphErrMsgs Errors Messages :"
print GraphErrMsgs.Messages()

RunOk = GraphErrMsgs.Run()
if RunOk != 0 :
    print "Error : Success call to Run"
    print "GraphErrMsgs test failed"
    sys.exit(1)

GraphErrMsgs.DoneW()

print "GraphErrMsgs Errors Messages :"
print GraphErrMsgs.Messages()


from SuperV import *

# GraphBadInputInLoop
GraphBadInputInLoop = Graph( "GraphBadInputInLoop" )

anXmlFile = os.getenv('DATA_DIR') + '/Superv/Graphs/GraphBadInputInLoop.xml'

GraphBadInputInLoop.Import( anXmlFile )

RunOk = GraphBadInputInLoop.Run()
if RunOk != 0 :
    print "Error : Success call to Run"
    print "GraphBadInputInLoop test failed"
    sys.exit(1)

GraphBadInputInLoop.DoneW()

print "GraphBadInputInLoop Errors Messages :"
print GraphBadInputInLoop.Messages()



from SuperV import *

# GraphBadInputInLoop1
GraphBadInputInLoop1 = Graph( "GraphBadInputInLoop1" )

anXmlFile = os.getenv('DATA_DIR') + '/Superv/Graphs/GraphBadInputInLoop1.xml'

GraphBadInputInLoop1.Import( anXmlFile )

RunOk = GraphBadInputInLoop1.Run()
if RunOk != 0 :
    print "Error : Success call to Run"
    print "GraphBadInputInLoop1 test failed"
    sys.exit(1)

GraphBadInputInLoop1.DoneW()

print "GraphBadInputInLoop1 Errors Messages :"
print GraphBadInputInLoop1.Messages()



from SuperV import *

# GraphBadInputInLoop2
GraphBadInputInLoop2 = Graph( "GraphBadInputInLoop2" )

anXmlFile = os.getenv('DATA_DIR') + '/Superv/Graphs/GraphBadInputInLoop2.xml'

GraphBadInputInLoop2.Import( anXmlFile )

RunOk = GraphBadInputInLoop2.Run()
if RunOk != 0 :
    print "Error : Success call to Run"
    print "GraphBadInputInLoop2 test failed"
    sys.exit(1)

GraphBadInputInLoop2.DoneW()

print "GraphBadInputInLoop2 Errors Messages :"
print GraphBadInputInLoop2.Messages()



from SuperV import *

# GraphBadInputInLoop3
GraphBadInputInLoop3 = Graph( "GraphBadInputInLoop3" )

anXmlFile = os.getenv('DATA_DIR') + '/Superv/Graphs/GraphBadInputInLoop3.xml'

GraphBadInputInLoop3.Import( anXmlFile )

RunOk = GraphBadInputInLoop3.Run()
if RunOk != 0 :
    print "Error : Success call to Run"
    print "GraphBadInputInLoop3 test failed"
    sys.exit(1)

GraphBadInputInLoop3.DoneW()

print "GraphBadInputInLoop3 Errors Messages :"
print GraphBadInputInLoop3.Messages()



from SuperV import *

# GraphBadOutputOfLoop
GraphBadOutputOfLoop = Graph( "GraphBadOutputOfLoop" )

anXmlFile = os.getenv('DATA_DIR') + '/Superv/Graphs/GraphBadOutputOfLoop.xml'

GraphBadOutputOfLoop.Import( anXmlFile )

RunOk = GraphBadOutputOfLoop.Run()
if RunOk != 0 :
    print "Error : Success call to Run"
    print "GraphBadOutputOfLoop test failed"
    sys.exit(1)

GraphBadOutputOfLoop.DoneW()

print "GraphBadOutputOfLoop Errors Messages :"
print GraphBadOutputOfLoop.Messages()



from SuperV import *

# GraphBadOutputOfLoop1
GraphBadOutputOfLoop1 = Graph( "GraphBadOutputOfLoop1" )

anXmlFile = os.getenv('DATA_DIR') + '/Superv/Graphs/GraphBadOutputOfLoop1.xml'

GraphBadOutputOfLoop1.Import( anXmlFile )

RunOk = GraphBadOutputOfLoop1.Run()
if RunOk != 0 :
    print "Error : Success call to Run"
    print "GraphBadOutputOfLoop1 test failed"
    sys.exit(1)

GraphBadOutputOfLoop1.DoneW()

print "GraphBadOutputOfLoop1 Errors Messages :"
print GraphBadOutputOfLoop1.Messages()



from SuperV import *

# GraphBadOutputOfLoop2
GraphBadOutputOfLoop2 = Graph( "GraphBadOutputOfLoop2" )

anXmlFile = os.getenv('DATA_DIR') + '/Superv/Graphs/GraphBadOutputOfLoop2.xml'

GraphBadOutputOfLoop2.Import( anXmlFile )

RunOk = GraphBadOutputOfLoop2.Run()
if RunOk != 0 :
    print "Error : Success call to Run"
    print "GraphBadOutputOfLoop2 test failed"
    sys.exit(1)

GraphBadOutputOfLoop2.DoneW()

print "GraphBadOutputOfLoop2 Errors Messages :"
print GraphBadOutputOfLoop2.Messages()



from SuperV import *

# GraphConvertObjRefCheckNotCompatible
GraphConvertObjRefCheckNotCompatible = Graph( "GraphConvertObjRefCheckNotCompatible" )

anXmlFile = os.getenv('DATA_DIR') + '/Superv/Graphs/GraphConvertObjRefCheckNotCompatible.xml'

GraphConvertObjRefCheckNotCompatible.Import( anXmlFile )

RunOk = GraphConvertObjRefCheckNotCompatible.Run()
if RunOk != 0 :
    print "Error : Success call to Run"
    print "GraphConvertObjRefCheckNotCompatible test failed"
    sys.exit(1)

GraphConvertObjRefCheckNotCompatible.DoneW()

print "GraphConvertObjRefCheckNotCompatible Errors Messages :"
print GraphConvertObjRefCheckNotCompatible.Messages()




from SuperV import *

# GraphWrongPythonFunctions
GraphWrongPythonFunctions = Graph( "GraphWrongPythonFunctions" )

anXmlFile = os.getenv('DATA_DIR') + '/Superv/Graphs/GraphWrongPythonFunctions.xml'

GraphWrongPythonFunctions.Import( anXmlFile )

RunOk = GraphWrongPythonFunctions.Run()
if RunOk != 0 :
    print "Error : Success call to Run"
    print "GraphWrongPythonFunctions test failed"
    sys.exit(1)

GraphWrongPythonFunctions.DoneW()

print "GraphWrongPythonFunctions Errors Messages :"
print GraphWrongPythonFunctions.Messages()



from SuperV import *

# GraphWrongContainerOrComponent
GraphWrongContainerOrComponent = Graph( "GraphWrongContainerOrComponent" )

anXmlFile = os.getenv('DATA_DIR') + '/Superv/Graphs/GraphWrongContainerOrComponent.xml'

GraphWrongContainerOrComponent.Import( anXmlFile )

RunOk = GraphWrongContainerOrComponent.Run()
if RunOk == 0 :
    print "Error : UnSuccess call to Run"
    print "GraphWrongContainerOrComponent test failed"
    sys.exit(1)

GraphWrongContainerOrComponent.DoneW()

print "GraphWrongContainerOrComponent Errors Messages :"
print GraphWrongContainerOrComponent.Messages()



from SuperV import *

# GraphLoopSwitchDefaultAborted
GraphLoopSwitchDefaultAborted = Graph( "GraphLoopSwitchDefaultAborted" )

anXmlFile = os.getenv('DATA_DIR') + '/Superv/Graphs/GraphLoopSwitchDefaultAborted.xml'

GraphLoopSwitchDefaultAborted.Import( anXmlFile )

RunOk = GraphLoopSwitchDefaultAborted.Run()
if RunOk == 0 :
    print "Error : UnSuccess call to Run"
    print "GraphLoopSwitchDefaultAborted test failed"
    sys.exit(1)

GraphLoopSwitchDefaultAborted.DoneW()

print "GraphLoopSwitchDefaultAborted Errors Messages :"
print GraphLoopSwitchDefaultAborted.Messages()



from SuperV import *

# GraphLoopSwitchOfSwitchAborted
GraphLoopSwitchOfSwitchAborted = Graph( "GraphLoopSwitchOfSwitchAborted" )

anXmlFile = os.getenv('DATA_DIR') + '/Superv/Graphs/GraphLoopSwitchOfSwitchAborted.xml'

GraphLoopSwitchOfSwitchAborted.Import( anXmlFile )

RunOk = GraphLoopSwitchOfSwitchAborted.Run()
if RunOk == 0 :
    print "Error : UnSuccess call to Run"
    print "GraphLoopSwitchOfSwitchAborted test failed"
    sys.exit(1)

GraphLoopSwitchOfSwitchAborted.DoneW()

print "GraphLoopSwitchOfSwitchAborted Errors Messages :"
print GraphLoopSwitchOfSwitchAborted.Messages()



from SuperV import *

# GraphLoopSwitchOfSwitchNOTValid
GraphLoopSwitchOfSwitchNOTValid = Graph( "GraphLoopSwitchOfSwitchNOTValid" )

anXmlFile = os.getenv('DATA_DIR') + '/Superv/Graphs/GraphLoopSwitchOfSwitchNOTValid.xml'

GraphLoopSwitchOfSwitchNOTValid.Import( anXmlFile )

RunOk = GraphLoopSwitchOfSwitchNOTValid.Run()
if RunOk != 0 :
    print "Error : Success call to Run"
    print "GraphLoopSwitchOfSwitchNOTValid test failed"
    sys.exit(1)

GraphLoopSwitchOfSwitchNOTValid.DoneW()

print "GraphLoopSwitchOfSwitchNOTValid Errors Messages :"
print GraphLoopSwitchOfSwitchNOTValid.Messages()


from SuperV import *

# GraphLoopSwitchsAborted
GraphLoopSwitchsAborted = Graph( "GraphLoopSwitchsAborted" )

anXmlFile = os.getenv('DATA_DIR') + '/Superv/Graphs/GraphLoopSwitchsAborted.xml'

GraphLoopSwitchsAborted.Import( anXmlFile )

RunOk = GraphLoopSwitchsAborted.Run()
if RunOk == 0 :
    print "Error : UnSuccess call to Run"
    print "GraphLoopSwitchsAborted test failed"
    sys.exit(1)

GraphLoopSwitchsAborted.DoneW()

print "GraphLoopSwitchsAborted Errors Messages :"
print GraphLoopSwitchsAborted.Messages()



from SuperV import *

# GraphLoopSwitchsBranchesDefaultAborted
GraphLoopSwitchsBranchesDefaultAborted = Graph( "GraphLoopSwitchsBranchesDefaultAborted" )

anXmlFile = os.getenv('DATA_DIR') + '/Superv/Graphs/GraphLoopSwitchsBranchesDefaultAborted.xml'

GraphLoopSwitchsBranchesDefaultAborted.Import( anXmlFile )

RunOk = GraphLoopSwitchsBranchesDefaultAborted.Run()
if RunOk == 0 :
    print "Error : UnSuccess call to Run"
    print "GraphLoopSwitchsBranchesDefaultAborted test failed"
    sys.exit(1)

GraphLoopSwitchsBranchesDefaultAborted.DoneW()

print "GraphLoopSwitchsBranchesDefaultAborted Errors Messages :"
print GraphLoopSwitchsBranchesDefaultAborted.Messages()



from SuperV import *

# GraphLoopSwitchsNOTValid
GraphLoopSwitchsNOTValid = Graph( "GraphLoopSwitchsNOTValid" )

anXmlFile = os.getenv('DATA_DIR') + '/Superv/Graphs/GraphLoopSwitchsNOTValid.xml'

GraphLoopSwitchsNOTValid.Import( anXmlFile )

RunOk = GraphLoopSwitchsNOTValid.Run()
if RunOk != 0 :
    print "Error : Success call to Run"
    print "GraphLoopSwitchsNOTValid test failed"
    sys.exit(1)

GraphLoopSwitchsNOTValid.DoneW()

print "GraphLoopSwitchsNOTValid Errors Messages :"
print GraphLoopSwitchsNOTValid.Messages()


from SuperV import *

# GraphSwitchCheckBranch2NOTValid
GraphSwitchCheckBranch2NOTValid = Graph( "GraphSwitchCheckBranch2NOTValid" )

anXmlFile = os.getenv('DATA_DIR') + '/Superv/Graphs/GraphSwitchCheckBranch2NOTValid.xml'

GraphSwitchCheckBranch2NOTValid.Import( anXmlFile )

RunOk = GraphSwitchCheckBranch2NOTValid.Run()
if RunOk != 0 :
    print "Error : Success call to Run"
    print "GraphSwitchCheckBranch2NOTValid test failed"
    sys.exit(1)

GraphSwitchCheckBranch2NOTValid.DoneW()

print "GraphSwitchCheckBranch2NOTValid Errors Messages :"
print GraphSwitchCheckBranch2NOTValid.Messages()


from SuperV import *

# GraphSwitchCheckDefault1NOTValid
GraphSwitchCheckDefault1NOTValid = Graph( "GraphSwitchCheckDefault1NOTValid" )

anXmlFile = os.getenv('DATA_DIR') + '/Superv/Graphs/GraphSwitchCheckDefault1NOTValid.xml'

GraphSwitchCheckDefault1NOTValid.Import( anXmlFile )

RunOk = GraphSwitchCheckDefault1NOTValid.Run()
if RunOk != 0 :
    print "Error : Success call to Run"
    print "GraphSwitchCheckDefault1NOTValid test failed"
    sys.exit(1)

GraphSwitchCheckDefault1NOTValid.DoneW()

print "GraphSwitchCheckDefault1NOTValid Errors Messages :"
print GraphSwitchCheckDefault1NOTValid.Messages()


from SuperV import *

# GraphSwitchCheckDefault2NOTValid
GraphSwitchCheckDefault2NOTValid = Graph( "GraphSwitchCheckDefault2NOTValid" )

anXmlFile = os.getenv('DATA_DIR') + '/Superv/Graphs/GraphSwitchCheckDefault2NOTValid.xml'

GraphSwitchCheckDefault2NOTValid.Import( anXmlFile )

RunOk = GraphSwitchCheckDefault2NOTValid.Run()
if RunOk != 0 :
    print "Error : Success call to Run"
    print "GraphSwitchCheckDefault2NOTValid test failed"
    sys.exit(1)

GraphSwitchCheckDefault2NOTValid.DoneW()

print "GraphSwitchCheckDefault2NOTValid Errors Messages :"
print GraphSwitchCheckDefault2NOTValid.Messages()


from SuperV import *

# GraphSwitchCheckDefault2NOTValid1
GraphSwitchCheckDefault2NOTValid1 = Graph( "GraphSwitchCheckDefault2NOTValid1" )

anXmlFile = os.getenv('DATA_DIR') + '/Superv/Graphs/GraphSwitchCheckDefault2NOTValid1.xml'

GraphSwitchCheckDefault2NOTValid1.Import( anXmlFile )

RunOk = GraphSwitchCheckDefault2NOTValid1.Run()
if RunOk != 0 :
    print "Error : Success call to Run"
    print "GraphSwitchCheckDefault2NOTValid1 test failed"
    sys.exit(1)

GraphSwitchCheckDefault2NOTValid1.DoneW()

print "GraphSwitchCheckDefault2NOTValid1 Errors Messages :"
print GraphSwitchCheckDefault2NOTValid1.Messages()


from SuperV import *

# GraphSwitchCheckDefault2NOTValid2
GraphSwitchCheckDefault2NOTValid2 = Graph( "GraphSwitchCheckDefault2NOTValid2" )

anXmlFile = os.getenv('DATA_DIR') + '/Superv/Graphs/GraphSwitchCheckDefault2NOTValid2.xml'

GraphSwitchCheckDefault2NOTValid2.Import( anXmlFile )

RunOk = GraphSwitchCheckDefault2NOTValid2.Run()
if RunOk != 0 :
    print "Error : Success call to Run"
    print "GraphSwitchCheckDefault2NOTValid2 test failed"
    sys.exit(1)

GraphSwitchCheckDefault2NOTValid2.DoneW()

print "GraphSwitchCheckDefault2NOTValid2 Errors Messages :"
print GraphSwitchCheckDefault2NOTValid2.Messages()


from SuperV import *

# GraphSwitchCrash
GraphSwitchCrash = Graph( "GraphSwitchCrash" )

anXmlFile = os.getenv('DATA_DIR') + '/Superv/Graphs/GraphSwitchCrash.xml'

GraphSwitchCrash.Import( anXmlFile )

RunOk = GraphSwitchCrash.Run()
if RunOk == 0 :
    print "Error : UnSuccess call to Run"
    print "GraphSwitchCrash test failed"
    sys.exit(1)

GraphSwitchCrash.DoneW()

print "GraphSwitchCrash Errors Messages :"
print GraphSwitchCrash.Messages()


from SuperV import *

# GraphSwitchErrExec
GraphSwitchErrExec = Graph( "GraphSwitchErrExec" )

anXmlFile = os.getenv('DATA_DIR') + '/Superv/Graphs/GraphSwitchErrExec.xml'

GraphSwitchErrExec.Import( anXmlFile )

RunOk = GraphSwitchErrExec.Run()
if RunOk == 0 :
    print "Error : UnSuccess call to Run"
    print "GraphSwitchErrExec test failed"
    sys.exit(1)

GraphSwitchErrExec.DoneW()

print "GraphSwitchErrExec Errors Messages :"
print GraphSwitchErrExec.Messages()


from SuperV import *

# GraphSwitchGatesNoDefaultAborted
GraphSwitchGatesNoDefaultAborted = Graph( "GraphSwitchGatesNoDefaultAborted" )

anXmlFile = os.getenv('DATA_DIR') + '/Superv/Graphs/GraphSwitchGatesNoDefaultAborted.xml'

GraphSwitchGatesNoDefaultAborted.Import( anXmlFile )

RunOk = GraphSwitchGatesNoDefaultAborted.Run()
if RunOk == 0 :
    print "Error : UnSuccess call to Run"
    print "GraphSwitchGatesNoDefaultAborted test failed"
    sys.exit(1)

GraphSwitchGatesNoDefaultAborted.DoneW()

print "GraphSwitchGatesNoDefaultAborted Errors Messages :"
print GraphSwitchGatesNoDefaultAborted.Messages()


from SuperV import *

# GraphSwitchOutputNOTValid
GraphSwitchOutputNOTValid = Graph( "GraphSwitchOutputNOTValid" )

anXmlFile = os.getenv('DATA_DIR') + '/Superv/Graphs/GraphSwitchOutputNOTValid.xml'

GraphSwitchOutputNOTValid.Import( anXmlFile )

RunOk = GraphSwitchOutputNOTValid.Run()
if RunOk != 0 :
    print "Error : Success call to Run"
    print "GraphSwitchOutputNOTValid test failed"
    sys.exit(1)

GraphSwitchOutputNOTValid.DoneW()

print "GraphSwitchOutputNOTValid Errors Messages :"
print GraphSwitchOutputNOTValid.Messages()


from SuperV import *

# GraphSyrControlAborted
GraphSyrControlAborted = Graph( "GraphSyrControlAborted" )

anXmlFile = os.getenv('DATA_DIR') + '/Superv/Graphs/GraphSyrControlAborted.xml'

GraphSyrControlAborted.Import( anXmlFile )

RunOk = GraphSyrControlAborted.Run()
if RunOk == 0 :
    print "Error : UnSuccess call to Run"
    print "GraphSyrControlAborted test failed"
    sys.exit(1)

GraphSyrControlAborted.DoneW()

print "GraphSyrControlAborted Errors Messages :"
print GraphSyrControlAborted.Messages()


from SuperV import *

# GraphSyrControlAve
GraphSyrControlAve = Graph( "GraphSyrControlAve" )

anXmlFile = os.getenv('DATA_DIR') + '/Superv/Graphs/GraphSyrControlAve.xml'

GraphSyrControlAve.Import( anXmlFile )

RunOk = GraphSyrControlAve.Run()
if RunOk == 0 :
    print "Error : Success call to Run"
    print "GraphSyrControlAve test failed"
    sys.exit(1)

GraphSyrControlAve.DoneW()

State = GraphSyrControlAve.State()
if State != SUPERV.DoneState:
    Messages = GraphSyrControlAve.Messages()
    print "Error GraphSyrControlAve has Messages :"
    print Messages
    sys.exit(1)


from SuperV import *

# GraphSyrControlAveValid
GraphSyrControlAveValid = Graph( "GraphSyrControlAveValid" )

anXmlFile = os.getenv('DATA_DIR') + '/Superv/Graphs/GraphSyrControlAveValid.xml'

GraphSyrControlAveValid.Import( anXmlFile )

RunOk = GraphSyrControlAveValid.Run()
if RunOk == 0 :
    print "Error : Success call to Run"
    print "GraphSyrControlAveValid test failed"
    sys.exit(1)

GraphSyrControlAveValid.DoneW()

Messages = GraphSyrControlAveValid.Messages()
State = GraphSyrControlAveValid.State()
if State != SUPERV.DoneState:
    print "Error GraphSyrControlAveValid has Messages :"
    print Messages
    sys.exit(1)


from SuperV import *

# GraphSyrControlAveExecutable
GraphSyrControlAveExecutable = Graph( "GraphSyrControlAveExecutable" )

anXmlFile = os.getenv('DATA_DIR') + '/Superv/Graphs/GraphSyrControlAveExecutable.xml'

GraphSyrControlAveExecutable.Import( anXmlFile )

RunOk = GraphSyrControlAveExecutable.Run()
if RunOk == 0 :
    print "Error : UnSuccess call to Run"
    print "GraphSyrControlAveExecutable test failed"
    sys.exit(1)

GraphSyrControlAveExecutable.DoneW()

State = GraphSyrControlAveExecutable.State()
if State != SUPERV.DoneState:
    Messages = GraphSyrControlAveExecutable.Messages()
    print "Error GraphSyrControlAveExecutable has Messages :"
    print Messages
    sys.exit(1)


from SuperV import *

# GraphSyrControlAveNOTValid
GraphSyrControlAveNOTValid = Graph( "GraphSyrControlAveNOTValid" )

anXmlFile = os.getenv('DATA_DIR') + '/Superv/Graphs/GraphSyrControlAveNOTValid.xml'

GraphSyrControlAveNOTValid.Import( anXmlFile )

RunOk = GraphSyrControlAveNOTValid.Run()
if RunOk != 0 :
    print "Error : Success call to Run"
    print "GraphSyrControlAveNOTValid test failed"
    sys.exit(1)

GraphSyrControlAveNOTValid.DoneW()

print "GraphSyrControlAveNOTValid Errors Messages :"
print GraphSyrControlAveNOTValid.Messages()


from SuperV import *

# GraphSyrControlAveNOTValid1
GraphSyrControlAveNOTValid1 = Graph( "GraphSyrControlAveNOTValid1" )

anXmlFile = os.getenv('DATA_DIR') + '/Superv/Graphs/GraphSyrControlAveNOTValid1.xml'

GraphSyrControlAveNOTValid1.Import( anXmlFile )

RunOk = GraphSyrControlAveNOTValid1.Run()
if RunOk != 0 :
    print "Error : UnSuccess call to Run"
    print "GraphSyrControlAveNOTValid1 test failed"
    sys.exit(1)

GraphSyrControlAveNOTValid1.DoneW()

print "GraphSyrControlAveNOTValid1 Errors Messages :"
print GraphSyrControlAveNOTValid1.Messages()



from SuperV import *

# GraphSwitch
GraphSwitch = Graph( "GraphSwitch" )

anXmlFile = os.getenv('DATA_DIR') + '/Superv/Graphs/GraphSwitch.xml'

GraphSwitch.Import( anXmlFile )

RunOk = GraphSwitch.Run()
if RunOk == 0 :
    print "Error : UnSuccess call to Run"
    print "GraphSwitch test failed"
    sys.exit(1)

GraphSwitch.DoneW()

Messages = GraphSwitch.Messages()
if Messages != "" :
    print "Error GraphSwitch has Messages :"
    print Messages
    sys.exit(1)


from SuperV import *

# GraphSwitchExecutable
GraphSwitchExecutable = Graph( "GraphSwitchExecutable" )

anXmlFile = os.getenv('DATA_DIR') + '/Superv/Graphs/GraphSwitchExecutable.xml'

GraphSwitchExecutable.Import( anXmlFile )

RunOk = GraphSwitchExecutable.Run()
if RunOk == 0 :
    print "Error : UnSuccess call to Run"
    print "GraphSwitchExecutable test failed"
    sys.exit(1)

GraphSwitchExecutable.DoneW()

Messages = GraphSwitchExecutable.Messages()
if Messages != "" :
    print "Error GraphSwitchExecutable has Messages :"
    print Messages
    sys.exit(1)


from SuperV import *

# GraphSwitchNOTExecutable
GraphSwitchNOTExecutable = Graph( "GraphSwitchNOTExecutable" )

anXmlFile = os.getenv('DATA_DIR') + '/Superv/Graphs/GraphSwitchNOTExecutable.xml'

GraphSwitchNOTExecutable.Import( anXmlFile )

RunOk = GraphSwitchNOTExecutable.Run()
if RunOk != 0 :
    print "Error : Success call to Run"
    print "GraphSwitchNOTExecutable test failed"
    sys.exit(1)

GraphSwitchNOTExecutable.DoneW()

print "GraphSwitchNOTExecutable Errors Messages :"
print GraphSwitchNOTExecutable.Messages()


from SuperV import *

# GraphSwitchNOTExecutable1
GraphSwitchNOTExecutable1 = Graph( "GraphSwitchNOTExecutable1" )

anXmlFile = os.getenv('DATA_DIR') + '/Superv/Graphs/GraphSwitchNOTExecutable1.xml'

GraphSwitchNOTExecutable1.Import( anXmlFile )

RunOk = GraphSwitchNOTExecutable1.Run()
if RunOk != 0 :
    print "Error : Success call to Run"
    print "GraphSwitchNOTExecutable1 test failed"
    sys.exit(1)

GraphSwitchNOTExecutable1.DoneW()

print "GraphSwitchNOTExecutable1 Errors Messages :"
print GraphSwitchNOTExecutable1.Messages()



from SuperV import *

# GraphSwitchNOTExecutable2
GraphSwitchNOTExecutable2 = Graph( "GraphSwitchNOTExecutable2" )

anXmlFile = os.getenv('DATA_DIR') + '/Superv/Graphs/GraphSwitchNOTExecutable2.xml'

GraphSwitchNOTExecutable2.Import( anXmlFile )

RunOk = GraphSwitchNOTExecutable2.Run()
if RunOk != 0 :
    print "Error : Success call to Run"
    print "GraphSwitchNOTExecutable2 test failed"
    sys.exit(1)

GraphSwitchNOTExecutable2.DoneW()

print "GraphSwitchNOTExecutable2 Errors Messages :"
print GraphSwitchNOTExecutable2.Messages()



from SuperV import *

# GraphSwitchBranchGates
GraphSwitchBranchGates = Graph( "GraphSwitchBranchGates" )

anXmlFile = os.getenv('DATA_DIR') + '/Superv/Graphs/GraphSwitchBranchGates.xml'

GraphSwitchBranchGates.Import( anXmlFile )

RunOk = GraphSwitchBranchGates.Run()
if RunOk == 0 :
    print "Error : UnSuccess call to Run"
    print "GraphSwitchBranchGates test failed"
    sys.exit(1)

GraphSwitchBranchGates.DoneW()

Messages = GraphSwitchBranchGates.Messages()
if Messages != "" :
    print "Error GraphSwitchBranchGates has Messages :"
    print Messages
    sys.exit(1)


from SuperV import *

# GraphInLinesUnValid
GraphInLinesUnValid = Graph( "GraphInLinesUnValid" )

anXmlFile = os.getenv('DATA_DIR') + '/Superv/Graphs/GraphInLinesUnValid.xml'

GraphInLinesUnValid.Import( anXmlFile )

RunOk = GraphInLinesUnValid.Run()
if RunOk != 0 :
    print "Error : Success call to Run"
    print "GraphInLinesUnValid test failed"
    sys.exit(1)

GraphInLinesUnValid.DoneW()

print "GraphInLinesUnValid Errors Messages :"
print GraphInLinesUnValid.Messages()



from SuperV import *

# GraphSwitchCheckBranch2NOTValid
GraphSwitchCheckBranch2NOTValid = Graph( "GraphSwitchCheckBranch2NOTValid" )

anXmlFile = os.getenv('DATA_DIR') + '/Superv/Graphs/GraphSwitchCheckBranch2NOTValid.xml'

GraphSwitchCheckBranch2NOTValid.Import( anXmlFile )

RunOk = GraphSwitchCheckBranch2NOTValid.Run()
if RunOk != 0 :
    print "Error : Success call to Run"
    print "GraphSwitchCheckBranch2NOTValid test failed"
    sys.exit(1)

GraphSwitchCheckBranch2NOTValid.DoneW()

print "GraphSwitchCheckBranch2NOTValid Errors Messages :"
print GraphSwitchCheckBranch2NOTValid.Messages()



from SuperV import *

# GraphSwitchCheckDefault1NOTValid
GraphSwitchCheckDefault1NOTValid = Graph( "GraphSwitchCheckDefault1NOTValid" )

anXmlFile = os.getenv('DATA_DIR') + '/Superv/Graphs/GraphSwitchCheckDefault1NOTValid.xml'

GraphSwitchCheckDefault1NOTValid.Import( anXmlFile )

RunOk = GraphSwitchCheckDefault1NOTValid.Run()
if RunOk != 0 :
    print "Error : Success call to Run"
    print "GraphSwitchCheckDefault1NOTValid test failed"
    sys.exit(1)

GraphSwitchCheckDefault1NOTValid.DoneW()

print "GraphSwitchCheckDefault1NOTValid Errors Messages :"
print GraphSwitchCheckDefault1NOTValid.Messages()


from SuperV import *

# GraphSwitchCheckDefault2NOTValid
GraphSwitchCheckDefault2NOTValid = Graph( "GraphSwitchCheckDefault2NOTValid" )

anXmlFile = os.getenv('DATA_DIR') + '/Superv/Graphs/GraphSwitchCheckDefault2NOTValid.xml'

GraphSwitchCheckDefault2NOTValid.Import( anXmlFile )

RunOk = GraphSwitchCheckDefault2NOTValid.Run()
if RunOk != 0 :
    print "Error : Success call to Run"
    print "GraphSwitchCheckDefault2NOTValid test failed"
    sys.exit(1)

GraphSwitchCheckDefault2NOTValid.DoneW()

print "GraphSwitchCheckDefault2NOTValid Errors Messages :"
print GraphSwitchCheckDefault2NOTValid.Messages()




from SuperV import *

# GraphSwitchCheckDefault2NOTValid1
GraphSwitchCheckDefault2NOTValid1 = Graph( "GraphSwitchCheckDefault2NOTValid1" )

anXmlFile = os.getenv('DATA_DIR') + '/Superv/Graphs/GraphSwitchCheckDefault2NOTValid1.xml'

GraphSwitchCheckDefault2NOTValid1.Import( anXmlFile )

RunOk = GraphSwitchCheckDefault2NOTValid1.Run()
if RunOk != 0 :
    print "Error : Success call to Run"
    print "GraphSwitchCheckDefault2NOTValid1 test failed"
    sys.exit(1)

GraphSwitchCheckDefault2NOTValid1.DoneW()

print "GraphSwitchCheckDefault2NOTValid1 Errors Messages :"
print GraphSwitchCheckDefault2NOTValid1.Messages()



from SuperV import *

# GraphSwitchCheckDefault2NOTValid2
GraphSwitchCheckDefault2NOTValid2 = Graph( "GraphSwitchCheckDefault2NOTValid2" )

anXmlFile = os.getenv('DATA_DIR') + '/Superv/Graphs/GraphSwitchCheckDefault2NOTValid2.xml'

GraphSwitchCheckDefault2NOTValid2.Import( anXmlFile )

RunOk = GraphSwitchCheckDefault2NOTValid2.Run()
if RunOk != 0 :
    print "Error : Success call to Run"
    print "GraphSwitchCheckDefault2NOTValid2 test failed"
    sys.exit(1)

GraphSwitchCheckDefault2NOTValid2.DoneW()

print "GraphSwitchCheckDefault2NOTValid2 Errors Messages :"
print GraphSwitchCheckDefault2NOTValid2.Messages()


from SuperV import *

# GraphLoopsCoupled
GraphLoopsCoupled = Graph( "GraphLoopsCoupled" )

anXmlFile = os.getenv('DATA_DIR') + '/Superv/Graphs/GraphLoopsCoupled.xml'

GraphLoopsCoupled.Import( anXmlFile )

RunOk = GraphLoopsCoupled.Run()
if RunOk != 0 :
    print "Error : Success call to Run"
    print "GraphLoopsCoupled test failed"
    sys.exit(1)

GraphLoopsCoupled.DoneW()

print "GraphLoopsCoupled Errors Messages :"
print GraphLoopsCoupled.Messages()


from SuperV import *

# GraphLoopGates
GraphLoopGates = Graph( "GraphLoopGates" )

anXmlFile = os.getenv('DATA_DIR') + '/Superv/Graphs/GraphLoopGates.xml'

GraphLoopGates.Import( anXmlFile )

RunOk = GraphLoopGates.Run()
if RunOk == 0 :
    print "Error : UnSuccess call to Run"
    print "GraphLoopGates test failed"
    sys.exit(1)

GraphLoopGates.DoneW()

Messages = GraphSwitchBranchGates.Messages()
if Messages != "" :
    print "Error GraphSwitchBranchGates has Messages :"
    print Messages
    sys.exit(1)



from SuperV import *

# GraphLoopGatesNOTValid
GraphLoopGatesNOTValid = Graph( "GraphLoopGatesNOTValid" )

anXmlFile = os.getenv('DATA_DIR') + '/Superv/Graphs/GraphLoopGatesNOTValid.xml'

GraphLoopGatesNOTValid.Import( anXmlFile )

RunOk = GraphLoopGatesNOTValid.Run()
if RunOk != 0 :
    print "Error : Success call to Run"
    print "GraphLoopGatesNOTValid test failed"
    sys.exit(1)

GraphLoopGatesNOTValid.DoneW()

print "GraphLoopGatesNOTValid Errors Messages :"
print GraphLoopGatesNOTValid.Messages()


from SuperV import *

# GraphSwitchManyCases
GraphSwitchManyCases = Graph( "GraphSwitchManyCases" )

anXmlFile = os.getenv('DATA_DIR') + '/Superv/Graphs/GraphSwitchManyCases.xml'

GraphSwitchManyCases.Import( anXmlFile )

RunOk = GraphSwitchManyCases.Run()
if RunOk == 0:
    print "Error : UnSuccess call to Run"
    print "GraphSwitchManyCases test failed"
    sys.exit(1)

GraphSwitchManyCases.DoneW()

print "GraphSwitchManyCases Errors Messages :"
print GraphSwitchManyCases.Messages()



from SuperV import *

# GraphLoopTwoBranches
GraphLoopTwoBranches = Graph( "GraphLoopTwoBranches" )

anXmlFile = os.getenv('DATA_DIR') + '/Superv/Graphs/GraphLoopTwoBranches.xml'

GraphLoopTwoBranches.Import( anXmlFile )

RunOk = GraphLoopTwoBranches.Run()
if RunOk == 0 :
    print "Error : UnSuccess call to Run"
    print "GraphLoopTwoBranches test failed"
    sys.exit(1)

GraphLoopTwoBranches.DoneW()

print "GraphLoopTwoBranches Errors Messages :"
print GraphLoopTwoBranches.Messages()



from SuperV import *

# GraphSwitchOutputNOTValid
GraphSwitchOutputNOTValid = Graph( "GraphSwitchOutputNOTValid" )

anXmlFile = os.getenv('DATA_DIR') + '/Superv/Graphs/GraphSwitchOutputNOTValid.xml'

GraphSwitchOutputNOTValid.Import( anXmlFile )

RunOk = GraphSwitchOutputNOTValid.Run()
if RunOk != 0 :
    print "Error : Success call to Run"
    print "GraphSwitchOutputNOTValid test failed"
    sys.exit(1)

GraphSwitchOutputNOTValid.DoneW()

print "GraphSwitchOutputNOTValid Errors Messages :"
print GraphSwitchOutputNOTValid.Messages()



from SuperV import *

# GraphLoopSwitch
GraphLoopSwitch = Graph( "GraphLoopSwitch" )

anXmlFile = os.getenv('DATA_DIR') + '/Superv/Graphs/GraphLoopSwitch.xml'

GraphLoopSwitch.Import( anXmlFile )

RunOk = GraphLoopSwitch.Run()
if RunOk == 0 :
    print "Error : UnSuccess call to Run"
    print "GraphLoopSwitch test failed"
    sys.exit(1)

GraphLoopSwitch.DoneW()

Messages = GraphLoopSwitch.Messages()
if Messages != "" :
    print "Error GraphLoopSwitch has Messages :"
    print Messages
    sys.exit(1)



from SuperV import *

# GraphLoopSwitchGateDefault
GraphLoopSwitchGateDefault = Graph( "GraphLoopSwitchGateDefault" )

anXmlFile = os.getenv('DATA_DIR') + '/Superv/Graphs/GraphLoopSwitchGateDefault.xml'

GraphLoopSwitchGateDefault.Import( anXmlFile )

RunOk = GraphLoopSwitchGateDefault.Run()
if RunOk == 0 :
    print "Error : UnSuccess call to Run"
    print "GraphLoopSwitchGateDefault test failed"
    sys.exit(1)

GraphLoopSwitchGateDefault.DoneW()

Messages = GraphLoopSwitchGateDefault.Messages()
if Messages != "" :
    print "Error GraphLoopSwitchGateDefault has Messages :"
    print Messages
    sys.exit(1)


from SuperV import *

# GraphLoopSwitchGatesDefault
GraphLoopSwitchGatesDefault = Graph( "GraphLoopSwitchGatesDefault" )

anXmlFile = os.getenv('DATA_DIR') + '/Superv/Graphs/GraphLoopSwitchGatesDefault.xml'

GraphLoopSwitchGatesDefault.Import( anXmlFile )

RunOk = GraphLoopSwitchGatesDefault.Run()
if RunOk == 1 :
    print "Error : Success call to Run"
    print "GraphLoopSwitchGatesDefault test failed"
    sys.exit(1)

GraphLoopSwitchGatesDefault.DoneW()

print "GraphLoopSwitchGatesDefault Errors Messages :"
print GraphLoopSwitchGatesDefault.Messages()
