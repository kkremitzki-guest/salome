#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph GraphInLinesConvertCheck
#
from SuperV import *
# Graph creation 
GraphInLinesConvertCheck = Graph( 'GraphInLinesConvertCheck' )
GraphInLinesConvertCheck.SetName( 'GraphInLinesConvertCheck' )
GraphInLinesConvertCheck.SetAuthor( 'JR' )
GraphInLinesConvertCheck.SetComment( '' )
GraphInLinesConvertCheck.Coords( 0 , 0 )

# Creation of Factory Nodes

Addition = GraphInLinesConvertCheck.FNode( 'AddComponent' , 'AddComponent' , 'Addition' )
Addition.SetName( 'Addition' )
Addition.SetAuthor( '' )
Addition.SetContainer( 'localhost/FactoryServer' )
Addition.SetComment( 'Addition from AddComponent' )
Addition.Coords( 232 , 514 )

# Creation of InLine Nodes
PyBoolCheck = []
PyBoolCheck.append( 'def BoolCheck( InBool ) : ' )
PyBoolCheck.append( '	return InBool ' )
PyBoolCheck.append( '' )
BoolCheck = GraphInLinesConvertCheck.INode( 'BoolCheck' , PyBoolCheck )
BoolCheck.InPort( 'InBool' , 'boolean' )
BoolCheck.OutPort( 'OutBool' , 'boolean' )
BoolCheck.SetName( 'BoolCheck' )
BoolCheck.SetAuthor( '' )
BoolCheck.SetComment( 'InLine Node' )
BoolCheck.Coords( 234 , 92 )

PyCharCheck = []
PyCharCheck.append( 'def CharCheck( InChar ) : ' )
PyCharCheck.append( '	return InChar ' )
PyCharCheck.append( '' )
CharCheck = GraphInLinesConvertCheck.INode( 'CharCheck' , PyCharCheck )
CharCheck.InPort( 'InChar' , 'char' )
CharCheck.OutPort( 'OutChar' , 'char' )
CharCheck.SetName( 'CharCheck' )
CharCheck.SetAuthor( '' )
CharCheck.SetComment( 'InLine Node' )
CharCheck.Coords( 30 , 142 )

PyShortCheck = []
PyShortCheck.append( 'def ShortCheck( InShort ) : ' )
PyShortCheck.append( '	return InShort ' )
PyShortCheck.append( '' )
ShortCheck = GraphInLinesConvertCheck.INode( 'ShortCheck' , PyShortCheck )
ShortCheck.InPort( 'InShort' , 'short' )
ShortCheck.OutPort( 'OutShort' , 'short' )
ShortCheck.SetName( 'ShortCheck' )
ShortCheck.SetAuthor( '' )
ShortCheck.SetComment( 'InLine Node' )
ShortCheck.Coords( 234 , 230 )

PyFloatCheck = []
PyFloatCheck.append( 'def FloatCheck( InFloat ) : ' )
PyFloatCheck.append( '	return InFloat ' )
PyFloatCheck.append( '' )
FloatCheck = GraphInLinesConvertCheck.INode( 'FloatCheck' , PyFloatCheck )
FloatCheck.InPort( 'InFloat' , 'float' )
FloatCheck.OutPort( 'OutFloat' , 'float' )
FloatCheck.SetName( 'FloatCheck' )
FloatCheck.SetAuthor( '' )
FloatCheck.SetComment( 'InLine Node' )
FloatCheck.Coords( 230 , 375 )

PyLongCheck = []
PyLongCheck.append( 'def LongCheck( InLong ) : ' )
PyLongCheck.append( '	return InLong  ' )
PyLongCheck.append( '' )
LongCheck = GraphInLinesConvertCheck.INode( 'LongCheck' , PyLongCheck )
LongCheck.InPort( 'InLong' , 'long' )
LongCheck.OutPort( 'OutLong' , 'long' )
LongCheck.SetName( 'LongCheck' )
LongCheck.SetAuthor( '' )
LongCheck.SetComment( 'InLine Node' )
LongCheck.Coords( 30 , 282 )

PyStringCheck = []
PyStringCheck.append( 'def StringCheck( aString ) : ' )
PyStringCheck.append( '	return aString ' )
PyStringCheck.append( '' )
StringCheck = GraphInLinesConvertCheck.INode( 'StringCheck' , PyStringCheck )
StringCheck.InPort( 'InString' , 'string' )
StringCheck.OutPort( 'OutString' , 'string' )
StringCheck.SetName( 'StringCheck' )
StringCheck.SetAuthor( '' )
StringCheck.SetComment( 'InLine Node' )
StringCheck.Coords( 30 , 6 )

PyDoubleCheck = []
PyDoubleCheck.append( 'def DoubleCheck( InDouble ) : ' )
PyDoubleCheck.append( '	return InDouble ' )
PyDoubleCheck.append( '' )
DoubleCheck = GraphInLinesConvertCheck.INode( 'DoubleCheck' , PyDoubleCheck )
DoubleCheck.InPort( 'InDouble' , 'double' )
DoubleCheck.OutPort( 'OutDouble' , 'double' )
DoubleCheck.SetName( 'DoubleCheck' )
DoubleCheck.SetAuthor( '' )
DoubleCheck.SetComment( 'Compute Node' )
DoubleCheck.Coords( 23 , 427 )

PyMiscTypes = []
PyMiscTypes.append( 'def MiscTypes( InString , InBool , InChar , InShort , InLong , InFloat , InDouble , InObjRef ) : ' )
PyMiscTypes.append( '	return InString,InBool,InChar,InShort,InLong,InFloat,InDouble,InObjRef ' )
MiscTypes = GraphInLinesConvertCheck.INode( 'MiscTypes' , PyMiscTypes )
MiscTypes.InPort( 'InString' , 'string' )
MiscTypes.InPort( 'InBool' , 'boolean' )
MiscTypes.InPort( 'InChar' , 'char' )
MiscTypes.InPort( 'InShort' , 'short' )
MiscTypes.InPort( 'InLong' , 'long' )
MiscTypes.InPort( 'InFloat' , 'float' )
MiscTypes.InPort( 'InDouble' , 'double' )
MiscTypes.InPort( 'InObjRef' , 'SuperVision::Adder' )
MiscTypes.OutPort( 'OutString' , 'string' )
MiscTypes.OutPort( 'OutBool' , 'boolean' )
MiscTypes.OutPort( 'OutChar' , 'char' )
MiscTypes.OutPort( 'OutShort' , 'short' )
MiscTypes.OutPort( 'OutLong' , 'long' )
MiscTypes.OutPort( 'OutFloat' , 'float' )
MiscTypes.OutPort( 'OutDouble' , 'double' )
MiscTypes.OutPort( 'OutObjRef' , 'SuperVisionTest::Addre' )
MiscTypes.SetName( 'MiscTypes' )
MiscTypes.SetAuthor( '' )
MiscTypes.SetComment( 'InLine Node' )
MiscTypes.Coords( 477 , 231 )

# Creation of Links
BoolCheckOutBool = BoolCheck.Port( 'OutBool' )
MiscTypesInBool = GraphInLinesConvertCheck.Link( BoolCheckOutBool , MiscTypes.Port( 'InBool' ) )
MiscTypesInBool.AddCoord( 1 , 446 , 341 )
MiscTypesInBool.AddCoord( 2 , 446 , 172 )

CharCheckOutChar = CharCheck.Port( 'OutChar' )
MiscTypesInChar = GraphInLinesConvertCheck.Link( CharCheckOutChar , MiscTypes.Port( 'InChar' ) )
MiscTypesInChar.AddCoord( 1 , 431 , 368 )
MiscTypesInChar.AddCoord( 2 , 431 , 223 )

ShortCheckOutShort = ShortCheck.Port( 'OutShort' )
MiscTypesInShort = GraphInLinesConvertCheck.Link( ShortCheckOutShort , MiscTypes.Port( 'InShort' ) )
MiscTypesInShort.AddCoord( 1 , 415 , 397 )
MiscTypesInShort.AddCoord( 2 , 414 , 310 )

FloatCheckOutFloat = FloatCheck.Port( 'OutFloat' )
MiscTypesInFloat = GraphInLinesConvertCheck.Link( FloatCheckOutFloat , MiscTypes.Port( 'InFloat' ) )

AdditionAdder = Addition.Port( 'Adder' )
MiscTypesInObjRef = GraphInLinesConvertCheck.Link( AdditionAdder , MiscTypes.Port( 'InObjRef' ) )
MiscTypesInObjRef.AddCoord( 1 , 462 , 514 )
MiscTypesInObjRef.AddCoord( 2 , 461 , 593 )

LongCheckOutLong = LongCheck.Port( 'OutLong' )
MiscTypesInLong = GraphInLinesConvertCheck.Link( LongCheckOutLong , MiscTypes.Port( 'InLong' ) )
MiscTypesInLong.AddCoord( 1 , 406 , 426 )
MiscTypesInLong.AddCoord( 2 , 405 , 363 )

StringCheckOutString = StringCheck.Port( 'OutString' )
MiscTypesInString = GraphInLinesConvertCheck.Link( StringCheckOutString , MiscTypes.Port( 'InString' ) )
MiscTypesInString.AddCoord( 1 , 462 , 312 )
MiscTypesInString.AddCoord( 2 , 461 , 87 )

DoubleCheckOutDouble = DoubleCheck.Port( 'OutDouble' )
MiscTypesInDouble = GraphInLinesConvertCheck.Link( DoubleCheckOutDouble , MiscTypes.Port( 'InDouble' ) )
MiscTypesInDouble.AddCoord( 1 , 445 , 485 )
MiscTypesInDouble.AddCoord( 2 , 445 , 508 )

# Creation of Input datas
BoolCheckInBool = BoolCheck.Input( 'InBool' , 1)
CharCheckInChar = CharCheck.Input( 'InChar' , 255)
ShortCheckInShort = ShortCheck.Input( 'InShort' , 16383)
FloatCheckInFloat = FloatCheck.Input( 'InFloat' , 3.14159)
LongCheckInLong = LongCheck.Input( 'InLong' , 1234567890)
StringCheckInString = StringCheck.Input( 'InString' , 'aString')
DoubleCheckInDouble = DoubleCheck.Input( 'InDouble' , 1.23457)

# Creation of Output variables
MiscTypesOutString = MiscTypes.Port( 'OutString' )
MiscTypesOutBool = MiscTypes.Port( 'OutBool' )
MiscTypesOutChar = MiscTypes.Port( 'OutChar' )
MiscTypesOutShort = MiscTypes.Port( 'OutShort' )
MiscTypesOutLong = MiscTypes.Port( 'OutLong' )
MiscTypesOutFloat = MiscTypes.Port( 'OutFloat' )
MiscTypesOutDouble = MiscTypes.Port( 'OutDouble' )
MiscTypesOutObjRef = MiscTypes.Port( 'OutObjRef' )

GraphInLinesConvertCheck.Run()
GraphInLinesConvertCheck.DoneW()
GraphInLinesConvertCheck.PrintPorts()
