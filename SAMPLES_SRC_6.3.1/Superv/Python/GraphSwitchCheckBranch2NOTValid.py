#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph GraphSwitchCheckBranch2NOTValid
#
from SuperV import *

# Graph creation of GraphSwitchCheckBranch2NOTValid
def DefGraphSwitchCheckBranch2NOTValid() :
    GraphSwitchCheckBranch2NOTValid = Graph( 'GraphSwitchCheckBranch2NOTValid' )
    GraphSwitchCheckBranch2NOTValid.SetName( 'GraphSwitchCheckBranch2NOTValid' )
    GraphSwitchCheckBranch2NOTValid.SetAuthor( 'JR' )
    GraphSwitchCheckBranch2NOTValid.SetComment( 'Switch with two SwitchBranches, One NotSwitchBranch and the DefaultBranch
Shared Node between Branches' )
    GraphSwitchCheckBranch2NOTValid.Coords( 0 , 0 )
    
    # Creation of Factory Nodes
    
    # Creation of InLine Nodes
    PyIsOdd = []
    PyIsOdd.append( 'from time import *    ' )
    PyIsOdd.append( 'def IsOdd(aBranch) :        ' )
    PyIsOdd.append( '    print a,"IsOdd (GraphSwitch)"       ' )
    PyIsOdd.append( '    sleep( 1 )    ' )
    PyIsOdd.append( '    return a      ' )
    IsOdd = GraphSwitchCheckBranch2NOTValid.INode( 'IsOdd' , PyIsOdd )
    IsOdd.SetName( 'IsOdd' )
    IsOdd.SetAuthor( '' )
    IsOdd.SetComment( 'Python function' )
    IsOdd.Coords( 649 , 6 )
    IIsOdda = IsOdd.InPort( 'a' , 'long' )
    IIsOddBranch = IsOdd.InPort( 'Branch' , 'string' )
    IIsOddGate = IsOdd.GetInPort( 'Gate' )
    OIsOdda = IsOdd.OutPort( 'a' , 'long' )
    OIsOddGate = IsOdd.GetOutPort( 'Gate' )
    
    PyIsEven = []
    PyIsEven.append( 'from time import *     ' )
    PyIsEven.append( 'def IsEven(aBranch) :         ' )
    PyIsEven.append( '    print a,"IsEven (GraphSwitch)"        ' )
    PyIsEven.append( '    sleep( 1 )     ' )
    PyIsEven.append( '    return a       ' )
    IsEven = GraphSwitchCheckBranch2NOTValid.INode( 'IsEven' , PyIsEven )
    IsEven.SetName( 'IsEven' )
    IsEven.SetAuthor( '' )
    IsEven.SetComment( 'Compute Node' )
    IsEven.Coords( 657 , 451 )
    IIsEvena = IsEven.InPort( 'a' , 'long' )
    IIsEvenBranch = IsEven.InPort( 'Branch' , 'string' )
    IIsEvenGate = IsEven.GetInPort( 'Gate' )
    OIsEvena = IsEven.OutPort( 'a' , 'long' )
    OIsEvenGate = IsEven.GetOutPort( 'Gate' )
    
    PyBranchNode = []
    PyBranchNode.append( 'from time import *  ' )
    PyBranchNode.append( 'def Print_1(Branch) :      ' )
    PyBranchNode.append( '    print "Print ",Branch  ' )
    PyBranchNode.append( '    sleep(1)  ' )
    PyBranchNode.append( '    return Branch      ' )
    BranchNode = GraphSwitchCheckBranch2NOTValid.INode( 'Print_1' , PyBranchNode )
    BranchNode.SetName( 'BranchNode' )
    BranchNode.SetAuthor( '' )
    BranchNode.SetComment( 'Compute Node' )
    BranchNode.Coords( 429 , 177 )
    IBranchNodeBranch = BranchNode.InPort( 'Branch' , 'string' )
    IBranchNodeGate = BranchNode.GetInPort( 'Gate' )
    OBranchNodeBranch = BranchNode.OutPort( 'Branch' , 'string' )
    OBranchNodeGate = BranchNode.GetOutPort( 'Gate' )
    
    Pydefault = []
    Pydefault.append( 'from time import *  ' )
    Pydefault.append( 'def default(a,Branch) :  ' )
    Pydefault.append( '    sleep(1)  ' )
    Pydefault.append( '    return a   ' )
    default = GraphSwitchCheckBranch2NOTValid.INode( 'default' , Pydefault )
    default.SetName( 'default' )
    default.SetAuthor( '' )
    default.SetComment( 'Compute Node' )
    default.Coords( 655 , 306 )
    Idefaulta = default.InPort( 'a' , 'long' )
    IdefaultBranch = default.InPort( 'Branch' , 'string' )
    IdefaultGate = default.GetInPort( 'Gate' )
    Odefaulta = default.OutPort( 'a' , 'long' )
    OdefaultGate = default.GetOutPort( 'Gate' )
    
    # Creation of Loop Nodes
    PyInitLoopSwitch = []
    PyInitLoopSwitch.append( 'def InitLoop(Index,Min,Max) :        ' )
    PyInitLoopSwitch.append( '	Index = Max  ' )
    PyInitLoopSwitch.append( '	return Index,Min,Max       ' )
    PyMoreInitLoopSwitch = []
    PyMoreInitLoopSwitch.append( 'from time import * ' )
    PyMoreInitLoopSwitch.append( 'def MoreLoop(Index,Min,Max) : ' )
    PyMoreInitLoopSwitch.append( '	sleep(1)  ' )
    PyMoreInitLoopSwitch.append( '	if Index >= Min :     ' )
    PyMoreInitLoopSwitch.append( '		DoLoop = 1       ' )
    PyMoreInitLoopSwitch.append( '	else :       ' )
    PyMoreInitLoopSwitch.append( '		DoLoop = 0       ' )
    PyMoreInitLoopSwitch.append( '	return DoLoop,Index,Min,Max       ' )
    PyNextInitLoopSwitch = []
    PyNextInitLoopSwitch.append( 'def NextLoop(Index,Min,Max) :       ' )
    PyNextInitLoopSwitch.append( '	Index = Index - 1       ' )
    PyNextInitLoopSwitch.append( '	return Index,Min,Max       ' )
    InitLoopSwitch,EndOfInitLoopSwitch = GraphSwitchCheckBranch2NOTValid.LNode( 'InitLoop' , PyInitLoopSwitch , 'MoreLoop' , PyMoreInitLoopSwitch , 'NextLoop' , PyNextInitLoopSwitch )
    EndOfInitLoopSwitch.SetName( 'EndOfInitLoopSwitch' )
    EndOfInitLoopSwitch.SetAuthor( '' )
    EndOfInitLoopSwitch.SetComment( 'Compute Node' )
    EndOfInitLoopSwitch.Coords( 1073 , 216 )
    PyEndOfInitLoopSwitch = []
    PyEndOfInitLoopSwitch.append( 'from time import *  ' )
    PyEndOfInitLoopSwitch.append( 'def EndOfInitLoopSwitch(DoLoop,Index,Min,Max) :  ' )
    PyEndOfInitLoopSwitch.append( '    sleep(1)  ' )
    PyEndOfInitLoopSwitch.append( '    return DoLoop,Index,Min,Max  ' )
    EndOfInitLoopSwitch.SetPyFunction( 'EndOfInitLoopSwitch' , PyEndOfInitLoopSwitch )
    IInitLoopSwitchDoLoop = InitLoopSwitch.GetInPort( 'DoLoop' )
    IInitLoopSwitchIndex = InitLoopSwitch.InPort( 'Index' , 'long' )
    IInitLoopSwitchMin = InitLoopSwitch.InPort( 'Min' , 'long' )
    IInitLoopSwitchMax = InitLoopSwitch.InPort( 'Max' , 'long' )
    IInitLoopSwitchGate = InitLoopSwitch.GetInPort( 'Gate' )
    OInitLoopSwitchDoLoop = InitLoopSwitch.GetOutPort( 'DoLoop' )
    OInitLoopSwitchIndex = InitLoopSwitch.GetOutPort( 'Index' )
    OInitLoopSwitchMin = InitLoopSwitch.GetOutPort( 'Min' )
    OInitLoopSwitchMax = InitLoopSwitch.GetOutPort( 'Max' )
    IEndOfInitLoopSwitchDoLoop = EndOfInitLoopSwitch.GetInPort( 'DoLoop' )
    IEndOfInitLoopSwitchIndex = EndOfInitLoopSwitch.GetInPort( 'Index' )
    IEndOfInitLoopSwitchMin = EndOfInitLoopSwitch.GetInPort( 'Min' )
    IEndOfInitLoopSwitchMax = EndOfInitLoopSwitch.GetInPort( 'Max' )
    IEndOfInitLoopSwitchGate = EndOfInitLoopSwitch.GetInPort( 'Gate' )
    OEndOfInitLoopSwitchDoLoop = EndOfInitLoopSwitch.GetOutPort( 'DoLoop' )
    OEndOfInitLoopSwitchIndex = EndOfInitLoopSwitch.GetOutPort( 'Index' )
    OEndOfInitLoopSwitchMin = EndOfInitLoopSwitch.GetOutPort( 'Min' )
    OEndOfInitLoopSwitchMax = EndOfInitLoopSwitch.GetOutPort( 'Max' )
    OEndOfInitLoopSwitchGate = EndOfInitLoopSwitch.GetOutPort( 'Gate' )
    InitLoopSwitch.SetName( 'InitLoopSwitch' )
    InitLoopSwitch.SetAuthor( '' )
    InitLoopSwitch.SetComment( 'Compute Node' )
    InitLoopSwitch.Coords( 10 , 157 )
    
    # Creation of Switch Nodes
    PySwitch = []
    PySwitch.append( 'from time import *        ' )
    PySwitch.append( 'def Switch(a) : ' )
    PySwitch.append( '    sleep(1) ' )
    PySwitch.append( '    Branch = "Negative or null"    ' )
    PySwitch.append( '    if a <= 0 :       ' )
    PySwitch.append( '        return 0,0,a,Branch,1  ' )
    PySwitch.append( '    if ( a & 1 ) == 0 :    ' )
    PySwitch.append( '        Branch = "Even"    ' )
    PySwitch.append( '    else :    ' )
    PySwitch.append( '        Branch = "Odd"    ' )
    PySwitch.append( '    return a & 1,1-(a&1),a,Branch,0  ' )
    Switch,EndOfSwitch = GraphSwitchCheckBranch2NOTValid.SNode( 'Switch' , PySwitch )
    EndOfSwitch.SetName( 'EndOfSwitch' )
    EndOfSwitch.SetAuthor( '' )
    EndOfSwitch.SetComment( 'Compute Node' )
    EndOfSwitch.Coords( 880 , 216 )
    PyEndOfSwitch = []
    PyEndOfSwitch.append( 'from time import * ' )
    PyEndOfSwitch.append( 'def EndOfSwitch(a,Branch) : ' )
    PyEndOfSwitch.append( '    sleep(1) ' )
    PyEndOfSwitch.append( '    return a ' )
    EndOfSwitch.SetPyFunction( 'EndOfSwitch' , PyEndOfSwitch )
    IEndOfSwitcha = EndOfSwitch.InPort( 'a' , 'long' )
    IEndOfSwitchBranch = EndOfSwitch.InPort( 'Branch' , 'string' )
    IEndOfSwitchDefault = EndOfSwitch.GetInPort( 'Default' )
    OEndOfSwitcha = EndOfSwitch.OutPort( 'a' , 'long' )
    OEndOfSwitchGate = EndOfSwitch.GetOutPort( 'Gate' )
    Switch.SetName( 'Switch' )
    Switch.SetAuthor( '' )
    Switch.SetComment( 'Compute Node' )
    Switch.Coords( 194 , 157 )
    ISwitcha = Switch.InPort( 'a' , 'long' )
    ISwitchGate = Switch.GetInPort( 'Gate' )
    OSwitchOdd = Switch.OutPort( 'Odd' , 'long' )
    OSwitchEven = Switch.OutPort( 'Even' , 'int' )
    OSwitcha = Switch.OutPort( 'a' , 'int' )
    OSwitchBranch = Switch.OutPort( 'Branch' , 'string' )
    OSwitchdefault = Switch.OutPort( 'default' , 'boolean' )
    OSwitchDefault = Switch.GetOutPort( 'Default' )
    
    # Creation of Links
    LIsOddaEndOfSwitcha = GraphSwitchCheckBranch2NOTValid.Link( OIsOdda , IEndOfSwitcha )
    
    LIsEvenaEndOfSwitcha = GraphSwitchCheckBranch2NOTValid.Link( OIsEvena , IEndOfSwitcha )
    
    LBranchNodeBranchEndOfSwitchBranch = GraphSwitchCheckBranch2NOTValid.Link( OBranchNodeBranch , IEndOfSwitchBranch )
    
    LBranchNodeBranchIsOddBranch = GraphSwitchCheckBranch2NOTValid.Link( OBranchNodeBranch , IIsOddBranch )
    
    LBranchNodeBranchdefaultBranch = GraphSwitchCheckBranch2NOTValid.Link( OBranchNodeBranch , IdefaultBranch )
    
    LBranchNodeBranchIsEvenBranch = GraphSwitchCheckBranch2NOTValid.Link( OBranchNodeBranch , IIsEvenBranch )
    
    LInitLoopSwitchIndexSwitcha = GraphSwitchCheckBranch2NOTValid.Link( OInitLoopSwitchIndex , ISwitcha )
    
    LInitLoopSwitchMinEndOfInitLoopSwitchMin = GraphSwitchCheckBranch2NOTValid.Link( OInitLoopSwitchMin , IEndOfInitLoopSwitchMin )
    
    LInitLoopSwitchMaxEndOfInitLoopSwitchMax = GraphSwitchCheckBranch2NOTValid.Link( OInitLoopSwitchMax , IEndOfInitLoopSwitchMax )
    
    LSwitchOddIsOddGate = GraphSwitchCheckBranch2NOTValid.Link( OSwitchOdd , IIsOddGate )
    LSwitchOddIsOddGate.AddCoord( 1 , 400 , 121 )
    LSwitchOddIsOddGate.AddCoord( 2 , 401 , 188 )
    
    LSwitchEvenIsEvenGate = GraphSwitchCheckBranch2NOTValid.Link( OSwitchEven , IIsEvenGate )
    LSwitchEvenIsEvenGate.AddCoord( 1 , 391 , 568 )
    LSwitchEvenIsEvenGate.AddCoord( 2 , 392 , 208 )
    
    LSwitchadefaulta = GraphSwitchCheckBranch2NOTValid.Link( OSwitcha , Idefaulta )
    
    LSwitchaIsEvena = GraphSwitchCheckBranch2NOTValid.Link( OSwitcha , IIsEvena )
    
    LSwitchaIsOdda = GraphSwitchCheckBranch2NOTValid.Link( OSwitcha , IIsOdda )
    
    LSwitchBranchBranchNodeBranch = GraphSwitchCheckBranch2NOTValid.Link( OSwitchBranch , IBranchNodeBranch )
    
    LSwitchdefaultdefaultGate = GraphSwitchCheckBranch2NOTValid.Link( OSwitchdefault , IdefaultGate )
    LSwitchdefaultdefaultGate.AddCoord( 1 , 410 , 422 )
    LSwitchdefaultdefaultGate.AddCoord( 2 , 410 , 269 )
    LSwitchdefaultdefaultGate.AddCoord( 3 , 410 , 268 )
    
    LSwitchDefaultEndOfSwitchDefault = GraphSwitchCheckBranch2NOTValid.Link( OSwitchDefault , IEndOfSwitchDefault )
    
    LEndOfSwitchaEndOfInitLoopSwitchIndex = GraphSwitchCheckBranch2NOTValid.Link( OEndOfSwitcha , IEndOfInitLoopSwitchIndex )
    
    LdefaultaEndOfSwitcha = GraphSwitchCheckBranch2NOTValid.Link( Odefaulta , IEndOfSwitcha )
    
    # Input datas
    IInitLoopSwitchIndex.Input( 0 )
    IInitLoopSwitchMin.Input( -5 )
    IInitLoopSwitchMax.Input( 10 )
    
    # Output Ports of the graph
    #OEndOfInitLoopSwitchIndex = EndOfInitLoopSwitch.GetOutPort( 'Index' )
    #OEndOfInitLoopSwitchMin = EndOfInitLoopSwitch.GetOutPort( 'Min' )
    #OEndOfInitLoopSwitchMax = EndOfInitLoopSwitch.GetOutPort( 'Max' )
    return GraphSwitchCheckBranch2NOTValid


GraphSwitchCheckBranch2NOTValid = DefGraphSwitchCheckBranch2NOTValid()
