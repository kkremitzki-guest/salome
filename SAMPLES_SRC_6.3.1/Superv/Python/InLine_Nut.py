#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
#####################################################################
#Created                :17/02/2005
#Auhtor                 :KOVALTCHUK Alexey 
#####################################################################
#
def Geometry(y1=50, y2=90):
    from batchmode_geompy import *
    import  math 
    import os
    #Sketcher_1 creation
    Sketcher_1 = MakeSketcher("Sketcher:F 100 -57.7:TT 100 57.7:TT 0 115.47:TT -100 57.7:TT -100 -57.7:TT 0 -115.47:WW") 
    addToStudy(Sketcher_1, "Sketcher_1")
    Face_1 = MakeFace(Sketcher_1, 1)
    addToStudy(Face_1, "Face_1")
    #Line creation
    Line_1 = MakeLineTwoPnt(MakeVertex(0,0,0), MakeVertex(0,0,100))
    addToStudy(Line_1, "Line_1")
    #Prism creation
    Prism_1 = MakePrismVecH(Face_1, Line_1, 100)
    addToStudy(Prism_1, "Prism_1")
    #Sketcher_2 creation
    #"Sketcher:F 50 0:TT 80 0:TT 112 13:TT 112 48:TT 80 63:TT 80 90:TT 50 90:WW"
    command = "Sketcher:F " + str(y1)+ " 0:TT 80 0:TT 112 13:TT 112 48:TT 80 63:TT 80 " + str(y2)+ ":TT " + str(y1) + " " + str(y2) + ":WW"
    Sketcher_2 = MakeSketcher(command, [0,0,0, 1,0,0, 0,1,0])
    addToStudy(Sketcher_2, "Sketcher_2")
    Face_2 = MakeFace(Sketcher_2, 1)
    addToStudy(Face_2, "Face_2")
    #Revolution creation
    Revolution_1 = MakeRevolution(Face_2, Line_1, 2*math.pi)
    addToStudy(Revolution_1, "Revolution_1")
    #Common applying
    Common_1 = MakeBoolean(Revolution_1, Prism_1, 1)
    addToStudy(Common_1, "Common_1")
    #Explode Common_1 on edges
    CommonExplodedListEdges = SubShapeAll(Common_1, ShapeType["EDGE"])
    for i in range(0, len(CommonExplodedListEdges)):
        name = "Edge_"+str(i+1)
        addToStudyInFather(Common_1, CommonExplodedListEdges[i], name)
    #Fillet applying
    ID = GetSubShapeID(Common_1, CommonExplodedListEdges[0])
    Fillet_1 = MakeFillet(Common_1, 10, ShapeType["EDGE"], [6])
    addToStudy(Fillet_1, "Fillet_1")
    #Chamfer applying
    Chamfer_1 = MakeChamferEdge(Fillet_1, 10, 10, 16, 50 )
    addToStudy(Chamfer_1, "Chamfer_1")
    Chamfer_2 = MakeChamferEdge(Chamfer_1, 10, 10, 21, 31 )
    addToStudy(Chamfer_2, "Chamfer_2")
    #Import of the shape from "slots.brep"
    thePath = os.getenv("DATA_DIR")
    theFileName = thePath + "/Shapes/Brep/slots.brep"
    theShapeForCut = ImportBREP(theFileName)
    addToStudy(theShapeForCut, "slot.brep_1")
    #Cut applying
    Cut_1 = MakeBoolean(Chamfer_2, theShapeForCut, 2)
    addToStudy(Cut_1, "Cut_1")
    return Cut_1
    

def Mesh(theNameOfTheShape = "Cut_1", theAverageLength = 5, theMaxElementArea = 20, theMaxElementVolume = 150):
    #Format of the <theNameOfTheShape> parameter is: "[[first level object/[second level object/[.../]]]Name"
    from batchmode_smesh import *
    import StdMeshers
    smesh.SetCurrentStudy(myStudy)
    theNameOfTheShape = "/Geometry/"+ str(theNameOfTheShape)
    SObject = myStudy.FindObjectByPath(theNameOfTheShape)
    if SObject == None:
        raise RuntimeError, "It is an incorrect object name..."
    shape_mesh = IDToObject( SObject.GetID() )
    mesh = smesh.CreateMesh(shape_mesh)
    idmesh = ObjectToID(mesh)
    SetName( idmesh, "Mesh" )

    #HYPOTHESIS CREATION
    print "-------------------------- Average length"
    theName = "AverageLength" + str(theAverageLength)
    hAvLength = smesh.CreateHypothesis( "LocalLength", "libStdMeshersEngine.so" )
    hAvLength.SetLength( theAverageLength )
    print hAvLength.GetName()
    print hAvLength.GetId()
    SetName(ObjectToID(hAvLength), theName)

    print "-------------------------- MaxElementArea"
    theName = "MaxElementArea" + str( theMaxElementArea )
    hArea = smesh.CreateHypothesis( "MaxElementArea", "libStdMeshersEngine.so" )
    hArea.SetMaxElementArea( theMaxElementArea )
    print hArea.GetName()
    print hArea.GetId()
    print hArea.GetMaxElementArea()
    SetName(ObjectToID(hArea), theName)
    
    print "-------------------------- MaxElementVolume"
    theName = "MaxElementVolume" + str( theMaxElementVolume )
    hVolume = smesh.CreateHypothesis( "MaxElementVolume", "libStdMeshersEngine.so" )
    hVolume.SetMaxElementVolume( theMaxElementVolume )
    print hVolume.GetName()
    print hVolume.GetId()
    print hVolume.GetMaxElementVolume()
    SetName(ObjectToID(hVolume), theName)
    
    mesh.AddHypothesis(shape_mesh, hAvLength)
    mesh.AddHypothesis(shape_mesh, hArea)
    mesh.AddHypothesis(shape_mesh, hVolume)
    
    print "-------------------------- Regular_1D"
    
    algoReg1D = smesh.CreateHypothesis( "Regular_1D", "libStdMeshersEngine.so" )
    listHyp = algoReg1D.GetCompatibleHypothesis()
    for hyp in listHyp:
        print hyp
        print algoReg1D.GetName()
        print algoReg1D.GetId()
    SetName(ObjectToID(algoReg1D), "Wire discretisation")
        
    print "-------------------------- MEFISTO_2D"
    algoMef = smesh.CreateHypothesis( "MEFISTO_2D", "libStdMeshersEngine.so" )
    listHyp = algoMef.GetCompatibleHypothesis()
    for hyp in listHyp:
        print hyp
        print algoMef.GetName()
        print algoMef.GetId()
    SetName(ObjectToID(algoMef), "Triangle (Mefisto)")

    print "-------------------------- NETGEN_3D"        
    algoNg = smesh.CreateHypothesis( "NETGEN_3D", "libNETGENEngine.so" )
    for hyp in listHyp:
        print hyp
        print algoNg.GetName()
        print algoNg.GetId()
    SetName(ObjectToID(algoNg), "Tetrahedron (NETGEN)")
    mesh.AddHypothesis(shape_mesh, algoReg1D)
    mesh.AddHypothesis(shape_mesh, algoMef)
    mesh.AddHypothesis(shape_mesh, algoNg)
    smesh.Compute(mesh,shape_mesh)
            
    print "Information about the mesh:"
    print "Number of nodes       : ", mesh.NbNodes()
    print "Number of edges       : ", mesh.NbEdges()
    print "Number of faces       : ", mesh.NbFaces()
    print "Number of triangles   : ", mesh.NbTriangles()
    print "Number of quadrangles : ", mesh.NbQuadrangles()
    print "Number of volumes     : ", mesh.NbVolumes()
    print "Number of tetrahedrons: ", mesh.NbTetras()
            

