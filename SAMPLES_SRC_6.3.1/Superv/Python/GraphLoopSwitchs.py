#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph GraphLoopSwitchs_1
#
from SuperV import *

# Graph creation of GraphLoopSwitchs_1
def DefGraphLoopSwitchs_1() :
    GraphLoopSwitchs_1 = Graph( 'GraphLoopSwitchs_1' )
    GraphLoopSwitchs_1.SetName( 'GraphLoopSwitchs_1' )
    GraphLoopSwitchs_1.SetAuthor( 'JR' )
    GraphLoopSwitchs_1.SetComment( '' )
    GraphLoopSwitchs_1.Coords( 0 , 0 )
    
    # Creation of Factory Nodes
    
    # Creation of InLine Nodes
    PyIsOdd = []
    PyIsOdd.append( 'from time import *     ' )
    PyIsOdd.append( 'def IsOdd(a) :         ' )
    PyIsOdd.append( '    print a,"IsOdd"        ' )
    PyIsOdd.append( '    sleep( 1 )     ' )
    PyIsOdd.append( '    return a,1 ' )
    IsOdd = GraphLoopSwitchs_1.INode( 'IsOdd' , PyIsOdd )
    IsOdd.SetName( 'IsOdd' )
    IsOdd.SetAuthor( '' )
    IsOdd.SetComment( 'Python function' )
    IsOdd.Coords( 476 , 50 )
    IIsOdda = IsOdd.InPort( 'a' , 'long' )
    IIsOddGate = IsOdd.GetInPort( 'Gate' )
    OIsOdda = IsOdd.OutPort( 'a' , 'long' )
    OIsOddOdd = IsOdd.OutPort( 'Odd' , 'long' )
    OIsOddGate = IsOdd.GetOutPort( 'Gate' )
    
    PyIsEven = []
    PyIsEven.append( 'from time import *     ' )
    PyIsEven.append( 'def IsEven(a) :         ' )
    PyIsEven.append( '    print a,"IsEven"        ' )
    PyIsEven.append( '    sleep( 1 )     ' )
    PyIsEven.append( '    return a,1 ' )
    IsEven = GraphLoopSwitchs_1.INode( 'IsEven' , PyIsEven )
    IsEven.SetName( 'IsEven' )
    IsEven.SetAuthor( '' )
    IsEven.SetComment( 'Python function' )
    IsEven.Coords( 482 , 292 )
    IIsEvena = IsEven.InPort( 'a' , 'long' )
    IIsEvenGate = IsEven.GetInPort( 'Gate' )
    OIsEvena = IsEven.OutPort( 'a' , 'long' )
    OIsEvenEven = IsEven.OutPort( 'Even' , 'long' )
    OIsEvenGate = IsEven.GetOutPort( 'Gate' )
    
    PySwitchsCompare = []
    PySwitchsCompare.append( 'from time import * ' )
    PySwitchsCompare.append( 'def SwitchsCompare(aOdd,Odd,aEven,Even) : ' )
    PySwitchsCompare.append( '    sleep(1) ' )
    PySwitchsCompare.append( '    return aOdd  ' )
    SwitchsCompare = GraphLoopSwitchs_1.INode( 'SwitchsCompare' , PySwitchsCompare )
    SwitchsCompare.SetName( 'SwitchsCompare' )
    SwitchsCompare.SetAuthor( '' )
    SwitchsCompare.SetComment( 'Compute Node' )
    SwitchsCompare.Coords( 919 , 242 )
    ISwitchsCompareaOdd = SwitchsCompare.InPort( 'aOdd' , 'long' )
    ISwitchsCompareOdd = SwitchsCompare.InPort( 'Odd' , 'boolean' )
    ISwitchsCompareaEven = SwitchsCompare.InPort( 'aEven' , 'long' )
    ISwitchsCompareEven = SwitchsCompare.InPort( 'Even' , 'boolean' )
    ISwitchsCompareGate = SwitchsCompare.GetInPort( 'Gate' )
    OSwitchsComparea = SwitchsCompare.OutPort( 'a' , 'long' )
    OSwitchsCompareGate = SwitchsCompare.GetOutPort( 'Gate' )
    
    # Creation of Loop Nodes
    PyLoop = []
    PyLoop.append( 'def InitLoop(Index,Min,Max) :      ' )
    PyLoop.append( '	return Index,Min,Max     ' )
    PyMoreLoop = []
    PyMoreLoop.append( 'def MoreLoop(Index,Min,Max) :     ' )
    PyMoreLoop.append( '	if Index <= Max :   ' )
    PyMoreLoop.append( '		DoLoop = 1     ' )
    PyMoreLoop.append( '	else :     ' )
    PyMoreLoop.append( '		DoLoop = 0     ' )
    PyMoreLoop.append( '	return DoLoop,Index,Min,Max     ' )
    PyNextLoop = []
    PyNextLoop.append( 'def NextLoop(Index,Min,Max) :     ' )
    PyNextLoop.append( '	Index = Index + 1     ' )
    PyNextLoop.append( '	return Index,Min,Max     ' )
    Loop,EndOfLoop = GraphLoopSwitchs_1.LNode( 'InitLoop' , PyLoop , 'MoreLoop' , PyMoreLoop , 'NextLoop' , PyNextLoop )
    EndOfLoop.SetName( 'EndOfLoop' )
    EndOfLoop.SetAuthor( '' )
    EndOfLoop.SetComment( 'Compute Node' )
    EndOfLoop.Coords( 1102 , 282 )
    PyEndOfLoop = []
    EndOfLoop.SetPyFunction( '' , PyEndOfLoop )
    ILoopDoLoop = Loop.GetInPort( 'DoLoop' )
    ILoopIndex = Loop.InPort( 'Index' , 'long' )
    ILoopMin = Loop.InPort( 'Min' , 'long' )
    ILoopMax = Loop.InPort( 'Max' , 'long' )
    ILoopGate = Loop.GetInPort( 'Gate' )
    OLoopDoLoop = Loop.GetOutPort( 'DoLoop' )
    OLoopIndex = Loop.GetOutPort( 'Index' )
    OLoopMin = Loop.GetOutPort( 'Min' )
    OLoopMax = Loop.GetOutPort( 'Max' )
    IEndOfLoopDoLoop = EndOfLoop.GetInPort( 'DoLoop' )
    IEndOfLoopIndex = EndOfLoop.GetInPort( 'Index' )
    IEndOfLoopMin = EndOfLoop.GetInPort( 'Min' )
    IEndOfLoopMax = EndOfLoop.GetInPort( 'Max' )
    IEndOfLoopGate = EndOfLoop.GetInPort( 'Gate' )
    OEndOfLoopDoLoop = EndOfLoop.GetOutPort( 'DoLoop' )
    OEndOfLoopIndex = EndOfLoop.GetOutPort( 'Index' )
    OEndOfLoopMin = EndOfLoop.GetOutPort( 'Min' )
    OEndOfLoopMax = EndOfLoop.GetOutPort( 'Max' )
    OEndOfLoopGate = EndOfLoop.GetOutPort( 'Gate' )
    Loop.SetName( 'Loop' )
    Loop.SetAuthor( '' )
    Loop.SetComment( 'Compute Node' )
    Loop.Coords( 12 , 276 )
    
    # Creation of Switch Nodes
    PySwitchOdd = []
    PySwitchOdd.append( 'from time import *  ' )
    PySwitchOdd.append( 'def Switch(a) :    ' )
    PySwitchOdd.append( '    sleep(1)     ' )
    PySwitchOdd.append( '    return a & 1,1-(a&1),a     ' )
    SwitchOdd,EndOfSwitchOdd = GraphLoopSwitchs_1.SNode( 'Switch' , PySwitchOdd )
    EndOfSwitchOdd.SetName( 'EndOfSwitchOdd' )
    EndOfSwitchOdd.SetAuthor( '' )
    EndOfSwitchOdd.SetComment( 'Compute Node' )
    EndOfSwitchOdd.Coords( 711 , 161 )
    PyEndOfSwitchOdd = []
    PyEndOfSwitchOdd.append( 'from time import * ' )
    PyEndOfSwitchOdd.append( 'def EndOfSwitchOdd(a,Odd) : ' )
    PyEndOfSwitchOdd.append( '    sleep(1) ' )
    PyEndOfSwitchOdd.append( '    return a,Odd ' )
    EndOfSwitchOdd.SetPyFunction( 'EndOfSwitchOdd' , PyEndOfSwitchOdd )
    IEndOfSwitchOdda = EndOfSwitchOdd.InPort( 'a' , 'long' )
    IEndOfSwitchOddOdd = EndOfSwitchOdd.InPort( 'Odd' , 'boolean' )
    IEndOfSwitchOddDefault = EndOfSwitchOdd.GetInPort( 'Default' )
    OEndOfSwitchOdda = EndOfSwitchOdd.OutPort( 'a' , 'long' )
    OEndOfSwitchOddOdd = EndOfSwitchOdd.OutPort( 'Odd' , 'boolean' )
    OEndOfSwitchOddGate = EndOfSwitchOdd.GetOutPort( 'Gate' )
    SwitchOdd.SetName( 'SwitchOdd' )
    SwitchOdd.SetAuthor( '' )
    SwitchOdd.SetComment( 'Compute Node' )
    SwitchOdd.Coords( 240 , 141 )
    ISwitchOdda = SwitchOdd.InPort( 'a' , 'long' )
    ISwitchOddGate = SwitchOdd.GetInPort( 'Gate' )
    OSwitchOddOdd = SwitchOdd.OutPort( 'Odd' , 'long' )
    OSwitchOddEven = SwitchOdd.OutPort( 'Even' , 'int' )
    OSwitchOdda = SwitchOdd.OutPort( 'a' , 'int' )
    OSwitchOddDefault = SwitchOdd.GetOutPort( 'Default' )
    
    PySwitchEven = []
    PySwitchEven.append( 'from time import *   ' )
    PySwitchEven.append( 'def Switch(a) : ' )
    PySwitchEven.append( '    sleep(1)   ' )
    PySwitchEven.append( '    return a & 1,1-(a&1),a     ' )
    SwitchEven,EndOfSwitchEven = GraphLoopSwitchs_1.SNode( 'Switch' , PySwitchEven )
    EndOfSwitchEven.SetName( 'EndOfSwitchEven' )
    EndOfSwitchEven.SetAuthor( '' )
    EndOfSwitchEven.SetComment( 'Compute Node' )
    EndOfSwitchEven.Coords( 718 , 361 )
    PyEndOfSwitchEven = []
    PyEndOfSwitchEven.append( 'from time import * ' )
    PyEndOfSwitchEven.append( 'def EndOfSwitchEven(a,Even) : ' )
    PyEndOfSwitchEven.append( '    sleep(1) ' )
    PyEndOfSwitchEven.append( '    return a,Even ' )
    EndOfSwitchEven.SetPyFunction( 'EndOfSwitchEven' , PyEndOfSwitchEven )
    IEndOfSwitchEvena = EndOfSwitchEven.InPort( 'a' , 'long' )
    IEndOfSwitchEvenEven = EndOfSwitchEven.InPort( 'Even' , 'boolean' )
    IEndOfSwitchEvenDefault = EndOfSwitchEven.GetInPort( 'Default' )
    OEndOfSwitchEvena = EndOfSwitchEven.OutPort( 'a' , 'long' )
    OEndOfSwitchEvenEven = EndOfSwitchEven.OutPort( 'Even' , 'boolean' )
    OEndOfSwitchEvenGate = EndOfSwitchEven.GetOutPort( 'Gate' )
    SwitchEven.SetName( 'SwitchEven' )
    SwitchEven.SetAuthor( '' )
    SwitchEven.SetComment( 'Compute Node' )
    SwitchEven.Coords( 235 , 386 )
    ISwitchEvena = SwitchEven.InPort( 'a' , 'long' )
    ISwitchEvenGate = SwitchEven.GetInPort( 'Gate' )
    OSwitchEvenOdd = SwitchEven.OutPort( 'Odd' , 'long' )
    OSwitchEvenEven = SwitchEven.OutPort( 'Even' , 'int' )
    OSwitchEvena = SwitchEven.OutPort( 'a' , 'int' )
    OSwitchEvenDefault = SwitchEven.GetOutPort( 'Default' )
    
    # Creation of Links
    LIsOddaEndOfSwitchOdda = GraphLoopSwitchs_1.Link( OIsOdda , IEndOfSwitchOdda )
    
    LIsOddOddEndOfSwitchOddOdd = GraphLoopSwitchs_1.Link( OIsOddOdd , IEndOfSwitchOddOdd )
    
    LSwitchOddOddIsOddGate = GraphLoopSwitchs_1.Link( OSwitchOddOdd , IIsOddGate )
    
    LSwitchOddaIsOdda = GraphLoopSwitchs_1.Link( OSwitchOdda , IIsOdda )
    
    LSwitchOddDefaultEndOfSwitchOddDefault = GraphLoopSwitchs_1.Link( OSwitchOddDefault , IEndOfSwitchOddDefault )
    
    LEndOfSwitchOddaSwitchsCompareaOdd = GraphLoopSwitchs_1.Link( OEndOfSwitchOdda , ISwitchsCompareaOdd )
    
    LEndOfSwitchOddOddSwitchsCompareOdd = GraphLoopSwitchs_1.Link( OEndOfSwitchOddOdd , ISwitchsCompareOdd )
    
    LIsEvenaEndOfSwitchEvena = GraphLoopSwitchs_1.Link( OIsEvena , IEndOfSwitchEvena )
    
    LIsEvenEvenEndOfSwitchEvenEven = GraphLoopSwitchs_1.Link( OIsEvenEven , IEndOfSwitchEvenEven )
    
    LLoopIndexSwitchEvena = GraphLoopSwitchs_1.Link( OLoopIndex , ISwitchEvena )
    
    LLoopIndexSwitchOdda = GraphLoopSwitchs_1.Link( OLoopIndex , ISwitchOdda )
    
    LLoopMinEndOfLoopMin = GraphLoopSwitchs_1.Link( OLoopMin , IEndOfLoopMin )
    
    LLoopMaxEndOfLoopMax = GraphLoopSwitchs_1.Link( OLoopMax , IEndOfLoopMax )
    
    LSwitchEvenEvenIsEvenGate = GraphLoopSwitchs_1.Link( OSwitchEvenEven , IIsEvenGate )
    
    LSwitchEvenaIsEvena = GraphLoopSwitchs_1.Link( OSwitchEvena , IIsEvena )
    
    LSwitchEvenDefaultEndOfSwitchEvenDefault = GraphLoopSwitchs_1.Link( OSwitchEvenDefault , IEndOfSwitchEvenDefault )
    
    LEndOfSwitchEvenaSwitchsCompareaEven = GraphLoopSwitchs_1.Link( OEndOfSwitchEvena , ISwitchsCompareaEven )
    
    LEndOfSwitchEvenEvenSwitchsCompareEven = GraphLoopSwitchs_1.Link( OEndOfSwitchEvenEven , ISwitchsCompareEven )
    
    LSwitchsCompareaEndOfLoopIndex = GraphLoopSwitchs_1.Link( OSwitchsComparea , IEndOfLoopIndex )
    
    # Input datas
    ILoopIndex.Input( 0 )
    ILoopMin.Input( 0 )
    ILoopMax.Input( 23 )
    
    # Output Ports of the graph
    #OSwitchOddEven = SwitchOdd.GetOutPort( 'Even' )
    #OEndOfLoopIndex = EndOfLoop.GetOutPort( 'Index' )
    #OEndOfLoopMin = EndOfLoop.GetOutPort( 'Min' )
    #OEndOfLoopMax = EndOfLoop.GetOutPort( 'Max' )
    #OSwitchEvenOdd = SwitchEven.GetOutPort( 'Odd' )
    return GraphLoopSwitchs_1


GraphLoopSwitchs_1 = DefGraphLoopSwitchs_1()
