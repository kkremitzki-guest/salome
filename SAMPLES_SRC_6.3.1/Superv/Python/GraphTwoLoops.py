#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph GraphTwoLoops
#
from SuperV import *

# Graph creation of GraphTwoLoops
def DefGraphTwoLoops() :
    GraphTwoLoops = Graph( 'GraphTwoLoops' )
    GraphTwoLoops.SetName( 'GraphTwoLoops' )
    GraphTwoLoops.SetAuthor( 'JR' )
    GraphTwoLoops.SetComment( '' )
    GraphTwoLoops.Coords( 0 , 0 )
    
    # Creation of Factory Nodes
    
    # Creation of Loop Nodes
    PyLoop1 = []
    PyLoop1.append( 'def Loop1(Index,Max) :      ' )
    PyLoop1.append( '	return Index,Max        ' )
    PyLoop1.append( '' )
    PyMoreLoop1 = []
    PyMoreLoop1.append( 'import time  ' )
    PyMoreLoop1.append( 'def More(Index,Max) :   ' )
    PyMoreLoop1.append( '	time.sleep(2)  ' )
    PyMoreLoop1.append( '	DoLoop = 0       ' )
    PyMoreLoop1.append( '	if Index < Max :       ' )
    PyMoreLoop1.append( '		DoLoop = 1       ' )
    PyMoreLoop1.append( '	return DoLoop,Index,Max       ' )
    PyMoreLoop1.append( '' )
    PyNextLoop1 = []
    PyNextLoop1.append( 'def Next(Index,Max) :       ' )
    PyNextLoop1.append( '	Index = Index + 1       ' )
    PyNextLoop1.append( '	return Index,Max       ' )
    PyNextLoop1.append( '' )
    Loop1,EndLoop1 = GraphTwoLoops.LNode( 'Loop1' , PyLoop1 , 'More' , PyMoreLoop1 , 'Next' , PyNextLoop1 )
    EndLoop1.SetName( 'EndLoop1' )
    EndLoop1.SetAuthor( '' )
    EndLoop1.SetComment( 'Compute Node' )
    EndLoop1.Coords( 634 , 177 )
    PyEndLoop1 = []
    EndLoop1.SetPyFunction( 'EndLoop1' , PyEndLoop1 )
    ILoop1DoLoop = Loop1.GetInPort( 'DoLoop' )
    ILoop1Index = Loop1.InPort( 'Index' , 'long' )
    ILoop1Max = Loop1.InPort( 'Max' , 'long' )
    ILoop1Gate = Loop1.GetInPort( 'Gate' )
    OLoop1DoLoop = Loop1.GetOutPort( 'DoLoop' )
    OLoop1Index = Loop1.GetOutPort( 'Index' )
    OLoop1Max = Loop1.GetOutPort( 'Max' )
    IEndLoop1DoLoop = EndLoop1.GetInPort( 'DoLoop' )
    IEndLoop1Index = EndLoop1.GetInPort( 'Index' )
    IEndLoop1Max = EndLoop1.GetInPort( 'Max' )
    IEndLoop1Gate = EndLoop1.GetInPort( 'Gate' )
    OEndLoop1DoLoop = EndLoop1.GetOutPort( 'DoLoop' )
    OEndLoop1Index = EndLoop1.GetOutPort( 'Index' )
    OEndLoop1Max = EndLoop1.GetOutPort( 'Max' )
    OEndLoop1Gate = EndLoop1.GetOutPort( 'Gate' )
    Loop1.SetName( 'Loop1' )
    Loop1.SetAuthor( '' )
    Loop1.SetComment( 'Compute Node' )
    Loop1.Coords( 7 , 175 )
    
    PyLoop = []
    PyMoreLoop = []
    PyMoreLoop.append( '' )
    PyNextLoop = []
    PyNextLoop.append( 'def Next(Index,Max) : ' )
    PyNextLoop.append( '	Index = Index + 1 ' )
    PyNextLoop.append( '	return Index,Max ' )
    PyNextLoop.append( '' )
    Loop,EndLoop = GraphTwoLoops.LNode( '' , PyLoop , '' , PyMoreLoop , 'Next' , PyNextLoop )
    EndLoop.SetName( 'EndLoop' )
    EndLoop.SetAuthor( '' )
    EndLoop.SetComment( 'Compute Node' )
    EndLoop.Coords( 426 , 180 )
    PyEndLoop = []
    PyEndLoop.append( 'import time ' )
    PyEndLoop.append( 'def EndLoop(DoLoop,Index,Max) :  ' )
    PyEndLoop.append( '	time.sleep(1) ' )
    PyEndLoop.append( '	DoLoop = 0  ' )
    PyEndLoop.append( '	if Index < Max :  ' )
    PyEndLoop.append( '		DoLoop = 1  ' )
    PyEndLoop.append( '	return DoLoop,Index,Max  ' )
    PyEndLoop.append( '' )
    EndLoop.SetPyFunction( 'EndLoop' , PyEndLoop )
    ILoopDoLoop = Loop.GetInPort( 'DoLoop' )
    ILoopIndex = Loop.InPort( 'Index' , 'long' )
    ILoopMax = Loop.InPort( 'Max' , 'long' )
    ILoopGate = Loop.GetInPort( 'Gate' )
    OLoopDoLoop = Loop.GetOutPort( 'DoLoop' )
    OLoopIndex = Loop.GetOutPort( 'Index' )
    OLoopMax = Loop.GetOutPort( 'Max' )
    IEndLoopDoLoop = EndLoop.GetInPort( 'DoLoop' )
    IEndLoopIndex = EndLoop.GetInPort( 'Index' )
    IEndLoopMax = EndLoop.GetInPort( 'Max' )
    IEndLoopGate = EndLoop.GetInPort( 'Gate' )
    OEndLoopDoLoop = EndLoop.GetOutPort( 'DoLoop' )
    OEndLoopIndex = EndLoop.GetOutPort( 'Index' )
    OEndLoopMax = EndLoop.GetOutPort( 'Max' )
    OEndLoopGate = EndLoop.GetOutPort( 'Gate' )
    Loop.SetName( 'Loop' )
    Loop.SetAuthor( '' )
    Loop.SetComment( 'Compute Node' )
    Loop.Coords( 208 , 179 )
    
    # Creation of Links
    LLoop1IndexEndLoop1Index = GraphTwoLoops.Link( OLoop1Index , IEndLoop1Index )
    
    LLoop1MaxEndLoop1Max = GraphTwoLoops.Link( OLoop1Max , IEndLoop1Max )
    
    LLoop1GateLoopGate = GraphTwoLoops.Link( OLoop1Gate , ILoopGate )
    
    LLoopIndexEndLoopIndex = GraphTwoLoops.Link( OLoopIndex , IEndLoopIndex )
    
    LLoopMaxEndLoopMax = GraphTwoLoops.Link( OLoopMax , IEndLoopMax )
    
    LEndLoopGateEndLoop1Gate = GraphTwoLoops.Link( OEndLoopGate , IEndLoop1Gate )
    
    # Input datas
    ILoop1Index.Input( 5 )
    ILoop1Max.Input( 15 )
    ILoopIndex.Input( 2 )
    ILoopMax.Input( 20 )
    
    # Output Ports of the graph
    #OEndLoop1Index = EndLoop1.GetOutPort( 'Index' )
    #OEndLoop1Max = EndLoop1.GetOutPort( 'Max' )
    #OEndLoopIndex = EndLoop.GetOutPort( 'Index' )
    #OEndLoopMax = EndLoop.GetOutPort( 'Max' )
    return GraphTwoLoops


GraphTwoLoops = DefGraphTwoLoops()
