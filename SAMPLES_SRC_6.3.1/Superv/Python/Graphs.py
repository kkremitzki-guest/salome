#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph Graphs
#
from SuperV import *
# Graph creation 
Graphs = Graph( 'Graphs' )
Graphs.SetName( 'Graphs' )
Graphs.SetAuthor( '' )
Graphs.SetComment( '' )
Graphs.Coords( 0 , 0 )

# Creation of Factory Nodes

Add = Graphs.FNode( 'AddComponent' , 'AddComponent' , 'Add' )
Add.SetName( 'Add' )
Add.SetAuthor( '' )
Add.SetContainer( 'localhost/FactoryServer' )
Add.SetComment( 'Add from AddComponent' )
Add.Coords( 30 , 8 )

Sub = Graphs.FNode( 'SubComponent' , 'SubComponent' , 'Sub' )
Sub.SetName( 'Sub' )
Sub.SetAuthor( '' )
Sub.SetContainer( 'localhost/FactoryServer' )
Sub.SetComment( 'Sub from SubComponent' )
Sub.Coords( 241 , 38 )

Mul = Graphs.FNode( 'MulComponent' , 'MulComponent' , 'Mul' )
Mul.SetName( 'Mul' )
Mul.SetAuthor( '' )
Mul.SetContainer( 'localhost/FactoryServer' )
Mul.SetComment( 'Mul from MulComponent' )
Mul.Coords( 439 , 39 )

Div = Graphs.FNode( 'DivComponent' , 'DivComponent' , 'Div' )
Div.SetName( 'Div' )
Div.SetAuthor( '' )
Div.SetContainer( 'localhost/FactoryServer' )
Div.SetComment( 'Div from DivComponent' )
Div.Coords( 633 , 120 )

Add_1 = Graphs.FNode( 'AddComponent' , 'AddComponent' , 'Add' )
Add_1.SetName( 'Add_1' )
Add_1.SetAuthor( '' )
Add_1.SetContainer( 'localhost/FactoryServer' )
Add_1.SetComment( 'Add from AddComponent' )
Add_1.Coords( 13 , 297 )

Sub_1 = Graphs.FNode( 'SubComponent' , 'SubComponent' , 'Sub' )
Sub_1.SetName( 'Sub_1' )
Sub_1.SetAuthor( '' )
Sub_1.SetContainer( 'localhost/FactoryServer' )
Sub_1.SetComment( 'Sub from SubComponent' )
Sub_1.Coords( 235 , 217 )

Mul_1 = Graphs.FNode( 'MulComponent' , 'MulComponent' , 'Mul' )
Mul_1.SetName( 'Mul_1' )
Mul_1.SetAuthor( '' )
Mul_1.SetContainer( 'localhost/FactoryServer' )
Mul_1.SetComment( 'Mul from MulComponent' )
Mul_1.Coords( 423 , 375 )

Div_1 = Graphs.FNode( 'DivComponent' , 'DivComponent' , 'Div' )
Div_1.SetName( 'Div_1' )
Div_1.SetAuthor( '' )
Div_1.SetContainer( 'localhost/FactoryServer' )
Div_1.SetComment( 'Div from DivComponent' )
Div_1.Coords( 630 , 284 )

# Creation of Links
Addz = Add.Port( 'z' )
Subx = Graphs.Link( Addz , Sub.Port( 'x' ) )

Subz = Sub.Port( 'z' )
Divx = Graphs.Link( Subz , Div.Port( 'x' ) )
Divx.AddCoord( 1 , 426 , 201 )
Divx.AddCoord( 2 , 425 , 119 )

Mulx = Graphs.Link( Subz , Mul.Port( 'x' ) )

Mulz = Mul.Port( 'z' )
Divy = Graphs.Link( Mulz , Div.Port( 'y' ) )
Divy.AddCoord( 1 , 619 , 230 )
Divy.AddCoord( 2 , 620 , 120 )

Add_1FuncValue = Add_1.Port( 'FuncValue' )
Mul_1x = Graphs.Link( Add_1FuncValue , Mul_1.Port( 'x' ) )
Mul_1x.AddCoord( 1 , 396 , 455 )
Mul_1x.AddCoord( 2 , 395 , 378 )

Sub_1x = Graphs.Link( Add_1FuncValue , Sub_1.Port( 'x' ) )
Sub_1x.AddCoord( 1 , 215 , 297 )
Sub_1x.AddCoord( 2 , 215 , 378 )

Add_1z = Add_1.Port( 'z' )
Sub_1y = Graphs.Link( Add_1z , Sub_1.Port( 'y' ) )
Sub_1y.AddCoord( 1 , 197 , 327 )
Sub_1y.AddCoord( 2 , 196 , 406 )

Sub_1z = Sub_1.Port( 'z' )
Div_1x = Graphs.Link( Sub_1z , Div_1.Port( 'x' ) )
Div_1x.AddCoord( 1 , 604 , 364 )
Div_1x.AddCoord( 2 , 603 , 297 )

Mul_1z = Mul_1.Port( 'z' )
Div_1y = Graphs.Link( Mul_1z , Div_1.Port( 'y' ) )
Div_1y.AddCoord( 1 , 610 , 394 )
Div_1y.AddCoord( 2 , 610 , 455 )

# Creation of Input datas
Addx = Add.Input( 'x' , 3)
Addy = Add.Input( 'y' , 5)
Suby = Sub.Input( 'y' , 7)
Muly = Mul.Input( 'y' , 11)
Add_1x = Add_1.Input( 'x' , 1)
Add_1y = Add_1.Input( 'y' , 2)
Mul_1y = Mul_1.Input( 'y' , 4)

# Creation of Output variables
AddFuncValue = Add.Port( 'FuncValue' )
Divz = Div.Port( 'z' )
Div_1z = Div_1.Port( 'z' )

Graphs.Run()

Graphs.DoneW()

Graphs.State()

Graphs.PrintPorts()

