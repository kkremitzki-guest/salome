#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph GraphEditSwitchs
#
from SuperV import *

# Graph creation of GraphEditSwitchs
def DefGraphEditSwitchs() :
    GraphEditSwitchs = Graph( 'GraphEditSwitchs' )
    GraphEditSwitchs.SetName( 'GraphEditSwitchs' )
    GraphEditSwitchs.SetAuthor( '' )
    GraphEditSwitchs.SetComment( '' )
    GraphEditSwitchs.Coords( 0 , 0 )
    
    # Creation of Factory Nodes
    
    # Creation of InLine Nodes
    PyIsOdd = []
    PyIsOdd.append( 'from time import *   ' )
    PyIsOdd.append( 'def IsOdd(a) :       ' )
    PyIsOdd.append( '    print a,"IsOdd (GraphSwitchs)"      ' )
    PyIsOdd.append( '    sleep( 1 )   ' )
    PyIsOdd.append( '    return a     ' )
    IsOdd = GraphEditSwitchs.INode( 'IsOdd' , PyIsOdd )
    IsOdd.SetName( 'IsOdd' )
    IsOdd.SetAuthor( '' )
    IsOdd.SetComment( 'Python function' )
    IsOdd.Coords( 388 , 50 )
    IIsOdda = IsOdd.InPort( 'a' , 'long' )
    IIsOddGate = IsOdd.GetInPort( 'Gate' )
    OIsOdda = IsOdd.OutPort( 'a' , 'long' )
    OIsOddGate = IsOdd.GetOutPort( 'Gate' )
    
    PyIsOdd_1 = []
    PyIsOdd_1.append( 'from time import *   ' )
    PyIsOdd_1.append( 'def IsOdd(a) :       ' )
    PyIsOdd_1.append( '    print a,"IsOdd_1 (GraphSwitchs)"      ' )
    PyIsOdd_1.append( '    sleep( 1 )   ' )
    PyIsOdd_1.append( '    return a     ' )
    IsOdd_1 = GraphEditSwitchs.INode( 'IsOdd' , PyIsOdd_1 )
    IsOdd_1.SetName( 'IsOdd_1' )
    IsOdd_1.SetAuthor( '' )
    IsOdd_1.SetComment( 'Python function' )
    IsOdd_1.Coords( 419 , 308 )
    IIsOdd_1a = IsOdd_1.InPort( 'a' , 'long' )
    IIsOdd_1Gate = IsOdd_1.GetInPort( 'Gate' )
    OIsOdd_1a = IsOdd_1.OutPort( 'a' , 'long' )
    OIsOdd_1Gate = IsOdd_1.GetOutPort( 'Gate' )
    
    # Creation of Loop Nodes
    PyInitLoop = []
    PyInitLoop.append( 'def InitLoop(Index,Min,Max) :      ' )
    PyInitLoop.append( '	return Index,Min,Max     ' )
    PyMoreInitLoop = []
    PyMoreInitLoop.append( 'def MoreLoop(Index,Min,Max) :     ' )
    PyMoreInitLoop.append( '	if Index <= Max :   ' )
    PyMoreInitLoop.append( '		DoLoop = 1     ' )
    PyMoreInitLoop.append( '	else :     ' )
    PyMoreInitLoop.append( '		DoLoop = 0     ' )
    PyMoreInitLoop.append( '	return DoLoop,Index,Min,Max     ' )
    PyNextInitLoop = []
    PyNextInitLoop.append( 'def NextLoop(Index,Min,Max) :     ' )
    PyNextInitLoop.append( '	Index = Index + 1     ' )
    PyNextInitLoop.append( '	return Index,Min,Max     ' )
    InitLoop,EndOfInitLoop = GraphEditSwitchs.LNode( 'InitLoop' , PyInitLoop , 'MoreLoop' , PyMoreInitLoop , 'NextLoop' , PyNextInitLoop )
    EndOfInitLoop.SetName( 'EndOfInitLoop' )
    EndOfInitLoop.SetAuthor( '' )
    EndOfInitLoop.SetComment( 'Compute Node' )
    EndOfInitLoop.Coords( 783 , 381 )
    PyEndOfInitLoop = []
    EndOfInitLoop.SetPyFunction( '' , PyEndOfInitLoop )
    IInitLoopDoLoop = InitLoop.GetInPort( 'DoLoop' )
    IInitLoopIndex = InitLoop.InPort( 'Index' , 'long' )
    IInitLoopMin = InitLoop.InPort( 'Min' , 'long' )
    IInitLoopMax = InitLoop.InPort( 'Max' , 'long' )
    IInitLoopGate = InitLoop.GetInPort( 'Gate' )
    OInitLoopDoLoop = InitLoop.GetOutPort( 'DoLoop' )
    OInitLoopIndex = InitLoop.GetOutPort( 'Index' )
    OInitLoopMin = InitLoop.GetOutPort( 'Min' )
    OInitLoopMax = InitLoop.GetOutPort( 'Max' )
    IEndOfInitLoopDoLoop = EndOfInitLoop.GetInPort( 'DoLoop' )
    IEndOfInitLoopIndex = EndOfInitLoop.GetInPort( 'Index' )
    IEndOfInitLoopMin = EndOfInitLoop.GetInPort( 'Min' )
    IEndOfInitLoopMax = EndOfInitLoop.GetInPort( 'Max' )
    IEndOfInitLoopGate = EndOfInitLoop.GetInPort( 'Gate' )
    OEndOfInitLoopDoLoop = EndOfInitLoop.GetOutPort( 'DoLoop' )
    OEndOfInitLoopGate = EndOfInitLoop.GetOutPort( 'Gate' )
    OEndOfInitLoopIndex = EndOfInitLoop.GetOutPort( 'Index' )
    OEndOfInitLoopMin = EndOfInitLoop.GetOutPort( 'Min' )
    OEndOfInitLoopMax = EndOfInitLoop.GetOutPort( 'Max' )
    InitLoop.SetName( 'InitLoop' )
    InitLoop.SetAuthor( '' )
    InitLoop.SetComment( 'Compute Node' )
    InitLoop.Coords( 10 , 388 )
    
    # Creation of Switch Nodes
    PySwitch = []
    PySwitch.append( 'from time import * ' )
    PySwitch.append( 'def Switch(a) :   ' )
    PySwitch.append( '    if ( a & 1 ) == 0 : ' )
    PySwitch.append( '        sleep(1)    ' )
    PySwitch.append( '    return a & 1,1-(a&1),a    ' )
    Switch,EndOfSwitch = GraphEditSwitchs.SNode( 'Switch' , PySwitch )
    EndOfSwitch.SetName( 'EndOfSwitch' )
    EndOfSwitch.SetAuthor( '' )
    EndOfSwitch.SetComment( 'Compute Node' )
    EndOfSwitch.Coords( 587 , 126 )
    PyEndOfSwitch = []
    EndOfSwitch.SetPyFunction( '' , PyEndOfSwitch )
    IEndOfSwitcha = EndOfSwitch.InPort( 'a' , 'long' )
    IEndOfSwitchDefault = EndOfSwitch.GetInPort( 'Default' )
    OEndOfSwitcha = EndOfSwitch.OutPort( 'a' , 'long' )
    OEndOfSwitchGate = EndOfSwitch.GetOutPort( 'Gate' )
    Switch.SetName( 'Switch' )
    Switch.SetAuthor( '' )
    Switch.SetComment( 'Compute Node' )
    Switch.Coords( 186 , 130 )
    ISwitcha = Switch.InPort( 'a' , 'long' )
    ISwitchGate = Switch.GetInPort( 'Gate' )
    OSwitchOdd = Switch.OutPort( 'Odd' , 'long' )
    OSwitchEven = Switch.OutPort( 'Even' , 'int' )
    OSwitcha = Switch.OutPort( 'a' , 'int' )
    OSwitchDefault = Switch.GetOutPort( 'Default' )
    
    PySwitch_1 = []
    PySwitch_1.append( 'from time import *  ' )
    PySwitch_1.append( 'def Switch(a) :    ' )
    PySwitch_1.append( '    if ( a & 1 ) == 0 :  ' )
    PySwitch_1.append( '        sleep(1)  ' )
    PySwitch_1.append( '    return a & 1,1-(a&1),a    ' )
    Switch_1,EndSwitch = GraphEditSwitchs.SNode( 'Switch' , PySwitch_1 )
    EndSwitch.SetName( 'EndSwitch' )
    EndSwitch.SetAuthor( '' )
    EndSwitch.SetComment( 'Compute Node' )
    EndSwitch.Coords( 605 , 382 )
    PyEndSwitch = []
    EndSwitch.SetPyFunction( '' , PyEndSwitch )
    IEndSwitcha = EndSwitch.InPort( 'a' , 'long' )
    IEndSwitchDefault = EndSwitch.GetInPort( 'Default' )
    OEndSwitcha = EndSwitch.OutPort( 'a' , 'long' )
    OEndSwitchGate = EndSwitch.GetOutPort( 'Gate' )
    Switch_1.SetName( 'Switch_1' )
    Switch_1.SetAuthor( '' )
    Switch_1.SetComment( 'Compute Node' )
    Switch_1.Coords( 193 , 388 )
    ISwitch_1a = Switch_1.InPort( 'a' , 'long' )
    ISwitch_1Gate = Switch_1.GetInPort( 'Gate' )
    OSwitch_1Odd = Switch_1.OutPort( 'Odd' , 'long' )
    OSwitch_1Even = Switch_1.OutPort( 'Even' , 'int' )
    OSwitch_1a = Switch_1.OutPort( 'a' , 'int' )
    OSwitch_1Default = Switch_1.GetOutPort( 'Default' )
    
    # Creation of Links
    LIsOddaEndOfSwitcha = GraphEditSwitchs.Link( OIsOdda , IEndOfSwitcha )
    LIsOddaEndOfSwitcha.AddCoord( 1 , 573 , 160 )
    LIsOddaEndOfSwitcha.AddCoord( 2 , 572 , 130 )
    
    LIsOdd_1aEndSwitcha = GraphEditSwitchs.Link( OIsOdd_1a , IEndSwitcha )
    LIsOdd_1aEndSwitcha.AddCoord( 1 , 599 , 416 )
    LIsOdd_1aEndSwitcha.AddCoord( 2 , 598 , 389 )
    
    LInitLoopIndexSwitch_1a = GraphEditSwitchs.Link( OInitLoopIndex , ISwitch_1a )
    
    LInitLoopIndexSwitcha = GraphEditSwitchs.Link( OInitLoopIndex , ISwitcha )
    
    LInitLoopMinEndOfInitLoopMin = GraphEditSwitchs.Link( OInitLoopMin , IEndOfInitLoopMin )
    
    LInitLoopMaxEndOfInitLoopMax = GraphEditSwitchs.Link( OInitLoopMax , IEndOfInitLoopMax )
    
    LSwitchOddIsOddGate = GraphEditSwitchs.Link( OSwitchOdd , IIsOddGate )
    
    LSwitchaIsOdda = GraphEditSwitchs.Link( OSwitcha , IIsOdda )
    LSwitchaIsOdda.AddCoord( 1 , 365 , 129 )
    LSwitchaIsOdda.AddCoord( 2 , 365 , 222 )
    
    LSwitchDefaultEndOfSwitchDefault = GraphEditSwitchs.Link( OSwitchDefault , IEndOfSwitchDefault )
    LSwitchDefaultEndOfSwitchDefault.AddCoord( 1 , 572 , 194 )
    LSwitchDefaultEndOfSwitchDefault.AddCoord( 2 , 571 , 257 )
    
    LSwitch_1OddIsOdd_1Gate = GraphEditSwitchs.Link( OSwitch_1Odd , IIsOdd_1Gate )
    
    LSwitch_1EvenEndSwitchDefault = GraphEditSwitchs.Link( OSwitch_1Even , IEndSwitchDefault )
    
    LSwitch_1aIsOdd_1a = GraphEditSwitchs.Link( OSwitch_1a , IIsOdd_1a )
    LSwitch_1aIsOdd_1a.AddCoord( 1 , 379 , 387 )
    LSwitch_1aIsOdd_1a.AddCoord( 2 , 378 , 481 )
    
    # Input datas
    IInitLoopIndex.Input( 0 )
    IInitLoopMin.Input( 0 )
    IInitLoopMax.Input( 20 )
    
    # Input Ports of the graph
    #IEndOfInitLoopIndex = EndOfInitLoop.GetInPort( 'Index' )
    
    # Output Ports of the graph
    #OEndOfInitLoopIndex = EndOfInitLoop.GetOutPort( 'Index' )
    #OEndOfInitLoopMin = EndOfInitLoop.GetOutPort( 'Min' )
    #OEndOfInitLoopMax = EndOfInitLoop.GetOutPort( 'Max' )
    #OSwitchEven = Switch.GetOutPort( 'Even' )
    #OEndOfSwitcha = EndOfSwitch.GetOutPort( 'a' )
    #OEndSwitcha = EndSwitch.GetOutPort( 'a' )
    return GraphEditSwitchs


GraphEditSwitchs = DefGraphEditSwitchs()
