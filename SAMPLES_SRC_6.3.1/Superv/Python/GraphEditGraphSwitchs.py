#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
from GraphSwitchs import *

GraphSwitchs.SetName('GraphEditSwitchs')
GraphEditSwitchs = GraphSwitchs

exec GraphEditSwitchs.ListNodes()
InitLoopSwitch.destroy()

aPyFile = os.getenv('DATA_DIR') + '/Superv/Python/GraphEditGraphSwitchs_1.py'
GraphEditSwitchs.Export( aPyFile )

InitLoop.SetName('InitLoopSwitch')
InitLoopSwitch = InitLoop
EndOfInitLoop.SetName('EndOfInitLoopSwitch')
EndOfInitLoopSwitch = EndOfInitLoop

exec InitLoopSwitch.ListPorts()
exec Switch.ListPorts()

GraphEditSwitchs.Link( OInitLoopSwitchIndex , ISwitcha )

exec EndOfInitLoopSwitch.ListPorts()
IEndOfInitLoopSwitchIndex.Link().destroy()

aPyFile = os.getenv('DATA_DIR') + '/Superv/Python/GraphEditGraphSwitchs_2.py'
GraphEditSwitchs.Export( aPyFile )

exec EndOfSwitch.ListPorts()
GraphEditSwitchs.Link(OEndOfSwitcha,IEndOfInitLoopSwitchIndex)

exec EndSwitch.ListPorts()
GraphEditSwitchs.Link(OEndSwitchGate,IEndOfInitLoopSwitchGate)

aPyFile = os.getenv('DATA_DIR') + '/Superv/Python/GraphEditGraphSwitchs_3.py'
GraphEditSwitchs.Export( aPyFile )

IsOdd_1.SetName('IsEven')
IsEven = IsOdd_1

exec IsEven.ListPorts()
IIsEvena.Link().destroy()

IIsEvenGate.Link().destroy()

exec Switch_1.ListPorts()
GraphEditSwitchs.Link(OSwitch_1Even,IIsEvenGate)

aPyFile = os.getenv('DATA_DIR') + '/Superv/Python/GraphEditGraphSwitchs_4.py'
GraphEditSwitchs.Export( aPyFile )

GraphEditSwitchs.PrintLinks()

L = GraphEditSwitchs.Link(OSwitch_1Even,IEndSwitchDefault)
L.destroy()

aPyFile = os.getenv('DATA_DIR') + '/Superv/Python/GraphEditGraphSwitchs_5.py'
GraphEditSwitchs.Export( aPyFile )

GraphEditSwitchs.Link(OSwitch_1Default,IEndSwitchDefault)

GraphEditSwitchs.Link(OSwitch_1a,IIsEvena)

GraphEditSwitchs.Export( aPyFile)

Switch_1.SetName('')

from SuperV import *

aPyFile = os.getenv('DATA_DIR') + '/Superv/Python/GraphEditGraphSwitchs_3.py'
GraphEditSwitchs = Graph( aPyFile )

