#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph GraphSIGSEGV
#
from SuperV import *

# Graph creation of GraphSIGSEGV
def DefGraphSIGSEGV() :
    GraphSIGSEGV = Graph( 'GraphSIGSEGV' )
    GraphSIGSEGV.SetName( 'GraphSIGSEGV' )
    GraphSIGSEGV.SetAuthor( 'JR' )
    GraphSIGSEGV.SetComment( '' )
    GraphSIGSEGV.Coords( 0 , 0 )
    
    # Creation of Factory Nodes
    
    SIGSEGVfunc = GraphSIGSEGV.FNode( 'SIGNALSComponent' , 'SIGNALSComponent' , 'SIGSEGVfunc' )
    SIGSEGVfunc.SetName( 'SIGSEGVfunc' )
    SIGSEGVfunc.SetAuthor( '' )
    SIGSEGVfunc.SetContainer( 'localhost/FactoryServer' )
    SIGSEGVfunc.SetComment( 'SIGSEGVfunc from SIGNALSComponent' )
    SIGSEGVfunc.Coords( 152 , 197 )
    ISIGSEGVfuncGate = SIGSEGVfunc.GetInPort( 'Gate' )
    OSIGSEGVfuncreturn = SIGSEGVfunc.GetOutPort( 'return' )
    OSIGSEGVfuncGate = SIGSEGVfunc.GetOutPort( 'Gate' )
    
    # Output Ports of the graph
    #OSIGSEGVfuncreturn = SIGSEGVfunc.GetOutPort( 'return' )
    return GraphSIGSEGV


GraphSIGSEGV = DefGraphSIGSEGV()
