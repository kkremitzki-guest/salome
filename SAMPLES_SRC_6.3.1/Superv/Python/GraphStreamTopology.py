#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph GraphStreamTopology
#
from SuperV import *
# Graph creation 
GraphStreamTopology = StreamGraph( 'GraphStreamTopology' )
GraphStreamTopology.SetStreamParams( 300 , SUPERV.WithoutTrace , 0 )
GraphStreamTopology.SetName( 'GraphStreamTopology' )
GraphStreamTopology.SetAuthor( 'JR' )
GraphStreamTopology.SetComment( 'Test of SubStreamGraphs of a StreamGraph' )
GraphStreamTopology.Coords( 0 , 0 )

# Creation of Factory Nodes

# Creation of InLine Nodes
PyNode_A_1 = []
PyNode_A_1.append( 'def Node_A_1() :        ' )
PyNode_A_1.append( '    return 1      ' )
Node_A_1 = GraphStreamTopology.INode( 'Node_A_1' , PyNode_A_1 )
Node_A_1.SetName( 'Node_A_1' )
Node_A_1.SetAuthor( '' )
Node_A_1.SetComment( 'Python function' )
Node_A_1.Coords( 32 , 65 )
INode_A_1Gate = Node_A_1.GetInPort( 'Gate' )
ONode_A_1a_1 = Node_A_1.OutPort( 'a_1' , 'long' )
ONode_A_1Gate = Node_A_1.GetOutPort( 'Gate' )
INode_A_1istream_A_1_1 = Node_A_1.InStreamPort( 'istream_A_1_1' , SALOME_ModuleCatalog.DATASTREAM_INTEGER , SALOME_ModuleCatalog.DATASTREAM_TEMPORAL )
INode_A_1istream_A_1_1.SetParams( SUPERV.TI , SUPERV.L1 , SUPERV.EXTRANULL )
INode_A_1istream_A_1_2 = Node_A_1.InStreamPort( 'istream_A_1_2' , SALOME_ModuleCatalog.DATASTREAM_INTEGER , SALOME_ModuleCatalog.DATASTREAM_TEMPORAL )
INode_A_1istream_A_1_2.SetParams( SUPERV.TI , SUPERV.L1 , SUPERV.EXTRANULL )
INode_A_1istream_A_1_3 = Node_A_1.InStreamPort( 'istream_A_1_3' , SALOME_ModuleCatalog.DATASTREAM_INTEGER , SALOME_ModuleCatalog.DATASTREAM_TEMPORAL )
INode_A_1istream_A_1_3.SetParams( SUPERV.TI , SUPERV.L1 , SUPERV.EXTRANULL )

PyNode_A_2 = []
PyNode_A_2.append( 'def Node_A_2() :        ' )
PyNode_A_2.append( '    return 1      ' )
Node_A_2 = GraphStreamTopology.INode( 'Node_A_2' , PyNode_A_2 )
Node_A_2.SetName( 'Node_A_2' )
Node_A_2.SetAuthor( '' )
Node_A_2.SetComment( 'Python function' )
Node_A_2.Coords( 31 , 336 )
INode_A_2Gate = Node_A_2.GetInPort( 'Gate' )
ONode_A_2a_2 = Node_A_2.OutPort( 'a_2' , 'long' )
ONode_A_2Gate = Node_A_2.GetOutPort( 'Gate' )
INode_A_2istream_A_2_1 = Node_A_2.InStreamPort( 'istream_A_2_1' , SALOME_ModuleCatalog.DATASTREAM_INTEGER , SALOME_ModuleCatalog.DATASTREAM_TEMPORAL )
INode_A_2istream_A_2_1.SetParams( SUPERV.TI , SUPERV.L1 , SUPERV.EXTRANULL )
ONode_A_2ostream_A_2_1 = Node_A_2.OutStreamPort( 'ostream_A_2_1' , SALOME_ModuleCatalog.DATASTREAM_INTEGER , SALOME_ModuleCatalog.DATASTREAM_TEMPORAL )
ONode_A_2ostream_A_2_1.SetNumberOfValues( 0 )

PyNode_B_1 = []
PyNode_B_1.append( 'def Node_B_1( n ) :        ' )
PyNode_B_1.append( '    return n      ' )
Node_B_1 = GraphStreamTopology.INode( 'Node_B_1' , PyNode_B_1 )
Node_B_1.SetName( 'Node_B_1' )
Node_B_1.SetAuthor( '' )
Node_B_1.SetComment( 'Python function' )
Node_B_1.Coords( 254 , 64 )
INode_B_1b_1 = Node_B_1.InPort( 'b_1' , 'long' )
INode_B_1Gate = Node_B_1.GetInPort( 'Gate' )
ONode_B_1b_1 = Node_B_1.OutPort( 'b_1' , 'long' )
ONode_B_1Gate = Node_B_1.GetOutPort( 'Gate' )
INode_B_1istream_B_1_1 = Node_B_1.InStreamPort( 'istream_B_1_1' , SALOME_ModuleCatalog.DATASTREAM_INTEGER , SALOME_ModuleCatalog.DATASTREAM_TEMPORAL )
INode_B_1istream_B_1_1.SetParams( SUPERV.TI , SUPERV.L1 , SUPERV.EXTRANULL )

PyNode_B_2 = []
PyNode_B_2.append( 'def Node_B_2( n ) :        ' )
PyNode_B_2.append( '    return n      ' )
Node_B_2 = GraphStreamTopology.INode( 'Node_B_2' , PyNode_B_2 )
Node_B_2.SetName( 'Node_B_2' )
Node_B_2.SetAuthor( '' )
Node_B_2.SetComment( 'Python function' )
Node_B_2.Coords( 255 , 336 )
INode_B_2b_2 = Node_B_2.InPort( 'b_2' , 'long' )
INode_B_2Gate = Node_B_2.GetInPort( 'Gate' )
ONode_B_2b_2 = Node_B_2.OutPort( 'b_2' , 'long' )
ONode_B_2Gate = Node_B_2.GetOutPort( 'Gate' )
INode_B_2istream_B_2_1 = Node_B_2.InStreamPort( 'istream_B_2_1' , SALOME_ModuleCatalog.DATASTREAM_INTEGER , SALOME_ModuleCatalog.DATASTREAM_TEMPORAL )
INode_B_2istream_B_2_1.SetParams( SUPERV.TI , SUPERV.L1 , SUPERV.EXTRANULL )
ONode_B_2ostream_B_2_1 = Node_B_2.OutStreamPort( 'ostream_B_2_1' , SALOME_ModuleCatalog.DATASTREAM_INTEGER , SALOME_ModuleCatalog.DATASTREAM_TEMPORAL )
ONode_B_2ostream_B_2_1.SetNumberOfValues( 0 )
ONode_B_2ostream_B_2_2 = Node_B_2.OutStreamPort( 'ostream_B_2_2' , SALOME_ModuleCatalog.DATASTREAM_INTEGER , SALOME_ModuleCatalog.DATASTREAM_TEMPORAL )
ONode_B_2ostream_B_2_2.SetNumberOfValues( 0 )

PyNode_C_1 = []
PyNode_C_1.append( 'def Node_C_1( n ) :        ' )
PyNode_C_1.append( '    return       ' )
Node_C_1 = GraphStreamTopology.INode( 'Node_C_1' , PyNode_C_1 )
Node_C_1.SetName( 'Node_C_1' )
Node_C_1.SetAuthor( '' )
Node_C_1.SetComment( 'Python function' )
Node_C_1.Coords( 478 , 63 )
INode_C_1c_1 = Node_C_1.InPort( 'c_1' , 'long' )
INode_C_1Gate = Node_C_1.GetInPort( 'Gate' )
ONode_C_1Gate = Node_C_1.GetOutPort( 'Gate' )
ONode_C_1ostream_C_1_1 = Node_C_1.OutStreamPort( 'ostream_C_1_1' , SALOME_ModuleCatalog.DATASTREAM_INTEGER , SALOME_ModuleCatalog.DATASTREAM_TEMPORAL )
ONode_C_1ostream_C_1_1.SetNumberOfValues( 0 )

PyNode_C_2 = []
PyNode_C_2.append( 'def Node_C_2( n ) :        ' )
PyNode_C_2.append( '    return n      ' )
Node_C_2 = GraphStreamTopology.INode( 'Node_C_2' , PyNode_C_2 )
Node_C_2.SetName( 'Node_C_2' )
Node_C_2.SetAuthor( '' )
Node_C_2.SetComment( 'Python function' )
Node_C_2.Coords( 477 , 337 )
INode_C_2c_2 = Node_C_2.InPort( 'c_2' , 'long' )
INode_C_2Gate = Node_C_2.GetInPort( 'Gate' )
ONode_C_2c_2 = Node_C_2.OutPort( 'c_2' , 'long' )
ONode_C_2Gate = Node_C_2.GetOutPort( 'Gate' )
INode_C_2istream_C_2_1 = Node_C_2.InStreamPort( 'istream_C_2_1' , SALOME_ModuleCatalog.DATASTREAM_INTEGER , SALOME_ModuleCatalog.DATASTREAM_TEMPORAL )
INode_C_2istream_C_2_1.SetParams( SUPERV.TI , SUPERV.L1 , SUPERV.EXTRANULL )

PyNode_D_1 = []
PyNode_D_1.append( 'def Node_D_1( n ) :        ' )
PyNode_D_1.append( '    return       ' )
Node_D_1 = GraphStreamTopology.INode( 'Node_D_1' , PyNode_D_1 )
Node_D_1.SetName( 'Node_D_1' )
Node_D_1.SetAuthor( '' )
Node_D_1.SetComment( 'Python function' )
Node_D_1.Coords( 702 , 336 )
INode_D_1d_1 = Node_D_1.InPort( 'd_1' , 'long' )
INode_D_1Gate = Node_D_1.GetInPort( 'Gate' )
ONode_D_1Gate = Node_D_1.GetOutPort( 'Gate' )
ONode_D_1ostream_D_1_1 = Node_D_1.OutStreamPort( 'ostream_D_1_1' , SALOME_ModuleCatalog.DATASTREAM_INTEGER , SALOME_ModuleCatalog.DATASTREAM_TEMPORAL )
ONode_D_1ostream_D_1_1.SetNumberOfValues( 0 )

# Creation of Links
LNode_A_1a_1Node_B_1b_1 = GraphStreamTopology.Link( ONode_A_1a_1 , INode_B_1b_1 )

LNode_A_2a_2Node_B_2b_2 = GraphStreamTopology.Link( ONode_A_2a_2 , INode_B_2b_2 )

LNode_A_2ostream_A_2_1Node_A_1istream_A_1_1 = GraphStreamTopology.StreamLink( ONode_A_2ostream_A_2_1 , INode_A_1istream_A_1_1 )
LNode_A_2ostream_A_2_1Node_A_1istream_A_1_1.AddCoord( 1 , 14 , 162 )
LNode_A_2ostream_A_2_1Node_A_1istream_A_1_1.AddCoord( 2 , 13 , 37 )
LNode_A_2ostream_A_2_1Node_A_1istream_A_1_1.AddCoord( 3 , 211 , 36 )
LNode_A_2ostream_A_2_1Node_A_1istream_A_1_1.AddCoord( 4 , 210 , 435 )

LNode_B_1b_1Node_C_1c_1 = GraphStreamTopology.Link( ONode_B_1b_1 , INode_C_1c_1 )

LNode_B_2b_2Node_C_2c_2 = GraphStreamTopology.Link( ONode_B_2b_2 , INode_C_2c_2 )

LNode_B_2ostream_B_2_1Node_A_1istream_A_1_2 = GraphStreamTopology.StreamLink( ONode_B_2ostream_B_2_1 , INode_A_1istream_A_1_2 )
LNode_B_2ostream_B_2_1Node_A_1istream_A_1_2.AddCoord( 1 , 8 , 195 )
LNode_B_2ostream_B_2_1Node_A_1istream_A_1_2.AddCoord( 2 , 7 , 324 )
LNode_B_2ostream_B_2_1Node_A_1istream_A_1_2.AddCoord( 3 , 443 , 323 )
LNode_B_2ostream_B_2_1Node_A_1istream_A_1_2.AddCoord( 4 , 442 , 434 )

LNode_B_2ostream_B_2_2Node_A_2istream_A_2_1 = GraphStreamTopology.StreamLink( ONode_B_2ostream_B_2_2 , INode_A_2istream_A_2_1 )
LNode_B_2ostream_B_2_2Node_A_2istream_A_2_1.AddCoord( 1 , 16 , 433 )
LNode_B_2ostream_B_2_2Node_A_2istream_A_2_1.AddCoord( 2 , 15 , 583 )
LNode_B_2ostream_B_2_2Node_A_2istream_A_2_1.AddCoord( 3 , 434 , 584 )
LNode_B_2ostream_B_2_2Node_A_2istream_A_2_1.AddCoord( 4 , 434 , 467 )

LNode_C_1ostream_C_1_1Node_A_1istream_A_1_3 = GraphStreamTopology.StreamLink( ONode_C_1ostream_C_1_1 , INode_A_1istream_A_1_3 )
LNode_C_1ostream_C_1_1Node_A_1istream_A_1_3.AddCoord( 1 , 21 , 226 )
LNode_C_1ostream_C_1_1Node_A_1istream_A_1_3.AddCoord( 2 , 20 , 296 )
LNode_C_1ostream_C_1_1Node_A_1istream_A_1_3.AddCoord( 3 , 663 , 295 )
LNode_C_1ostream_C_1_1Node_A_1istream_A_1_3.AddCoord( 4 , 662 , 161 )

LNode_C_2c_2Node_D_1d_1 = GraphStreamTopology.Link( ONode_C_2c_2 , INode_D_1d_1 )

LNode_D_1ostream_D_1_1Node_B_2istream_B_2_1 = GraphStreamTopology.StreamLink( ONode_D_1ostream_D_1_1 , INode_B_2istream_B_2_1 )
LNode_D_1ostream_D_1_1Node_B_2istream_B_2_1.AddCoord( 1 , 232 , 433 )
LNode_D_1ostream_D_1_1Node_B_2istream_B_2_1.AddCoord( 2 , 231 , 553 )
LNode_D_1ostream_D_1_1Node_B_2istream_B_2_1.AddCoord( 3 , 903 , 553 )
LNode_D_1ostream_D_1_1Node_B_2istream_B_2_1.AddCoord( 4 , 902 , 434 )

LNode_D_1ostream_D_1_1Node_C_2istream_C_2_1 = GraphStreamTopology.StreamLink( ONode_D_1ostream_D_1_1 , INode_C_2istream_C_2_1 )
LNode_D_1ostream_D_1_1Node_C_2istream_C_2_1.AddCoord( 1 , 461 , 434 )
LNode_D_1ostream_D_1_1Node_C_2istream_C_2_1.AddCoord( 2 , 461 , 524 )
LNode_D_1ostream_D_1_1Node_C_2istream_C_2_1.AddCoord( 3 , 889 , 525 )
LNode_D_1ostream_D_1_1Node_C_2istream_C_2_1.AddCoord( 4 , 888 , 434 )

LNode_D_1ostream_D_1_1Node_B_1istream_B_1_1 = GraphStreamTopology.StreamLink( ONode_D_1ostream_D_1_1 , INode_B_1istream_B_1_1 )
LNode_D_1ostream_D_1_1Node_B_1istream_B_1_1.AddCoord( 1 , 236 , 162 )
LNode_D_1ostream_D_1_1Node_B_1istream_B_1_1.AddCoord( 2 , 236 , 249 )
LNode_D_1ostream_D_1_1Node_B_1istream_B_1_1.AddCoord( 3 , 887 , 249 )
LNode_D_1ostream_D_1_1Node_B_1istream_B_1_1.AddCoord( 4 , 888 , 432 )

# Output Ports of the graph




GraphStreamTopology.Run()
GraphStreamTopology.DoneW()
print GraphStreamTopology.State()

GraphStreamTopology.PrintPorts()

subgraphs = GraphStreamTopology.SubGraphsNumber()
i = 1
while i <= subgraphs :
    nodes = GraphStreamTopology.SubGraphsNodes( i )
    j = 0
    while j < len(nodes) :
        print 'SubGraph',i,nodes[j].Name()
        j = j + 1
    i = i + 1


substreamgraphs = GraphStreamTopology.SubStreamGraphsNumber()
i = 1
while i <= substreamgraphs :
    nodes = GraphStreamTopology.SubStreamGraphsNodes( i )
    j = 0
    while j < len(nodes) :
        print 'SubStreamGraph',i,nodes[j].Name()
        j = j + 1
    i = i + 1



GraphStreamTopology.Export( '/tmp/GraphStreamTopology.xml' )

GraphStreamTopology = StreamGraph( '/tmp/GraphStreamTopology.xml' )

GraphStreamTopology.PrintPorts()

GraphStreamTopology.Run()
GraphStreamTopology.DoneW()
GraphStreamTopology.State()

subgraphs = GraphStreamTopology.SubGraphsNumber()
i = 1
while i <= subgraphs :
    nodes = GraphStreamTopology.SubGraphsNodes( i )
    j = 0
    while j < len(nodes) :
        print 'SubGraph',i,nodes[j].Name()
        j = j + 1
    i = i + 1


substreamgraphs = GraphStreamTopology.SubStreamGraphsNumber()
i = 1
while i <= substreamgraphs :
    nodes = GraphStreamTopology.SubStreamGraphsNodes( i )
    j = 0
    while j < len(nodes) :
        print 'SubStreamGraph',i,nodes[j].Name()
        j = j + 1
    i = i + 1

