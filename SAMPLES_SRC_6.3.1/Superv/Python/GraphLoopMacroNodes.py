#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph GraphLoopMacroNodes
#
from SuperV import *

# Graph creation of GraphLoopMacroNodes
def DefGraphLoopMacroNodes() :
    GraphLoopMacroNodes = Graph( 'GraphLoopMacroNodes' )
    GraphLoopMacroNodes.SetName( 'GraphLoopMacroNodes' )
    GraphLoopMacroNodes.SetAuthor( 'JR' )
    GraphLoopMacroNodes.SetComment( '' )
    GraphLoopMacroNodes.Coords( 0 , 0 )
    
    # Creation of Factory Nodes
    
    Add = GraphLoopMacroNodes.FNode( 'AddComponent' , 'AddComponent' , 'Add' )
    Add.SetName( 'Add' )
    Add.SetAuthor( '' )
    Add.SetContainer( 'FactoryServer' )
    Add.SetComment( 'Add from AddComponent' )
    Add.Coords( 205 , 238 )
    IAddx = Add.GetInPort( 'x' )
    IAddy = Add.GetInPort( 'y' )
    IAddGate = Add.GetInPort( 'Gate' )
    OAddFuncValue = Add.GetOutPort( 'FuncValue' )
    OAddz = Add.GetOutPort( 'z' )
    OAddGate = Add.GetOutPort( 'Gate' )
    
    Sub = GraphLoopMacroNodes.FNode( 'SubComponent' , 'SubComponent' , 'Sub' )
    Sub.SetName( 'Sub' )
    Sub.SetAuthor( '' )
    Sub.SetContainer( 'FactoryServer' )
    Sub.SetComment( 'Sub from SubComponent' )
    Sub.Coords( 394 , 146 )
    ISubx = Sub.GetInPort( 'x' )
    ISuby = Sub.GetInPort( 'y' )
    ISubGate = Sub.GetInPort( 'Gate' )
    OSubz = Sub.GetOutPort( 'z' )
    OSubGate = Sub.GetOutPort( 'Gate' )
    
    Mul = GraphLoopMacroNodes.FNode( 'MulComponent' , 'MulComponent' , 'Mul' )
    Mul.SetName( 'Mul' )
    Mul.SetAuthor( '' )
    Mul.SetContainer( 'FactoryServer' )
    Mul.SetComment( 'Mul from MulComponent' )
    Mul.Coords( 821 , 319 )
    IMulx = Mul.GetInPort( 'x' )
    IMuly = Mul.GetInPort( 'y' )
    IMulGate = Mul.GetInPort( 'Gate' )
    OMulz = Mul.GetOutPort( 'z' )
    OMulGate = Mul.GetOutPort( 'Gate' )
    
    Div = GraphLoopMacroNodes.FNode( 'DivComponent' , 'DivComponent' , 'Div' )
    Div.SetName( 'Div' )
    Div.SetAuthor( '' )
    Div.SetContainer( 'FactoryServer' )
    Div.SetComment( 'Div from DivComponent' )
    Div.Coords( 825 , 133 )
    IDivx = Div.GetInPort( 'x' )
    IDivy = Div.GetInPort( 'y' )
    IDivGate = Div.GetInPort( 'Gate' )
    ODivz = Div.GetOutPort( 'z' )
    ODivGate = Div.GetOutPort( 'Gate' )
    
    # Creation of InLine Nodes
    PyResultsControl = []
    PyResultsControl.append( 'def ResultsControl(x,y,zDiv,zMul) :' )
    PyResultsControl.append( '    Add_FuncValue = x - y' )
    PyResultsControl.append( '    Add_z = x + y' )
    PyResultsControl.append( '    Sub_z = 1.5 - Add_z' )
    PyResultsControl.append( '    Macro_GraphAdd_Add_FuncValue = Sub_z - Add_FuncValue' )
    PyResultsControl.append( '    Macro_GraphAdd_Add_z = Sub_z + Add_FuncValue' )
    PyResultsControl.append( '    Macro_GraphSub_z = Add_FuncValue - Add_z' )
    PyResultsControl.append( '    Div_z = Macro_GraphAdd_Add_FuncValue/Macro_GraphAdd_Add_z' )
    PyResultsControl.append( '    Mul_z = Sub_z * Macro_GraphSub_z' )
    PyResultsControl.append( '    OK = \'Okay\'' )
    PyResultsControl.append( '    if Div_z != zDiv or Mul_z != zMul :' )
    PyResultsControl.append( '        OK = \'KO\'' )
    PyResultsControl.append( '    return zDiv,zMul,Div_z,Mul_z,OK' )
    PyResultsControl.append( '' )
    ResultsControl = GraphLoopMacroNodes.INode( 'ResultsControl' , PyResultsControl )
    ResultsControl.SetName( 'ResultsControl' )
    ResultsControl.SetAuthor( '' )
    ResultsControl.SetComment( 'Compute Node' )
    ResultsControl.Coords( 1102 , 196 )
    IResultsControlx = ResultsControl.InPort( 'x' , 'double' )
    IResultsControly = ResultsControl.InPort( 'y' , 'double' )
    IResultsControlzDiv = ResultsControl.InPort( 'zDiv' , 'double' )
    IResultsControlzMul = ResultsControl.InPort( 'zMul' , 'double' )
    IResultsControlGate = ResultsControl.GetInPort( 'Gate' )
    OResultsControlzDiv = ResultsControl.OutPort( 'zDiv' , 'double' )
    OResultsControlzMul = ResultsControl.OutPort( 'zMul' , 'double' )
    OResultsControlDiv_z = ResultsControl.OutPort( 'Div_z' , 'double' )
    OResultsControlMul_z = ResultsControl.OutPort( 'Mul_z' , 'double' )
    OResultsControlOK = ResultsControl.OutPort( 'OK' , 'string' )
    OResultsControlGate = ResultsControl.GetOutPort( 'Gate' )
    
    # Creation of Loop Nodes
    PyLoop = []
    PyLoop.append( 'def Init(Index,Min,Max,Incr,zDiv,zMul,OK) :' )
    PyLoop.append( '        if Min <= Max :' )
    PyLoop.append( '            Index = Min' )
    PyLoop.append( '        else :' )
    PyLoop.append( '            Index = Max' )
    PyLoop.append( '        return Index,Min,Max,Incr,zDiv,zMul,OK' )
    PyLoop.append( '' )
    PyMoreLoop = []
    PyMoreLoop.append( 'def More(Index,Min,Max,Incr,zDiv,zMul,OK) :' )
    PyMoreLoop.append( '        if Index < Max :' )
    PyMoreLoop.append( '            DoLoop = 1' )
    PyMoreLoop.append( '        else :' )
    PyMoreLoop.append( '            DoLoop = 0' )
    PyMoreLoop.append( '        return DoLoop,Index,Min,Max,Incr,zDiv,zMul,OK' )
    PyMoreLoop.append( '' )
    PyNextLoop = []
    PyNextLoop.append( 'def Next(Index,Min,Max,Incr,zDiv,zMul,OK) :' )
    PyNextLoop.append( '        Index = Index + Incr' )
    PyNextLoop.append( '        return Index,Min,Max,Incr,zDiv,zMul,OK' )
    PyNextLoop.append( '' )
    Loop,EndLoop = GraphLoopMacroNodes.LNode( 'Init' , PyLoop , 'More' , PyMoreLoop , 'Next' , PyNextLoop )
    EndLoop.SetName( 'EndLoop' )
    EndLoop.SetAuthor( '' )
    EndLoop.SetComment( 'Compute Node' )
    EndLoop.Coords( 1354 , 156 )
    PyEndLoop = []
    PyEndLoop.append( 'def EndLoop(DoLoop,Index,Min,Max,Incr,zDiv,zMul,OK) :' )
    PyEndLoop.append( '	print \'EndLoop\',DoLoop,Index,Min,Max,Incr,zDiv,zMul,OK' )
    PyEndLoop.append( '	if OK != \'Okay\' :' )
    PyEndLoop.append( '	    DoLoop = 0' )
    PyEndLoop.append( '	return DoLoop,Index,Min,Max,Incr ,zDiv,zMul,OK' )
    PyEndLoop.append( '' )
    EndLoop.SetPyFunction( 'EndLoop' , PyEndLoop )
    ILoopDoLoop = Loop.GetInPort( 'DoLoop' )
    ILoopIndex = Loop.InPort( 'Index' , 'long' )
    ILoopMin = Loop.InPort( 'Min' , 'long' )
    ILoopMax = Loop.InPort( 'Max' , 'long' )
    ILoopIncr = Loop.InPort( 'Incr' , 'long' )
    ILoopzDiv = Loop.InPort( 'zDiv' , 'double' )
    ILoopzMul = Loop.InPort( 'zMul' , 'double' )
    ILoopOK = Loop.InPort( 'OK' , 'string' )
    ILoopGate = Loop.GetInPort( 'Gate' )
    OLoopDoLoop = Loop.GetOutPort( 'DoLoop' )
    OLoopIndex = Loop.GetOutPort( 'Index' )
    OLoopMin = Loop.GetOutPort( 'Min' )
    OLoopMax = Loop.GetOutPort( 'Max' )
    OLoopIncr = Loop.GetOutPort( 'Incr' )
    OLoopzDiv = Loop.GetOutPort( 'zDiv' )
    OLoopzMul = Loop.GetOutPort( 'zMul' )
    OLoopOK = Loop.GetOutPort( 'OK' )
    OLoopGate = Loop.GetOutPort( 'Gate' )
    IEndLoopDoLoop = EndLoop.GetInPort( 'DoLoop' )
    IEndLoopIndex = EndLoop.GetInPort( 'Index' )
    IEndLoopMin = EndLoop.GetInPort( 'Min' )
    IEndLoopMax = EndLoop.GetInPort( 'Max' )
    IEndLoopIncr = EndLoop.GetInPort( 'Incr' )
    IEndLoopzDiv = EndLoop.GetInPort( 'zDiv' )
    IEndLoopzMul = EndLoop.GetInPort( 'zMul' )
    IEndLoopOK = EndLoop.GetInPort( 'OK' )
    IEndLoopGate = EndLoop.GetInPort( 'Gate' )
    OEndLoopDoLoop = EndLoop.GetOutPort( 'DoLoop' )
    OEndLoopIndex = EndLoop.GetOutPort( 'Index' )
    OEndLoopMin = EndLoop.GetOutPort( 'Min' )
    OEndLoopMax = EndLoop.GetOutPort( 'Max' )
    OEndLoopIncr = EndLoop.GetOutPort( 'Incr' )
    OEndLoopzDiv = EndLoop.GetOutPort( 'zDiv' )
    OEndLoopzMul = EndLoop.GetOutPort( 'zMul' )
    OEndLoopOK = EndLoop.GetOutPort( 'OK' )
    OEndLoopGate = EndLoop.GetOutPort( 'Gate' )
    Loop.SetName( 'Loop' )
    Loop.SetAuthor( '' )
    Loop.SetComment( 'Compute Node' )
    Loop.Coords( 5 , 265 )
    
    # Creation of Macro Nodes
    GraphAdd_1 = DefGraphAdd_1()
    Macro_GraphAdd = GraphLoopMacroNodes.GraphMNode( GraphAdd_1 )
    Macro_GraphAdd.SetCoupled( 'GraphAdd_1' )
    Macro_GraphAdd.SetName( 'Macro_GraphAdd' )
    Macro_GraphAdd.SetAuthor( '' )
    Macro_GraphAdd.SetComment( 'Macro Node' )
    Macro_GraphAdd.Coords( 625 , 133 )
    IMacro_GraphAddAdd__x = Macro_GraphAdd.GetInPort( 'Add__x' )
    IMacro_GraphAddAdd__y = Macro_GraphAdd.GetInPort( 'Add__y' )
    IMacro_GraphAddGate = Macro_GraphAdd.GetInPort( 'Gate' )
    OMacro_GraphAddAdd__FuncValue = Macro_GraphAdd.GetOutPort( 'Add__FuncValue' )
    OMacro_GraphAddAdd__z = Macro_GraphAdd.GetOutPort( 'Add__z' )
    OMacro_GraphAddGate = Macro_GraphAdd.GetOutPort( 'Gate' )
    
    GraphSub_1 = DefGraphSub_1()
    Macro_GraphSub = GraphLoopMacroNodes.GraphMNode( GraphSub_1 )
    Macro_GraphSub.SetCoupled( 'GraphSub_1' )
    Macro_GraphSub.SetName( 'Macro_GraphSub' )
    Macro_GraphSub.SetAuthor( '' )
    Macro_GraphSub.SetComment( 'Macro Node' )
    Macro_GraphSub.Coords( 512 , 319 )
    IMacro_GraphSubSub__x = Macro_GraphSub.GetInPort( 'Sub__x' )
    IMacro_GraphSubSub__y = Macro_GraphSub.GetInPort( 'Sub__y' )
    IMacro_GraphSubGate = Macro_GraphSub.GetInPort( 'Gate' )
    OMacro_GraphSubSub__z = Macro_GraphSub.GetOutPort( 'Sub__z' )
    OMacro_GraphSubGate = Macro_GraphSub.GetOutPort( 'Gate' )
    
    # Creation of Links
    LAddFuncValueMacro_GraphSubSub__x = GraphLoopMacroNodes.Link( OAddFuncValue , IMacro_GraphSubSub__x )
    
    LAddFuncValueMacro_GraphAddAdd__y = GraphLoopMacroNodes.Link( OAddFuncValue , IMacro_GraphAddAdd__y )
    LAddFuncValueMacro_GraphAddAdd__y.AddCoord( 1 , 512 , 309 )
    
    LAddzSuby = GraphLoopMacroNodes.Link( OAddz , ISuby )
    
    LAddzMacro_GraphSubSub__y = GraphLoopMacroNodes.Link( OAddz , IMacro_GraphSubSub__y )
    
    LSubzMacro_GraphAddAdd__x = GraphLoopMacroNodes.Link( OSubz , IMacro_GraphAddAdd__x )
    
    LSubzMulx = GraphLoopMacroNodes.Link( OSubz , IMulx )
    LSubzMulx.AddCoord( 1 , 767 , 389 )
    LSubzMulx.AddCoord( 2 , 767 , 297 )
    LSubzMulx.AddCoord( 3 , 592 , 297 )
    LSubzMulx.AddCoord( 4 , 592 , 217 )
    
    LMulzResultsControlzMul = GraphLoopMacroNodes.Link( OMulz , IResultsControlzMul )
    
    LDivzResultsControlzDiv = GraphLoopMacroNodes.Link( ODivz , IResultsControlzDiv )
    
    LMacro_GraphAddAdd__FuncValueDivx = GraphLoopMacroNodes.Link( OMacro_GraphAddAdd__FuncValue , IDivx )
    
    LMacro_GraphAddAdd__zDivy = GraphLoopMacroNodes.Link( OMacro_GraphAddAdd__z , IDivy )
    
    LMacro_GraphSubSub__zMuly = GraphLoopMacroNodes.Link( OMacro_GraphSubSub__z , IMuly )
    
    LMacro_GraphSubGateMulGate = GraphLoopMacroNodes.Link( OMacro_GraphSubGate , IMulGate )
    
    LLoopIndexEndLoopIndex = GraphLoopMacroNodes.Link( OLoopIndex , IEndLoopIndex )
    
    LLoopIndexAddx = GraphLoopMacroNodes.Link( OLoopIndex , IAddx )
    
    LLoopIndexResultsControlx = GraphLoopMacroNodes.Link( OLoopIndex , IResultsControlx )
    
    LLoopMinEndLoopMin = GraphLoopMacroNodes.Link( OLoopMin , IEndLoopMin )
    
    LLoopMaxEndLoopMax = GraphLoopMacroNodes.Link( OLoopMax , IEndLoopMax )
    
    LLoopMaxAddy = GraphLoopMacroNodes.Link( OLoopMax , IAddy )
    
    LLoopMaxResultsControly = GraphLoopMacroNodes.Link( OLoopMax , IResultsControly )
    
    LLoopIncrEndLoopIncr = GraphLoopMacroNodes.Link( OLoopIncr , IEndLoopIncr )
    
    LResultsControlzDivEndLoopzDiv = GraphLoopMacroNodes.Link( OResultsControlzDiv , IEndLoopzDiv )
    
    LResultsControlzMulEndLoopzMul = GraphLoopMacroNodes.Link( OResultsControlzMul , IEndLoopzMul )
    
    LResultsControlOKEndLoopOK = GraphLoopMacroNodes.Link( OResultsControlOK , IEndLoopOK )
    
    # Input datas
    ISubx.Input( 1.5 )
    ILoopIndex.Input( 0 )
    ILoopMin.Input( 7 )
    ILoopMax.Input( 17 )
    ILoopIncr.Input( 1 )
    ILoopzDiv.Input( 0 )
    ILoopzMul.Input( 0 )
    ILoopOK.Input( 'Okay' )
    
    # Output Ports of the graph
    #OEndLoopIndex = EndLoop.GetOutPort( 'Index' )
    #OEndLoopMin = EndLoop.GetOutPort( 'Min' )
    #OEndLoopMax = EndLoop.GetOutPort( 'Max' )
    #OEndLoopIncr = EndLoop.GetOutPort( 'Incr' )
    #OEndLoopzDiv = EndLoop.GetOutPort( 'zDiv' )
    #OEndLoopzMul = EndLoop.GetOutPort( 'zMul' )
    #OEndLoopOK = EndLoop.GetOutPort( 'OK' )
    #OResultsControlDiv_z = ResultsControl.GetOutPort( 'Div_z' )
    #OResultsControlMul_z = ResultsControl.GetOutPort( 'Mul_z' )
    return GraphLoopMacroNodes

# Graph creation of GraphAdd_1
def DefGraphAdd_1() :
    GraphAdd_1 = Graph( 'GraphAdd_1' )
    GraphAdd_1.SetCoupled( 'Macro_GraphAdd' )
    GraphAdd_1.SetName( 'GraphAdd_1' )
    GraphAdd_1.SetAuthor( '' )
    GraphAdd_1.SetComment( '' )
    GraphAdd_1.Coords( 0 , 0 )
    
    # Creation of Factory Nodes
    
    Add = GraphAdd_1.FNode( 'AddComponent' , 'AddComponent' , 'Add' )
    Add.SetName( 'Add' )
    Add.SetAuthor( '' )
    Add.SetContainer( 'localhost/FactoryServer' )
    Add.SetComment( 'Add from AddComponent' )
    Add.Coords( 55 , 61 )
    IAddx = Add.GetInPort( 'x' )
    IAddy = Add.GetInPort( 'y' )
    IAddGate = Add.GetInPort( 'Gate' )
    OAddFuncValue = Add.GetOutPort( 'FuncValue' )
    OAddz = Add.GetOutPort( 'z' )
    OAddGate = Add.GetOutPort( 'Gate' )
    
    # Input Ports of the graph
    #IAddx = Add.GetInPort( 'x' )
    #IAddy = Add.GetInPort( 'y' )
    
    # Output Ports of the graph
    #OAddFuncValue = Add.GetOutPort( 'FuncValue' )
    #OAddz = Add.GetOutPort( 'z' )
    return GraphAdd_1

# Graph creation of GraphSub_1
def DefGraphSub_1() :
    GraphSub_1 = Graph( 'GraphSub_1' )
    GraphSub_1.SetCoupled( 'Macro_GraphSub' )
    GraphSub_1.SetName( 'GraphSub_1' )
    GraphSub_1.SetAuthor( '' )
    GraphSub_1.SetComment( '' )
    GraphSub_1.Coords( 0 , 0 )
    
    # Creation of Factory Nodes
    
    Sub = GraphSub_1.FNode( 'SubComponent' , 'SubComponent' , 'Sub' )
    Sub.SetName( 'Sub' )
    Sub.SetAuthor( '' )
    Sub.SetContainer( 'localhost/FactoryServer' )
    Sub.SetComment( 'Sub from SubComponent' )
    Sub.Coords( 55 , 71 )
    ISubx = Sub.GetInPort( 'x' )
    ISuby = Sub.GetInPort( 'y' )
    ISubGate = Sub.GetInPort( 'Gate' )
    OSubz = Sub.GetOutPort( 'z' )
    OSubGate = Sub.GetOutPort( 'Gate' )
    
    # Input Ports of the graph
    #ISubx = Sub.GetInPort( 'x' )
    #ISuby = Sub.GetInPort( 'y' )
    
    # Output Ports of the graph
    #OSubz = Sub.GetOutPort( 'z' )
    return GraphSub_1


GraphLoopMacroNodes = DefGraphLoopMacroNodes()
