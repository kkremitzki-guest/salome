#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
#%dataflow_calculator_example.py%
#==============================================================================
#  File      : dataflow_calculator_example.py
#  Created   : 30 sept 2002
#  Author    : Laurent DADA
#  Project   : SALOME
#==============================================================================
#==============================================================================
#  Example of dataflow with CalculatorComponent services (Add, Mult and Const)
#  and MED data (mesh and field).
#==============================================================================
#==============================================================================
#
import batchmode_salome
import SALOME_MED
from batchmode_SuperV import *
#==============================================================================
datadir = os.getenv("DATA_DIR")
if len(datadir) != 0:
        datadir=datadir+ "/MedFiles/"
input_file  = datadir+'pointe.med'
print 'dataflow_calculator_example.py',input_file
str= os.getenv("HOME")
if str == None:
        str = "/tmp"
output_file = str + 'test_dataflow_calculator.med'

export_xmlfile = str + "/my_dataflow_calculator_example.xml"

#==============================================================================
# Load mesh and field in MED component, register into current study and get
# a field under the following name : fieldnodedouble 
#==============================================================================
from batchmode_MED import *

#==============================================================================
# Define a function for printing on Salome TUI the" until_index" first values
# for each component of a field (MED data)
#==============================================================================

def PrintField(aField,until_index):
    # check parameter
    if aField is None:
        print "PrintField() : aField is None "
        return
    name    = aField.getName()
    desc    = aField.getDescription()
    nb_comp = aField.getNumberOfComponents()
    values  = aField.getValue(SALOME_MED.MED_FULL_INTERLACE)
    support = aField.getSupport()
    if (support.isOnAllElements()):
        nb_node = support.getMesh().getNumberOfElements(support.getEntity(),SALOME_MED.MED_ALL_ELEMENTS)
    else:
        nb_node = support.getNumberOfElements(SALOME_MED.MED_ALL_ELEMENTS);

    if until_index > nb_node:
        max_index = nb_node
    else:
        max_index = until_index
    time  = aField.getTime()
    iter  = aField.getIterationNumber()
    print "------------------------------------------------"
    print "Field %s"%(name)
    print "    Description          : %s"%(desc)
    print "    Number of components : %d"%(nb_comp)
    print "    Number of nodes      : %d"%(nb_node)
    print "    Iteration number     : %d"%(iter)
    print "    Time                 : %f"%(time)
    icomp = 0
    while icomp < nb_comp :
        icomp = icomp + 1
        namec = aField.getComponentName(icomp)
        unit  = aField.getComponentUnit(icomp)
        print "        Component             : %s"%(namec)
        print "              Unit            : %s"%(type)
        print "              first %d values :"%(until_index)
        index = 0
        while index < max_index :
            index = index + 1
            print "                    component %d index %d :%f"%(icomp,index,values[(index-1)+(icomp-1)*nb_comp])
    print "------------------------------------------------"

#----------------------------------------------------------------------

med_comp.readStructFileWithFieldType(input_file, studyCurrent)

med_obj = getMedObjectFromStudy()

nbMeshes = med_obj.getNumberOfMeshes()

nbFields = med_obj.getNumberOfFields()

print ""
print "The med file ",input_file," has ",nbMeshes," Meshe(s) and ",nbFields," Field(s)"
print ""

mesh = getMeshObjectFromStudy(1)

name = mesh.getName()

nbNodes = mesh.getNumberOfNodes()

spaceDim = mesh.getSpaceDimension()

print "The mesh from the Study is ",name,".It is a ",spaceDim,"-D mesh and it has ",nbNodes,"Nodes"
print ""

fieldcelldouble = getFieldIntObjectFromStudy(1,1)
if (fieldcelldouble == None):
    fieldcelldouble = getFieldDoubleObjectFromStudy(1,1)
    print "The following field is a float (double) one"
else:
    print "The following field is an integer one"

AnalyzeField(fieldcelldouble)

fieldnodedouble = getFieldIntObjectFromStudy(2,1)
if (fieldnodedouble == None):
    fieldnodedouble = getFieldDoubleObjectFromStudy(2,1)
    print "The following field is a float (double) one"
else:
    print "The following field is an integer one"

AnalyzeField(fieldnodedouble)
#----------------------------------------------------------------------

#==============================================================================
# Building the Dataflow
#==============================================================================

myGraph = Graph("CalculatorDataflow")

# nodes and links
# -------------------

scal_field_const = myGraph.Node("Calculator","Calculator","Constant")
scal_field_mult  = myGraph.Node("Calculator","Calculator","Mul")
scal_field_add   = myGraph.Node("Calculator","Calculator","Add")

write_initial     = myGraph.Node("Calculator","Calculator","writeMEDfile")
write_result      = myGraph.Node("Calculator","Calculator","writeMEDfile")

link1 = myGraph.Link( scal_field_const.Port("return") , scal_field_add.Port("field1") )
link2 = myGraph.Link( scal_field_mult.Port("return") , scal_field_add.Port("field2") )
link3 = myGraph.Link( scal_field_add.Port("return") , write_result.Port("field1") )


# machines ressources
# -------------------

#myGraph.SetContainer('FactoryServer')
scal_field_const.SetContainer('FactoryServer')
scal_field_mult.SetContainer('FactoryServer')
scal_field_add.SetContainer('FactoryServer')
write_initial.SetContainer('FactoryServer')
write_result.SetContainer('FactoryServer')

# validation and exporting (xml format)
# ---------------------------------
print myGraph.Export(export_xmlfile)

print myGraph.IsValid()

#==============================================================================
# Dataflow Input
#==============================================================================

# Ports Input
# ----------------------------

scal_const_in2      = scal_field_const.Input( "x1", 10. )
scal_mult_in2       = scal_field_mult.Input( "x1", -1. )
result_write        = write_result.Input( "filename", output_file)
initial_write_in2   = write_initial.Input( "filename", output_file)

# exporting with constant inputs (xml format)
# -------------------------------------------

print myGraph.Export(export_xmlfile)

# Other ports Input
# ----------------------------

print "Print fieldnodedouble"
PrintField(fieldnodedouble,20)

scal_const_in1      = scal_field_const.Input( "field1", fieldnodedouble )
scal_mult_in1       = scal_field_mult.Input( "field1", fieldnodedouble )
initial_write_in1   = write_initial.Input( "field1", fieldnodedouble)

print myGraph.IsExecutable()

#==============================================================================
# Running the Dataflow (asynchronous)
#==============================================================================

print myGraph.Run()

print myGraph.DoneW()

new_field   = scal_field_add.Port("return").ToAny().value()
print "Print new_field"
PrintField(new_field,20)




