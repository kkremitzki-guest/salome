#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph GraphSwitch_1_1
#
from SuperV import *

# Graph creation of GraphSwitch_1_1
def DefGraphSwitch_1_1() :
    GraphSwitch_1_1 = Graph( 'GraphSwitch_1_1' )
    GraphSwitch_1_1.SetName( 'GraphSwitch_1_1' )
    GraphSwitch_1_1.SetAuthor( '' )
    GraphSwitch_1_1.SetComment( '' )
    GraphSwitch_1_1.Coords( 0 , 0 )
    
    # Creation of Factory Nodes
    
    # Creation of InLine Nodes
    PyIsOdd = []
    PyIsOdd.append( 'from time import *   ' )
    PyIsOdd.append( 'def IsOdd(a) :       ' )
    PyIsOdd.append( '    print a,"IsOdd (GraphSwitch)"      ' )
    PyIsOdd.append( '    sleep( 1 )   ' )
    PyIsOdd.append( '    return a     ' )
    IsOdd = GraphSwitch_1_1.INode( 'IsOdd' , PyIsOdd )
    IsOdd.SetName( 'IsOdd' )
    IsOdd.SetAuthor( '' )
    IsOdd.SetComment( 'Python function' )
    IsOdd.Coords( 388 , 50 )
    IIsOdda = IsOdd.InPort( 'a' , 'long' )
    IIsOddGate = IsOdd.GetInPort( 'Gate' )
    OIsOdda = IsOdd.OutPort( 'a' , 'long' )
    OIsOddGate = IsOdd.GetOutPort( 'Gate' )
    
    PyIsEven = []
    PyIsEven.append( 'from time import *    ' )
    PyIsEven.append( 'def IsEven(a) :        ' )
    PyIsEven.append( '    print a,"IsEven (GraphSwitch)"       ' )
    PyIsEven.append( '    sleep( 1 )    ' )
    PyIsEven.append( '    return a      ' )
    IsEven = GraphSwitch_1_1.INode( 'IsEven' , PyIsEven )
    IsEven.SetName( 'IsEven' )
    IsEven.SetAuthor( '' )
    IsEven.SetComment( 'Compute Node' )
    IsEven.Coords( 397 , 279 )
    IIsEvena = IsEven.InPort( 'a' , 'long' )
    IIsEvenGate = IsEven.GetInPort( 'Gate' )
    OIsEvena = IsEven.OutPort( 'a' , 'long' )
    OIsEvenGate = IsEven.GetOutPort( 'Gate' )
    
    # Creation of Loop Nodes
    PyInitLoopSwitch = []
    PyInitLoopSwitch.append( 'def InitLoop(Index,Min,Max) :       ' )
    PyInitLoopSwitch.append( '	Index = Max ' )
    PyInitLoopSwitch.append( '	return Index,Min,Max      ' )
    PyMoreInitLoopSwitch = []
    PyMoreInitLoopSwitch.append( 'def MoreLoop(Index,Min,Max) :      ' )
    PyMoreInitLoopSwitch.append( '	if Index >= Min :    ' )
    PyMoreInitLoopSwitch.append( '		DoLoop = 1      ' )
    PyMoreInitLoopSwitch.append( '	else :      ' )
    PyMoreInitLoopSwitch.append( '		DoLoop = 0      ' )
    PyMoreInitLoopSwitch.append( '	return DoLoop,Index,Min,Max      ' )
    PyNextInitLoopSwitch = []
    PyNextInitLoopSwitch.append( 'def NextLoop(Index,Min,Max) :      ' )
    PyNextInitLoopSwitch.append( '	Index = Index - 1      ' )
    PyNextInitLoopSwitch.append( '	return Index,Min,Max      ' )
    InitLoopSwitch,EndOfInitLoopSwitch = GraphSwitch_1_1.LNode( 'InitLoop' , PyInitLoopSwitch , 'MoreLoop' , PyMoreInitLoopSwitch , 'NextLoop' , PyNextInitLoopSwitch )
    EndOfInitLoopSwitch.SetName( 'EndOfInitLoopSwitch' )
    EndOfInitLoopSwitch.SetAuthor( '' )
    EndOfInitLoopSwitch.SetComment( 'Compute Node' )
    EndOfInitLoopSwitch.Coords( 775 , 169 )
    PyEndOfInitLoopSwitch = []
    EndOfInitLoopSwitch.SetPyFunction( '' , PyEndOfInitLoopSwitch )
    IInitLoopSwitchDoLoop = InitLoopSwitch.GetInPort( 'DoLoop' )
    IInitLoopSwitchIndex = InitLoopSwitch.InPort( 'Index' , 'long' )
    IInitLoopSwitchMin = InitLoopSwitch.InPort( 'Min' , 'long' )
    IInitLoopSwitchMax = InitLoopSwitch.InPort( 'Max' , 'long' )
    IInitLoopSwitchGate = InitLoopSwitch.GetInPort( 'Gate' )
    OInitLoopSwitchDoLoop = InitLoopSwitch.GetOutPort( 'DoLoop' )
    OInitLoopSwitchIndex = InitLoopSwitch.GetOutPort( 'Index' )
    OInitLoopSwitchMin = InitLoopSwitch.GetOutPort( 'Min' )
    OInitLoopSwitchMax = InitLoopSwitch.GetOutPort( 'Max' )
    IEndOfInitLoopSwitchDoLoop = EndOfInitLoopSwitch.GetInPort( 'DoLoop' )
    IEndOfInitLoopSwitchIndex = EndOfInitLoopSwitch.GetInPort( 'Index' )
    IEndOfInitLoopSwitchMin = EndOfInitLoopSwitch.GetInPort( 'Min' )
    IEndOfInitLoopSwitchMax = EndOfInitLoopSwitch.GetInPort( 'Max' )
    IEndOfInitLoopSwitchGate = EndOfInitLoopSwitch.GetInPort( 'Gate' )
    OEndOfInitLoopSwitchDoLoop = EndOfInitLoopSwitch.GetOutPort( 'DoLoop' )
    OEndOfInitLoopSwitchIndex = EndOfInitLoopSwitch.GetOutPort( 'Index' )
    OEndOfInitLoopSwitchMin = EndOfInitLoopSwitch.GetOutPort( 'Min' )
    OEndOfInitLoopSwitchMax = EndOfInitLoopSwitch.GetOutPort( 'Max' )
    OEndOfInitLoopSwitchGate = EndOfInitLoopSwitch.GetOutPort( 'Gate' )
    InitLoopSwitch.SetName( 'InitLoopSwitch' )
    InitLoopSwitch.SetAuthor( '' )
    InitLoopSwitch.SetComment( 'Compute Node' )
    InitLoopSwitch.Coords( 10 , 129 )
    
    # Creation of Switch Nodes
    PySwitch = []
    PySwitch.append( 'from time import *     ' )
    PySwitch.append( 'def Switch(a) : ' )
    PySwitch.append( '    sleep(1) ' )
    PySwitch.append( '    if a <= 0 :    ' )
    PySwitch.append( '        return 0,0,a    ' )
    PySwitch.append( '    return a & 1,1-(a&1),a        ' )
    Switch,EndOfSwitch = GraphSwitch_1_1.SNode( 'Switch' , PySwitch )
    EndOfSwitch.SetName( 'EndOfSwitch' )
    EndOfSwitch.SetAuthor( '' )
    EndOfSwitch.SetComment( 'Compute Node' )
    EndOfSwitch.Coords( 583 , 169 )
    PyEndOfSwitch = []
    EndOfSwitch.SetPyFunction( 'EndSwitch_1' , PyEndOfSwitch )
    IEndOfSwitcha = EndOfSwitch.InPort( 'a' , 'long' )
    IEndOfSwitchDefault = EndOfSwitch.GetInPort( 'Default' )
    OEndOfSwitcha = EndOfSwitch.OutPort( 'a' , 'long' )
    OEndOfSwitchGate = EndOfSwitch.GetOutPort( 'Gate' )
    Switch.SetName( 'Switch' )
    Switch.SetAuthor( '' )
    Switch.SetComment( 'Compute Node' )
    Switch.Coords( 195 , 129 )
    ISwitcha = Switch.InPort( 'a' , 'long' )
    ISwitchGate = Switch.GetInPort( 'Gate' )
    OSwitchOdd = Switch.OutPort( 'Odd' , 'long' )
    OSwitchEven = Switch.OutPort( 'Even' , 'int' )
    OSwitcha = Switch.OutPort( 'a' , 'int' )
    OSwitchDefault = Switch.GetOutPort( 'Default' )
    
    # Creation of Links
    LIsOddaEndOfSwitcha = GraphSwitch_1_1.Link( OIsOdda , IEndOfSwitcha )
    
    LInitLoopSwitchIndexSwitcha = GraphSwitch_1_1.Link( OInitLoopSwitchIndex , ISwitcha )
    
    LInitLoopSwitchMinEndOfInitLoopSwitchMin = GraphSwitch_1_1.Link( OInitLoopSwitchMin , IEndOfInitLoopSwitchMin )
    
    LInitLoopSwitchMaxEndOfInitLoopSwitchMax = GraphSwitch_1_1.Link( OInitLoopSwitchMax , IEndOfInitLoopSwitchMax )
    
    LIsEvenaEndOfSwitcha = GraphSwitch_1_1.Link( OIsEvena , IEndOfSwitcha )
    
    LSwitchOddIsOddGate = GraphSwitch_1_1.Link( OSwitchOdd , IIsOddGate )
    
    LSwitchEvenIsEvenGate = GraphSwitch_1_1.Link( OSwitchEven , IIsEvenGate )
    
    LSwitchaIsOdda = GraphSwitch_1_1.Link( OSwitcha , IIsOdda )
    
    LSwitchaIsEvena = GraphSwitch_1_1.Link( OSwitcha , IIsEvena )
    
    LSwitchDefaultEndOfSwitchDefault = GraphSwitch_1_1.Link( OSwitchDefault , IEndOfSwitchDefault )
    
    LEndOfSwitchaEndOfInitLoopSwitchIndex = GraphSwitch_1_1.Link( OEndOfSwitcha , IEndOfInitLoopSwitchIndex )
    
    # Input datas
    IInitLoopSwitchIndex.Input( 0 )
    IInitLoopSwitchMin.Input( -5 )
    IInitLoopSwitchMax.Input( 10 )
    
    # Output Ports of the graph
    #OEndOfInitLoopSwitchIndex = EndOfInitLoopSwitch.GetOutPort( 'Index' )
    #OEndOfInitLoopSwitchMin = EndOfInitLoopSwitch.GetOutPort( 'Min' )
    #OEndOfInitLoopSwitchMax = EndOfInitLoopSwitch.GetOutPort( 'Max' )
    return GraphSwitch_1_1


GraphSwitch_1_1 = DefGraphSwitch_1_1()
