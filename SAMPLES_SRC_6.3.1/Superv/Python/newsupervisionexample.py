#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
#  File   : newsupervisionexample.py
#  Module : SuperVisionTest
#
from SuperV import *
# Graph creation 
newsupervisionexample = Graph( 'newsupervisionexample' )
newsupervisionexample.SetName( 'newsupervisionexample' )
newsupervisionexample.SetAuthor( '' )
newsupervisionexample.SetComment( '' )
newsupervisionexample.Coords( 0 , 0 )

# Creation of Computing Nodes
Add = newsupervisionexample.Node( 'AddComponent' , 'AddComponent' , 'Add' )
Add.SetName( 'Add' )
Add.SetAuthor( '' )
Add.SetContainer( 'FactoryServer' )
Add.SetComment( '' )
Add.Coords( 15 , 241 )
Sub = newsupervisionexample.Node( 'SubComponent' , 'SubComponent' , 'Sub' )
Sub.SetName( 'Sub' )
Sub.SetAuthor( '' )
Sub.SetContainer( 'FactoryServer' )
Sub.SetComment( '' )
Sub.Coords( 227 , 99 )
Mul = newsupervisionexample.Node( 'MulComponent' , 'MulComponent' , 'Mul' )
Mul.SetName( 'Mul' )
Mul.SetAuthor( '' )
Mul.SetContainer( 'FactoryServer' )
Mul.SetComment( '' )
Mul.Coords( 443 , 278 )
Div = newsupervisionexample.Node( 'DivComponent' , 'DivComponent' , 'Div' )
Div.SetName( 'Div' )
Div.SetAuthor( '' )
Div.SetContainer( 'FactoryServer' )
Div.SetComment( '' )
Div.Coords( 634 , 97 )

# Creation of intermediate Output variables and of Computing Links
Addz = Add.Port( 'z' )
Suby = newsupervisionexample.Link( Addz , Sub.Port( 'y' ) )
Muly = newsupervisionexample.Link( Addz , Mul.Port( 'y' ) )
Subz = Sub.Port( 'z' )
Mulx = newsupervisionexample.Link( Subz , Mul.Port( 'x' ) )
Divx = newsupervisionexample.Link( Subz , Div.Port( 'x' ) )
Mulz = Mul.Port( 'z' )
Divy = newsupervisionexample.Link( Mulz , Div.Port( 'y' ) )

# Creation of Input datas
Addx = Add.Input( 'x' , 3)
Addy = Add.Input( 'y' , 4.5)
Subx = Sub.Input( 'x' , 1.5)

# Creation of Output variables
AddFuncValue = Add.Port( 'FuncValue' )
Divz = Div.Port( 'z' )

newsupervisionexample.Run()

newsupervisionexample.DoneW()

newsupervisionexample.State()

newsupervisionexample.PrintPorts()

