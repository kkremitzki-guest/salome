#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph GraphGOTO1
#
from SuperV import *

# Graph creation of GraphGOTO1
def DefGraphGOTO1() :
    GraphGOTO1 = Graph( 'GraphGOTO1' )
    GraphGOTO1.SetName( 'GraphGOTO1' )
    GraphGOTO1.SetAuthor( 'JR' )
    GraphGOTO1.SetComment( 'Syracuse algorithm' )
    GraphGOTO1.Coords( 0 , 0 )
    
    # Creation of Factory Nodes
    
    test_ISEVEN = GraphGOTO1.FNode( 'SyrComponent' , 'SyrComponent' , 'C_ISEVEN' )
    test_ISEVEN.SetName( 'test_ISEVEN' )
    test_ISEVEN.SetAuthor( '' )
    test_ISEVEN.SetContainer( 'localhost/FactoryServer' )
    test_ISEVEN.SetComment( 'C_ISEVEN from SyrComponent' )
    test_ISEVEN.Coords( 195 , 417 )
    Itest_ISEVENanInteger = test_ISEVEN.GetInPort( 'anInteger' )
    Itest_ISEVENGate = test_ISEVEN.GetInPort( 'Gate' )
    Otest_ISEVENBoolEven = test_ISEVEN.GetOutPort( 'BoolEven' )
    Otest_ISEVENGate = test_ISEVEN.GetOutPort( 'Gate' )
    
    test_ISONE = GraphGOTO1.FNode( 'SyrComponent' , 'SyrComponent' , 'C_ISONE' )
    test_ISONE.SetName( 'test_ISONE' )
    test_ISONE.SetAuthor( '' )
    test_ISONE.SetContainer( 'localhost/FactoryServer' )
    test_ISONE.SetComment( 'C_ISONE from SyrComponent' )
    test_ISONE.Coords( 201 , 145 )
    Itest_ISONEanInteger = test_ISONE.GetInPort( 'anInteger' )
    Itest_ISONEGate = test_ISONE.GetInPort( 'Gate' )
    Otest_ISONEBoolOne = test_ISONE.GetOutPort( 'BoolOne' )
    Otest_ISONEGate = test_ISONE.GetOutPort( 'Gate' )
    
    m3p1 = GraphGOTO1.FNode( 'SyrComponent' , 'SyrComponent' , 'C_M3P1' )
    m3p1.SetName( 'm3p1' )
    m3p1.SetAuthor( '' )
    m3p1.SetContainer( 'localhost/FactoryServer' )
    m3p1.SetComment( 'C_M3P1 from SyrComponent' )
    m3p1.Coords( 861 , 46 )
    Im3p1anOddInteger = m3p1.GetInPort( 'anOddInteger' )
    Im3p1Gate = m3p1.GetInPort( 'Gate' )
    Om3p1anEvenInteger = m3p1.GetOutPort( 'anEvenInteger' )
    Om3p1Gate = m3p1.GetOutPort( 'Gate' )
    
    div2 = GraphGOTO1.FNode( 'SyrComponent' , 'SyrComponent' , 'C_DIV2' )
    div2.SetName( 'div2' )
    div2.SetAuthor( '' )
    div2.SetContainer( 'localhost/FactoryServer' )
    div2.SetComment( 'C_DIV2 from SyrComponent' )
    div2.Coords( 858 , 466 )
    Idiv2anEvenInteger = div2.GetInPort( 'anEvenInteger' )
    Idiv2Gate = div2.GetInPort( 'Gate' )
    Odiv2anInteger = div2.GetOutPort( 'anInteger' )
    Odiv2Gate = div2.GetOutPort( 'Gate' )
    
    incr = GraphGOTO1.FNode( 'SyrComponent' , 'SyrComponent' , 'C_INCR' )
    incr.SetName( 'incr' )
    incr.SetAuthor( '' )
    incr.SetContainer( 'localhost/FactoryServer' )
    incr.SetComment( 'C_INCR from SyrComponent' )
    incr.Coords( 865 , 169 )
    IincraCount = incr.GetInPort( 'aCount' )
    IincrGate = incr.GetInPort( 'Gate' )
    OincraNewCount = incr.GetOutPort( 'aNewCount' )
    OincrGate = incr.GetOutPort( 'Gate' )
    
    # Creation of InLine Nodes
    Pylabel_begin = []
    Pylabel_begin.append( 'def label_begin( NB , KB ):' )
    Pylabel_begin.append( '    print "label_begin",NB,KB' )
    Pylabel_begin.append( '    return NB,KB' )
    label_begin = GraphGOTO1.INode( 'label_begin' , Pylabel_begin )
    label_begin.SetName( 'label_begin' )
    label_begin.SetAuthor( '' )
    label_begin.SetComment( 'Python function' )
    label_begin.Coords( 9 , 250 )
    Ilabel_beginNB = label_begin.InPort( 'NB' , 'long' )
    Ilabel_beginKB = label_begin.InPort( 'KB' , 'long' )
    Ilabel_beginGate = label_begin.GetInPort( 'Gate' )
    Olabel_beginNT = label_begin.OutPort( 'NT' , 'long' )
    Olabel_beginKT = label_begin.OutPort( 'KT' , 'long' )
    Olabel_beginGate = label_begin.GetOutPort( 'Gate' )
    
    Pylabel_test = []
    Pylabel_test.append( 'def label_test( ValEven , ValOne , NB , KB ):' )
    Pylabel_test.append( '    print "label_begin",ValEven,ValOne,NB,KB' )
    Pylabel_test.append( '    return ValEven,ValOne,NB,KB' )
    label_test = GraphGOTO1.INode( 'label_test' , Pylabel_test )
    label_test.SetName( 'label_test' )
    label_test.SetAuthor( '' )
    label_test.SetComment( 'Python function' )
    label_test.Coords( 396 , 193 )
    Ilabel_testValEven = label_test.InPort( 'ValEven' , 'long' )
    Ilabel_testValOne = label_test.InPort( 'ValOne' , 'long' )
    Ilabel_testNT = label_test.InPort( 'NT' , 'long' )
    Ilabel_testKT = label_test.InPort( 'KT' , 'long' )
    Ilabel_testGate = label_test.GetInPort( 'Gate' )
    Olabel_testValEven = label_test.OutPort( 'ValEven' , 'long' )
    Olabel_testValOne = label_test.OutPort( 'ValOne' , 'long' )
    Olabel_testNT = label_test.OutPort( 'NT' , 'long' )
    Olabel_testKT = label_test.OutPort( 'KT' , 'long' )
    Olabel_testGate = label_test.GetOutPort( 'Gate' )
    
    # Creation of Switch Nodes
    Pytest = []
    Pytest.append( 'def Switch_OneEven( ValOne , ValEven , NT , KT ) :' )
    Pytest.append( '    Finished = ValOne' )
    Pytest.append( '    if Finished == 0 :' )
    Pytest.append( '        Incr = 1' )
    Pytest.append( '        Even = ValEven' )
    Pytest.append( '        if Even == 0 :' )
    Pytest.append( '            Odd = 1' )
    Pytest.append( '        else :' )
    Pytest.append( '            Odd = 0' )
    Pytest.append( '    else :' )
    Pytest.append( '        Incr = 0' )
    Pytest.append( '        Even = 0' )
    Pytest.append( '        Odd = 0' )
    Pytest.append( '    Even = ValEven' )
    Pytest.append( '    return Finished,Incr,Even,Odd,NT,KT' )
    test,EndSwitch_OneEven = GraphGOTO1.SNode( 'Switch_OneEven' , Pytest )
    EndSwitch_OneEven.SetName( 'EndSwitch_OneEven' )
    EndSwitch_OneEven.SetAuthor( '' )
    EndSwitch_OneEven.SetComment( 'Compute Node' )
    EndSwitch_OneEven.Coords( 1256 , 305 )
    PyEndSwitch_OneEven = []
    PyEndSwitch_OneEven.append( 'def EndSwitch_OneEven( Finished , K ):' )
    PyEndSwitch_OneEven.append( '    print "label_begin",Finished,K' )
    PyEndSwitch_OneEven.append( '    return Finished,K' )
    EndSwitch_OneEven.SetPyFunction( 'EndSwitch_OneEven' , PyEndSwitch_OneEven )
    IEndSwitch_OneEvenFinished = EndSwitch_OneEven.InPort( 'Finished' , 'long' )
    IEndSwitch_OneEvenK = EndSwitch_OneEven.InPort( 'K' , 'long' )
    IEndSwitch_OneEvenDefault = EndSwitch_OneEven.GetInPort( 'Default' )
    OEndSwitch_OneEvenFinished = EndSwitch_OneEven.OutPort( 'Finished' , 'long' )
    OEndSwitch_OneEvenK = EndSwitch_OneEven.OutPort( 'K' , 'long' )
    OEndSwitch_OneEvenGate = EndSwitch_OneEven.GetOutPort( 'Gate' )
    test.SetName( 'test' )
    test.SetAuthor( '' )
    test.SetComment( 'Compute Node' )
    test.Coords( 595 , 239 )
    ItestValOne = test.InPort( 'ValOne' , 'long' )
    ItestValEven = test.InPort( 'ValEven' , 'long' )
    ItestNT = test.InPort( 'NT' , 'long' )
    ItestKT = test.InPort( 'KT' , 'long' )
    ItestGate = test.GetInPort( 'Gate' )
    OtestFinished = test.OutPort( 'Finished' , 'long' )
    OtestIncr = test.OutPort( 'Incr' , 'long' )
    OtestEven = test.OutPort( 'Even' , 'long' )
    OtestOdd = test.OutPort( 'Odd' , 'long' )
    OtestN = test.OutPort( 'N' , 'long' )
    OtestK = test.OutPort( 'K' , 'long' )
    OtestDefault = test.GetOutPort( 'Default' )
    
    # Creation of GOTO Nodes
    Pycontrol_m3p1 = []
    Pycontrol_m3p1.append( 'def control_m3p1( N , K ):' )
    Pycontrol_m3p1.append( '    return 0,1,N,K' )
    control_m3p1 = GraphGOTO1.GNode( 'control_m3p1' , Pycontrol_m3p1 , 'label_test' )
    control_m3p1.SetName( 'control_m3p1' )
    control_m3p1.SetAuthor( '' )
    control_m3p1.SetComment( 'Compute Node' )
    control_m3p1.Coords( 1073 , 87 )
    Icontrol_m3p1N = control_m3p1.InPort( 'N' , 'long' )
    Icontrol_m3p1K = control_m3p1.InPort( 'K' , 'long' )
    Icontrol_m3p1Gate = control_m3p1.GetInPort( 'Gate' )
    Ocontrol_m3p1ValOne = control_m3p1.OutPort( 'ValOne' , 'long' )
    Ocontrol_m3p1ValEven = control_m3p1.OutPort( 'ValEven' , 'long' )
    Ocontrol_m3p1NT = control_m3p1.OutPort( 'NT' , 'long' )
    Ocontrol_m3p1KT = control_m3p1.OutPort( 'KT' , 'long' )
    Ocontrol_m3p1Gate = control_m3p1.GetOutPort( 'Gate' )
    
    Pycontrol_div2 = []
    Pycontrol_div2.append( 'def control_div2( N , NB ) :' )
    Pycontrol_div2.append( '    return N,NB' )
    control_div2 = GraphGOTO1.GNode( 'control_div2' , Pycontrol_div2 , 'label_begin' )
    control_div2.SetName( 'control_div2' )
    control_div2.SetAuthor( '' )
    control_div2.SetComment( 'Compute Node' )
    control_div2.Coords( 1128 , 453 )
    Icontrol_div2N = control_div2.InPort( 'N' , 'long' )
    Icontrol_div2K = control_div2.InPort( 'K' , 'long' )
    Icontrol_div2Gate = control_div2.GetInPort( 'Gate' )
    Ocontrol_div2NB = control_div2.OutPort( 'NB' , 'long' )
    Ocontrol_div2KB = control_div2.OutPort( 'KB' , 'long' )
    Ocontrol_div2Gate = control_div2.GetOutPort( 'Gate' )
    
    # Creation of Links
    Ltest_ISEVENBoolEvenlabel_testValEven = GraphGOTO1.Link( Otest_ISEVENBoolEven , Ilabel_testValEven )
    Ltest_ISEVENBoolEvenlabel_testValEven.AddCoord( 1 , 369 , 273 )
    Ltest_ISEVENBoolEvenlabel_testValEven.AddCoord( 2 , 370 , 498 )
    
    Ltest_ISONEBoolOnelabel_testValOne = GraphGOTO1.Link( Otest_ISONEBoolOne , Ilabel_testValOne )
    Ltest_ISONEBoolOnelabel_testValOne.AddCoord( 1 , 384 , 281 )
    Ltest_ISONEBoolOnelabel_testValOne.AddCoord( 2 , 384 , 225 )
    
    Lm3p1anEvenIntegercontrol_m3p1N = GraphGOTO1.Link( Om3p1anEvenInteger , Icontrol_m3p1N )
    
    Ldiv2anIntegercontrol_div2N = GraphGOTO1.Link( Odiv2anInteger , Icontrol_div2N )
    
    LincraNewCountcontrol_m3p1K = GraphGOTO1.Link( OincraNewCount , Icontrol_m3p1K )
    LincraNewCountcontrol_m3p1K.AddCoord( 1 , 1048 , 139 )
    LincraNewCountcontrol_m3p1K.AddCoord( 2 , 1048 , 241 )
    
    LincraNewCountcontrol_div2K = GraphGOTO1.Link( OincraNewCount , Icontrol_div2K )
    LincraNewCountcontrol_div2K.AddCoord( 1 , 1052 , 504 )
    LincraNewCountcontrol_div2K.AddCoord( 2 , 1049 , 239 )
    
    Llabel_beginNTlabel_testNT = GraphGOTO1.Link( Olabel_beginNT , Ilabel_testNT )
    
    Llabel_beginNTtest_ISONEanInteger = GraphGOTO1.Link( Olabel_beginNT , Itest_ISONEanInteger )
    Llabel_beginNTtest_ISONEanInteger.AddCoord( 1 , 192 , 226 )
    Llabel_beginNTtest_ISONEanInteger.AddCoord( 2 , 191 , 331 )
    
    Llabel_beginNTtest_ISEVENanInteger = GraphGOTO1.Link( Olabel_beginNT , Itest_ISEVENanInteger )
    Llabel_beginNTtest_ISEVENanInteger.AddCoord( 1 , 191 , 494 )
    Llabel_beginNTtest_ISEVENanInteger.AddCoord( 2 , 192 , 331 )
    
    Llabel_beginKTlabel_testKT = GraphGOTO1.Link( Olabel_beginKT , Ilabel_testKT )
    
    Llabel_testValEventestValEven = GraphGOTO1.Link( Olabel_testValEven , ItestValEven )
    
    Llabel_testValOnetestValOne = GraphGOTO1.Link( Olabel_testValOne , ItestValOne )
    
    Llabel_testNTtestNT = GraphGOTO1.Link( Olabel_testNT , ItestNT )
    
    Llabel_testKTtestKT = GraphGOTO1.Link( Olabel_testKT , ItestKT )
    
    LtestFinishedEndSwitch_OneEvenFinished = GraphGOTO1.Link( OtestFinished , IEndSwitch_OneEvenFinished )
    
    LtestEvendiv2Gate = GraphGOTO1.Link( OtestEven , Idiv2Gate )
    LtestEvendiv2Gate.AddCoord( 1 , 793 , 561 )
    LtestEvendiv2Gate.AddCoord( 2 , 794 , 310 )
    
    LtestOddm3p1Gate = GraphGOTO1.Link( OtestOdd , Im3p1Gate )
    LtestOddm3p1Gate.AddCoord( 1 , 778 , 138 )
    LtestOddm3p1Gate.AddCoord( 2 , 780 , 328 )
    
    LtestNm3p1anOddInteger = GraphGOTO1.Link( OtestN , Im3p1anOddInteger )
    LtestNm3p1anOddInteger.AddCoord( 1 , 808 , 113 )
    LtestNm3p1anOddInteger.AddCoord( 2 , 807 , 352 )
    
    LtestNdiv2anEvenInteger = GraphGOTO1.Link( OtestN , Idiv2anEvenInteger )
    LtestNdiv2anEvenInteger.AddCoord( 1 , 806 , 537 )
    LtestNdiv2anEvenInteger.AddCoord( 2 , 807 , 351 )
    
    LtestKEndSwitch_OneEvenK = GraphGOTO1.Link( OtestK , IEndSwitch_OneEvenK )
    
    LtestKincraCount = GraphGOTO1.Link( OtestK , IincraCount )
    LtestKincraCount.AddCoord( 1 , 773 , 236 )
    LtestKincraCount.AddCoord( 2 , 773 , 370 )
    
    LtestDefaultEndSwitch_OneEvenDefault = GraphGOTO1.Link( OtestDefault , IEndSwitch_OneEvenDefault )
    LtestDefaultEndSwitch_OneEvenDefault.AddCoord( 1 , 840 , 381 )
    LtestDefaultEndSwitch_OneEvenDefault.AddCoord( 2 , 839 , 394 )
    
    Lcontrol_m3p1Gatelabel_testGate = GraphGOTO1.Link( Ocontrol_m3p1Gate , Ilabel_testGate )
    Lcontrol_m3p1Gatelabel_testGate.AddCoord( 1 , 388 , 388 )
    Lcontrol_m3p1Gatelabel_testGate.AddCoord( 2 , 389 , 597 )
    Lcontrol_m3p1Gatelabel_testGate.AddCoord( 3 , 1441 , 604 )
    Lcontrol_m3p1Gatelabel_testGate.AddCoord( 4 , 1441 , 199 )
    
    Lcontrol_div2Gatelabel_beginGate = GraphGOTO1.Link( Ocontrol_div2Gate , Ilabel_beginGate )
    Lcontrol_div2Gatelabel_beginGate.AddCoord( 1 , 4 , 388 )
    Lcontrol_div2Gatelabel_beginGate.AddCoord( 2 , 3 , 587 )
    Lcontrol_div2Gatelabel_beginGate.AddCoord( 3 , 1307 , 586 )
    Lcontrol_div2Gatelabel_beginGate.AddCoord( 4 , 1307 , 528 )
    
    # Input datas
    Ilabel_beginNB.Input( 7 )
    Ilabel_beginKB.Input( 0 )
    
    # Output Ports of the graph
    #OtestIncr = test.GetOutPort( 'Incr' )
    #OEndSwitch_OneEvenFinished = EndSwitch_OneEven.GetOutPort( 'Finished' )
    #OEndSwitch_OneEvenK = EndSwitch_OneEven.GetOutPort( 'K' )
    return GraphGOTO1


GraphGOTO1 = DefGraphGOTO1()

GraphGOTO1.Run()
GraphGOTO1.DoneW()
GraphGOTO1.State()
GraphGOTO1.PrintPorts()
