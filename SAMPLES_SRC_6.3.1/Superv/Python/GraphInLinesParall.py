#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph GraphInLines
#
from SuperV import *
# Graph creation 
GraphInLinesParall = Graph( 'GraphInLinesParall' )
GraphInLinesParall.SetName( 'GraphInLinesParall' )
GraphInLinesParall.SetAuthor( '' )
GraphInLinesParall.SetComment( '' )
GraphInLinesParall.Coords( 0 , 0 )

# Creation of Factory Nodes

# Creation of InLine Nodes
PyAdd = []
PyAdd.append( 'import time ' )
PyAdd.append( 'def Add(a,b) :   ' )
PyAdd.append( '    print "Add will wait 5 seconds" ' )
PyAdd.append( '    time.sleep(5) ' )
PyAdd.append( '    print "Add waited" ' )
PyAdd.append( '    return a+b   ' )
PyAdd.append( '' )
Add = GraphInLinesParall.INode( 'Add' , PyAdd )
Add.InPort( 'a' , 'long' )
Add.InPort( 'b' , 'long' )
Add.OutPort( 'f' , 'long' )
Add.SetName( 'Add' )
Add.SetAuthor( '' )
Add.SetComment( 'Python function' )
Add.Coords( 351 , 77 )

PySub = []
PySub.append( 'def Sub(a,b) : ' )
PySub.append( '    return a-b ' )
PySub.append( '' )
Sub = GraphInLinesParall.INode( 'Sub' , PySub )
Sub.InPort( 'a' , 'long' )
Sub.InPort( 'b' , 'long' )
Sub.OutPort( 'f' , 'long' )
Sub.SetName( 'Sub' )
Sub.SetAuthor( '' )
Sub.SetComment( 'Python function' )
Sub.Coords( 86 , 333 )

PyMul = []
PyMul.append( 'import time  ' )
PyMul.append( 'def Mul(a,b) :    ' )
PyMul.append( '    print "Mul will wait 5 seconds"  ' )
PyMul.append( '    time.sleep(5)  ' )
PyMul.append( '    print "Mul waited"  ' )
PyMul.append( '    return a*b  ' )
Mul = GraphInLinesParall.INode( 'Mul' , PyMul )
Mul.InPort( 'a' , 'long' )
Mul.InPort( 'b' , 'long' )
Mul.OutPort( 'Result' , 'long' )
Mul.SetName( 'Mul' )
Mul.SetAuthor( '' )
Mul.SetComment( 'Python function' )
Mul.Coords( 616 , 247 )

# Creation of Links
Subf = Sub.Port( 'f' )
Mulb = GraphInLinesParall.Link( Subf , Mul.Port( 'b' ) )
Mulb.AddCoord( 1 , 583 , 357 )
Mulb.AddCoord( 2 , 583 , 413 )
Mulb.AddCoord( 3 , 282 , 413 )

Addb = GraphInLinesParall.Link( Subf , Add.Port( 'b' ) )
Addb.AddCoord( 1 , 282 , 186 )
Addb.AddCoord( 2 , 283 , 413 )

Mula = GraphInLinesParall.Link( Subf , Mul.Port( 'a' ) )
Mula.AddCoord( 1 , 583 , 328 )
Mula.AddCoord( 2 , 583 , 412 )

# Creation of Input datas
Adda = Add.Input( 'a' , 1)
Suba = Sub.Input( 'a' , 3)
Subb = Sub.Input( 'b' , 4)

# Creation of Output variables
Addf = Add.Port( 'f' )
MulResult = Mul.Port( 'Result' )

GraphInLinesParall.Run()
GraphInLinesParall.DoneW()
GraphInLinesParall.PrintPorts()
