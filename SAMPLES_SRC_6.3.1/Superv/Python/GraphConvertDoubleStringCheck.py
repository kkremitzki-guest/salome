#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph GraphConvertDoubleStringCheck
#
from SuperV import *
# Graph creation 
GraphConvertDoubleStringCheck = Graph( 'GraphConvertDoubleStringCheck' )
GraphConvertDoubleStringCheck.SetName( 'GraphConvertDoubleStringCheck' )
GraphConvertDoubleStringCheck.SetAuthor( 'JR' )
GraphConvertDoubleStringCheck.SetComment( 'Check conversions of String' )
GraphConvertDoubleStringCheck.Coords( 0 , 0 )

# Creation of Factory Nodes

MiscTypes = GraphConvertDoubleStringCheck.FNode( 'TypesCheck' , 'TypesCheck' , 'MiscTypes' )
MiscTypes.SetName( 'MiscTypes' )
MiscTypes.SetAuthor( '' )
MiscTypes.SetContainer( 'localhost/FactoryServer' )
MiscTypes.SetComment( 'MiscTypes from TypesCheck' )
MiscTypes.Coords( 284 , 28 )

# Creation of InLine Nodes
PyDoubleString = []
PyDoubleString.append( 'def DoubleString() :  ' )
PyDoubleString.append( '    string = "3.1415926535"  ' )
PyDoubleString.append( '    return string  ' )
PyDoubleString.append( ' ' )
DoubleString = GraphConvertDoubleStringCheck.INode( 'DoubleString' , PyDoubleString )
DoubleString.OutPort( 'OutString' , 'string' )
DoubleString.SetName( 'DoubleString' )
DoubleString.SetAuthor( 'JR' )
DoubleString.SetComment( 'InLine Node' )
DoubleString.Coords( 14 , 114 )

# Creation of Links
DoubleStringOutString = DoubleString.Port( 'OutString' )
MiscTypesInString = GraphConvertDoubleStringCheck.Link( DoubleStringOutString , MiscTypes.Port( 'InString' ) )

MiscTypesInBool = GraphConvertDoubleStringCheck.Link( DoubleStringOutString , MiscTypes.Port( 'InBool' ) )

MiscTypesInChar = GraphConvertDoubleStringCheck.Link( DoubleStringOutString , MiscTypes.Port( 'InChar' ) )

MiscTypesInShort = GraphConvertDoubleStringCheck.Link( DoubleStringOutString , MiscTypes.Port( 'InShort' ) )

MiscTypesInLong = GraphConvertDoubleStringCheck.Link( DoubleStringOutString , MiscTypes.Port( 'InLong' ) )

MiscTypesInFloat = GraphConvertDoubleStringCheck.Link( DoubleStringOutString , MiscTypes.Port( 'InFloat' ) )

MiscTypesInDouble = GraphConvertDoubleStringCheck.Link( DoubleStringOutString , MiscTypes.Port( 'InDouble' ) )

MiscTypesInObjRef = GraphConvertDoubleStringCheck.Link( DoubleStringOutString , MiscTypes.Port( 'InObjRef' ) )

# Creation of Output variables
MiscTypesOutString = MiscTypes.Port( 'OutString' )
MiscTypesOutBool = MiscTypes.Port( 'OutBool' )
MiscTypesOutChar = MiscTypes.Port( 'OutChar' )
MiscTypesOutShort = MiscTypes.Port( 'OutShort' )
MiscTypesOutLong = MiscTypes.Port( 'OutLong' )
MiscTypesOutFloat = MiscTypes.Port( 'OutFloat' )
MiscTypesOutDouble = MiscTypes.Port( 'OutDouble' )
MiscTypesOutObjRef = MiscTypes.Port( 'OutObjRef' )

GraphConvertDoubleStringCheck.Run()
GraphConvertDoubleStringCheck.DoneW()
GraphConvertDoubleStringCheck.PrintPorts()
