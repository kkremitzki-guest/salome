#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph GraphConvertFloatStringCheck
#
from SuperV import *
# Graph creation 
GraphConvertFloatStringCheck = Graph( 'GraphConvertFloatStringCheck' )
GraphConvertFloatStringCheck.SetName( 'GraphConvertFloatStringCheck' )
GraphConvertFloatStringCheck.SetAuthor( 'JR' )
GraphConvertFloatStringCheck.SetComment( 'Check conversions of String' )
GraphConvertFloatStringCheck.Coords( 0 , 0 )

# Creation of Factory Nodes

MiscTypes = GraphConvertFloatStringCheck.FNode( 'TypesCheck' , 'TypesCheck' , 'MiscTypes' )
MiscTypes.SetName( 'MiscTypes' )
MiscTypes.SetAuthor( '' )
MiscTypes.SetContainer( 'localhost/FactoryServer' )
MiscTypes.SetComment( 'MiscTypes from TypesCheck' )
MiscTypes.Coords( 284 , 28 )

# Creation of InLine Nodes
PyFloatString = []
PyFloatString.append( 'def FloatString() :  ' )
PyFloatString.append( '    string = "3.1415926535"  ' )
PyFloatString.append( '    return string  ' )
PyFloatString.append( ' ' )
FloatString = GraphConvertFloatStringCheck.INode( 'FloatString' , PyFloatString )
FloatString.OutPort( 'OutString' , 'string' )
FloatString.SetName( 'FloatString' )
FloatString.SetAuthor( 'JR' )
FloatString.SetComment( 'InLine Node' )
FloatString.Coords( 14 , 114 )

# Creation of Links
FloatStringOutString = FloatString.Port( 'OutString' )
MiscTypesInString = GraphConvertFloatStringCheck.Link( FloatStringOutString , MiscTypes.Port( 'InString' ) )

MiscTypesInBool = GraphConvertFloatStringCheck.Link( FloatStringOutString , MiscTypes.Port( 'InBool' ) )

MiscTypesInChar = GraphConvertFloatStringCheck.Link( FloatStringOutString , MiscTypes.Port( 'InChar' ) )

MiscTypesInShort = GraphConvertFloatStringCheck.Link( FloatStringOutString , MiscTypes.Port( 'InShort' ) )

MiscTypesInLong = GraphConvertFloatStringCheck.Link( FloatStringOutString , MiscTypes.Port( 'InLong' ) )

MiscTypesInFloat = GraphConvertFloatStringCheck.Link( FloatStringOutString , MiscTypes.Port( 'InFloat' ) )

MiscTypesInDouble = GraphConvertFloatStringCheck.Link( FloatStringOutString , MiscTypes.Port( 'InDouble' ) )

MiscTypesInObjRef = GraphConvertFloatStringCheck.Link( FloatStringOutString , MiscTypes.Port( 'InObjRef' ) )

# Creation of Output variables
MiscTypesOutString = MiscTypes.Port( 'OutString' )
MiscTypesOutBool = MiscTypes.Port( 'OutBool' )
MiscTypesOutChar = MiscTypes.Port( 'OutChar' )
MiscTypesOutShort = MiscTypes.Port( 'OutShort' )
MiscTypesOutLong = MiscTypes.Port( 'OutLong' )
MiscTypesOutFloat = MiscTypes.Port( 'OutFloat' )
MiscTypesOutDouble = MiscTypes.Port( 'OutDouble' )
MiscTypesOutObjRef = MiscTypes.Port( 'OutObjRef' )

GraphConvertFloatStringCheck.Run()
GraphConvertFloatStringCheck.DoneW()
GraphConvertFloatStringCheck.PrintPorts()
