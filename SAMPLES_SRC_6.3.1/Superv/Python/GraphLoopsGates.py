#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph GraphLoopsGates_1
#
from SuperV import *

# Graph creation of GraphLoopsGates_1
def DefGraphLoopsGates_1() :
    GraphLoopsGates_1 = Graph( 'GraphLoopsGates_1' )
    GraphLoopsGates_1.SetName( 'GraphLoopsGates_1' )
    GraphLoopsGates_1.SetAuthor( 'JR' )
    GraphLoopsGates_1.SetComment( '' )
    GraphLoopsGates_1.Coords( 0 , 0 )
    
    # Creation of Factory Nodes
    
    Add = GraphLoopsGates_1.FNode( 'AddComponent' , 'AddComponent' , 'Add' )
    Add.SetName( 'Add' )
    Add.SetAuthor( '' )
    Add.SetContainer( 'localhost/FactoryServer' )
    Add.SetComment( 'Add from AddComponent' )
    Add.Coords( 230 , 76 )
    IAddx = Add.GetInPort( 'x' )
    IAddy = Add.GetInPort( 'y' )
    IAddGate = Add.GetInPort( 'Gate' )
    OAddFuncValue = Add.GetOutPort( 'FuncValue' )
    OAddz = Add.GetOutPort( 'z' )
    OAddGate = Add.GetOutPort( 'Gate' )
    
    Add_1 = GraphLoopsGates_1.FNode( 'AddComponent' , 'AddComponent' , 'Add' )
    Add_1.SetName( 'Add_1' )
    Add_1.SetAuthor( '' )
    Add_1.SetContainer( 'localhost/FactoryServer' )
    Add_1.SetComment( 'Add from AddComponent' )
    Add_1.Coords( 225 , 304 )
    IAdd_1x = Add_1.GetInPort( 'x' )
    IAdd_1y = Add_1.GetInPort( 'y' )
    IAdd_1Gate = Add_1.GetInPort( 'Gate' )
    OAdd_1FuncValue = Add_1.GetOutPort( 'FuncValue' )
    OAdd_1z = Add_1.GetOutPort( 'z' )
    OAdd_1Gate = Add_1.GetOutPort( 'Gate' )
    
    Sub = GraphLoopsGates_1.FNode( 'SubComponent' , 'SubComponent' , 'Sub' )
    Sub.SetName( 'Sub' )
    Sub.SetAuthor( '' )
    Sub.SetContainer( 'localhost/FactoryServer' )
    Sub.SetComment( 'Sub from SubComponent' )
    Sub.Coords( 447 , 77 )
    ISubx = Sub.GetInPort( 'x' )
    ISuby = Sub.GetInPort( 'y' )
    ISubGate = Sub.GetInPort( 'Gate' )
    OSubz = Sub.GetOutPort( 'z' )
    OSubGate = Sub.GetOutPort( 'Gate' )
    
    Sub_1 = GraphLoopsGates_1.FNode( 'SubComponent' , 'SubComponent' , 'Sub' )
    Sub_1.SetName( 'Sub_1' )
    Sub_1.SetAuthor( '' )
    Sub_1.SetContainer( 'localhost/FactoryServer' )
    Sub_1.SetComment( 'Sub from SubComponent' )
    Sub_1.Coords( 448 , 304 )
    ISub_1x = Sub_1.GetInPort( 'x' )
    ISub_1y = Sub_1.GetInPort( 'y' )
    ISub_1Gate = Sub_1.GetInPort( 'Gate' )
    OSub_1z = Sub_1.GetOutPort( 'z' )
    OSub_1Gate = Sub_1.GetOutPort( 'Gate' )
    
    # Creation of InLine Nodes
    PyGate = []
    PyGate.append( 'from time import *  ' )
    PyGate.append( 'def Gate(G1,G2) :  ' )
    PyGate.append( '    sleep(1)  ' )
    PyGate.append( '    return G1&G2  ' )
    Gate = GraphLoopsGates_1.INode( 'Gate' , PyGate )
    Gate.SetName( 'Gate' )
    Gate.SetAuthor( '' )
    Gate.SetComment( 'Compute Node' )
    Gate.Coords( 640 , 224 )
    IGateG1 = Gate.InPort( 'G1' , 'long' )
    IGateG2 = Gate.InPort( 'G2' , 'long' )
    IGateGate = Gate.GetInPort( 'Gate' )
    OGateG = Gate.OutPort( 'G' , 'long' )
    OGateGate = Gate.GetOutPort( 'Gate' )
    
    # Creation of Loop Nodes
    PyInit = []
    PyInit.append( 'from time import *    ' )
    PyInit.append( 'def Init(Index,Min,Max) :    ' )
    PyInit.append( '    Index = Min    ' )
    PyInit.append( '    sleep(1)    ' )
    PyInit.append( '    return Index,Min,Max    ' )
    PyMoreInit = []
    PyMoreInit.append( 'def More(Index,Min,Max) :   ' )
    PyMoreInit.append( '    if Index < Max :   ' )
    PyMoreInit.append( '        DoLoop = 1   ' )
    PyMoreInit.append( '    else :   ' )
    PyMoreInit.append( '        DoLoop = 0  ' )
    PyMoreInit.append( '    return DoLoop,Index,Min,Max   ' )
    PyNextInit = []
    PyNextInit.append( 'from time import * ' )
    PyNextInit.append( 'def Next(Index,Min,Max) :   ' )
    PyNextInit.append( '    Index = Index + 1 ' )
    PyNextInit.append( '    sleep(1) ' )
    PyNextInit.append( '    return Index,Min,Max    ' )
    Init,EndInit = GraphLoopsGates_1.LNode( 'Init' , PyInit , 'More' , PyMoreInit , 'Next' , PyNextInit )
    EndInit.SetName( 'EndInit' )
    EndInit.SetAuthor( '' )
    EndInit.SetComment( 'Compute Node' )
    EndInit.Coords( 838 , 243 )
    PyEndInit = []
    EndInit.SetPyFunction( 'EndInit' , PyEndInit )
    IInitDoLoop = Init.GetInPort( 'DoLoop' )
    IInitIndex = Init.InPort( 'Index' , 'long' )
    IInitMin = Init.InPort( 'Min' , 'long' )
    IInitMax = Init.InPort( 'Max' , 'long' )
    IInitGate = Init.GetInPort( 'Gate' )
    OInitDoLoop = Init.GetOutPort( 'DoLoop' )
    OInitIndex = Init.GetOutPort( 'Index' )
    OInitMin = Init.GetOutPort( 'Min' )
    OInitMax = Init.GetOutPort( 'Max' )
    IEndInitDoLoop = EndInit.GetInPort( 'DoLoop' )
    IEndInitIndex = EndInit.GetInPort( 'Index' )
    IEndInitMin = EndInit.GetInPort( 'Min' )
    IEndInitMax = EndInit.GetInPort( 'Max' )
    IEndInitGate = EndInit.GetInPort( 'Gate' )
    OEndInitDoLoop = EndInit.GetOutPort( 'DoLoop' )
    OEndInitIndex = EndInit.GetOutPort( 'Index' )
    OEndInitMin = EndInit.GetOutPort( 'Min' )
    OEndInitMax = EndInit.GetOutPort( 'Max' )
    OEndInitGate = EndInit.GetOutPort( 'Gate' )
    Init.SetName( 'Init' )
    Init.SetAuthor( '' )
    Init.SetComment( 'Compute Node' )
    Init.Coords( 29 , 251 )
    
    PyInit_1 = []
    PyInit_1.append( 'from time import *    ' )
    PyInit_1.append( 'def Init_1(Index,Min,Max) :    ' )
    PyInit_1.append( '    Index = Min    ' )
    PyInit_1.append( '    sleep(1)    ' )
    PyInit_1.append( '    return Index,Min,Max    ' )
    PyMoreInit_1 = []
    PyMoreInit_1.append( 'def More_1(Index,Min,Max) :   ' )
    PyMoreInit_1.append( '    if Index < Max :   ' )
    PyMoreInit_1.append( '        DoLoop = 1   ' )
    PyMoreInit_1.append( '    else :   ' )
    PyMoreInit_1.append( '        DoLoop = 0  ' )
    PyMoreInit_1.append( '    return DoLoop,Index,Min,Max   ' )
    PyNextInit_1 = []
    PyNextInit_1.append( 'from time import * ' )
    PyNextInit_1.append( 'def Next_1(Index,Min,Max) :   ' )
    PyNextInit_1.append( '    Index = Index + 1 ' )
    PyNextInit_1.append( '    sleep(1) ' )
    PyNextInit_1.append( '    return Index,Min,Max    ' )
    Init_1,EndInit_1 = GraphLoopsGates_1.LNode( 'Init_1' , PyInit_1 , 'More_1' , PyMoreInit_1 , 'Next_1' , PyNextInit_1 )
    EndInit_1.SetName( 'EndInit_1' )
    EndInit_1.SetAuthor( '' )
    EndInit_1.SetComment( 'Compute Node' )
    EndInit_1.Coords( 1049 , 201 )
    PyEndInit_1 = []
    EndInit_1.SetPyFunction( 'EndInit_1' , PyEndInit_1 )
    IInit_1DoLoop = Init_1.GetInPort( 'DoLoop' )
    IInit_1Index = Init_1.InPort( 'Index' , 'long' )
    IInit_1Min = Init_1.InPort( 'Min' , 'long' )
    IInit_1Max = Init_1.InPort( 'Max' , 'long' )
    IInit_1Gate = Init_1.GetInPort( 'Gate' )
    OInit_1DoLoop = Init_1.GetOutPort( 'DoLoop' )
    OInit_1Index = Init_1.GetOutPort( 'Index' )
    OInit_1Min = Init_1.GetOutPort( 'Min' )
    OInit_1Max = Init_1.GetOutPort( 'Max' )
    IEndInit_1DoLoop = EndInit_1.GetInPort( 'DoLoop' )
    IEndInit_1Index = EndInit_1.GetInPort( 'Index' )
    IEndInit_1Min = EndInit_1.GetInPort( 'Min' )
    IEndInit_1Max = EndInit_1.GetInPort( 'Max' )
    IEndInit_1Gate = EndInit_1.GetInPort( 'Gate' )
    OEndInit_1DoLoop = EndInit_1.GetOutPort( 'DoLoop' )
    OEndInit_1Index = EndInit_1.GetOutPort( 'Index' )
    OEndInit_1Min = EndInit_1.GetOutPort( 'Min' )
    OEndInit_1Max = EndInit_1.GetOutPort( 'Max' )
    OEndInit_1Gate = EndInit_1.GetOutPort( 'Gate' )
    Init_1.SetName( 'Init_1' )
    Init_1.SetAuthor( '' )
    Init_1.SetComment( 'Compute Node' )
    Init_1.Coords( 9 , 24 )
    
    # Creation of Links
    LInitIndexEndInitIndex = GraphLoopsGates_1.Link( OInitIndex , IEndInitIndex )
    
    LInitIndexSubx = GraphLoopsGates_1.Link( OInitIndex , ISubx )
    
    LInitMinEndInitMin = GraphLoopsGates_1.Link( OInitMin , IEndInitMin )
    
    LInitMinSuby = GraphLoopsGates_1.Link( OInitMin , ISuby )
    
    LInitMinSub_1x = GraphLoopsGates_1.Link( OInitMin , ISub_1x )
    
    LInitMaxEndInitMax = GraphLoopsGates_1.Link( OInitMax , IEndInitMax )
    
    LInitMaxSub_1y = GraphLoopsGates_1.Link( OInitMax , ISub_1y )
    
    LInitGateAdd_1Gate = GraphLoopsGates_1.Link( OInitGate , IAdd_1Gate )
    
    LInitGateAddGate = GraphLoopsGates_1.Link( OInitGate , IAddGate )
    
    LEndInitGateEndInit_1Gate = GraphLoopsGates_1.Link( OEndInitGate , IEndInit_1Gate )
    
    LAddGateSubGate = GraphLoopsGates_1.Link( OAddGate , ISubGate )
    
    LAdd_1GateSub_1Gate = GraphLoopsGates_1.Link( OAdd_1Gate , ISub_1Gate )
    
    LSubGateGateG1 = GraphLoopsGates_1.Link( OSubGate , IGateG1 )
    
    LSub_1GateGateG2 = GraphLoopsGates_1.Link( OSub_1Gate , IGateG2 )
    
    LGateGateEndInitGate = GraphLoopsGates_1.Link( OGateGate , IEndInitGate )
    
    LInit_1IndexEndInit_1Index = GraphLoopsGates_1.Link( OInit_1Index , IEndInit_1Index )
    
    LInit_1MinEndInit_1Min = GraphLoopsGates_1.Link( OInit_1Min , IEndInit_1Min )
    
    LInit_1MaxEndInit_1Max = GraphLoopsGates_1.Link( OInit_1Max , IEndInit_1Max )
    
    LInit_1GateInitGate = GraphLoopsGates_1.Link( OInit_1Gate , IInitGate )
    LInit_1GateInitGate.AddCoord( 1 , 9 , 346 )
    LInit_1GateInitGate.AddCoord( 2 , 9 , 199 )
    LInit_1GateInitGate.AddCoord( 3 , 194 , 199 )
    LInit_1GateInitGate.AddCoord( 4 , 194 , 119 )
    
    # Input datas
    IInitIndex.Input( 0 )
    IInitMin.Input( 5 )
    IInitMax.Input( 8 )
    IAddx.Input( 1 )
    IAddy.Input( 2 )
    IAdd_1x.Input( 3 )
    IAdd_1y.Input( 4 )
    IInit_1Index.Input( 0 )
    IInit_1Min.Input( 1 )
    IInit_1Max.Input( 3 )
    
    # Output Ports of the graph
    #OEndInitIndex = EndInit.GetOutPort( 'Index' )
    #OEndInitMin = EndInit.GetOutPort( 'Min' )
    #OEndInitMax = EndInit.GetOutPort( 'Max' )
    #OAddFuncValue = Add.GetOutPort( 'FuncValue' )
    #OAddz = Add.GetOutPort( 'z' )
    #OAdd_1FuncValue = Add_1.GetOutPort( 'FuncValue' )
    #OAdd_1z = Add_1.GetOutPort( 'z' )
    #OSubz = Sub.GetOutPort( 'z' )
    #OSub_1z = Sub_1.GetOutPort( 'z' )
    #OGateG = Gate.GetOutPort( 'G' )
    #OEndInit_1Index = EndInit_1.GetOutPort( 'Index' )
    #OEndInit_1Min = EndInit_1.GetOutPort( 'Min' )
    #OEndInit_1Max = EndInit_1.GetOutPort( 'Max' )
    return GraphLoopsGates_1


GraphLoopsGates_1 = DefGraphLoopsGates_1()
