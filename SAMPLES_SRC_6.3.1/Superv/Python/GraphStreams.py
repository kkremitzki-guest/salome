#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph GraphStreams
#
from SuperV import *
# Graph creation 
GraphStreams = StreamGraph( 'GraphStreams' )
GraphStreams.SetStreamParams( 1000 , SUPERV.SummaryTrace , 1.5 )
GraphStreams.SetName( 'GraphStreams' )
GraphStreams.SetAuthor( 'JR' )
GraphStreams.SetComment( 'Graph with DataStreamPorts CEA/EDF : Calcium' )
GraphStreams.Coords( 0 , 0 )

# Creation of Factory Nodes

Add = GraphStreams.FNode( 'DataStreamFactory' , 'DataStreamFactory' , 'Add' )
Add.SetName( 'Add' )
Add.SetAuthor( '' )
Add.SetContainer( 'FactoryServer' )
Add.SetComment( 'Add from DataStreamFactory' )
Add.Coords( 255 , 171 )
IAddx = Add.GetInPort( 'x' )
IAddy = Add.GetInPort( 'y' )
IAddGate = Add.GetInPort( 'Gate' )
OAddz = Add.GetOutPort( 'z' )
OAddGate = Add.GetOutPort( 'Gate' )
IAddistream = Add.GetInStreamPort( 'istream' )
IAddistream.SetParams( SUPERV.SCHENULL , SUPERV.INTERNULL , SUPERV.EXTRANULL )
OAddostream = Add.GetOutStreamPort( 'ostream' )
OAddostream.SetNumberOfValues( 0 )
OAddOStream = Add.GetOutStreamPort( 'OStream' )
OAddOStream.SetNumberOfValues( 10 )

Sub = GraphStreams.FNode( 'DataStreamFactory' , 'DataStreamFactory' , 'Sub' )
Sub.SetName( 'Sub' )
Sub.SetAuthor( '' )
Sub.SetContainer( 'FactoryServer' )
Sub.SetComment( 'Sub from DataStreamFactory' )
Sub.Coords( 519 , 48 )
ISubx = Sub.GetInPort( 'x' )
ISuby = Sub.GetInPort( 'y' )
ISubGate = Sub.GetInPort( 'Gate' )
OSubz = Sub.GetOutPort( 'z' )
OSubGate = Sub.GetOutPort( 'Gate' )
ISubistream = Sub.GetInStreamPort( 'istream' )
ISubistream.SetParams( SUPERV.SCHENULL , SUPERV.INTERNULL , SUPERV.EXTRANULL )
ISubIstream = Sub.GetInStreamPort( 'Istream' )
ISubIstream.SetParams( SUPERV.SCHENULL , SUPERV.INTERNULL , SUPERV.EXTRANULL )

Mul = GraphStreams.FNode( 'DataStreamFactory' , 'DataStreamFactory' , 'Mul' )
Mul.SetName( 'Mul' )
Mul.SetAuthor( '' )
Mul.SetContainer( 'FactoryServer' )
Mul.SetComment( 'Mul from DataStreamFactory' )
Mul.Coords( 724 , 142 )
IMulx = Mul.GetInPort( 'x' )
IMuly = Mul.GetInPort( 'y' )
IMulGate = Mul.GetInPort( 'Gate' )
OMulz = Mul.GetOutPort( 'z' )
OMulGate = Mul.GetOutPort( 'Gate' )

Div = GraphStreams.FNode( 'DataStreamFactory' , 'DataStreamFactory' , 'Div' )
Div.SetName( 'Div' )
Div.SetAuthor( '' )
Div.SetContainer( 'FactoryServer' )
Div.SetComment( 'Div from DataStreamFactory' )
Div.Coords( 935 , 48 )
IDivx = Div.GetInPort( 'x' )
IDivy = Div.GetInPort( 'y' )
IDivGate = Div.GetInPort( 'Gate' )
ODivz = Div.GetOutPort( 'z' )
ODivGate = Div.GetOutPort( 'Gate' )
ODivostream = Div.GetOutStreamPort( 'ostream' )
ODivostream.SetNumberOfValues( 0 )

Add_1 = GraphStreams.FNode( 'DataStreamFactory' , 'DataStreamFactory' , 'Add' )
Add_1.SetName( 'Add_1' )
Add_1.SetAuthor( '' )
Add_1.SetContainer( 'localhost/FactoryServer' )
Add_1.SetComment( 'Add from DataStreamFactory' )
Add_1.Coords( 252 , 499 )
IAdd_1x = Add_1.GetInPort( 'x' )
IAdd_1y = Add_1.GetInPort( 'y' )
IAdd_1Gate = Add_1.GetInPort( 'Gate' )
OAdd_1z = Add_1.GetOutPort( 'z' )
OAdd_1Gate = Add_1.GetOutPort( 'Gate' )
IAdd_1istream = Add_1.GetInStreamPort( 'istream' )
IAdd_1istream.SetParams( SUPERV.SCHENULL , SUPERV.INTERNULL , SUPERV.EXTRANULL )
OAdd_1ostream = Add_1.GetOutStreamPort( 'ostream' )
OAdd_1ostream.SetNumberOfValues( 0 )
OAdd_1OStream = Add_1.GetOutStreamPort( 'OStream' )
OAdd_1OStream.SetNumberOfValues( 0 )

Sub_1 = GraphStreams.FNode( 'DataStreamFactory' , 'DataStreamFactory' , 'Sub' )
Sub_1.SetName( 'Sub_1' )
Sub_1.SetAuthor( '' )
Sub_1.SetContainer( 'localhost/FactoryServer' )
Sub_1.SetComment( 'Sub from DataStreamFactory' )
Sub_1.Coords( 516 , 385 )
ISub_1x = Sub_1.GetInPort( 'x' )
ISub_1y = Sub_1.GetInPort( 'y' )
ISub_1Gate = Sub_1.GetInPort( 'Gate' )
OSub_1z = Sub_1.GetOutPort( 'z' )
OSub_1Gate = Sub_1.GetOutPort( 'Gate' )
ISub_1istream = Sub_1.GetInStreamPort( 'istream' )
ISub_1istream.SetParams( SUPERV.SCHENULL , SUPERV.INTERNULL , SUPERV.EXTRANULL )
ISub_1Istream = Sub_1.GetInStreamPort( 'Istream' )
ISub_1Istream.SetParams( SUPERV.SCHENULL , SUPERV.INTERNULL , SUPERV.EXTRANULL )

Mul_1 = GraphStreams.FNode( 'DataStreamFactory' , 'DataStreamFactory' , 'Mul' )
Mul_1.SetName( 'Mul_1' )
Mul_1.SetAuthor( '' )
Mul_1.SetContainer( 'localhost/FactoryServer' )
Mul_1.SetComment( 'Mul from DataStreamFactory' )
Mul_1.Coords( 731 , 487 )
IMul_1x = Mul_1.GetInPort( 'x' )
IMul_1y = Mul_1.GetInPort( 'y' )
IMul_1Gate = Mul_1.GetInPort( 'Gate' )
OMul_1z = Mul_1.GetOutPort( 'z' )
OMul_1Gate = Mul_1.GetOutPort( 'Gate' )

Div_1 = GraphStreams.FNode( 'DataStreamFactory' , 'DataStreamFactory' , 'Div' )
Div_1.SetName( 'Div_1' )
Div_1.SetAuthor( '' )
Div_1.SetContainer( 'localhost/FactoryServer' )
Div_1.SetComment( 'Div from DataStreamFactory' )
Div_1.Coords( 931 , 385 )
IDiv_1x = Div_1.GetInPort( 'x' )
IDiv_1y = Div_1.GetInPort( 'y' )
IDiv_1Gate = Div_1.GetInPort( 'Gate' )
ODiv_1z = Div_1.GetOutPort( 'z' )
ODiv_1Gate = Div_1.GetOutPort( 'Gate' )
ODiv_1ostream = Div_1.GetOutStreamPort( 'ostream' )
ODiv_1ostream.SetNumberOfValues( 0 )

# Creation of Loop Nodes
PyLoop = []
PyLoop.append( 'import time ' )
PyLoop.append( 'def Init(Index,Min,Max,Incr,Z_Div,Z_Div_1,y_Add,x_Sub) :   ' )
PyLoop.append( '    if Min <= Max :   ' )
PyLoop.append( '        Index = Min   ' )
PyLoop.append( '    else :   ' )
PyLoop.append( '        Index = Max   ' )
PyLoop.append( '    time.sleep( 1 ) ' )
PyLoop.append( '    return Index,Min,Max,Incr,Z_Div,Z_Div_1,y_Add,x_Sub ' )
PyMoreLoop = []
PyMoreLoop.append( 'import time ' )
PyMoreLoop.append( 'def More(Index,Min,Max,Incr,Z_Div,Z_Div_1,y_Add,x_Sub) :   ' )
PyMoreLoop.append( '    if Index < Max :   ' )
PyMoreLoop.append( '        DoLoop = 1   ' )
PyMoreLoop.append( '    else :   ' )
PyMoreLoop.append( '        DoLoop = 0   ' )
PyMoreLoop.append( '    time.sleep( 1 ) ' )
PyMoreLoop.append( '    return DoLoop,Index,Min,Max,Incr,Z_Div,Z_Div_1,y_Add,x_Sub ' )
PyNextLoop = []
PyNextLoop.append( 'import time ' )
PyNextLoop.append( 'def Next(Index,Min,Max,Incr,Z_Div,Z_Div_1,y_Add,x_Sub) :   ' )
PyNextLoop.append( '    Index = Index + Incr   ' )
PyNextLoop.append( '    time.sleep( 1 ) ' )
PyNextLoop.append( '    return Index,Min,Max,Incr,Z_Div,Z_Div_1,y_Add,x_Sub ' )
Loop,EndLoop = GraphStreams.LNode( 'Init' , PyLoop , 'More' , PyMoreLoop , 'Next' , PyNextLoop )
EndLoop.SetName( 'EndLoop' )
EndLoop.SetAuthor( '' )
EndLoop.SetComment( '' )
EndLoop.Coords( 1158 , 393 )
PyEndLoop = []
EndLoop.SetPyFunction( 'EndInit' , PyEndLoop )
ILoopInitLoop = Loop.GetInPort( 'DoLoop' )
ILoopIndex = Loop.InPort( 'Index' , 'long' )
ILoopMin = Loop.InPort( 'Min' , 'long' )
ILoopMax = Loop.InPort( 'Max' , 'long' )
ILoopIncr = Loop.InPort( 'Incr' , 'long' )
ILoopz_Div = Loop.InPort( 'z_Div' , 'double' )
ILoopz_Div_1 = Loop.InPort( 'z_Div_1' , 'double' )
ILoopy_Add = Loop.InPort( 'y_Add' , 'double' )
ILoopx_Sub = Loop.InPort( 'x_Sub' , 'double' )
ILoopGate = Loop.GetInPort( 'Gate' )
OLoopDoLoop = Loop.GetOutPort( 'DoLoop' )
OLoopIndex = Loop.GetOutPort( 'Index' )
OLoopMin = Loop.GetOutPort( 'Min' )
OLoopMax = Loop.GetOutPort( 'Max' )
OLoopIncr = Loop.GetOutPort( 'Incr' )
OLoopz_Div = Loop.GetOutPort( 'z_Div' )
OLoopz_Div_1 = Loop.GetOutPort( 'z_Div_1' )
OLoopy_Add = Loop.GetOutPort( 'y_Add' )
OLoopx_Sub = Loop.GetOutPort( 'x_Sub' )
IEndLoopDoLoop = EndLoop.GetInPort( 'DoLoop' )
IEndLoopIndex = EndLoop.GetInPort( 'Index' )
IEndLoopMin = EndLoop.GetInPort( 'Min' )
IEndLoopMax = EndLoop.GetInPort( 'Max' )
IEndLoopIncr = EndLoop.GetInPort( 'Incr' )
IEndLoopz_Div = EndLoop.GetInPort( 'z_Div' )
IEndLoopz_Div_1 = EndLoop.GetInPort( 'z_Div_1' )
IEndLoopy_Add = EndLoop.GetInPort( 'y_Add' )
IEndLoopx_Sub = EndLoop.GetInPort( 'x_Sub' )
IEndLoopGate = EndLoop.GetInPort( 'Gate' )
OEndLoopDoLoop = EndLoop.GetOutPort( 'DoLoop' )
OEndLoopIndex = EndLoop.GetOutPort( 'Index' )
OEndLoopMin = EndLoop.GetOutPort( 'Min' )
OEndLoopMax = EndLoop.GetOutPort( 'Max' )
OEndLoopIncr = EndLoop.GetOutPort( 'Incr' )
OEndLoopz_Div = EndLoop.GetOutPort( 'z_Div' )
OEndLoopz_Div_1 = EndLoop.GetOutPort( 'z_Div_1' )
OEndLoopy_Add = EndLoop.GetOutPort( 'y_Add' )
OEndLoopx_Sub = EndLoop.GetOutPort( 'x_Sub' )
Loop.SetName( 'Loop' )
Loop.SetAuthor( '' )
Loop.SetComment( '' )
Loop.Coords( 5 , 391 )

# Creation of Links
LAddzSuby = GraphStreams.Link( OAddz , ISuby )
LAddzSuby.AddCoord( 1 , 448 , 143 )
LAddzSuby.AddCoord( 2 , 449 , 237 )

LAddzMuly = GraphStreams.Link( OAddz , IMuly )
LAddzMuly.AddCoord( 1 , 708 , 237 )
LAddzMuly.AddCoord( 2 , 708 , 270 )
LAddzMuly.AddCoord( 3 , 449 , 270 )
LAddzMuly.AddCoord( 4 , 448 , 238 )

LAddostreamSubistream = GraphStreams.StreamLink( OAddostream , ISubistream )
LAddostreamSubistream.AddCoord( 1 , 474 , 175 )
LAddostreamSubistream.AddCoord( 2 , 474 , 297 )

LAddOStreamSubIstream = GraphStreams.StreamLink( OAddOStream , ISubIstream )
LAddOStreamSubIstream.AddCoord( 1 , 497 , 207 )
LAddOStreamSubIstream.AddCoord( 2 , 498 , 328 )

LSubzMulx = GraphStreams.Link( OSubz , IMulx )
LSubzMulx.AddCoord( 1 , 706 , 207 )
LSubzMulx.AddCoord( 2 , 706 , 114 )

LSubzDivx = GraphStreams.Link( OSubz , IDivx )

LMulzDivy = GraphStreams.Link( OMulz , IDivy )
LMulzDivy.AddCoord( 1 , 912 , 143 )
LMulzDivy.AddCoord( 2 , 911 , 208 )

LDivzEndLoopz_Div = GraphStreams.Link( ODivz , IEndLoopz_Div )
LDivzEndLoopz_Div.AddCoord( 1 , 1147 , 537 )
LDivzEndLoopz_Div.AddCoord( 2 , 1148 , 113 )

LDivostreamAdd_1istream = GraphStreams.StreamLink( ODivostream , IAdd_1istream )
LDivostreamAdd_1istream.AddCoord( 1 , 233 , 626 )
LDivostreamAdd_1istream.AddCoord( 2 , 232 , 763 )
LDivostreamAdd_1istream.AddCoord( 3 , 1126 , 763 )
LDivostreamAdd_1istream.AddCoord( 4 , 1125 , 175 )

LAdd_1zSub_1y = GraphStreams.Link( OAdd_1z , ISub_1y )
LAdd_1zSub_1y.AddCoord( 1 , 445 , 478 )
LAdd_1zSub_1y.AddCoord( 2 , 444 , 566 )

LAdd_1zMul_1y = GraphStreams.Link( OAdd_1z , IMul_1y )
LAdd_1zMul_1y.AddCoord( 1 , 703 , 582 )
LAdd_1zMul_1y.AddCoord( 2 , 703 , 677 )
LAdd_1zMul_1y.AddCoord( 3 , 445 , 678 )
LAdd_1zMul_1y.AddCoord( 4 , 444 , 567 )

LAdd_1ostreamSub_1istream = GraphStreams.StreamLink( OAdd_1ostream , ISub_1istream )
LAdd_1ostreamSub_1istream.AddCoord( 1 , 473 , 511 )
LAdd_1ostreamSub_1istream.AddCoord( 2 , 473 , 627 )

LAdd_1OStreamSub_1Istream = GraphStreams.StreamLink( OAdd_1OStream , ISub_1Istream )
LAdd_1OStreamSub_1Istream.AddCoord( 1 , 498 , 543 )
LAdd_1OStreamSub_1Istream.AddCoord( 2 , 497 , 658 )

LSub_1zMul_1x = GraphStreams.Link( OSub_1z , IMul_1x )
LSub_1zMul_1x.AddCoord( 1 , 708 , 552 )
LSub_1zMul_1x.AddCoord( 2 , 707 , 450 )

LSub_1zDiv_1x = GraphStreams.Link( OSub_1z , IDiv_1x )

LMul_1zDiv_1y = GraphStreams.Link( OMul_1z , IDiv_1y )
LMul_1zDiv_1y.AddCoord( 1 , 913 , 479 )
LMul_1zDiv_1y.AddCoord( 2 , 913 , 551 )

LDiv_1zEndLoopz_Div_1 = GraphStreams.Link( ODiv_1z , IEndLoopz_Div_1 )
LDiv_1zEndLoopz_Div_1.AddCoord( 1 , 1138 , 567 )
LDiv_1zEndLoopz_Div_1.AddCoord( 2 , 1138 , 449 )

LDiv_1ostreamAddistream = GraphStreams.StreamLink( ODiv_1ostream , IAddistream )
LDiv_1ostreamAddistream.AddCoord( 1 , 229 , 297 )
LDiv_1ostreamAddistream.AddCoord( 2 , 229 , 426 )
LDiv_1ostreamAddistream.AddCoord( 3 , 475 , 425 )
LDiv_1ostreamAddistream.AddCoord( 4 , 475 , 352 )
LDiv_1ostreamAddistream.AddCoord( 5 , 1110 , 352 )
LDiv_1ostreamAddistream.AddCoord( 6 , 1111 , 512 )

LLoopIndexEndLoopIndex = GraphStreams.Link( OLoopIndex , IEndLoopIndex )

LLoopIndexAddx = GraphStreams.Link( OLoopIndex , IAddx )
LLoopIndexAddx.AddCoord( 1 , 201 , 236 )
LLoopIndexAddx.AddCoord( 2 , 202 , 420 )

LLoopIndexAdd_1x = GraphStreams.Link( OLoopIndex , IAdd_1x )
LLoopIndexAdd_1x.AddCoord( 1 , 201 , 562 )
LLoopIndexAdd_1x.AddCoord( 2 , 202 , 422 )

LLoopMinEndLoopMin = GraphStreams.Link( OLoopMin , IEndLoopMin )

LLoopMaxEndLoopMax = GraphStreams.Link( OLoopMax , IEndLoopMax )

LLoopIncrEndLoopIncr = GraphStreams.Link( OLoopIncr , IEndLoopIncr )

LLoopy_AddEndLoopy_Add = GraphStreams.Link( OLoopy_Add , IEndLoopy_Add )

LLoopy_AddAddy = GraphStreams.Link( OLoopy_Add , IAddy )
LLoopy_AddAddy.AddCoord( 1 , 183 , 266 )
LLoopy_AddAddy.AddCoord( 2 , 182 , 593 )

LLoopy_AddAdd_1y = GraphStreams.Link( OLoopy_Add , IAdd_1y )

LLoopx_SubEndLoopx_Sub = GraphStreams.Link( OLoopx_Sub , IEndLoopx_Sub )

LLoopx_SubSubx = GraphStreams.Link( OLoopx_Sub , ISubx )
LLoopx_SubSubx.AddCoord( 1 , 167 , 113 )
LLoopx_SubSubx.AddCoord( 2 , 167 , 625 )

LLoopx_SubSub_1x = GraphStreams.Link( OLoopx_Sub , ISub_1x )
LLoopx_SubSub_1x.AddCoord( 1 , 167 , 450 )
LLoopx_SubSub_1x.AddCoord( 2 , 168 , 624 )

# Input datas
ILoopIndex.Input( 0 )
ILoopMin.Input( 1 )
ILoopMax.Input( 10 )
ILoopIncr.Input( 1 )
ILoopz_Div.Input( 0 )
ILoopz_Div_1.Input( 0 )
ILoopy_Add.Input( 4.5 )
ILoopx_Sub.Input( 1.5 )

# Output Ports of the graph
#OLoopz_Div = Loop.GetOutPort( 'z_Div' )
#OLoopz_Div_1 = Loop.GetOutPort( 'z_Div_1' )
#OEndLoopIndex = EndLoop.GetOutPort( 'Index' )
#OEndLoopMin = EndLoop.GetOutPort( 'Min' )
#OEndLoopMax = EndLoop.GetOutPort( 'Max' )
#OEndLoopIncr = EndLoop.GetOutPort( 'Incr' )
#OEndLoopz_Div = EndLoop.GetOutPort( 'z_Div' )
#OEndLoopz_Div_1 = EndLoop.GetOutPort( 'z_Div_1' )
#OEndLoopy_Add = EndLoop.GetOutPort( 'y_Add' )
#OEndLoopx_Sub = EndLoop.GetOutPort( 'x_Sub' )


GraphStreams.Run()
GraphStreams.DoneW()
GraphStreams.State()

GraphStreams.PrintPorts()

subgraphs = GraphStreams.SubGraphsNumber()
i = 1
while i <= subgraphs :
    nodes = GraphStreams.SubGraphsNodes( i )
    j = 0
    while j < len(nodes) :
        print 'SubGraph',i,nodes[j].Name()
        j = j + 1
    i = i + 1


substreamgraphs = GraphStreams.SubStreamGraphsNumber()
i = 1
while i <= substreamgraphs :
    nodes = GraphStreams.SubStreamGraphsNodes( i )
    j = 0
    while j < len(nodes) :
        print 'SubStreamGraph',i,nodes[j].Name()
        j = j + 1
    i = i + 1


GraphStreams.Export('/tmp/GraphStreams.xml')

GraphStreams = StreamGraph( '/tmp/GraphStreams.xml' )
GraphStreams.Run()
GraphStreams.DoneW()
GraphStreams.PrintPorts()
print GraphStreams.State()

GraphStreams.IsExecutable()

subgraphs = GraphStreams.SubGraphsNumber()
i = 1
while i <= subgraphs :
    nodes = GraphStreams.SubGraphsNodes( i )
    j = 0
    while j < len(nodes) :
        print 'SubGraph',i,nodes[j].Name()
        j = j + 1
    i = i + 1


substreamgraphs = GraphStreams.SubStreamGraphsNumber()
i = 1
while i <= substreamgraphs :
    nodes = GraphStreams.SubStreamGraphsNodes( i )
    j = 0
    while j < len(nodes) :
        print 'SubStreamGraph',i,nodes[j].Name()
        j = j + 1
    i = i + 1

