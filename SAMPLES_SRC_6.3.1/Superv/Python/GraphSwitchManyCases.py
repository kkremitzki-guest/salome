#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph GraphSwitchManyCases
#
from SuperV import *

# Graph creation of GraphSwitchManyCases
def DefGraphSwitchManyCases() :
    GraphSwitchManyCases = Graph( 'GraphSwitchManyCases' )
    GraphSwitchManyCases.SetName( 'GraphSwitchManyCases' )
    GraphSwitchManyCases.SetAuthor( 'JR' )
    GraphSwitchManyCases.SetComment( '' )
    GraphSwitchManyCases.Coords( 0 , 0 )
    
    # Creation of Factory Nodes
    
    # Creation of InLine Nodes
    PyCase1 = []
    PyCase1.append( 'from time import *  ' )
    PyCase1.append( 'def Case1(a) :  ' )
    PyCase1.append( '    sleep(1)  ' )
    PyCase1.append( '    return a ' )
    Case1 = GraphSwitchManyCases.INode( 'Case1' , PyCase1 )
    Case1.SetName( 'Case1' )
    Case1.SetAuthor( '' )
    Case1.SetComment( 'Compute Node' )
    Case1.Coords( 448 , 13 )
    ICase1a = Case1.InPort( 'a' , 'long' )
    ICase1Gate = Case1.GetInPort( 'Gate' )
    OCase1a = Case1.OutPort( 'a' , 'long' )
    OCase1Gate = Case1.GetOutPort( 'Gate' )
    
    PyCase3 = []
    PyCase3.append( 'from time import *  ' )
    PyCase3.append( 'def Case3(b) :  ' )
    PyCase3.append( '    sleep(1)  ' )
    PyCase3.append( '    return b  ' )
    Case3 = GraphSwitchManyCases.INode( 'Case3' , PyCase3 )
    Case3.SetName( 'Case3' )
    Case3.SetAuthor( '' )
    Case3.SetComment( 'Compute Node' )
    Case3.Coords( 458 , 286 )
    ICase3b = Case3.InPort( 'b' , 'long' )
    ICase3Gate = Case3.GetInPort( 'Gate' )
    OCase3b = Case3.OutPort( 'b' , 'long' )
    OCase3Gate = Case3.GetOutPort( 'Gate' )
    
    PyCase4 = []
    PyCase4.append( 'from time import *  ' )
    PyCase4.append( 'def Case4(c) :  ' )
    PyCase4.append( '    sleep(1)  ' )
    PyCase4.append( '    return   ' )
    Case4 = GraphSwitchManyCases.INode( 'Case4' , PyCase4 )
    Case4.SetName( 'Case4' )
    Case4.SetAuthor( '' )
    Case4.SetComment( 'Compute Node' )
    Case4.Coords( 459 , 493 )
    ICase4c = Case4.InPort( 'c' , 'long' )
    ICase4Gate = Case4.GetInPort( 'Gate' )
    OCase4Gate = Case4.GetOutPort( 'Gate' )
    
    PyCase2a = []
    PyCase2a.append( 'from time import *  ' )
    PyCase2a.append( 'def Case2a(a) :  ' )
    PyCase2a.append( '    sleep(1)  ' )
    PyCase2a.append( '    return a  ' )
    Case2a = GraphSwitchManyCases.INode( 'Case2a' , PyCase2a )
    Case2a.SetName( 'Case2a' )
    Case2a.SetAuthor( '' )
    Case2a.SetComment( 'Compute Node' )
    Case2a.Coords( 453 , 154 )
    ICase2aa = Case2a.InPort( 'a' , 'long' )
    ICase2aGate = Case2a.GetInPort( 'Gate' )
    OCase2aa = Case2a.OutPort( 'a' , 'long' )
    OCase2aGate = Case2a.GetOutPort( 'Gate' )
    
    # Creation of Loop Nodes
    PyLoop = []
    PyLoop.append( 'def Init(Index,Max,Min) : ' )
    PyLoop.append( '    Index = Max ' )
    PyLoop.append( '    return Index,Max,Min ' )
    PyMoreLoop = []
    PyMoreLoop.append( 'def More(Index,Max,Min) : ' )
    PyMoreLoop.append( '    if Index >= Min : ' )
    PyMoreLoop.append( '        DoLoop = 1 ' )
    PyMoreLoop.append( '    else : ' )
    PyMoreLoop.append( '        DoLoop = 0 ' )
    PyMoreLoop.append( '    return DoLoop,Index,Max,Min ' )
    PyNextLoop = []
    PyNextLoop.append( 'def Next(Index,Max,Min) : ' )
    PyNextLoop.append( '    Index = Index - 1 ' )
    PyNextLoop.append( '    return Index,Max,Min ' )
    Loop,EndLoop = GraphSwitchManyCases.LNode( 'Init' , PyLoop , 'More' , PyMoreLoop , 'Next' , PyNextLoop )
    EndLoop.SetName( 'EndLoop' )
    EndLoop.SetAuthor( '' )
    EndLoop.SetComment( 'Compute Node' )
    EndLoop.Coords( 972 , 344 )
    PyEndLoop = []
    EndLoop.SetPyFunction( 'EndLoop' , PyEndLoop )
    ILoopDoLoop = Loop.GetInPort( 'DoLoop' )
    ILoopIndex = Loop.InPort( 'Index' , 'long' )
    ILoopMax = Loop.InPort( 'Max' , 'long' )
    ILoopMin = Loop.InPort( 'Min' , 'long' )
    ILoopGate = Loop.GetInPort( 'Gate' )
    OLoopDoLoop = Loop.GetOutPort( 'DoLoop' )
    OLoopIndex = Loop.GetOutPort( 'Index' )
    OLoopMax = Loop.GetOutPort( 'Max' )
    OLoopMin = Loop.GetOutPort( 'Min' )
    IEndLoopDoLoop = EndLoop.GetInPort( 'DoLoop' )
    IEndLoopIndex = EndLoop.GetInPort( 'Index' )
    IEndLoopMax = EndLoop.GetInPort( 'Max' )
    IEndLoopMin = EndLoop.GetInPort( 'Min' )
    IEndLoopGate = EndLoop.GetInPort( 'Gate' )
    OEndLoopDoLoop = EndLoop.GetOutPort( 'DoLoop' )
    OEndLoopIndex = EndLoop.GetOutPort( 'Index' )
    OEndLoopMax = EndLoop.GetOutPort( 'Max' )
    OEndLoopMin = EndLoop.GetOutPort( 'Min' )
    OEndLoopGate = EndLoop.GetOutPort( 'Gate' )
    Loop.SetName( 'Loop' )
    Loop.SetAuthor( '' )
    Loop.SetComment( 'Compute Node' )
    Loop.Coords( 15 , 336 )
    
    # Creation of Switch Nodes
    PySwitch = []
    PySwitch.append( 'from time import *    ' )
    PySwitch.append( 'def Switch(Index) :    ' )
    PySwitch.append( '    sleep(1)   ' )
    PySwitch.append( '    SB1 = 0   ' )
    PySwitch.append( '    SB2 = 0   ' )
    PySwitch.append( '    if (Index & 3) == 3 :     ' )
    PySwitch.append( '        SB1 = 1   ' )
    PySwitch.append( '    elif (Index & 3) == 0 :     ' )
    PySwitch.append( '        SB2 = 1   ' )
    PySwitch.append( '    return SB1,SB2,Index,Index+1,Index+2,Index+3,Index+4,Index+5     ' )
    Switch,EndSwitch = GraphSwitchManyCases.SNode( 'Switch' , PySwitch )
    EndSwitch.SetName( 'EndSwitch' )
    EndSwitch.SetAuthor( '' )
    EndSwitch.SetComment( 'Compute Node' )
    EndSwitch.Coords( 732 , 344 )
    PyEndSwitch = []
    PyEndSwitch.append( 'from time import *   ' )
    PyEndSwitch.append( 'def EndSwitch(a,b,dd,e,f) :   ' )
    PyEndSwitch.append( '    sleep(1)   ' )
    PyEndSwitch.append( '    if b != (a+1) :  ' )
    PyEndSwitch.append( '        print 'EndSwitch ERROR' ' )
    PyEndSwitch.append( '        pass  ' )
    PyEndSwitch.append( '    if dd != (a+3) :  ' )
    PyEndSwitch.append( '        print 'EndSwitch ERROR' ' )
    PyEndSwitch.append( '        pass  ' )
    PyEndSwitch.append( '    if e != (a+4) :  ' )
    PyEndSwitch.append( '        print 'EndSwitch ERROR' ' )
    PyEndSwitch.append( '        pass  ' )
    PyEndSwitch.append( '    if f != (a+5) :  ' )
    PyEndSwitch.append( '        print 'EndSwitch ERROR' ' )
    PyEndSwitch.append( '        pass  ' )
    PyEndSwitch.append( '    return a    ' )
    EndSwitch.SetPyFunction( 'EndSwitch' , PyEndSwitch )
    IEndSwitcha = EndSwitch.InPort( 'a' , 'long' )
    IEndSwitchb = EndSwitch.InPort( 'b' , 'long' )
    IEndSwitchdd = EndSwitch.InPort( 'dd' , 'long' )
    IEndSwitche = EndSwitch.InPort( 'e' , 'long' )
    IEndSwitchf = EndSwitch.InPort( 'f' , 'long' )
    IEndSwitchDefault = EndSwitch.GetInPort( 'Default' )
    OEndSwitchIndex = EndSwitch.OutPort( 'Index' , 'long' )
    OEndSwitchGate = EndSwitch.GetOutPort( 'Gate' )
    Switch.SetName( 'Switch' )
    Switch.SetAuthor( '' )
    Switch.SetComment( 'Compute Node' )
    Switch.Coords( 232 , 285 )
    ISwitchIndex = Switch.InPort( 'Index' , 'long' )
    ISwitchGate = Switch.GetInPort( 'Gate' )
    OSwitchSB1 = Switch.OutPort( 'SB1' , 'long' )
    OSwitchSB2 = Switch.OutPort( 'SB2' , 'long' )
    OSwitcha = Switch.OutPort( 'a' , 'long' )
    OSwitchb = Switch.OutPort( 'b' , 'long' )
    OSwitchc = Switch.OutPort( 'c' , 'long' )
    OSwitchd = Switch.OutPort( 'd' , 'long' )
    OSwitche = Switch.OutPort( 'e' , 'long' )
    OSwitchf = Switch.OutPort( 'f' , 'long' )
    OSwitchDefault = Switch.GetOutPort( 'Default' )
    
    # Creation of Links
    LLoopIndexSwitchIndex = GraphSwitchManyCases.Link( OLoopIndex , ISwitchIndex )
    
    LLoopMaxEndLoopMax = GraphSwitchManyCases.Link( OLoopMax , IEndLoopMax )
    
    LLoopMinEndLoopMin = GraphSwitchManyCases.Link( OLoopMin , IEndLoopMin )
    
    LSwitchSB1Case1Gate = GraphSwitchManyCases.Link( OSwitchSB1 , ICase1Gate )
    
    LSwitchSB2Case2aGate = GraphSwitchManyCases.Link( OSwitchSB2 , ICase2aGate )
    
    LSwitchaCase1a = GraphSwitchManyCases.Link( OSwitcha , ICase1a )
    
    LSwitchaCase2aa = GraphSwitchManyCases.Link( OSwitcha , ICase2aa )
    
    LSwitchbCase3b = GraphSwitchManyCases.Link( OSwitchb , ICase3b )
    
    LSwitchcCase4c = GraphSwitchManyCases.Link( OSwitchc , ICase4c )
    
    LSwitchdEndSwitchdd = GraphSwitchManyCases.Link( OSwitchd , IEndSwitchdd )
    
    LSwitcheEndSwitche = GraphSwitchManyCases.Link( OSwitche , IEndSwitche )
    
    LSwitchDefaultEndSwitchDefault = GraphSwitchManyCases.Link( OSwitchDefault , IEndSwitchDefault )
    
    LEndSwitchIndexEndLoopIndex = GraphSwitchManyCases.Link( OEndSwitchIndex , IEndLoopIndex )
    
    LCase1aEndSwitcha = GraphSwitchManyCases.Link( OCase1a , IEndSwitcha )
    
    LCase3bEndSwitchb = GraphSwitchManyCases.Link( OCase3b , IEndSwitchb )
    
    LCase4GateEndSwitchDefault = GraphSwitchManyCases.Link( OCase4Gate , IEndSwitchDefault )
    
    LCase2aaEndSwitcha = GraphSwitchManyCases.Link( OCase2aa , IEndSwitcha )
    
    # Input datas
    ILoopIndex.Input( 0 )
    ILoopMax.Input( 23 )
    ILoopMin.Input( -7 )
    
    # Input Ports of the graph
    #IEndSwitchf = EndSwitch.GetInPort( 'f' )
    
    # Output Ports of the graph
    #OEndLoopIndex = EndLoop.GetOutPort( 'Index' )
    #OEndLoopMax = EndLoop.GetOutPort( 'Max' )
    #OEndLoopMin = EndLoop.GetOutPort( 'Min' )
    #OSwitchf = Switch.GetOutPort( 'f' )
    return GraphSwitchManyCases


GraphSwitchManyCases = DefGraphSwitchManyCases()
