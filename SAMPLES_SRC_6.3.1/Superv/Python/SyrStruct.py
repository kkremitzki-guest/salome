#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph SyrStruct
#
from SuperV import *

# Graph creation of SyrStruct
def DefSyrStruct() :
    SyrStruct = Graph( 'SyrStruct' )
    SyrStruct.SetName( 'SyrStruct' )
    SyrStruct.SetAuthor( 'JR' )
    SyrStruct.SetComment( 'Syracuse algorithm' )
    SyrStruct.Coords( 0 , 0 )
    
    # Creation of Factory Nodes
    
    m3 = SyrStruct.FNode( 'SyrComponent' , 'SyrComponent' , 'C_M3' )
    m3.SetName( 'm3' )
    m3.SetAuthor( '' )
    m3.SetContainer( 'localhost/FactoryServer' )
    m3.SetComment( 'C_M3 from SyrComponent' )
    m3.Coords( 672 , 28 )
    Im3anOddInteger = m3.GetInPort( 'anOddInteger' )
    Im3Gate = m3.GetInPort( 'Gate' )
    Om3anInteger = m3.GetOutPort( 'anInteger' )
    Om3Gate = m3.GetOutPort( 'Gate' )
    
    m3incr = SyrStruct.FNode( 'SyrComponent' , 'SyrComponent' , 'C_INCR' )
    m3incr.SetName( 'm3incr' )
    m3incr.SetAuthor( '' )
    m3incr.SetContainer( 'localhost/FactoryServer' )
    m3incr.SetComment( 'C_INCR from SyrComponent' )
    m3incr.Coords( 899 , 28 )
    Im3incraCount = m3incr.GetInPort( 'aCount' )
    Im3incrGate = m3incr.GetInPort( 'Gate' )
    Om3incraNewCount = m3incr.GetOutPort( 'aNewCount' )
    Om3incrGate = m3incr.GetOutPort( 'Gate' )
    
    incra = SyrStruct.FNode( 'SyrComponent' , 'SyrComponent' , 'C_INCR' )
    incra.SetName( 'incra' )
    incra.SetAuthor( '' )
    incra.SetContainer( 'localhost/FactoryServer' )
    incra.SetComment( 'C_INCR from SyrComponent' )
    incra.Coords( 824 , 218 )
    IincraaCount = incra.GetInPort( 'aCount' )
    IincraGate = incra.GetInPort( 'Gate' )
    OincraaNewCount = incra.GetOutPort( 'aNewCount' )
    OincraGate = incra.GetOutPort( 'Gate' )
    
    div2 = SyrStruct.FNode( 'SyrComponent' , 'SyrComponent' , 'C_DIV2' )
    div2.SetName( 'div2' )
    div2.SetAuthor( '' )
    div2.SetContainer( 'localhost/FactoryServer' )
    div2.SetComment( 'C_DIV2 from SyrComponent' )
    div2.Coords( 817 , 431 )
    Idiv2anEvenInteger = div2.GetInPort( 'anEvenInteger' )
    Idiv2Gate = div2.GetInPort( 'Gate' )
    Odiv2anInteger = div2.GetOutPort( 'anInteger' )
    Odiv2Gate = div2.GetOutPort( 'Gate' )
    
    incrb = SyrStruct.FNode( 'SyrComponent' , 'SyrComponent' , 'C_INCR' )
    incrb.SetName( 'incrb' )
    incrb.SetAuthor( '' )
    incrb.SetContainer( 'localhost/FactoryServer' )
    incrb.SetComment( 'C_INCR from SyrComponent' )
    incrb.Coords( 821 , 574 )
    IincrbaCount = incrb.GetInPort( 'aCount' )
    IincrbGate = incrb.GetInPort( 'Gate' )
    OincrbaNewCount = incrb.GetOutPort( 'aNewCount' )
    OincrbGate = incrb.GetOutPort( 'Gate' )
    
    # Creation of InLine Nodes
    PySyrComponent = []
    PySyrComponent.append( 'from LifeCycleCORBA import * ' )
    PySyrComponent.append( 'def SyrComponent( aContainer , aComponent ) : ' )
    PySyrComponent.append( '    orb = CORBA.ORB_init([], CORBA.ORB_ID) ' )
    PySyrComponent.append( '    lcc = LifeCycleCORBA(orb) ' )
    PySyrComponent.append( '    ComponentRef = lcc.FindOrLoadComponent( aContainer , aComponent ) ' )
    PySyrComponent.append( '    return ComponentRef ' )
    SyrComponent = SyrStruct.INode( 'SyrComponent' , PySyrComponent )
    SyrComponent.SetName( 'SyrComponent' )
    SyrComponent.SetAuthor( '' )
    SyrComponent.SetComment( 'SyrComponent( aContainer , aComponent )' )
    SyrComponent.Coords( 0 , 0 )
    ISyrComponentaContainer = SyrComponent.InPort( 'aContainer' , 'string' )
    ISyrComponentaComponent = SyrComponent.InPort( 'aComponent' , 'string' )
    ISyrComponentGate = SyrComponent.GetInPort( 'Gate' )
    OSyrComponentSyrComponentobjref = SyrComponent.OutPort( 'SyrComponentobjref' , 'objref' )
    OSyrComponentGate = SyrComponent.GetOutPort( 'Gate' )
    
    # Creation of Loop Nodes
    PyforN = []
    PyforN.append( 'def InitN( NN , K , SyrComponent , min , max ) : ' )
    PyforN.append( '    N = max ' )
    PyforN.append( '    if min > 0 : ' )
    PyforN.append( '        if max >= min : ' )
    PyforN.append( '            N = min ' )
    PyforN.append( '    return N,K,SyrComponent,min,max ' )
    PyMoreforN = []
    PyMoreforN.append( 'def MoreN( NN , KK , SyrComponent , min , max ) : ' )
    PyMoreforN.append( '    N = NN ' )
    PyMoreforN.append( '    OutLoop = 0 ' )
    PyMoreforN.append( '    if max > NN : ' )
    PyMoreforN.append( '        OutLoop = 1 ' )
    PyMoreforN.append( '    return OutLoop,N,0,SyrComponent,min,max ' )
    PyNextforN = []
    PyNextforN.append( 'def NextN( NN , KK , SyrComponent , min , max ) : ' )
    PyNextforN.append( '    N = NN + 1 ' )
    PyNextforN.append( '    K = KK ' )
    PyNextforN.append( '    return N,K,SyrComponent,min,max ' )
    forN,EndOfforN = SyrStruct.LNode( 'InitN' , PyforN , 'MoreN' , PyMoreforN , 'NextN' , PyNextforN )
    EndOfforN.SetName( 'EndOfforN' )
    EndOfforN.SetAuthor( '' )
    EndOfforN.SetComment( 'Compute Node' )
    EndOfforN.Coords( 1590 , 331 )
    PyEndOfforN = []
    EndOfforN.SetPyFunction( '' , PyEndOfforN )
    IforNDoLoop = forN.GetInPort( 'DoLoop' )
    IforNN = forN.InPort( 'N' , 'long' )
    IforNK = forN.InPort( 'K' , 'long' )
    IforNSyrComponent = forN.InPort( 'SyrComponent' , 'objref' )
    IforNmin = forN.InPort( 'min' , 'long' )
    IforNmax = forN.InPort( 'max' , 'long' )
    IforNGate = forN.GetInPort( 'Gate' )
    OforNDoLoop = forN.GetOutPort( 'DoLoop' )
    OforNN = forN.GetOutPort( 'N' )
    OforNK = forN.GetOutPort( 'K' )
    OforNSyrComponent = forN.GetOutPort( 'SyrComponent' )
    OforNmin = forN.GetOutPort( 'min' )
    OforNmax = forN.GetOutPort( 'max' )
    IEndOfforNDoLoop = EndOfforN.GetInPort( 'DoLoop' )
    IEndOfforNN = EndOfforN.GetInPort( 'N' )
    IEndOfforNK = EndOfforN.GetInPort( 'K' )
    IEndOfforNSyrComponent = EndOfforN.GetInPort( 'SyrComponent' )
    IEndOfforNmin = EndOfforN.GetInPort( 'min' )
    IEndOfforNmax = EndOfforN.GetInPort( 'max' )
    IEndOfforNGate = EndOfforN.GetInPort( 'Gate' )
    OEndOfforNDoLoop = EndOfforN.GetOutPort( 'DoLoop' )
    OEndOfforNN = EndOfforN.GetOutPort( 'N' )
    OEndOfforNK = EndOfforN.GetOutPort( 'K' )
    OEndOfforNSyrComponent = EndOfforN.GetOutPort( 'SyrComponent' )
    OEndOfforNmin = EndOfforN.GetOutPort( 'min' )
    OEndOfforNmax = EndOfforN.GetOutPort( 'max' )
    OEndOfforNGate = EndOfforN.GetOutPort( 'Gate' )
    forN.SetName( 'forN' )
    forN.SetAuthor( '' )
    forN.SetComment( 'InitN , MoreN , NextN' )
    forN.Coords( 20 , 302 )
    
    PywhileNotOne = []
    PywhileNotOne.append( 'import threading' )
    PywhileNotOne.append( 'import SyrComponent_idl' )
    PywhileNotOne.append( 'def InitNotOne( SyrComponent , N , K ) :' )
    PywhileNotOne.append( '    return SyrComponent,N,K' )
    PyMorewhileNotOne = []
    PyMorewhileNotOne.append( 'import SyrComponent_idl' )
    PyMorewhileNotOne.append( 'def MoreNotOne( SyrComponent , N , K ) :' )
    PyMorewhileNotOne.append( '    OutLoop = 1 - SyrComponent.C_ISONE( N )' )
    PyMorewhileNotOne.append( '    return OutLoop,SyrComponent,N,K ' )
    PyNextwhileNotOne = []
    PyNextwhileNotOne.append( 'import SyrComponent_idl' )
    PyNextwhileNotOne.append( 'def NextNotOne( SyrComponent , N , K ) :' )
    PyNextwhileNotOne.append( '    return SyrComponent,N,K' )
    whileNotOne,EndOfwhileNotOne = SyrStruct.LNode( 'InitNotOne' , PywhileNotOne , 'MoreNotOne' , PyMorewhileNotOne , 'NextNotOne' , PyNextwhileNotOne )
    EndOfwhileNotOne.SetName( 'EndOfwhileNotOne' )
    EndOfwhileNotOne.SetAuthor( '' )
    EndOfwhileNotOne.SetComment( 'Compute Node' )
    EndOfwhileNotOne.Coords( 1405 , 311 )
    PyEndOfwhileNotOne = []
    EndOfwhileNotOne.SetPyFunction( '' , PyEndOfwhileNotOne )
    IwhileNotOneDoLoop = whileNotOne.GetInPort( 'DoLoop' )
    IwhileNotOneSyrComponent = whileNotOne.InPort( 'SyrComponent' , 'objref' )
    IwhileNotOneN = whileNotOne.InPort( 'N' , 'long' )
    IwhileNotOneK = whileNotOne.InPort( 'K' , 'long' )
    IwhileNotOneGate = whileNotOne.GetInPort( 'Gate' )
    OwhileNotOneDoLoop = whileNotOne.GetOutPort( 'DoLoop' )
    OwhileNotOneSyrComponent = whileNotOne.GetOutPort( 'SyrComponent' )
    OwhileNotOneN = whileNotOne.GetOutPort( 'N' )
    OwhileNotOneK = whileNotOne.GetOutPort( 'K' )
    IEndOfwhileNotOneDoLoop = EndOfwhileNotOne.GetInPort( 'DoLoop' )
    IEndOfwhileNotOneSyrComponent = EndOfwhileNotOne.GetInPort( 'SyrComponent' )
    IEndOfwhileNotOneN = EndOfwhileNotOne.GetInPort( 'N' )
    IEndOfwhileNotOneK = EndOfwhileNotOne.GetInPort( 'K' )
    IEndOfwhileNotOneGate = EndOfwhileNotOne.GetInPort( 'Gate' )
    OEndOfwhileNotOneDoLoop = EndOfwhileNotOne.GetOutPort( 'DoLoop' )
    OEndOfwhileNotOneSyrComponent = EndOfwhileNotOne.GetOutPort( 'SyrComponent' )
    OEndOfwhileNotOneN = EndOfwhileNotOne.GetOutPort( 'N' )
    OEndOfwhileNotOneK = EndOfwhileNotOne.GetOutPort( 'K' )
    OEndOfwhileNotOneGate = EndOfwhileNotOne.GetOutPort( 'Gate' )
    whileNotOne.SetName( 'whileNotOne' )
    whileNotOne.SetAuthor( '' )
    whileNotOne.SetComment( 'InitNotOne , MoreNotOne , NextNotOne' )
    whileNotOne.Coords( 215 , 282 )
    
    Pyfori = []
    Pyfori.append( 'def Initfori( ii , K ) :  ' )
    Pyfori.append( '    ii = 0    ' )
    Pyfori.append( '    return ii,K    ' )
    PyMorefori = []
    PyMorefori.append( 'from time import *    ' )
    PyMorefori.append( 'def Morefori( ii , K ) :    ' )
    PyMorefori.append( '    OutLoop = 0    ' )
    PyMorefori.append( '    if ii < 2 :    ' )
    PyMorefori.append( '        OutLoop = 1 ' )
    PyMorefori.append( '    return OutLoop,ii,K    ' )
    PyNextfori = []
    PyNextfori.append( 'def Nextfori( ii , K ) :    ' )
    PyNextfori.append( '    ii = ii + 1 ' )
    PyNextfori.append( '    return ii,K    ' )
    fori,EndOffori = SyrStruct.LNode( 'Initfori' , Pyfori , 'Morefori' , PyMorefori , 'Nextfori' , PyNextfori )
    EndOffori.SetName( 'EndOffori' )
    EndOffori.SetAuthor( '' )
    EndOffori.SetComment( 'Compute Node' )
    EndOffori.Coords( 1009 , 238 )
    PyEndOffori = []
    EndOffori.SetPyFunction( '' , PyEndOffori )
    IforiDoLoop = fori.GetInPort( 'DoLoop' )
    Iforii = fori.InPort( 'i' , 'long' )
    IforiK = fori.InPort( 'K' , 'long' )
    IforiGate = fori.GetInPort( 'Gate' )
    OforiDoLoop = fori.GetOutPort( 'DoLoop' )
    Oforii = fori.GetOutPort( 'i' )
    OforiK = fori.GetOutPort( 'K' )
    IEndOfforiDoLoop = EndOffori.GetInPort( 'DoLoop' )
    IEndOfforii = EndOffori.GetInPort( 'i' )
    IEndOfforiK = EndOffori.GetInPort( 'K' )
    IEndOfforiGate = EndOffori.GetInPort( 'Gate' )
    OEndOfforiDoLoop = EndOffori.GetOutPort( 'DoLoop' )
    OEndOfforii = EndOffori.GetOutPort( 'i' )
    OEndOfforiK = EndOffori.GetOutPort( 'K' )
    OEndOfforiGate = EndOffori.GetOutPort( 'Gate' )
    fori.SetName( 'fori' )
    fori.SetAuthor( '' )
    fori.SetComment( 'Initfori, Morefori, Nextfori' )
    fori.Coords( 641 , 238 )
    
    PywhileEven = []
    PywhileEven.append( 'import SyrComponent_idl' )
    PywhileEven.append( 'def InitEven( SyrComponent , N , K ) :' )
    PywhileEven.append( '    return SyrComponent,N,K' )
    PyMorewhileEven = []
    PyMorewhileEven.append( 'import SyrComponent_idl' )
    PyMorewhileEven.append( 'def MoreEven( SyrComponent , N , K ) :' )
    PyMorewhileEven.append( '    OutLoop = SyrComponent.C_ISEVEN( N )' )
    PyMorewhileEven.append( '    return OutLoop,SyrComponent,N,K ' )
    PyNextwhileEven = []
    PyNextwhileEven.append( 'import SyrComponent_idl' )
    PyNextwhileEven.append( 'def NextEven( SyrComponent , N , K ) :' )
    PyNextwhileEven.append( '    return SyrComponent,N,K' )
    whileEven,EndOfwhileEven = SyrStruct.LNode( 'InitEven' , PywhileEven , 'MoreEven' , PyMorewhileEven , 'NextEven' , PyNextwhileEven )
    EndOfwhileEven.SetName( 'EndOfwhileEven' )
    EndOfwhileEven.SetAuthor( '' )
    EndOfwhileEven.SetComment( 'Compute Node' )
    EndOfwhileEven.Coords( 1006 , 451 )
    PyEndOfwhileEven = []
    EndOfwhileEven.SetPyFunction( '' , PyEndOfwhileEven )
    IwhileEvenDoLoop = whileEven.GetInPort( 'DoLoop' )
    IwhileEvenSyrComponent = whileEven.InPort( 'SyrComponent' , 'objref' )
    IwhileEvenN = whileEven.InPort( 'N' , 'long' )
    IwhileEvenK = whileEven.InPort( 'K' , 'long' )
    IwhileEvenGate = whileEven.GetInPort( 'Gate' )
    OwhileEvenDoLoop = whileEven.GetOutPort( 'DoLoop' )
    OwhileEvenSyrComponent = whileEven.GetOutPort( 'SyrComponent' )
    OwhileEvenN = whileEven.GetOutPort( 'N' )
    OwhileEvenK = whileEven.GetOutPort( 'K' )
    IEndOfwhileEvenDoLoop = EndOfwhileEven.GetInPort( 'DoLoop' )
    IEndOfwhileEvenSyrComponent = EndOfwhileEven.GetInPort( 'SyrComponent' )
    IEndOfwhileEvenN = EndOfwhileEven.GetInPort( 'N' )
    IEndOfwhileEvenK = EndOfwhileEven.GetInPort( 'K' )
    IEndOfwhileEvenGate = EndOfwhileEven.GetInPort( 'Gate' )
    OEndOfwhileEvenDoLoop = EndOfwhileEven.GetOutPort( 'DoLoop' )
    OEndOfwhileEvenSyrComponent = EndOfwhileEven.GetOutPort( 'SyrComponent' )
    OEndOfwhileEvenN = EndOfwhileEven.GetOutPort( 'N' )
    OEndOfwhileEvenK = EndOfwhileEven.GetOutPort( 'K' )
    OEndOfwhileEvenGate = EndOfwhileEven.GetOutPort( 'Gate' )
    whileEven.SetName( 'whileEven' )
    whileEven.SetAuthor( '' )
    whileEven.SetComment( 'InitEven, MoreEven, NextEven' )
    whileEven.Coords( 632 , 451 )
    
    # Creation of Switch Nodes
    PyifNotEven = []
    PyifNotEven.append( 'import SyrComponent_idl' )
    PyifNotEven.append( 'def ifNotEven( SyrComponent , N , K ) :' )
    PyifNotEven.append( '    Even = SyrComponent.C_ISEVEN( N )' )
    PyifNotEven.append( '    Odd = 1 - Even' )
    PyifNotEven.append( '    return Odd,Even,SyrComponent,N,K' )
    ifNotEven,EndOfifNotEven = SyrStruct.SNode( 'ifNotEven' , PyifNotEven )
    EndOfifNotEven.SetName( 'EndOfifNotEven' )
    EndOfifNotEven.SetAuthor( '' )
    EndOfifNotEven.SetComment( 'Compute Node' )
    EndOfifNotEven.Coords( 1220 , 331 )
    PyEndOfifNotEven = []
    EndOfifNotEven.SetPyFunction( '' , PyEndOfifNotEven )
    IEndOfifNotEvenN = EndOfifNotEven.InPort( 'N' , 'long' )
    IEndOfifNotEvenK = EndOfifNotEven.InPort( 'K' , 'long' )
    IEndOfifNotEvenDefault = EndOfifNotEven.GetInPort( 'Default' )
    OEndOfifNotEvenN = EndOfifNotEven.OutPort( 'N' , 'long' )
    OEndOfifNotEvenK = EndOfifNotEven.OutPort( 'K' , 'long' )
    OEndOfifNotEvenGate = EndOfifNotEven.GetOutPort( 'Gate' )
    ifNotEven.SetName( 'ifNotEven' )
    ifNotEven.SetAuthor( '' )
    ifNotEven.SetComment( 'ifNotEven' )
    ifNotEven.Coords( 407 , 282 )
    IifNotEvenSyrComponent = ifNotEven.InPort( 'SyrComponent' , 'objref' )
    IifNotEvenN = ifNotEven.InPort( 'N' , 'long' )
    IifNotEvenK = ifNotEven.InPort( 'K' , 'long' )
    IifNotEvenGate = ifNotEven.GetInPort( 'Gate' )
    OifNotEvenOdd = ifNotEven.OutPort( 'Odd' , 'long' )
    OifNotEvenEven = ifNotEven.OutPort( 'Even' , 'long' )
    OifNotEvenSyrComponent = ifNotEven.OutPort( 'SyrComponent' , 'objref' )
    OifNotEvenN = ifNotEven.OutPort( 'N' , 'long' )
    OifNotEvenK = ifNotEven.OutPort( 'K' , 'long' )
    OifNotEvenDefault = ifNotEven.GetOutPort( 'Default' )
    
    # Creation of Links
    Lm3anIntegerm3incraCount = SyrStruct.Link( Om3anInteger , Im3incraCount )
    
    Lm3incraNewCountEndOfifNotEvenN = SyrStruct.Link( Om3incraNewCount , IEndOfifNotEvenN )
    Lm3incraNewCountEndOfifNotEvenN.AddCoord( 1 , 1193 , 362 )
    Lm3incraNewCountEndOfifNotEvenN.AddCoord( 2 , 1191 , 100 )
    
    LincraaNewCountEndOfforiK = SyrStruct.Link( OincraaNewCount , IEndOfforiK )
    
    Ldiv2anIntegerEndOfwhileEvenN = SyrStruct.Link( Odiv2anInteger , IEndOfwhileEvenN )
    
    LincrbaNewCountEndOfwhileEvenK = SyrStruct.Link( OincrbaNewCount , IEndOfwhileEvenK )
    LincrbaNewCountEndOfwhileEvenK.AddCoord( 1 , 992 , 521 )
    LincrbaNewCountEndOfwhileEvenK.AddCoord( 2 , 992 , 645 )
    
    LSyrComponentSyrComponentobjrefforNSyrComponent = SyrStruct.Link( OSyrComponentSyrComponentobjref , IforNSyrComponent )
    LSyrComponentSyrComponentobjrefforNSyrComponent.AddCoord( 1 , 8 , 373 )
    LSyrComponentSyrComponentobjrefforNSyrComponent.AddCoord( 2 , 8 , 181 )
    LSyrComponentSyrComponentobjrefforNSyrComponent.AddCoord( 3 , 196 , 181 )
    LSyrComponentSyrComponentobjrefforNSyrComponent.AddCoord( 4 , 196 , 71 )
    
    LforNNwhileNotOneN = SyrStruct.Link( OforNN , IwhileNotOneN )
    
    LforNNEndOfforNN = SyrStruct.Link( OforNN , IEndOfforNN )
    
    LforNKwhileNotOneK = SyrStruct.Link( OforNK , IwhileNotOneK )
    
    LforNSyrComponentEndOfforNSyrComponent = SyrStruct.Link( OforNSyrComponent , IEndOfforNSyrComponent )
    
    LforNSyrComponentwhileNotOneSyrComponent = SyrStruct.Link( OforNSyrComponent , IwhileNotOneSyrComponent )
    LforNSyrComponentwhileNotOneSyrComponent.AddCoord( 1 , 197 , 313 )
    LforNSyrComponentwhileNotOneSyrComponent.AddCoord( 2 , 197 , 373 )
    
    LforNminEndOfforNmin = SyrStruct.Link( OforNmin , IEndOfforNmin )
    
    LforNmaxEndOfforNmax = SyrStruct.Link( OforNmax , IEndOfforNmax )
    
    LwhileNotOneSyrComponentEndOfwhileNotOneSyrComponent = SyrStruct.Link( OwhileNotOneSyrComponent , IEndOfwhileNotOneSyrComponent )
    
    LwhileNotOneSyrComponentifNotEvenSyrComponent = SyrStruct.Link( OwhileNotOneSyrComponent , IifNotEvenSyrComponent )
    
    LwhileNotOneNifNotEvenN = SyrStruct.Link( OwhileNotOneN , IifNotEvenN )
    
    LwhileNotOneKifNotEvenK = SyrStruct.Link( OwhileNotOneK , IifNotEvenK )
    
    LEndOfwhileNotOneKEndOfforNK = SyrStruct.Link( OEndOfwhileNotOneK , IEndOfforNK )
    
    LforiiEndOfforii = SyrStruct.Link( Oforii , IEndOfforii )
    
    LforiKincraaCount = SyrStruct.Link( OforiK , IincraaCount )
    
    LEndOfforiKEndOfifNotEvenK = SyrStruct.Link( OEndOfforiK , IEndOfifNotEvenK )
    LEndOfforiKEndOfifNotEvenK.AddCoord( 1 , 1180 , 382 )
    LEndOfforiKEndOfifNotEvenK.AddCoord( 2 , 1180 , 289 )
    
    LwhileEvenSyrComponentEndOfwhileEvenSyrComponent = SyrStruct.Link( OwhileEvenSyrComponent , IEndOfwhileEvenSyrComponent )
    
    LwhileEvenNdiv2anEvenInteger = SyrStruct.Link( OwhileEvenN , Idiv2anEvenInteger )
    
    LwhileEvenKincrbaCount = SyrStruct.Link( OwhileEvenK , IincrbaCount )
    LwhileEvenKincrbaCount.AddCoord( 1 , 805 , 645 )
    LwhileEvenKincrbaCount.AddCoord( 2 , 805 , 522 )
    
    LEndOfwhileEvenNEndOfifNotEvenN = SyrStruct.Link( OEndOfwhileEvenN , IEndOfifNotEvenN )
    LEndOfwhileEvenNEndOfifNotEvenN.AddCoord( 1 , 1192 , 362 )
    LEndOfwhileEvenNEndOfifNotEvenN.AddCoord( 2 , 1192 , 502 )
    
    LEndOfwhileEvenKEndOfifNotEvenK = SyrStruct.Link( OEndOfwhileEvenK , IEndOfifNotEvenK )
    LEndOfwhileEvenKEndOfifNotEvenK.AddCoord( 1 , 1180 , 382 )
    LEndOfwhileEvenKEndOfifNotEvenK.AddCoord( 2 , 1180 , 522 )
    
    LifNotEvenOddm3Gate = SyrStruct.Link( OifNotEvenOdd , Im3Gate )
    LifNotEvenOddm3Gate.AddCoord( 1 , 594 , 123 )
    LifNotEvenOddm3Gate.AddCoord( 2 , 594 , 313 )
    
    LifNotEvenOddforiGate = SyrStruct.Link( OifNotEvenOdd , IforiGate )
    
    LifNotEvenEvenwhileEvenGate = SyrStruct.Link( OifNotEvenEven , IwhileEvenGate )
    LifNotEvenEvenwhileEvenGate.AddCoord( 1 , 594 , 546 )
    LifNotEvenEvenwhileEvenGate.AddCoord( 2 , 594 , 334 )
    
    LifNotEvenSyrComponentwhileEvenSyrComponent = SyrStruct.Link( OifNotEvenSyrComponent , IwhileEvenSyrComponent )
    LifNotEvenSyrComponentwhileEvenSyrComponent.AddCoord( 1 , 588 , 482 )
    LifNotEvenSyrComponentwhileEvenSyrComponent.AddCoord( 2 , 588 , 354 )
    
    LifNotEvenNwhileEvenN = SyrStruct.Link( OifNotEvenN , IwhileEvenN )
    LifNotEvenNwhileEvenN.AddCoord( 1 , 603 , 502 )
    LifNotEvenNwhileEvenN.AddCoord( 2 , 603 , 373 )
    
    LifNotEvenNm3anOddInteger = SyrStruct.Link( OifNotEvenN , Im3anOddInteger )
    LifNotEvenNm3anOddInteger.AddCoord( 1 , 605 , 99 )
    LifNotEvenNm3anOddInteger.AddCoord( 2 , 604 , 372 )
    
    LifNotEvenKwhileEvenK = SyrStruct.Link( OifNotEvenK , IwhileEvenK )
    LifNotEvenKwhileEvenK.AddCoord( 1 , 620 , 523 )
    LifNotEvenKwhileEvenK.AddCoord( 2 , 620 , 396 )
    
    LifNotEvenKforiK = SyrStruct.Link( OifNotEvenK , IforiK )
    LifNotEvenKforiK.AddCoord( 1 , 620 , 289 )
    LifNotEvenKforiK.AddCoord( 2 , 620 , 395 )
    
    LifNotEvenDefaultEndOfifNotEvenDefault = SyrStruct.Link( OifNotEvenDefault , IEndOfifNotEvenDefault )
    LifNotEvenDefaultEndOfifNotEvenDefault.AddCoord( 1 , 1204 , 407 )
    LifNotEvenDefaultEndOfifNotEvenDefault.AddCoord( 2 , 1203 , 12 )
    LifNotEvenDefaultEndOfifNotEvenDefault.AddCoord( 3 , 581 , 13 )
    LifNotEvenDefaultEndOfifNotEvenDefault.AddCoord( 4 , 581 , 418 )
    
    LEndOfifNotEvenNEndOfwhileNotOneN = SyrStruct.Link( OEndOfifNotEvenN , IEndOfwhileNotOneN )
    
    LEndOfifNotEvenKEndOfwhileNotOneK = SyrStruct.Link( OEndOfifNotEvenK , IEndOfwhileNotOneK )
    
    # Input datas
    ISyrComponentaContainer.Input( 'FactoryServer' )
    ISyrComponentaComponent.Input( 'SyrComponent' )
    IforNN.Input( 0 )
    IforNK.Input( 0 )
    IforNmin.Input( 5 )
    IforNmax.Input( 9 )
    Iforii.Input( 0 )
    
    # Output Ports of the graph
    #OEndOfforNN = EndOfforN.GetOutPort( 'N' )
    #OEndOfforNK = EndOfforN.GetOutPort( 'K' )
    #OEndOfforNSyrComponent = EndOfforN.GetOutPort( 'SyrComponent' )
    #OEndOfforNmin = EndOfforN.GetOutPort( 'min' )
    #OEndOfforNmax = EndOfforN.GetOutPort( 'max' )
    #OEndOfwhileNotOneSyrComponent = EndOfwhileNotOne.GetOutPort( 'SyrComponent' )
    #OEndOfwhileNotOneN = EndOfwhileNotOne.GetOutPort( 'N' )
    #OEndOfforii = EndOffori.GetOutPort( 'i' )
    #OEndOfwhileEvenSyrComponent = EndOfwhileEven.GetOutPort( 'SyrComponent' )
    return SyrStruct


SyrStruct = DefSyrStruct()
