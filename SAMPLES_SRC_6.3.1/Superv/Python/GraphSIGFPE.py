#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph GraphSIGFPE
#
from SuperV import *

# Graph creation of GraphSIGFPE
def DefGraphSIGFPE() :
    GraphSIGFPE = Graph( 'GraphSIGFPE' )
    GraphSIGFPE.SetName( 'GraphSIGFPE' )
    GraphSIGFPE.SetAuthor( 'JR' )
    GraphSIGFPE.SetComment( '' )
    GraphSIGFPE.Coords( 0 , 0 )
    
    # Creation of Factory Nodes
    
    SIGFPEfunc = GraphSIGFPE.FNode( 'SIGNALSComponent' , 'SIGNALSComponent' , 'SIGFPEfunc' )
    SIGFPEfunc.SetName( 'SIGFPEfunc' )
    SIGFPEfunc.SetAuthor( '' )
    SIGFPEfunc.SetContainer( 'localhost/FactoryServer' )
    SIGFPEfunc.SetComment( 'SIGFPEfunc from SIGNALSComponent' )
    SIGFPEfunc.Coords( 134 , 137 )
    ISIGFPEfunca = SIGFPEfunc.GetInPort( 'a' )
    ISIGFPEfuncb = SIGFPEfunc.GetInPort( 'b' )
    ISIGFPEfuncGate = SIGFPEfunc.GetInPort( 'Gate' )
    OSIGFPEfuncreturn = SIGFPEfunc.GetOutPort( 'return' )
    OSIGFPEfuncGate = SIGFPEfunc.GetOutPort( 'Gate' )
    
    # Input datas
    ISIGFPEfunca.Input( 1 )
    ISIGFPEfuncb.Input( 0 )
    
    # Output Ports of the graph
    #OSIGFPEfuncreturn = SIGFPEfunc.GetOutPort( 'return' )
    return GraphSIGFPE


GraphSIGFPE = DefGraphSIGFPE()
