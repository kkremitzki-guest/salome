# Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
# See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#

include $(top_srcdir)/adm_local/unix/make_common_starter.am

# Library target
lib_LTLIBRARIES = libATOMIC.la

# Library sources
dist_libATOMIC_la_SOURCES =	   \
	ATOMICGUI.h		   \
	ATOMICGUI.cxx              \
	ATOMICGUI_Data.h           \
	ATOMICGUI_Data.cxx         \
	ATOMICGUI_DataObject.h     \
	ATOMICGUI_DataObject.cxx   \
	ATOMICGUI_DataModel.h      \
	ATOMICGUI_DataModel.cxx    \
	ATOMICGUI_AddAtomDlg.h     \
	ATOMICGUI_AddAtomDlg.cxx   \
	ATOMICGUI_Selection.h      \
	ATOMICGUI_Selection.cxx    \
	ATOMICGUI_Operation.h      \
	ATOMICGUI_Operation.cxx    \
	ATOMICGUI_AddAtomOp.h      \
	ATOMICGUI_AddAtomOp.cxx    \
	ATOMICGUI_CreateMolOp.h    \
	ATOMICGUI_CreateMolOp.cxx  \
	ATOMICGUI_RenameOp.h       \
	ATOMICGUI_RenameOp.cxx     \
	ATOMICGUI_DeleteOp.h       \
	ATOMICGUI_DeleteOp.cxx     \
	ATOMICGUI_ImportExportOp.h \
	ATOMICGUI_ImportExportOp.cxx

# MOC pre-processing
MOC_FILES =                              \
	ATOMICGUI_moc.cxx                \
	ATOMICGUI_DataModel_moc.cxx      \
	ATOMICGUI_AddAtomDlg_moc.cxx     \
	ATOMICGUI_Operation_moc.cxx      \
	ATOMICGUI_CreateMolOp_moc.cxx    \
	ATOMICGUI_DeleteOp_moc.cxx       \
	ATOMICGUI_RenameOp_moc.cxx       \
	ATOMICGUI_ImportExportOp_moc.cxx \
	ATOMICGUI_AddAtomOp_moc.cxx

nodist_libATOMIC_la_SOURCES = $(MOC_FILES)

# additionnal compilation flags
libATOMIC_la_CPPFLAGS = \
	$(QT_INCLUDES) \
	$(VTK_INCLUDES) \
	$(CAS_CPPFLAGS) \
	$(PYTHON_INCLUDES) \
	$(GUI_CXXFLAGS)

# additionnal linkage flags
libATOMIC_la_LDFLAGS = \
	$(GUI_LDFLAGS) \
	-lLightApp

# resources files
nodist_salomeres_DATA = \
        ATOMIC_images.qm \
        ATOMIC_msg_en.qm
