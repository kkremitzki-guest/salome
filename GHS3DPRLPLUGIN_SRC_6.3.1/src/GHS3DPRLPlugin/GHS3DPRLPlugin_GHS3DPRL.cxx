// Copyright (C) 2007-2011  CEA/DEN, EDF R&D
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

// ---
// File   : GHS3DPRLPlugin_GHS3DPRL.cxx
// Author : Christian VAN WAMBEKE (CEA) (from Hexotic plugin Lioka RAZAFINDRAZAKA)
// ---
//
#include "GHS3DPRLPlugin_GHS3DPRL.hxx"
#include "GHS3DPRLPlugin_Hypothesis.hxx"

#include "SMDS_MeshElement.hxx"
#include "SMDS_MeshNode.hxx"

#include <TopExp_Explorer.hxx>
#include <OSD_File.hxx>

#include "utilities.h"

#ifndef WIN32
#include <sys/sysinfo.h>
#endif

#ifdef _DEBUG_
#define DUMP(txt) \
//  cout << txt
#else
#define DUMP(txt)
#endif

#include <SMESH_Gen.hxx>
#include <SMESHDS_Mesh.hxx>
#include <SMESH_ControlsDef.hxx>

#include <list>
#include <Standard_ProgramError.hxx>
#include <TCollection_AsciiString.hxx>
#include <TopTools_MapOfShape.hxx>
#include <TopoDS.hxx>
#include <BRepGProp.hxx>
#include <GProp_GProps.hxx>

/* 
extern "C" {
#include <med.h>
//#include <med_config.h>
#include <med_utils.h>
//#include <med_misc.h>
#include <stdlib.h>
}
using namespace med_2_2;*/

static void removeFile( const TCollection_AsciiString& fileName )
{
  try {
    OSD_File( fileName ).Remove();
  }
  catch ( Standard_ProgramError ) {
    MESSAGE("Can't remove file: " << fileName.ToCString() << " ; file does not exist or permission denied");
  }
}

//=============================================================================
GHS3DPRLPlugin_GHS3DPRL::GHS3DPRLPlugin_GHS3DPRL(int hypId, int studyId, SMESH_Gen* gen)
  : SMESH_3D_Algo(hypId, studyId, gen)
{
  MESSAGE("GHS3DPRLPlugin_GHS3DPRL::GHS3DPRLPlugin_GHS3DPRL");
  _name = "GHS3DPRL_3D";
  _shapeType = (1 << TopAbs_SHELL) | (1 << TopAbs_SOLID);// 1 bit /shape type
  _countSubMesh=0;
  _nodeRefNumber=0;
  _compatibleHypothesis.push_back("GHS3DPRL_Parameters");
}

//=============================================================================
GHS3DPRLPlugin_GHS3DPRL::~GHS3DPRLPlugin_GHS3DPRL()
{
  MESSAGE("GHS3DPRLPlugin_GHS3DPRL::~GHS3DPRLPlugin_GHS3DPRL");
}

//=============================================================================
bool GHS3DPRLPlugin_GHS3DPRL::CheckHypothesis
                         (SMESH_Mesh& aMesh,
                          const TopoDS_Shape& aShape,
                          SMESH_Hypothesis::Hypothesis_Status& aStatus)
{
  //MESSAGE("GHS3DPRLPlugin_GHS3DPRL::CheckHypothesis");
  _hypothesis = NULL;

  list<const SMESHDS_Hypothesis*>::const_iterator itl;
  const SMESHDS_Hypothesis* theHyp;

  const list<const SMESHDS_Hypothesis*>& hyps = GetUsedHypothesis(aMesh, aShape);
  int nbHyp = hyps.size();
  if (!nbHyp)
  {
    aStatus = SMESH_Hypothesis::HYP_OK;
    return true;  // can work with no hypothesis
  }

  itl = hyps.begin();
  theHyp = (*itl); // use only the first hypothesis

  string hypName = theHyp->GetName();
  if (hypName == "GHS3DPRL_Parameters")
  {
    _hypothesis = static_cast<const GHS3DPRLPlugin_Hypothesis*> (theHyp);
    ASSERT(_hypothesis);
    aStatus = SMESH_Hypothesis::HYP_OK;
  }
  else
    aStatus = SMESH_Hypothesis::HYP_INCOMPATIBLE;

  return aStatus == SMESH_Hypothesis::HYP_OK;
}

//=======================================================================
static bool writeGHS3DPRLFiles (const TCollection_AsciiString &  GHS3DPRL_In,
                                SMESHDS_Mesh *                   theMesh,
                                map <int,int> &                  theSmdsToGHS3DPRLIdMap,
                                map <int,const SMDS_MeshNode*> & theGHS3DPRLIdToNodeMap)
{
   bool Ok;
   int ifam=0;
   TCollection_AsciiString namefile(GHS3DPRL_In);
   namefile+=".points";
   removeFile(namefile);
   ofstream theFile;
   theFile.open(namefile.ToCString(),ios::out);
#ifdef WIN32
   Ok=theFile->is_open();
#else
   Ok=theFile.rdbuf()->is_open();
#endif
   if (!Ok)
   {
      INFOS("Can't write into "<<namefile.ToCString());
      return false;
   }
   cout<<endl<<"writeGHS3DPRLFiles version 2.1 "<<endl;
   cout<<endl<<"Creating GHS3DPRL processed mesh file : "<<namefile<<endl;

   int nbVertices=theMesh->NbNodes();
   int nbFaces=theMesh->NbFaces();        //triangles or quadrangles
   const char* space="  ";
   const int dummyint=1;                  //nrs,nsd,refnum=1 (for wrap)

   // Writing SMESH points into GHS3DPRL File.points
   theFile<<nbVertices<<endl;

   int aSmdsNodeID=1;
   const SMDS_MeshNode* node_2;
   SMDS_NodeIteratorPtr itOnNode=theMesh->nodesIterator();
   //int ifam=100;//test famille
   theFile.precision(15); theFile.setf(ios::scientific,ios::floatfield);
   //cout<<"set precision 15 on float\n";
   while (itOnNode->more())
   {
      node_2 = itOnNode->next();
      theSmdsToGHS3DPRLIdMap.insert(map <int,int>::value_type(node_2->GetID(),aSmdsNodeID));
      theGHS3DPRLIdToNodeMap.insert(map <int,const SMDS_MeshNode*>::value_type(aSmdsNodeID,node_2));
      theFile<<node_2->X()<<space<<node_2->Y()<<space<<node_2->Z()<<space<<ifam<<endl;
      aSmdsNodeID++;
      //if (aSmdsNodeID==11) ifam++;
   }
   //no specified points;
   theFile.close();

   namefile=GHS3DPRL_In+".faces";
   removeFile(namefile);
   theFile.open(namefile.ToCString(),ios::out);
#ifdef WIN32
   Ok=theFile->is_open();
#else
   Ok=theFile.rdbuf()->is_open();
#endif
   if (!Ok)
   {
      INFOS("Can't write into "<<namefile.ToCString());
      return false;
   }
   cout<<endl<<"Creating GHS3DPRL processed mesh file : "<<namefile<<endl;

   // Writing SMESH faces into GHS3DPRL File.faces
   theFile<<nbFaces<<" 0"<<endl;   //NB_ELEMS DUMMY_INT
                                   //" 0" is a reserved parameter

   const SMDS_MeshElement* aFace;
   map<int,int>::const_iterator itOnSmdsNode;
   SMDS_ElemIteratorPtr itOnFaceNode;
   SMDS_FaceIteratorPtr itOnSmdsFace = theMesh->facesIterator();
   long nbNoTriangles=0;
   int ifaces=0;
   //ifam=300;
   while (itOnSmdsFace->more())
   {
      aFace=itOnSmdsFace->next();
      itOnFaceNode=aFace->nodesIterator();
      const int nbNodes=aFace->NbNodes();
      if (nbNodes!=3) nbNoTriangles++;
      ifaces++;
      theFile<<nbNodes<<space;        // NB_NODES
      while (itOnFaceNode->more())
      {
          aSmdsNodeID=itOnFaceNode->next()->GetID();
          itOnSmdsNode=theSmdsToGHS3DPRLIdMap.find(aSmdsNodeID);
          ASSERT(itOnSmdsNode!=theSmdsToGHS3DPRLIdMap.end());
          theFile<<space<<(*itOnSmdsNode).second; //NODE_1 NODE_2 ...
      }
      //(NB_NODES+1) times: DUMMY_INT
      //if (ifaces==11) ifam++;
      theFile<<space<<ifam;
      for ( int i=1; i<=nbNodes; i++) theFile<<space<<200+i;
      theFile<<endl;
   }
   theFile.close();

   cout<<"Processed mesh files created, they contains :\n";
   cout<<"    "<<nbVertices<<" vertices\n";
   if (nbNoTriangles==0)
      cout<<"    "<<nbFaces<<" faces\n\n";
   else
      cout<<"    "<<nbFaces<<" faces with "<<nbNoTriangles<<"faces no triangles\n\n";
   return true;
}

//=======================================================================
static bool getInt( int & theValue, char * & theLine )
{
  char *ptr;
  theValue = strtol( theLine, &ptr, 10 );
  if ( ptr == theLine ||
      // there must not be neither '.' nor ',' nor 'E' ...
      (*ptr != ' ' && *ptr != '\n' && *ptr != '\0'))
    return false;

  DUMP( "  " << theValue );
  theLine = ptr;
  return true;
}

//=======================================================================
static bool getDouble( double & theValue, char * & theLine )
{
  char *ptr;
  theValue = strtod( theLine, &ptr );
  if ( ptr == theLine )
    return false;

  DUMP( "   " << theValue );
  theLine = ptr;
  return true;
}

//=======================================================================

#define GHS3DPRLPlugin_BUFLENGTH 256
#define GHS3DPRLPlugin_ReadLine(aPtr,aBuf,aFile,aLineNb) \
{  aPtr = fgets( aBuf, GHS3DPRLPlugin_BUFLENGTH - 2, aFile ); aLineNb++; DUMP(endl); }

//=======================================================================
static bool readResult(FILE *                           theFile,
                       SMESHDS_Mesh *                   theMesh,
                       const TopoDS_Shape &             theShape,
                       map <int,const SMDS_MeshNode*> & theGHS3DPRLIdToNodeMap,
                       const TCollection_AsciiString &  GHS3DPRL_Out,
                       int &                            nodeRefNumber)
{
  // ---------------------------------
  // Read generated elements and nodes
  // ---------------------------------

  cout << "Reading GHS3DPRL output file : " << GHS3DPRL_Out << endl;
  cout << endl;

  char aBuffer[ GHS3DPRLPlugin_BUFLENGTH ];
  char * aPtr;
  int aLineNb = 0;
  int shapeID = theMesh->ShapeToIndex( theShape );

  int line = 1, EndOfFile = 0, nbElem = 0, nField = 10, nbRef = 0, aGHS3DPRLNodeID = 0;
  const char * theField;

  const char * tabField [nField];
  int    tabRef [nField];

  tabField[0] = "MeshVersionFormatted";    tabRef[0] = 0;
  tabField[1] = "Dimension";               tabRef[1] = 0;
  tabField[2] = "Vertices";                tabRef[2] = 3;
  tabField[3] = "Edges";                   tabRef[3] = 2;
  tabField[4] = "Triangles";               tabRef[4] = 3;
  tabField[5] = "Quadrilaterals";          tabRef[5] = 4;
  tabField[6] = "Hexahedra";               tabRef[6] = 8;
  tabField[7] = "Corners";                 tabRef[7] = 1;
  tabField[8] = "Ridges";                  tabRef[0] = 1;
  tabField[9] = "End";                     tabRef[0] = 0;

  nodeRefNumber += theMesh->NbNodes();

  SMDS_NodeIteratorPtr itOnGHS3DPRLInputNode = theMesh->nodesIterator();
  while ( itOnGHS3DPRLInputNode->more() )
      theMesh->RemoveNode( itOnGHS3DPRLInputNode->next() );

  while ( EndOfFile == 0  ) {
      GHS3DPRLPlugin_ReadLine( aPtr, aBuffer, theFile, aLineNb );
      for ( int iField = 0; iField < nField; iField++ ) {
          stringstream theMessage;
          theField = tabField[iField];
          if ( strncmp(aPtr, theField, strlen(theField)) == 0 ) {
              if ( strcmp(theField, "End") == 0 ) {
                  EndOfFile = 1;
                  theMessage << "End of GHS3DPRL output file has been reached";
              }
              else {
                  GHS3DPRLPlugin_ReadLine( aPtr, aBuffer, theFile, aLineNb );
                  line++;
                  getInt( nbElem, aPtr );

                  if ( strcmp(theField, "MeshVersionFormatted") == 0 )
                      theMessage << "GHS3DPRL mesh descriptor : " << theField << " " << nbElem;
                  else if ( strcmp(theField, "Dimension") == 0 )
                      theMessage << "GHS3DPRL mesh of " << nbElem << "D dimension";
                  else if ( strcmp(theField, "Vertices")       == 0 ||
                            strcmp(theField, "Edges")          == 0 ||
                            strcmp(theField, "Quadrilaterals") == 0 ||
                            strcmp(theField, "Hexahedra")      == 0 ) {
                      nbRef = tabRef[iField];
                      GHS3DPRLPlugin_ReadLine( aPtr, aBuffer, theFile, aLineNb );           // read blank line

                      if ( strcmp(theField, "Vertices") == 0 ) {
                          int aGHS3DPRLID;
                          double coord[nbRef];
                          SMDS_MeshNode * aGHS3DPRLNode;

                          for ( int iElem = 0; iElem < nbElem; iElem++ ) {
                              aGHS3DPRLID = iElem + 1 + nodeRefNumber;
                              GHS3DPRLPlugin_ReadLine( aPtr, aBuffer, theFile, aLineNb );   // read file lines
                              for ( int iCoord = 0; iCoord < 3; iCoord++ )
                                  getDouble ( coord[ iCoord ], aPtr );
                              aGHS3DPRLNode = theMesh->AddNode(coord[0], coord[1], coord[2]);
                              theMesh->SetNodeInVolume( aGHS3DPRLNode, shapeID );
                              theGHS3DPRLIdToNodeMap[ aGHS3DPRLID ] = aGHS3DPRLNode;
                          }
                      }
                      else {
                          const SMDS_MeshNode * node[nbRef];
                          SMDS_MeshElement* aGHS3DPRLElement;
                          map <int,const SMDS_MeshNode*>::iterator itOnGHS3DPRLNode;

                          for ( int iElem = 0; iElem < nbElem; iElem++ ) {
                              GHS3DPRLPlugin_ReadLine( aPtr, aBuffer, theFile, aLineNb );   // read file lines
                              for ( int iRef = 0; iRef < nbRef; iRef++ ) {
                                  getInt ( aGHS3DPRLNodeID, aPtr );                         // read nbRef aGHS3DPRLNodeID
                                  aGHS3DPRLNodeID += nodeRefNumber;
                                  itOnGHS3DPRLNode = theGHS3DPRLIdToNodeMap.find( aGHS3DPRLNodeID );
                                  node[ iRef ] = itOnGHS3DPRLNode->second;
                              }

                              if ( strcmp(theField, "Edges") == 0 )                        // create an element
                                  aGHS3DPRLElement = theMesh->AddEdge( node[0], node[1] );
                              else if ( strcmp(theField, "Quadrilaterals") == 0 )
                                  aGHS3DPRLElement = theMesh->AddFace( node[0], node[1], node[2], node[3] );
                              else if ( strcmp(theField, "Hexahedra") == 0 )
                                  aGHS3DPRLElement = theMesh->AddVolume( node[0], node[1], node[2], node[3], node[4], node[5], node[6], node[7] );

                              theMesh->SetMeshElementOnShape( aGHS3DPRLElement, shapeID );
                          }
                      }
                      theMessage << nbElem << " " << theField << " created";
                  }
              }
              if ( theMessage.str().size() != 0 ) {
                  cout << theMessage.str() << endl;
                  break;
              }
          }
      }
  }
  cout << endl;
  return true;
}

//=============================================================================
// Pass parameters to GHS3DPRL
void GHS3DPRLPlugin_GHS3DPRL::SetParameters(const GHS3DPRLPlugin_Hypothesis* hyp)
{
  if (hyp) {
    MESSAGE("GHS3DPRLPlugin_GHS3DPRL::SetParameters");
    _MEDName = hyp->GetMEDName();  //"DOMAIN\0"
    _NbPart = hyp->GetNbPart();
    _KeepFiles = hyp->GetKeepFiles();
    _Background = hyp->GetBackground();
    _ToMeshHoles = hyp->GetToMeshHoles();
  }
}

//=======================================================================
//before launching salome
//SALOME_TMP_DIR (for keep tepal intermediates files) could be set in user's directories
static TCollection_AsciiString getTmpDir()
{
  TCollection_AsciiString aTmpDir;
  char *Tmp_dir = getenv("SALOME_TMP_DIR");
  if (Tmp_dir == NULL) Tmp_dir = getenv("TMP");
  if(Tmp_dir != NULL)
  {
    aTmpDir = Tmp_dir;
#ifdef WIN32
      if(aTmpDir.Value(aTmpDir.Length()) != '\\') aTmpDir+='\\';
#else
      if(aTmpDir.Value(aTmpDir.Length()) != '/') aTmpDir+='/';
#endif
  }
  else
  {
#ifdef WIN32
      aTmpDir = TCollection_AsciiString("C:\\");
#else
      aTmpDir = TCollection_AsciiString("/tmp/");
#endif
  }
  return aTmpDir;
}

//=============================================================================
// Here we are going to use the GHS3DPRL mesher
bool GHS3DPRLPlugin_GHS3DPRL::Compute(SMESH_Mesh& theMesh,
                                     const TopoDS_Shape& theShape)
{
   bool Ok;
   TCollection_AsciiString pluginerror("ghs3dprl: ");
   SMESHDS_Mesh* meshDS = theMesh.GetMeshDS();
   //cout<<"GetMeshDS done\n";
   if (_countSubMesh==0){
      MESSAGE("GHS3DPRLPlugin_GHS3DPRL::Compute");
      _countTotal=0;
      TopExp_Explorer expf(meshDS->ShapeToMesh(), TopAbs_SOLID);
      for ( ; expf.More(); expf.Next() ) _countTotal++;
   }
   _countSubMesh++;
   //cout<<"Compute _countSubMesh "<<_countSubMesh<<endl;
   //no stuff if multiples submesh, multiple call compute
   //mesh all in one pass tepal (the last)
   if (_countSubMesh != _countTotal ) return true;

   //stuff on last call 
   if (_hypothesis==NULL){
      pluginerror += "No existing parameters/hypothesis for GHS3DPRL";
      cout <<"\n"<<pluginerror<<"\n\n";
      error(COMPERR_ALGO_FAILED, pluginerror.ToCString() );
      return false;
   }
   SetParameters(_hypothesis);
   cout << "\n" << _name << " parameters :\n" << endl;
   cout << "     generic path/name of MED Files = " << _MEDName << endl;
   cout << "     number of partitions = " << _NbPart << endl;
   cout << "     mesh holes = " << _ToMeshHoles << endl;
   cout << "     keep intermediates files (from tepal) = " << _KeepFiles << endl;
   cout << "     background (from tepal) = " << _Background << "\n\n";

      //string tmpDir=getTmpDir_new();
      TCollection_AsciiString
         tmpDir=getTmpDir(),
         GHS3DPRL_In,GHS3DPRL_Out,GHS3DPRL_Outxml,
         run_GHS3DPRL("tepal2med "),rm("rm "),run_nokeep_files,
         NbPart,fileskinmed(""),path,casenamemed;  //_MEDName.c_str());

      casenamemed += (char *)_MEDName.c_str();
      int n=casenamemed.SearchFromEnd('/');
      if (n>0) {
         path=casenamemed.SubString(1,n);
         casenamemed=casenamemed.SubString(n+1,casenamemed.Length()); 
      }
      else
         path=tmpDir;

      if (casenamemed.Length()>20){
         casenamemed=casenamemed.SubString(1,20);
         cerr<<"MEDName truncated (no more 20 characters) = "<<casenamemed<<endl;
      }
      cout<<"path="<<path<<endl;
      cout<<"casenamemed="<<casenamemed<<endl;

      map <int,int> aSmdsToGHS3DPRLIdMap;
      map <int,const SMDS_MeshNode*> aGHS3DPRLIdToNodeMap;
      GHS3DPRL_In = path + "GHS3DPRL";
      GHS3DPRL_Out = path + casenamemed;
      GHS3DPRL_Outxml = path + casenamemed + ".xml"; //master file
      NbPart=_NbPart;
      //tepal2med --casename=/home/whoami/tmp/GHS3DPRL --number=5 --medname=DOMAIN 
      //          --limitswap=1000 --verbose=0 --test=no --menu=no --launchtepal=yes;
      
      run_GHS3DPRL = run_GHS3DPRL +
                     " --casename=" + GHS3DPRL_In + 
                     " --number=" + NbPart + 
                     " --medname=" + GHS3DPRL_Out +
                     " --launchtepal=yes";
      if (_ToMeshHoles) run_GHS3DPRL += " --meshholes=yes"; else run_GHS3DPRL += " --meshholes=no";
      if (_Background) run_GHS3DPRL += " --background=yes"; else run_GHS3DPRL += " --background=no";
      run_nokeep_files = rm +GHS3DPRL_In + "* " + path + "tepal.log";
      system( run_nokeep_files.ToCString() ); //clean files
      run_nokeep_files = rm + GHS3DPRL_In + "* ";

      cout<<"GHS3DPRL command : "<<run_GHS3DPRL.ToCString()<<endl;
      cout<<"Write files .faces .point ...";
      GHS3DPRL_Out = path + casenamemed;
      removeFile( GHS3DPRL_Outxml ); //only the master xml file
      Ok=writeGHS3DPRLFiles(GHS3DPRL_In, meshDS, aSmdsToGHS3DPRLIdMap, aGHS3DPRLIdToNodeMap);
      if (Ok) {cout<<" ...done\n";}
      else {
         cout<<" ...NOT done\n";
         pluginerror = pluginerror + "problem writing input tepal files " + GHS3DPRL_In + "[.faces|.points]";
      }

      //Ecriture dans un fichier MED ?v2.? meme si not Ok
      //create empty file -> avoid warning message
      //med_idt fid=MEDouvrir((const char *)fileskinmed.ToCString(),MED_CREATION);
      //med_err ret=MEDfermer(fid);
      //fileskinmed=fileskinmed + "cp /home/wambeke/empty.med "+ path + "GHS3DPRL_skin.med";
      //system( fileskinmed.ToCString() );
      fileskinmed=path + "GHS3DPRL_skin.med";
      cout<<"Write file "<<fileskinmed<<"...";
      theMesh.ExportMED(fileskinmed.ToCString(),"SKIN_INITIAL",true,1);
      cout<<" ...done\n";

      if (!Ok) {
         error(COMPERR_ALGO_FAILED, pluginerror.ToCString());
         return false; //pb abandonne
      }

      //sometimes it is better to wait flushing files on slow filesystem...
      system( "sleep 3" );
      system( run_GHS3DPRL.ToCString() );
      system( "sleep 3" );

      //cout<<"!!!reprise plugin!!!"<<endl;

      if (_Background) {
         pluginerror = pluginerror + "backgrounding... plugin is not waiting for output files "+ casenamemed + "_*.med";
         cout<<pluginerror<<endl;
         error(COMPERR_ALGO_FAILED, pluginerror.ToCString());
         return false; //but it is not a problem but if true my message is overwritten
         //return true; //but it is not a problem, 
      }

      // read a result, GHS3DPRL_Out is the name of master file (previous xml format)
      FILE * aResultFile = fopen( GHS3DPRL_Outxml.ToCString(), "r" );
      if (aResultFile){
          //Ok = readResult( aResultFile, meshDS, theShape, aGHS3DPRLIdToNodeMap, GHS3DPRL_Out, _nodeRefNumber );
          Ok = true;
          Ok = false; //but it is not a problem but if true my message is overwritten
          fclose(aResultFile);
          cout<<"GHS3DPRL OK output master file "<<casenamemed<<".xml exist !\n\n";
          pluginerror = pluginerror + "new tetaedra not in memory, but stored in files "+ casenamemed + "_*.med";
          cout<<pluginerror<<endl;
          error(COMPERR_ALGO_FAILED, pluginerror.ToCString());
          if (!_KeepFiles) system( run_nokeep_files.ToCString() );
      }
      else{
          Ok = false; //it is a problem AND my message is NOT overwritten
          pluginerror = pluginerror + "output master file " + casenamemed + ".xml do not exist";
          cout<<pluginerror<<endl;
          error(COMPERR_ALGO_FAILED, pluginerror.ToCString() );
          cout<<"GHS3DPRL KO output files "<<GHS3DPRL_Out<<" do not exist ! see intermediates files keeped:\n";
          TCollection_AsciiString run_list_files("ls -alt ");
          run_list_files +=  GHS3DPRL_Out + "* " + GHS3DPRL_In + "* " + path + "tepal.log";
          system( run_list_files.ToCString() );
          cout<<endl;
      }
      _countSubMesh=0;


    return Ok;
   /*pid_t pid = fork();
   if (pid > 0) {
      //Processus p�re
      cout<<"le pere est la\n";
      system("echo le pere > lepere.tmp");
      system("sleep 10");
      system("ps -edf |grep wambeke > pslepere.tmp");
      cout<<"le pere return 0\n";
      return 0; //ok
      //exit(0);
   } else if (pid == 0) {
      //Processus fils
      cout<<"le fils est la\n";
      //On rend le fils ind�pendant de tout terminal
      setsid();
      system("sleep 20");
      system("echo le fils > lefils.tmp");
      system("sleep 20");
      system("ps -edf |grep wambeke > pslefils.tmp");
      cout<<"le fils return 0\n";
      return 0; //ok
   } else {
      //Traitement d'erreur
      cout<<"ya probleme sur fork()\n";
      return 1; //ko
   }*/
}

//=============================================================================
ostream & GHS3DPRLPlugin_GHS3DPRL::SaveTo(ostream & save)
{
  return save;
}

//=============================================================================
istream & GHS3DPRLPlugin_GHS3DPRL::LoadFrom(istream & load)
{
  return load;
}

//=============================================================================
ostream & operator << (ostream & save, GHS3DPRLPlugin_GHS3DPRL & hyp)
{
  return hyp.SaveTo( save );
}

//=============================================================================
istream & operator >> (istream & load, GHS3DPRLPlugin_GHS3DPRL & hyp)
{
  return hyp.LoadFrom( load );
}


//=============================================================================
/*!
 *  
 */
//=============================================================================
bool GHS3DPRLPlugin_GHS3DPRL::Evaluate(SMESH_Mesh& aMesh,
				       const TopoDS_Shape& aShape,
				       MapShapeNbElems& aResMap)
{
  int nbtri = 0, nbqua = 0;
  double fullArea = 0.0;
  for (TopExp_Explorer exp(aShape, TopAbs_FACE); exp.More(); exp.Next()) {
    TopoDS_Face F = TopoDS::Face( exp.Current() );
    SMESH_subMesh *sm = aMesh.GetSubMesh(F);
    MapShapeNbElemsItr anIt = aResMap.find(sm);
    if( anIt==aResMap.end() ) {
      SMESH_ComputeErrorPtr& smError = sm->GetComputeError();
      smError.reset( new SMESH_ComputeError(COMPERR_ALGO_FAILED,
					    "Submesh can not be evaluated",this));
      return false;
    }
    std::vector<int> aVec = (*anIt).second;
    nbtri += Max(aVec[SMDSEntity_Triangle],aVec[SMDSEntity_Quad_Triangle]);
    nbqua += Max(aVec[SMDSEntity_Quadrangle],aVec[SMDSEntity_Quad_Quadrangle]);
    GProp_GProps G;
    BRepGProp::SurfaceProperties(F,G);
    double anArea = G.Mass();
    fullArea += anArea;
  }

  // collect info from edges
  int nb0d_e = 0, nb1d_e = 0;
  bool IsQuadratic = false;
  bool IsFirst = true;
  TopTools_MapOfShape tmpMap;
  for (TopExp_Explorer exp(aShape, TopAbs_EDGE); exp.More(); exp.Next()) {
    TopoDS_Edge E = TopoDS::Edge(exp.Current());
    if( tmpMap.Contains(E) )
      continue;
    tmpMap.Add(E);
    SMESH_subMesh *aSubMesh = aMesh.GetSubMesh(exp.Current());
    MapShapeNbElemsItr anIt = aResMap.find(aSubMesh);
    std::vector<int> aVec = (*anIt).second;
    nb0d_e += aVec[SMDSEntity_Node];
    nb1d_e += Max(aVec[SMDSEntity_Edge],aVec[SMDSEntity_Quad_Edge]);
    if(IsFirst) {
      IsQuadratic = (aVec[SMDSEntity_Quad_Edge] > aVec[SMDSEntity_Edge]);
      IsFirst = false;
    }
  }
  tmpMap.Clear();

  double ELen = sqrt(2.* ( fullArea/(nbtri+nbqua*2) ) / sqrt(3.0) );

  GProp_GProps G;
  BRepGProp::VolumeProperties(aShape,G);
  double aVolume = G.Mass();
  double tetrVol = 0.1179*ELen*ELen*ELen;
  double CoeffQuality = 0.9;
  int nbVols = (int)aVolume/tetrVol/CoeffQuality;
  int nb1d_f = (nbtri*3 + nbqua*4 - nb1d_e) / 2;
  int nb1d_in = (int) ( nbVols*6 - nb1d_e - nb1d_f ) / 5;
  std::vector<int> aVec(SMDSEntity_Last);
  for(int i=0; i<SMDSEntity_Last; i++) aVec[i]=0;
  if( IsQuadratic ) {
    aVec[SMDSEntity_Node] = nb1d_in/6 + 1 + nb1d_in;
    aVec[SMDSEntity_Quad_Tetra] = nbVols - nbqua*2;
    aVec[SMDSEntity_Quad_Pyramid] = nbqua;
  }
  else {
    aVec[SMDSEntity_Node] = nb1d_in/6 + 1;
    aVec[SMDSEntity_Tetra] = nbVols - nbqua*2;
    aVec[SMDSEntity_Pyramid] = nbqua;
  }
  SMESH_subMesh *sm = aMesh.GetSubMesh(aShape);
  aResMap.insert(std::make_pair(sm,aVec));

  return true;
}
