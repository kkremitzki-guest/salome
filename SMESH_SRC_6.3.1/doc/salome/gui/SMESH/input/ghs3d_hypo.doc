/*!

\page ghs3d_hypo_page GHS3D Parameters hypothesis

\n GHS3D Parameters hypothesis works only with <b>Tetrahedron (GHS3D)</b> 
algorithm. This algorithm is a commercial software.

<h1>General parameters</h1>

\image html ghs3d_parameters_basic.png

<ul>
<li><b>Name</b> - allows to define the name of the hypothesis (GHS3D 
Parameters by default).</li>

<li><b>To mesh holes</b> - if checked, the algorithm will 
create mesh in the holes inside a solid shape, else only the outermost
shape will be meshed. Volumic elements created within holes are bound
to the solid.</li>

<li><b>Optimization level</b> - allows choosing the required
optimization level: none, light, medium or strong. Higher level of
optimisation provides better mesh, but can be time-consuming.
</li>

<h1>Advanced parameters</h1>

\image html ghs3d_parameters_advanced.png

<li><b>Maximum memory size</b> - launches ghs3d software with
work space limited to the specified amount of RAM, in Mbytes. If this option is
checked off, the software will be launched with 7O% of the total RAM space. </li>

<li><b>Initial memory size</b> - starts ghs3d software with
the specified amount of work space, in Mbytes. If this option is checked off, the
software will be started with 100 Megabytes of working space. </li>

<li><b>Working directory</b> - allows defining the folder for input and output
files of ghs3d software, which are the files starting with "GHS3D_" prefix. </li>

<li><b>Keep working files</b> - allows checking input and output files
of ghs3d software, while usually these files are removed after the
launch of the mesher.</li>

<li><b>Verbose level</b> - to choose verbosity level in the range from
0 to 10.
<ul> <li>0, no standard output,
</li><li>2, prints the data, quality statistics of the skin and final
meshes and indicates when the final mesh is being saved. In addition
the software gives indication regarding the CPU time.
</li><li>10, same as 2 plus the main steps in the computation, quality
statistics histogram of the skin mesh, quality statistics histogram
together with the characteristics of the final mesh.
</li></ul></li>

<li><b>To create new nodes</b> - if this option is checked off, ghs3d
tries to create tetrahedrons using only the nodes of the 2D mesh.</li>

<li><b>To remove the initial central point</b> TetMesh-GHS3D adds an internal point 
at the gravity centre of the bounding box to speed up and to simplify 
the meshing process. However, it is possible to refrain from creating 
this point by using the command line option -no initial central point. This can be
particularly useful to generate a volume mesh without internal points at all and in some rare cases
at the boundary regeneration phase when it is impossible to proceed
with the standard options
(for example, when one dimension of the domain is more than 20 times greater than the other two).
Use this option if the boundary regeneration has failed with the standard parameters and before using
the recovery version (command line option -C).
Note: when using this option, the speed of the meshing process may
decrease, and the quality may change.
Note: the boundary regeneration may fail with this option, in some rare cases.</li>

<li><b>To use boundary recovery version</b> - enables using a
boundary recovery module which tries to
create volume meshes starting from very poor quality surface meshes
(almost flat triangles on the surface, high density propagation,
extreme aspect ratios, etc.) which fails with the standard version. The
resulting volume mesh will however most likely have a very poor
quality (poor aspect ratio of elements, tetrahedra with a very small
positive volume).</li>

<li><b>To use FEM correction</b> - Applies finite-element correction by 
replacing overconstrained elements where it is possible. At first the process 
slices the overconstrained edges and at second the overconstrained 
facets. This ensures that there are no edges with two boundary
vertices and that there are no facets with three boundary vertices. TetMesh-GHS3D gives the initial 
and final overconstrained edges and facets. It also gives the facets
which have three edges on the boundary.
Note: when using this option, the speed of the meshing process may 
decrease, quality may change, and the smallest volume may be smaller.
By default, the FEM correction is not used.</li>

<li><b>Option as text</b> - allows to input in the command line any text
for ghs3d, for example, advanced options. </li>

</ul>


<h1>Enforced vertices</h1>

\image html ghs3d_enforced_vertices.png

GHS3D algorithm can locally make the mesh finer. It is possible to define enforced vertices in the volume where the mesh will be detailed.

A node will be created at the enforced vertex coordinates. There is no need to create a vertex in CAD.

An enforced vertex is defined by:
<ul>
<li>The (x,y,z) cartesian coordinates</li>
<li>A constant physical size</li>
</ul>

<br><b>See Also</b> a sample TUI Script of the \ref tui_ghs3d "creation of a Ghs3D hypothesis", including enforced vertices.


*/
