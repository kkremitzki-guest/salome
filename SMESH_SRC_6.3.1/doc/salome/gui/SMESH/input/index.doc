/*!

\mainpage Introduction to MESH

\image html a-viewgeneral.png

\n \b MESH module of SALOME is destined for:
<ul>
<li>\ref importing_exporting_meshes_page "import and export of meshes in MED format";</li>
<li>\subpage about_meshes_page "meshing geometrical models"
previously created or imported by the Geometry component; </li>
<li>\subpage viewing_meshes_overview_page "viewing created meshes" in
the VTK viewer;</li>
<li>\subpage grouping_elements_page "creating groups of mesh elements";</li>
<li>applying to meshes \subpage quality_page "Quality Controls", 
allowing to highlight important elements;
<li>filtering sub-sets of mesh entities (nodes elements) using
\subpage filters_page "Filters" functionality;</li>
<li>\subpage modifying_meshes_page "modifying meshes" with a vast
array of dedicated operations;</li> 
<li>different \subpage measurements_page "measurements" of the mesh objects;
<li>easily setting parameters via the variables predefined in
\subpage using_notebook_mesh_page "Salome notebook".</li>
</ul>

Almost all mesh module functionalities are accessible via
\subpage smeshpy_interface_page "Mesh module Python interface".

Other functions are available in <a class="el" target="_new" href="../../tui/SMESH/docutils/index.html">salome.smesh python package</a>.


\image html image7.jpg "Example of MESH module usage for engineering tasks"

*/
