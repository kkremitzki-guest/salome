/*!

\page extrusion_page Extrusion

\n Extrusion is used to build mesh elements of plus one
dimension than the input ones. Any line or planar element can be
extruded. Line elements will extrude into quadrilateral plane elements.
Triangular and Quadrilateral plane elements extrude into Pentahedron
and Hexahedron solids respectively.

<em>To use extrusion:</em>
<ol>
<li>From the \b Modification menu choose the \b Extrusion item or click
<em>"Extrusion"</em> button in the toolbar. 

\image html image91.png
<center><em>"Extrusion" button</em></center>

The following dialog common for line and planar elements will appear:

\image html extrusionalongaline1.png

\image html extrusionalongaline2.png

</li>

<li>In this dialog:
<ul>
<li>select the type of elements which will be extruded (1D or 2D),</li>
<li>specify the IDs of the elements which will be extruded:
<ul>
<li><b>Select the whole mesh, submesh or group</b> activating this
checkbox; or</li>
<li>choose mesh elements with the mouse in the 3D Viewer. It is
possible to select a whole area with a mouse frame; or</li> 
<li>input the element IDs directly in <b>ID Elements</b> field. The selected elements will be highlighted in the
viewer; or</li>
<li>apply Filters. <b>Set filter</b> button allows to apply a filter to the selection of elements. See more
about filters in the \ref selection_filter_library_page "Selection filter library" page.</li>
</ul>
</li>
<li>If the <b>Extrude to Distance</b> radio button is selected</li>
<ul>
<li>specify the distance at which the elements will be extruded,</li>
</ul>
<li>If the <b>Extrude Along Vector</b> radio button is selected</li>
<ul>
<li>specify the coordinates of the vector along which the elements will be extruded, or select the face (the normal to the face will define the vector)</li>
<li>specify the distance of extrusion along the vector,</li>
</ul>
<li>specify the number of steps;</li>
<li>activate  <b>Generate Groups</b> checkbox if it is necessary to copy the groups of
elements of the source mesh to the newly created one. </li>
</li>
</ul>

<li>Click \b Apply or <b> Apply and Close</b>  button to confirm the operation.</li>
</ol>

\image html image77.jpg "The mesh with an edge selected for extrusion"

\image html image76.jpg "The mesh with extruded edge" 

<br><b>See Also</b> a sample TUI Script of an 
\ref tui_extrusion "Extrusion" operation. 

*/
