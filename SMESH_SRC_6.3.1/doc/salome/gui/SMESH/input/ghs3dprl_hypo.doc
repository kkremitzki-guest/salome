/*!

\page ghs3dprl_hypo_page GHS3DPRL Parameters hypothesis

\n GHS3DPRL Parameters hypothesis works only with <b>Tetrahedron (Tepal with TetMesh-GHS3D)</b> algorithm. 
This algorithm is a commercial software, its use requires a licence (http://www.distene.com/fr/build/offer.html).
\n Tepal_V1.4 gives the possibility to generate a partitioned
mesh with 200 million tetrahedrons on a computer with average memory size
(2Go RAM) in about 50 hours on one CPU (Xeon, 2008).
\n New Tepal_V2.0 gives the possibility to generate a partitioned mesh with (for the moment) no more than 100 million 
tetrahedrons on computers using MPI, (Total 16 Go RAM) 
in about 900 seconds (!yes! : !seconds!) on 2 octo processors (Xeon, 2009).
The launch of this beta-version is described below.
\n This is a serious alternative to GHS3D, which requires a much less common
configuration with 64Go RAM to only try to make a partition of a mesh with
200 million tetrahedrons, no result guaranteed.
\n
\note The Plugin doesn't load in the Memory the supposedly large resulting meshes. 
The meshes are saved in MED files and can be imported in the user-defined location via menu File-Import-MED Files.
\n Pay attention, that Salome GUI needs 2Go RAM to load a MED
file with 5 million tetrahedrons.

\image html ghs3dprl_parameters_basic.png

<ul>
<li>
<b>Name</b> - allows to define the name of the hypothesis (GHS3DPRL Parameters by default).
</li>
<li>
<b>MED_Name</b> - allows to define the path and the prefix of the 
resulting MED files ("DOMAIN" by default). 
If the path is not defined, the environment variable $SALOME_TMP_DIR
is used. If $SALOME_TMP_DIR is not defined as well, the environment
variable $TMP is used.
</li>
<li>
<b>Nb_Part</b> - allows to define the number of generated MED files.
The initial skin (triangles) will be meshed (tetrahedrons) and partitioned 
in Nb_Part by the elementary algorithm implemented in Tepal.<br>
</li>
<li>
<b>Keep_Files</b> - if this box is checked, input files of Tepal 
(GHS3DPRL.points and GHS3DPRL.faces) are not deleted after use (...if the
background mode was not used).
</li>
<li>
<b>Tepal_in_Background</b> - if this box is checked, Tepal execution
and MED file generation are launched in background mode and the user
can even exit Salome. Pay attention that in this case Tepal algorithm works
independently of "killSalome.py", and sometimes on another host.
</li>
<li>
<b>To_Mesh_Holes</b> - if this box is checked, the parameter component 
of tetmesh-ghs3d will mesh holes.
</li>

<h1>Modifying GHS3DPRL Advanced Parameters</h1><br>
GHS3DPRL Plugin launches a standalone binary executable tepal2med.<br>
tepal2med launches tepal, waits for the end of computation, and
converts the resulting output tepal files into MED files.<br>
Some advanced optional parameters are accessible as arguments.<br>

If keep_files option is checked, it is possible to re-launch tepal2med
or tepal in the Terminal as a command with custom parameters.<br>

<li>
<b>Advanced tepal2med Parameters</b> - type "tepal2med --help" in the Terminal. <p>

\verbatim
myname@myhost > /export/home/myname/salome_5/GHS3DPRLPLUGIN_5/bin/salome/tepal2med --help
Available options:
   --help         : produces this help message
   --casename     : path and name of input tepal2med files which are
                       - output files of tepal .msg .noboite .faces .points .glo
                       - output file of GHS3DPRL_Plugin casename_skin.med (optional)
                         with initial skin and its initial groups
   --number       : number of partitions
   --medname      : path and name of output MED files
   --limitswap    : max size of working cpu memory (Mo) (before swapping on .temp files)
   --verbose      : trace of execution (0->6)
   --test         : more tests about joints, before generation of output files
   --menu         : a GUI menu for option number
   --launchtepal  : also launch tepal on files casename.faces and casename.points and option number
   --meshholes    : force parameter component of tetmesh-ghs3d to mesh holes
   --background   : force background mode from launch tepal and generation of final MED files (big meshes)
   --deletegroups : regular expression (see QRegExp) which matches unwanted groups in final MED files
                    (try --deletegroups="(\bAll_Nodes|\bAll_Faces)"
                    (try --deletegroups="((\bAll_|\bNew_)(N|F|T))"
example:
   tepal2med --casename=/tmp/GHS3DPRL --number=2 --medname=DOMAIN --limitswap=1000 
             --verbose=0 --test=yes --menu=no --launchtepal=no

\endverbatim
\n
</li>
<li>
<b>Advanced Tepal_V1.4 Parameters</b> <p>

\verbatim

USAGE : tepal options

With options :
     --filename name (-f name) :
          Prefix of the input case (MANDATORY)

     --ndom n (-n n) :
          Number of subdomains to make (MANDATORY)

     --ghs3d ghs3d options (-g ghs3d options) :
          Runs temesh ghs3d on a previously generated subdomain. (ghs3d options must be "quoted")

     --memory m (-m m) :
          Max amount of memory (megabytes) allowed for ghs in the cutting process. (default is 0 : unlimited)

     --mesh_only  (-Z ) :
          Only (re)meshes all subdomains and updates communications messages

     --mesh_call command (-c command) :
          Calls the user specified command for meshing all the
          subomains after their skin has been generated

     --stats_only  (-S ) :
          Only computes and shows some statistics on subdomains

     --rebuild  (-r ) :
          Merges final subdomains skins

     --rebuild_tetra  (-R ) :
          Merges final subdomains skins and tetraedra

     --rebuild_iface  (-i ) :
          Includes interfaces in final subdomains merge

     --rebuild_retag  (-t ) :
          Tags vertices, faces (and tetra if selected) with their
          subdomain number in the final merge of subdomains (keeps the lowest tag for shared elements)

     --rebuild_ensight_parts  (-e ) :
          Builds ensight geom file with parts

     --tetmesh_args str (-G str) :
          Arguments to pass to Tetmesh during cutting process

\endverbatim
\n
</li>
<li>
<b>Advanced ghs3d Parameters (through Tepal_V1.4's --tetmesh_args)</b> - type "ghs3d -h" in a Terminal. <p>

\verbatim
myname@myhost > ghs3d -h

USE
    /export/home/myname/ghs3d-4.0/DISTENE/Tools/TetMesh-GHS3D4.0/bin/Linux/ghs3dV4.0
    [-u] [-m memory>] [-M MEMORY] [-f prefix] [-v verbose]
    [-c component] [-p0] [-C] [-E count] [-t] [-o level]
    [-I filetype] [-a/-b] [-O n m]

DESCRIPTION

 -u (-h)        : prints this message.

 -m memory      : launches the software with memory in Megabytes.
                  The default value of this parameter is 64 Megabytes and its
                  minimum value is 10 Megabytes.
                  It is also possible to set this parameter with the
                  environment variable GHS3D_MEMORY by means of an operation
                  equivalent to:
                           setenv GHS3D_MEMORY memory,
                  the value specified in the command line has the priority on
                  the environment variable.

 -M MEMORY      : provides the automatic memory adjustment feature.
                  If MEMORY (in Megabytes) is equal to zero, the size of the work space is
                  calculated from the input. If MEMORY is not equal to
                  zero, the software starts with MEMORY amount of work space.
                  The software reallocates memory as necessary.
                  The start value of  MEMORY can range from 0 to 64 Megabytes,
                  the maximum depends on -m option and the actual memory available.

 -f prefix      : defines the generic prefix of the files.

 -v verbose     : sets the output level parameter (the verbose parameter
                  must be in the range 0 to 10).

 -c component   : chooses the meshed component. If the parameter is
                      0, all components will be meshed, if
                      1, only the main (outermost) component will be meshed

 -p0            : disables creation of internal points.

 -C             : uses an alternative boundary recovery mechanism. It should be used only
                  when the standard boundary recovery fails.

 -E count       : sets the extended output for error messages. If -E is used,
                  the error messages will be printed, it is possible
                  to indicate the maximum number of printed messages between 1 and 100.

 -t             : generates an error file prefix.Log

 -o level       : sets the required optimisation level.
                  Valid optimisation levels are:
                  none, light, standard or strong,
                  with increase of "quality vs speed" ratio.

 -I filetype    : defines the input mesh format as follows:
                    -IP input files are ascii files, named prefix.points
                     and prefix.faces - this is the default type of files
                    -IPb input files are binary files, named prefix.pointsb
                     and prefix.facesb
                    -IM input file is ascii file, named prefix.mesh
                  where prefix is defined with -f option

 -a/-b          : selects the output file type:
                    -a for ascii (the default) and
                    -b for binary.

 -On            : saves a NOPO file in addition. NOPO is the mesh data
                  structure of the Simail and Modulef software packages.
 -Om            : saves a mesh file in addition.
 -Omn           : saves both NOPO and mesh files.

 ==============================================================================
                   TETMESH-GHS3D SOFTWARE 4.0-3 (December, 2006)
                                 END OF SESSION
                COPYRIGHT (C)1989-2006 INRIA ALL RIGHTS RESERVED
 ==============================================================================
      ( Distene SAS
        Phone: +33(0)1-69-26-62-10   Fax: +33(0)1-69-26-90-33
        EMail: support@distene.com )

\endverbatim
\n
</li>
<h1>Saving user's preferred GHS3DPRL Advanced Parameters</h1><br>
GHS3DPRL Plugin launches standalone binary executable tepal2med.<br>
You may rename file tepal2med as tepal2med.exe for example, and replace
tepal2med by a shell script at your convenience to overriding parameters.
<br>... or else $PATH modification... .<br>Idem for file tepal.<br><br>
<li>
<b>Advanced tepal2med Parameters</b> - overriding parameter deletegroups<p>
You may rename tepal2med as tepal2med.exe for example.

\code
#!/bin/bash
#script tepal2med overriding parameter deletegroups
#we have renamed binary executable tepal2med as tepal2med.exe
#echo tepal2med initial parameters are $1 $2 $3 $4 ... or $*
#$0 is ignored

tepal2med.exe $* --deletegroups="(\bAll_Nodes|\bAll_Faces)"

\endcode
\n
</li>
<li>
<b>Advanced Tepal_V1.4 Parameters</b> - overriding parameter component of ghs3d (to mesh holes). <p>
You may rename tepal as tepal.exe for example.

\code
#!/bin/bash
#script tepal overriding parameter component of tetmesh-ghs3d
#we have renamed binary executable tepal as tepal.exe

#optionnaly we could set licence only for us
DISTENE_LICENSE_FILE="Use global envvar: DLIM8VAR"
DLIM8VAR="dlim8 1:1:29030@is142356/0016175ef08c::a1ba1...etc...e19"
SIMULOGD_LICENSE_FILE=29029@is142356

tepal.exe $* --tetmesh_args "-c 0"

\endcode
\n
</li>
<li>
<b>Advanced tepal Parameters</b> - overriding launching tepal on other host. <p>
You may rename tepal as tepal.exe for example.

\code
#!/bin/bash
#script tepal overriding launching tepal on other host (tepal run 64 bits only)
#we have renamed binary executable tepal as tepal.exe
#common file system (same path) otherwise scp... on input or result files
#ssh -keygen -t rsa done and files id_rsa et id-rsa.pub move in ~/.ssh

#example of typical command
#tepal -f /home/myname/tmp/GHS3DPRL -n 4 > /home/myname/tmp/tepal.log
#echo parameters $1 $2 $3 $4 ... or $*

#tepal licence ought to be known on otherhost
ssh otherhost "tepal.exe $* > /home/myname/tmp/tepal.log"

#or more and more
#ssh otherhost "tepal.exe $* --tetmesh_args \"-c 0\"" > /home/myname/tmp/tepal.log

\endcode
\n
</li>

<h1>Tepal_V2.0 and MPI use.</h1><br>
This all new beta-version needs MPI, (openmpi-1.3.1 was used). To use it you have to proceed 
as done in "overriding parameter component of ghs3d".
Advanced ghs3d Parameters (through Tepal_V1.4's --tetmesh_args) are not assumed yet.
It meshes holes.
\n You may rename tepal as tepal64_v2.exe for example, and replace tepal by a shell script like below.

<li>
<b>example tepal_v2_mpirun.</b><p>

\code

#!/bin/bash
#script tepal overriding launching Tepal_V2.0 with MPI (tepal run 64 bits only).
#we have renamed binary executable tepal as tepal64_v2.exe.
#typical command to launch tepal v1 :
#tepal -f /tmp/myname/GHS3DPRL -n 16 > /tmp/myname/tepal.log
#this file is an exemple to transform this call for tepal v2.0, 
#   (beta version using .mesh input file)
#you have to adapt for your convenience.

#first problem  is convert v1 input files GHS3DPRL.faces and GHS3DPRL.points 
#               to v2 input file GHS3DPRL.mesh.
#second problem is to launch on heterogeneous system linux cluster of 
#               2 hosts (64 bits) of 8 nodes (by example)
#               with different 2 executables codes linked on 2 different
#               openmpi shared library codes.
#third problem  is convert tepal v2 output files GHS3DPRL*.mesh
#               to v1 input files GHS3DPRL*.faces an GHS3DPRL*.points.

#you have to work on the same physical disk and same path input and ouput files : $SAME_DIR
#you have to work on different physical disk but same path and name for executable files 
#    (and shared libraries) : $DIFF_DIR

echo "parameter 0="$0
echo "parameter 1="$1
echo "parameter 2="$2
echo "parameter 3="$3
echo "parameter 4="$4

export SAME_DIR=/same_physical_disk_and_same path/tmp
export DIFF_DIR=/different_physical_disk_but_same path/myname

#copy input local files from local current directory (something like /tmp/myname)
#in this case we need /tmp/myname and $SAME_DIR different
cd $SAME_DIR
rm *
cp $2* .

export IN_FILES=`basename $2`
export IN_DIR=`dirname $2`
#created .mesh from .faces et .points
/through_salome_path/facespoints2mesh.py $IN_FILES

#there are 2 executable openmpi and library through 2 physical DIFF_DIR
export PATH=$DIFF_DIR/openmpi-1.3.1_install/bin:${PATH}
export LD_LIBRARY_PATH=$DIFF_DIR/openmpi-1.3.1_install/lib:${LD_LIBRARY_PATH}

#there are 2 executables tepal_v2 through 2 physical DIFF_DIR
export LD_LIBRARY_PATH=$DIFF_DIR/tepal-2.0.0/bin/Linux_64:${LD_LIBRARY_PATH}
export PATH=$DIFF_DIR/tepal-2.0.0/bin/Linux_64:$PATH

#small test betweeen friends
#rm hostnames.log
#mpirun -n $4 hostname >> hostnames.log

#there necessary set env licence file for tepal v2
export DISTENE_LICENSE_FILE="Use global envvar: DLIM8VAR"
export DLIM8VAR="dlim8 1:1:29030@is142356/0016175ef08c::a1ba...9e19"
export SIMULOGD_LICENSE_FILE=29029@is142356 
export LICENSE_FILE=/product/distene/dlim8.var.sh

#mpirun with necessary set envenvironment
export TMP_ENV="-x PATH -x LD_LIBRARY_PATH -x DISTENE_LICENSE_FILE -x DLIM8VAR \
                -x SIMULOGD_LICENSE_FILE -x LICENSE_FILE"
#mpirun $TMPENV -n $4 which tepal64_v2.exe >> hostnames.log

#real mpirun uncomment after verify small test
mpirun $TMPENV -n $4 tepal64_v2.exe --in $IN_FILES.mesh --out $IN_FILES.mesh --verbose 100

#convert output files tepalv1 format
/through_salome_path/mesh2facespoints.py $IN_FILES

#copy ouputs files from $SAME_DIR to local current directory (something like /tmp/myname)
cp -f hostnames.log $IN_DIR
cp -f $IN_FILES* $IN_DIR

#ls -al $SAME_DIR
#cat $SAME_DIR/hostnames.log
#cat /tmp/myname/tepal.log

\endcode
\n
</li>

<h1>TUI use.</h1><br>

<li>
<b>example ex30_tepal.py.</b><p>

\code

#!/bin/python
import os

import geompy
import smesh

# Parameters
# ----------

results = "/tmp/ZZ"

radius =  50
height = 200

# Build a cylinder
# ----------------

base = geompy.MakeVertex(0, 0, 0)
direction = geompy.MakeVectorDXDYDZ(0, 0, 1)

cylinder = geompy.MakeCylinder(base, direction, radius, height)

geompy.addToStudy(cylinder, "Cylinder")

# Define a mesh on a geometry
# ---------------------------

m = smesh.Mesh(cylinder)

# 2D mesh with BLSURF
# -------------------

algo2d = m.Triangle(smesh.BLSURF)

algo2d.SetPhysicalMesh(1)
algo2d.SetPhySize(5)

algo2d.SetGeometricMesh(0)

# 3D mesh with tepal
# ------------------

algo3d = m.Tetrahedron(smesh.GHS3DPRL)

algo3d.SetMEDName(results)
algo3d.SetNbPart(4)
algo3d.SetBackground(False)
algo3d.SetKeepFiles(False)
algo3d.SetToMeshHoles(True)

# Launch meshers
# --------------

status = m.Compute()

# Test if ok
# ----------

if os.access(results+".xml", os.F_OK):
    print "Ok: tepal"
else:
    print "KO: tepal"
\endcode
\n
</li>
</ul>


*/
