
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 Documentation of the programming interface (API)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

This section describes the python packages and modules of the
``salome.smesh`` python package. The main part is generated from the
code documentation included in source python files.

:mod:`salome.smesh` -- Package containing the SMESH python utilities
====================================================================

:mod:`smeshstudytools` -- Tools to access SMESH objects in the study
--------------------------------------------------------------------

.. automodule:: salome.smesh.smeshstudytools
   :members:
