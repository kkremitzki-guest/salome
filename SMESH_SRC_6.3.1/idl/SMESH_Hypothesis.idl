// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  File   : SMESH_Hypothesis.idl
//  Author : Paul RASCLE, EDF
//  $Header: /home/server/cvs/SMESH/SMESH_SRC/idl/SMESH_Hypothesis.idl,v 1.10.20.1.8.1 2011-06-02 05:57:20 vsr Exp $
//
#ifndef _SMESH_HYPOTHESIS_IDL_
#define _SMESH_HYPOTHESIS_IDL_

#include "SALOME_Exception.idl"
#include "SALOME_GenericObj.idl"

module SMESH
{
  enum Dimension
  {
    DIM_0D,
    DIM_1D,
    DIM_2D,
    DIM_3D
  };
  
  typedef sequence<string> ListOfParameters;

  interface SMESH_Hypothesis : SALOME::GenericObj
  {
    
    /*!
     * Get the Hypothesis typeName 
     */
    string GetName();

    /*!
     * Get the Hypothesis plugin library Name 
     */
    string GetLibName();

    /*!
     * Get the internal Id 
     */
    long GetId();

    /*!
     *  Set list of parameters
     *  \param theParameters is a string containing the notebook variables separated by ":" symbol,
     *         used for Hypothesis creation
     */
    void SetParameters (in string theParameters);

    /*!
     *  Return list of notebook variables used for Hypothesis creation separated by ":" symbol
     */
    string GetParameters();

    /*!
     *  Return list of last notebook variables used for Hypothesis creation.
     */
    ListOfParameters GetLastParameters();

    /*!
     *  Set list of parameters
     *  \param theParameters is a string containing the last notebook variables separated by ":" symbol,
     *         used for Hypothesis creation
     */
    void SetLastParameters(in string theParameters);
    
    /*!
     * Clear parameters list
     */
    void ClearParameters();

    /*!
     * Verify whether hypothesis supports given entity type 
     */
    boolean IsDimSupported( in Dimension type );
  };

  typedef sequence<string> ListOfHypothesisName;

  interface SMESH_Algo : SMESH_Hypothesis
  {
    /*!
     * Get list of hypothesis that can be used with this algorithm
     */
    ListOfHypothesisName GetCompatibleHypothesis();

  };

  interface SMESH_0D_Algo : SMESH_Algo
  {
    /*!
     * 
     */
  };

  interface SMESH_1D_Algo : SMESH_Algo
  {
    /*!
     * 
     */
  };

  interface SMESH_2D_Algo : SMESH_Algo
  {
    /*!
     * 
     */
  };

  interface SMESH_3D_Algo : SMESH_Algo
  {
    /*!
     * 
     */
  };
};

  // -----------------------------------------------------------------
  // Specific Algorithms in separate idl file
  // -----------------------------------------------------------------


#endif
