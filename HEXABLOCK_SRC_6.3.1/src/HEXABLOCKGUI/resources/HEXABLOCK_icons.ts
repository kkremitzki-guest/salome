<!DOCTYPE TS>
<!--
  Copyright (C) 2007-2008  CEA/DEN, EDF R&D, OPEN CASCADE

  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com

-->
<TS version="1.1" >
    <context>
        <name>@default</name>
        <message>
            <source>ICON_NEW_DOCUMENT</source>
            <translation>new_document.png</translation>
        </message>
        <message>
            <source>ICON_IMPORT_DOCUMENT</source>
            <translation>import_document.png</translation>
        </message>
        <message>
            <source>ICON_ADD_VERTEX</source>
            <translation>add_vertex.png</translation>
        </message>
        <message>
            <source>ICON_ADD_EDGE</source>
            <translation>add_edge.png</translation>
        </message>
        <message>
            <source>ICON_ADD_QUAD</source>
            <translation>add_quad.png</translation>
        </message>
        <message>
            <source>ICON_ADD_HEXA</source>
            <translation>add_hexa.png</translation>
        </message>
        <message>
            <source>ICON_ADD_VECTOR</source>
            <translation>add_vector.png</translation>
        </message>
        <message>
            <source>ICON_ADD_CYLINDER</source>
            <translation>add_cylinder.png</translation>
        </message>
        <message>
            <source>ICON_ADD_PIPE</source>
            <translation>add_pipe.png</translation>
        </message>
        <message>
            <source>ICON_MAKE_GRID</source>
            <translation>make_grid.png</translation>
        </message>
        <message>
            <source>ICON_MAKE_CYLINDER</source>
            <translation>make_cylinder.png</translation>
        </message>
        <message>
            <source>ICON_MAKE_PIPE</source>
            <translation>make_pipe.png</translation>
        </message>
        <message>
            <source>ICON_MAKE_CYLINDERS</source>
            <translation>make_cylinders.png</translation>
        </message>
        <message>
            <source>ICON_MAKE_PIPES</source>
            <translation>make_pipes.png</translation>
        </message>
        <message>
            <source>ICON_REMOVE_HEXA</source>
            <translation>remove_hexa.png</translation>
        </message>
        <message>
            <source>ICON_PRISM_QUAD</source>
            <translation>prism_quad.png</translation>
        </message>
        <message>
            <source>ICON_JOIN_QUAD</source>
            <translation>join_quad.png</translation>
        </message>
        <message>
            <source>ICON_MERGE</source>
            <translation>merge.png</translation>
        </message>
        <message>
            <source>ICON_DISCONNECT</source>
            <translation>disconnect.png</translation>
        </message>
        <message>
            <source>ICON_CUT_EDGE</source>
            <translation>cut_edge.png</translation>
        </message>
        <message>
            <source>ICON_MAKE_TRANSFORMATION</source>
            <translation>make_transformation.png</translation>
        </message>
        <message>
            <source>ICON_MAKE_SYMMETRY</source>
            <translation>make_symmetry.png</translation>
        </message>
        <message>
            <source>ICON_PERFORM_TRANSFORMATION</source>
            <translation>perform_transformation.png</translation>
        </message>
        <message>
            <source>ICON_PERFORM_SYMMETRY</source>
            <translation>perform_symmetry.png</translation>
        </message>
        <message>
            <source>ICON_ADD_GROUP</source>
            <translation>add_group.png</translation>
        </message>
        <message>
            <source>ICON_REMOVE_GROUP</source>
            <translation>remove_group.png</translation>
        </message>
        <message>
            <source>ICON_ADD_LAW</source>
            <translation>add_law.png</translation>
        </message>
        <message>
            <source>ICON_REMOVE_LAW</source>
            <translation>remove_law.png</translation>
        </message>
        <message>
            <source>ICON_SET_PROPAGATION</source>
            <translation>set_propagation.png</translation>
        </message>
    </context>
</TS>
