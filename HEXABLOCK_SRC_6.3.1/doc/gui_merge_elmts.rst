:tocdepth: 3

.. _guimergeelements:

============== 
Merge elements
==============

To merge elements in the **Main Menu** select **Model -> Merge**.

.. _guimergequads:

Merge 2 quadrangles
===================

**Arguments:**

- 2 quadrangles (q1 and q2)
- 4 vertices (v1, v2, v3, v4)


The dialogue box to merge 2 quadrangles is:


.. image:: _static/gui_merge_quads.png
   :align: center

.. centered::
   Merge 2 Quadrangles

.. _guimergeedges:

Merge 2 edges
=============

**Arguments:**

- 2 edges (e1 and e2)
- 2 vertices (v1 and v2)

The dialogue box to merge 2 edges is:

.. image:: _static/gui_merge_edges.png
   :align: center

.. centered::
   Merge 2 Edges

.. _guimergevertices:

Merge 2 vertices
================

**Arguments:** 2 vertices (v1 and v2)


The dialogue box to merge 2 vertices is:


.. image:: _static/gui_merge_vertices.png
   :align: center

.. centered::
   Merge 2 Vertices


TUI command: :ref:`tuimergeelements`
