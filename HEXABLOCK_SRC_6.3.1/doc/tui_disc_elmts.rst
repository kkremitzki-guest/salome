:tocdepth: 3

.. _tuidisconnectelements:

===================
Disconnect elements
===================

Disconnect a quadrangle::

 	elts = doc.disconnectQuad(hexa, qua)

Disconnect an edge::

	 elts = doc.disconnectEdge(hexa, edg)
 
Disconnect a vertex::

 	elts = doc.disconnectVertex(hexa, ver)

**todo exemple (cf. test_disconnect())**

GUI command: :ref:`guidisconnectelements`
