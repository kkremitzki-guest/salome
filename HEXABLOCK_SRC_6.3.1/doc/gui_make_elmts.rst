:tocdepth: 3

.. _guimakeelements:

======================================
Make elements by transforming elements
======================================

To make elements by transforming elements in the **Main Menu** select
**Model -> Make transformation**.

.. _guimaketranslation:

Make elements by translation
============================

**Arguments:**

- elements
- vector *todo kesako ?*


The dialogue box to make elements by translation is:

.. image:: _static/gui_make_transfo_translation.png
   :align: center

.. centered::
   Make Elements by Translation

.. _guimakescaling:

Make elements by scaling
========================

**Arguments:**

- elements
- vertex *todo kesako ?*
- k *todo kesako ?*

The dialogue box to make elements by scaling is:

.. image:: _static/gui_make_transfo_scale.png
   :align: center

.. centered::
   Make Elements by Scaling

.. _guimakerotation:

Make elements by rotation
=========================

**Arguments:**

- elements
- vector *todo kesako ?*
- vertex *todo kesako ?*
- angle


The dialogue box to make elements by rotation is:


.. image:: _static/gui_make_transfo_rotation.png
   :align: center

.. centered::
   Make Elements by Rotation


TUI command: :ref:`tuimakeelements`
