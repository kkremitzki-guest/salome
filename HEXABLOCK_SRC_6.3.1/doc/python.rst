:tocdepth: 4

.. _python:

###############################################
Using Hexablock with the Python interface (TUI)
###############################################

By the links below you can find sample scripts for all operations provided by Hexablock module

.. toctree::
   :maxdepth: 2
 
   tui_component.rst
   tui_document.rst
   tui_vertex.rst
   tui_edge.rst
   tui_quadrangle.rst
   tui_hexahedron.rst
   tui_vector.rst
   tui_cyl.rst
   tui_pipe.rst
   tui_remove.rst
   tui_cartgrid.rst
   tui_cylgrid.rst
   tui_sphergrid.rst
   tui_blocks_for_cyl_pipe.rst
   tui_prism_join_quad.rst
   tui_merge_elmts.rst
   tui_disc_elmts.rst
   tui_cut_hexa.rst
   tui_make_elmts.rst
   tui_modify_elmts.rst
   tui_asso_quad_to_geom.rst
   tui_groups.rst
   tui_discret_law.rst
   tui_propag.rst
   tui_mesh.rst











