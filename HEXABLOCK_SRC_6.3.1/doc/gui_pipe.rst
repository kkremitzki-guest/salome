:tocdepth: 3


.. _guipipe:

====
Pipe
====

To create a **Pipe** in the **Main Menu** select **Model -> Add pipe** 

**Arguments:** 1 vertex + 1 vector + internal radius + external
  radius + height (h)

The dialogue box for the creation of a pipe is:

.. image:: _static/gui_pipe.png
   :align: center

.. centered::
   Create a Pipe

TUI command: :ref:`tuipipe`
