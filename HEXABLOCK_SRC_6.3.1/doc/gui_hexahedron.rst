:tocdepth: 3

.. _guihexahedron:

==========
Hexahedron
==========

To create an **Hexaedron** in the **Main Menu** select **Model -> Add hexa** 

There are 2 algorithms to create an **Hexaedron**.

Firstly you can define an **Hexaedron** through **Quad 1**, **Quad
2**, **Quad 3**, **Quad 4**, **Quad 5**, and **Quad 6**.

**Arguments:** 6 quadrangles

The dialogue box for the creation of an hexahedron from quads is:

.. image:: _static/gui_hexa_quads.png
   :align: center

.. centered::
   Create an Hexahedron from quads

Secondly you can define an **Hexaedron** through **Vertex 1**, **Vertex
2**, **Vertex 3**, **Vertex 4**, **Vertex 5**, **Vertex 6**, **Vertex
7**, and **Vertex 8**, which ares the points through wich the **Hexaedron** passes.

**Arguments:** 8 vertices

The dialogue box for the creation of an hexahedron from vertices is:

.. image:: _static/gui_hexa_vertices.png
   :align: center

.. centered::
   Create an Hexahedron from vertices


TUI command: :ref:`tuihexahedron`
