:tocdepth: 3


.. _tuiremove:

=============
Remove blocks
=============

Remove one block of the model of blocks::

	 doc.removeHexa(hexa)

Remove all blocks connected of the model of blocks::

	 doc.removeConnectedHexa(hexa)



.. image:: _static/dialogbox_remove.PNG
   :align: center

.. centered::
   legende

.. image:: _static/remove3.PNG
   :align: center


GUI command: :ref:`guiremove`

