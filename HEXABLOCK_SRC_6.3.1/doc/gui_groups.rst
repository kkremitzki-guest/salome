:tocdepth: 3

.. _guigroups:

==========================
Manage groups on the model
==========================

.. _guiaddgroup:

Add group
=========

A group is characterized by:

- its type
- its name
- its elements

To define groups on the model in the **Main Menu** select **Groups ->
Add group**.

.. image:: _static/gui_add_group.png
   :align: center

.. centered::
   Add group

Remove group
============

To remove group of the model in the **Main Menu** select **Groups ->
Remove group**.

*todo copie d'ecran*

.. image:: _static/gui_remove_group.png
   :align: center

.. centered::
   Remove group


TUI command: :ref:`tuigroups`
