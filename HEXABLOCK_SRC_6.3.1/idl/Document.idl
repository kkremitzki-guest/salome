//  Copyright (C) 2009-2011  CEA/DEN, EDF R&D
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public
//  License as published by the Free Software Foundation; either
//  version 2.1 of the License.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
//  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
#ifndef __Document_idl__
#define __Document_idl__
/*!
 \defgroup EXAMPLES SALOME EXAMPLES components
 */
#include "SALOME_Exception.idl"
#include "SALOME_GenericObj.idl"

#include "GEOM_Gen.idl"

#include "Edge.idl"

/*!  \ingroup EXAMPLES

This package contains the interface HEXABLOCK_ORB used 
for  %HEXABLOCK component as an example in %SALOME application.
*/
module HEXABLOCK_ORB
{

  interface Element;
  interface Vertex;
  interface Edge;
  interface Quad;
  interface Hexa;
  interface Vector;
  interface Cylinder;
  interface Pipe;
  interface Elements;
  interface CrossElements;
  interface Law;
  interface Group;
  interface Propagation;

  typedef sequence<Quad>  Quads;
  typedef GEOM::GEOM_Object  Shape;
  typedef sequence<GEOM::GEOM_Object>  Shapes;

  interface Document : SALOME::GenericObj
  {
    void purge()
      raises (SALOME::SALOME_Exception);

    void dump()
      raises (SALOME::SALOME_Exception);
    /*!
    */
    string getFile()
      raises (SALOME::SALOME_Exception);

    void setFile( in string fileName )
      raises (SALOME::SALOME_Exception);

    boolean isSavedFile()
      raises (SALOME::SALOME_Exception);

//     void saveFile() 
    long saveFile() //CS_NOT_SPEC
      raises (SALOME::SALOME_Exception);

    long saveVtk( in string fname );//CS_NOT_SPEC

    /*!
    */
    void setTolerance( in double tol ) raises (SALOME::SALOME_Exception);
    double getTolerance() raises (SALOME::SALOME_Exception);

    /*!
        Sommets
    */
    Vertex addVertex( in double x, in double y, in double z )
        raises (SALOME::SALOME_Exception);

    long countVertex()
        raises (SALOME::SALOME_Exception);

    Vertex getVertex( in long i )
        raises (SALOME::SALOME_Exception);

    Vertex findVertex( in double x, in double y, in double z )
        raises (SALOME::SALOME_Exception);


    /*!
        ArÃªtes
    */
    Edge addEdge( in Vertex v0, in Vertex v1 )
        raises (SALOME::SALOME_Exception);

    long countEdge()
        raises (SALOME::SALOME_Exception);

    Edge getEdge( in long i )
        raises (SALOME::SALOME_Exception);

    Edge findEdge( in Vertex p1, in Vertex p2 )
        raises (SALOME::SALOME_Exception);


    /*!
        Quadrangles
    */
    Quad addQuad( in Edge e0, in Edge e1, in Edge e2, in Edge e3 )
        raises (SALOME::SALOME_Exception);

    Quad addQuadVertices( in Vertex v0, in Vertex v1, in Vertex v2, in Vertex v3 )
        raises (SALOME::SALOME_Exception);

    long countQuad()
        raises (SALOME::SALOME_Exception);

    Quad getQuad( in long i )
        raises (SALOME::SALOME_Exception);

    Quad findQuad( in Vertex v1, in Vertex v2 )
        raises (SALOME::SALOME_Exception);



    /*!
        HexaÃ¨dre
    */
    Hexa addHexa( in Quad q0, in Quad q1, in Quad q2, in Quad q3, in Quad q4, in Quad q5 )
        raises (SALOME::SALOME_Exception);

    Hexa addHexaVertices( in Vertex v1, in Vertex v2, in Vertex v3, in Vertex v4,
                  in Vertex v5, in Vertex v6, in Vertex v7, in Vertex v8 )
        raises (SALOME::SALOME_Exception);

    long countHexa()
        raises (SALOME::SALOME_Exception);

    Hexa getHexa( in long i )
        raises (SALOME::SALOME_Exception);

    Hexa findHexa( in Vertex p1, in Vertex p2 )
        raises (SALOME::SALOME_Exception);



    /*!
        Vecteurs
    */
    Vector addVector( in double dx, in double dy, in double dz )
        raises (SALOME::SALOME_Exception);

    Vector addVectorVertices( in Vertex v1, in Vertex v2 )
        raises (SALOME::SALOME_Exception);

//     long countVector()
//         raises (SALOME::SALOME_Exception);
// 
//     Vector getVector( in long i )
//         raises (SALOME::SALOME_Exception);


    /*!
        Cylindre
    */
    Cylinder addCylinder( in Vertex base, in Vector direction, in double radius, in double height )
        raises (SALOME::SALOME_Exception);

//     long countCylinder()
//         raises (SALOME::SALOME_Exception);
// 
//     Cylinder getCylinder( in long i )
//         raises (SALOME::SALOME_Exception);


    /*!
        Pipe
    */
    Pipe addPipe( in Vertex base, in Vector direction,
                  in double int_radius, in double ext_radius, 
                  in double height )
        raises (SALOME::SALOME_Exception);

//     long countPipe()
//         raises (SALOME::SALOME_Exception);
// 
//     Pipe getPipe( in long i )
//         raises (SALOME::SALOME_Exception);


    /*!
        Remove block
    */
    boolean removeHexa( in Hexa h )
        raises (SALOME::SALOME_Exception);

    boolean removeConnectedHexa( in Hexa h )
        raises (SALOME::SALOME_Exception);

   /*!
       Grille cartÃ©siennes
   */
    Elements makeCartesian( in Vertex pt, 
            in Vector vx, in Vector vy, in Vector vz,
            in long nx, in long ny, in long nz)
        raises (SALOME::SALOME_Exception);

    Elements makeCartesian1( in Vertex v, 
            in Vector v1,
            in long px, in long py, in long pz,
            in long mx, in long my, in long mz )
        raises (SALOME::SALOME_Exception);

    /*!
        Grille cylindrique
    */
    Elements makeCylindrical( in Vertex pt,
          in Vector vex, in Vector vez,
          in double dr, in double da, in double dl,
          in long nr, in long na, in long nl,
          in boolean fill )
        raises (SALOME::SALOME_Exception);

    /*!
        Grille sphÃ©rique
    */
//     Elements makeSpherical( in Vertex pt,
//           in double dx,
//           in double dy,
//           in double dz,
//           in long n )
//         raises (SALOME::SALOME_Exception); //CS_TODO

    Elements makeSpherical( in Vertex pt,
        in Vector dv,
        in long n,
        in double k )
        raises (SALOME::SALOME_Exception); //CS_TO_DEL


    /*!
        Cylindre dÃ©coupÃ© en blocs
    */
//     Elements makeCylinder( in Cylinder cyl, in long nr, in long na, in long nl )
//         raises (SALOME::SALOME_Exception);//CS_TODO
    Elements makeCylinder( in Cylinder cyl, in Vector vr, in long nr, in long na, in long nl )
        raises (SALOME::SALOME_Exception);//CS_NEW CS_NOT_SPEC
   

    /*!
        Tuyau decoupe en blocs
    */
    Elements makePipe( in Pipe p, in Vector v, in long nr, in long na, in long nl )
        raises (SALOME::SALOME_Exception);

    /*!
        2 Cylindres en T dÃ©coupÃ©s en blocs
    */
    CrossElements makeCylinders( in Cylinder c1, in Cylinder c2 )
      raises (SALOME::SALOME_Exception);//CS_NEW CS_NOT_SPEC

    /*!
        2 Tuyau decoupe en intersection decoupees en blocs
    */
    Elements makePipes( in Pipe p1, in Pipe p2 )
        raises (SALOME::SALOME_Exception);

    /*!
        Prismer des quadrangles
    */
    Elements prismQuad( in Quad qd, in Vector v, in long nb )
        raises (SALOME::SALOME_Exception);

    Elements prismQuads( in Quads qds, in Vector v, in long nb )
        raises (SALOME::SALOME_Exception); //CS_NEW

    Elements joinQuad( in Quad qa, in Quad qb,
                       in Vertex va1, in Vertex vb1,
                       in Vertex va2, in Vertex vb2,
                       in long nb )
        raises (SALOME::SALOME_Exception);

    Elements joinQuads( in Quads qds,
                        in Quad qb,
                        in Vertex va1, in Vertex vb1,
                        in Vertex va2, in Vertex vb2,
                        in long nb )
        raises (SALOME::SALOME_Exception);
// 
// 
//     /*!
//         Fusionner 2 Ã©lÃ©ments de mÃªme nature //CS_NOT_SPEC
//     */

//     Elements mergeQuads( in Quad qa, in Quad qb,
//                         in Vertex va1, in Vertex vb1,
//                         in Vertex va2, in Vertex vb2 )
//         raises (SALOME::SALOME_Exception);

    long mergeQuads( in Quad qa, in Quad qb,
                        in Vertex va1, in Vertex vb1,
                        in Vertex va2, in Vertex vb2 )
        raises (SALOME::SALOME_Exception);//CS_NOT_SPEC //CS_NEW

// 
//     Elements mergeEdges( in Edge e1, in Edge e2,
//                         in Vertex v1, in Vertex v2 )
//         raises (SALOME::SALOME_Exception);

    long mergeEdges( in Edge e1, in Edge e2,
                        in Vertex v1, in Vertex v2 )
        raises (SALOME::SALOME_Exception);//CS_NOT_SPEC //CS_NEW

//     Elements mergeVertices( in Vertex v1, in Vertex v2 )
//         raises (SALOME::SALOME_Exception);
    long mergeVertices( in Vertex v1, in Vertex v2 )
        raises (SALOME::SALOME_Exception);//CS_NOT_SPEC //CS_NEW


    /*!
        DÃ©connecter des Ã©lÃ©ments du modÃ¨le
    */
    Elements disconnectQuad( in Hexa h, in Quad q )  //CS_NOT_SPEC
//     long disconnectQuad( in Hexa h, in Quad q )//CS_NOT_SPEC
//         raises (SALOME::SALOME_Exception);
//     Quad disconnectQuad( in Hexa h, in Quad q )//CS_NOT_SPEC
        raises (SALOME::SALOME_Exception);

    Elements disconnectEdge( in Hexa h, in Edge e )//CS_NOT_SPEC
	raises (SALOME::SALOME_Exception);
//     Elements disconnectEdge( in Hexa h, in Edge e )
//     long disconnectEdge( in Hexa h, in Edge e ) 
//     Edge disconnectEdge( in Hexa h, in Edge e ) //CS_NOT_SPEC
//         raises (SALOME::SALOME_Exception);

    Elements disconnectVertex( in Hexa h, in Vertex v )//CS_NOT_SPEC
//     long disconnectVertex( in Hexa h, in Vertex v )
//     Vertex disconnectVertex( in Hexa h, in Vertex v ) //CS_NOT_SPEC
        raises (SALOME::SALOME_Exception);


    /*!
        Couper des hexaÃ¨dres du modÃ¨le
    */
    Elements cut( in Edge e, in long nb_of_cuts )
        raises (SALOME::SALOME_Exception);

    /*!
        Make elements by transforming elements
    */
//     Elements makeTranslation( in Element e, in Vector vec )
//         raises (SALOME::SALOME_Exception);
    Elements makeTranslation( in Elements l, in Vector vec )
        raises (SALOME::SALOME_Exception);
// 
//     Elements makeScale( in Vertex e, in Vertex ver, in double k )
//         raises (SALOME::SALOME_Exception);
// 
    Elements makeRotation(in Elements l, in Vertex ver, in Vector vec, in double angle)
        raises (SALOME::SALOME_Exception);
// 
//     Elements makeSymmetryPoint(in Element e, in Vertex ver)
//         raises (SALOME::SALOME_Exception);
// 
//     Elements makeSymmetryLine(in Element e, in Vertex ver, in Vector vec)
//         raises (SALOME::SALOME_Exception);
// 
//     Elements makeSymmetryPlane(in Element e, in Vertex ver, in Vector vec)
//         raises (SALOME::SALOME_Exception);
// 
    /*!
    Modify elements by transforming elements
    */
//     void performTranslation(in Element e, in Vector vec)
//         raises (SALOME::SALOME_Exception);

    void performTranslation(in Elements l, in Vector vec)
        raises (SALOME::SALOME_Exception);
// 
//     void performScale(in Element e, in Vertex ver, in double k)
//         raises (SALOME::SALOME_Exception);
// 
    void performRotation(in Elements l, in Vertex ver, in Vector vec, in double angle)
        raises (SALOME::SALOME_Exception);
// 
//     void performSymmetryPoint(in Element e, in Vertex ver)
//         raises (SALOME::SALOME_Exception);
// 
//     void performSymmetryLine(in Element e, in Vertex ver, in Vector vec)
//         raises (SALOME::SALOME_Exception);
// 
//     void performSymmetryPlane(in Element e, in Vertex ver, in Vector vec)
//         raises (SALOME::SALOME_Exception);
// 
//     /*!
//                     --------
//                     MAILLAGE
//                     --------
//     */
// 
//     /*!
//         CrÃ©er, Ã©diter et supprimer un groupe
//     */
//     HexaGroup addHexaGroup( in string name )
    Group addHexaGroup( in string name )
        raises (SALOME::SALOME_Exception);

//     QuadGroup addQuadGroup( in string name )
    Group addQuadGroup( in string name )
        raises (SALOME::SALOME_Exception);

//     EdgeGroup addEdgeGroup( in string name )
    Group addEdgeGroup( in string name )
        raises (SALOME::SALOME_Exception);

//     HexaNodeGroup addHexaNodeGroup( in string name )
    Group addHexaNodeGroup( in string name )
        raises (SALOME::SALOME_Exception);

//     QuadNodeGroup addQuadNodeGroup( in string name )
    Group addQuadNodeGroup( in string name )
        raises (SALOME::SALOME_Exception);

//     EdgeNodeGroup addEdgeNodeGroup( in string name )
    Group addEdgeNodeGroup( in string name )
        raises (SALOME::SALOME_Exception);

//     VertexNodeGroup addVertexNodeGroup( in string name )
    Group addVertexNodeGroup( in string name )
        raises (SALOME::SALOME_Exception);

    long removeGroup( in Group g)
        raises (SALOME::SALOME_Exception);

    long countGroup()
      raises (SALOME::SALOME_Exception);

    Group getGroup( in long i )
      raises (SALOME::SALOME_Exception);

    Group findGroup( in string name )
      raises (SALOME::SALOME_Exception);



    /*!
        DÃ©finir une loi de discrÃ©tisation
    */
    Law addLaw( in string name, in long nb_nodes )
        raises (SALOME::SALOME_Exception);

    long countLaw()
        raises (SALOME::SALOME_Exception);

    Law getLaw( in long i )
      raises (SALOME::SALOME_Exception);

    void removeLaw( in Law l )
        raises (SALOME::SALOME_Exception);

    Law findLaw( in string name )
      raises (SALOME::SALOME_Exception);


    /*!
        Discretization defined on the model of blocks 
    */
    long countPropagation()
        raises (SALOME::SALOME_Exception);

    Propagation getPropagation( in long i )
        raises (SALOME::SALOME_Exception);

    Propagation findPropagation( in Edge e )
        raises (SALOME::SALOME_Exception);

    /*!
        Association on lines
    */
   long associateOpenedLine (in Edge mstart, in Edges mline, in Shape gstart, 
                             in double pstart, in Shapes gline, in double pend)
        raises (SALOME::SALOME_Exception);

   long associateClosedLine (in Vertex mfirst, in Edge mstart, in Edges  mline, 
                            in Shape  gstart, in double pstart, in Shapes gline)
        raises (SALOME::SALOME_Exception);


  };
};


#endif

