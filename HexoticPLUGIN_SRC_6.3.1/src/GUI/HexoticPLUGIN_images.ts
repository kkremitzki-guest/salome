<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="en_US">
    <context>
        <name>@default</name>
        <message>
            <source>ICON_DLG_Hexotic_PARAMETERS</source>
            <translation>mesh_hypo_Hexotic.png</translation>
        </message>
        <message>
            <source>ICON_DLG_Hexotic_PARAMETERS_3D</source>
            <translation>mesh_hypo_Hexotic.png</translation>
        </message>
        <message>
            <source>ICON_SMESH_TREE_ALGO_Hexotic_2D3D</source>
            <translation>mesh_tree_algo_Hexotic.png</translation>
        </message>
        <message>
            <source>ICON_SMESH_TREE_ALGO_Hexotic_3D</source>
            <translation>mesh_tree_algo_Hexotic.png</translation>
        </message>
        <message>
            <source>ICON_SMESH_TREE_HYPO_Hexotic_Parameters</source>
            <translation>mesh_tree_hypo_Hexotic.png</translation>
        </message>
        <message>
            <source>ICON_SMESH_TREE_HYPO_Hexotic_Parameters_3D</source>
            <translation>mesh_tree_hypo_Hexotic.png</translation>
        </message>
    </context>
</TS>
