<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="en_US">
<context>
    <name>@default</name>
    <message>
        <source>Hexotic_3D_HYPOTHESIS</source>
        <translation>Hexotic 3D</translation>
    </message>
    <message>
        <source>Hexotic_3D_TITLE</source>
        <translation>Hypothesis Construction</translation>
    </message>
    <message>
        <source>Hexotic_HEXES_MAX_LEVEL</source>
        <translation>Nb. Hexes Max Level</translation>
    </message>
    <message>
        <source>Hexotic_HEXES_MIN_LEVEL</source>
        <translation>Nb. Hexes Min Level</translation>
    </message>
    <message>
        <source>Hexotic_IGNORE_RIDGES</source>
        <translation>Generate smooth meshes no ridges</translation>
    </message>
    <message>
        <source>Hexotic_INVALID_ELEMENTS</source>
        <translation>Authorize invalid elements</translation>
    </message>
    <message>
        <source>Hexotic_QUADRANGLES</source>
        <translation>Salome Quadrangles</translation>
    </message>
    <message>
        <source>Hexotic_SHARP_ANGLE_THRESHOLD</source>
        <translation>Sharp angle threshold in degrees</translation>
    </message>
    <message>
        <source>Hexotic_INFO</source>
        <translation>&lt;b&gt;Warning&lt;/b&gt;: Hexotic algorithm is still in the development and no any guarantee can be given about the result</translation>
    </message>
</context>
</TS>
