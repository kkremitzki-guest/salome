dnl Copyright (C) 2007-2011  CEA/DEN, EDF R&D
dnl
dnl This library is free software; you can redistribute it and/or
dnl modify it under the terms of the GNU Lesser General Public
dnl License as published by the Free Software Foundation; either
dnl version 2.1 of the License.
dnl
dnl This library is distributed in the hope that it will be useful,
dnl but WITHOUT ANY WARRANTY; without even the implied warranty of
dnl MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
dnl Lesser General Public License for more details.
dnl
dnl You should have received a copy of the GNU Lesser General Public
dnl License along with this library; if not, write to the Free Software
dnl Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
dnl
dnl See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
dnl

dnl  File   : check_HexoticPLUGIN.m4
dnl  Author : Vadim SANDLER, Open CASCADE S.A.S (vadim.sandler@opencascade.com)
dnl
AC_DEFUN([CHECK_HEXOTICPLUGIN],[

AC_CHECKING(for Hexotic mesh plugin)

Hexoticplugin_ok=no

HexoticPLUGIN_LDFLAGS=""
HexoticPLUGIN_CXXFLAGS=""

AC_ARG_WITH(Hexoticplugin,
	    [  --with-Hexoticplugin=DIR root directory path of Hexotic mesh plugin installation ])

if test "$with_Hexoticplugin" != "no" ; then
    if test "$with_Hexoticplugin" == "yes" || test "$with_Hexoticplugin" == "auto"; then
	if test "x$HexoticPLUGIN_ROOT_DIR" != "x" ; then
            HexoticPLUGIN_DIR=$HexoticPLUGIN_ROOT_DIR
        fi
    else
        HexoticPLUGIN_DIR="$with_Hexoticplugin"
    fi

    if test "x$HexoticPLUGIN_DIR" != "x" ; then
	if test -f ${HexoticPLUGIN_DIR}/lib${LIB_LOCATION_SUFFIX}/salome/libHexoticEngine.so ; then
	    Hexoticplugin_ok=yes
	    AC_MSG_RESULT(Using Hexotic mesh plugin distribution in ${HexoticPLUGIN_DIR})
	    HexoticPLUGIN_ROOT_DIR=${HexoticPLUGIN_DIR}
	    HexoticPLUGIN_LDFLAGS=-L${HexoticPLUGIN_DIR}/lib${LIB_LOCATION_SUFFIX}/salome
	    HexoticPLUGIN_CXXFLAGS=-I${HexoticPLUGIN_DIR}/include/salome
	else
	    AC_MSG_WARN("Cannot find compiled Hexotic mesh plugin distribution")
	fi
    else
	AC_MSG_WARN("Cannot find compiled Hexotic mesh plugin distribution")
    fi
fi

AC_MSG_RESULT(for Hexotic mesh plugin: $Hexoticplugin_ok)

AC_SUBST(HexoticPLUGIN_ROOT_DIR)
AC_SUBST(HexoticPLUGIN_LDFLAGS)
AC_SUBST(HexoticPLUGIN_CXXFLAGS)
 
])dnl
