<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr_FR" sourcelanguage="en_US">
<context>
    <name>@default</name>
    <message>
        <source>ADD_OPTION</source>
        <translation>Ajouter l&apos;option</translation>
    </message>
    <message>
        <source>BLSURF_ALLOW_QUADRANGLES</source>
        <translation>Autoriser les quadrangles (Test)</translation>
    </message>
    <message>
        <source>BLSURF_ANGLE_MESH_C</source>
        <translation>Angle de maillage C</translation>
    </message>
    <message>
        <source>BLSURF_ANGLE_MESH_S</source>
        <translation>Angle de maillage S</translation>
    </message>
    <message>
        <source>BLSURF_CUSTOM_GEOM</source>
        <translation>Personnalisé</translation>
    </message>
    <message>
        <source>BLSURF_CUSTOM_USER</source>
        <translation>Personnalisé</translation>
    </message>
    <message>
        <source>BLSURF_DECIMESH</source>
        <translation>S&apos;affranchir des frontières des surfaces</translation>
    </message>
    <message>
        <source>BLSURF_DEFAULT_GEOM</source>
        <translation>Défaut</translation>
    </message>
    <message>
        <source>BLSURF_DEFAULT_USER</source>
        <translation>Défaut</translation>
    </message>
    <message>
        <source>BLSURF_GEOM_MESH</source>
        <translation>Maillage géométrique</translation>
    </message>
    <message>
        <source>BLSURF_GRADATION</source>
        <translation>Taux d&apos;accroissement</translation>
    </message>
    <message>
        <source>BLSURF_HGEOMAX</source>
        <translation>Taille géométrique maximale</translation>
    </message>
    <message>
        <source>BLSURF_HGEOMIN</source>
        <translation>Taille géométrique minimale</translation>
    </message>
    <message>
        <source>BLSURF_HPHYDEF</source>
        <translation>Taille d&apos;utilisateur</translation>
    </message>
    <message>
        <source>BLSURF_HPHYMAX</source>
        <translation>Taille physique maximale</translation>
    </message>
    <message>
        <source>BLSURF_HPHYMIN</source>
        <translation>Taille physique minimale</translation>
    </message>
    <message>
        <source>BLSURF_HYPOTHESIS</source>
        <translation>BLSURF 2D</translation>
    </message>
    <message>
        <source>BLSURF_PHY_MESH</source>
        <translation>Maillage physique</translation>
    </message>
    <message>
        <source>BLSURF_ADV_ARGS</source>
        <translation>Avancé</translation>
    </message>
    <message>
        <source>BLSURF_TITLE</source>
        <translation>Construction d&apos;une hypothèse</translation>
    </message>
    <message>
        <source>BLSURF_TOPOLOGY</source>
        <translation>Topologie</translation>
    </message>
    <message>
        <source>BLSURF_TOPOLOGY_CAD</source>
        <translation>A partir de la CAO</translation>
    </message>
    <message>
        <source>BLSURF_TOPOLOGY_PROCESS</source>
        <translation>Prétraitement</translation>
    </message>
    <message>
        <source>BLSURF_TOPOLOGY_PROCESS2</source>
        <translation>Prétraitement++</translation>
    </message>
    <message>
        <source>BLSURF_VERBOSITY</source>
        <translation>Niveau de verbosité</translation>
    </message>
    <message>
        <source>OBLIGATORY_VALUE</source>
        <translation>(Valeur obligatoire)</translation>
    </message>
    <message>
        <source>OPTION_NAME_COLUMN</source>
        <translation>Option</translation>
    </message>
    <message>
        <source>OPTION_VALUE_COLUMN</source>
        <translation>Valeur</translation>
    </message>
    <message>
        <source>REMOVE_OPTION</source>
        <translation>Effacer l&apos;option</translation>
    </message>
    <message>
        <source>BLSURF_SIZE_MAP</source>
        <translation>Tailles locales</translation>
    </message>
    <message>
        <source>SMP_ENTRY_COLUMN</source>
        <translation>Entrée</translation>
    </message>
    <message>
        <source>SMP_NAME_COLUMN</source>
        <translation>Nom</translation>
    </message>
    <message>
        <source>SMP_SIZEMAP_COLUMN</source>
        <translation>Taille locale</translation>
    </message>
    <message>
        <source>BLSURF_SM_SURFACE</source>
        <translation>Sur une face (ou groupe)</translation>
    </message>
    <message>
        <source>BLSURF_SM_EDGE</source>
        <translation>Sur une arête (ou groupe)</translation>
    </message>
    <message>
        <source>BLSURF_SM_POINT</source>
        <translation>Sur un point (ou groupe)</translation>
    </message>
    <message>
        <source>BLSURF_SM_ATTRACTOR</source>
        <translation>Ajouter un attracteur</translation>
    </message>
    <message>
        <source>BLSURF_SM_STD_TAB</source>
        <translation>Simple</translation>
    </message>
    <message>
        <source>BLSURF_SM_ATT_TAB</source>
        <translation>Avancé</translation>
    </message>
    <message>
        <source>BLSURF_SM_REMOVE</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <source>BLSURF_SM_ADD</source>
        <translation>Ajouter</translation>
    </message>
    <message>
        <source>BLSURF_SM_MODIFY</source>
        <translation>Modifier</translation>
    </message>
    <message>
        <source>BLSURF_SM_PARAMS</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <source>BLSURF_ATTRACTOR</source>
        <translation>Attracteur</translation>
    </message>
    <message>
        <source>BLSURF_CONST_SIZE</source>
        <translation>Taille constante autour d&apos;un objet</translation>
    </message>
    <message>
        <source>BLSURF_ATT_DIST</source>
        <translation>Dist. d&apos;influence :</translation>
    </message>
    <message>
        <source>BLSURF_ATT_RADIUS</source>
        <translation>Constant sur :</translation>
    </message>
    <message>
        <source>BLSURF_SM_SIZE</source>
        <translation>Taille Locale :</translation>
    </message>
    <message>
        <source>BLSURF_SM_DIST</source>
        <translation>Distance</translation>
    </message>
    <message>
        <source>BLS_SEL_SHAPE</source>
        <translation>Sélectionnez un objet</translation>
    </message>
    <message>
        <source>BLS_SEL_FACE</source>
        <translation>Sélectionnez une face</translation>
    </message>
    <message>
        <source>BLS_SEL_ATTRACTOR</source>
        <translation>Sélectionnez l&apos;attracteur</translation>
    </message>
    <message>
        <source>BLSURF_SM_SURF_VALUE</source>
        <translation>Taille sur les/la face(s)</translation>
    </message>
    <message>
        <source>BLSURF_SM_EDGE_VALUE</source>
        <translation>Taille sur les/l&apos;arête(s)</translation>
    </message>
    <message>
        <source>BLSURF_SM_POINT_VALUE</source>
        <translation>Taille sur les/le point(s)</translation>
    </message>
    <message>
        <source>BLSURF_ENF_VER</source>
        <translation>Points de passage</translation>
    </message>
    <message>
        <source>BLSURF_ENF_VER_FACE_ENTRY_COLUMN</source>
        <translation>ID de face</translation>
    </message>
    <message>
        <source>BLSURF_ENF_VER_NAME_COLUMN</source>
        <translation>Nom</translation>
    </message>
    <message>
        <source>BLSURF_ENF_VER_X_COLUMN</source>
        <translation>X</translation>
    </message>
    <message>
        <source>BLSURF_ENF_VER_Y_COLUMN</source>
        <translation>Y</translation>
    </message>
    <message>
        <source>BLSURF_ENF_VER_Z_COLUMN</source>
        <translation>Z</translation>
    </message>
    <message>
        <source>BLSURF_ENF_VER_ENTRY_COLUMN</source>
        <translation>ID de point</translation>
    </message>
    <message>
        <source>BLSURF_ENF_VER_GROUP_COLUMN</source>
        <translation>Groupe</translation>
    </message>
    <message>
        <source>BLSURF_ENF_SELECT_FACE</source>
        <translation>Sélectionner une face</translation>
    </message>
    <message>
        <source>BLSURF_ENF_SELECT_VERTEX</source>
        <translation>Sélectionner un point</translation>
    </message>
    <message>
        <source>BLSURF_ENF_VER_X_LABEL</source>
        <translation>X:</translation>
    </message>
    <message>
        <source>BLSURF_ENF_VER_Y_LABEL</source>
        <translation>Y:</translation>
    </message>
    <message>
        <source>BLSURF_ENF_VER_Z_LABEL</source>
        <translation>Z:</translation>
    </message>
    <message>
        <source>BLSURF_ENF_VER_GROUP_LABEL</source>
        <translation>Groupe:</translation>
    </message>
    <message>
        <source>BLSURF_ENF_VER_VERTEX</source>
        <translation>Ajouter un point de passage</translation>
    </message>
    <message>
        <source>BLSURF_ENF_VER_REMOVE</source>
        <translation>Supprimer un point</translation>
    </message>
    <message>
        <source>BLSURF_ENF_VER_GROUPS</source>
        <translation>Groupe unique</translation>
    </message>
</context>
</TS>
