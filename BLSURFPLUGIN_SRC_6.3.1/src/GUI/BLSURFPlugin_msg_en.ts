<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="en_US">
<context>
    <name>@default</name>
    <message>
        <source>ADD_OPTION</source>
        <translation>Add option</translation>
    </message>
    <message>
        <source>BLSURF_ALLOW_QUADRANGLES</source>
        <translation>Allow Quadrangles (Test)</translation>
    </message>
    <message>
        <source>BLSURF_ANGLE_MESH_C</source>
        <translation>Angle Mesh C</translation>
    </message>
    <message>
        <source>BLSURF_ANGLE_MESH_S</source>
        <translation>Angle Mesh S</translation>
    </message>
    <message>
        <source>BLSURF_CUSTOM_GEOM</source>
        <translation>Custom</translation>
    </message>
    <message>
        <source>BLSURF_CUSTOM_USER</source>
        <translation>Custom</translation>
    </message>
    <message>
        <source>BLSURF_DECIMESH</source>
        <translation>Patch independent</translation>
    </message>
    <message>
        <source>BLSURF_DEFAULT_GEOM</source>
        <translation>None</translation>
    </message>
    <message>
        <source>BLSURF_DEFAULT_USER</source>
        <translation>None</translation>
    </message>
    <message>
        <source>BLSURF_GEOM_MESH</source>
        <translation>Geometrical Mesh</translation>
    </message>
    <message>
        <source>BLSURF_GRADATION</source>
        <translation>Gradation</translation>
    </message>
    <message>
        <source>BLSURF_HGEOMAX</source>
        <translation>Max Geometrical Size</translation>
    </message>
    <message>
        <source>BLSURF_HGEOMIN</source>
        <translation>Min Geometrical Size</translation>
    </message>
    <message>
        <source>BLSURF_HPHYDEF</source>
        <translation>User Size</translation>
    </message>
    <message>
        <source>BLSURF_HPHYMAX</source>
        <translation>Max Physical Size</translation>
    </message>
    <message>
        <source>BLSURF_HPHYMIN</source>
        <translation>Min Physical Size</translation>
    </message>
    <message>
        <source>BLSURF_HYPOTHESIS</source>
        <translation>BLSURF 2D</translation>
    </message>
    <message>
        <source>BLSURF_PHY_MESH</source>
        <translation>Physical Mesh</translation>
    </message>
    <message>
        <source>BLSURF_ADV_ARGS</source>
        <translation>Advanced</translation>
    </message>
    <message>
        <source>BLSURF_TITLE</source>
        <translation>Hypothesis Construction</translation>
    </message>
    <message>
        <source>BLSURF_TOPOLOGY</source>
        <translation>Topology</translation>
    </message>
    <message>
        <source>BLSURF_TOPOLOGY_CAD</source>
        <translation>From CAD</translation>
    </message>
    <message>
        <source>BLSURF_TOPOLOGY_PROCESS</source>
        <translation>Pre-process</translation>
    </message>
    <message>
        <source>BLSURF_TOPOLOGY_PROCESS2</source>
        <translation>Pre-process++</translation>
    </message>
    <message>
        <source>BLSURF_VERBOSITY</source>
        <translation>Verbosity level</translation>
    </message>
    <message>
        <source>OBLIGATORY_VALUE</source>
        <translation>(Obligatory value)</translation>
    </message>
    <message>
        <source>OPTION_NAME_COLUMN</source>
        <translation>Option</translation>
    </message>
    <message>
        <source>OPTION_VALUE_COLUMN</source>
        <translation>Value</translation>
    </message>
    <message>
        <source>REMOVE_OPTION</source>
        <translation>Clear option</translation>
    </message>
    <message>
        <source>BLSURF_SIZE_MAP</source>
        <translation>Local Size</translation>
    </message>
    <message>
        <source>SMP_ENTRY_COLUMN</source>
        <translation>Entry</translation>
    </message>
    <message>
        <source>SMP_NAME_COLUMN</source>
        <translation>Name</translation>
    </message>
    <message>
        <source>SMP_SIZEMAP_COLUMN</source>
        <translation>Local size</translation>
    </message>
    <message>
        <source>BLSURF_SM_SURFACE</source>
        <translation>On face (or group)</translation>
    </message>
    <message>
        <source>BLSURF_SM_EDGE</source>
        <translation>On edge (or group)</translation>
    </message>
    <message>
        <source>BLSURF_SM_POINT</source>
        <translation>On point (or group)</translation>
    </message>
    <message>
        <source>BLSURF_SM_ATTRACTOR</source>
        <translation>Add Attractor</translation>
    </message>
    <message>
        <source>BLSURF_SM_STD_TAB</source>
        <translation>Simple map</translation>
    </message>
    <message>
        <source>BLSURF_SM_ATT_TAB</source>
        <translation>Advanced</translation>
    </message>
    <message>
        <source>BLSURF_SM_PARAMS</source>
        <translation>Parameters</translation>
    </message>
    <message>
        <source>BLSURF_ATTRACTOR</source>
        <translation>Attractor</translation>
    </message>
    <message>
        <source>BLSURF_CONST_SIZE</source>
        <translation>Constant size near shape</translation>
    </message>
    <message>
        <source>BLSURF_ATT_DIST</source>
        <translation>Influence dist.</translation>
    </message>
    <message>
        <source>BLSURF_ATT_RADIUS</source>
        <translation>Constant over</translation>
    </message>
    <message>
        <source>BLSURF_SM_SIZE</source>
        <translation>Local Size</translation>
    </message>
    <message>
        <source>BLSURF_SM_DIST</source>
        <translation>Distance</translation>
    </message>
    <message>
        <source>BLS_SEL_SHAPE</source>
        <translation>Select a shape</translation>
    </message>
    <message>
        <source>BLS_SEL_FACE</source>
        <translation>Select a face</translation>
    </message>
    <message>
        <source>BLS_SEL_ATTRACTOR</source>
        <translation>Select the attractor</translation>
    </message>
    <message>
        <source>BLSURF_SM_ADD</source>
        <translation>Add</translation>
    </message>
    <message>
        <source>BLSURF_SM_MODIFY</source>
        <translation>Modify</translation>
    </message>
    <message>
        <source>BLSURF_SM_REMOVE</source>
        <translation>Remove</translation>
    </message>
    <message>
        <source>BLSURF_SM_SURF_VALUE</source>
        <translation>Size on face(s)</translation>
    </message>
    <message>
        <source>BLSURF_SM_EDGE_VALUE</source>
        <translation>Size on edge(s)</translation>
    </message>
    <message>
        <source>BLSURF_SM_POINT_VALUE</source>
        <translation>Size on point(s)</translation>
    </message>
    <message>
        <source>BLSURF_ENF_VER</source>
        <translation>Enforced vertices</translation>
    </message>
    <message>
        <source>BLSURF_ENF_VER_FACE_ENTRY_COLUMN</source>
        <translation>Face Entry</translation>
    </message>
    <message>
        <source>BLSURF_ENF_VER_NAME_COLUMN</source>
        <translation>Name</translation>
    </message>
    <message>
        <source>BLSURF_ENF_VER_X_COLUMN</source>
        <translation>X</translation>
    </message>
    <message>
        <source>BLSURF_ENF_VER_Y_COLUMN</source>
        <translation>Y</translation>
    </message>
    <message>
        <source>BLSURF_ENF_VER_Z_COLUMN</source>
        <translation>Z</translation>
    </message>
    <message>
        <source>BLSURF_ENF_VER_ENTRY_COLUMN</source>
        <translation>Vertex Entry</translation>
    </message>
    <message>
        <source>BLSURF_ENF_VER_GROUP_COLUMN</source>
        <translation>Group</translation>
    </message>
    <message>
        <source>BLSURF_ENF_SELECT_FACE</source>
        <translation>Select a face</translation>
    </message>
    <message>
        <source>BLSURF_ENF_SELECT_VERTEX</source>
        <translation>Select a vertex</translation>
    </message>
    <message>
        <source>BLSURF_ENF_VER_X_LABEL</source>
        <translation>X:</translation>
    </message>
    <message>
        <source>BLSURF_ENF_VER_Y_LABEL</source>
        <translation>Y:</translation>
    </message>
    <message>
        <source>BLSURF_ENF_VER_Z_LABEL</source>
        <translation>Z:</translation>
    </message>
    <message>
        <source>BLSURF_ENF_VER_GROUP_LABEL</source>
        <translation>Group:</translation>
    </message>
    <message>
        <source>BLSURF_ENF_VER_VERTEX</source>
        <translation>Add enforced vertex</translation>
    </message>
    <message>
        <source>BLSURF_ENF_VER_REMOVE</source>
        <translation>Remove vertex</translation>
    </message>
    <message>
        <source>BLSURF_ENF_VER_GROUPS</source>
        <translation>Unique group</translation>
    </message>
</context>
</TS>
