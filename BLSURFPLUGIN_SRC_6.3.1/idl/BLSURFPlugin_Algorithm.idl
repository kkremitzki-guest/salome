// Copyright (C) 2007-2011  CEA/DEN, EDF R&D
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

// ---
// File    : BLSURFPlugin_Algorithm.idl
// Authors : Francis KLOSS (OCC) & Patrick LAUG (INRIA) & Lioka RAZAFINDRAZAKA (CEA)
//           Size maps developement: Nicolas GEIMER (OCC) & Gilles DAVID (EURIWARE)
// ---
//
#ifndef _SMESH_BLSURFALGORITHM_IDL_
#define _SMESH_BLSURFALGORITHM_IDL_

#include "SMESH_Hypothesis.idl"
#include "GEOM_Gen.idl"
#include "SALOME_Exception.idl"

/*!
 * BLSURFPlugin: interfaces to BLSURF related hypotheses and algorithms
 */
module BLSURFPlugin
{
  typedef sequence<string> string_array;

  // Enforced vertex name
  typedef string TEnfName;
  // Entry
  typedef string TEntry;
  typedef sequence<TEntry> TEntryList;
  // Group name
  typedef string TEnfGroupName;
  
  // Coordinates of enforced vertex
  typedef sequence<double,3> TEnfVertexCoords;
  // List of coords
  typedef sequence<TEnfVertexCoords> TEnfVertexCoordsList;
  
  // Enforced vertex
  struct TEnfVertex {
    TEnfName name;
    TEntry geomEntry;
    TEnfVertexCoords coords;
    TEnfGroupName grpName;
    TEntryList faceEntries;
  };
  // List of enforced vertices
  typedef sequence<TEnfVertex> TEnfVertexList;

  // Map Face Entry / List of enforced vertices
  struct TFaceEntryEnfVertexListMapElement {
    TEntry faceEntry;
    TEnfVertexList enfVertexList;
  };
  typedef sequence<TFaceEntryEnfVertexListMapElement> TFaceEntryEnfVertexListMap;
  
  // Map Face Entry / List of coords
  struct TFaceEntryCoordsListMapElement {
    TEntry faceEntry;
    TEnfVertexCoordsList coordsList;
  };
  typedef sequence<TFaceEntryCoordsListMapElement> TFaceEntryCoordsListMap;
  
  // Map Face Entry / List of enf vertex entries
  struct TFaceEntryEnfVertexEntryListMapElement {
    TEntry faceEntry;
    TEntryList enfVertexEntryList;
  };
  typedef sequence<TFaceEntryEnfVertexEntryListMapElement> TFaceEntryEnfVertexEntryListMap;
  
  // Map Coords / Enforced vertex
  struct TCoordsEnfVertexElement {
    TEnfVertexCoords coords;
    TEnfVertex enfVertex;
  };
  typedef sequence<TCoordsEnfVertexElement> TCoordsEnfVertexMap;
  
  // Map Enf Vertex Entry / Enforced vertex
  struct TEnfVertexEntryEnfVertexElement {
    TEntry enfVertexEntry;
    TEnfVertex enfVertex;
  };
  typedef sequence<TEnfVertexEntryEnfVertexElement> TEnfVertexEntryEnfVertexMap;

  // Attractors
  struct TAttractorParams {
    string faceEntry;
    string attEntry;
    double startSize;
    double endSize;
    double infDist;
    double constDist;
  };
  typedef sequence<TAttractorParams> TAttParamsMap;
  
  /*!
   * BLSURFPlugin_BLSURF: interface of BLSURF algorithm
   */
  interface BLSURFPlugin_BLSURF : SMESH::SMESH_2D_Algo
  {
  };

  /*!
   * BLSURFPlugin_Hypothesis: interface of "BLSURF parameters" hypothesis
   */
  interface BLSURFPlugin_Hypothesis : SMESH::SMESH_Hypothesis
  {
    /*!
     * Sets topology usage way defining how mesh conformity is assured
     * value=0 - mesh conformity is assured by conformity of a shape
     * value=1,2 - mesh conformity is assured by pre-processing a CAD model
     */
    void SetTopology(in long way);
    long GetTopology();

    /*!
     * Sets a way to define size of mesh elements to generate
     * 0 - size is defined automatically
     * 1 - size is set by SetPhySize() method
     */
    void SetPhysicalMesh(in long isCustom);
    long GetPhysicalMesh();

    /*!
     * Sets size of mesh elements to generate
     */
    void SetPhySize(in double size);
    double GetPhySize();

    /*!
     * Sets lower boundary of mesh element size (PhySize)
     */
    void SetPhyMin(in double theMinSize);
    double GetPhyMin();

    /*!
     * Sets upper boundary of mesh element size (PhySize)
     */
    void SetPhyMax(in double theMaxSize);
    double GetPhyMax();

    /*!
     * Sets a way to define maximum angular deflection of mesh from CAD model
     * 0 - deflection is defined automatically
     * 1 - deflection is set by SetAngleMeshS() and SetAngleMeshC() methods
     */
    void SetGeometricMesh(in long isCustom);
    long GetGeometricMesh();

    /*!
     * Sets angular deflection (in degrees) of a mesh face from CAD surface
     */
    void SetAngleMeshS(in double angle);
    double GetAngleMeshS();

    /*!
     * Sets angular deflection (in degrees) of a mesh edge from CAD curve
     */
    void SetAngleMeshC(in double angle);
    double GetAngleMeshC();

    /*!
     * Sets lower boundary of mesh element size computed to respect angular deflection
     */
    void SetGeoMin(in double theMinSize);
    double GetGeoMin();

    /*!
     * Sets upper boundary of mesh element size computed to respect angular deflection
     */
    void SetGeoMax(in double theMaxSize);
    double GetGeoMax();

    /*!
     * Sets maximal allowed ratio between the lengths of two adjacent edges
     */
    void SetGradation(in double ratio);
    double GetGradation();

    /*!
     * Sets to create quadrilateral elements or not
     */
    void SetQuadAllowed(in boolean allowed);
    boolean GetQuadAllowed();

    /*!
     * To respect geometrical edges or not
     */
    void SetDecimesh(in boolean toIgnoreEdges);
    boolean GetDecimesh();

    /*!
     * Sets verbosity level in the range 0 to 100.
     */
    void SetVerbosity(in short theVal) raises (SALOME::SALOME_Exception);
    short GetVerbosity();

    /*!
     * Sets advanced option value
     */
    void SetOptionValue(in string optionName,
                        in string optionValue) raises (SALOME::SALOME_Exception);
    string GetOptionValue(in string optionName) raises (SALOME::SALOME_Exception);
    /*!
     * Unsets advanced option
     */
    void UnsetOption(in string optionName);

    /*!
     * Return array of strings each of which is option name concatenated
     * with option value devided by semicolon - "option_name:option_value".
     * Option value is empty if an option is not set.
     * Note: the method is mostly for interaction with GUI.
     */
    string_array GetOptionValues();

    /*!
     * Set option values each in the form "option_name[:option_value]".
     * Note: the method is mostly for interaction with GUI.
     */
    void SetOptionValues(in string_array options);

    /*!
     * SizeMap
     */
    void SetSizeMapEntries(in string_array sizeMaps);
    void ClearSizeMaps();

    void UnsetEntry(in string entry);

    /*!
     * Set/unset a SizeMap on geom object
     */
    void         SetSizeMap(in GEOM::GEOM_Object GeomObj, in string sizeMap);
    void         UnsetSizeMap(in GEOM::GEOM_Object GeomObj);
    
    /*!
     * Set a SizeMap on geom object given by entry
     */
    void         SetSizeMapEntry(in string entry, in string sizeMap);
    string       GetSizeMapEntry(in string entry);
    string_array GetSizeMapEntries();

    /*!
     * Set/unset an attractor on geom object
     */
    void         SetAttractor(in GEOM::GEOM_Object GeomObj, in string attractor);
    void         UnsetAttractor(in GEOM::GEOM_Object GeomObj);
    
    /*!
     * Set an attractor on geom object given by entry
     */
    void         SetAttractorEntry(in string entry, in string attractor);
    string       GetAttractorEntry(in string entry);
    string_array GetAttractorEntries();

    /*!
     * Set/unset an attractor given as geom object on another geom object
     */
    void         SetAttractorGeom(in GEOM::GEOM_Object GeomObj, in GEOM::GEOM_Object AttractorShape, in double StartSize, in double EndSize, in double ActionRadius, in double ConstantRadius);
    void         UnsetAttractorGeom(in GEOM::GEOM_Object GeomObj);

    /*!
     * Set an attractor given by entry on a geom object given by entry
     */
    void         SetClassAttractorEntry(in string entry, in string att_entry, in double StartSize, in double EndSize, in double ActionRadius, in double ConstantRadius );
    BLSURFPlugin::TAttParamsMap GetAttractorParams();



/*
    void         SetCustomSizeMapEntry(in string entry, in string sizeMap);
    string       GetCustomSizeMapEntry(in string entry);
    void         SetCustomSizeMap(in GEOM::GEOM_Object GeomObj, in string sizeMap);
    void         UnsetCustomSizeMap(in GEOM::GEOM_Object GeomObj);
    string_array GetCustomSizeMapEntries();
*/
    ///////////////////////
    // ENFORCED VERTEXES //
    ///////////////////////
    
    TFaceEntryEnfVertexListMap      GetAllEnforcedVerticesByFace();
    TEnfVertexList                  GetAllEnforcedVertices();
    
    TFaceEntryCoordsListMap         GetAllCoordsByFace();
    TCoordsEnfVertexMap             GetAllEnforcedVerticesByCoords();
    
    TFaceEntryEnfVertexEntryListMap GetAllEnfVertexEntriesByFace();
    TEnfVertexEntryEnfVertexMap     GetAllEnforcedVerticesByEnfVertexEntry();
    
    void                            ClearAllEnforcedVertices();
    
   /*!
    * Set/get/unset an enforced vertex on geom face
    */
    boolean SetEnforcedVertex(in GEOM::GEOM_Object theFace, in double x, in double y, in double z) raises (SALOME::SALOME_Exception);
    boolean SetEnforcedVertexNamed(in GEOM::GEOM_Object theFace, in double x, in double y, in double z, in TEnfName theVertexName) raises (SALOME::SALOME_Exception);
    boolean SetEnforcedVertexGeom(in GEOM::GEOM_Object theFace, in GEOM::GEOM_Object theVertex) raises (SALOME::SALOME_Exception);
    boolean SetEnforcedVertexWithGroup(in GEOM::GEOM_Object theFace, in double x, in double y, in double z, in TEnfGroupName groupName) raises (SALOME::SALOME_Exception);
    boolean SetEnforcedVertexNamedWithGroup(in GEOM::GEOM_Object theFace, in double x, in double y, in double z, in TEnfName theVertexName, in TEnfGroupName groupName) raises (SALOME::SALOME_Exception);
    boolean SetEnforcedVertexGeomWithGroup(in GEOM::GEOM_Object theFace, in GEOM::GEOM_Object theVertex, in TEnfGroupName groupName) raises (SALOME::SALOME_Exception);

    
    TEnfVertexList GetEnforcedVertices(in GEOM::GEOM_Object theFace) raises (SALOME::SALOME_Exception);
    
    boolean UnsetEnforcedVertex(in GEOM::GEOM_Object theFace, in double x, in double y, in double z) raises (SALOME::SALOME_Exception);
    boolean UnsetEnforcedVertexGeom(in GEOM::GEOM_Object theFace, in GEOM::GEOM_Object theVertex) raises (SALOME::SALOME_Exception);
    boolean UnsetEnforcedVertices(in GEOM::GEOM_Object theFace) raises (SALOME::SALOME_Exception);
    
   /*!
    * Set/get/unset an enforced vertex on geom face given by entry
    */
    boolean SetEnforcedVertexEntry(in TEntry theFaceEntry, in double x, in double y, in double z, 
                                   in TEnfName theVertexName, in TEntry theVertexEntry, in string groupName) raises (SALOME::SALOME_Exception);
//    boolean SetEnforcedVertexEntryNamed(in TEntry theFaceEntry, in double x, in double y, in double z, in TEnfName theVertexName) raises (SALOME::SALOME_Exception);
//    boolean SetEnforcedVertexEntryGeom(in TEntry theFaceEntry, in TEntry theVertexEntry, in TEnfName theVertexName) raises (SALOME::SALOME_Exception);
    /* TODO GROUPS
    void SetEnforcedVertexEntryWithGroup(in string theFaceEntry, in double x, in double y, in double z, in TEnfGroupName groupName)
        raises (SALOME::SALOME_Exception);
    */
//     
    TEnfVertexList GetEnforcedVerticesEntry(in TEntry theFaceEntry) raises (SALOME::SALOME_Exception);
//     
    boolean UnsetEnforcedVertexEntry(in TEntry theFaceEntry, in double x, in double y, in double z, in TEntry theVertexEntry) raises (SALOME::SALOME_Exception);
//    boolean UnsetEnforcedVertexEntryGeom(in TEntry theFaceEntry, in TEntry theVertexEntry) raises (SALOME::SALOME_Exception);
    boolean UnsetEnforcedVerticesEntry(in TEntry theFaceEntry) raises (SALOME::SALOME_Exception);

   /*!
     * Set/get an enforced vertex on geom object and add it to a group
    */
    /* TODO GROUPS
    void SetEnforcedVertexGroupName(in double x, in double y, in double z, in TEnfGroupName groupName)
        raises (SALOME::SALOME_Exception);
    string GetEnforcedVertexGroupName(in double x, in double y, in double z)
        raises (SALOME::SALOME_Exception);
    */
    ///////////////////////

  };
};

#endif
