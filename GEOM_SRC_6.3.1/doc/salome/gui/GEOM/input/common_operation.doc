/*!

\page common_operation_page Common

For detail description of the Boolean operations please refer to
<a href="SALOME_BOA_PA.pdf">this document</a>. 
It provides a general review of the Partition and Boolean
operations algorithms, describes the usage methodology and highlighs
major limitations of these operations.

To produce a \b Common operation in the <b>Main Menu</b> select <b>Operations - > Boolean - > Common</b>

This operation cuts the common part of two shapes and transforms it into an independent geometrical object.

The \b Result will be any \b GEOM_Object.
<b>TUI Command:</b> <em>geompy.MakeCommon(s1, s2)</em>
<b>Arguments:</b> Name + 2 shapes.
<b>Advanced option:</b>
\ref restore_presentation_parameters_page "Set presentation parameters and subshapes from arguments".

\image html bool2.png

<b>Example:</b>

\image html fusesn1.png "The initial shapes"

\image html commonsn.png "The resulting object" 

Our <b>TUI Scripts</b> provide you with useful examples of the use of
\ref tui_common "Boolean Operations".

*/
