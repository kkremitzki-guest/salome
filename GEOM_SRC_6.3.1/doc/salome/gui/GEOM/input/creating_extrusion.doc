/*!

\page create_extrusion_page Extrusion

\n To generate an \b Extrusion on an object in the <b>Main Menu</b>
select <b>New Entity - > Generation  - > Extrusion</b>

\n There are 3 algorithms for creation of an \b Extrusion (Prism).
\n The \b Result of the operation will be a GEOM_Object (edge, face, shell
solid or compsolid).

\n Firstly, you can define the <b>Base Shape</b> (a basis of the
extrusion), the \b Vector (a direction of the extrusion) and the \b
Height of extrusion. Optionally you can define the <b>Scale Factor</b> to
build extrusion with scaled opposite base. Scaling is possible only
with 1D and 2D bases.
<br>It is possible to select in GUI several Base Shapes to make
several extrusions (using Shift button).
\n The \b Result of the operation will be a GEOM_Object (edge, face,
shell, solid or compsolid).
\n <b> Both Directions </b> checkbox allows extruding the source
object both forward and backward. With this option scaling is not possible.
\n <b>TUI Command:</b> <em>geompy.MakePrismVecH(Base, Vector, Height, theScaleFactor = -1.0)</em>
\n <b>Arguments:</b> Name + one or several shapes (vertex, edge, planar wire, face or
shell) serving as base objects + 1 vector (for direction of the
extrusion) + 1 value (dimension) + 1 optional value (scale factor for
the opposite base).
\n<b>Advanced options</b> \ref preview_anchor "Preview"

\image html extrusion1.png

\n Secondly, you can define the \b Extrusion by the <b>Base Shape(s)</b>
and the \b Start and <b>End Point</b> of the \b Vector (in this way
you don't need to create it in advance). Optionally you can define the
<b>Scale Factor</b> to build extrusion with scaled opposite
base. Scaling is possible only with 1D and 2D bases.
\n <b> Both Directions </b> checkbox allows extruding the source
object both forward and backward.  With this option scaling is not possible.
\n <b>TUI Command:</b> <em>geompy.MakePrism(Base, Point1, Point2, theScaleFactor = -1.0)</em>
\n <b>Arguments:</b> Name + one or several shapes (vertex, edge, planar wire, face or
shell) serving as base objects + 2 vertices + 1 optional value (scale factor for
the opposite base).

\image html extrusion2.png

\n Finally, you can define the \b Extrusion by the <b>Base Shape(s)</b>
and the <b>DX, DY, DZ</b> Vector. Optionally you can define the
<b>Scale Factor</b> to build extrusion with scaled opposite
base. Scaling is possible only with 1D and 2D bases.\n
<b>Both Directions</b> checkbox allows extruding the
source objects both forward and backward. With this option scaling is not possible.
\n <b>TUI Command:</b> <em>geompy.MakePrismDXDYDZ(Base, dx, dy, dz, theScaleFactor = -1.0)</em>
\n <b>Arguments:</b> Name + one or several shapes (vertex, edge, planar wire, face or
shell) serving as base objects + 3 axis directions + 1 optional value (scale factor for
the opposite base).

\image html extrusion3.png

<b>Examples:</b>

\image html prisms_basessn.png
<center>Base Shape</center>

\image html prismssn.png
<center>Prisms</center>

Our <b>TUI Scripts</b> provide you with useful examples of creation of
\ref tui_creation_prism "Complex Geometric Objects".

*/
