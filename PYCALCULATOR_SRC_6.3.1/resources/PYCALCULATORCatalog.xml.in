<?xml version='1.0' encoding='us-ascii' ?>
<!--
  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE

  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com

-->
 
<!-- XML component catalog -->
<begin-catalog>
    <!-- Path prefix information -->
    <path-prefix-list></path-prefix-list>
    <!-- Component list -->
    <component-list>
        <component>
            <!-- Component identification -->
            <component-name>PYCALCULATOR</component-name>
            <component-username>PyCalculator</component-username>
            <component-type>OTHER</component-type>
            <component-author>LD</component-author>
            <component-version>@VERSION@</component-version>
            <component-comment>Test component (Arithmetic operations with MED_Field)</component-comment>
            <component-multistudy>0</component-multistudy>
            <component-icone>PYCALCULATOR.png</component-icone>
            <constraint>hostname = localhost</constraint>
            <component-impltype>0</component-impltype>
	    <!-- Component interfaces -->
            <component-interface-list>
                <component-interface-name>PYCALCULATOR_Gen</component-interface-name>
                <component-interface-comment>No comment</component-interface-comment>
                <!-- Service list -->
		<component-service-list>
                    <component-service>
                        <!-- service-identification -->
                        <service-name>Add</service-name>
                        <service-author>LD</service-author>
                        <service-version>1.0</service-version>
                        <service-comment>Addition of 2 fields of double</service-comment>
                        <service-by-default>1</service-by-default>
                        <!-- service-connexion -->
                        <inParameter-list>
                            <inParameter>
                                <inParameter-name>field1</inParameter-name>
                                <inParameter-type>SALOME_MED/FIELDDOUBLE</inParameter-type>
                                <inParameter-comment>first field of double</inParameter-comment>
                            </inParameter>
                            <inParameter>
                                <inParameter-name>field2</inParameter-name>
                                <inParameter-type>SALOME_MED/FIELDDOUBLE</inParameter-type>
                                <inParameter-comment>second field of double</inParameter-comment>
                            </inParameter>
                        </inParameter-list>
                        <outParameter-list>
                            <outParameter>
                                <outParameter-name>result</outParameter-name>
                                <outParameter-type>SALOME_MED/FIELDDOUBLE</outParameter-type>
                                <outParameter-comment>result</outParameter-comment>
                            </outParameter>
                        </outParameter-list>
                        <DataStream-list></DataStream-list>
                    </component-service>
                    <component-service>
                        <!-- service-identification -->
                        <service-name>Mul</service-name>
                        <service-author>LD</service-author>
                        <service-version>1.0</service-version>
                        <service-comment>Multiplication of 2 fields of double</service-comment>
                        <service-by-default>0</service-by-default>
                        <!-- service-connexion -->
                        <inParameter-list>
                            <inParameter>
                                <inParameter-name>field1</inParameter-name>
                                <inParameter-type>SALOME_MED/FIELDDOUBLE</inParameter-type>
                                <inParameter-comment>initial field of double</inParameter-comment>
                            </inParameter>
                            <inParameter>
                                <inParameter-name>x1</inParameter-name>
                                <inParameter-type>double</inParameter-type>
                                <inParameter-comment>multiplicator</inParameter-comment>
                            </inParameter>
                        </inParameter-list>
                        <outParameter-list>
                            <outParameter>
                                <outParameter-name>result</outParameter-name>
                                <outParameter-type>SALOME_MED/FIELDDOUBLE</outParameter-type>
                                <outParameter-comment>result</outParameter-comment>
                            </outParameter>
                        </outParameter-list>
                        <DataStream-list></DataStream-list>
                    </component-service>
                    <component-service>
                        <!-- service-identification -->
                        <service-name>Constant</service-name>
                        <service-author>LD</service-author>
                        <service-version>1.0</service-version>
                        <service-comment>Build a constant field of doubles</service-comment>
                        <service-by-default>0</service-by-default>
                        <!-- service-connexion -->
                        <inParameter-list>
                            <inParameter>
                                <inParameter-name>field1</inParameter-name>
                                <inParameter-type>SALOME_MED/FIELDDOUBLE</inParameter-type>
                                <inParameter-comment>allows to build the support</inParameter-comment>
                            </inParameter>
                            <inParameter>
                                <inParameter-name>x1</inParameter-name>
                                <inParameter-type>double</inParameter-type>
                                <inParameter-comment>the constant</inParameter-comment>
                            </inParameter>
                        </inParameter-list>
                        <outParameter-list>
                            <outParameter>
                                <outParameter-name>result</outParameter-name>
                                <outParameter-type>SALOME_MED/FIELDDOUBLE</outParameter-type>
                                <outParameter-comment>result based on first field support</outParameter-comment>
                            </outParameter>
                        </outParameter-list>
                        <DataStream-list></DataStream-list>
                    </component-service>
                    <component-service>
                        <!-- service-identification -->
                        <service-name>writeMEDfile</service-name>
                        <service-author>LD</service-author>
                        <service-version>1.0</service-version>
                        <service-comment>write a field in a Med file</service-comment>
                        <service-by-default>0</service-by-default>
                        <!-- service-connexion -->
                        <inParameter-list>
                            <inParameter>
                                <inParameter-name>field1</inParameter-name>
                                <inParameter-type>SALOME_MED/FIELDDOUBLE</inParameter-type>
                                <inParameter-comment>transient Med field</inParameter-comment>
                            </inParameter>
                            <inParameter>
                                <inParameter-name>filename</inParameter-name>
                                <inParameter-type>string</inParameter-type>
                                <inParameter-comment>Med filename</inParameter-comment>
                            </inParameter>
                        </inParameter-list>
                        <outParameter-list></outParameter-list>
                        <DataStream-list></DataStream-list>
                    </component-service>
                </component-service-list>
            </component-interface-list>
        </component>
    </component-list>
</begin-catalog>
