
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 Documentation of the programming interface (API)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

This section describes the python modules of the
``salome.kernel`` python package. The main part is generated from the
code documentation included in source python files.

.. toctree::
   :maxdepth: 3

   kernel.rst
   parametric.rst
