
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 Documentation of the KERNEL python packages
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Main documentation
==================

.. toctree::
   :maxdepth: 3

   overview.rst
   docapi.rst


Additional documentation
========================

.. toctree::
   :maxdepth: 3

   salomepypkg.rst
