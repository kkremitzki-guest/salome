:mod:`salome.kernel` -- Package containing the KERNEL python utilities
======================================================================

:mod:`deprecation` -- Indication of deprecated modules and functions
--------------------------------------------------------------------

.. automodule:: salome.kernel.deprecation
   :members:


:mod:`termcolor` -- Display colored text in terminal
----------------------------------------------------

.. automodule:: salome.kernel.termcolor
   :members:
   :exclude-members: TEST_termcolor


:mod:`logger` -- Logging utility
--------------------------------

.. automodule:: salome.kernel.logger

.. autoclass:: Logger
   :members:
   :show-inheritance:

.. autoclass:: ExtLogger
   :members:
   :show-inheritance:


:mod:`studyedit` -- Study editor
--------------------------------

.. automodule:: salome.kernel.studyedit
   :members:
