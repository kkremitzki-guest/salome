=================================================================
User's guide, for developpers and users
=================================================================

*html version of this document is produced with docutils*::

  rst2html < doc.txt > doc.html

*This document corresponds to SALOME2 3.1.0*
*NOT UP TO DATE with 3.2.0*

.. contents::
.. sectnum::

+-------------------------------------------+
| **WORK in PROGRESS, INCOMPLETE DOCUMENT** |
+-------------------------------------------+

-------------------------------------------------------------------------------

This guide provides you with some basic concepts for developing and
using the SALOME platform. You will find some information on the
general technical architecture ...

Introduction
============

General information
-------------------

This document has been initialized by collecting and updating an existing
documentation. The restructured text format has been selected for
writing. This format can be read with a basic editor except for
pictures and some hypertext links. You can build the html pages using
the docutils scripts provided with python packages on most platform.

Definitions
-----------

``WORK IN PROGRESS``

Module
   definition of a module and/or link to the definition

Container
   definition of a container
  

General concepts
================
modules et d�pendances (s'inspirer de PYHELLO)


Filesystem organisation
========================

Voir doc de JR "Organisation de la plate-forme"

A typical source working directory
----------------------------------
organisation type des sources d'un module standard (sp�cifications techniques)

A typical installation directory
--------------------------------
organisation type des produits install�s


Building process
================
Proc�dures de compilation (renvoie au install.sh)


Developer's guide - basic concepts
=========================================

Guide du d�veloppeur: �l�ments de conception
  - zoom sur certains �l�ments techniques bons � connaitre pour faire
    �voluer le KERNEL sans tout casser.
  - les ressources du kernel:
    - trace, debug, exception (cf. kernel_ressources.tex)
    - classes batch (pr�sentation puis renvoi � la doc d'Ivan)
  - d�veloppement de tests unitaires

Developer's guide - managing the development space
==================================================

- Guide du d�veloppeur: gestion de l'espace de d�veloppement
  - principe de mise en oeuvre (r�le des �tapes: build_configure, ...)
  - description des fichiers m4 et du principe de mise en oeuvre
  - les Makefile.in, ... (cf. doc guide du d�veloppeur).
  - �volution des proc�dures de construction
  - personalisation des proc�dures de construction

Developer' guide - integration tools
==================================== 
- Guide de l'int�grateur (d�veloppeur de nouveaux modules)
(on lui montre ici le principe de construction et les ressources �
disposition pour faire le travail)
  - cr�ation d'un modules
  - int�gration code bo�te noire (perfect, solver)
  - int�gration biblioth�que de fonctions (hxx2salome, voir avec
    N.Crouzet)
  - int�gration de mod�les de donn�es (xdata) 


End user's guide
================
- Guide de l'utilisateur
  - concept d'application (renvoie doc Paul)
  - commandes avanc�es (showNS, exemple de contact de la
    session, d'un engine, utilisation du lifeCycle, du module salome,
    des modules geompy et smesh)
  - utilisation en mode distribu� (doc de B. Sechet)
  - GUI and TUI documentation















RST Exemples
============

See INSTALL_ for general information on required configuration and 
prerequisites, compilation procedure, setting environment principles.

.. _INSTALL: ./INSTALL.html


-------------------------------------------------------------------------------

+----------------------------------+------------------------------------------+
| `General KERNEL documentation`_  | `End User KERNEL Doxygen documentation`_ |
+----------------------------------+------------------------------------------+

.. _`General KERNEL documentation`:           ./index.html
.. _`End User KERNEL Doxygen documentation`:  ./tui/KERNEL/index.html
