# Copyright (C) 2010-2011  CEA/DEN, EDF R&D
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
# See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#

# create a plugin that implements a sample toolbar action

QT4_WRAP_CPP(MOC_SRCS MyToolBarActions.h)

# we implement a pqConePanel.h for the ConeSource
ADD_PARAVIEW_ACTION_GROUP(IFACES IFACE_SRCS CLASS_NAME MyToolBarActions 
                          GROUP_NAME "ToolBar/MyActions")

# create a plugin for this panel
ADD_PARAVIEW_PLUGIN(GUISampleToolBar "1.0" 
                    GUI_INTERFACES ${IFACES} 
                    SOURCES MyToolBarActions.cxx ${MOC_SRCS} ${IFACE_SRCS})

INSTALL(
	TARGETS GUISampleToolBar 
	DESTINATION lib/paraview
)
