// PARAVIS : ParaView wrapper SALOME module
//
// Copyright (C) 2010-2011  CEA/DEN, EDF R&D
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
// File   : PVGUI_Module.h
// Author : Sergey ANIKIN
//


#ifndef PVGUI_Module_H
#define PVGUI_Module_H

#include <SalomeApp_Module.h>

#include <ostream>
#include <vtkType.h>

class QMenu;
class QDockWidget;
class QToolBar;
class vtkPVMain;
class pqOptions;
class pqServer;
class pqViewManager;
class pqMainWindowCore;
class vtkEventQtSlotConnect;
class pqPythonScriptEditor;


class PVGUI_Module : public SalomeApp_Module
{
  Q_OBJECT
   
  //! Menu actions
  enum { // Menu "File"
         OpenFileId,

	 LoadStateId,
	 SaveStateId,

	 SaveDataId,
	 SaveScreenshotId,
	 ExportId,

	 SaveAnimationId,
	 SaveGeometryId,

	 ConnectId,
	 DisconnectId,

	 // Menu "Edit"
	 UndoId,
	 RedoId,

	 CameraUndoId,
	 CameraRedoId,

   FindDataId,   
	 ChangeInputId,
   IgnoreTimeId, 
	 DeleteId,
	 DeleteAllId,

	 SettingsId,
	 ViewSettingsId,

	 // Menu "View"
   FullScreenId, 

	 // Menu "Animation"
	 FirstFrameId,
	 PreviousFrameId,
	 PlayId,
	 NextFrameId,
	 LastFrameId,
	 LoopId,

	 // Menu "Tools" 
	 CreateCustomFilterId,
	 ManageCustomFiltersId,
	 CreateLookmarkId,
	 ManageLinksId,
	 AddCameraLinkId,
	 ManagePluginsExtensionsId,
	 DumpWidgetNamesId,
	 RecordTestId,
	 RecordTestScreenshotId,
	 PlayTestId,
	 MaxWindowSizeId,
	 CustomWindowSizeId,
	 TimerLogId,
	 OutputWindowId,
	 PythonShellId,
	 ShowTraceId,

	 // Menu "Help" 
	 AboutParaViewId,
	 ParaViewHelpId,
	 EnableTooltipsId,

	 // Menu "Window" - "New Window"
	 ParaViewNewWindowId,

	 // "Save state" ParaVis module root object popup
	 SaveStatePopupId,

	 // "Add state" and "Reload state" popups
	 AddStatePopupId,
	 CleanAndAddStatePopupId,

	 // "Rename" and "Delete" popups (Object Browser)
	 ParaVisRenameId,
	 ParaVisDeleteId
  };

public:
  PVGUI_Module();
  ~PVGUI_Module();

  virtual void           initialize( CAM_Application* );
  virtual void           windows( QMap<int, int>& ) const;

  pqViewManager*         getMultiViewManager() const;

  virtual QString engineIOR() const;

  void openFile(const char* theName);
  void executeScript(const char *script);
  void saveParaviewState(const char* theFileName);
  void loadParaviewState(const char* theFileName);
  void clearParaviewState();

  QString getTraceString();
  void saveTrace(const char* theName);

  pqServer* getActiveServer();

  virtual void createPreferences();

  virtual void contextMenuPopup(const QString& theClient, QMenu* theMenu, QString& theTitle);

public slots:
  void onImportFromVisu(QString theEntry);

private:
  //! Initialize ParaView if not yet done (once per session)
  static bool            pvInit();  
 
  //! Create actions for ParaView GUI operations
  void                   pvCreateActions();

  //! Create menus for ParaView GUI operations duplicating menus in pqMainWindow ParaView class
  void                   pvCreateMenus();

  //! Create toolbars for ParaView GUI operations duplicating toolbars in pqMainWindow ParaView class
  void                   pvCreateToolBars();

  //! Create dock widgets for ParaView widgets
  void                   setupDockWidgets();

  //! Save states of dockable ParaView widgets
  void                   saveDockWidgetsState();

  //! Restore states of dockable ParaView widgets
  void                   restoreDockWidgetsState();

  //! Shows or hides ParaView view window
  void                   showView( bool );    

  //! Returns QMenu object for a given menu id
  QMenu*                 getMenu( const int );
  
  //! Discover help project files from the resources.
  QString getHelpFileName();

  void                   deleteTemporaryFiles();
  
  //QList<QToolBar*>       getParaViewToolbars();

  //! Create actions for ParaViS
  void createActions();

  //! Create menus for ParaViS
  void createMenus();

  //! Load selected state
  void loadSelectedState(bool toClear);

  //! update macros state
  void updateMacros();

private slots:

  void showHelpForProxy( const QString& proxy );
  
  void onPreAccept();
  void onPostAccept();
  void endWaitCursor();

  void activateTrace();

  //  void buildToolbarsMenu();

  void showParaViewHelp();
  void showHelp(const QString& url);

  void onConnectionCreated(vtkIdType);

  void onStartProgress();
  void onEndProgress();
  void onShowTrace();

  void onNewParaViewWindow();

  void onSaveMultiState();
  void onAddState();
  void onCleanAddState();

  void onRename();
  void onDelete();

public slots:
  virtual bool           activateModule( SUIT_Study* );
  virtual bool           deactivateModule( SUIT_Study* );
  virtual void           onApplicationClosed( SUIT_Application* );
  virtual void           studyClosed( SUIT_Study* );

protected slots:
  virtual void           onModelOpened();

private:
  class pqImplementation;
  pqImplementation*      Implementation;

  int                    mySelectionControlsTb;
  int                    mySourcesMenuId;
  int                    myFiltersMenuId;
  int                    myToolbarsMenuId;
  int                    myMacrosMenuId;
  
  typedef QMap<QWidget*, bool> WgMap;
  WgMap                  myDockWidgets;
  WgMap                  myToolbars;
  WgMap                  myToolbarBreaks;

  QStringList            myTemporaryFiles;

  QtMsgHandler           myOldMsgHandler;
  QTimer* myTraceTimer;

  vtkEventQtSlotConnect *VTKConnect;

  pqPythonScriptEditor* myTraceWindow;

  int myStateCounter;
};

#endif // PVGUI_Module_H
