// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

using namespace std;

#include "ATOMICGUI.h"

#include "ATOMICGUI_DataModel.h"
#include "ATOMICGUI_DataObject.h"
#include "ATOMICGUI_AddAtomDlg.h"

#include <LightApp_Application.h>
#include <LightApp_DataOwner.h>
#include <LightApp_SelectionMgr.h>

#include <SUIT_ResourceMgr.h>
#include <SUIT_Session.h>
#include <SUIT_Desktop.h>

#include <qaction.h>
#include <qmenu.h>

/*! Constructor */
ATOMICGUI::ATOMICGUI()
: LightApp_Module( "ATOMICGUI" )
{
}

/*! Initialization funciton.  Called only once on first activation of GUI module.  */
void ATOMICGUI::initialize ( CAM_Application* app )
{
  LightApp_Module::initialize( app );// call parent's implementation

  SUIT_Desktop* parent = application()->desktop();
  SUIT_ResourceMgr* resMgr = SUIT_Session::session()->resourceMgr();

  // create actions
  createAction( agCreateMol, tr("TOP_CREATE_MOL"), resMgr->loadPixmap("ATOMIC", tr("ICON_MOLECULE")),
                tr("MEN_CREATE_MOL"), tr("STB_CREATE_MOL"), 0, parent, false, this, SLOT(onOperation()) );
  createAction( agAddAtom,   tr("TOP_ADD_ATOM"),   resMgr->loadPixmap("ATOMIC", tr("ICON_ATOM")),
                tr("MEN_ADD_ATOM"),   tr("STB_ADD_ATOM"),   0, parent, false, this, SLOT(onOperation()) );

  // init popup menus
  int aAtomicMnu = createMenu( tr( "MEN_ATOMIC" ), -1, -1, 50 );
  createMenu( agCreateMol, aAtomicMnu, 10 );
  createMenu( separator(), aAtomicMnu, -1, 10 );
  createMenu( agAddAtom,   aAtomicMnu, 10 );

  // create toolbar
  int tbId = createTool( tr( "MEN_ATOMIC" ) );
  createTool( agCreateMol, tbId );
  createTool( agAddAtom,   tbId );
}

/*! Returns default icon of a component. */
QString ATOMICGUI::iconName() const
{
  return QObject::tr( "ICON_ATOMIC" );
}

/*! Returns list of windows required for this GUI module. */
void ATOMICGUI::windows ( QMap<int, int>& aMap ) const
{
  aMap.insert( LightApp_Application::WT_ObjectBrowser, Qt::LeftDockWidgetArea );
}

/*! Returns list of entities of selected objects. */
void ATOMICGUI::selected( QStringList& entries, const bool multiple )
{
  LightApp_SelectionMgr* mgr = getApp()->selectionMgr();
  if( !mgr )
    return;

  SUIT_DataOwnerPtrList anOwnersList;
  mgr->selected( anOwnersList );

  for ( int i = 0; i < anOwnersList.size(); i++ )
  {
    const LightApp_DataOwner* owner = dynamic_cast<const LightApp_DataOwner*>( anOwnersList[ i ].get() );
    QStringList es = owner->entry().split( "_" );
    if ( es.count() > 1 && es[ 0 ] == "ATOMICGUI" && es[ 1 ] != "root" )
    {
      entries.append( owner->entry() );
      if( !multiple )
        break;
    }
  }
}

/*! Instantiation of a custom Data Model. */
CAM_DataModel* ATOMICGUI::createDataModel()
{
  return new ATOMICGUI_DataModel( this );
}

/*! Activation function. Called on every activation of a GUI module. */
bool ATOMICGUI::activateModule ( SUIT_Study* study )
{
  bool isDone = LightApp_Module::activateModule( study );
  if ( !isDone ) return false;

  setMenuShown( true );
  setToolShown( true );

  return isDone;
}

/*! Deactivation function. Called on every deactivation of a GUI module. */
bool ATOMICGUI::deactivateModule ( SUIT_Study* study )
{
  setMenuShown( false );
  setToolShown( false );

  return LightApp_Module::deactivateModule( study );
}

/*! slot connected to all functions of the component (create molecule, add atom, etc.) */
void ATOMICGUI::onOperation()
{
  if( sender() && sender()->inherits( "QAction" ) )
  {
    int id = actionId( ( QAction* )sender() );
    if ( id == agCreateMol ) {
      ATOMICGUI_DataModel* dm = dynamic_cast<ATOMICGUI_DataModel*>( dataModel() );
      if ( dm ) {
        dm->createMolecule();
        getApp()->updateObjectBrowser();
      }
    }
    else if ( id == agAddAtom ) {
      QStringList entries;
      selected( entries, false );
      ATOMICGUI_AddAtomDlg dlg ( getApp()->desktop() );
      int res = dlg.exec();
      ATOMICGUI_DataModel* dm = dynamic_cast<ATOMICGUI_DataModel*>( dataModel() );
      if( dm && res == QDialog::Accepted && dlg.acceptData( entries ) ) {
        QString name;
        double x, y, z;
        dlg.data( name, x, y, z );
        dm->addAtom( entries.first(), name, x, y, z );
        getApp()->updateObjectBrowser();
      }
    }
  }
}

/*! Called on popup menu request by LightApp_Application. */
void ATOMICGUI::contextMenuPopup( const QString& client, QMenu* menu, QString& /*title*/ )
{
  if ( client == "ObjectBrowser" ) {
    QStringList entries;
    selected( entries, false );
    if ( entries.size() ) {
      QStringList es = entries.first().split( "_" );
      if ( es.count() == 2 && es[ 0 ] == "ATOMICGUI" ) { // selected object belongs to ATOMICGUI
                                                         // and it is a molecule object
        menu->addAction(action( agAddAtom ));
      }
    }
  }
}


#if defined(WNT)
#define ATOMICGUI_EXPORT __declspec(dllexport)
#else   // WNT
#define ATOMICGUI_EXPORT
#endif  // WNT

/*! GUI module instantiation function */
extern "C" {
  ATOMICGUI_EXPORT CAM_Module* createModule() {
    return new ATOMICGUI();
  }
}
