// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "VISU_Engine_i.hh"
#include "utilities.h"

#include CORBA_SERVER_HEADER(SALOME_ModuleCatalog)

#include "SALOME_NamingService.hxx"
#include "Utils_ExceptHandlers.hxx"

using namespace std;

#ifdef _DEBUG_
static int MYDEBUG = 1;
#else
static int MYDEBUG = 0;
#endif

#ifdef WNT
#if defined VISU_ENGINE_EXPORTS || defined VISUEngine_EXPORTS
#define VISU_ENGINE_EXPORT __declspec(dllexport)
#else
#define VISU_ENGINE_EXPORT __declspec(dllimport)
#endif
#else
#define VISU_ENGINE_EXPORT
#endif

UNEXPECT_CATCH(SalomeException, SALOME::SALOME_Exception);

extern "C" {
VISU_ENGINE_EXPORT
  PortableServer::ObjectId *
  VISUEngine_factory(CORBA::ORB_ptr orb, PortableServer::POA_ptr poa, PortableServer::ObjectId * contId,
                     const char *instanceName, const char *interfaceName)
  {
    MESSAGE("VisuEngine_factory : "<<interfaceName);
    // Check session: MZN: 24.11.2006 PAL 13948
    SALOME_NamingService aNamingService(orb);
    CORBA::Object_ptr anObject = aNamingService.Resolve("/Kernel/Session");
    SALOME::Session_var aSession = SALOME::Session::_narrow(anObject);
    if (CORBA::is_nil(aSession))
      return NULL;

    VISU::VISU_Gen_i * pVISU_Gen = new VISU::VISU_Gen_i(aSession, orb, poa, contId, instanceName, interfaceName);
    return pVISU_Gen->getId() ;
  }
}

namespace VISU{
  //===========================================================================
  VISU_Gen_i::VISU_Gen_i(SALOME::Session_ptr session,
                         CORBA::ORB_ptr orb,
                         PortableServer::POA_ptr poa,
                         PortableServer::ObjectId * contId,
                         const char *instanceName,
                         const char *interfaceName) :
    Engines_Component_i(orb, poa, contId, instanceName, interfaceName)
  {
    _thisObj = this ;
    _id = _poa->activate_object(_thisObj);
#ifndef WIN32
    Engines::EngineComponent_var aComponent = session->GetComponent("libVISUEngineImpl.so");
#else
    Engines::EngineComponent_var aComponent = session->GetComponent("VISUEngineImpl.dll");
#endif
    myVisuGen = VISU::VISU_Gen::_narrow(aComponent);
  }

  VISU_Gen_i::~VISU_Gen_i(){
    if(MYDEBUG) MESSAGE("VISU_Gen_i::~VISU_Gen_i");
  }

  VISU::VISUType VISU_Gen_i::GetType() {
        return myVisuGen->GetType();
  };

  //===========================================================================
  bool VISU_Gen_i::Load(SALOMEDS::SComponent_ptr theComponent,
                        const SALOMEDS::TMPFile & theStream,
                        const char* theURL,
                        bool isMultiFile)
  {
    return myVisuGen->Load(theComponent,theStream,theURL,isMultiFile);
  }

  bool VISU_Gen_i::LoadASCII(SALOMEDS::SComponent_ptr theComponent,
                             const SALOMEDS::TMPFile & theStream,
                             const char* theURL,
                             bool isMultiFile)
  {
    return Load(theComponent, theStream, theURL, isMultiFile);
  }

  char* VISU_Gen_i::LocalPersistentIDToIOR(SALOMEDS::SObject_ptr theSObject,
                                           const char* aLocalPersistentID,
                                           CORBA::Boolean isMultiFile,
                                           CORBA::Boolean isASCII)
  {
    return myVisuGen->LocalPersistentIDToIOR(theSObject, aLocalPersistentID, isMultiFile, isASCII);
  }


  //===========================================================================
  SALOMEDS::TMPFile* VISU_Gen_i::Save(SALOMEDS::SComponent_ptr theComponent,
                                      const char* theURL,
                                      bool isMultiFile)
  {
    return myVisuGen->Save(theComponent,theURL,isMultiFile);
  }

  SALOMEDS::TMPFile* VISU_Gen_i::SaveASCII(SALOMEDS::SComponent_ptr theComponent,
                                           const char* theURL,
                                           bool isMultiFile)
  {
    return myVisuGen->Save(theComponent,theURL,isMultiFile);
  }


  char* VISU_Gen_i::IORToLocalPersistentID(SALOMEDS::SObject_ptr theSObject,
                                           const char* IORString,
                                           CORBA::Boolean isMultiFile,
                                           CORBA::Boolean isASCII)
  {
    return myVisuGen->IORToLocalPersistentID(theSObject, IORString, isMultiFile, isASCII);
  }


  char* VISU_Gen_i::GetID(){
    return myVisuGen->GetID();
  }


  void VISU_Gen_i::SetCurrentStudy(SALOMEDS::Study_ptr theStudy){
    myVisuGen->SetCurrentStudy(theStudy);
  }


  SALOMEDS::Study_ptr VISU_Gen_i::GetCurrentStudy(){
    return myVisuGen->GetCurrentStudy();
  }


  ViewManager_ptr VISU_Gen_i::GetViewManager(){
    return myVisuGen->GetViewManager();
  }


  SALOMEDS::SObject_ptr VISU_Gen_i::ImportTables(const char* theFileName,
                                                 bool theFirstStrAsTitle)
  {
    return myVisuGen->ImportTables(theFileName,theFirstStrAsTitle);
  }


  CORBA::Boolean VISU_Gen_i::ExportTableToFile(SALOMEDS::SObject_ptr theTable,
                                               const char* theFileName)
  {
    return myVisuGen->ExportTableToFile(theTable, theFileName);
  }


  Result_ptr VISU_Gen_i::ImportFile(const char* theFileName){
    return myVisuGen->ImportFile(theFileName);
  }


  Result_ptr VISU_Gen_i::CreateResult(const char* theFileName){
    return myVisuGen->CreateResult(theFileName);
  }


  Result_ptr VISU_Gen_i::CopyAndImportFile(const char* theFileName){
    return myVisuGen->CopyAndImportFile(theFileName);
  }


  Result_ptr VISU_Gen_i::ImportMed(SALOMEDS::SObject_ptr theMedSObject){
    return myVisuGen->ImportMed(theMedSObject);
  }


  Result_ptr VISU_Gen_i::ImportMedField(SALOME_MED::FIELD_ptr theField){
    return myVisuGen->ImportMedField(theField);
  }


  Mesh_ptr VISU_Gen_i::MeshOnEntity(Result_ptr theResult,
                                    const char* theMeshName,
                                    VISU::Entity theEntity)
  {
    return myVisuGen->MeshOnEntity(theResult,theMeshName,theEntity);
  }

  Mesh_ptr VISU_Gen_i::FamilyMeshOnEntity(Result_ptr theResult,
                                          const char* theMeshName,
                                          VISU::Entity theEntity,
                                          const char* theFamilyName)
  {
    return myVisuGen->FamilyMeshOnEntity(theResult,theMeshName,theEntity,theFamilyName);
  }

  Mesh_ptr VISU_Gen_i::GroupMesh(Result_ptr theResult,
                                 const char* theMeshName,
                                 const char* theGroupName)
  {
    return myVisuGen->GroupMesh(theResult,theMeshName,theGroupName);
  }


  void VISU_Gen_i::RenameEntityInStudy(Result_ptr theResult, const char* theMeshName,
                                       VISU::Entity theEntity, const char* theNewName)
  {
    myVisuGen->RenameEntityInStudy(theResult,theMeshName,theEntity,theNewName);
  }

  void VISU_Gen_i::RenameFamilyInStudy(Result_ptr theResult, const char* theMeshName,
                                       VISU::Entity theEntity, const char* theFamilyName,
                                       const char* theNewName)
  {
    myVisuGen->RenameFamilyInStudy(theResult,theMeshName,theEntity,theFamilyName,theNewName);
  }

  void VISU_Gen_i::RenameGroupInStudy(Result_ptr theResult, const char* theMeshName,
                                      const char* theGroupName, const char* theNewName)
  {
    myVisuGen->RenameGroupInStudy(theResult,theMeshName,theGroupName,theNewName);
  }


  ScalarMap_ptr
  VISU_Gen_i
  ::ScalarMapOnField(Result_ptr theResult,
                     const char* theMeshName,
                     VISU::Entity theEntity,
                     const char* theFieldName,
                     CORBA::Long theIteration)
  {
    return myVisuGen->ScalarMapOnField(theResult,theMeshName,theEntity,theFieldName,theIteration);
  }


  GaussPoints_ptr
  VISU_Gen_i
  ::GaussPointsOnField(Result_ptr theResult,
                       const char* theMeshName,
                       VISU::Entity theEntity,
                       const char* theFieldName,
                       CORBA::Long theIteration)
  {
    return myVisuGen->GaussPointsOnField(theResult,theMeshName,theEntity,theFieldName,theIteration);
  }


  DeformedShape_ptr
  VISU_Gen_i
  ::DeformedShapeOnField(Result_ptr theResult,
                         const char* theMeshName,
                         VISU::Entity theEntity,
                         const char* theFieldName,
                         CORBA::Long theIteration)
  {
    return myVisuGen->DeformedShapeOnField(theResult,theMeshName,theEntity,theFieldName,theIteration);
  }

  DeformedShapeAndScalarMap_ptr
  VISU_Gen_i
  ::ScalarMapOnDeformedShapeOnField(Result_ptr theResult,
                                    const char* theMeshName,
                                    VISU::Entity theEntity,
                                    const char* theFieldName,
                                    CORBA::Long theIteration)
  {
    return DeformedShapeAndScalarMapOnField(theResult,theMeshName,theEntity,theFieldName,theIteration);
  }

  DeformedShapeAndScalarMap_ptr
  VISU_Gen_i
  ::DeformedShapeAndScalarMapOnField(Result_ptr theResult,
                                     const char* theMeshName,
                                     VISU::Entity theEntity,
                                     const char* theFieldName,
                                     CORBA::Long theIteration)
  {
    return myVisuGen->DeformedShapeAndScalarMapOnField(theResult,theMeshName,theEntity,theFieldName,theIteration);
  }

  Vectors_ptr
  VISU_Gen_i
  ::VectorsOnField(Result_ptr theResult,
                   const char* theMeshName,
                   VISU::Entity theEntity,
                   const char* theFieldName,
                   CORBA::Long theIteration)
  {
    return myVisuGen->VectorsOnField(theResult,theMeshName,theEntity,theFieldName,theIteration);
  }

  IsoSurfaces_ptr
  VISU_Gen_i
  ::IsoSurfacesOnField(Result_ptr theResult,
                       const char* theMeshName,
                       VISU::Entity theEntity,
                       const char* theFieldName,
                       CORBA::Long theIteration)
  {
    return myVisuGen->IsoSurfacesOnField(theResult,theMeshName,theEntity,theFieldName,theIteration);
  }


  StreamLines_ptr
  VISU_Gen_i
  ::StreamLinesOnField(Result_ptr theResult,
                       const char* theMeshName,
                       VISU::Entity theEntity,
                       const char* theFieldName,
                       CORBA::Long theIteration)
  {
    return myVisuGen->StreamLinesOnField(theResult,theMeshName,theEntity,theFieldName,theIteration);
  }


  CutPlanes_ptr
  VISU_Gen_i
  ::CutPlanesOnField(Result_ptr theResult,
                     const char* theMeshName,
                     VISU::Entity theEntity,
                     const char* theFieldName,
                     CORBA::Long theIteration)
  {
    return myVisuGen->CutPlanesOnField(theResult,theMeshName,theEntity,theFieldName,theIteration);
  }


  CutLines_ptr
  VISU_Gen_i
  ::CutLinesOnField(Result_ptr theResult,
                    const char* theMeshName,
                    VISU::Entity theEntity,
                    const char* theFieldName,
                    CORBA::Long theIteration)
  {
    return myVisuGen->CutLinesOnField(theResult,theMeshName,theEntity,theFieldName,theIteration);
  }


  CutSegment_ptr
  VISU_Gen_i
  ::CutSegmentOnField(Result_ptr theResult,
                      const char* theMeshName,
                      VISU::Entity theEntity,
                      const char* theFieldName,
                      CORBA::Long theIteration)
  {
    return myVisuGen->CutSegmentOnField(theResult,theMeshName,theEntity,theFieldName,theIteration);
  }


  Plot3D_ptr
  VISU_Gen_i
  ::Plot3DOnField(Result_ptr theResult,
                  const char* theMeshName,
                  VISU::Entity theEntity,
                  const char* theFieldName,
                  CORBA::Long theIteration)
  {
    return myVisuGen->Plot3DOnField(theResult,theMeshName,theEntity,theFieldName,theIteration);
  }

  Table_ptr VISU_Gen_i::CreateTable(const char* theTableEntry){
    return myVisuGen->CreateTable(theTableEntry);
  }


  Curve_ptr VISU_Gen_i::CreateCurve(Table_ptr theTable,
                                    CORBA::Long theHRow,
                                    CORBA::Long theVRow)
  {
    return myVisuGen->CreateCurve(theTable,theHRow,theVRow);
  }

  Curve_ptr VISU_Gen_i::CreateCurveWithZ(Table_ptr theTable,
                                    CORBA::Long theHRow,
                                    CORBA::Long theVRow,
                                    CORBA::Long theZRow)
  {
    return myVisuGen->CreateCurveWithZ(theTable,theHRow,theVRow,theZRow);
  }

  Curve_ptr  VISU_Gen_i::CreateCurveWithZExt(Table_ptr theTable,
                                             CORBA::Long theHRow,
                                             CORBA::Long theVRow,
                                             CORBA::Long theZRow,
                                             CORBA::Boolean theIsV2)
  {
    return myVisuGen->CreateCurveWithZExt(theTable,theHRow,theVRow,theZRow,theIsV2);
  }

  Container_ptr VISU_Gen_i::CreateContainer(){
    return myVisuGen->CreateContainer();
  }


  Animation_ptr VISU_Gen_i::CreateAnimation(View3D_ptr theView3D){
    return myVisuGen->CreateAnimation(theView3D);
  }


  Evolution_ptr VISU_Gen_i::CreateEvolution(XYPlot_ptr theXYPlot){
    return myVisuGen->CreateEvolution(theXYPlot);
  }


  void VISU_Gen_i::DeleteResult(Result_ptr theResult){
    myVisuGen->DeleteResult(theResult);
  }


  void VISU_Gen_i::DeletePrs3d(Prs3d_ptr thePrs3d){
    myVisuGen->DeletePrs3d(thePrs3d);
  }


  void VISU_Gen_i::Close(SALOMEDS::SComponent_ptr theComponent){
    myVisuGen->Close(theComponent);
  }


  char* VISU_Gen_i::ComponentDataType(){
    return CORBA::string_dup("VISU");
  }


  CORBA::Boolean VISU_Gen_i::CanPublishInStudy(CORBA::Object_ptr theIOR) {
    return myVisuGen->CanPublishInStudy(theIOR);
  }


  SALOMEDS::SObject_ptr VISU_Gen_i::PublishInStudy(SALOMEDS::Study_ptr theStudy,
                                                   SALOMEDS::SObject_ptr theSObject,
                                                   CORBA::Object_ptr theObject,
                                                   const char* theName)
    throw (SALOME::SALOME_Exception)
  {
    return myVisuGen->PublishInStudy(theStudy, theSObject, theObject, theName);
  }


  CORBA::Boolean VISU_Gen_i::CanCopy(SALOMEDS::SObject_ptr theObject) {
    return myVisuGen->CanCopy(theObject);
  }


  SALOMEDS::TMPFile* VISU_Gen_i::CopyFrom(SALOMEDS::SObject_ptr theObject, CORBA::Long& theObjectID) {
    return myVisuGen->CopyFrom(theObject, theObjectID);
  }


  CORBA::Boolean VISU_Gen_i::CanPaste(const char* theComponentName, CORBA::Long theObjectID) {
    return myVisuGen->CanPaste(theComponentName, theObjectID);
  }


  SALOMEDS::SObject_ptr VISU_Gen_i::PasteInto(const SALOMEDS::TMPFile& theStream,
                                              CORBA::Long theObjectID,
                                              SALOMEDS::SObject_ptr theObject)
  {
    return myVisuGen->PasteInto(theStream,theObjectID,theObject);
  }
  Engines::TMPFile* VISU_Gen_i::DumpPython(CORBA::Object_ptr theStudy,
                                           CORBA::Boolean theIsPublished,
					   CORBA::Boolean theIsMultiFile,
                                           CORBA::Boolean& theIsValidScript)
  {
    return myVisuGen->DumpPython(theStudy, theIsPublished, theIsMultiFile, theIsValidScript);
  }

  VISU::ColoredPrs3dCache_ptr
  VISU_Gen_i::
  GetColoredPrs3dCache(SALOMEDS::Study_ptr theStudy)
  {
    return myVisuGen->GetColoredPrs3dCache(theStudy);
  }



  CORBA::Long VISU_Gen_i::CreateClippingPlane(CORBA::Double X,CORBA::Double  Y, CORBA::Double Z,
                                              CORBA::Double dX, CORBA::Double dY, CORBA::Double dZ,
                                              CORBA::Boolean isAuto, const char* name)
  {
    return myVisuGen->CreateClippingPlane(X, Y, Z, dX, dY, dZ, isAuto, name);
  }


  void VISU_Gen_i::EditClippingPlane(CORBA::Long id, CORBA::Double X,CORBA::Double  Y, CORBA::Double Z,
                                     CORBA::Double dX, CORBA::Double dY, CORBA::Double dZ,
                                     CORBA::Boolean isAuto, const char* name)
  {
    myVisuGen->EditClippingPlane(id, X, Y, Z, dX, dY, dZ, isAuto, name);
  }

  /* Returns clipping plane by its Id */
  VISU::ClippingPlane* VISU_Gen_i::GetClippingPlane(CORBA::Long id)
  {
    return myVisuGen->GetClippingPlane(id);
  }

  /* Deletes clipping plane by its Id */
  CORBA::Boolean VISU_Gen_i::DeleteClippingPlane(CORBA::Long id)
  {
    return myVisuGen->DeleteClippingPlane(id);
  }

  /* Applyes a clipping plane with Id to presentation thePrs */
  CORBA::Boolean VISU_Gen_i::ApplyClippingPlane(Prs3d_ptr thePrs, CORBA::Long id)
  {
    return myVisuGen->ApplyClippingPlane(thePrs, id);
  }

  CORBA::Boolean VISU_Gen_i::DetachClippingPlane(Prs3d_ptr thePrs, CORBA::Long id)
  {
    return myVisuGen->DetachClippingPlane(thePrs, id);
  }

  /* Get number of clipping planes */
  CORBA::Long VISU_Gen_i::GetClippingPlanesNb()
  {
    return myVisuGen->GetClippingPlanesNb();
  }

  CORBA::Boolean VISU_Gen_i::VTK2MED( const VISU::string_array& theVTKFiles,
                                      const char* theMEDFile,
                                      const char* theMeshName,
                                      const VISU::double_array& theTStamps )
  {
    return myVisuGen->VTK2MED(theVTKFiles, theMEDFile, theMeshName, theTStamps);
  }

  /* Load texture from file */
  CORBA::Long VISU_Gen_i::LoadTexture(const char* theTextureFile)
  {
    return myVisuGen->LoadTexture(theTextureFile);
  }
};
