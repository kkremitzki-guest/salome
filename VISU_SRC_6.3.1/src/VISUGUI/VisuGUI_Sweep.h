// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU VISUGUI : GUI of VISU component
//  File   : VisuGUI_Sweep.h
//  Author : Oleg UVAROV
//  Module : VISU
//
#ifndef VisuGUI_Sweep_HeaderFile
#define VisuGUI_Sweep_HeaderFile

#include "SALOME_GenericObjPointer.hh"

#include <vtkSmartPointer.h>

#include <QWidget>
#include <QPixmap>

class QComboBox;
class QToolButton;
class QSlider;
class QCheckBox;
class QMainWindow;
class QTimer;
class QAction;

class SalomeApp_IntSpinBox;
class SalomeApp_DoubleSpinBox;

class LightApp_SelectionMgr;
class SVTK_ViewWindow;
class VISU_Actor;
class VisuGUI;


namespace VISU
{
  class ColoredPrs3d_i;
}


//----------------------------------------------------------------------------
class VisuGUI_Sweep : public QWidget
{
  Q_OBJECT;

public:
  VisuGUI_Sweep( VisuGUI* theModule, 
                 QMainWindow* theParent,
                 LightApp_SelectionMgr* theSelectionMgr );

  virtual ~VisuGUI_Sweep();

  QAction* toggleViewAction();

public slots:
  virtual void     onSelectionChanged();

  virtual void     onFirst();

  virtual void     onPrevious();

  virtual void     onPlay( bool );

  virtual void     onNext();

  virtual void     onLast();

  virtual void     onStop();

  virtual void     onEnable( bool );

  virtual void     onValueChanged( int );

  virtual void     onDelayChanged( double );

  virtual void     onNumberOfStepsChanged( int );

  virtual void     onModeChanged( int );

  virtual void     onTimeout();

  virtual void     onToggleView( bool );

  void             onModuleDeactivated();

  void             onModuleActivated();

private:
  QSlider*          mySweepSlider;

  QToolButton*      myFirstButton;
  QToolButton*      myPreviousButton;
  QToolButton*      myPlayButton;
  QToolButton*      myNextButton;
  QToolButton*      myLastButton;

  QCheckBox*        myIsCycled;

  QComboBox*        mySweepMode;
  SalomeApp_IntSpinBox* myNumberOfSteps;

  QComboBox*        myIntervals;
  SalomeApp_DoubleSpinBox*  myStepDelay;

  QTimer*           myTimer;
  QPixmap           myPlayPixmap;
  QPixmap           myPausePixmap;
  QAction*          myToggleViewAction;

  VisuGUI*          myModule;
  SVTK_ViewWindow*  myViewWindow;

  VISU_Actor*       myActor;
  SALOME::GenericObjPtr< VISU::ColoredPrs3d_i > myColoredPrs3d;
};


//----------------------------------------------------------------------------


#endif
