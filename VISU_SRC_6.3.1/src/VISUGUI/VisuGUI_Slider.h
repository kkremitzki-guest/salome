// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU VISUGUI : GUI of VISU component
//  File   : VisuGUI_Slider.h
//  Author : Oleg UVAROV
//  Module : VISU
//
#ifndef VisuGUI_Slider_HeaderFile
#define VisuGUI_Slider_HeaderFile

#include <QWidget>
#include <QPixmap>

#include <vector>

#include "SALOMEconfig.h"
#include CORBA_SERVER_HEADER(VISU_Gen)

namespace VISU
{
  class ColoredPrs3dHolder_i;
}

class QComboBox;
class QLabel;
class QLineEdit;
class QToolButton;
class QSlider;
class QCheckBox;
class QRadioButton;
class QMainWindow;
class QTimer;
class QAction;

class SalomeApp_DoubleSpinBox;
class LightApp_SelectionMgr;
class VisuGUI;

class VisuGUI_Slider : public QWidget
{
  Q_OBJECT;

public:
  VisuGUI_Slider( VisuGUI* theModule, 
                  QMainWindow* theParent,
                  LightApp_SelectionMgr* theSelectionMgr );

  virtual ~VisuGUI_Slider();
  
  QAction* toggleViewAction();

public slots:
  virtual void     onSelectionChanged();

  virtual void     onTimeStampActivated( int );

  virtual void     onFirst();
  virtual void     onPrevious();
  virtual void     onPlay( bool );
  virtual void     onNext();
  virtual void     onLast();

  virtual void     onValueChanged( int );

  virtual void     onSpeedChanged( int );

  virtual void     onTimeout();

  virtual void     onToggleView( bool );

  void onMemoryModeChanged( bool );
  void onMemorySizeChanged( double );

protected slots:
  void onModuleDeactivated();
  void onModuleActivated();

protected:
  virtual void     enableControls( bool );
  virtual void     updateMemoryState();
  virtual bool     checkHolderList();

private:
  LightApp_SelectionMgr* mySelectionMgr;
  VISU::ViewManager_var  myViewManager;
  VisuGUI*               myModule;

  QSlider*         mySlider;
  QLabel*          myFirstTimeStamp;
  QLabel*          myLastTimeStamp;

  QWidget* myPlayTab;

  QToolButton*     myIsCycled;
  QToolButton*     myFirstButton;
  QToolButton*     myPreviousButton;
  QToolButton*     myPlayButton;
  QToolButton*     myNextButton;
  QToolButton*     myLastButton;

  QComboBox*       myTimeStampStrings;
  QComboBox*       myTimeStampIndexes;

  QSlider*         mySpeedSlider;

  QRadioButton*     myMinimalMemoryButton;
  QRadioButton*     myLimitedMemoryButton;
  SalomeApp_DoubleSpinBox*  myLimitedMemory;

  QLineEdit*      myUsedMemory;
  QLineEdit*      myFreeMemory;

  typedef std::vector<VISU::ColoredPrs3dHolder_var> THolderList;
  THolderList     myHolderList;
  
  QTimer*         myTimer;
  QPixmap         myPlayPixmap;
  QPixmap         myPausePixmap;
  QAction*        myToggleViewAction;
};

#endif
