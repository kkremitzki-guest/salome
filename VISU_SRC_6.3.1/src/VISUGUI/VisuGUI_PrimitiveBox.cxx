// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU VISUGUI : GUI of VISU component
//  File   : VisuGUI_PrimitiveBox.cxx
//  Author : Oleg UVAROV
//  Module : VISU
//
#include "VisuGUI_PrimitiveBox.h"
#include "VisuGUI_Tools.h"

#include <VISU_OpenGLPointSpriteMapper.hxx>

#include <SUIT_ResourceMgr.h>
#include <SUIT_Session.h>

#include <SalomeApp_DoubleSpinBox.h>
#include <SalomeApp_IntSpinBox.h>

#include <QLayout>
#include <QLabel>
#include <QLineEdit>
#include <QButtonGroup>
#include <QRadioButton>
#include <QPushButton>
#include <QFileDialog>

#include <iostream>

using namespace std;

VisuGUI_PrimitiveBox::VisuGUI_PrimitiveBox( QWidget* parent ) :
  QGroupBox( parent )
{
  SUIT_ResourceMgr* aResourceMgr = SUIT_Session::session()->resourceMgr();

  setTitle( tr( "PRIMITIVE_TITLE" ) );
  //setColumnLayout(0, Qt::Vertical );
  //layout()->setSpacing( 0 );
  //layout()->setMargin( 0 );

  QGridLayout* aLayout = new QGridLayout( this );
  aLayout->setAlignment(Qt::AlignTop | Qt::AlignCenter);
  aLayout->setSpacing(6);
  aLayout->setMargin(11);

  myPrimitiveType = VISU_OpenGLPointSpriteMapper::PointSprite;

  QString aRootDir = QString( getenv( "VISU_ROOT_DIR") ) + "/share/salome/resources/visu/";
  myMainTexture = aRootDir + "sprite_texture.bmp";
  myAlphaTexture = aRootDir + "sprite_alpha.bmp";

  // Primitive Type
  QGroupBox* aPrimitiveTypeGroup = new QGroupBox( this );
  aPrimitiveTypeGroup->setMinimumWidth( 450 );
  QHBoxLayout* aPrimLayout = new QHBoxLayout( aPrimitiveTypeGroup );
  aPrimLayout->setMargin( 0 );

  //QButtonGroup* aPrimitiveTypeGroup = new QButtonGroup( 3, Qt::Horizontal, this, "PrimitiveTypeGroup" );
  //aPrimitiveTypeGroup->setMinimumWidth( 450 );
  //aPrimitiveTypeGroup->setRadioButtonExclusive( true );
  //aPrimitiveTypeGroup->setFrameStyle( QFrame::NoFrame );
  //aPrimitiveTypeGroup->layout()->setMargin( 0 );

  myPointSpriteButton = new QRadioButton( tr( "POINT_SPRITE" ), aPrimitiveTypeGroup );
  myOpenGLPointButton = new QRadioButton( tr( "OPENGL_POINT" ), aPrimitiveTypeGroup );
  myGeomSphereButton = new QRadioButton( tr( "GEOMETRICAL_SPHERE" ), aPrimitiveTypeGroup );

  aPrimLayout->addWidget( myPointSpriteButton );
  aPrimLayout->addWidget( myOpenGLPointButton );
  aPrimLayout->addWidget( myGeomSphereButton );

  aLayout->addWidget( aPrimitiveTypeGroup, 0, 0, 1, 2 );

  // Clamp ( Point Sprite & OpenGL Point )
  myClampLabel = new QLabel( tr( "CLAMP" ), this );
  myClampSpinBox = new SalomeApp_DoubleSpinBox( this );
  VISU::initSpinBox( myClampSpinBox, 1.0, 512.0, 1.0, "parametric_precision" );
  myClampSpinBox->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );

  aLayout->addWidget( myClampLabel, 1, 0 );
  aLayout->addWidget( myClampSpinBox, 1, 1, 1, 2 );

  // Main Texture ( Point Sprite )
  myMainTextureLabel = new QLabel( tr( "MAIN_TEXTURE" ), this );
  myMainTextureLineEdit = new QLineEdit( this );
  myMainTextureButton = new QPushButton( this );
  myMainTextureButton->setAutoDefault( false );
  myMainTextureButton->setIcon( aResourceMgr->loadPixmap( "VISU", tr( "ICON_LOAD_TEXTURE" ) ) );
  connect( myMainTextureButton, SIGNAL( clicked() ), this, SLOT( onBrowseMainTexture() ) );

  aLayout->addWidget( myMainTextureLabel, 2, 0 );
  aLayout->addWidget( myMainTextureLineEdit, 2, 1 );
  aLayout->addWidget( myMainTextureButton, 2, 2 );

  // Alpha Texture ( Point Sprite )
  myAlphaTextureLabel = new QLabel( tr( "ALPHA_TEXTURE" ), this );
  myAlphaTextureLineEdit = new QLineEdit( this );
  myAlphaTextureButton = new QPushButton( this );
  myAlphaTextureButton->setAutoDefault( false );
  myAlphaTextureButton->setIcon( aResourceMgr->loadPixmap( "VISU", tr( "ICON_LOAD_TEXTURE" ) ) );
  connect( myAlphaTextureButton, SIGNAL( clicked() ), this, SLOT( onBrowseAlphaTexture() ) );

  aLayout->addWidget( myAlphaTextureLabel, 3, 0 );
  aLayout->addWidget( myAlphaTextureLineEdit, 3, 1 );
  aLayout->addWidget( myAlphaTextureButton, 3, 2 );

  // Alpha Threshold ( Point Sprite )
  myAlphaThresholdLabel = new QLabel( tr( "ALPHA_THRESHOLD" ), this );
  myAlphaThresholdSpinBox = new SalomeApp_DoubleSpinBox( this );
  VISU::initSpinBox( myAlphaThresholdSpinBox,  0.0, 1.0, 0.1, "parametric_precision" );  
  myAlphaThresholdSpinBox->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );

  aLayout->addWidget( myAlphaThresholdLabel, 4, 0 );
  aLayout->addWidget( myAlphaThresholdSpinBox, 4, 1, 1, 2 );

  // Resolution ( Geometrical Sphere )
  myResolutionLabel = new QLabel( tr( "RESOLUTION" ), this );
  myResolutionSpinBox = new SalomeApp_IntSpinBox( this );
  VISU::initSpinBox( myResolutionSpinBox, 3, 100, 1 );    
  myResolutionSpinBox->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );
  connect( myResolutionSpinBox, SIGNAL( valueChanged( int ) ), this, SLOT( onResolutionChanged( int ) ) );

  aLayout->addWidget( myResolutionLabel, 5, 0 );
  aLayout->addWidget( myResolutionSpinBox, 5, 1, 1, 2 );

  // Number of faces ( Geometrical Sphere )
  myFaceNumberLabel = new QLabel( tr( "FACE_NUMBER" ), this );
  myFaceNumberLineEdit = new QLineEdit( this );
  myFaceNumberLineEdit->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );
  myFaceNumberLineEdit->setEnabled( false );
  //myFaceNumberLineEdit->setReadOnly( true );

  aLayout->addWidget( myFaceNumberLabel, 6, 0 );
  aLayout->addWidget( myFaceNumberLineEdit, 6, 1, 1, 2 );

  // Notification ( Geometrical Sphere )
  myFaceLimitLabel = new QLabel( tr( "FACE_LIMIT" ), this );
  myFaceLimitSpinBox = new SalomeApp_IntSpinBox( this );
  VISU::initSpinBox( myFaceLimitSpinBox, 10, 1000000, 10 );      
  myFaceLimitSpinBox->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );

  aLayout->addWidget( myFaceLimitLabel, 7, 0 );
  aLayout->addWidget( myFaceLimitSpinBox, 7, 1, 1, 2 );

  connect( myPointSpriteButton, SIGNAL( clicked() ), this, SLOT( onTogglePointSprite() ) );
  connect( myOpenGLPointButton, SIGNAL( clicked() ), this, SLOT( onToggleOpenGLPoint() ) );
  connect( myGeomSphereButton,  SIGNAL( clicked() ), this, SLOT( onToggleGeomSphere() ) );
}

void VisuGUI_PrimitiveBox::onTogglePointSprite()
{
  myPrimitiveType = VISU_OpenGLPointSpriteMapper::PointSprite;

  myClampLabel->show();
  myClampSpinBox->show();

  myMainTextureLabel->show();
  myMainTextureLineEdit->show();
  myMainTextureButton->show();

  myAlphaTextureLabel->show();
  myAlphaTextureLineEdit->show();
  myAlphaTextureButton->show();

  myAlphaThresholdLabel->show();
  myAlphaThresholdSpinBox->show();

  myResolutionLabel->hide();
  myResolutionSpinBox->hide();

  myFaceNumberLabel->hide();
  myFaceNumberLineEdit->hide();

  myFaceLimitLabel->hide();
  myFaceLimitSpinBox->hide();
}

void VisuGUI_PrimitiveBox::onToggleOpenGLPoint()
{
  myPrimitiveType = VISU_OpenGLPointSpriteMapper::OpenGLPoint;

  myClampLabel->show();
  myClampSpinBox->show();

  myMainTextureLabel->hide();
  myMainTextureLineEdit->hide();
  myMainTextureButton->hide();

  myAlphaTextureLabel->hide();
  myAlphaTextureLineEdit->hide();
  myAlphaTextureButton->hide();

  myAlphaThresholdLabel->hide();
  myAlphaThresholdSpinBox->hide();

  myResolutionLabel->hide();
  myResolutionSpinBox->hide();

  myFaceNumberLabel->hide();
  myFaceNumberLineEdit->hide();

  myFaceLimitLabel->hide();
  myFaceLimitSpinBox->hide();
}

void VisuGUI_PrimitiveBox::onToggleGeomSphere()
{
  myPrimitiveType = VISU_OpenGLPointSpriteMapper::GeomSphere;

  myClampLabel->hide();
  myClampSpinBox->hide();

  myMainTextureLabel->hide();
  myMainTextureLineEdit->hide();
  myMainTextureButton->hide();

  myAlphaTextureLabel->hide();
  myAlphaTextureLineEdit->hide();
  myAlphaTextureButton->hide();

  myAlphaThresholdLabel->hide();
  myAlphaThresholdSpinBox->hide();

  myResolutionLabel->show();
  myResolutionSpinBox->show();

  myFaceNumberLabel->show();
  myFaceNumberLineEdit->show();

  myFaceLimitLabel->show();
  myFaceLimitSpinBox->show();
}

void VisuGUI_PrimitiveBox::onResolutionChanged( int theResolution )
{
  setFaceNumber( 2 * theResolution * ( theResolution - 2 ) );
}

void VisuGUI_PrimitiveBox::setPrimitiveType( int theType )
{
  myPrimitiveType = theType;

  switch( myPrimitiveType )
  {
    case VISU_OpenGLPointSpriteMapper::PointSprite :
      myPointSpriteButton->setChecked( true );
      onTogglePointSprite();
      break;
    case VISU_OpenGLPointSpriteMapper::OpenGLPoint :
      myOpenGLPointButton->setChecked( true );
      onToggleOpenGLPoint();
      break;
    case VISU_OpenGLPointSpriteMapper::GeomSphere :
      myGeomSphereButton->setChecked( true );
      onToggleGeomSphere();
      break;
    default : break;
  }
}

float VisuGUI_PrimitiveBox::getClamp() const
{
  return myClampSpinBox->value();
}

void VisuGUI_PrimitiveBox::setClamp( float theClamp )
{
  myClampSpinBox->setValue( theClamp );
}

void VisuGUI_PrimitiveBox::setClampMaximum( float theClampMaximum )
{
  myClampSpinBox->setMaximum( theClampMaximum );
}

void VisuGUI_PrimitiveBox::setMainTexture( const QString& theMainTexture )
{
  myMainTexture = theMainTexture;
  myMainTextureLineEdit->setText( theMainTexture.section( '/', -1 ) );
}

void VisuGUI_PrimitiveBox::setAlphaTexture( const QString& theAlphaTexture )
{
  myAlphaTexture = theAlphaTexture;
  myAlphaTextureLineEdit->setText( theAlphaTexture.section( '/', -1 ) );
}

float VisuGUI_PrimitiveBox::getAlphaThreshold() const
{
  return myAlphaThresholdSpinBox->value();
}

void VisuGUI_PrimitiveBox::setAlphaThreshold( float theAlphaThreshold )
{
  myAlphaThresholdSpinBox->setValue( theAlphaThreshold );
}

int VisuGUI_PrimitiveBox::getResolution() const
{
  return myResolutionSpinBox->value();
}

void VisuGUI_PrimitiveBox::setResolution( int theResolution )
{
  myResolutionSpinBox->setValue( theResolution );
}

int VisuGUI_PrimitiveBox::getFaceNumber() const
{
  int aResolution = getResolution();
  return 2 * aResolution * ( aResolution - 2 );
  //return myFaceNumberLineEdit->text().toInt();
}

void VisuGUI_PrimitiveBox::setFaceNumber( int theFaceNumber )
{
  myFaceNumberLineEdit->setText( QString::number( theFaceNumber ) );
}

int VisuGUI_PrimitiveBox::getFaceLimit() const
{
  return myFaceLimitSpinBox->value();
}

void VisuGUI_PrimitiveBox::setFaceLimit( int theFaceLimit )
{
  myFaceLimitSpinBox->setValue( theFaceLimit );
}

void VisuGUI_PrimitiveBox::onBrowseMainTexture()
{
  QString aRootDir = QString( getenv( "VISU_ROOT_DIR") ) + "/share/salome/resources/visu/";
  QString aFileName = QFileDialog::getOpenFileName( this,
                                                    0,
                                                    aRootDir,
                                                    "Bitmap (*.bmp *.jpg *.png)" );

  if( aFileName.isNull() )
    return;

  myMainTexture = aFileName;
  myMainTextureLineEdit->setText( aFileName.section( '/', -1 ) );
}

void VisuGUI_PrimitiveBox::onBrowseAlphaTexture()
{
  QString aRootDir = QString( getenv( "VISU_ROOT_DIR") ) + "/share/salome/resources/visu/";
  QString aFileName = QFileDialog::getOpenFileName( this,
                                                    0,
                                                    aRootDir,
                                                    "Bitmap (*.bmp *.jpg *.png)" );

  if( aFileName.isNull() )
    return;

  myAlphaTexture = aFileName;
  myAlphaTextureLineEdit->setText( aFileName.section( '/', -1 ) );
}
