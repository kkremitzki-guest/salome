// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU VISUGUI : GUI of VISU component
//  File   : VisuGUI_Module.cxx
//  Author : Laurent CORNABE
//  Module : VISU
//  $Header: /home/server/cvs/VISU/VISU_SRC/src/VISUGUI/VisuGUI_Module.cxx,v 1.10.2.7.6.3.2.3 2011-06-03 09:55:40 vsr Exp $
//
#include "VisuGUI_Module.h"

#include "QtxPopupMgr.h"

#include "SUIT_Study.h"
#include "SUIT_Desktop.h"
#include "SUIT_ResourceMgr.h"
#include "SUIT_Accel.h"
#include "SUIT_Session.h"

#include "CAM_Module.h"

#include "SALOME_Event.h"
#include "SalomeApp_Application.h"
#include "LightApp_SelectionMgr.h"
#include "LightApp_VTKSelector.h"
#include "LightApp_Preferences.h"
#include "LightApp_Displayer.h"

#include "SALOMEDSClient_ClientFactory.hxx"
#include "SALOMEDSClient_IParameters.hxx"

//#include "VVTK_ViewManager.h"
//#include "VVTK_ViewWindow.h"
//#include "VVTK_ViewModel.h"

#include "SVTK_ViewModel.h"
#include "SVTK_ViewManager.h"
#include "SVTK_ViewWindow.h"
#include "SVTK_RenderWindowInteractor.h"
#include "VISU_Event.h"

#include "VTKViewer_Algorithm.h"

#include "VisuGUI_Prs3dTools.h"
#include "VisuGUI_ClippingDlg.h"

#include "VISU_GaussPoints_i.hh"
#include "VISU_GaussPtsAct.h"
#include "VisuGUI_GaussPointsDlg.h"

#include "VISU_Gen_i.hh"
#include "VISU_Result_i.hh"
#include "VISU_CutLinesBase_i.hh"

#include "VISU_Actor.h"
#include "VISU_ScalarMapAct.h"
#include "VisuGUI_Tools.h"
#include "VisuGUI_ActionsDef.h"

#include "VISU_WidgetCtrl.hxx"
#include "VISU_PlanesWidget.hxx"
#include "VISU_SphereWidget.hxx"

#include "SalomeApp_Study.h"
//#include "VVTK_MainWindow.h"
#include "VISU_View_i.hh"

#ifndef DISABLE_VTKVIEWER
#ifndef DISABLE_SALOMEOBJECT
  #include <SVTK_ViewWindow.h>
  #include <SVTK_ViewModel.h>
#else
  #include <VTKViewer_ViewWindow.h>
#endif
  #include <VTKViewer_ViewModel.h>
#endif
#ifndef DISABLE_OCCVIEWER
  #include <OCCViewer_ViewWindow.h>
  #include <OCCViewer_ViewPort3d.h>
#ifndef DISABLE_SALOMEOBJECT
  #include <SOCC_ViewModel.h>
#else
  #include <OCCViewer_ViewModel.h>
#endif
#endif
#ifndef DISABLE_GLVIEWER
  #include <GLViewer_ViewFrame.h>
  #include <GLViewer_ViewPort.h>
#endif
#ifndef DISABLE_PLOT2DVIEWER
  #include <Plot2d_ViewWindow.h>
  #include <Plot2d_ViewFrame.h>
#ifndef DISABLE_SALOMEOBJECT
  #include <SPlot2d_ViewModel.h>
#else
  #include <Plot2d_ViewModel.h>
#endif
#endif

#include <QAction>

#include <vtkRenderer.h>
#include <vtkCamera.h>
#include <vtkTimerLog.h>
#include <vtkPlane.h>

#include <sstream>


#ifdef _DEBUG_
static int MYDEBUG = 0;
#else
static int MYDEBUG = 0;
#endif

#define SAVE_VISU_STATE          5090

#define GAUSS_NEW_VIEWER         5100
#define GAUSS_CREATE_PRS         5200
#define GAUSS_RENAME             5210
#define GAUSS_EDIT_PRS           5300
#define GAUSS_COPY_PRS           5310
#define GAUSS_ERASE_PRS          5400
#define GAUSS_DISPLAY_PRS        5500
#define GAUSS_DISPLAY_ONLY_PRS   5600

//#define GAUSS_SAVE_CONFIGURATION       5700
//#define GAUSS_OVERWRITE_CONFIGURATION  5701
//#define GAUSS_RESTORE_CONFIGURATION    5702
//#define GAUSS_RENAME_CONFIGURATION     5703

using namespace std;

void
CreateCurves( SalomeApp_Module* theModule,
              VISU::CutLinesBase_i* thePrs,
              QDialog* theDlg,
              const bool theCreate = true );

using namespace VISU;

namespace VISU
{
  //---------------------------------------------------------------
  typedef void (SUIT_ViewWindow::* TViewVisibility)();
  void SetViewVisibility(SalomeApp_Application* app,
                         TViewVisibility theViewVisibility)
  {
    ViewManagerList l;
    //app->viewManagers( VVTK_Viewer::Type(), l );
    app->viewManagers( SVTK_Viewer::Type(), l );
    ViewManagerList::const_iterator anIt = l.begin(), aLast = l.end();
    for( ; anIt!=aLast; anIt++ )
      if( SUIT_ViewManager* aViewManager = *anIt )
      {
        QVector<SUIT_ViewWindow*> aViews = aViewManager->getViews();
        int aSize = aViews.size();
        for(int anId = 0; anId < aSize; anId++)
        {
          if(SUIT_ViewWindow* aView = aViews[anId])
            (aView->* theViewVisibility)();
        }
      }
  }
}

//---------------------------------------------------------------
VisuGUI_Module::VisuGUI_Module() : 
  VisuGUI(),
  LightApp_Module("VISU")
{
}


//---------------------------------------------------------------
VisuGUI_Module::~VisuGUI_Module()
{
}


//---------------------------------------------------------------
void VisuGUI_Module::initialize( CAM_Application* theApp )
{
  VisuGUI::initialize( theApp );

  SUIT_ResourceMgr* aResourceMgr = SUIT_Session::session()->resourceMgr();

  // "Save VISU State" command is moved from SalomeApp_Applicaiton
  createAction( SAVE_VISU_STATE, tr( "TOT_DESK_FILE_SAVE_GUI_STATE" ), QIcon(),
                tr( "MEN_DESK_FILE_SAVE_GUI_STATE" ), tr( "PRP_DESK_FILE_SAVE_GUI_STATE" ),
                0, getApp()->desktop(), false, getApp(), SLOT( onSaveGUIState() ) );
  int fileMenu = createMenu( tr( "MEN_DESK_FILE" ), -1 );
  createMenu( SAVE_VISU_STATE, fileMenu, 9, -1 );

  QPixmap aPixmap;
  /*  aPixmap = aResourceMgr->loadPixmap("VISU",tr("ICON_GAUSS_NEW_VIEWER"));
  createAction( GAUSS_NEW_VIEWER, 
                tr("MEN_GAUSS_NEW_VIEWER"), 
                aPixmap,
                tr("MEN_GAUSS_NEW_VIEWER"),
                tr("MEN_GAUSS_NEW_VIEWER"),
                Qt::ALT+Qt::Key_S,
                this,
                false,
                this,
                SLOT(onCreateViewManager()));
  int windowMenu = createMenu( tr( "MEN_DESK_WINDOW" ), -1, 100 );
  int newWinMenu = createMenu( tr( "MEN_DESK_NEWWINDOW" ), windowMenu, -1, 0 );
  createMenu( action( GAUSS_NEW_VIEWER ), newWinMenu, -1 );
  */
  // Add actions to menus
  createMenu( tr( "MEN_GAUSS" ), -1, -1, 30 );
  //createMenu( GAUSS_CREATE_PRS, aMenuId, 10 );

  QString aViewerType = SVTK_Viewer::Type();
  SUIT_Accel* accel = getApp()->accel();
  accel->setActionKey( SUIT_Accel::PanLeft, Qt::Key_Left, aViewerType );
  accel->setActionKey( SUIT_Accel::PanRight, Qt::Key_Right, aViewerType );
  accel->setActionKey( SUIT_Accel::PanUp, Qt::Key_Up, aViewerType );
  accel->setActionKey( SUIT_Accel::PanDown, Qt::Key_Down, aViewerType );
  accel->setActionKey( SUIT_Accel::ZoomIn, Qt::Key_PageUp, aViewerType );
  accel->setActionKey( SUIT_Accel::ZoomOut, Qt::Key_PageDown, aViewerType );
  accel->setActionKey( SUIT_Accel::RotateLeft, Qt::CTRL+Qt::Key_Left, aViewerType );
  accel->setActionKey( SUIT_Accel::RotateRight, Qt::CTRL+Qt::Key_Right, aViewerType );
  accel->setActionKey( SUIT_Accel::RotateUp, Qt::CTRL+Qt::Key_Up, aViewerType );
  accel->setActionKey( SUIT_Accel::RotateDown, Qt::CTRL+Qt::Key_Down, aViewerType );
  accel->setActionKey( SVTK::PlusSpeedIncrementEvent, Qt::Key_Plus, aViewerType );
  accel->setActionKey( SVTK::MinusSpeedIncrementEvent, Qt::Key_Minus, aViewerType );
  
  connect( getApp(), SIGNAL( viewManagerAdded( SUIT_ViewManager* ) ),
           this, SLOT( OnViewManagerAdded (SUIT_ViewManager*) ) );
  
  // Prepare popup menus
  QtxPopupMgr* mgr = popupMgr();
  QString aRule;

  aPixmap = aResourceMgr->loadPixmap( "VISU", tr( "ICON_GAUSS_POINTS" ) );
  createAction( GAUSS_CREATE_PRS, tr("MEN_GAUSS_CREATE_PRS"), aPixmap,
                tr("MEN_GAUSS_CREATE_PRS"), "", 0, this, false,
                this, SLOT(OnCreateGaussPoints()));
  mgr->insert( action( GAUSS_CREATE_PRS ), -1, 0, -1 );
  mgr->setRule( action( GAUSS_CREATE_PRS ),
                "client='ObjectBrowser' and selcount=1 "
                "and type in {'VISU::TTIMESTAMP' 'VISU::TFIELD'} "
                "and $medEntity in {'EDGE_ENTITY' 'FACE_ENTITY' 'CELL_ENTITY'} "
                "and $medSource in {'eImportFile' 'eCopyAndImportFile'}" );

  createMenu( action( GAUSS_CREATE_PRS ), createMenu( tr( "MEN_VISUALISATION" ), -1 ), -1 );
  createTool( GAUSS_CREATE_PRS, createTool( tr( "TOOL_VISUALISATION" ) ), -1 );

  /*  
      createAction( GAUSS_RENAME, VisuGUI::tr("MEN_RENAME"), QIcon(),
                VisuGUI::tr("MEN_RENAME"), "", 0, this, false,
                this, SLOT(OnRename()));
		mgr->insert( action( GAUSS_RENAME ), -1, 0, -1 );
		mgr->setRule( action( GAUSS_RENAME ),
		"selcount=1 and type='VISU::TGAUSSPOINTS'" );
  */
  
  createAction( GAUSS_EDIT_PRS, VisuGUI::tr("MEN_EDIT_PRS"), QIcon(),
                VisuGUI::tr("MEN_EDIT_PRS"), "", 0, this, false,
                this, SLOT(OnEditGaussPoints()));
  mgr->insert( action( GAUSS_EDIT_PRS ), -1, 0, -1 );
  mgr->setRule( action( GAUSS_EDIT_PRS ),
    "selcount=1 and type='VISU::TGAUSSPOINTS'" );

  createAction( GAUSS_COPY_PRS, VisuGUI::tr("MEN_COPY_PRS"), QIcon(),
                VisuGUI::tr("MEN_COPY_PRS"), "", 0, this, false,
                this, SLOT(OnCopyPresentation()));
  mgr->insert( action( GAUSS_COPY_PRS ), -1, 0, -1 );
  mgr->setRule( action( GAUSS_COPY_PRS ),
    "selcount=1 and type='VISU::TGAUSSPOINTS'" );

  action( GAUSS_COPY_PRS )->setEnabled(false);
  
  QStringList viewers;

#ifndef DISABLE_OCCVIEWER
#ifndef DISABLE_SALOMEOBJECT
  viewers.append( SOCC_Viewer::Type() );
#else
  viewers.append( OCCViewer_Viewer::Type() );
#endif
#endif
#ifndef DISABLE_VTKVIEWER
#ifndef DISABLE_SALOMEOBJECT
  viewers.append( SVTK_Viewer::Type() );
  //viewers.append( VVTK_Viewer::Type() );
#else
  viewers.append( VTKViewer_Viewer::Type() );
#endif
#endif
#ifndef DISABLE_PLOT2DVIEWER
#ifndef DISABLE_SALOMEOBJECT
  viewers.append( SPlot2d_Viewer::Type() );
#else
  viewers.append( Plot2d_Viewer::Type() );
#endif
#endif

  if( !viewers.isEmpty() )
    {
      QString strViewers = "{ ", temp = "'%1' ";
      QStringList::const_iterator anIt = viewers.begin(), aLast = viewers.end();
      for( ; anIt!=aLast; anIt++ )
        strViewers+=temp.arg( *anIt );
      strViewers+="}";
      mgr->setRule( action(myEraseAll), QString( "client in %1" ).arg( strViewers ) );
    }

  aRule = "(selcount>0 and type='VISU::TGAUSSPOINTS')";

/*  createAction( GAUSS_ERASE_PRS, VisuGUI::tr("MEN_HIDE"), QIcon(),
                VisuGUI::tr("MEN_HIDE"), "", 0, this, false,
                this, SLOT(OnErasePrs()));
  mgr->insert( action( GAUSS_ERASE_PRS ), -1, -1, -1 ); // erase
  mgr->setRule( action( GAUSS_ERASE_PRS ),
    aRule + " and ({true} in $canBeDisplayed) and (isVisible=true)" );

  createAction( GAUSS_DISPLAY_PRS, VisuGUI::tr("MEN_SHOW"), QIcon(),
                VisuGUI::tr("MEN_SHOW"), "", 0, this, false,
                this, SLOT(OnDisplayPrs()));
  mgr->insert( action( GAUSS_DISPLAY_PRS ), -1, -1, -1 ); // display
  mgr->setRule( action( GAUSS_DISPLAY_PRS ),
    aRule + " and ({true} in $canBeDisplayed) and (isVisible=false)" );

  createAction( GAUSS_DISPLAY_ONLY_PRS, VisuGUI::tr("MEN_DISPLAY_ONLY"), QIcon(),
                VisuGUI::tr("MEN_DISPLAY_ONLY"), "", 0, this, false,
                this, SLOT(OnDisplayOnlyPrs()));
  mgr->insert( action( GAUSS_DISPLAY_ONLY_PRS ), -1, -1, -1 ); // display only
  mgr->setRule( action( GAUSS_DISPLAY_ONLY_PRS ),
    aRule + " and ({true} in $canBeDisplayed)" );*/
/*
  createAction( GAUSS_RENAME_CONFIGURATION, VisuGUI::tr("MEN_RENAME"), QIcon(),
                VisuGUI::tr("MEN_RENAME"), "", 0, this, false,
                this, SLOT(OnRename()));
  mgr->insert( action( GAUSS_RENAME_CONFIGURATION ), -1, -1, -1 );
  mgr->setRule( action( GAUSS_RENAME_CONFIGURATION ), "selcount=1 and type='VISU::TGAUSSVIEW'" );

  createAction( GAUSS_SAVE_CONFIGURATION, tr("MEN_SAVE_CONFIGURATION"), QIcon(),
                tr("MEN_SAVE_CONFIGURATION"), "", 0, this, false,
                this, SLOT(OnSaveConfiguration()));
  mgr->insert( action( GAUSS_SAVE_CONFIGURATION ), -1, -1, -1 );
  mgr->setRule( action( GAUSS_SAVE_CONFIGURATION ),
    "selcount>=0 and client='SVTK' and activeView='SVTK'" );

  createAction( GAUSS_OVERWRITE_CONFIGURATION, tr("MEN_OVERWRITE_CONFIGURATION"), QIcon(),
                tr("MEN_OVERWRITE_CONFIGURATION"), "", 0, this, false,
                this, SLOT(OnOverwriteConfiguration()));
  mgr->insert( action( GAUSS_OVERWRITE_CONFIGURATION ), -1, -1, -1 );
  mgr->setRule( action( GAUSS_OVERWRITE_CONFIGURATION ),
    "selcount>0 and type='VISU::TGAUSSVIEW' and activeView='SVTK'" );

  createAction( GAUSS_RESTORE_CONFIGURATION, tr("MEN_RESTORE_CONFIGURATION"), QIcon(),
                tr("MEN_RESTORE_CONFIGURATION"), "", 0, this, false,
                this, SLOT(OnRestoreConfiguration()));
  mgr->insert( action( GAUSS_RESTORE_CONFIGURATION ), -1, -1, -1 );
  mgr->setRule( action( GAUSS_RESTORE_CONFIGURATION ),
  "selcount=1 and type='VISU::TGAUSSVIEW'" );
*/
}

// //---------------------------------------------------------------
// bool VisuGUI_Module::activateModule( SUIT_Study* theStudy )
// {
//   VisuGUI::activateModule( theStudy );

//   //SetViewVisibility(getApp(),&SUIT_ViewWindow::show);

//   return true;
// }

//---------------------------------------------------------------
// bool VisuGUI_Module::deactivateModule( SUIT_Study* theStudy )
// {
//   return VisuGUI::deactivateModule( theStudy );

//   //SetViewVisibility(getApp(),&SUIT_ViewWindow::hide);

//   //return true;
// }


//---------------------------------------------------------------
// SUIT_ViewManager* VisuGUI_Module::onCreateViewManager()
// {
//   SalomeApp_Application* anApp = getApp();
//   SUIT_ResourceMgr* aResourceMgr = anApp->resourceMgr();
//   VVTK_ViewManager* aViewManager = new VVTK_ViewManager( anApp->activeStudy(), anApp->desktop() );
//   VVTK_Viewer* aViewer = (VVTK_Viewer*)aViewManager->getViewModel();
//   aViewer->setBackgroundColor( aResourceMgr->colorValue( "VTKViewer", "background", aViewer->backgroundColor() ) );
//   aViewer->setProjectionMode( aResourceMgr->integerValue( "VTKViewer", "projection_mode", aViewer->projectionMode() ) );
//   aViewer->setTrihedronSize( aResourceMgr->integerValue( "VTKViewer", "trihedron_size", aViewer->trihedronSize() ),
//                           aResourceMgr->booleanValue( "VTKViewer", "relative_size", aViewer->trihedronRelative() ) );
//   aViewer->setIncrementalSpeed( aResourceMgr->integerValue( "VTKViewer", "speed_value", aViewer->incrementalSpeed() ),
//                              aResourceMgr->integerValue( "VTKViewer", "speed_mode", aViewer->incrementalSpeedMode() ) );
//   aViewer->setInteractionStyle( aResourceMgr->integerValue( "VTKViewer", "navigation_mode", aViewer->interactionStyle() ) );
//   aViewer->setSpacemouseButtons( aResourceMgr->integerValue( "VTKViewer", "spacemouse_func1_btn", aViewer->spacemouseBtn(1) ),
//                               aResourceMgr->integerValue( "VTKViewer", "spacemouse_func2_btn", aViewer->spacemouseBtn(1) ),
//                               aResourceMgr->integerValue( "VTKViewer", "spacemouse_func5_btn", aViewer->spacemouseBtn(1) ) );

//   new LightApp_VTKSelector( aViewer, anApp->selectionMgr() );

//   anApp->addViewManager( aViewManager );

//   return aViewer->getViewManager();
// }


//---------------------------------------------------------------
void VisuGUI_Module::createPreferences()
{
  VisuGUI::createPreferences();

  createGaussPointsPreferences();
  //createInsideCursorPreferences();
  //createOutsideCursorPreferences();
  createPickingPreferences();
  // createSpaceMousePreferences();
  //createRecorderPreferences();
}


//---------------------------------------------------------------
void VisuGUI_Module::createGaussPointsPreferences()
{
  int gaussTab = addPreference( tr( "VISU_GAUSS_PREF_TAB_TTL" ) );
  int primitiveGr = addPreference( tr( "VISU_GAUSS_PREF_PRIMITIVE_GROUP_TTL" ), gaussTab );
  setPreferenceProperty( primitiveGr, "columns", 2 );

  int primitiveTypePref = addPreference( tr( "VISU_GAUSS_PREF_PRIMITIVE_TYPE" ), primitiveGr,
                                         LightApp_Preferences::Selector, "VISU",
                                         "point_sprite_primitive_type" );

  QStringList values;
  values.append( tr( "VISU_GAUSS_PREF_POINTSPRITE" ) );
  values.append( tr( "VISU_GAUSS_PREF_OPENGLPOINT" ) );
  values.append( tr( "VISU_GAUSS_PREF_GEOMSPHERE" ) );
  QList<QVariant> indices;
  indices.append( 0 );
  indices.append( 1 );
  indices.append( 2 );
  setPreferenceProperty( primitiveTypePref, "strings", values );
  setPreferenceProperty( primitiveTypePref, "indexes", indices );

  int clampPref = addPreference( tr( "VISU_GAUSS_PREF_CLAMP" ), primitiveGr,
                                 LightApp_Preferences::IntSpin, "VISU", "point_sprite_clamp" );
  setPreferenceProperty( clampPref, "min", 1 );
  setPreferenceProperty( clampPref, "max", 512 );

  addPreference( tr( "VISU_GAUSS_PREF_MAIN_TEXTURE" ), primitiveGr,
                 LightApp_Preferences::File, "VISU", "point_sprite_main_texture" );

  addPreference( tr( "VISU_GAUSS_PREF_ALPHA_TEXTURE" ), primitiveGr,
                 LightApp_Preferences::File, "VISU", "point_sprite_alpha_texture" );

  int alphaThresholdPref = addPreference( tr( "VISU_GAUSS_PREF_ALPHA_THRESHOLD" ), primitiveGr,
                                          LightApp_Preferences::DblSpin, "VISU",
                                          "point_sprite_alpha_threshold" );
  setPreferenceProperty( alphaThresholdPref, "min", 0.0 );
  setPreferenceProperty( alphaThresholdPref, "max", 1.0 );
  setPreferenceProperty( alphaThresholdPref, "step", 0.1 );

  int resolutionPref = addPreference( tr( "VISU_GAUSS_PREF_RESOLUTION" ), primitiveGr,
                                      LightApp_Preferences::IntSpin, "VISU", "geom_sphere_resolution" );
  setPreferenceProperty( resolutionPref, "min", 3 );
  setPreferenceProperty( resolutionPref, "max", 100 );

  int faceLimitPref = addPreference( tr( "VISU_GAUSS_PREF_FACE_LIMIT" ), primitiveGr,
                                     LightApp_Preferences::IntSpin, "VISU", "geom_sphere_face_limit" );
  setPreferenceProperty( faceLimitPref, "min", 10 );
  setPreferenceProperty( faceLimitPref, "max", 1000000 );

  int sizeGr = addPreference( tr( "VISU_GAUSS_PREF_SIZE_GROUP_TTL" ), gaussTab );
  setPreferenceProperty( sizeGr, "columns", 2 );

  int minSizePref = addPreference( tr( "VISU_GAUSS_PREF_MIN_SIZE" ), sizeGr,
                                   LightApp_Preferences::IntSpin, "VISU", "point_sprite_min_size" );
  setPreferenceProperty( minSizePref, "min", 1 );
  setPreferenceProperty( minSizePref, "max", 100 );

  int maxSizePref = addPreference( tr( "VISU_GAUSS_PREF_MAX_SIZE" ), sizeGr,
                                   LightApp_Preferences::IntSpin, "VISU", "point_sprite_max_size" );
  setPreferenceProperty( maxSizePref, "min", 1 );
  setPreferenceProperty( maxSizePref, "max", 100 );

  int magnificationPref = addPreference( tr( "VISU_GAUSS_PREF_MAGNIFICATION" ), sizeGr,
                                         LightApp_Preferences::IntSpin, "VISU",
                                         "point_sprite_magnification" );
  setPreferenceProperty( magnificationPref, "min", 10 );
  setPreferenceProperty( magnificationPref, "max", 1000 );

  int incrementPref = addPreference( tr( "VISU_GAUSS_PREF_INCREMENT" ), sizeGr,
                                     LightApp_Preferences::DblSpin, "VISU", "point_sprite_increment" );
  setPreferenceProperty( incrementPref, "min", 0.01 );
  setPreferenceProperty( incrementPref, "max", 10 );
  setPreferenceProperty( incrementPref, "step", 0.1 );

  int geomGr = addPreference( tr( "VISU_GAUSS_PREF_GEOM_GROUP_TTL" ), gaussTab );
  setPreferenceProperty( geomGr, "columns", 2 );

  int sizePref = addPreference( tr( "VISU_GAUSS_PREF_SIZE" ), geomGr,
                                LightApp_Preferences::IntSpin, "VISU", "point_sprite_size" );
  setPreferenceProperty( sizePref, "min", 1 );
  setPreferenceProperty( sizePref, "max", 100 );

  addPreference( tr( "VISU_GAUSS_PREF_COLOR" ), geomGr,
                 LightApp_Preferences::Color, "VISU", "point_sprite_color" );

  // ScalarBar Preferences
  int scalarBarGr = addPreference( tr( "VISU_GAUSS_SCALAR_BAR_PREF_GROUP_TTL" ), gaussTab );
  setPreferenceProperty( scalarBarGr, "columns", 2 );

  int activeBarPref = addPreference( tr( "VISU_GAUSS_PREF_ACTIVE_BAR" ), scalarBarGr,
                                     LightApp_Preferences::Selector, "VISU", "scalar_bar_active_local" );

  values.clear();
  values.append( tr( "VISU_GAUSS_PREF_LOCAL" ) );
  values.append( tr( "VISU_GAUSS_PREF_GLOBAL" ) );
  indices.clear();
  indices.append( 0 );
  indices.append( 1 );
  setPreferenceProperty( activeBarPref, "strings", values );
  setPreferenceProperty( activeBarPref, "indexes", indices );

  addPreference( tr( "VISU_GAUSS_PREF_DISPLAY_GLOBAL" ), scalarBarGr,
                 LightApp_Preferences::Bool, "VISU", "scalar_bar_display_global" );

  int colorPref = addPreference( tr( "VISU_GAUSS_PREF_SCALAR_BAR_MODE" ), scalarBarGr,
                                 LightApp_Preferences::Selector, "VISU", "scalar_bar_bicolor" );

  values.clear();
  values.append( tr( "VISU_GAUSS_PREF_BICOLOR" ) );
  values.append( tr( "VISU_GAUSS_PREF_RAINBOW" ) );
  indices.clear();
  indices.append( 0 );
  indices.append( 1 );
  setPreferenceProperty( colorPref, "strings", values );
  setPreferenceProperty( colorPref, "indexes", indices );

  int spacingPref = addPreference( tr( "VISU_GAUSS_PREF_SPACING" ), scalarBarGr,
                                     LightApp_Preferences::DblSpin, "VISU", "scalar_bar_spacing" );
  setPreferenceProperty( spacingPref, "min", 0.01 );
  setPreferenceProperty( spacingPref, "max", 1.0 );
  setPreferenceProperty( spacingPref, "step", 0.01 );

  // spacemouse
  int spacemouseGr = addPreference( tr( "VISU_SPACEMOUSE_PREF" ), gaussTab );
  setPreferenceProperty( spacemouseGr, "columns", 2 );
  int spacemousePref3 = addPreference( tr( "VISU_SPACEMOUSE_PREF_3" ), spacemouseGr,
                                       LightApp_Preferences::Selector, "VISU",
                                       "spacemouse_func3_btn" ); //decrease_gauss_point_magnification
  int spacemousePref4 = addPreference( tr( "VISU_SPACEMOUSE_PREF_4" ), spacemouseGr,
                                       LightApp_Preferences::Selector, "VISU",
                                       "spacemouse_func4_btn" ); //increase_gauss_point_magnification
  values.clear();
  values.append( tr( "VISU_SPACEMOUSE_PREF_BTN_1" ) );
  values.append( tr( "VISU_SPACEMOUSE_PREF_BTN_2" ) );
  values.append( tr( "VISU_SPACEMOUSE_PREF_BTN_3" ) );
  values.append( tr( "VISU_SPACEMOUSE_PREF_BTN_4" ) );
  values.append( tr( "VISU_SPACEMOUSE_PREF_BTN_5" ) );
  values.append( tr( "VISU_SPACEMOUSE_PREF_BTN_6" ) );
  values.append( tr( "VISU_SPACEMOUSE_PREF_BTN_7" ) );
  values.append( tr( "VISU_SPACEMOUSE_PREF_BTN_8" ) );
  values.append( tr( "VISU_SPACEMOUSE_PREF_BTN_*" ) );
  values.append( tr( "VISU_SPACEMOUSE_PREF_BTN_10" ) );
  values.append( tr( "VISU_SPACEMOUSE_PREF_BTN_11" ) );
  indices.clear();
  indices.append( 1 );
  indices.append( 2 );
  indices.append( 3 );
  indices.append( 4 );
  indices.append( 5 );
  indices.append( 6 );
  indices.append( 7 );
  indices.append( 8 );
  indices.append( 9 ); // == button_*
  indices.append( 10 );
  indices.append( 11 );
  setPreferenceProperty( spacemousePref3, "strings", values );
  setPreferenceProperty( spacemousePref3, "indexes", indices );
  setPreferenceProperty( spacemousePref4, "strings", values );
  setPreferenceProperty( spacemousePref4, "indexes", indices );
}


//---------------------------------------------------------------
void VisuGUI_Module::createInsideCursorPreferences()
{
  int insideCursorTab = addPreference( tr( "VISU_GAUSS_INSIDE_CURSOR_PREF_TAB_TTL" ) );

  int primitiveGr = addPreference( tr( "VISU_GAUSS_PREF_PRIMITIVE_GROUP_TTL" ), insideCursorTab );
  setPreferenceProperty( primitiveGr, "columns", 2 );

  int primitiveTypePref = addPreference( tr( "VISU_GAUSS_PREF_PRIMITIVE_TYPE" ), primitiveGr,
                                         LightApp_Preferences::Selector, "VISU",
                                         "inside_point_sprite_primitive_type" );

  QStringList values;
  values.append( tr( "VISU_GAUSS_PREF_POINTSPRITE" ) );
  values.append( tr( "VISU_GAUSS_PREF_OPENGLPOINT" ) );
  values.append( tr( "VISU_GAUSS_PREF_GEOMSPHERE" ) );
  QList<QVariant> indices;
  indices.append( 0 );
  indices.append( 1 );
  indices.append( 2 );
  setPreferenceProperty( primitiveTypePref, "strings", values );
  setPreferenceProperty( primitiveTypePref, "indexes", indices );

  int clampPref = addPreference( tr( "VISU_GAUSS_PREF_CLAMP" ), primitiveGr,
                                 LightApp_Preferences::IntSpin, "VISU", "inside_point_sprite_clamp" );
  setPreferenceProperty( clampPref, "min", 1 );
  setPreferenceProperty( clampPref, "max", 512 );

  addPreference( tr( "VISU_GAUSS_PREF_MAIN_TEXTURE" ), primitiveGr,
                 LightApp_Preferences::File, "VISU", "inside_point_sprite_main_texture" );

  addPreference( tr( "VISU_GAUSS_PREF_ALPHA_TEXTURE" ), primitiveGr,
                 LightApp_Preferences::File, "VISU", "inside_point_sprite_alpha_texture" );

  int alphaThresholdPref = addPreference( tr( "VISU_GAUSS_PREF_ALPHA_THRESHOLD" ), primitiveGr,
                                          LightApp_Preferences::DblSpin, "VISU",
                                          "inside_point_sprite_alpha_threshold" );
  setPreferenceProperty( alphaThresholdPref, "min", 0.0 );
  setPreferenceProperty( alphaThresholdPref, "max", 1.0 );
  setPreferenceProperty( alphaThresholdPref, "step", 0.1 );

  int resolutionPref = addPreference( tr( "VISU_GAUSS_PREF_RESOLUTION" ), primitiveGr,
                                      LightApp_Preferences::IntSpin, "VISU",
                                      "inside_geom_sphere_resolution" );
  setPreferenceProperty( resolutionPref, "min", 3 );
  setPreferenceProperty( resolutionPref, "max", 100 );

  int faceLimitPref = addPreference( tr( "VISU_GAUSS_PREF_FACE_LIMIT" ), primitiveGr,
                                     LightApp_Preferences::IntSpin, "VISU",
                                     "inside_geom_sphere_face_limit" );
  setPreferenceProperty( faceLimitPref, "min", 10 );
  setPreferenceProperty( faceLimitPref, "max", 1000000 );

  int sizeGr = addPreference( tr( "VISU_GAUSS_PREF_SIZE_GROUP_TTL" ), insideCursorTab );
  setPreferenceProperty( sizeGr, "columns", 4 );

  int minSizePref = addPreference( tr( "VISU_GAUSS_PREF_MIN_SIZE" ), sizeGr,
                                   LightApp_Preferences::IntSpin, "VISU",
                                   "inside_point_sprite_min_size" );
  setPreferenceProperty( minSizePref, "min", 1 );
  setPreferenceProperty( minSizePref, "max", 100 );

  int maxSizePref = addPreference( tr( "VISU_GAUSS_PREF_MAX_SIZE" ), sizeGr,
                                   LightApp_Preferences::IntSpin, "VISU",
                                   "inside_point_sprite_max_size" );
  setPreferenceProperty( maxSizePref, "min", 1 );
  setPreferenceProperty( maxSizePref, "max", 100 );

  int magnificationGr = addPreference( tr( "VISU_GAUSS_PREF_MAGNIFICATION_GROUP_TTL" ), insideCursorTab );
  setPreferenceProperty( magnificationGr, "columns", 4 );

  int magnificationPref = addPreference( tr( "VISU_GAUSS_PREF_MAGNIFICATION" ), magnificationGr,
                                         LightApp_Preferences::IntSpin, "VISU",
                                         "inside_point_sprite_magnification" );
  setPreferenceProperty( magnificationPref, "min", 10 );
  setPreferenceProperty( magnificationPref, "max", 1000 );

  int incrementPref = addPreference( tr( "VISU_GAUSS_PREF_INCREMENT" ), magnificationGr,
                                     LightApp_Preferences::DblSpin, "VISU",
                                     "inside_point_sprite_increment" );
  setPreferenceProperty( incrementPref, "min", 0.01 );
  setPreferenceProperty( incrementPref, "max", 10 );
  setPreferenceProperty( incrementPref, "step", 0.1 );
}


//---------------------------------------------------------------
void VisuGUI_Module::createOutsideCursorPreferences()
{
  int outsideCursorTab = addPreference( tr( "VISU_GAUSS_OUTSIDE_CURSOR_PREF_TAB_TTL" ) );

  int primitiveGr = addPreference( tr( "VISU_GAUSS_PREF_PRIMITIVE_GROUP_TTL" ), outsideCursorTab );
  setPreferenceProperty( primitiveGr, "columns", 2 );

  int primitiveTypePref = addPreference( tr( "VISU_GAUSS_PREF_PRIMITIVE_TYPE" ), primitiveGr,
                                         LightApp_Preferences::Selector, "VISU",
                                         "outside_point_sprite_primitive_type" );

  QStringList values;
  values.append( tr( "VISU_GAUSS_PREF_POINTSPRITE" ) );
  values.append( tr( "VISU_GAUSS_PREF_OPENGLPOINT" ) );
  values.append( tr( "VISU_GAUSS_PREF_GEOMSPHERE" ) );
  QList<QVariant> indices;
  indices.append( 0 );
  indices.append( 1 );
  indices.append( 2 );
  setPreferenceProperty( primitiveTypePref, "strings", values );
  setPreferenceProperty( primitiveTypePref, "indexes", indices );

  int clampPref = addPreference( tr( "VISU_GAUSS_PREF_CLAMP" ), primitiveGr,
                                 LightApp_Preferences::IntSpin, "VISU", "outside_point_sprite_clamp" );
  setPreferenceProperty( clampPref, "min", 1 );
  setPreferenceProperty( clampPref, "max", 512 );

  addPreference( tr( "VISU_GAUSS_PREF_MAIN_TEXTURE" ), primitiveGr,
                 LightApp_Preferences::File, "VISU", "outside_point_sprite_main_texture" );

  addPreference( tr( "VISU_GAUSS_PREF_ALPHA_TEXTURE" ), primitiveGr,
                 LightApp_Preferences::File, "VISU", "outside_point_sprite_alpha_texture" );

  int alphaThresholdPref = addPreference( tr( "VISU_GAUSS_PREF_ALPHA_THRESHOLD" ), primitiveGr,
                                          LightApp_Preferences::DblSpin, "VISU",
                                          "outside_point_sprite_alpha_threshold" );
  setPreferenceProperty( alphaThresholdPref, "min", 0.0 );
  setPreferenceProperty( alphaThresholdPref, "max", 1.0 );
  setPreferenceProperty( alphaThresholdPref, "step", 0.1 );

  int resolutionPref = addPreference( tr( "VISU_GAUSS_PREF_RESOLUTION" ), primitiveGr,
                                      LightApp_Preferences::IntSpin, "VISU",
                                      "outside_geom_sphere_resolution" );
  setPreferenceProperty( resolutionPref, "min", 3 );
  setPreferenceProperty( resolutionPref, "max", 100 );

  int faceLimitPref = addPreference( tr( "VISU_GAUSS_PREF_FACE_LIMIT" ), primitiveGr,
                                     LightApp_Preferences::IntSpin, "VISU",
                                     "outside_geom_sphere_face_limit" );
  setPreferenceProperty( faceLimitPref, "min", 10 );
  setPreferenceProperty( faceLimitPref, "max", 1000000 );

  int sizeGr = addPreference( tr( "VISU_GAUSS_PREF_SIZE_GROUP_TTL" ), outsideCursorTab );
  setPreferenceProperty( sizeGr, "columns", 2 );

  int sizePref = addPreference( tr( "VISU_GAUSS_PREF_SIZE" ), sizeGr,
                                LightApp_Preferences::IntSpin, "VISU", "outside_point_sprite_size" );
  setPreferenceProperty( sizePref, "min", 1 );
  setPreferenceProperty( sizePref, "max", 100 );

  int colorGr = addPreference( tr( "VISU_GAUSS_PREF_COLOR_GROUP_TTL" ), outsideCursorTab );
  setPreferenceProperty( colorGr, "columns", 2 );

  addPreference( tr( "VISU_GAUSS_PREF_UNIFORM_COLOR" ), colorGr,
                 LightApp_Preferences::Bool, "VISU", "outside_point_sprite_uniform" );

  addPreference( tr( "VISU_GAUSS_PREF_COLOR" ), colorGr,
                 LightApp_Preferences::Color, "VISU", "outside_point_sprite_color" );
}


//---------------------------------------------------------------
void VisuGUI_Module::createPickingPreferences()
{
  int pickingTab = addPreference( tr( "VISU_PICKING_PREF_TAB_TTL" ) );

  // Cursor
  int cursorGr = addPreference( tr( "VISU_PICKING_PREF_CURSOR_GROUP_TTL" ), pickingTab );
  setPreferenceProperty( cursorGr, "columns", 2 );

  int cursorSizePref = addPreference( tr( "VISU_PICKING_PREF_CURSOR_SIZE" ), cursorGr,
                                      LightApp_Preferences::DblSpin, "VISU", "picking_cursor_size" );
  setPreferenceProperty( cursorSizePref, "min", 0 );
  setPreferenceProperty( cursorSizePref, "max", 1.0 );
  setPreferenceProperty( cursorSizePref, "step", 0.1 );

  int pyramidHeightPref = addPreference( tr( "VISU_PICKING_PREF_PYRAMID_HEIGHT" ), cursorGr,
                                         LightApp_Preferences::DblSpin, "VISU", "picking_pyramid_height" );
  setPreferenceProperty( pyramidHeightPref, "min", 1 );
  setPreferenceProperty( pyramidHeightPref, "max", 100 );

  /*int selectionColorPref = */
  addPreference( tr( "VISU_PICKING_PREF_SELECTION_COLOR" ), cursorGr,
                 LightApp_Preferences::Color, "VISU", "picking_selection_color" );

  // Tolerance
  int toleranceGr = addPreference( tr( "VISU_PICKING_PREF_TOLERANCE_GROUP_TTL" ), pickingTab );
  setPreferenceProperty( toleranceGr, "columns", 2 );

  int pointTolerancePref = addPreference( tr( "VISU_PICKING_PREF_POINT_SELECTION_TOLERANCE" ), toleranceGr,
                                          LightApp_Preferences::DblSpin, "VISU", "picking_point_tolerance" );
  setPreferenceProperty( pointTolerancePref, "min", 0.001 );
  setPreferenceProperty( pointTolerancePref, "max", 10 );
  setPreferenceProperty( pointTolerancePref, "step", 0.01 );

  // Info window
  int infoWindowGr = addPreference( tr( "VISU_PICKING_PREF_INFO_WINDOW_GROUP_TTL" ), pickingTab );
  setPreferenceProperty( infoWindowGr, "columns", 2 );

  int infoWindowPref = addPreference( tr( "VISU_PICKING_PREF_INFO_WINDOW" ), infoWindowGr,
                                      LightApp_Preferences::Bool, "VISU", "picking_info_window" );
  setPreferenceProperty( infoWindowPref, "columns", 2 );

  int transparencyPref = addPreference( tr( "VISU_PICKING_PREF_TRANSPARENCY" ), infoWindowGr,
                                        LightApp_Preferences::IntSpin, "VISU", "picking_transparency" );
  setPreferenceProperty( transparencyPref, "min", 0 );
  setPreferenceProperty( transparencyPref, "max", 100 );
  setPreferenceProperty( transparencyPref, "step", 10 );

  int positionPref = addPreference( tr( "VISU_PICKING_PREF_POSITION" ), infoWindowGr,
                                    LightApp_Preferences::Selector, "VISU", "picking_position" );
  QStringList values;
  values.append( tr( "VISU_PICKING_PREF_BELOW_POINT" ) );
  values.append( tr( "VISU_PICKING_PREF_TOP_LEFT_CORNER" ) );
  QList<QVariant> indices;
  indices.append( 0 );
  indices.append( 1 );
  setPreferenceProperty( positionPref, "strings", values );
  setPreferenceProperty( positionPref, "indexes", indices );

  // Camera
  int cameraGr = addPreference( tr( "VISU_PICKING_PREF_CAMERA_GROUP_TTL" ), pickingTab );
  setPreferenceProperty( cameraGr, "columns", 2 );

  int cameraPref = addPreference( tr( "VISU_PICKING_PREF_CAMERA_MOVEMENT" ), cameraGr,
                                  LightApp_Preferences::Bool, "VISU", "picking_camera_movement" );
  setPreferenceProperty( cameraPref, "columns", 2 );

  int zoomFactorPref = addPreference( tr( "VISU_PICKING_PREF_ZOOM_FACTOR" ), cameraGr,
                                      LightApp_Preferences::DblSpin, "VISU", "picking_zoom_factor" );
  setPreferenceProperty( zoomFactorPref, "min", 0.1 );
  setPreferenceProperty( zoomFactorPref, "max", 10.0 );
  setPreferenceProperty( zoomFactorPref, "step", 0.1 );

  int stepNumberPref = addPreference( tr( "VISU_PICKING_PREF_STEP_NUMBER" ), cameraGr,
                                      LightApp_Preferences::IntSpin, "VISU", "picking_step_number" );
  setPreferenceProperty( stepNumberPref, "min", 1 );
  setPreferenceProperty( stepNumberPref, "max", 100 );

  // Display parent mesh
  int parentMeshGr = addPreference( tr( "VISU_PICKING_PREF_PARENT_MESH_TTL" ), pickingTab );
  setPreferenceProperty( parentMeshGr, "columns", 1 );

  addPreference( tr( "VISU_PICKING_PREF_DISPLAY_PARENT_MESH" ), parentMeshGr,
                 LightApp_Preferences::Bool, "VISU", "picking_display_parent_mesh" );
}


//---------------------------------------------------------------
void VisuGUI_Module::createSpaceMousePreferences()
{
  int mouseTab = addPreference( tr( "VISU_MOUSE_PREF_TAB_TLT" ) );

  int mouseGr = addPreference( tr( "VISU_MOUSE_PREF_GROUP_TLT" ), mouseTab );
  setPreferenceProperty( mouseGr, "columns", 2 );
  int mousePref = addPreference( tr( "VISU_MOUSE_PREF" ), mouseGr,
                                 LightApp_Preferences::Selector, "VISU", "mouse_behaviour" );
  QStringList values;
  values.append( tr( "VISU_MOUSE_PREF_STANDARD" ) );
  values.append( tr( "VISU_MOUSE_PREF_KEYBOARD_FREE" ) );
  QList<QVariant> indices;
  indices.append( 0 );
  indices.append( 1 );
  setPreferenceProperty( mousePref, "strings", values );
  setPreferenceProperty( mousePref, "indexes", indices );

  int keybrdGr = addPreference( tr( "VISU_KEYBOARD_PREF_GROUP_TTL" ), mouseTab );
  setPreferenceProperty( keybrdGr, "columns", 2 );
  int keybrdPref = addPreference( tr( "VISU_KEYBOARD_PREF" ), keybrdGr,
                                  LightApp_Preferences::IntSpin, "VISU", "speed_increment" );
  setPreferenceProperty( keybrdPref,  "max",  1000  );

  int spacemouseGr = addPreference( tr( "VISU_SPACEMOUSE_PREF" ), mouseTab );
  setPreferenceProperty( spacemouseGr, "columns", 2 );
  int spacemousePref1 = addPreference( tr( "VISU_SPACEMOUSE_PREF_1" ), spacemouseGr,
                                       LightApp_Preferences::Selector, "VISU",
                                       "spacemouse_func1_btn" ); //decrease_speed_increment
  int spacemousePref2 = addPreference( tr( "VISU_SPACEMOUSE_PREF_2" ), spacemouseGr,
                                       LightApp_Preferences::Selector, "VISU",
                                       "spacemouse_func2_btn" ); //increase_speed_increment
  int spacemousePref3 = addPreference( tr( "VISU_SPACEMOUSE_PREF_3" ), spacemouseGr,
                                       LightApp_Preferences::Selector, "VISU",
                                       "spacemouse_func3_btn" ); //decrease_gauss_point_magnification
  int spacemousePref4 = addPreference( tr( "VISU_SPACEMOUSE_PREF_4" ), spacemouseGr,
                                       LightApp_Preferences::Selector, "VISU",
                                       "spacemouse_func4_btn" ); //increase_gauss_point_magnification
  int spacemousePref5 = addPreference( tr( "VISU_SPACEMOUSE_PREF_5" ), spacemouseGr,
                                       LightApp_Preferences::Selector, "VISU",
                                       "spacemouse_func5_btn" ); //dominant_combined_switch
  values.clear();
  values.append( tr( "VISU_SPACEMOUSE_PREF_BTN_1" ) );
  values.append( tr( "VISU_SPACEMOUSE_PREF_BTN_2" ) );
  values.append( tr( "VISU_SPACEMOUSE_PREF_BTN_3" ) );
  values.append( tr( "VISU_SPACEMOUSE_PREF_BTN_4" ) );
  values.append( tr( "VISU_SPACEMOUSE_PREF_BTN_5" ) );
  values.append( tr( "VISU_SPACEMOUSE_PREF_BTN_6" ) );
  values.append( tr( "VISU_SPACEMOUSE_PREF_BTN_7" ) );
  values.append( tr( "VISU_SPACEMOUSE_PREF_BTN_8" ) );
  values.append( tr( "VISU_SPACEMOUSE_PREF_BTN_*" ) );
  values.append( tr( "VISU_SPACEMOUSE_PREF_BTN_10" ) );
  values.append( tr( "VISU_SPACEMOUSE_PREF_BTN_11" ) );
  indices.clear();
  indices.append( 1 );
  indices.append( 2 );
  indices.append( 3 );
  indices.append( 4 );
  indices.append( 5 );
  indices.append( 6 );
  indices.append( 7 );
  indices.append( 8 );
  indices.append( 9 ); // == button_*
  indices.append( 10 );
  indices.append( 11 );
  setPreferenceProperty( spacemousePref1, "strings", values );
  setPreferenceProperty( spacemousePref1, "indexes", indices );
  setPreferenceProperty( spacemousePref2, "strings", values );
  setPreferenceProperty( spacemousePref2, "indexes", indices );
  setPreferenceProperty( spacemousePref3, "strings", values );
  setPreferenceProperty( spacemousePref3, "indexes", indices );
  setPreferenceProperty( spacemousePref4, "strings", values );
  setPreferenceProperty( spacemousePref4, "indexes", indices );
  setPreferenceProperty( spacemousePref5, "strings", values );
  setPreferenceProperty( spacemousePref5, "indexes", indices );
}


//---------------------------------------------------------------
void VisuGUI_Module::createRecorderPreferences()
{
  int recorderTab = addPreference( tr( "VISU_RECORDER_PREF_TAB_TTL" ) );

  int recorderGr = addPreference( tr( "VISU_RECORDER_PREF_GROUP_TTL" ), recorderTab );
  setPreferenceProperty( recorderGr, "columns", 2 );

  int modePref = addPreference( tr( "VISU_RECORDER_PREF_RECORDING_MODE" ), recorderGr,
                                LightApp_Preferences::Selector, "VISU", "recorder_mode" );
  QStringList values;
  values.append( tr( "VISU_RECORDER_PREF_SKIPPED_FRAMES" ) );
  values.append( tr( "VISU_RECORDER_PREF_ALL_DISLPAYED_FRAMES" ) );
  QList<QVariant> indices;
  indices.append( 0 );
  indices.append( 1 );
  setPreferenceProperty( modePref, "strings", values );
  setPreferenceProperty( modePref, "indexes", indices );

  int fpsPref = addPreference( tr( "VISU_RECORDER_PREF_FPS" ), recorderGr,
                               LightApp_Preferences::DblSpin, "VISU", "recorder_fps" );
  setPreferenceProperty( fpsPref, "min", 0.1 );
  setPreferenceProperty( fpsPref, "max", 100 );

  int qualityPref = addPreference( tr( "VISU_RECORDER_PREF_QUALITY" ), recorderGr,
                                   LightApp_Preferences::IntSpin, "VISU", "recorder_quality" );
  setPreferenceProperty( qualityPref, "min", 1 );
  setPreferenceProperty( qualityPref, "max", 100 );

  addPreference( tr( "VISU_RECORDER_PREF_PROGRESSIVE" ), recorderGr,
                 LightApp_Preferences::Bool, "VISU", "recorder_progressive" );
}


//---------------------------------------------------------------
void VisuGUI_Module::OnCreateGaussPoints()
{
  double initialTime = vtkTimerLog::GetCPUTime();
  //CreatePrs3d<VISU::GaussPoints_i, VisuGUI_GaussPointsDlg, 1>(this, VVTK_Viewer::Type());
  Prs3d_i* aPrs = CreatePrs3d<VISU::GaussPoints_i, VisuGUI_GaussPointsDlg, 1>(this, SVTK_Viewer::Type());
  processPresentationCreated(aPrs);
  INFOS( "VisuGUI_Module::OnCreateGaussPoints() : Gauss Points created in " <<
         vtkTimerLog::GetCPUTime() - initialTime << " seconds" );
}

// void VisuGUI_Module::OnViewManagerAdded(SUIT_ViewManager* viewMgr)
// {
//   QString type = viewMgr->getViewModel()->getType();
//   if ( type == VVTK_Viewer::Type() ) 
//     connect( viewMgr, SIGNAL( viewCreated( SUIT_ViewWindow* ) ),
//              this, SLOT( OnViewCreated( SUIT_ViewWindow* ) ) );
// }

void VisuGUI_Module::OnViewCreated(SUIT_ViewWindow* view)
{
  SVTK_ViewWindow* viewWindow = dynamic_cast<SVTK_ViewWindow*>( view );
  if ( viewWindow ) {
    setProperty( viewWindow, "speed_increment" );
    setProperty( viewWindow, "spacemouse_func1_btn" );
    setProperty( viewWindow, "spacemouse_func2_btn" );
    setProperty( viewWindow, "spacemouse_func3_btn" );
    setProperty( viewWindow, "spacemouse_func4_btn" );
    setProperty( viewWindow, "spacemouse_func5_btn" );
  }
}

void VisuGUI_Module::setProperty( SVTK_ViewWindow* viewWindow, const QString& pref )
{
  if ( !viewWindow )
    return;

  SUIT_ResourceMgr* resMgr = SUIT_Session::session()->resourceMgr();
  //SVTK_MainWindow* aMainWindow = viewWindow->getMainWindow();
  int val;
  if ( pref == "speed_increment" ) {
    val = resMgr->integerValue( "VTKViewer", pref, 10 );
    viewWindow->InvokeEvent( SVTK::SetSpeedIncrementEvent, &val );
  }
  else if ( pref == "spacemouse_func1_btn" ) {
    val = resMgr->integerValue( "VTKViewer", pref, 1 );
    viewWindow->InvokeEvent( SVTK::SetSMDecreaseSpeedEvent, &val );
  }
  else if ( pref == "spacemouse_func2_btn" ) {
    val = resMgr->integerValue( "VTKViewer", pref, 2 );
    viewWindow->InvokeEvent( SVTK::SetSMIncreaseSpeedEvent, &val );
  }
  else if ( pref == "spacemouse_func3_btn" ) {
    val = resMgr->integerValue( "VISU", pref, 10 );
    viewWindow->InvokeEvent( VISU::SetSMDecreaseMagnificationEvent, &val );
  }
  else if ( pref == "spacemouse_func4_btn" ) {
    val = resMgr->integerValue( "VISU", pref, 11 );
    viewWindow->InvokeEvent( VISU::SetSMIncreaseMagnificationEvent, &val );
  }
  else if ( pref == "spacemouse_func5_btn" ) {
    val = resMgr->integerValue( "VTKViewer", pref, 9 );
    viewWindow->InvokeEvent( SVTK::SetSMDominantCombinedSwitchEvent, &val );
  }
}

void VisuGUI_Module::setProperty( SVTK_ViewManager* vm, const QString& prop )
{
  if ( !vm )
    return;

  QVector<SUIT_ViewWindow*> windows = vm->getViews();
  for ( int n = windows.count(), i = 0; i < n; i++ )
    setProperty( dynamic_cast<SVTK_ViewWindow*>( windows[i] ), prop );
}

void VisuGUI_Module::preferencesChanged( const QString& group, const QString& pref )
{
  VisuGUI::preferencesChanged(group,pref);

//   if ( group == "VISU" && ( pref == "speed_increment" || pref == "spacemouse_func1_btn" ||
//                          pref == "spacemouse_func2_btn" || pref == "spacemouse_func3_btn" || 
//                          pref == "spacemouse_func4_btn" || pref == "spacemouse_func5_btn" ) ) {

//     // update properties of VVTK view windows
//     SUIT_ViewManager* vm = getApp()->getViewManager( VVTK_Viewer::Type(), false );
//     if ( vm ) 
//       setProperty( dynamic_cast<SVTK_ViewManager*>( vm ), pref );
//   }
}


//---------------------------------------------------------------
SUIT_ViewManager* VisuGUI_Module::getViewManager(const QString& theType, 
               const bool theIsCreate)
{
  if (SUIT_ViewManager* aViewManager = VisuGUI::getViewManager(theType,theIsCreate))
    return aViewManager;

//   if (theIsCreate && theType == VVTK_Viewer::Type())
//     return onCreateViewManager();

  return NULL;
}

//---------------------------------------------------------------
void VisuGUI_Module::OnEditGaussPoints()
{
  VISU::TSelectionInfo aSelectionInfo = VISU::GetSelectedObjects(this);
  if(aSelectionInfo.empty())
      return;

  VISU::TSelectionItem aSelectionItem = aSelectionInfo.front();
  Handle(SALOME_InteractiveObject) anIO = aSelectionItem.myIO;

  if(VISU::Prs3d_i* aPrs3d = VISU::GetPrs3dToModify(this, aSelectionItem.myObjectInfo.myBase)){
    if (SVTK_ViewWindow* aViewWindow = GetViewWindow<SVTK_Viewer>(this)) {
      EditPrs3d<VISU::GaussPoints_i, VisuGUI_GaussPointsDlg, 1>(this, anIO, aPrs3d, aViewWindow);
    }
    // Create VVTK_ViewWindow, if it does not exist
//     if (VVTK_ViewWindow* aViewWindow = GetViewWindow<VVTK_Viewer>(this)) {
//       EditPrs3d<VISU::GaussPoints_i, VisuGUI_GaussPointsDlg, 1>(this, anIO, aPrs3d, aViewWindow);
//     }
  }
}


//---------------------------------------------------------------
namespace
{
  /*  void   GetViewParams(VVTK_MainWindow* theViewWindow,
                       const char* theSuffix,
                       std::ostringstream& theStr)
  {
    vtkFloatingPointType aColor[3];
    vtkRenderer* aRenderer = theViewWindow->getRenderer();
    aRenderer->GetBackground(aColor);
    Storable::DataToStream(theStr,(std::string("myColor") + theSuffix + ".R").c_str(),aColor[0]);
    Storable::DataToStream(theStr,(std::string("myColor") + theSuffix + ".G").c_str(),aColor[1]);
    Storable::DataToStream(theStr,(std::string("myColor") + theSuffix + ".B").c_str(),aColor[2]);

    double aPosition[3];
    vtkCamera* aCamera = aRenderer->GetActiveCamera();
    aCamera->GetPosition(aPosition);
    Storable::DataToStream(theStr,(std::string("myPosition") + theSuffix + "[0]").c_str(),aPosition[0]);
    Storable::DataToStream(theStr,(std::string("myPosition") + theSuffix + "[1]").c_str(),aPosition[1]);
    Storable::DataToStream(theStr,(std::string("myPosition") + theSuffix + "[2]").c_str(),aPosition[2]);

    double aFocalPnt[3];
    aCamera->GetFocalPoint(aFocalPnt);
    Storable::DataToStream(theStr,(std::string("myFocalPnt") + theSuffix + "[0]").c_str(),aFocalPnt[0]);
    Storable::DataToStream(theStr,(std::string("myFocalPnt") + theSuffix + "[1]").c_str(),aFocalPnt[1]);
    Storable::DataToStream(theStr,(std::string("myFocalPnt") + theSuffix + "[2]").c_str(),aFocalPnt[2]);

    double aViewUp[3];
    aCamera->GetViewUp(aViewUp);
    Storable::DataToStream(theStr,(std::string("myViewUp") + theSuffix + "[0]").c_str(),aViewUp[0]);
    Storable::DataToStream(theStr,(std::string("myViewUp") + theSuffix + "[1]").c_str(),aViewUp[1]);
    Storable::DataToStream(theStr,(std::string("myViewUp") + theSuffix + "[2]").c_str(),aViewUp[2]);

    vtkFloatingPointType aParallelScale = aCamera->GetParallelScale();
    Storable::DataToStream(theStr,(std::string("myParallelScale") + theSuffix).c_str(),aParallelScale);

    double aScaleFactor[3];
    theViewWindow->GetScale(aScaleFactor);
    Storable::DataToStream(theStr,(std::string("myScaleFactor") + theSuffix + "[0]").c_str(),aScaleFactor[0]);
    Storable::DataToStream(theStr,(std::string("myScaleFactor") + theSuffix + "[1]").c_str(),aScaleFactor[1]);
    Storable::DataToStream(theStr,(std::string("myScaleFactor") + theSuffix + "[2]").c_str(),aScaleFactor[2]);
  }
  */
  //---------------------------------------------------------------
  /*  void SetViewParams(VVTK_MainWindow* theViewWindow,
                     const char* theSuffix,
                     const Storable::TRestoringMap& theMap)
  {
    vtkFloatingPointType aColor[3];
    aColor[0] = Storable::FindValue(theMap,std::string("myColor") + theSuffix + ".R").toDouble();
    aColor[1] = Storable::FindValue(theMap,std::string("myColor") + theSuffix + ".G").toDouble();
    aColor[2] = Storable::FindValue(theMap,std::string("myColor") + theSuffix + ".B").toDouble();
    vtkRenderer* aRenderer = theViewWindow->getRenderer();
    aRenderer->SetBackground(aColor);

    double aPosition[3];
    aPosition[0] = Storable::FindValue(theMap,std::string("myPosition") + theSuffix + "[0]").toDouble();
    aPosition[1] = Storable::FindValue(theMap,std::string("myPosition") + theSuffix + "[1]").toDouble();
    aPosition[2] = Storable::FindValue(theMap,std::string("myPosition") + theSuffix + "[2]").toDouble();
    vtkCamera* aCamera = aRenderer->GetActiveCamera();
    aCamera->SetPosition(aPosition);

    double aFocalPnt[3];
    aFocalPnt[0] = Storable::FindValue(theMap,std::string("myFocalPnt") + theSuffix + "[0]").toDouble();
    aFocalPnt[1] = Storable::FindValue(theMap,std::string("myFocalPnt") + theSuffix + "[1]").toDouble();
    aFocalPnt[2] = Storable::FindValue(theMap,std::string("myFocalPnt") + theSuffix + "[2]").toDouble();
    aCamera->SetFocalPoint(aFocalPnt);

    double aViewUp[3];
    aViewUp[0] = Storable::FindValue(theMap,std::string("myViewUp") + theSuffix + "[0]").toDouble();
    aViewUp[1] = Storable::FindValue(theMap,std::string("myViewUp") + theSuffix + "[1]").toDouble();
    aViewUp[2] = Storable::FindValue(theMap,std::string("myViewUp") + theSuffix + "[2]").toDouble();
    aCamera->SetViewUp(aViewUp);

    vtkFloatingPointType aParallelScale = Storable::FindValue(theMap,std::string("myParallelScale") + theSuffix).toDouble();
    aCamera->SetParallelScale(aParallelScale);

    double aScaleFactor[3];
    aScaleFactor[0] = Storable::FindValue(theMap,std::string("myScaleFactor") + theSuffix + "[0]").toDouble();
    aScaleFactor[1] = Storable::FindValue(theMap,std::string("myScaleFactor") + theSuffix + "[1]").toDouble();
    aScaleFactor[2] = Storable::FindValue(theMap,std::string("myScaleFactor") + theSuffix + "[2]").toDouble();
    theViewWindow->SetScale(aScaleFactor);
  }
  */
  //---------------------------------------------------------------
  /*  void   GetViewParams(VVTK_MainWindow1* theViewWindow,
                       std::ostringstream& theStr)
  {
    GetViewParams(theViewWindow,"1",theStr);

    VISU_WidgetCtrl* aWidgetCtrl = theViewWindow->GetWidgetCtrl();
    if(aWidgetCtrl->GetEnabled()){
      std::string aSegmentationMode;
      if(aWidgetCtrl->IsPlanesActive()){
        VISU_PlanesWidget *aPlanesWidget = aWidgetCtrl->GetPlanesWidget();
        vtkFloatingPointType anOrigin[3];
        aPlanesWidget->GetOrigin(anOrigin);
        Storable::DataToStream(theStr,"myCursorOrigin[0]",anOrigin[0]);
        Storable::DataToStream(theStr,"myCursorOrigin[1]",anOrigin[1]);
        Storable::DataToStream(theStr,"myCursorOrigin[2]",anOrigin[2]);

        vtkFloatingPointType aNormal[3];
        aPlanesWidget->GetNormal(aNormal);
        Storable::DataToStream(theStr,"myCursorNormal[0]",aNormal[0]);
        Storable::DataToStream(theStr,"myCursorNormal[1]",aNormal[1]);
        Storable::DataToStream(theStr,"myCursorNormal[2]",aNormal[2]);

        vtkFloatingPointType aDepth = aPlanesWidget->Distance();
        Storable::DataToStream(theStr,"myCursorDepth",aDepth);

        aSegmentationMode = "Planes";
      }else if(aWidgetCtrl->IsSphereActive()){
        VISU_SphereWidget *aSphereWidget = aWidgetCtrl->GetSphereWidget();
        vtkFloatingPointType aCenter[3];
        aSphereWidget->GetCenter(aCenter);
        Storable::DataToStream(theStr,"mySphereCursorCenter[0]",aCenter[0]);
        Storable::DataToStream(theStr,"mySphereCursorCenter[1]",aCenter[1]);
        Storable::DataToStream(theStr,"mySphereCursorCenter[2]",aCenter[2]);

        vtkFloatingPointType aRadius = aSphereWidget->GetRadius();
        Storable::DataToStream(theStr,"mySphereCursorRaduis",aRadius);

        aSegmentationMode = "Sphere";
      }

      Storable::DataToStream(theStr,"mySegmentationMode",aSegmentationMode.c_str());
    }
  }
  */
  //---------------------------------------------------------------
  /*void SetViewParams(VVTK_MainWindow1* theViewWindow,
                     const Storable::TRestoringMap& theMap)
  {
    SetViewParams(theViewWindow,"1",theMap);
  }

  */
  //---------------------------------------------------------------
  /*void GetViewParams(VVTK_MainWindow2* theViewWindow,
                     std::ostringstream& theStr)
  {
    GetViewParams(theViewWindow,"2",theStr);
  }

  void SetViewParams(VVTK_MainWindow2* theViewWindow,
                     const Storable::TRestoringMap& theMap)
  {
    SetViewParams(theViewWindow,"2",theMap);
  }
  */

  //---------------------------------------------------------------
  /*std::string GetViewParams(VVTK_ViewWindow* theViewWindow)
  {
    std::ostringstream aStream;

    Storable::DataToStream(aStream,"myComment","GAUSSVIEW");

    SVTK_Selector* aSelector = theViewWindow->GetSelector();
    Selection_Mode aSelectionMode = aSelector->SelectionMode();
    Storable::DataToStream(aStream,"mySelectionMode",aSelectionMode);

    GetViewParams(theViewWindow->getMainWindow1(),aStream);
    GetViewParams(theViewWindow->getMainWindow2(),aStream);

    return aStream.str();
  }
  */
  //---------------------------------------------------------------
  struct TSelection
  {
    bool myIsSelected;
    bool myHasSubId;
    int mySubId;

    TSelection():
      myIsSelected(false),
      myHasSubId(false),
      mySubId(-1)
    {}
  };

  typedef std::map<std::string,TSelection> TVisibleEntries;

  struct TGetVisibleEntries
  {
    TVisibleEntries& myVisibleEntries;

    TGetVisibleEntries(TVisibleEntries& theVisibleEntries):
      myVisibleEntries(theVisibleEntries)
    {}

    void operator()(VISU_GaussPtsAct* theActor) 
    {
      if(theActor->GetVisibility()){
        const Handle(SALOME_InteractiveObject)& anIO = theActor->getIO();
        myVisibleEntries.insert(TVisibleEntries::value_type(anIO->getEntry(),TSelection()));
      }
    }
  };

  /*void GetGaussPointsSelection(SVTK_ViewWindow* theViewWindow,
                               TVisibleEntries& theVisibleEntries)
  {
    // First find all visible Gauss Points presentations
    vtkRenderer* aRenderer = theViewWindow->getRenderer();
    vtkActorCollection* anActors = aRenderer->GetActors();
    TGetVisibleEntries aGetVisibleEntries(theVisibleEntries);
    SVTK::ForEach<VISU_GaussPtsAct>(anActors,
                                    aGetVisibleEntries);

    // Next, find the sub-ids for the visible Gauss Points presentations
    SVTK_Selector* aSelector = theViewWindow->GetSelector();
    const SALOME_ListIO& aListIO = aSelector->StoredIObjects();
    SALOME_ListIteratorOfListIO anIter(aListIO);
    for(; anIter.More(); anIter.Next()){
      Handle(SALOME_InteractiveObject) anIO = anIter.Value();
      std::string anEntry = anIO->getEntry();
      TVisibleEntries::iterator anEntriesIter = theVisibleEntries.find(anEntry);
      if(anEntriesIter != theVisibleEntries.end()){
        TSelection& aSelection = anEntriesIter->second;
        aSelection.myIsSelected = true;
        TColStd_IndexedMapOfInteger anIndexes;
        aSelector->GetIndex(anIO,anIndexes);
        if(anIndexes.Extent() > 0){
          aSelection.myHasSubId = true;
          aSelection.mySubId = anIndexes(1);
        }
      }
    }
  }
  */

  //---------------------------------------------------------------
  inline void CreateReference(_PTR(Study) theStudyDocument,
                              _PTR(StudyBuilder) theStudyBuilder,
                              _PTR(SObject) theFatherSObject, 
                              const string& theRefEntry,
                              const TSelection& theSelection)
  {
    _PTR(SObject) aNewObj = theStudyBuilder->NewObject(theFatherSObject);
    _PTR(SObject) aRefSObj = theStudyDocument->FindObjectID(theRefEntry);
    theStudyBuilder->Addreference(aNewObj,aRefSObj);

    std::ostringstream aStream;
    Storable::DataToStream(aStream,"myIsSelected",theSelection.myIsSelected);
    Storable::DataToStream(aStream,"myHasSubId",theSelection.myHasSubId);
    Storable::DataToStream(aStream,"mySubId",theSelection.mySubId);

    _PTR(GenericAttribute) anAttr;
    anAttr = theStudyBuilder->FindOrCreateAttribute(aNewObj,"AttributeString");
    _PTR(AttributeString) aComment(anAttr);
    aComment->SetValue(aStream.str());
  }


  //---------------------------------------------------------------
  /*void SetGaussPointsSelection(VisuGUI* theModule,
                               SVTK_ViewWindow* theViewWindow,
                               _PTR(Study) theCStudy,
                               _PTR(SObject) theSObject)
  {
    SVTK_Selector* aSelector = theViewWindow->GetSelector();
    aSelector->ClearIObjects();

    bool anIsFirst = true;
    _PTR(ChildIterator) aChildIter = theCStudy->NewChildIterator(theSObject);
    for (; aChildIter->More(); aChildIter->Next()) {
      _PTR(SObject) aChildSObject = aChildIter->Value();
      _PTR(SObject) aSObject;
      if(aChildSObject->ReferencedObject(aSObject)){
        CORBA::Object_var anObject = VISU::ClientSObjectToObject(aSObject);
        PortableServer::ServantBase_var aServant = VISU::GetServant(anObject);
        if(VISU::Prs3d_i* aPrs3d = dynamic_cast<VISU::Prs3d_i*>(aServant.in())){
          // To set visiblity
          VISU::UpdateViewer(theModule, aPrs3d, anIsFirst, false);
          anIsFirst = false;

          // To update selection
          Storable::TRestoringMap aMap = Storable::GetStorableMap(aChildSObject);
          if(!aMap.empty()){
            bool anIsSelected = aMap["myIsSelected"].toInt();
            bool aHasSubId = aMap["myHasSubId"].toInt();
            int aSubId = aMap["mySubId"].toInt();

            if(anIsSelected){
              std::string anEntry = aSObject->GetID();
              Handle(SALOME_InteractiveObject) anIO = new SALOME_InteractiveObject(anEntry.c_str(),"");
              aSelector->AddIObject(anIO);
              if(aHasSubId)
                aSelector->AddOrRemoveIndex(anIO,aSubId,false);
            }
          }
        }
      }
    }

    aSelector->EndPickCallback(); // To invoke selection changed signal
  }
  */

  //---------------------------------------------------------------
  /*void OnStoreConfiguration(SalomeApp_Module* theModule,
                            bool theIsNew)
  {
    _PTR(Study) aCStudy = GetCStudy(GetAppStudy(theModule));
    if (CheckLock(aCStudy,GetDesktop(theModule)))
      return;

    LightApp_SelectionMgr* aSelectionMgr = GetSelectionMgr(theModule);

    SUIT_ViewManager* aViewManager = theModule->getApp()->activeViewManager();
        if(aViewManager->getType() == VVTK_Viewer::Type()){
      SUIT_ViewWindow* aWindow = aViewManager->getActiveView();
      VVTK_ViewWindow* aViewWindow = dynamic_cast<VVTK_ViewWindow*>(aWindow);

      SUIT_Study* aSStudy = aViewManager->study();
      SalomeApp_Study* aStudy = dynamic_cast<SalomeApp_Study*>(aSStudy);
      _PTR(Study) aCStudy = aStudy->studyDS();
      _PTR(StudyBuilder) aStudyBuilder = aCStudy->NewBuilder();

      std::string anEntry;
      std::string aValue = GetViewParams(aViewWindow);

      if(theIsNew){
        _PTR(SComponent) aSComponent = ClientFindOrCreateVisuComponent(aCStudy);

        static int myNbConfigs = 0;
        std::string aName = VISU::GenerateName("Config.", ++myNbConfigs).toLatin1().data();

        std::string aSComponentEntry = aSComponent->GetID();
        anEntry = CreateAttributes(aCStudy,
                                   aSComponentEntry.c_str(),
                                   "",
                                   "",
                                   aName.c_str(),
                                   "",
                                   aValue.c_str());
      }else{
        SALOME_ListIO aListIO;
        aSelectionMgr->selectedObjects(aListIO);
        SALOME_ListIteratorOfListIO aListIter( aListIO );
        for(; aListIter.More(); aListIter.Next()){
          Handle(SALOME_InteractiveObject) anIO = aListIter.Value();
          _PTR(SObject) aSObject = aCStudy->FindObjectID(anIO->getEntry());
          _PTR(GenericAttribute) anAttr;
          if(aSObject->FindAttribute(anAttr,"AttributeString")){
            _PTR(AttributeString) aComment(anAttr);
            std::string aCommentValue(aComment->Value());
            if(aCommentValue.compare("myComment=GAUSSVIEW") >= 0){
              aComment->SetValue(aValue.c_str());
              anEntry = aSObject->GetID();

              _PTR(ChildIterator) aChildIter = aCStudy->NewChildIterator(aSObject);
              for (; aChildIter->More(); aChildIter->Next()) {
                _PTR(SObject) aChildSObject = aChildIter->Value();
                aStudyBuilder->RemoveObject(aChildSObject);
              }
              break;
            }
          }
        }
      }

      if(anEntry != ""){
        TVisibleEntries aVisibleEntries;
        GetGaussPointsSelection(aViewWindow,
                                aVisibleEntries);

        _PTR(SObject) aSObject = aCStudy->FindObjectID(anEntry);
        _PTR(StudyBuilder) aStudyBuilder = aCStudy->NewBuilder();
        TVisibleEntries::const_iterator anIter =  aVisibleEntries.begin();
        for(; anIter != aVisibleEntries.end(); anIter++){
          const std::string& anEntry = anIter->first;
          const TSelection& aSelection = anIter->second;

          CreateReference(aCStudy,
                          aStudyBuilder,
                          aSObject,
                          anEntry,
                          aSelection);
        }

        //UpdateObjBrowser(theModule,true,aSObject);
        UpdateObjBrowser(theModule,true);
      }
    } 
    }*/


  //---------------------------------------------------------------
/*  template<class TMainWindow>
  void SetMainWindowParams(VisuGUI* theModule,
                           _PTR(SObject) theSObject,
                           VVTK_ViewWindow* theViewWindow,
                           TMainWindow* theMainWindow)
  {
    _PTR(Study) aCStudy = GetCStudy(GetAppStudy(theModule));

    Storable::TRestoringMap aMap = Storable::GetStorableMap(theSObject);
    if(!aMap.empty())
      SetViewParams(theMainWindow, aMap);
      }*/
}


//---------------------------------------------------------------
// void VisuGUI_Module::OnSaveConfiguration()
// {
//   ::OnStoreConfiguration(this,true);
// }


//---------------------------------------------------------------
// void VisuGUI_Module::OnOverwriteConfiguration()
// {
//   ::OnStoreConfiguration(this,false);
// }


//---------------------------------------------------------------
/*void VisuGUI_Module::OnRestoreConfiguration()
{
  LightApp_SelectionMgr* aSelectionMgr = GetSelectionMgr(this);

  SALOME_ListIO aListIO;
  aSelectionMgr->selectedObjects(aListIO);
  if(aListIO.Extent() > 1)
    return;

  if(SUIT_ViewManager* aViewManager = getViewManager(VVTK_Viewer::Type(),true)){
    const Handle(SALOME_InteractiveObject)& anIO = aListIO.First();
    _PTR(Study) aCStudy = GetCStudy(GetAppStudy(this));
    _PTR(SObject) aSObject = aCStudy->FindObjectID(anIO->getEntry());
    myConfigSObject = aSObject;
    Storable::TRestoringMap aMap = Storable::GetStorableMap(aSObject);
    if(!aMap.empty()){
      SUIT_ViewWindow* aViewWindow = aViewManager->getActiveView();
      
      Selection_Mode aSelectionMode = Storable::FindValue(aMap,"mySelectionMode").toInt();
      SVTK_Selector* aSelector = aViewWindow->GetSelector();
      aSelector->SetSelectionMode(aSelectionMode);

      SetGaussPointsSelection(this,aViewWindow,aCStudy,aSObject);

      std::string aSegmentationMode;
      if(VVTK_MainWindow1* aMainWindow = aViewWindow->getMainWindow1()){
        aMainWindow->SetPlanesSegementation(false);
        aMainWindow->SetSphereSegementation(false);
        VISU_WidgetCtrl* aWidgetCtrl = aMainWindow->GetWidgetCtrl();
        aSegmentationMode = Storable::FindValue(aMap,"mySegmentationMode").toLatin1().data();

        if(aSegmentationMode == "Planes"){
          VISU_PlanesWidget *aPlanesWidget = aWidgetCtrl->GetPlanesWidget();
          vtkFloatingPointType anOrigin[3];
          anOrigin[0] = Storable::FindValue(aMap,"myCursorOrigin[0]").toDouble();
          anOrigin[1] = Storable::FindValue(aMap,"myCursorOrigin[1]").toDouble();
          anOrigin[2] = Storable::FindValue(aMap,"myCursorOrigin[2]").toDouble();
          aPlanesWidget->SetOrigin(anOrigin);

          vtkFloatingPointType aNormal[3];
          aNormal[0] = Storable::FindValue(aMap,"myCursorNormal[0]").toDouble();
          aNormal[1] = Storable::FindValue(aMap,"myCursorNormal[1]").toDouble();
          aNormal[2] = Storable::FindValue(aMap,"myCursorNormal[2]").toDouble();
          aPlanesWidget->SetNormal(aNormal);

          vtkFloatingPointType aDepth = Storable::FindValue(aMap,"myCursorDepth").toDouble();
          aPlanesWidget->SetDistance(aDepth);

          aMainWindow->SetPlanesSegementation(true);
        }else if(aSegmentationMode == "Sphere"){
          VISU_SphereWidget *aSphereWidget = aWidgetCtrl->GetSphereWidget();
          vtkFloatingPointType aCenter[3];
          aCenter[0] = Storable::FindValue(aMap,"mySphereCursorCenter[0]").toDouble();
          aCenter[1] = Storable::FindValue(aMap,"mySphereCursorCenter[1]").toDouble();
          aCenter[2] = Storable::FindValue(aMap,"mySphereCursorCenter[2]").toDouble();
          aSphereWidget->SetCenter(aCenter);

          vtkFloatingPointType aRadius = Storable::FindValue(aMap,"mySphereCursorRaduis").toDouble();
          aSphereWidget->SetRadius(aRadius);

          aMainWindow->SetSphereSegementation(true);
        }
      }

      if(VVTK_MainWindow1* aMainWindow = aViewWindow->getMainWindow1()){
        SVTK_RenderWindowInteractor* anInteractor = aMainWindow->GetInteractor();
        if(anInteractor->isVisible()){
          SetMainWindowParams(this,myConfigSObject,aViewWindow,aMainWindow);
        }else
          anInteractor->installEventFilter(this);
      }

      if(aSegmentationMode != ""){
        if(VVTK_MainWindow2* aMainWindow = aViewWindow->getMainWindow2()){
          SVTK_RenderWindowInteractor* anInteractor = aMainWindow->GetInteractor();
          if(anInteractor->isVisible())
            SetMainWindowParams(this,myConfigSObject,aViewWindow,aMainWindow);
          else
            anInteractor->installEventFilter(this);
        }
      }

      SetGaussPointsSelection(this,aViewWindow,aCStudy,aSObject);
    }
  }
}*/


//---------------------------------------------------------------
//bool VisuGUI_Module::eventFilter( QObject * theWatched, QEvent * theEvent )
//{
//  bool aRet = VisuGUI::eventFilter(theWatched,theEvent);
//   if(theEvent->type() == QEvent::Show){
//     if(SUIT_ViewManager* aViewManager = getViewManager(VVTK_Viewer::Type(),false)){
//       SUIT_ViewWindow* aWindow = aViewManager->getActiveView();
//       VVTK_ViewWindow* aViewWindow = dynamic_cast<VVTK_ViewWindow*>(aWindow);
//       if(VVTK_MainWindow1* aMainWindow = aViewWindow->getMainWindow1()){
//      SVTK_RenderWindowInteractor* anInteractor = aMainWindow->GetInteractor();
//      if(theWatched == anInteractor){
//        SetMainWindowParams(this,myConfigSObject,aViewWindow,aMainWindow);
//        anInteractor->removeEventFilter(this);
//      }
//       }
//       if(VVTK_MainWindow2* aMainWindow = aViewWindow->getMainWindow2()){
//      SVTK_RenderWindowInteractor* anInteractor = aMainWindow->GetInteractor();
//      if(theWatched == aMainWindow->GetInteractor()){
//        SetMainWindowParams(this,myConfigSObject,aViewWindow,aMainWindow);
//        anInteractor->removeEventFilter(this);
//      }
//       }
//     }
//   }
//  return aRet;
//}


const char gSeparator = '_'; // character used to separate parameter names
const char gDigitsSep = ':'; // character used to separate numeric parameter values (color = r:g:b)
/*!
 * \brief Virtual public
 *
 * This method is called just before the study document is saved, so the module has a possibility
 * to store visual parameters in AttributeParameter attribue(s)
 */
void VisuGUI_Module::storeVisualParameters(int savePoint)
{
  SalomeApp_Study* study = dynamic_cast<SalomeApp_Study*>( SUIT_Session::session()->activeApplication()->activeStudy() );
  if( !study || !study->studyDS() )
    return;
  _PTR(Study) studyDS = study->studyDS();
  _PTR(AttributeParameter) ap = studyDS->GetModuleParameters("Interface Applicative", moduleName().toLatin1().data(), savePoint);
  _PTR(IParameters) ip = ClientFactory::getIParameters(ap);

  // viewers counters are used for storing view_numbers in IParameters
  int svtkViewers( 0 ), /*vvtkViewers( 0 ),*/ plotViewers( 0 );

  // componentName is used for encoding of entries when storing them in IParameters
  _PTR(SComponent) visuEng = ClientFindOrCreateVisuComponent( studyDS );
  std::string componentName = visuEng->ComponentDataType();

  QList<SUIT_ViewManager*> lst;
  QList<SUIT_ViewManager*>::Iterator it;

  // saving VVTK viewer parameters.  VVTK (Gauss Viewers) are NOT created by SalomeApp since
  // VVTK is declared in VISU, so here we store VVTK view window parameters.
  // VisuGUI_Module::restoreVisualParameters() creates VVTK_Views and restores its parameters.
  /*  ip->setProperty( "ActiveGaussViewer", "-1" ); 
  getApp()->viewManagers( VVTK_Viewer::Type(), lst );
  for ( it = lst.begin(); it != lst.end(); it++ ) {
    if ( SUIT_ViewWindow* vwin = (*it)->getActiveView() ) {
      // using predefined string "GaussViewer" as "entry"..  it's a hardcoded "workaround".
      // gauss viewer parameters are retrieved using this "entry" string.
      // name of parameter  = caption of gauss ViewWindow
      // value of parameter = ViewWindow's visual parameters
      ip->setParameter( "GaussViewer",
                        vwin->windowTitle().toLatin1().data(),
                        vwin->getVisualParameters().toLatin1().data() );

      if ( application()->desktop()->activeWindow() == vwin )
        ip->setProperty( "ActiveGaussViewer", QString::number( vvtkViewers ).toLatin1().data() );
      vvtkViewers++;
    }
  }
  */
  // VISU module opens one SVTK viewer in activateModule().  This causes a bug in save-restore visual
  // parameters: it no SVTK view was saved, we need NOT any SVTK on restore.  Here we store if any is open..
  /*
  lst.clear();
  getApp()->viewManagers( SVTK_Viewer::Type(), lst );
  ip->setProperty( "VtkViewersCount", QString::number( lst.count() ).latin1() );
  */

  // main cycle to store parameters of displayed objects
  lst.clear();
  getApp()->viewManagers( lst );
  /*vvtkViewers = */svtkViewers = plotViewers = 0;
  for ( it = lst.begin(); it != lst.end(); it++ ) {
    SUIT_ViewManager* vman = *it;
    QString vType = vman->getType();
    int* viewsCounter = vType == SVTK_Viewer::Type()    ? &svtkViewers :
      //vType == VVTK_Viewer::Type()    ? &vvtkViewers :
                        vType == SPlot2d_Viewer::Type() ? &plotViewers : 0;

    // saving VTK actors' properties
    if ( vType == SVTK_Viewer::Type() /*||  // processing SVTK and VVTK viewers in the same
                                        vType == VVTK_Viewer::Type()*/ ) { // way (VVTK_ViewWindow inherits SVTK_ViewWindow)       

      QVector<SUIT_ViewWindow*> views = vman->getViews();
      for ( int i = 0, iEnd = vman->getViewsCount(); i < iEnd; i++ ) {
        if ( SVTK_ViewWindow* vtkView = dynamic_cast<SVTK_ViewWindow*>( views[i] ) ) {
          VTK::ActorCollectionCopy aCopy(vtkView->getRenderer()->GetActors());
          vtkActorCollection* allActors = aCopy.GetActors();
          allActors->InitTraversal();
          while ( vtkActor* actor = allActors->GetNextActor() ) {
            if ( actor->GetVisibility() ) { // store only visible actors
              if ( VISU_Actor* vActor = VISU_Actor::SafeDownCast( actor ) ) {
                if ( vActor->hasIO() ) { // actor corresponds to existing obj

                  Handle(SALOME_InteractiveObject) io = vActor->getIO();
                  // entry is "ecoded" = it does NOT contain component adress, since it is a
                  // subject to change on next component loading
                  std::string entry = ip->encodeEntry( io->getEntry(), componentName );

                  std::string param, vtkParam = vType.toLatin1().data();
                  vtkParam += gSeparator;
                  vtkParam += QString::number( *viewsCounter ).toLatin1().data();
                  vtkParam += gSeparator;

                  param = vtkParam + "Visibility";
                  ip->setParameter( entry, param, "On" );
                  param = vtkParam + "Name";
                  ip->setParameter( entry, param, vActor->getName() );
                  param = vtkParam + "RepresentationMode";
                  ip->setParameter( entry, param, QString::number( vActor->GetRepresentation() ).toLatin1().data() );
                  param = vtkParam + "Quadratic2DRepresentation";
                  ip->setParameter( entry, param, QString::number( vActor->GetQuadratic2DRepresentation() ).toLatin1().data() );
                  param = vtkParam + "Opacity";
                  ip->setParameter( entry, param, QString::number( vActor->GetOpacity() ).toLatin1().data() );
                  vtkFloatingPointType r, g, b;
                  vActor->GetColor(r, g, b);
                  QString colorStr  = QString::number( r ); colorStr += gDigitsSep;
                          colorStr += QString::number( g ); colorStr += gDigitsSep;
                          colorStr += QString::number( b );
                  param = vtkParam + "Color";
                  ip->setParameter( entry, param, colorStr.toLatin1().data() );
                  param = vtkParam + "LineWidth";
                  ip->setParameter( entry, param, QString::number( vActor->GetLineWidth() ).toLatin1().data() );
                  if ( vActor->IsShrunkable() && vActor->IsShrunk() ) {
                    param = vtkParam + "ShrinkMode";
                    ip->setParameter( entry, param, "On" );
                    param = vtkParam + "ShrinkFactor";
                    ip->setParameter( entry, param, QString::number( vActor->GetShrinkFactor() ).toLatin1().data() );
                  }
                  VISU_ScalarMapAct* scalarMapActor = dynamic_cast<VISU_ScalarMapAct*>( vActor );
                  if ( scalarMapActor && scalarMapActor->IsShading() ) {
                    param = vtkParam + "Shading";
                    ip->setParameter( entry, param, "On" );
                  }
                  if ( const VISU::Prs3d_i* vPrs = vActor->GetPrs3d() ) {
                    param = vtkParam + "ClippingPlane";
                    int nPlanes = vPrs->GetNumberOfClippingPlanes();
                    if ( !nPlanes )
                      ip->setParameter( entry, param, "Off" );
                    for ( int p = 0; p < nPlanes; p++ ) {
                      vtkPlane* plane = vPrs->GetClippingPlane( p );
                      vtkFloatingPointType normal[3], origin[3];
                      plane->GetNormal( normal );
                      plane->GetOrigin( origin );
                      std::string planeValue  = QString::number( normal[0] ).toLatin1().data(); planeValue += gDigitsSep;
                                  planeValue += QString::number( normal[1] ).toLatin1().data(); planeValue += gDigitsSep;
                                  planeValue += QString::number( normal[2] ).toLatin1().data(); planeValue += gDigitsSep;
                                  planeValue += QString::number( origin[0] ).toLatin1().data(); planeValue += gDigitsSep;
                                  planeValue += QString::number( origin[1] ).toLatin1().data(); planeValue += gDigitsSep;
                                  planeValue += QString::number( origin[2] ).toLatin1().data();
                      param = QString( "%1ClippingPlane_%2" ).arg( vtkParam.c_str() ).arg( p+1 ).toLatin1().data();
                      ip->setParameter( entry, param, planeValue );
                    }
                  }

                } // hasIO
              } // salome_actor successfull downcast to the VISU_Actor
              else if ( VISU_PointMap3dActor* vActor = VISU_PointMap3dActor::SafeDownCast( actor ) ) { //  PointMap3D
                if ( vActor->hasIO() ) { // actor corresponds to existing obj
                  Handle(SALOME_InteractiveObject) io = vActor->getIO();
                  // entry is "ecoded" = it does NOT contain component adress, since it is a
                  // subject to change on next component loading
                  std::string entry = ip->encodeEntry( io->getEntry(), componentName );
                  std::string param, vtkParam = vType.toLatin1().data();
                  vtkParam += gSeparator;
                  vtkParam += QString::number( *viewsCounter ).toLatin1().data();
                  vtkParam += gSeparator;

                  param = vtkParam + "Visibility";
                  ip->setParameter( entry, param, "On" );
                  param = vtkParam + "Name";
                  ip->setParameter( entry, param, vActor->getName() );
                  param = vtkParam + "RepresentationMode";
                  ip->setParameter( entry, param, QString::number( vActor->GetRepresentation() ).toLatin1().data() );
                  param = vtkParam + "Opacity";
                  ip->setParameter( entry, param, QString::number( vActor->GetOpacity() ).toLatin1().data() );
                  vtkFloatingPointType r, g, b;
                  vActor->GetColor(r, g, b);
                  QString colorStr  = QString::number( r ); colorStr += gDigitsSep;
                  colorStr += QString::number( g ); colorStr += gDigitsSep;
                  colorStr += QString::number( b );
                  param = vtkParam + "Color";
                  ip->setParameter( entry, param, colorStr.toLatin1().data() );
                  param = vtkParam + "LineWidth";
                  ip->setParameter( entry, param, QString::number( vActor->GetLineWidth() ).toLatin1().data() );
                  if ( vActor->IsShrunkable() && vActor->IsShrunk() ) {
                    param = vtkParam + "ShrinkMode";
                    ip->setParameter( entry, param, "On" );
                    param = vtkParam + "ShrinkFactor";
                    ip->setParameter( entry, param, QString::number( vActor->GetShrinkFactor() ).toLatin1().data() );
                  }
                }
	      } // salome actor is downcasted to the VISU_PointMap3dActor
            } // isVisible
          } // end of ..while.. actors traversal
        } // if ( vtkView )
      } // for ( views )
      (*viewsCounter)++;
    } // if ( SVTK view model )
    else if ( vType == SPlot2d_Viewer::Type() ) {  // processing Plot2d viewers
      QVector<SUIT_ViewWindow*> views = vman->getViews();
      for ( int i = 0, iEnd = vman->getViewsCount(); i < iEnd; i++ ) {
        if ( Plot2d_ViewWindow* plotView = dynamic_cast<Plot2d_ViewWindow*>( views[i] ) ) {
          Plot2d_ViewFrame* plotVF = plotView->getViewFrame();
          QList<Plot2d_Curve*> curves;
          QList<Plot2d_Curve*>::Iterator itCurve;
          plotVF->getCurves( curves );
          
          //Plot2d_Curve* curve;
          for ( itCurve = curves.begin(); itCurve != curves.end(); itCurve++ ) {
            if ( SPlot2d_Curve* sCurve = dynamic_cast<SPlot2d_Curve*>( *itCurve ) ) {
              if ( sCurve->hasIO() ) {

                Handle(SALOME_InteractiveObject) io = sCurve->getIO();
                // entry is "ecoded" = it does NOT contain component adress, since it is a
                // subject to change on next component loading
                std::string entry = ip->encodeEntry( io->getEntry(), componentName );

                std::string param, plotParam = vType.toLatin1().data(); plotParam += gSeparator;
                plotParam += QString::number( *viewsCounter ).toLatin1().data();      plotParam += gSeparator;

                param = plotParam + "Visibility";
                ip->setParameter( entry, param, "On" );
              }
            }
          } // for curves
        } // if ( plotView )
      } // for ( views )
      (*viewsCounter)++;
    } // if ( SPlot2d view model )
  }
}

const int ViewerType = 0;
const int ViewIndex = 1;
const int ParamName = 2;
// visual parameters are stored in strings as follows:
// ViewerType_ViewNumber_ParamName.  '_' is used as separator and should not be used in
// viewer type or parameter names

// return viewer type substring from parameter name
std::string getParam( const std::string& paramName, const int index )
{
  QStringList lst = QString( paramName.c_str() ).split( gSeparator, QString::SkipEmptyParts );
  if ( !lst.isEmpty() && index < lst.size() )
    return lst[index].toLatin1().data();
  return "";
}


/*!
 * \brief Virtual public
 *
 * This method is called after the study document is opened, so the module has a possibility to restore
 * visual parameters
 */
void VisuGUI_Module::restoreVisualParameters(int savePoint)
{
  SalomeApp_Study* study = dynamic_cast<SalomeApp_Study*>( SUIT_Session::session()->activeApplication()->activeStudy() );
  if( !study || !study->studyDS() )
    return;
  _PTR(Study) studyDS = study->studyDS();
  _PTR(AttributeParameter) ap =
    studyDS->GetModuleParameters("Interface Applicative", moduleName().toLatin1().data(), savePoint);
  _PTR(IParameters) ip = ClientFactory::getIParameters(ap);

  // actors are stored in a map after displaying of them for quicker access in future
  QMap<QString, QMap<QString, VISU_ActorBase*> > vtkActors; // map: entry to map: ViewType_<ID> to actor (SVTK/VVTK)

  std::vector<std::string> entries = ip->getEntries();

  for ( std::vector<std::string>::iterator entIt = entries.begin(); entIt != entries.end(); ++entIt ) {

    std::vector<std::string> paramNames = ip->getAllParameterNames( *entIt );
    std::vector<std::string> paramValues = ip->getAllParameterValues( *entIt );
    std::vector<std::string>::iterator namesIt = paramNames.begin();
    std::vector<std::string>::iterator valuesIt = paramValues.begin();
    
//     if ( *entIt == "GaussViewer" ) {
//       // parameter names are view window's captions, values - visual parameters.
//       for ( ; namesIt != paramNames.end(); ++namesIt, ++valuesIt ) {
//      SUIT_ViewManager* vman = onCreateViewManager();
//      SUIT_ViewWindow* vwin = vman->getActiveView(); 
//      vwin->setWindowTitle( (*namesIt).c_str() );

//      // wait untill the window is really shown.  This step fixes MANY bugs..
//      while ( !vwin->isVisible() )
//        qApp->processEvents();

//      vwin->setVisualParameters( (*valuesIt).c_str() );
//       }
//       continue; // skip to next entry
//     }
    
    // entry is a normal entry - it should be "decoded" (setting base adress of component) 
    QString entry( ip->decodeEntry( *entIt ).c_str() );

    //SRN: Added a check that the entry corresponds to Standard_Real object in the Study
    //     as the object may be deleted or modified after the visual state is saved.
    _PTR(SObject) so = studyDS->FindObjectID(entry.toLatin1().data());
    if(!so) continue;  //Skip the not existent entry

    for ( ; namesIt != paramNames.end(); ++namesIt, ++valuesIt ) {
      std::string viewerType = ::getParam( *namesIt, ViewerType );

      std::string paramName = ::getParam( *namesIt, ParamName );
      bool ok;
      std::string viewIndexStr = ::getParam( *namesIt, ViewIndex );
      int viewIndex = QString( viewIndexStr.c_str() ).toUInt( &ok );
      if ( !ok ) // bad conversion of view index to integer
        continue;

      //      cout << " -- " << viewerType << ": entry = " << entry.latin1() << ", paramName = " << paramName << endl;

      if ( viewerType == SVTK_Viewer::Type().toLatin1().data() /*||
                                                                 viewerType == VVTK_Viewer::Type().toLatin1().data()*/ ) {

        // used as inner map key for locating the actor.
        QString viewerTypeIndex = QString( viewerType.c_str() ) + QString::number( viewIndex );

        if ( paramName == "Visibility" && displayer() ) {
          // if VVTK, then we must create viewer first, because

          QList<SUIT_ViewManager*> lst;
          getApp()->viewManagers( viewerType.c_str(), lst );

          // SVTK/VVTK ViewManager always has 1 ViewWindow, so view index is index of view manager
          if ( viewIndex >= 0 && viewIndex < lst.count() ) {
            SUIT_ViewManager* vman = lst.at( viewIndex );
            SUIT_ViewModel* vmodel = vman->getViewModel();
            // both SVTK and VVTK view models can be casted to SALOME_View
            displayer()->Display( entry, true, dynamic_cast<SALOME_View*>( vmodel ) );

            // store displayed actor in a temporary map for quicker access later when restoring other parameters
            SVTK_ViewWindow* vtkView = (SVTK_ViewWindow*) vman->getActiveView();
            QMap<QString, VISU_ActorBase*> viewActorMap;
            if ( vtkActors.contains( entry ) )
              viewActorMap = vtkActors[ entry ];
            viewActorMap[ viewerTypeIndex ] = FindActorBase( GetAppStudy(this), vtkView, entry );
            vtkActors[ entry ] = viewActorMap;
          }
        }
        else { // the rest properties "work" with VISU_Actor, so we initialize it at first
          VISU_Actor* vActor = 0;
          VISU_PointMap3dActor* vP3dActor = 0;
          if ( vtkActors.contains( entry ) ) {
            QMap<QString, VISU_ActorBase*> viewActorMap = vtkActors[ entry ];
            if ( viewActorMap.contains( viewerTypeIndex ) ) {
              vActor = VISU_Actor::SafeDownCast( viewActorMap[ viewerTypeIndex ] );
	      vP3dActor = VISU_PointMap3dActor::SafeDownCast( viewActorMap[ viewerTypeIndex ] );
	    }
	  }
          QString val( (*valuesIt).c_str() );
          if ( vActor ) {
	    // salome_actor successfull downcast to the VISU_Actor
	    if ( paramName == "Name" )
	      vActor->setName( val.toLatin1().data() );

	    else if ( paramName == "RepresentationMode" )
	      vActor->SetRepresentation( val.toInt() );

	    else if (paramName == "Quadratic2DRepresentation")
	      vActor->SetQuadratic2DRepresentation(VISU_Actor::EQuadratic2DRepresentation(val.toInt()));

	    else if ( paramName == "Opacity" )
	      vActor->SetOpacity( val.toFloat() );
	    
	    else if ( paramName == "Color" ) {
	      QStringList colors = val.split( gDigitsSep, QString::SkipEmptyParts );
	      if ( colors.count() == 3 )
		vActor->SetColor( colors[0].toFloat(), colors[1].toFloat(), colors[2].toFloat() );
	    }

	    else if ( paramName == "LineWidth" )
	      vActor->SetLineWidth( val.toFloat() );

	    else if ( paramName == "ShrinkMode" ) {
	      vActor->SetShrinkable( true );
	      vActor->SetShrink();
	    }

	    else if ( paramName == "ShrinkFactor" )
	      vActor->SetShrinkFactor( val.toFloat() );

	    else if ( paramName == "Shading" ) {
	      if ( VISU_ScalarMapAct* scalarMapActor = dynamic_cast<VISU_ScalarMapAct*>( vActor ) )
		scalarMapActor->SetShading();
	    }

	    else if ( paramName.find( "ClippingPlane" ) != std::string::npos ) {
	      VISU::Prs3d_i* prs = vActor->GetPrs3d();
	      if ( !prs )
		continue;
	      
	      //prs->RemoveAllClippingPlanes();
	      for (int i = prs->GetNumberOfClippingPlanes() - 1; i >= 0 ; i--) {
		OrientedPlane* aPlane = dynamic_cast<OrientedPlane*>(prs->GetClippingPlane(i));
		if (aPlane) 
		  prs->RemoveClippingPlane(i);
	      }
	      if ( val != "Off" ) {
		QStringList vals = val.split( gDigitsSep, QString::SkipEmptyParts );
		if ( vals.count() == 6 ) { // format check: 6 float values
		  vtkFloatingPointType normal[3], origin[3];
		  for (int x = 0; x < 3; x++ ) {
		    normal[x] = vals[x].toFloat();
		    origin[x] = vals[x+3].toFloat();
		  }
		  OrientedPlane* plane = OrientedPlane::New();
		  plane->SetNormal( normal );
		  plane->SetOrigin( origin );
		  prs->AddClippingPlane( plane );
		  plane->Delete();
		}
	      }
	    }
	  } // if ( vActor ) --> salome_actor successfull downcast to the VISU_Actor
	  else if ( vP3dActor ) {
	    // salome actor is downcasted to the VISU_PointMap3dActor
	    if ( paramName == "Name" )
	      vP3dActor->setName( val.toLatin1().data() );

	    else if ( paramName == "RepresentationMode" )
	      vP3dActor->SetRepresentation( val.toInt() );

	    else if ( paramName == "Opacity" )
	      vP3dActor->SetOpacity( val.toFloat() );

	    else if ( paramName == "Color" ) {
	      QStringList colors = val.split( gDigitsSep, QString::SkipEmptyParts );
	      if ( colors.count() == 3 )
		vP3dActor->SetColor( colors[0].toFloat(), colors[1].toFloat(), colors[2].toFloat() );
	    }

	    else if ( paramName == "LineWidth" )
	      vP3dActor->SetLineWidth( val.toFloat() );

	    else if ( paramName == "ShrinkMode" ) {
	      vP3dActor->SetShrinkable( true );
	      vP3dActor->SetShrink();
	    }

	    else if ( paramName == "ShrinkFactor" )
	      vP3dActor->SetShrinkFactor( val.toFloat() );

	  } // else if ( vP3dActor ) --> salome actor is downcasted to the VISU_PointMap3dActor
        } // else ..
      } // if SVTK

      else if ( viewerType == SPlot2d_Viewer::Type().toLatin1().data() ) {

        if ( paramName == "Visibility" && displayer() ) {
          QList<SUIT_ViewManager*> lst;
          getApp()->viewManagers( viewerType.c_str(), lst );

          if ( viewIndex >= 0 && viewIndex < lst.count() ) {
            SUIT_ViewManager* vman = lst.at( viewIndex );
            SUIT_ViewModel* vmodel = vman->getViewModel();
            // SVTK and VVTK view models can be casted to SALOME_View
            displayer()->Display( entry, true, dynamic_cast<SALOME_View*>( vmodel ) );
          }
        }

      } // if SPlot2d

    } // for names/parameters iterator
  } // for entries iterator

  // [ update all SVTK/VVTK/Plot2D views
  QList<SUIT_ViewManager*> lst;
  getApp()->viewManagers( lst );
  for ( QList<SUIT_ViewManager*>::Iterator it = lst.begin(); it != lst.end(); it++ ) {
    SUIT_ViewModel* vmodel = (*it)->getViewModel();
    if ( !vmodel )
      continue;
    if ( vmodel->getType() == SVTK_Viewer::Type() /*||  // processing SVTK and VVTK viewers
                                                    vmodel->getType() == VVTK_Viewer::Type()*/ ) { // in the same way 
      SVTK_ViewWindow* vtkView = (SVTK_ViewWindow*) (*it)->getActiveView();
      vtkView->getRenderer()->ResetCameraClippingRange();
      vtkView->Repaint();
    }
    else if ( vmodel->getType() == SPlot2d_Viewer::Type().toLatin1().data() ) {
      Plot2d_ViewWindow* plotView = (Plot2d_ViewWindow*) (*it)->getActiveView();
      plotView->getViewFrame()->Repaint();
    }
  } // ] end of update views

  // VISU module opens one SVTK viewer in activateModule().  This causes a bug in save-restore visual
  // parameters: it no SVTK view was saved, we need NOT any SVTK on restore.  Here we close one
  // default SVTK if needed.
  /*
  QString openedSvtkViewerStr = ip->getProperty( "VtkViewersCount" ).c_str();
  int openedSvtkViewer = openedSvtkViewerStr.toInt( &ok );
  if ( ok && openedSvtkViewer == 0 ) {
    lst.clear();
    getApp()->viewManagers( SVTK_Viewer::Type(), lst );
    if ( lst.count() )
      lst.at( 0 )->closeAllViews();
  }
  */

  // if active Gauss Viewer is set ( != -1) then raise the gauss view window.
//   bool ok;
//   QString activeGaussViewerStr = ip->getProperty( "ActiveGaussViewer" ).c_str();
//   int activeGaussViewer = activeGaussViewerStr.toInt( &ok );
//   if ( ok && activeGaussViewer != -1 ) {
//     lst.clear();
//     getApp()->viewManagers( VVTK_Viewer::Type(), lst );
//     if ( activeGaussViewer >= 0 && activeGaussViewer < lst.count() ) {
//       SUIT_ViewWindow* activeView = lst.at( activeGaussViewer )->getActiveView();
//       if ( activeView ) {
//      activeView->activateWindow();
//      //activeView->setFocus();
//       }          
//     }
//   }
}

