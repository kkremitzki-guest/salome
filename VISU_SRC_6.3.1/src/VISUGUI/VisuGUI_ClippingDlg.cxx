// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "VisuGUI_ClippingDlg.h"

#include "VisuGUI.h"
#include "VisuGUI_Tools.h"
#include "VisuGUI_ViewTools.h"

#include "VISU_Prs3d_i.hh"
#include "VISU_Result_i.hh"
#include "VISU_ColoredPrs3dHolder_i.hh"

#include "VISU_PipeLine.hxx"
#include "VISU_PipeLineUtils.hxx"
#include "VISU_DataSetActor.h"

#include <SalomeApp_IntSpinBox.h>
#include <SalomeApp_DoubleSpinBox.h>

#include "LightApp_SelectionMgr.h"
#include "LightApp_Application.h"

#include "SVTK_ViewWindow.h"
#include <VTKViewer_Utilities.h>

#include "SUIT_Session.h"
#include "SUIT_Desktop.h"
#include "SUIT_MessageBox.h"
#include "SUIT_ResourceMgr.h"
#include "SUIT_OverrideCursor.h"

#include "SALOME_Actor.h"
#include "VISU_ViewManager_i.hh"

// QT Includes
#include <QLabel>
#include <QComboBox>
#include <QCheckBox>
#include <QLayout>
#include <QGroupBox>
#include <QButtonGroup>
#include <QValidator>
#include <QTabWidget>
#include <QRadioButton>
#include <QKeyEvent>
#include <QPushButton>
#include <QListWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QStackedWidget>

// VTK Includes
#include <vtkMath.h>
#include <vtkCamera.h>
#include <vtkRenderer.h>
#include <vtkDataSet.h>
#include <vtkDataSetMapper.h>
#include <vtkImplicitFunction.h>
#include <vtkPlaneSource.h>
#include <vtkPolyData.h>
#include <vtkUnstructuredGrid.h>
#include <vtkProperty.h>
#include <vtkImplicitFunctionCollection.h>

// OCCT Includes
#include <gp_Dir.hxx>

using namespace std;

namespace VISU {
  float GetFloat (const QString& theValue, float theDefault)
  {
    if (theValue.isEmpty()) return theDefault;
    SUIT_ResourceMgr* aResourceMgr = VISU::GetResourceMgr();
    QString aValue = aResourceMgr->stringValue("VISU",theValue);
    if (aValue.isEmpty()) return theDefault;
    return aValue.toFloat();
  }

  void RenderViewWindow (SVTK_ViewWindow* vw)
  {
    if (vw) {
      vw->getRenderer()->ResetCameraClippingRange();
      vw->Repaint();
    }
  }
};

//=================================================================================
//class    : OrientedPlane
//purpose  :
//=================================================================================
OrientedPlane* OrientedPlane::New() 
{
  return new OrientedPlane();
}

OrientedPlane* OrientedPlane::New (SVTK_ViewWindow* vw) 
{
  return new OrientedPlane(vw);
}

void OrientedPlane::SetOrientation(VISU::Orientation theOrientation) 
{
  myOrientation = theOrientation;
}

VISU::Orientation OrientedPlane::GetOrientation() 
{
  return myOrientation;
}

void OrientedPlane::SetDistance(float theDistance) 
{
  myDistance = theDistance;
}

float OrientedPlane::GetDistance() 
{
  return myDistance;
}

void OrientedPlane::ShallowCopy(OrientedPlane* theOrientedPlane)
{
  SetNormal(theOrientedPlane->GetNormal());
  SetOrigin(theOrientedPlane->GetOrigin());
  
  myOrientation = theOrientedPlane->GetOrientation();
  myDistance = theOrientedPlane->GetDistance();
  
  myAngle[0] = theOrientedPlane->myAngle[0];
  myAngle[1] = theOrientedPlane->myAngle[1];
  
  myPlaneSource->SetNormal(theOrientedPlane->myPlaneSource->GetNormal());
  myPlaneSource->SetOrigin(theOrientedPlane->myPlaneSource->GetOrigin());
  myPlaneSource->SetPoint1(theOrientedPlane->myPlaneSource->GetPoint1());
  myPlaneSource->SetPoint2(theOrientedPlane->myPlaneSource->GetPoint2());
}

OrientedPlane::OrientedPlane(SVTK_ViewWindow* vw):
  myOrientation(VISU::XY),
  myDistance(0.5),
  myViewWindow(vw)
{
  Init();
  myViewWindow->AddActor(myActor, false, false); // don't adjust actors
}

OrientedPlane::OrientedPlane():
  myOrientation(VISU::XY),
  myDistance(0.5),
  myViewWindow(NULL)
{
  Init();
}

void OrientedPlane::Init()
{
  myPlaneSource = vtkPlaneSource::New();
  
  myAngle[0] = myAngle[1] = 0.0;

  // Create and display actor
  myMapper = vtkDataSetMapper::New();
  myMapper->SetInput(myPlaneSource->GetOutput());

  myActor = SALOME_Actor::New();
  myActor->VisibilityOff();
  myActor->PickableOff();
  myActor->SetInfinitive(true);
  myActor->SetMapper(myMapper);
  
  vtkProperty* aProp = vtkProperty::New();
  float anRGB[3];
  
  SUIT_ResourceMgr* aResourceMgr = VISU::GetResourceMgr();
  
  QColor aFillColor = aResourceMgr->colorValue("SMESH", "fill_color", QColor(0, 170, 255));
  anRGB[0] = aFillColor.red()/255.;
  anRGB[1] = aFillColor.green()/255.;
  anRGB[2] = aFillColor.blue()/255.;
  aProp->SetColor(anRGB[0],anRGB[1],anRGB[2]);
  aProp->SetOpacity(0.75);
  myActor->SetProperty(aProp);
  aProp->Delete();
  
  vtkProperty* aBackProp = vtkProperty::New();
  QColor aBackFaceColor = aResourceMgr->colorValue("SMESH", "backface_color", QColor(0, 0, 255));//@
  anRGB[0] = aBackFaceColor.red()/255.;
  anRGB[1] = aBackFaceColor.green()/255.;
  anRGB[2] = aBackFaceColor.blue()/255.;
  aBackProp->SetColor(anRGB[0],anRGB[1],anRGB[2]);
  aBackProp->SetOpacity(0.75);
  myActor->SetBackfaceProperty(aBackProp);
  aBackProp->Delete();
}

OrientedPlane::~OrientedPlane()
{
  if ( !myViewWindow.isNull() )
    myViewWindow->RemoveActor(myActor);

  myActor->Delete();

  myMapper->RemoveAllInputs();
  myMapper->Delete();
  
  // commented: porting to vtk 5.0
  //myPlaneSource->UnRegisterAllOutputs();
  myPlaneSource->Delete();
}

struct TSetVisiblity {
  TSetVisiblity(int theIsVisible): myIsVisible(theIsVisible){}
  void operator()(VISU::TVTKPlane& theOrientedPlane){
    theOrientedPlane->myActor->SetVisibility(myIsVisible);
  }
  int myIsVisible;
};

//=================================================================================
// class    : VisuGUI_ClippingDlg()
// purpose  :
//
//=================================================================================
VisuGUI_ClippingDlg::VisuGUI_ClippingDlg (VisuGUI* theModule,
                                          bool modal )
  : QDialog(VISU::GetDesktop(theModule), Qt::WindowTitleHint | Qt::WindowSystemMenuHint ),
    mySelectionMgr(VISU::GetSelectionMgr(theModule)),
    myVisuGUI(theModule),
    myPrs3d(0),
    myIsSelectPlane(false),
    myDSActor(0)
{
  setWindowTitle(tr("TITLE"));
  setSizeGripEnabled(TRUE);
  setAttribute( Qt::WA_DeleteOnClose, true );

  QVBoxLayout* VisuGUI_ClippingDlgLayout = new QVBoxLayout(this);
  VisuGUI_ClippingDlgLayout->setSpacing(6);
  VisuGUI_ClippingDlgLayout->setMargin(11);
  
  QStackedWidget* aStackWidget = new QStackedWidget(this);
  VisuGUI_ClippingDlgLayout->addWidget(aStackWidget);
  // Local planes
  QWidget* aLocalPlanes = new QWidget(aStackWidget);
  QVBoxLayout* aLocalLayout = new QVBoxLayout(aLocalPlanes);
  aStackWidget->addWidget(aLocalPlanes);

  // Controls for selecting, creating, deleting planes
  QGroupBox* GroupPlanes = new QGroupBox (tr("GRP_PLANES"),  aLocalPlanes);
  QGridLayout* GroupPlanesLayout = new QGridLayout (GroupPlanes);
  GroupPlanesLayout->setAlignment(Qt::AlignTop);
  GroupPlanesLayout->setSpacing(6);
  GroupPlanesLayout->setMargin(11);
  aLocalLayout->addWidget(GroupPlanes);

  ComboBoxPlanes = new QComboBox (GroupPlanes);
  GroupPlanesLayout->addWidget(ComboBoxPlanes, 0, 0);

  QSpacerItem* spacerGP = new QSpacerItem (20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);
  GroupPlanesLayout->addItem(spacerGP, 0, 1);

  buttonNew = new QPushButton (GroupPlanes );
  buttonNew->setText(tr("BUT_NEW"));
  GroupPlanesLayout->addWidget(buttonNew, 0, 2);

  buttonDelete = new QPushButton(GroupPlanes);
  buttonDelete->setText(tr("BUT_DELETE"));
  GroupPlanesLayout->addWidget(buttonDelete, 0, 3);

  // Controls for defining plane parameters

  // Tab pane
  QGroupBox* GroupParameters = new QGroupBox(tr("GRP_PARAMETERS"), aLocalPlanes);
  QGridLayout* GroupParametersLayout = new QGridLayout (GroupParameters);
  GroupParametersLayout->setAlignment(Qt::AlignTop);
  GroupParametersLayout->setSpacing(6);
  GroupParametersLayout->setMargin(11);
  aLocalLayout->addWidget(GroupParameters);

  TabPane = new QTabWidget (GroupParameters);
  TabPane->addTab(createParamsTab()   , tr("TAB_NON_STRUCTURED"));
  TabPane->addTab(createIJKParamsTab(), tr("TAB_IJK_STRUCTURED"));
  GroupParametersLayout->addWidget(TabPane, 0, 0);





  // "Show preview" and "Auto Apply" check boxes
  QHBoxLayout* aCheckBoxLayout = new QHBoxLayout(this);
  VisuGUI_ClippingDlgLayout->addLayout(aCheckBoxLayout);

  PreviewCheckBox = new QCheckBox (tr("SHOW_PREVIEW_CHK"), this);
  PreviewCheckBox->setChecked(true);
  aCheckBoxLayout->addWidget(PreviewCheckBox);
  aCheckBoxLayout->addStretch();

  AutoApplyCheckBox = new QCheckBox (tr("AUTO_APPLY_CHK"), this);
  AutoApplyCheckBox->setChecked(false);
  aCheckBoxLayout->addWidget(AutoApplyCheckBox);

  // Controls for "Ok", "Apply" and "Close" button
  QGroupBox* GroupButtons = new QGroupBox (this);
  VisuGUI_ClippingDlgLayout->addWidget(GroupButtons);
  QSizePolicy aSizePolicy(QSizePolicy::Expanding,
                          QSizePolicy::Fixed );
  aSizePolicy.setHeightForWidth( GroupButtons->sizePolicy().hasHeightForWidth() );
  aSizePolicy.setHorizontalStretch( 0 );
  aSizePolicy.setVerticalStretch( 0 );
  GroupButtons->setSizePolicy( aSizePolicy );
  GroupButtons->setGeometry(QRect(10, 10, 281, 48));
  QGridLayout* GroupButtonsLayout = new QGridLayout (GroupButtons);
  GroupButtons->setLayout(GroupButtonsLayout);
  GroupButtonsLayout->setAlignment(Qt::AlignTop);
  GroupButtonsLayout->setSpacing(6);
  GroupButtonsLayout->setMargin(11);
  buttonHelp = new QPushButton (GroupButtons);
  buttonHelp->setText(tr("BUT_HELP"));
  buttonHelp->setAutoDefault(TRUE);
  GroupButtonsLayout->addWidget(buttonHelp, 0, 4);
  buttonCancel = new QPushButton (GroupButtons);
  buttonCancel->setText(tr("BUT_CLOSE"));
  buttonCancel->setAutoDefault(TRUE);
  GroupButtonsLayout->addWidget(buttonCancel, 0, 3);
  buttonApply = new QPushButton (GroupButtons);
  buttonApply->setText(tr("BUT_APPLY"));
  buttonApply->setAutoDefault(TRUE);
  GroupButtonsLayout->addWidget(buttonApply, 0, 1);
  QSpacerItem* spacer_9 = new QSpacerItem (20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);
  GroupButtonsLayout->addItem(spacer_9, 0, 2);
  buttonOk = new QPushButton (GroupButtons);
  buttonOk->setText(tr("BUT_OK"));
  buttonOk->setAutoDefault(TRUE);
  buttonOk->setDefault(TRUE);
  GroupButtonsLayout->addWidget(buttonOk, 0, 0);

  // Initial state
  VISU::initSpinBox( SpinBoxDistance, 0., 1., .01, "length_precision" );
  VISU::initSpinBox( SpinBoxRot1, -180., 180., 1., "angle_precision" );
  VISU::initSpinBox( SpinBoxRot2, -180., 180., 1., "angle_precision" );  

  ComboBoxOrientation->addItem(tr("PARALLEL_XOY_COMBO_ITEM"));
  ComboBoxOrientation->addItem(tr("PARALLEL_YOZ_COMBO_ITEM"));
  ComboBoxOrientation->addItem(tr("PARALLEL_ZOX_COMBO_ITEM"));

  SpinBoxDistance->setValue(0.5);

  onSelectionChanged();

  // signals and slots connections :
  connect(ComboBoxPlanes         , SIGNAL(activated(int))           , this, SLOT(onSelectPlane(int)));
  connect(buttonNew              , SIGNAL(clicked())                , this, SLOT(ClickOnNew()));
  connect(buttonDelete           , SIGNAL(clicked())                , this, SLOT(ClickOnDelete()));
  connect(ComboBoxOrientation    , SIGNAL(activated(int))           , this, SLOT(onSelectOrientation(int)));
  connect(SpinBoxDistance        , SIGNAL(valueChanged(double))     , this, SLOT(SetCurrentPlaneParam()));
  connect(SpinBoxRot1            , SIGNAL(valueChanged(double))     , this, SLOT(SetCurrentPlaneParam()));
  connect(SpinBoxRot2            , SIGNAL(valueChanged(double))     , this, SLOT(SetCurrentPlaneParam()));
  connect(ButtonGroupIJKAxis     , SIGNAL(buttonClicked(int))             , this, SLOT(onIJKAxisChanged(int)));
  connect(SpinBoxIJKIndex        , SIGNAL(valueChanged(int))        , this, SLOT(SetCurrentPlaneIJKParam()));
  connect(CheckBoxIJKPlaneReverse, SIGNAL(toggled(bool))            , this, SLOT(SetCurrentPlaneIJKParam()));
  connect(TabPane                , SIGNAL(currentChanged (QWidget*)), this, SLOT(onTabChanged(QWidget*)));

  connect(PreviewCheckBox  , SIGNAL(toggled(bool)), this, SLOT(OnPreviewToggle(bool)));
  connect(AutoApplyCheckBox, SIGNAL(toggled(bool)), this, SLOT(ClickOnApply()));

  connect(buttonOk    , SIGNAL(clicked()), this, SLOT(ClickOnOk()));
  connect(buttonApply , SIGNAL(clicked()), this, SLOT(ClickOnApply()));
  connect(buttonCancel, SIGNAL(clicked()), this, SLOT(ClickOnCancel()));
  connect(buttonHelp  , SIGNAL(clicked()), this, SLOT(ClickOnHelp()));

  connect(mySelectionMgr, SIGNAL(currentSelectionChanged()), this, SLOT(onSelectionChanged()));

  this->show();
}

//=================================================================================
// function : ~VisuGUI_ClippingDlg()
// purpose  :
//=================================================================================
VisuGUI_ClippingDlg::~VisuGUI_ClippingDlg()
{
  // no need to delete child widgets, Qt does it all for us
  SetPrs3d(NULL);
  std::for_each(myPlanes.begin(),myPlanes.end(),TSetVisiblity(false));
  VISU::RenderViewWindow(VISU::GetActiveViewWindow<SVTK_ViewWindow>(myVisuGUI));
}

//=================================================================================
// function : createParamsTab
// purpose  :
//=================================================================================
QWidget* VisuGUI_ClippingDlg::createParamsTab()
{
  QFrame* GroupParameters = new QFrame(this);
  QGridLayout* GroupParametersLayout = new QGridLayout(GroupParameters);
  GroupParametersLayout->setAlignment(Qt::AlignTop);
  GroupParametersLayout->setSpacing(6);
  GroupParametersLayout->setMargin(11);

  TextLabelOrientation = new QLabel(GroupParameters);
  TextLabelOrientation->setText(tr("LBL_ORIENTATION"));
  GroupParametersLayout->addWidget(TextLabelOrientation, 0, 0);

  ComboBoxOrientation = new QComboBox(GroupParameters);
  GroupParametersLayout->addWidget(ComboBoxOrientation, 0, 1);

  TextLabelDistance = new QLabel(GroupParameters);
  TextLabelDistance->setText(tr("LBL_DISTANCE"));
  GroupParametersLayout->addWidget(TextLabelDistance, 1, 0);

  SpinBoxDistance = new SalomeApp_DoubleSpinBox(GroupParameters);
  GroupParametersLayout->addWidget(SpinBoxDistance, 1, 1);

  TextLabelRot1 = new QLabel(GroupParameters);
  TextLabelRot1->setText(tr("LBL_ROTATION_YZ"));
  GroupParametersLayout->addWidget(TextLabelRot1, 2, 0);

  SpinBoxRot1 = new SalomeApp_DoubleSpinBox(GroupParameters);
  GroupParametersLayout->addWidget(SpinBoxRot1, 2, 1);

  TextLabelRot2 = new QLabel(GroupParameters);
  TextLabelRot2->setText(tr("LBL_ROTATION_XZ"));
  GroupParametersLayout->addWidget(TextLabelRot2, 3, 0);

  SpinBoxRot2 = new SalomeApp_DoubleSpinBox(GroupParameters);
  GroupParametersLayout->addWidget(SpinBoxRot2, 3, 1);

  return GroupParameters;
}

//=================================================================================
// function : createIJKParamsTab
// purpose  :
//=================================================================================
QWidget* VisuGUI_ClippingDlg::createIJKParamsTab()
{
  // tab layout
  WidgetIJKTab = new QFrame(this);
  QGridLayout* IJKParametersLayout = new QGridLayout(WidgetIJKTab);
  IJKParametersLayout->setAlignment(Qt::AlignTop);
  IJKParametersLayout->setSpacing(6);
  IJKParametersLayout->setMargin(11);

  // Axis group
  ButtonGroupIJKAxis = new QButtonGroup ( WidgetIJKTab);
  //QGroupBox* aGBGroupBoxIJKAxis= new QGroupBox(tr("GRP_IJK_AXIS"), WidgetIJKTab );
  GroupBoxIJKAxis= new QGroupBox(tr("GRP_IJK_AXIS"), WidgetIJKTab );
  QHBoxLayout* aHBLay = new QHBoxLayout( GroupBoxIJKAxis  );
  ButtonGroupIJKAxis->addButton( new QRadioButton (tr("I_RADIO_BTN"), GroupBoxIJKAxis), 0 );  // 0
  ButtonGroupIJKAxis->addButton( new QRadioButton (tr("J_RADIO_BTN"), GroupBoxIJKAxis), 1 );  // 1
  ButtonGroupIJKAxis->addButton( new QRadioButton (tr("K_RADIO_BTN"), GroupBoxIJKAxis), 2 );  // 2
  ButtonGroupIJKAxis->button(0)->setChecked(true);
  aHBLay->addWidget( ButtonGroupIJKAxis->button(0) );
  aHBLay->addWidget( ButtonGroupIJKAxis->button(1) );
  aHBLay->addWidget( ButtonGroupIJKAxis->button(2) );  

  // Index
  TextLabelIJKIndex = new QLabel(WidgetIJKTab);
  TextLabelIJKIndex->setText(tr("LBL_IJK_INDEX"));
  SpinBoxIJKIndex = new SalomeApp_IntSpinBox(WidgetIJKTab);
  SpinBoxIJKIndex->setAcceptNames( false );

  // Orientation
  CheckBoxIJKPlaneReverse = new QCheckBox (tr("REVERSE_NORMAL_CHK"), WidgetIJKTab);
  CheckBoxIJKPlaneReverse->setChecked(false);

  IJKParametersLayout->addWidget(GroupBoxIJKAxis, 0, 0, 1, 2);
  IJKParametersLayout->addWidget(TextLabelIJKIndex,          1, 0);
  IJKParametersLayout->addWidget(SpinBoxIJKIndex,            1, 1);
  IJKParametersLayout->addWidget(CheckBoxIJKPlaneReverse, 2, 0);

  return WidgetIJKTab;
}

//=================================================================================
// function : ClickOnApply()
// purpose  :
//=================================================================================
void VisuGUI_ClippingDlg::ClickOnApply()
{
  applyLocalPlanes();
}


//=================================================================================
// function : applyLocalPlanes()
// purpose  :
//=================================================================================
void VisuGUI_ClippingDlg::applyLocalPlanes()
{
  if (!myPrs3d)
    return;

  if (SVTK_ViewWindow* aViewWindow = VISU::GetActiveViewWindow<SVTK_ViewWindow>(myVisuGUI)) {
    SUIT_OverrideCursor wc;

    QWidget *aCurrWid = this->focusWidget();
    aCurrWid->clearFocus();
    aCurrWid->setFocus();

    // Save clipping planes, currently applied to the presentation
    // to enable restoring this state in case of failure.
    // Refer to bugs IPAL8849, IPAL8850 for more information.
    typedef vtkSmartPointer<vtkPlane> TPln;
    typedef std::vector<TPln> TPlns;
    bool isFailed = false;
    TPlns anOldPlanes;
    int iopl = 0, nbOldPlanes = myPrs3d->GetNumberOfClippingPlanes();
    for (; iopl < nbOldPlanes; iopl++) {
      anOldPlanes.push_back(myPrs3d->GetClippingPlane(iopl));
    }

    // Try to apply new clipping
    //myPrs3d->RemoveAllClippingPlanes();
    removeAllClippingPlanes(myPrs3d);

    VISU::TPlanes::iterator anIter = myPlanes.begin();
    for (; anIter != myPlanes.end(); anIter++) {
      OrientedPlane* anOrientedPlane = OrientedPlane::New(aViewWindow);
      anOrientedPlane->ShallowCopy(anIter->GetPointer());
      if (!myPrs3d->AddClippingPlane(anOrientedPlane)) {
        isFailed = true;
      }
      anOrientedPlane->Delete();
    }

    // Check contents of the resulting (clipped) presentation data
    if (!isFailed) {
      VISU_PipeLine* aPL = myPrs3d->GetPipeLine();
      vtkMapper* aMapper = aPL->GetMapper();
      vtkDataSet* aPrsData = aMapper->GetInput();
      aPrsData->Update();
      if (aPrsData->GetNumberOfCells() < 1) {
        isFailed = true;
      }
    }

    if (isFailed) {
      // Restore previous clipping state because of failure.
      //myPrs3d->RemoveAllClippingPlanes();
      removeAllClippingPlanes(myPrs3d);

      TPlns::iterator anOldIter = anOldPlanes.begin();
      for (; anOldIter != anOldPlanes.end(); anOldIter++) {
        myPrs3d->AddClippingPlane(anOldIter->GetPointer());
      }

      SUIT_MessageBox::warning(VISU::GetDesktop(myVisuGUI),
                               tr("WRN_VISU"),
                               tr("WRN_EMPTY_RESULTING_PRS"),
                               tr("BUT_OK") );
    }

    //VISU::RenderViewWindow(aViewWindow);
    VISU::RepaintViewWindows(myVisuGUI, myIO);
  }
}




//=================================================================================
// function : ClickOnOk()
// purpose  :
//=================================================================================
void VisuGUI_ClippingDlg::ClickOnOk()
{
  ClickOnApply();
  ClickOnCancel();
}

//=================================================================================
// function : ClickOnCancel()
// purpose  :
//=================================================================================
void VisuGUI_ClippingDlg::ClickOnCancel()
{
  close();
}

//=================================================================================
// function : ClickOnHelp()
// purpose  :
//=================================================================================
void VisuGUI_ClippingDlg::ClickOnHelp()
{
  QString aHelpFileName = "clipping_page.html";
  LightApp_Application* app = (LightApp_Application*)(SUIT_Session::session()->activeApplication());
  if (app)
    app->onHelpContextModule(myVisuGUI ? app->moduleName(myVisuGUI->moduleName()) : QString(""), aHelpFileName);
  else {
    QString platform;
#ifdef WIN32
    platform = "winapplication";
#else
    platform = "application";
#endif
    SUIT_MessageBox::warning(0, QObject::tr("WRN_WARNING"),
                             QObject::tr("EXTERNAL_BROWSER_CANNOT_SHOW_PAGE").
                             arg(app->resourceMgr()->stringValue("ExternalBrowser", platform)).arg(aHelpFileName),
                             QObject::tr("BUT_OK"));
  }
}

//=================================================================================
// function : onSelectionChanged()
// purpose  : Called when selection is changed
//=================================================================================
void VisuGUI_ClippingDlg::onSelectionChanged()
{
  if(SVTK_ViewWindow* aViewWindow = VISU::GetActiveViewWindow<SVTK_ViewWindow>(myVisuGUI)){

    VISU::TSelectionInfo aSelectionInfo = VISU::GetSelectedObjects(myVisuGUI);
    if(aSelectionInfo.empty())
      return;

    VISU::TSelectionItem aSelectionItem = aSelectionInfo.front();
    VISU::Base_i* aBase = aSelectionItem.myObjectInfo.myBase;
    if(!aBase) 
      return;
  
    Handle(SALOME_InteractiveObject) anIO = aSelectionItem.myIO;
    if (!anIO.IsNull()) 
      myIO = anIO;

    //----
    // rnv: fix for issue 0020114 (EDF VISU 918 : Impossible to 
    // create a new clipping plane on field presentation)
    // set last visited presentation from holder as myPrs3d
    VISU::ColoredPrs3dHolder_i* aHolder = dynamic_cast<VISU::ColoredPrs3dHolder_i*>(aBase);
    VISU::Prs3d_i* aPrs3d = NULL;
    if(aHolder) 
      aPrs3d = aHolder->GetPrs3dDevice();
    else
      aPrs3d = dynamic_cast<VISU::Prs3d_i*>(aBase);
    //----
    
    SetPrs3d(aPrs3d);
    if (myPrs3d) {
      std::for_each(myPlanes.begin(),myPlanes.end(),TSetVisiblity(false));
      myPlanes.clear();

      CORBA::Float anOffset[3];
      myPrs3d->GetOffset(anOffset[0],anOffset[1],anOffset[2]);

      vtkIdType anId = 0, anEnd = myPrs3d->GetNumberOfClippingPlanes();
      for (; anId < anEnd; anId++) {
        if (vtkImplicitFunction* aFunction = myPrs3d->GetClippingPlane(anId)) {
          if (OrientedPlane* aPlane = OrientedPlane::SafeDownCast(aFunction)) {
            OrientedPlane* anOrientedPlane = OrientedPlane::New(aViewWindow);
            VISU::TVTKPlane aTVTKPlane(anOrientedPlane);
            anOrientedPlane->Delete();
            aTVTKPlane->ShallowCopy(aPlane);
            aTVTKPlane->myActor->SetPosition(anOffset[0],anOffset[1],anOffset[2]);
            myPlanes.push_back(aTVTKPlane);
          }
        }
      }

      std::for_each(myPlanes.begin(),myPlanes.end(),
                    TSetVisiblity(PreviewCheckBox->isChecked()));
    }

    // enable/disable IJK tab
    TabPane->setTabEnabled(TabPane->indexOf(WidgetIJKTab), isStructured());
    Sinchronize();
    VISU::RenderViewWindow(aViewWindow);
  }
}

//=================================================================================
// function : onSelectPlane()
// purpose  :
//=================================================================================
void VisuGUI_ClippingDlg::onSelectPlane(int theIndex)
{
  if (!myPrs3d || myPlanes.empty())
    return;

  OrientedPlane* aPlane = myPlanes[theIndex].GetPointer();

  // Orientation
  VISU::Orientation anOrientation = aPlane->GetOrientation();

  // Rotations
  double aRot[2] = {aPlane->myAngle[0], aPlane->myAngle[1]};

  // Set plane parameters in the dialog
  myIsSelectPlane = true;
  setDistance(aPlane->GetDistance());
  setRotation(aRot[0], aRot[1]);
  int item = 0;
  switch (anOrientation) {
  case VISU::XY: item = 0; break;
  case VISU::YZ: item = 1; break;
  case VISU::ZX: item = 2; break;
  }
  ComboBoxOrientation->setCurrentIndex(item);

  bool isIJK = (TabPane->currentWidget() == WidgetIJKTab);
  if (isIJK)
    setIJKByNonStructured();
  else
    onSelectOrientation(item);

  myIsSelectPlane = false;
}

//=================================================================================
// function : ClickOnNew()
// purpose  :
//=================================================================================
void VisuGUI_ClippingDlg::ClickOnNew()
{
  VISU::TSelectionInfo aSelectionInfo = VISU::GetSelectedObjects(myVisuGUI);
  if(aSelectionInfo.empty())
    return;

  const VISU::TSelectionItem& aSelectionItem = aSelectionInfo[0];
  if(!aSelectionItem.myObjectInfo.myBase) 
    return;

  SetCurrentPlaneParam();
  
  if (!myPrs3d)
    return;

  if (SVTK_ViewWindow* aViewWindow = VISU::GetActiveViewWindow<SVTK_ViewWindow>(myVisuGUI)) {
    OrientedPlane* aPlane = OrientedPlane::New(aViewWindow);
    VISU::TVTKPlane aTVTKPlane(aPlane);
    myPlanes.push_back(aTVTKPlane);

    CORBA::Float anOffset[3];
    myPrs3d->GetOffset(anOffset[0],anOffset[1],anOffset[2]);
    aTVTKPlane->myActor->SetPosition(anOffset[0],anOffset[1],anOffset[2]);

    if (PreviewCheckBox->isChecked())
      aTVTKPlane->myActor->VisibilityOn();

    Sinchronize();
    SetCurrentPlaneParam();
  }
}

//=================================================================================
// function : ClickOnDelete()
// purpose  :
//=================================================================================
void VisuGUI_ClippingDlg::ClickOnDelete()
{
  if (!myPrs3d || myPlanes.empty())
    return;

  int aPlaneIndex = ComboBoxPlanes->currentIndex();

  VISU::TPlanes::iterator anIter = myPlanes.begin() + aPlaneIndex;
  anIter->GetPointer()->myActor->SetVisibility(false);
  myPlanes.erase(anIter);

  if(AutoApplyCheckBox->isChecked())
    ClickOnApply();

  Sinchronize();
  if (SVTK_ViewWindow* aViewWindow = VISU::GetActiveViewWindow<SVTK_ViewWindow>(myVisuGUI))
    VISU::RenderViewWindow(aViewWindow);
}

//=================================================================================
// function : onSelectOrientation()
// purpose  :
//=================================================================================
void VisuGUI_ClippingDlg::onSelectOrientation(int theItem)
{
  if (myPlanes.empty())
    return;

  if      (theItem == 0) {
    TextLabelRot1->setText(tr("LBL_ROTATION_YZ"));
    TextLabelRot2->setText(tr("LBL_ROTATION_XZ"));
  }
  else if (theItem == 1) {
    TextLabelRot1->setText(tr("LBL_ROTATION_ZX"));
    TextLabelRot2->setText(tr("LBL_ROTATION_YX"));
  }
  else if (theItem == 2) {
    TextLabelRot1->setText(tr("LBL_ROTATION_XY"));
    TextLabelRot2->setText(tr("LBL_ROTATION_ZY"));
  }

  if((QComboBox*)sender() == ComboBoxOrientation)
    SetCurrentPlaneParam();
}

//=================================================================================
// function : Sinchronize()
// purpose  : update control values according to plane selection
//=================================================================================
void VisuGUI_ClippingDlg::Sinchronize()
{
  int aNbPlanes = myPlanes.size();
  ComboBoxPlanes->clear();

  QString aName;
  for (int i = 1; i<=aNbPlanes; i++) {
    aName = QString(tr("PLANES_COMBO_ITEM_i")).arg(i);
    ComboBoxPlanes->addItem(aName);
  }

  int aPos = ComboBoxPlanes->count() - 1;
  ComboBoxPlanes->setCurrentIndex(aPos);

  bool anIsControlsEnable = (aPos >= 0);
  if (anIsControlsEnable) {
    onSelectPlane(aPos);
  } else {
    ComboBoxPlanes->addItem(tr("PLANES_COMBO_ITEM_no"));
    SpinBoxRot1->setValue(0.0);
    SpinBoxRot2->setValue(0.0);
    SpinBoxDistance->setValue(0.5);
  }

  buttonDelete           ->setEnabled(anIsControlsEnable);
  //buttonApply            ->setEnabled(anIsControlsEnable);
  //  PreviewCheckBox        ->setEnabled(anIsControlsEnable);
  //  AutoApplyCheckBox      ->setEnabled(anIsControlsEnable);

  ComboBoxOrientation    ->setEnabled(anIsControlsEnable);
  SpinBoxDistance        ->setEnabled(anIsControlsEnable);
  SpinBoxRot1            ->setEnabled(anIsControlsEnable);
  SpinBoxRot2            ->setEnabled(anIsControlsEnable);

  GroupBoxIJKAxis        ->setEnabled(anIsControlsEnable);
  SpinBoxIJKIndex        ->setEnabled(anIsControlsEnable);
  CheckBoxIJKPlaneReverse->setEnabled(anIsControlsEnable);
  //ENK: 23.11.2006 - PAL13176 - EDF228 VISU : Enhancement of structured datas processing
  if ( myPrs3d ) {
    VISU_PipeLine* aPipeLine = myPrs3d->GetPipeLine();
    VISU::PIDMapper anIDMapper = aPipeLine->GetIDMapper();
    if ( anIDMapper->IsStructured() ) {
      VISU::TStructuredId aStructuredId = anIDMapper->GetStructure();
      ButtonGroupIJKAxis->button(0)->setEnabled( aStructuredId[0] >= 0 );
      ButtonGroupIJKAxis->button(1)->setEnabled( aStructuredId[1] >= 0 );
      ButtonGroupIJKAxis->button(2)->setEnabled( aStructuredId[2] >= 0 );
    }
  }
  //ENK: 23.11.2006
}

//=================================================================================
// function : setRotation()
// purpose  :
//=================================================================================
void VisuGUI_ClippingDlg::setRotation(const double theRot1, const double theRot2)
{
  SpinBoxRot1->setValue(theRot1);
  SpinBoxRot2->setValue(theRot2);
}

//=================================================================================
// function : SetCurrentPlaneParam()
// purpose  :
//=================================================================================
void VisuGUI_ClippingDlg::SetCurrentPlaneParam()
{
  if (myPlanes.empty() || myIsSelectPlane)
    return;

  int aCurPlaneIndex = ComboBoxPlanes->currentIndex();

  OrientedPlane* aPlane = myPlanes[aCurPlaneIndex].GetPointer();

  vtkFloatingPointType aNormal[3];
  VISU::Orientation anOrientation;
  vtkFloatingPointType aDir[3][3] = {{0, 0, 0}, {0, 0, 0}};
  {
    static double aCoeff = vtkMath::Pi()/180.0;

    vtkFloatingPointType aRot[2] = {getRotation1(), getRotation2()};
    aPlane->myAngle[0] = aRot[0];
    aPlane->myAngle[1] = aRot[1];

    vtkFloatingPointType anU[2] = {cos(aCoeff*aRot[0]), cos(aCoeff*aRot[1])};
    vtkFloatingPointType aV[2] = {sqrt(1.0-anU[0]*anU[0]), sqrt(1.0-anU[1]*anU[1])};
    aV[0] = aRot[0] > 0? aV[0]: -aV[0];
    aV[1] = aRot[1] > 0? aV[1]: -aV[1];

    switch (ComboBoxOrientation->currentIndex()) {
    case 0:
      anOrientation = VISU::XY;

      aDir[0][1] = anU[0];
      aDir[0][2] = aV[0];

      aDir[1][0] = anU[1];
      aDir[1][2] = aV[1];

      break;
    case 1:
      anOrientation = VISU::YZ;

      aDir[0][2] = anU[0];
      aDir[0][0] = aV[0];

      aDir[1][1] = anU[1];
      aDir[1][0] = aV[1];

      break;
    case 2:
      anOrientation = VISU::ZX;

      aDir[0][0] = anU[0];
      aDir[0][1] = aV[0];

      aDir[1][2] = anU[1];
      aDir[1][1] = aV[1];

      break;
    }

    vtkMath::Cross(aDir[1],aDir[0],aNormal);
    vtkMath::Normalize(aNormal);
    vtkMath::Cross(aNormal,aDir[1],aDir[0]);
  }

  aPlane->SetOrientation(anOrientation);
  aPlane->SetDistance(getDistance());

  myPrs3d->SetPlaneParam(aNormal, 1. - getDistance(), aPlane);

  //Get bounds of the visible part of the dataset
  vtkFloatingPointType aBounds[6];
  myPrs3d->GetPipeLine()->GetVisibleBounds(aBounds);

  //Get center 
  vtkFloatingPointType aPnt[3];
  VISU::ComputeBoxCenter(aBounds,aPnt);

  vtkFloatingPointType* anOrigin = aPlane->GetOrigin();
  
  //Get Length of the diagonal
  vtkFloatingPointType aDel = VISU::ComputeBoxDiagonal(aBounds)/2.0;
  
  vtkFloatingPointType aDelta[2][3] = {{aDir[0][0]*aDel, aDir[0][1]*aDel, aDir[0][2]*aDel},
                                       {aDir[1][0]*aDel, aDir[1][1]*aDel, aDir[1][2]*aDel}};
  vtkFloatingPointType aParam, aPnt0[3], aPnt1[3], aPnt2[3];
  
  vtkFloatingPointType aPnt01[3] = {aPnt[0] - aDelta[0][0] - aDelta[1][0],
                                    aPnt[1] - aDelta[0][1] - aDelta[1][1],
                                    aPnt[2] - aDelta[0][2] - aDelta[1][2]};
  vtkFloatingPointType aPnt02[3] = {aPnt01[0] + aNormal[0],
                                    aPnt01[1] + aNormal[1],
                                    aPnt01[2] + aNormal[2]};
  vtkPlane::IntersectWithLine(aPnt01,aPnt02,aNormal,anOrigin,aParam,aPnt0);

  vtkFloatingPointType aPnt11[3] = {aPnt[0] - aDelta[0][0] + aDelta[1][0],
                                    aPnt[1] - aDelta[0][1] + aDelta[1][1],
                                    aPnt[2] - aDelta[0][2] + aDelta[1][2]};
  vtkFloatingPointType aPnt12[3] = {aPnt11[0] + aNormal[0],
                                    aPnt11[1] + aNormal[1],
                                    aPnt11[2] + aNormal[2]};
  vtkPlane::IntersectWithLine(aPnt11,aPnt12,aNormal,anOrigin,aParam,aPnt1);

  vtkFloatingPointType aPnt21[3] = {aPnt[0] + aDelta[0][0] - aDelta[1][0],
                                    aPnt[1] + aDelta[0][1] - aDelta[1][1],
                                    aPnt[2] + aDelta[0][2] - aDelta[1][2]};
  vtkFloatingPointType aPnt22[3] = {aPnt21[0] + aNormal[0],
                                    aPnt21[1] + aNormal[1],
                                    aPnt21[2] + aNormal[2]};
  vtkPlane::IntersectWithLine(aPnt21,aPnt22,aNormal,anOrigin,aParam,aPnt2);

  vtkPlaneSource* aPlaneSource = aPlane->myPlaneSource;
  aPlaneSource->SetNormal(aNormal[0],aNormal[1],aNormal[2]);
  aPlaneSource->SetOrigin(aPnt0[0],aPnt0[1],aPnt0[2]);
  aPlaneSource->SetPoint1(aPnt1[0],aPnt1[1],aPnt1[2]);
  aPlaneSource->SetPoint2(aPnt2[0],aPnt2[1],aPnt2[2]);

  if (AutoApplyCheckBox->isChecked())
    ClickOnApply();

  if (SVTK_ViewWindow* vw = VISU::GetActiveViewWindow<SVTK_ViewWindow>(myVisuGUI))
    VISU::RenderViewWindow(vw);
}

//=================================================================================
// function : onTabChanged
// purpose  :
//=================================================================================
void VisuGUI_ClippingDlg::onTabChanged(QWidget* newTab)
{
  if (newTab == WidgetIJKTab) // IJK
    setIJKByNonStructured();
  else {
    // set correct labels of rotation spin boxes
    onSelectOrientation(ComboBoxOrientation->currentIndex());
  }
}

//=================================================================================
// function : SetCurrentPlaneIJKParam
// purpose  : set non structured parameters by IJK parameters
//=================================================================================
void VisuGUI_ClippingDlg::SetCurrentPlaneIJKParam()
{  
  if (myPlanes.empty() || myIsSelectPlane || !WidgetIJKTab->isEnabled())
    return;

  VISU::Result_i* result = myPrs3d ? myPrs3d->GetCResult() : 0;
  if (!result)
    return;

  // get axis data
  int i, axId = ButtonGroupIJKAxis->id (ButtonGroupIJKAxis->checkedButton());
  VISU::Result_i::TAxis axis = (VISU::Result_i::TAxis) axId;
  gp_Dir dir;
  CORBA::String_var aMeshName = myPrs3d->GetMeshName();
  const vector<vtkFloatingPointType> * values =
    result->GetAxisInfo(aMeshName.in(), axis, dir);
  if (!values)
    return;

  // find distance;
  int index = SpinBoxIJKIndex->value();
  vtkFloatingPointType distance = 0;
  if (index < values->size())
    distance = (*values)[ index ];

  // find id of axis closest to dir
  // 0  || X-Y - axis Z
  // 1  || Y-Z - azis X
  // 2  || Z-X - axiz Y
  double cos[3] = { gp::DZ() * dir, gp::DX() * dir, gp::DY() * dir };
  double maxCos = 0;
  for (i = 0; i < 3; ++i) {
    if (Abs(cos[ i ]) > Abs (maxCos)) {
      maxCos = cos[ i ];
      axId = i;
    }
  }
  // find rotation angles
  vtkFloatingPointType angle[2];
  int rotId[2] = {
    (axId == 0) ? 2 : axId - 1,
    (axId == 2) ? 0 : axId + 1
    };
  static double aCoeff = 180.0/vtkMath::Pi();
  for (i = 0; i < 2; ++i) {
    vtkFloatingPointType cosin = cos[ rotId[ i ]];
//     if (maxCos < 0)
//       cosin = -cosin;
    angle[ i ] = asin(cosin) * aCoeff;
//     if (maxCos < 0)
//       angle[ i ] += 180. * (angle[ i ] < 0 ? 1. : -1.);
  }
  if (CheckBoxIJKPlaneReverse->isChecked()) {
    angle[ 0 ] += 180. * (angle[ 0 ] < 0 ? 1. : -1.);
    distance = 1. - distance;
  }
//   if (maxCos < 0)
//     distance = 1. - distance;

  // set paramerets
  myIsSelectPlane = true;
  ComboBoxOrientation->setCurrentIndex(axId);
  setRotation(-angle[0], -angle[1]);
  setDistance(distance);
  myIsSelectPlane = false;

  SetCurrentPlaneParam();
}

//=================================================================================
// function : setIJKByNonStructured
// purpose  : convert current non structured parameters to structured ones
//=================================================================================
void VisuGUI_ClippingDlg::setIJKByNonStructured()
{
  if (!myPrs3d || myPlanes.empty() || !myPrs3d->GetCResult())
    return;

  // get plane normal
  int planeIndex = ComboBoxPlanes->currentIndex();
  OrientedPlane* plane = myPlanes[ planeIndex ].GetPointer();
  vtkPlaneSource* planeSource = plane->myPlaneSource;
  vtkFloatingPointType * planeNormal = planeSource->GetNormal();
  gp_Dir normal(planeNormal[0], planeNormal[1], planeNormal[2]);

  // find a grid axis most co-directed with plane normal
  // and cartesian axis most co-directed with plane normal
  int i, maxAx = 0, gridAxId = 0;
  gp_Dir dir, gridDir;
  double maxDot = 0;
  const vector<vtkFloatingPointType> *curValues, *values = 0;
  VISU::Result_i* result = myPrs3d->GetCResult();
  int aNbAxes = 3;
  VISU_PipeLine* aPipeLine = myPrs3d->GetPipeLine();
  VISU::PIDMapper anIDMapper = aPipeLine->GetIDMapper();
  if ( anIDMapper->IsStructured() && !anIDMapper->myIsPolarType )
    aNbAxes = anIDMapper->GetStructureDim();
  for (i = 0; i < aNbAxes; ++i) {
    VISU::Result_i::TAxis axis = (VISU::Result_i::TAxis) i;
    CORBA::String_var aMeshName = myPrs3d->GetMeshName();
    curValues = result->GetAxisInfo(aMeshName.in(), axis, dir);
    if (curValues) {
      double dot = normal * dir;
      //ENK: 23.11.2006 - PAL13176
      if(i==0){
        maxDot = dot;
        gridDir = dir;
        values = curValues;
        gridAxId = i;
      } else if (Abs(dot) >= Abs(maxDot)) {
        maxDot = dot;
        gridDir = dir;
        values = curValues;
        gridAxId = i;
      }
      //ENK: 23.11.2006
    }
    if (Abs (planeNormal[ maxAx ]) < Abs (planeNormal[ i ]))
      maxAx = i;
  }
  gp_XYZ axDir(0,0,0);
  axDir.SetCoord(maxAx + 1, 1.);

  // find index value
  double v = SpinBoxDistance->value();
  // reverse value?
   bool reverse = (normal * axDir < 0); // normal and axis are opposite
   if (gridDir * axDir < 0) // grid dir and axis are opposite
     reverse = !reverse;
   if (reverse)
     v = 1. - v;
  for (i = 0; i < values->size(); ++i)
    if ((*values)[ i ] > v)
      break;
  if (i == values->size())
    --i;
  if (i != 0 && (*values)[ i ] - v > v - (*values)[ i - 1])
    --i;

  // set control values
  onIJKAxisChanged(gridAxId); // first of all update label and range of index
  myIsSelectPlane = true;
  CheckBoxIJKPlaneReverse->setChecked(normal * axDir < 0);
  SpinBoxIJKIndex->setValue(i);
  ButtonGroupIJKAxis->button(gridAxId)->setChecked( true );
  myIsSelectPlane = false;

  SetCurrentPlaneIJKParam();
}

//=================================================================================
// function : isStructured
// purpose  : return true if mesh is structured
//=================================================================================
bool VisuGUI_ClippingDlg::isStructured() const
{
  VISU::Result_i* result = myPrs3d ? myPrs3d->GetCResult() : 0;
  if (result) {
    gp_Dir dir;
    return result->GetAxisInfo(myPrs3d->GetCMeshName(),
                               VISU::Result_i::AXIS_X,
                               dir);
  }
  return false;
}

//=================================================================================
// function : onIJKAxisChanged
// purpose  : update Index range and call SetCurrentPlaneParam()
//=================================================================================
void VisuGUI_ClippingDlg::onIJKAxisChanged(int axisId)
{
  // set index range
  int maxIndex = 0;
  VISU::Result_i* result = myPrs3d ? myPrs3d->GetCResult() : 0;
  if (result) {
    VISU::Result_i::TAxis axis = (VISU::Result_i::TAxis) axisId;
    gp_Dir dir;
    CORBA::String_var aMeshName = myPrs3d->GetMeshName();
    const vector<vtkFloatingPointType> * indices = result->GetAxisInfo(aMeshName.in(),
                                                                       axis, dir);
    if (indices)
      maxIndex = indices->size() - 1;
  }
  QString text = tr("LBL_IJK_INDEX_TO_arg").arg(maxIndex);
  TextLabelIJKIndex->setText(text);
  SpinBoxIJKIndex->setRange(0, maxIndex);

  if (SpinBoxIJKIndex->value() > maxIndex)
    SpinBoxIJKIndex->setValue(0);

  SetCurrentPlaneIJKParam();
}

//=================================================================================
// function : OnPreviewToggle()
// purpose  :
//=================================================================================
void VisuGUI_ClippingDlg::OnPreviewToggle (bool theIsToggled)
{
  std::for_each(myPlanes.begin(),myPlanes.end(),TSetVisiblity(theIsToggled));
  if (SVTK_ViewWindow* vw = VISU::GetActiveViewWindow<SVTK_ViewWindow>(myVisuGUI))
    VISU::RenderViewWindow(vw);
}


//=================================================================================
// function : keyPressEvent()
// purpose  :
//=================================================================================
void VisuGUI_ClippingDlg::keyPressEvent( QKeyEvent* e )
{
  QDialog::keyPressEvent( e );
  if ( e->isAccepted() )
    return;

  if ( e->key() == Qt::Key_F1 )
    {
      e->accept();
      ClickOnHelp();
    }
}

void VisuGUI_ClippingDlg::SetPrs3d(VISU::Prs3d_i* thePrs)
{
  if(thePrs != myPrs3d){
    if(myPrs3d)
      myPrs3d->UnRegister();
    if(thePrs)
      thePrs->Register();
    myPrs3d = thePrs;
  } else 
    return;
}


void VisuGUI_ClippingDlg::removeAllClippingPlanes(VISU::Prs3d_i* thePrs)
{
  for (int i = thePrs->GetNumberOfClippingPlanes() - 1; i >= 0 ; i--) {
    OrientedPlane* aPlane = dynamic_cast<OrientedPlane*>(thePrs->GetClippingPlane(i));
    if (aPlane) 
      thePrs->RemoveClippingPlane(i);
  }
}


