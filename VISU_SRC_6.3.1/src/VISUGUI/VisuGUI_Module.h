// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU VISUGUI : GUI of VISU component
//  File   : VisuGUI_Module.h
//  Author : 
//  Module : VISU
//
#ifndef VisuGUI_Module_HeaderFile
#define VisuGUI_Module_HeaderFile

#include "VisuGUI.h"
#include "STD_Application.h"
#include "SALOMEDSClient_SObject.hxx"

class SUIT_ViewManager;
class SVTK_ViewManager;
class SVTK_ViewWindow;

#include "MED_SharedPtr.hxx"

//! This class inherits base VisuGUI.
/*! Used to display, erase end edit presentations in the VVTK viewer. */
class VisuGUI_Module: public VisuGUI
{
  Q_OBJECT;

public:
  VisuGUI_Module();

  virtual
  ~VisuGUI_Module();

  //  virtual
  //  bool
  //  eventFilter( QObject * theWatched, QEvent * theEvent );

  //! Redifined method of the module initializing.
  virtual
  void
  initialize( CAM_Application* );

  //! Redefined method of creating prefernces.
  virtual 
  void 
  createPreferences();

  virtual
  void
  preferencesChanged( const QString&, const QString& );

  virtual
  SUIT_ViewManager*
  getViewManager(const QString& theType, 
                 const bool theIsCreate);

  virtual 
  void
  storeVisualParameters(int savePoint);
  
  virtual
  void
  restoreVisualParameters(int savePoint);

  //public slots:
  //! Reimplemented method of the module deactivation.
  //  virtual bool deactivateModule( SUIT_Study* );

  //! Reimplemented method of the module activation.
  //  virtual bool activateModule( SUIT_Study* );

protected:
  //! Create preferences for Gauss Points presentation.
  virtual 
  void 
  createGaussPointsPreferences();

  //! Create preferences for Outside Cursor Gauss Points presentations.
  virtual 
  void 
  createInsideCursorPreferences();

  virtual 
  void 
  createOutsideCursorPreferences();

  //! Create preferences for Picking.
  virtual 
  void 
  createPickingPreferences();

  //! Create preferences for Space Mouse.
  virtual 
  void 
  createSpaceMousePreferences();

  //! Create preferences for Recorder.
  virtual 
  void 
  createRecorderPreferences();

protected slots:
  //  SUIT_ViewManager*
  //  onCreateViewManager();

  void
  OnCreateGaussPoints();

  void
  OnViewCreated(SUIT_ViewWindow*);

  //  void 
  //OnViewManagerAdded(SUIT_ViewManager*);

  //! Reimplemented method of the Gauss Points edition.
  virtual
  void
  OnEditGaussPoints();

  //! Virtual method of saving configuration.
  //  virtual
  //  void
  //  OnSaveConfiguration();

  //! Virtual method of overwriting configuration.
  //  virtual
  //  void
  //  OnOverwriteConfiguration();

  //! Virtual method of restoring configuration.
  //  virtual
  //  void
  //  OnRestoreConfiguration();

protected:
  _PTR(SObject) myConfigSObject;

  void setProperty( SVTK_ViewWindow*, const QString& );  // set a property (speed_increment, etc ) for SVTK ViewWindow
  void setProperty( SVTK_ViewManager*, const QString& ); // set a property for SVTK ViewWindow // set only 1 property for all ViewWindows of given view manager

};

#endif
