// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  File   : VisuGUI_CutLinesDlg.h
//  Author : VSV
//  Module : VISU
//
#ifndef VISUGUI_CUTLINESDLG_H
#define VISUGUI_CUTLINESDLG_H

#include "VisuGUI_CutPlanesDlg.h"

#include <QCheckBox>

class QLineEdit;
class QTabWidget;

class SUIT_ViewManager;
class SalomeApp_Module;
class VisuGUI_InputPane;
class SalomeApp_DoubleSpinBox;

namespace VISU
{
  class CutLines_i;
}

class VisuGUI_CutLinesDlg: public VisuGUI_ScalarBarBaseDlg
{
    Q_OBJECT

public:
    VisuGUI_CutLinesDlg (SalomeApp_Module* theModule);
    ~VisuGUI_CutLinesDlg();

    virtual void initFromPrsObject( VISU::ColoredPrs3d_i* thePrs,
                                    bool theInit );

    virtual int  storeToPrsObject(VISU::ColoredPrs3d_i* thePrs);

    bool isGenerateTable() { return myCreateTable->isChecked(); }
    bool isGenerateCurves() { return myCreateTable->isChecked() && myCurvesCheck->isChecked(); }

protected:
  virtual QString GetContextHelpFilePath();
 
protected slots:
  void accept();
  void reject();

private slots:
  void onPlaneSelect (int theId);
  void onCutSelect (int theId, bool theUpdate = true);
  void setBaseDefault (bool theUpdate = true);
  void setDefault (int all = -1);
  void DrawTable();
  void onValueChanged (int theRow, int theCol);
  void onPreviewCheck (bool thePreview);
  void onAllCurvesInvertedCheck(bool theInvert);
  void onRotation (double theValue);
  void onPlanePos (const QString& theValue);

private:
  void createPlanes();
  void deletePlanes();
  void updateGlyphs(bool update);

  QButtonGroup* mySelPlane;
  SalomeApp_DoubleSpinBox* myRotXSpn;
  SalomeApp_DoubleSpinBox* myRotYSpn;
  QLabel* myRotXLbl;
  QLabel* myRotYLbl;
  SalomeApp_DoubleSpinBox* myPosSpn;
  QLineEdit* myBasePlanePos;
  QCheckBox* myCBSetDef;
  QCheckBox* myCreateTable;
  QCheckBox* myCurvesCheck;

  QButtonGroup* mySelPlane2;
  SalomeApp_DoubleSpinBox* myNbSpn;
  SalomeApp_DoubleSpinBox* myRotXSpn2;
  SalomeApp_DoubleSpinBox* myRotYSpn2;
  QLabel* myRotXLbl2;
  QLabel* myRotYLbl2;
  SalomeApp_DoubleSpinBox* myPosSpn2;
  QTableWidget* myPosTable;
  bool hasInit;

  QDoubleValidator *mydvalidator;
  SALOME::GenericObjPtr<VISU::CutLines_i> myCutLines;

  QTabWidget*            myTabBox;
  VisuGUI_InputPane*     myInputPane;

  VISU::CutPlanes::Orientation getOrientaion (bool IsBasePlane = true);
  void setOrientation (const VISU::CutPlanes::Orientation orient);
  void setOrientation2 (const VISU::CutPlanes::Orientation orient);

  SALOME_Actor* myPreviewActor;
  SALOME_Actor* myPreviewActorGlyphs;
  QCheckBox* myPreviewCheck;
  QCheckBox* myAllCurvesInvertedCheck;
  QCheckBox* myUseAbsoluteLengthCheck;

  SALOME::GenericObjPtr<VISU::CutLines_i> myPrsCopy;
};

#endif // VISUGUI_CUTLINESDLG_H
