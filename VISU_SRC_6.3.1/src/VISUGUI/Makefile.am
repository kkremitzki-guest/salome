# Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
# See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#

#  VISU VISUGUI : GUI of VISU component
#  File   : Makefile.am
#  Author : Marc Tajchman (CEA)
#  Module : VISU
#
include $(top_srcdir)/adm_local/unix/make_common_starter.am

lib_LTLIBRARIES= libVISU.la

# .po files to transform in .qm
nodist_salomeres_DATA= \
	VISU_msg_en.qm VISU_msg_fr.qm VISU_images.qm

salomeinclude_HEADERS= \
	VisuGUI_Tools.h \
	VisuGUI_Prs3dTools.h \
	VisuGUI_ActionsDef.h \
	VisuGUI.h \
	VisuGUI_Module.h \
	VisuGUI_Selection.h \
	VisuGUI_ViewTools.h \
	VisuGUI_NameDlg.h \
	VisuGUI_FileDlg.h \
	VisuGUI_CursorDlg.h \
	VisuGUI_TimeAnimation.h \
	VisuGUI_EvolutionDlg.h \
	VisuGUI_EditContainerDlg.h \
	VisuGUI_ClippingDlg.h \
	VisuGUI_DialogRunner.h \
	VisuGUI_OffsetDlg.h \
	VisuGUI_Prs3dDlg.h \
	VisuGUI_ScalarBarDlg.h \
	VisuGUI_Plot3DDlg.h \
	VisuGUI_GaussPointsDlg.h \
	VisuGUI_DeformedShapeDlg.h \
	VisuGUI_IsoSurfacesDlg.h \
	VisuGUI_CutLinesDlg.h \
	VisuGUI_CutSegmentDlg.h \
	VisuGUI_CutPlanesDlg.h \
	VisuGUI_StreamLinesDlg.h \
	VisuGUI_VectorsDlg.h \
	VisuGUI_Displayer.h \
	VisuGUI_DeformedShapeAndScalarMapDlg.h \
	VisuGUI_SetupPlot2dDlg.h \
	VisuGUI_BuildProgressDlg.h \
	VisuGUI_TransparencyDlg.h \
	VisuGUI_ShrinkFactorDlg.h \
	VisuGUI_Timer.h \
	VisuGUI_Slider.h \
	VisuGUI_InputPane.h \
	VisuGUI_FieldFilter.h \
	VisuGUI_ValuesLabelingDlg.h \
	VisuGUI_PrimitiveBox.h \
	VisuGUI_SizeBox.h

dist_libVISU_la_SOURCES= \
	VisuGUI.cxx \
	VisuGUI_Factory.cxx \
	VisuGUI_Module.cxx \
	VisuGUI_InputPanel.cxx \
	VisuGUI_BasePanel.cxx \
	VisuGUI_Panel.cxx \
	VisuGUI_SelectionPanel.cxx \
	VisuGUI_SelectionPrefDlg.cxx \
	VisuGUI_GaussPointsSelectionPane.cxx \
	VisuGUI_FindPane.cxx \
	VisuGUI_FeatureEdgesPanel.cxx \
	VisuGUI_Tools.cxx \
	VisuGUI_ViewTools.cxx \
	VisuGUI_Selection.cxx \
	VisuGUI_NameDlg.cxx \
	VisuGUI_FileDlg.cxx \
	VisuGUI_CursorDlg.cxx \
	VisuGUI_TimeAnimation.cxx \
	VisuGUI_EvolutionDlg.cxx \
	VisuGUI_EditContainerDlg.cxx \
	VisuGUI_ClippingDlg.cxx \
	VisuGUI_DialogRunner.cxx \
	VisuGUI_OffsetDlg.cxx \
	VisuGUI_Prs3dDlg.cxx \
	VisuGUI_ScalarBarDlg.cxx \
	VisuGUI_Plot3DDlg.cxx \
	VisuGUI_GaussPointsDlg.cxx \
	VisuGUI_DeformedShapeDlg.cxx \
	VisuGUI_IsoSurfacesDlg.cxx \
	VisuGUI_CutLinesDlg.cxx \
	VisuGUI_CutSegmentDlg.cxx \
	VisuGUI_CutPlanesDlg.cxx \
	VisuGUI_StreamLinesDlg.cxx \
	VisuGUI_VectorsDlg.cxx \
	VisuGUI_Displayer.cxx \
	VisuGUI_DeformedShapeAndScalarMapDlg.cxx \
	VisuGUI_SetupPlot2dDlg.cxx \
	VisuGUI_BuildProgressDlg.cxx \
	VisuGUI_Table3dDlg.cxx \
	VisuGUI_TransparencyDlg.cxx \
	VisuGUI_ShrinkFactorDlg.cxx \
	VisuGUI_Timer.cxx \
	VisuGUI_Slider.cxx \
	VisuGUI_Sweep.cxx \
	VisuGUI_InputPane.cxx \
	VisuGUI_FileInfoDlg.cxx \
	VisuGUI_FieldFilter.cxx \
	VisuGUI_ClippingPanel.cxx \
	VisuGUI_ClippingPlaneDlg.cxx \
	VisuGUI_FilterScalarsDlg.cxx \
	VisuGUI_ValuesLabelingDlg.cxx \
	VisuGUI_PrimitiveBox.cxx \
	VisuGUI_SizeBox.cxx

MOC_FILES= \
	VisuGUI_moc.cxx \
	VisuGUI_Module_moc.cxx \
	VisuGUI_InputPanel_moc.cxx \
	VisuGUI_BasePanel_moc.cxx \
	VisuGUI_Panel_moc.cxx \
	VisuGUI_SelectionPanel_moc.cxx \
	VisuGUI_SelectionPrefDlg_moc.cxx \
	VisuGUI_GaussPointsSelectionPane_moc.cxx \
	VisuGUI_FindPane_moc.cxx \
	VisuGUI_FeatureEdgesPanel_moc.cxx \
	VisuGUI_NameDlg_moc.cxx \
	VisuGUI_FileDlg_moc.cxx \
	VisuGUI_CursorDlg_moc.cxx \
	VisuGUI_TimeAnimation_moc.cxx \
	VisuGUI_EvolutionDlg_moc.cxx \
	VisuGUI_EditContainerDlg_moc.cxx \
	VisuGUI_ClippingDlg_moc.cxx \
	VisuGUI_DialogRunner_moc.cxx \
	VisuGUI_OffsetDlg_moc.cxx \
	VisuGUI_Prs3dDlg_moc.cxx \
	VisuGUI_ScalarBarDlg_moc.cxx \
	VisuGUI_Plot3DDlg_moc.cxx \
	VisuGUI_GaussPointsDlg_moc.cxx \
	VisuGUI_DeformedShapeDlg_moc.cxx \
	VisuGUI_IsoSurfacesDlg_moc.cxx \
	VisuGUI_CutLinesDlg_moc.cxx \
	VisuGUI_CutSegmentDlg_moc.cxx \
	VisuGUI_CutPlanesDlg_moc.cxx \
	VisuGUI_StreamLinesDlg_moc.cxx \
	VisuGUI_VectorsDlg_moc.cxx \
	VisuGUI_DeformedShapeAndScalarMapDlg_moc.cxx \
	VisuGUI_SetupPlot2dDlg_moc.cxx \
	VisuGUI_BuildProgressDlg_moc.cxx \
	VisuGUI_Table3dDlg_moc.cxx \
	VisuGUI_TransparencyDlg_moc.cxx \
	VisuGUI_ShrinkFactorDlg_moc.cxx \
	VisuGUI_Slider_moc.cxx \
	VisuGUI_Sweep_moc.cxx \
	VisuGUI_InputPane_moc.cxx \
	VisuGUI_FileInfoDlg_moc.cxx \
	VisuGUI_ClippingPanel_moc.cxx \
	VisuGUI_ClippingPlaneDlg_moc.cxx \
	VisuGUI_FilterScalarsDlg_moc.cxx \
	VisuGUI_ValuesLabelingDlg_moc.cxx \
	VisuGUI_PrimitiveBox_moc.cxx \
	VisuGUI_SizeBox_moc.cxx

nodist_libVISU_la_SOURCES=$(MOC_FILES)

# additionnal information to compil and link file
libVISU_la_CPPFLAGS= \
	-ftemplate-depth-32 $(QT_INCLUDES) $(VTK_INCLUDES) @CAS_CXXFLAGS@ @CAS_CPPFLAGS@ \
	$(PYTHON_INCLUDES) $(HDF5_INCLUDES) $(QWT_INCLUDES) \
	$(KERNEL_CXXFLAGS) \
	$(MED_CXXFLAGS) \
	$(BOOST_CPPFLAGS) \
	$(GUI_CXXFLAGS) \
	$(CORBA_CXXFLAGS) $(CORBA_INCLUDES) \
	-I$(srcdir)/../OBJECT -I$(srcdir)/../VISU_I \
	-I$(top_builddir)/idl -I$(srcdir)/../CONVERTOR -I$(srcdir)/../PIPELINE

libVISU_la_LDFLAGS= \
	$(QWT_LIBS) \
	$(CAS_LDFLAGS) -lTKV3d \
	$(KERNEL_LDFLAGS) -lSalomeNS -lSalomeDSClient \
	$(GUI_LDFLAGS) -lSVTK -lSPlot2d -lSalomePrs -lOCCViewer -lViewerTools \
	../OBJECT/libVisuObject.la ../VISU_I/libVISUEngineImpl.la \
	../../idl/libSalomeIDLVISU.la

if MED_ENABLE_MULTIPR
  libVISU_la_CPPFLAGS+= $(MULTIPR_CPPFLAGS)
endif
