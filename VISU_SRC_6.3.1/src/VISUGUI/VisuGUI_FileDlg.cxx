// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  File   : VisuGUI_FileDlg.cxx
//  Author : 
//  Module : SALOME
//  $Header: /dn05/salome/CVS/SALOME_ROOT/SALOME/src/VISUGUI/Visu_FileDlg.cxx
//
#include <QApplication>
#include <QPushButton>
#include <QCheckBox>
#include <QString>
#include <QLabel>

#include "VISUConfig.hh"
#include "VisuGUI_FileDlg.h"
#include "SUIT_ResourceMgr.h"

using namespace std;

bool VisuGUI_FileDlg::IsBuild = false; 

/*!
Constructor
*/
VisuGUI_FileDlg::VisuGUI_FileDlg (QWidget* parent, 
                                  bool open, 
                                  bool showQuickDir, 
                                  bool modal) :
  SUIT_FileDlg(parent, open, showQuickDir, modal)
{ 
  myCBuildAll = new QCheckBox (tr("FULL_LOAD"), this);

  QLabel* label = new QLabel("", this);
  label->setMaximumWidth(0);
  QPushButton* pb = new QPushButton(this);               
  pb->setMaximumWidth(0);
  addWidgets( label, myCBuildAll, pb );
  
  bool toBuildAll = VISU::GetResourceMgr()->booleanValue("VISU", "full_med_loading", false);
  if (toBuildAll) myCBuildAll->setChecked(true);
}

/*!
  Destructor
*/
VisuGUI_FileDlg::~VisuGUI_FileDlg() 
{
}

/*!
  Processes selection : tries to set given path or filename as selection
*/
bool VisuGUI_FileDlg::processPath( const QString& path )
{
  if ( !path.isNull() ) {
    QFileInfo fi( path );
    if ( fi.exists() ) {
      if ( fi.isFile() )
      {
        setDirectory( fi.absoluteDir().absolutePath() );
        selectFile( fi.fileName() );
      }
      else if ( fi.isDir() )
        setDirectory( path );
      return true;
    }
    else {
      if ( QFileInfo( fi.absoluteDir().absolutePath() ).exists() ) {
        setDirectory( fi.absoluteDir().absolutePath() );
        return true;
      }
    }
  }
  return false;
}

/*!
  Returns the file name for Open/Save [ static ]
*/
QString VisuGUI_FileDlg::getFileName( QWidget*           parent, 
                                      const QString&     initial, 
                                      const QStringList& filters, 
                                      const QString&     caption,
                                      bool               open,
                                      bool               showQuickDir,
                                      SUIT_FileValidator* validator )
{            
  VisuGUI_FileDlg* fd = new VisuGUI_FileDlg( parent, open, showQuickDir, true );    
  if ( !caption.isEmpty() )
    fd->setWindowTitle( caption );
  if ( !initial.isEmpty() ) { 
    fd->processPath( initial ); // VSR 24/03/03 check for existing of directory has been added to avoid QFileDialog's bug
  }
  fd->setFilters( filters );        
  if ( validator )
    fd->setValidator( validator );
  fd->exec();
  QString filename = fd->selectedFile();
  
  VisuGUI_FileDlg::IsBuild = fd->IsChecked();
  
  delete fd;
  qApp->processEvents();
  
  return filename;
}

bool VisuGUI_FileDlg::IsChecked() 
{
  return myCBuildAll->isChecked();
}
