// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "VisuGUI_OffsetDlg.h"

#include "VisuGUI.h"
#include "VisuGUI_Tools.h"

#include "VISU_ViewManager_i.hh"
#include "VISU_Actor.h"

#include "LightApp_Application.h"
#include "LightApp_SelectionMgr.h"
#include "SALOME_ListIteratorOfListIO.hxx"
#include "SalomeApp_Application.h"
#include <SalomeApp_DoubleSpinBox.h>
#include "SVTK_ViewWindow.h"
#include "SVTK_ViewModel.h"
#include "SUIT_ViewManager.h"
#include "SUIT_Desktop.h"
#include "SUIT_Session.h"
#include "SUIT_MessageBox.h"
#include "SUIT_ResourceMgr.h"

// VTK Includes
#include "vtkRenderer.h"

// QT Includes
#include <QLabel>
#include <QLayout>
#include <QCheckBox>
#include <QGroupBox>
#include <QPushButton>
#include <QKeyEvent>


#define  MAXVAL 1e10


VisuGUI_OffsetDlg::VisuGUI_OffsetDlg (VisuGUI* theModule)
: QDialog(VISU::GetDesktop(theModule), Qt::WindowTitleHint | Qt::WindowSystemMenuHint ),
  myModule(theModule),
  mySelectionMgr(VISU::GetSelectionMgr(theModule))
{
  //myPrsList.setAutoDelete(false);
  //myPointMapList.setAutoDelete(false);

  setWindowTitle(tr("TIT_OFFSETDLG"));
  setSizeGripEnabled(TRUE);

  QVBoxLayout* TopLayout = new QVBoxLayout (this);
  TopLayout->setSpacing(6);
  TopLayout->setMargin(11);

  QWidget* anOffsetsPane = new QWidget (this);
  QHBoxLayout* aHBLay = new QHBoxLayout( anOffsetsPane );
  aHBLay->setSpacing(6);

  aHBLay->addWidget( new QLabel ("dX:", anOffsetsPane) );
  aHBLay->addWidget( myDxEdt = new SalomeApp_DoubleSpinBox (anOffsetsPane) );
  VISU::initSpinBox( myDxEdt, -MAXVAL, MAXVAL, 1., "length_precision" );

  aHBLay->addWidget( new QLabel("dY:", anOffsetsPane) );;
  aHBLay->addWidget( myDyEdt = new SalomeApp_DoubleSpinBox (anOffsetsPane) );
  VISU::initSpinBox( myDyEdt, -MAXVAL, MAXVAL, 1., "length_precision" );

  aHBLay->addWidget( new QLabel("dZ:", anOffsetsPane) );
  aHBLay->addWidget( myDzEdt = new SalomeApp_DoubleSpinBox (anOffsetsPane) );
  VISU::initSpinBox( myDzEdt, -MAXVAL, MAXVAL, 1., "length_precision" );

  QPushButton* aResetBtn = new QPushButton(tr("BTN_RESET"), anOffsetsPane);
  aHBLay->addWidget( aResetBtn );
  connect(aResetBtn, SIGNAL(clicked()), this, SLOT(onReset()));

  TopLayout->addWidget(anOffsetsPane);

  if (!VISU::GetCStudy(VISU::GetAppStudy(theModule))->GetProperties()->IsLocked()) {
    mySaveChk = new QCheckBox ("Save to presentation", this);
    TopLayout->addWidget(mySaveChk);
    mySaveChk->setChecked(true);
  } else {
    mySaveChk = 0;
  }

  // Common buttons ===========================================================
  QGroupBox* GroupButtons = new QGroupBox(this);
  //GroupButtons->setColumnLayout(0, Qt::Vertical);
  //GroupButtons->layout()->setSpacing(0);
  //GroupButtons->layout()->setMargin(0);
  QGridLayout* GroupButtonsLayout = new QGridLayout(GroupButtons);
  GroupButtonsLayout->setAlignment(Qt::AlignTop);
  GroupButtonsLayout->setSpacing(6);
  GroupButtonsLayout->setMargin(11);

  QPushButton* buttonOk = new QPushButton(tr("&OK"), GroupButtons);
  buttonOk->setAutoDefault(TRUE);
  buttonOk->setDefault(TRUE);
  GroupButtonsLayout->addWidget(buttonOk, 0, 0);
  GroupButtonsLayout->addItem(new QSpacerItem(5, 5, QSizePolicy::Expanding, QSizePolicy::Minimum), 0, 1);

  QPushButton* buttonApply = new QPushButton(tr("&Apply"), GroupButtons);
  buttonOk->setAutoDefault(TRUE);
  GroupButtonsLayout->addWidget(buttonApply, 0, 2);
  GroupButtonsLayout->addItem(new QSpacerItem(5, 5, QSizePolicy::Expanding, QSizePolicy::Minimum), 0, 3);

  QPushButton* buttonCancel = new QPushButton(tr("&Cancel") , GroupButtons);
  buttonCancel->setAutoDefault(TRUE);
  GroupButtonsLayout->addWidget(buttonCancel, 0, 4);
  GroupButtonsLayout->addItem(new QSpacerItem(5, 5, QSizePolicy::Expanding, QSizePolicy::Minimum), 0, 5);  

  QPushButton* buttonHelp = new QPushButton(tr("&Help") , GroupButtons );
  buttonHelp->setAutoDefault(TRUE);
  GroupButtonsLayout->addWidget(buttonHelp, 0, 6);

  TopLayout->addWidget(GroupButtons);

  connect(buttonOk,     SIGNAL(clicked()), this, SLOT(accept()));
  connect(buttonApply,  SIGNAL(clicked()), this, SLOT(onApply()));
  connect(buttonCancel, SIGNAL(clicked()), this, SLOT(reject()));
  connect(buttonHelp,   SIGNAL(clicked()), this, SLOT(onHelp()));
  connect(mySelectionMgr, SIGNAL(currentSelectionChanged()), this, SLOT(onSelectionChanged()));
  
  onSelectionChanged();
  show();
}

void VisuGUI_OffsetDlg::setVisible(bool show){
  if ( show  && getPrsCount() > 0 )
    QDialog::setVisible( show );
  else
    QDialog::setVisible( show );
}

void VisuGUI_OffsetDlg::onSelectionChanged(){
  if(!mySelectionMgr)
    return;
  
  //Clear old selection
  clearPresentations();

  SALOME_ListIO aListIO;
  mySelectionMgr->selectedObjects(aListIO);

  SalomeApp_Study* aStudy = VISU::GetAppStudy(myModule);
  SALOME_ListIteratorOfListIO anIter(aListIO);
  for (; anIter.More(); anIter.Next()) {
    Handle(SALOME_InteractiveObject) anIO = anIter.Value();
    if (anIO->hasEntry()) {
      QString anEntry(anIO->getEntry());
      VISU::TObjectInfo anObjectInfo = VISU::GetObjectByEntry(aStudy, anEntry.toLatin1().constData());
      if(VISU::Prs3d_i* aPrsObject = VISU::GetPrs3dFromBase(anObjectInfo.myBase))
        addPresentation(aPrsObject);
      else if (VISU::PointMap3d_i* aPrs = dynamic_cast<VISU::PointMap3d_i*>(anObjectInfo.myBase)) {
        addPointMapPresentation(aPrs);
      }
    }
  }
}

void VisuGUI_OffsetDlg::clearPresentations(){
  myPrsList.clear();
  myPointMapList.clear();
  myOldOffsets.clear();
  myOldPointMapOffsets.clear();
}

void VisuGUI_OffsetDlg::addPresentation (VISU::Prs3d_i* thePrs)
{
  myPrsList.append(thePrs);
  CORBA::Float anOffset[3];
  thePrs->GetOffset(anOffset[0],anOffset[1],anOffset[2]);
  OffsetStruct anOffs(anOffset[0],anOffset[1],anOffset[2]);
  myOldOffsets.append(anOffs);
  if (myPrsList.count() == 1) {
    setOffset(anOffs.myOffset);
  } else if (myPrsList.count() == 2) {
    OffsetStruct anOffs;
    setOffset(anOffs.myOffset);
  }
}

void VisuGUI_OffsetDlg::addPointMapPresentation (VISU::PointMap3d_i* thePrs)
{
  myPointMapList.append(thePrs);

  CORBA::Float anOffset[3];
  thePrs->GetOffset(anOffset[0],anOffset[1],anOffset[2]);
  OffsetStruct anOffs(anOffset[0],anOffset[1],anOffset[2]);
  myOldPointMapOffsets.append(anOffs);
  if (myPointMapList.count() == 1) {
    setOffset(anOffs.myOffset);
  } else if (myPointMapList.count() == 2) {
    OffsetStruct anOffs;
    setOffset(anOffs.myOffset);
  }
}

void VisuGUI_OffsetDlg::setOffset (const vtkFloatingPointType* theOffset)
{
  myDxEdt->setValue(theOffset[0]);
  myDyEdt->setValue(theOffset[1]);
  myDzEdt->setValue(theOffset[2]);
}

void VisuGUI_OffsetDlg::getOffset (vtkFloatingPointType* theOffset) const
{
  theOffset[0] = myDxEdt->value();
  theOffset[1] = myDyEdt->value();
  theOffset[2] = myDzEdt->value();
}

void VisuGUI_OffsetDlg::onReset()
{
  myDxEdt->setValue(0);
  myDyEdt->setValue(0);
  myDzEdt->setValue(0);
}

bool VisuGUI_OffsetDlg::isToSave() const
{
  if (mySaveChk)
    return mySaveChk->isChecked();
  else
    return false;
}

void VisuGUI_OffsetDlg::updateOffset (VISU::Prs3d_i* thePrs, vtkFloatingPointType* theOffset)
{
  if (myPrsList.count() == 0) 
    return;

  if (isToSave()) {
    thePrs->SetOffset(theOffset[0],theOffset[1],theOffset[2]);
    thePrs->UpdateActors();
    return;
  }

  ViewManagerList aViewManagerList;
  SalomeApp_Application* anApp = myModule->getApp();
  anApp->viewManagers(aViewManagerList);
  QList<SUIT_ViewManager*>::Iterator anVMIter = aViewManagerList.begin();
  for (; anVMIter !=  aViewManagerList.end(); anVMIter++ ) {
    QVector<SUIT_ViewWindow*> aViews = (*anVMIter)->getViews();
    for (int i = 0, iEnd = aViews.size(); i < iEnd; i++) {
      if (SUIT_ViewWindow* aViewWindow = aViews.at(i)) {
        if (SVTK_ViewWindow* vw = dynamic_cast<SVTK_ViewWindow*>(aViewWindow)) {
          if (VISU_Actor* anActor = VISU::FindActor(vw, thePrs)) {
                anActor->SetPosition(theOffset);
            vw->onAdjustTrihedron();
            vw->getRenderer()->ResetCameraClippingRange();
            vw->Repaint();
          }
        }
      }
    }
  }
}

void VisuGUI_OffsetDlg::updatePointMapOffset (VISU::PointMap3d_i* thePrs, vtkFloatingPointType* theOffset)
{
  if (myPointMapList.count() == 0) 
    return;

  if (isToSave())
    thePrs->SetOffset(theOffset[0],theOffset[1],theOffset[2]);

  ViewManagerList aViewManagerList;
  SalomeApp_Application* anApp = myModule->getApp();
  anApp->viewManagers(aViewManagerList);
  SUIT_ViewManager* aViewManager;
  foreach( aViewManager, aViewManagerList ) {
    QVector<SUIT_ViewWindow*> aViews = aViewManager->getViews();
    for (int i = 0, iEnd = aViews.size(); i < iEnd; i++) {
      if (SUIT_ViewWindow* aViewWindow = aViews.at(i)) {
        if (SVTK_ViewWindow* vw = dynamic_cast<SVTK_ViewWindow*>(aViewWindow)) {
          vw->onAdjustTrihedron();

          if (VISU_ActorBase* anActor = VISU::FindActorBase(vw, thePrs)) {
            anActor->SetPosition(theOffset);
            vw->highlight(thePrs->GetIO(), 1);
            vw->getRenderer()->ResetCameraClippingRange();
            vw->Repaint();
          }
        }
      }
    }
  }
}

void VisuGUI_OffsetDlg::accept()
{
  vtkFloatingPointType anOffset[3];
  getOffset(anOffset);
  for (int i = 0; i < myPrsList.count(); i++) {
    updateOffset(myPrsList.at(i), anOffset);
  }
  for (int i = 0; i < myPointMapList.count(); i++) {
    updatePointMapOffset(myPointMapList.at(i), anOffset);
  }
  QDialog::accept();
}

void VisuGUI_OffsetDlg::reject()
{
  for (int i = 0; i < myPrsList.count(); i++) {
    updateOffset(myPrsList.at(i), myOldOffsets[i].myOffset);
  }
  for (int i = 0; i < myPointMapList.count(); i++) {
    updatePointMapOffset(myPointMapList.at(i), myOldPointMapOffsets[i].myOffset);
  }
  QDialog::reject();
}

void VisuGUI_OffsetDlg::onApply()
{
  vtkFloatingPointType anOffset[3];
  getOffset(anOffset);

  for (int i = 0; i < myPrsList.count(); i++) {
    updateOffset(myPrsList.at(i), anOffset);
  }
  for (int i = 0; i < myPointMapList.count(); i++) {
    updatePointMapOffset(myPointMapList.at(i), anOffset);
  }
}

void VisuGUI_OffsetDlg::onHelp()
{
  QString aHelpFileName = "translate_presentation_page.html";
  LightApp_Application* app = (LightApp_Application*)(SUIT_Session::session()->activeApplication());
  if (app)
    app->onHelpContextModule(myModule ? app->moduleName(myModule->moduleName()) : QString(""), aHelpFileName);
  else {
    QString platform;
#ifdef WIN32
    platform = "winapplication";
#else
    platform = "application";
#endif
    SUIT_MessageBox::warning(0, QObject::tr("WRN_WARNING"),
                             QObject::tr("EXTERNAL_BROWSER_CANNOT_SHOW_PAGE").
                             arg(app->resourceMgr()->stringValue("ExternalBrowser", platform)).arg(aHelpFileName),
                             QObject::tr("BUT_OK"));
  }
}

void VisuGUI_OffsetDlg::keyPressEvent( QKeyEvent* e )
{
  QDialog::keyPressEvent( e );
  if ( e->isAccepted() )
    return;

  if ( e->key() == Qt::Key_F1 )
    {
      e->accept();
      onHelp();
    }
}
