// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  File   : VisuGUI_StreamLinesDlg.h
//  Author : Vitaly SMETANNIKOV
//  Module : VISU
//
#ifndef VISUGUI_STREAMLINESDLG_H
#define VISUGUI_STREAMLINESDLG_H

#include "VisuGUI_Prs3dDlg.h"

class QTabWidget;
class QCheckBox;
class QComboBox;


#include "VISUConfig.hh"

class SalomeApp_Module;
class VisuGUI_InputPane;
class SalomeApp_DoubleSpinBox;
class QtxColorButton;

namespace VISU
{
  class StreamLines_i;
}

class VisuGUI_StreamLinesDlg: public VisuGUI_ScalarBarBaseDlg
{
    Q_OBJECT
public:
    VisuGUI_StreamLinesDlg (SalomeApp_Module* theModule);
    ~VisuGUI_StreamLinesDlg();

    virtual void initFromPrsObject( VISU::ColoredPrs3d_i* thePrs,
                                  bool theInit );

    virtual int  storeToPrsObject(VISU::ColoredPrs3d_i* thePrs);

    void setColor( QColor color );
    void enableMagnColor( bool enable );

protected:
  virtual QString GetContextHelpFilePath();
  void storeToPrsCopy();

protected slots:
  void accept();
  void reject();

private slots:
  void StepLengthChanged(double theValue);
  void IntegrationStepChanged(double theValue);
  void PropagationTimeChanged(double theValue);
  void onSourceTypeChange(int theIndex);

private:
  VISU::Mesh_ptr createMesh(VISU::VISUType theType, QString theName);

    SalomeApp_DoubleSpinBox* myStepLen;
    SalomeApp_DoubleSpinBox* myIntegStepLen;
    SalomeApp_DoubleSpinBox* myPropTime;
    QComboBox* myDirCombo;
    QCheckBox* myUseScalar;
    QtxColorButton*   SelColor;
    //QCheckBox* myUseSrcChk;
    QComboBox* myUseSrcCombo;
    SalomeApp_DoubleSpinBox* myPntPercent;
    QComboBox* mySrcCombo;

    QTabWidget*            myTabBox;
    VisuGUI_InputPane*     myInputPane;

    QList<VISU::Prs3d_var> myPrsList;
    QList<VISU::Prs3d_var> myEntityList;
    QList<VISU::Prs3d_var> myFamilyList;
    QList<VISU::Prs3d_var> myGroupList;
    QStringList myEntitiesLst;
    QStringList myFamilisLst;
    QStringList myGroupsLst;
    QStringList myPrsLst;

    _PTR(SObject) mySelectionObj;
    SALOME::GenericObjPtr<VISU::StreamLines_i> myPrsCopy;
    SalomeApp_Module* myVisuGUI;
    bool myStatus;

private slots:
  //void setVColor();
  void enableSetColor();
};

#endif  //VISUGUI_STREAMLINESDLG_H
