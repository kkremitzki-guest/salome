// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU VISUGUI : GUI of VISU component
//  File   : VisuGUI_FeatureEdgesPanel.h
//  Author : Oleg Uvarov
//  Module : VISU
//
#ifndef VISUGUI_FEATUREEDGESPANEL_H
#define VISUGUI_FEATUREEDGESPANEL_H

#include "VisuGUI_Panel.h"

class QCheckBox;
class QGroupBox;

class SalomeApp_DoubleSpinBox;

class VISU_Actor;

class VisuGUI_FeatureEdgesPanel: public VisuGUI_Panel
{
  Q_OBJECT

public:
  VisuGUI_FeatureEdgesPanel( VisuGUI* theModule, QWidget* theParent = 0 );
  virtual ~VisuGUI_FeatureEdgesPanel ();

protected:
  virtual void              keyPressEvent( QKeyEvent* theEvent );
  virtual void              showEvent( QShowEvent* theEvent );

protected slots:
  virtual void              onModuleActivated();
  virtual void              onModuleDeactivated();

private slots:
  virtual void              onApply();
  virtual void              onClose();
  virtual void              onHelp();

  void                      onSelectionEvent();

private:
  VISU_Actor*               getSelectedActor() const;

private:
  QGroupBox*                myGrp;

  SalomeApp_DoubleSpinBox*  myAngleSpinBox;
  QCheckBox*                myFeatureEdgesCB;
  QCheckBox*                myBoundaryEdgesCB;
  QCheckBox*                myManifoldEdgesCB;
  QCheckBox*                myNonManifoldEdgesCB;
  QCheckBox*                myColoringCB;

  VISU_Actor*               myActor;
};

#endif
