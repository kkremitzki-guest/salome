#  -*- coding: iso-8859-1 -*-
# Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
# See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#

#  VISU VISU_SWIG : binding of C++ implementation and Python
#  File   : visu_view3d.py
#  Module : VISU
#
import salome
import visu_gui
import VISU
import os
import time

medFile = os.getenv("DATA_DIR") + "/MedFiles/fra.med"

#%====================Stage1: Creating a study====================%

print "Creating a new study... "
myVisu.SetCurrentStudy(salome.myStudy)

myVisu = visu_gui.myVisu
myVisu.SetCurrentStudy(salome.myStudy)
myViewManager = myVisu.GetViewManager()
if myViewManager is None : print "Error"
else : print "OK"

myView = myViewManager.Create3DView()

#%====================Stage2: Import MED file====================%

print "Importing Med file... "
myResult = myVisu.ImportFile(medFile)
if myResult is None : print "Error"
else : print "OK"

print "Creating mesh... "
myMeshName = 'LE VOLUME'
myCellEntity = VISU.CELL
mesh = myVisu.MeshOnEntity(myResult,myMeshName,myCellEntity);
if mesh is None : print "Error"
else : print "OK"

myView.Display(mesh)

#%====================Stage3: Changing of view parameters====================%

myView.FitAll()

aDelay=0.02

print "Rotating X..."
for i in range(0,100) :
    aPoint = (i,60,150)
    myView.SetPointOfView(aPoint) 
    myView.Update();
    time.sleep(aDelay)
    
print "OK"

print "Zooming out...",
aScale = myView.GetParallelScale()
print "Zooming 3D view..."
for i in range(0,50) :
    aScale = aScale + 0.05
    myView.SetParallelScale(aScale)
    myView.Update();
    time.sleep(aDelay)
print "OK"

print "Change Focal Point..."
aFocalPoint = myView.GetFocalPoint()
for i in range(0,61) :
    if i <= 30 :
      aFocalPoint[0] = aFocalPoint[0] - 0.1;
    else :
      aFocalPoint[0] = aFocalPoint[0] + 0.1;
    myView.SetFocalPoint(aFocalPoint) 
    myView.Update();
    time.sleep(aDelay)
for i in range(0,61) :
    if i <= 30 :
      aFocalPoint[1] = aFocalPoint[1] - 0.1;
    else :
      aFocalPoint[1] = aFocalPoint[1] + 0.1;
    myView.SetFocalPoint(aFocalPoint) 
    myView.Update();
    time.sleep(aDelay)
print "OK"

print "Change View Up..."
aViewUp = myView.GetViewUp();
for i in range(0,40) :
    aViewUp[0] = aViewUp[0] + 0.1;
    myView.SetViewUp(aViewUp) 
    myView.Update();
    time.sleep(aDelay*2)
print "OK"

#create second viewer

myView1 = myViewManager.Create3DView()
myView1.SplitRight()
myView1.Display(mesh)
myView1.FitAll()
time.sleep(1)

# Get properties from the First viewer and apply it to the New viewer
aP = myView.GetPointOfView()
myView1.SetPointOfView(aP)

aFP = myView.GetFocalPoint()
myView1.SetFocalPoint(aFP)

aVU = myView.GetViewUp()
myView1.SetViewUp(aVU)

aScale = myView.GetParallelScale()
myView1.SetParallelScale( aScale )

myView.Update();
myView1.Update();
time.sleep(1)

# Change Parameters in both viewers

print "Zooming...",
aScale = myView.GetParallelScale()
for i in range(0,70) :
    aScale = aScale - 0.08
    myView.SetParallelScale(aScale)
    myView.Update();
    myView1.SetParallelScale(aScale)
    myView1.Update();
    time.sleep(aDelay)
print "OK"

print "Rotating Y..."
aPoint = myView1.GetPointOfView();
for i in range(0,150) :
    aPoint[1] = aPoint[1] + 1;
    myView1.SetPointOfView(aPoint) 
    myView1.Update();
    myView.SetPointOfView(aPoint) 
    myView.Update();
print "OK"

print "Zooming back...",
for i in range(0,40) :
    aScale = aScale + 0.08
    myView.SetParallelScale(aScale)
    myView.Update();
    myView1.SetParallelScale(aScale)
    myView1.Update();
    time.sleep(aDelay)
print "OK"
