#  -*- coding: iso-8859-1 -*-
# Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
# See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#

# Set different point markers for presentation
#
import salome

import VISU
import visu_gui

import os
import time

data_dir = os.getenv("DATA_DIR")
sleep_delay = 1

myVisu = visu_gui.myVisu
myVisu.SetCurrentStudy(salome.myStudy)
myViewManager = myVisu.GetViewManager()
myView = myViewManager.Create3DView()

medFile = "fra.med"
aMeshName ="LE VOLUME"
anEntity = VISU.NODE
aFieldName = "VITESSE";
aTimeStampId = 1

medFile = data_dir + "/MedFiles/" + medFile

print "Build Scalar Map presentation"
myResult = myVisu.ImportFile(medFile)
aScalarMap = myVisu.ScalarMapOnField(myResult,aMeshName,anEntity,aFieldName,aTimeStampId)
myView.Display(aScalarMap);
myView.FitAll();

print "Set representation type to Point"
myView.SetPresentationType(aScalarMap, VISU.POINT)
time.sleep(sleep_delay)

print "Set standard marker 1"
aScalarMap.SetMarkerStd(VISU.MT_PLUS, VISU.MS_10)
myView.Update()
time.sleep(sleep_delay)

print "Set standard marker 2"
aScalarMap.SetMarkerStd(VISU.MT_STAR, VISU.MS_35)
myView.Update()
time.sleep(sleep_delay)

print "Set standard marker 3"
aScalarMap.SetMarkerStd(VISU.MT_O, VISU.MS_25)
myView.Update()
time.sleep(sleep_delay)

print "Set custom marker 1"
texture_1 = myVisu.LoadTexture(os.path.join(data_dir, "Textures", "texture1.dat"))
aScalarMap.SetMarkerTexture(texture_1)
myView.Update()
time.sleep(sleep_delay)

print "Set custom marker 2"
texture_2 = myVisu.LoadTexture(os.path.join(data_dir, "Textures", "texture2.dat"))
aScalarMap.SetMarkerTexture(texture_2)
myView.Update()
time.sleep(sleep_delay)

print "Set custom marker 3"
texture_3 = myVisu.LoadTexture(os.path.join(data_dir, "Textures", "texture3.dat"))
aScalarMap.SetMarkerTexture(texture_3)
myView.Update()
