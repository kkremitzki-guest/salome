#  -*- coding: iso-8859-1 -*-
# Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
# See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#

import os
import VISU
import salome
from visu_gui import *

myVisu.SetCurrentStudy(salome.myStudy)

aTableFile = os.getenv('DATA_DIR') + '/Tables/table_test.xls'

aTableSObject = myVisu.ImportTables(aTableFile, False)

aTable = None
if aTableSObject:
  anIsFound, aSObject = aTableSObject.FindSubObject(1)
  if anIsFound:
    aTable = aSObject.GetObject()

aViewManager = myVisu.GetViewManager();
aView = aViewManager.Create3DView()
if aView is None : print "Creating View Error"
print aTable

##########Get Values################
aScaleFactor = aTable.GetScaleFactor();
anIsContourPrs = aTable.GetIsContourPrs();
aNbOfContours = aTable.GetNbOfContours();
aScaling = aTable.GetScaling();
aMin = aTable.GetMin();
aMax = aTable.GetMax();
aSourceMin = aTable.GetSourceMin();
aSourceMax = aTable.GetSourceMax();
aPosX = aTable.GetPosX();
aPosY = aTable.GetPosY();
aHeight = aTable.GetHeight();
aWidth = aTable.GetWidth();
aNbColors = aTable.GetNbColors();
aLabels = aTable.GetLabels();
aBarOrientation = aTable.GetBarOrientation();


print "Get Values:"
print "aScaleFactor = ", aScaleFactor
print "anIsContourPrs = ", anIsContourPrs
print "aNbOfContours = ", aNbOfContours
print "aScaling = ", aScaling
print "===== SCALAR BAR ======="
print "aMin = ", aMin
print "aMax = ", aMax
print "aSourceMin = ", aSourceMin
print "aSourceMax = ", aSourceMax
print "aPosX = ", aPosX
print "aPosY = ", aPosY
print "aHeight = ", aHeight
print "aWidth = ", aWidth
print "aNbColors = ", aNbColors
print "aLabels = ", aLabels
print "aBarOrientation = ", aBarOrientation

aView.DisplayOnly( aTable )
aView.FitAll()