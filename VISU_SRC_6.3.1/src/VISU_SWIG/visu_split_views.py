#  -*- coding: iso-8859-1 -*-
# Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
# Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
# CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
# See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#

import VISU
import SALOMEDS
import salome
from visu_gui import *
from time import sleep

DELAY = 2

myVisu.SetCurrentStudy(salome.myStudy)

myViewManager = myVisu.GetViewManager()

################################################

medFile = "fra.med"
myFieldName = "VITESSE";

aMeshName ="LE VOLUME"
anEntity = VISU.NODE
aTimeStampId = 1

medFile = os.path.join(os.getenv('DATA_DIR'), 'MedFiles', medFile)
myResult = myVisu.ImportFile(medFile)

################################################

myView1 = myViewManager.Create3DView()
print "myView1 = myViewManager.Create3DView()"
myView1.SetTitle("The window will be soon destroyed!")

myView1.Maximize()
print "myView1.Maximize()"

myView1.Restore()
print "myView1.Restore()"

myView1.Minimize()
print "myView1.Minimize()"

sleep(DELAY)

myViewManager.Destroy(myView1)
print "myViewManager.Destroy(myView1)"

################################################

myView2 = myViewManager.Create3DView()
print "myView2 = myViewManager.Create3DView()"
myView2.SetTitle("The window to display Scalar Map")

aColor = SALOMEDS.Color(0.0,0.3,1.0)
print "aColor = SALOMEDS.Color(0.0,0.3,1.0)"
myView2.SetBackground(aColor)
print "myView2.SetBackground(aColor)"

aScalarMap = myVisu.ScalarMapOnField(myResult,aMeshName,anEntity,myFieldName,aTimeStampId)
print "myVisu.ScalarMapOnField(...)"

myView2.Display(aScalarMap);
print "myView2.Display(aScalarMap)"

myView2.SetFocalPoint([0,0,0])
print "myView2.SetFocalPoint([0,0,0])"
myView2.SetParallelScale(2)
print "myView2.SetParallelScale(2)"

myView2.FitAll()
print "myView3.FitAll"

sleep(DELAY)

################################################

myView3 = myViewManager.Create3DView()
print "myView3 = myViewManager.Create3DView()"
myView3.SetTitle("The window to display Cut Planes")

myView3.SplitRight()
print "myView3.SplitRight()"

aColor = SALOMEDS.Color(0.0,0.7,0.0)
print "aColor = SALOMEDS.Color(0.0,0.7,0.0)"
myView3.SetBackground(aColor)
print "myView3.SetBackground(aColor)"

aCutPlanes = myVisu.CutPlanesOnField(myResult,aMeshName,anEntity,myFieldName,aTimeStampId)
print "myVisu.CutPlanesOnField(...)"
aCutPlanes.SetOrientation(VISU.CutPlanes.XY,0,0)
print "aCutPlanes.SetOrientation(...)"

myView3.DisplayOnly(aCutPlanes);
print "myView3.DisplayOnly(aCutPlanes)"

aPoint = myView3.GetPointOfView()
aPoint[0] = aPoint[0] + 10
myView3.SetPointOfView(aPoint)
print "myView3.SetPointOfView(...)"
myView3.ScaleView(VISU.View3D.YAxis,10.0)
myView3.ScaleView(VISU.View3D.XAxis,3.0)
print "myView3.ScaleView(...)"

myView3.FitAll()

myView3.SaveViewParams('AAA')
print "myView3.SaveViewParams('AAA')"

myView3.RemoveScale()
print "myView3.RemoveScale()"

myView3.Update()
print "myView3.Update()"
myView3.FitAll()
print "myView3.FitAll()"

sleep(DELAY)

################################################

myView4 = myViewManager.Create3DView()
print "myView4 = myViewManager.Create3DView()"
myView4.SetTitle("The window to display Iso Surfaces")

aColor = SALOMEDS.Color(1.0,0.7,0.0)
print "aColor = SALOMEDS.Color(1.0,0.7,0.0)"
myView4.SetBackground(aColor)
print "myView4.SetBackground(aColor)"

aIsoSurfaces = myVisu.IsoSurfacesOnField(myResult, aMeshName, anEntity, myFieldName, aTimeStampId)
print "aIsoSurfaces = myVisu.IsoSurfacesOnField(...)"
aIsoSurfaces.SetNbSurfaces(10)
print "aIsoSurfaces.SetNbSurfaces(10)"

myView4.DisplayOnly(aIsoSurfaces)
print "myView4.DisplayOnly(aIsoSurfaces)"

myView4.FitAll()
print "myView4.FitAll()"

sleep(DELAY)

################################################

myView5 = myViewManager.Create3DView()
print "myView5 = myViewManager.Create3DView()"
myView5.SetTitle("The window to display Cut Lines")

myView5.SplitBottom()
print "myView5.SplitBottom()"

aColor = SALOMEDS.Color(0.7,0.7,0.7)
print "aColor = SALOMEDS.Color(0.7,0.7,0.7)"
myView5.SetBackground(aColor)
print "myView5.SetBackground(aColor)"

aCutLines = myVisu.CutLinesOnField(myResult, aMeshName, anEntity, myFieldName, aTimeStampId)
print "aCutLines = myVisu.CutLinesOnField(...)"
aCutLines.SetOrientation(VISU.CutPlanes.ZX,0,0)
print "aCutLines.SetOrientation(VISU.CutPlanes.ZX,0,0)"
aCutLines.SetOrientation2(VISU.CutPlanes.YZ,0,0)
print "aCutLines.SetOrientation2(VISU.CutPlanes.YZ,0,0)"

myView5.DisplayOnly(aCutLines)
print "myView5.DisplayOnly(aCutLines)"
myView5.FitAll()
print "myView5.FitAll()"

sleep(DELAY)

################################################

myView6 = myViewManager.CreateXYPlot()
print "myView6 = myViewManager.CreateXYPlot()"
myView6.SetTitle("The window to display curves")

aSObj = myStudy.FindObjectIOR(aCutLines.GetID())
aTable = myVisu.CreateTable( aSObj.GetID() )
print "aTable = myVisu.CreateTable(...)"

aContainer = myVisu.CreateContainer()
print "aContainer = myVisu.CreateContainer(...)"

aNbCurve = aTable.GetNbRows() - 1
for i in range(2,aNbCurve):
   aCurve = myVisu.CreateCurve( aTable, 1, i )
   print "myVisu.CreateCurve(aTable, 1, %d)" % i
   aContainer.AddCurve(aCurve)
   pass

myView6.Display(aContainer)
print "myView6.Display(aContainer)"

sleep(DELAY)

################################################

myView7 = myViewManager.Create3DView()
print "myView7 = myViewManager.Create3DView()"
myView7.SetTitle("The window to display Cut Segment")

aCutSegment = myVisu.CutSegmentOnField(myResult, aMeshName, anEntity, myFieldName, aTimeStampId)
print "aCutSegment = myVisu.CutSegmentOnField(...)"
aCutSegment.SetPoint1(0.0,0.0,0.0)
print "aCutSegment.SetPoint1(0.0,0.0,0.0)"
aCutSegment.SetPoint2(0.4,0.05,1.0)
print "aCutSegment.SetPoint2(0.4,0.05,1.0)"

myView7.DisplayOnly(aCutSegment)
print "myView7.DisplayOnly(aCutSegment)"

myView7.FitAll();
print "myView7.FitAll()"

sleep(DELAY)

################################################

myView4.Attract(myView2)
print "myView4.Attract(myView2)"

sleep(DELAY)

myView7.OnTop()
print "myView6.OnTop()"

sleep(DELAY)
