// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  File:
//  Author:  Alexey PETROV
//  Module : VISU
//
#include "VISU_Structures_impl.hxx"
#include "VISU_PointCoords.hxx"
#include "VISU_MeshValue.hxx"

#include "VISU_AppendFilter.hxx"
#include "VISU_AppendPolyData.hxx"
#include "VISU_MergeFilter.hxx"

#include "VISU_ConvertorUtils.hxx"
#include "VISU_CommonCellsFilter.hxx"

#include <vtkUnstructuredGrid.h>
#include <vtkPolyData.h>
#include <vtkCellType.h>
#include <vtkCell.h>

namespace VISU
{
  /*  vtkIdType
  VISUGeom2NbNodes(EGeometry theGeom)
  {
    switch(theGeom){
#ifndef VISU_ENABLE_QUADRATIC
    case VISU::eSEG3:
      return 2;
    case VISU::eTRIA6:
      return 3;
    case VISU::eQUAD8:
      return 4;
    case VISU::eTETRA10:
      return 4;
    case VISU::eHEXA20:
      return 8;
    case VISU::ePENTA15:
      return 6;
    case VISU::ePYRA13:
      return 5;
#endif
    case VISU::ePOLYGONE:
    case VISU::ePOLYEDRE:
      return -1;
    default:
      return theGeom % 100;
    }
  }

  vtkIdType
  VISUGeom2VTK(EGeometry theGeom)
  {
    switch(theGeom){
    case VISU::ePOINT1:
      return VTK_VERTEX;
    case VISU::eSEG2:
      return VTK_LINE;
    case VISU::eTRIA3:
      return VTK_TRIANGLE;
    case VISU::eQUAD4:
      return VTK_QUAD;
    case VISU::eTETRA4:
      return VTK_TETRA;
    case VISU::eHEXA8:
      return VTK_HEXAHEDRON;
    case VISU::ePENTA6:
      return VTK_WEDGE;
    case VISU::ePYRA5:
      return VTK_PYRAMID;

    case VISU::ePOLYGONE:
      return VTK_POLYGON;
    case VISU::ePOLYEDRE:
      return VTK_CONVEX_POINT_SET;

#ifndef VISU_ENABLE_QUADRATIC
    case VISU::eSEG3:
      return VTK_LINE;
    case VISU::eTRIA6:
      return VTK_TRIANGLE;
    case VISU::eQUAD8:
      return VTK_QUAD;
    case VISU::eTETRA10:
      return VTK_TETRA;
    case VISU::eHEXA20:
      return VTK_HEXAHEDRON;
    case VISU::ePENTA15:
      return VTK_WEDGE;
    case VISU::ePYRA13:
      return VTK_PYRAMID;

#else

    case VISU::eSEG3:
#if defined(VTK_QUADRATIC_EDGE) && defined(VISU_USE_VTK_QUADRATIC)
      return VTK_QUADRATIC_EDGE;
#else
      return VTK_POLY_LINE;
#endif

    case VISU::eTRIA6:
#if defined(VTK_QUADRATIC_TRIANGLE) && defined(VISU_USE_VTK_QUADRATIC)
      return VTK_QUADRATIC_TRIANGLE;
#else
      return VTK_POLYGON;
#endif

    case VISU::eQUAD8:
#if defined(VTK_QUADRATIC_QUAD) && defined(VISU_USE_VTK_QUADRATIC)
      return VTK_QUADRATIC_QUAD;
#else
      return VTK_POLYGON;
#endif

    case VISU::eTETRA10:
#if defined(VTK_QUADRATIC_TETRA) && defined(VISU_USE_VTK_QUADRATIC)
      return VTK_QUADRATIC_TETRA;
#else
      return VTK_CONVEX_POINT_SET;
#endif

    case VISU::eHEXA20:
#if defined(VTK_QUADRATIC_HEXAHEDRON) && defined(VISU_USE_VTK_QUADRATIC)
      return VTK_QUADRATIC_HEXAHEDRON;
#else
      return VTK_CONVEX_POINT_SET;
#endif

    case VISU::ePENTA15:
#if defined(VTK_QUADRATIC_WEDGE) && defined(VISU_USE_VTK_QUADRATIC)
      return VTK_QUADRATIC_WEDGE;
#else
      return VTK_CONVEX_POINT_SET;
#endif

    case VISU::ePYRA13:
#if defined(VTK_QUADRATIC_PYRAMID) && defined(VISU_USE_VTK_QUADRATIC)
      return VTK_QUADRATIC_PYRAMID;
#else
      return VTK_CONVEX_POINT_SET;
#endif

#endif //VISU_ENABLE_QUADRATIC

    default:
      return -1;
    }
    }*/


  EGeometry
  VTKGeom2VISU(vtkIdType theGeom)
  {
    switch(theGeom){
    case VTK_VERTEX:
      return VISU::ePOINT1;
    case VTK_LINE:
      return VISU::eSEG2;
    case VTK_TRIANGLE:
      return VISU::eTRIA3;
    case VTK_QUAD:
      return VISU::eQUAD4;
    case VTK_TETRA:
      return VISU::eTETRA4;
    case VTK_HEXAHEDRON:
      return VISU::eHEXA8;
    case VTK_WEDGE:
      return VISU::ePENTA6;
    case VTK_PYRAMID:
      return VISU::ePYRA5;

    case VTK_POLYGON:
      return VISU::ePOLYGONE;
    case VTK_CONVEX_POINT_SET:
      return VISU::ePOLYEDRE;

#if defined(VISU_ENABLE_QUADRATIC) && defined(VISU_USE_VTK_QUADRATIC)
  #if defined(VTK_QUADRATIC_EDGE)
    case VTK_QUADRATIC_EDGE:
      return VISU::eSEG3;
  #endif

  #if defined(VTK_QUADRATIC_TRIANGLE)
    case VTK_QUADRATIC_TRIANGLE:
      return VISU::eTRIA6;
  #endif

  #if defined(VTK_QUADRATIC_QUAD)
    case VTK_QUADRATIC_QUAD:
      return VISU::eQUAD8;
  #endif

  #if defined(VTK_QUADRATIC_TETRA)
    case VTK_QUADRATIC_TETRA:
      return VISU::eTETRA10;
  #endif

  #if defined(VTK_QUADRATIC_HEXAHEDRON)
    case VTK_QUADRATIC_HEXAHEDRON:
      return VISU::eHEXA20;
  #endif

  #if defined(VTK_QUADRATIC_WEDGE)
    case VTK_QUADRATIC_WEDGE:
      return VISU::ePENTA15;
  #endif

  #if defined(VTK_QUADRATIC_PYRAMID)
    case VTK_QUADRATIC_PYRAMID:
      return VISU::ePYRA13;
  #endif

#endif //VISU_ENABLE_QUADRATIC

    default:
      return EGeometry(-1);
    }
  }

  //---------------------------------------------------------------
  /*! Computes number of points by the given number of cells
   *  in assumption of regular hexahedral mesh structure
   */
  size_t
  GetNumberOfPoints(size_t theNbCells)
  {
    return size_t(pow(pow(theNbCells, 1.0/3.0) + 1.0, 3.0));
  }

  //---------------------------------------------------------------
  /*! Computes size dataset the given number of mesh macro metrics
   *  in assumption of regular hexahedral mesh structure
   */
  size_t
  GetDataSetSize(size_t theNbOfPoints,
                 size_t theNbOfCells,
                 size_t theCellsSize,
                 bool theComputeLinks)
  {
    size_t aPointsSize = 3*theNbOfPoints*sizeof(VISU::TCoord);
    size_t aConnectivityAndTypesSize = theCellsSize*sizeof(vtkIdType);
    size_t aLocationsSize = theNbOfCells*sizeof(int);
    vtkFloatingPointType aNbCellsPerPoint = theCellsSize / theNbOfCells - 1;
    size_t aLinksSize = theNbOfPoints * (vtkIdType(sizeof(vtkIdType)*aNbCellsPerPoint) + sizeof(short));
    if(!theComputeLinks)
      aLinksSize = 0;
    size_t aResult = aPointsSize + aConnectivityAndTypesSize + aLocationsSize + aLinksSize;
    return aResult;
  }

  //---------------------------------------------------------------
  TSizeCounter
  ::TSizeCounter():
    myNbCells(0),
    myCellsSize(0)
  {}


  //---------------------------------------------------------------
  TPolyDataHolder
  ::TPolyDataHolder()
  {}

  const PPolyData&
  TPolyDataHolder
  ::GetSource() const
  {
    if(!mySource.GetPointer()){
      mySource = vtkPolyData::New();
      mySource->Delete();
    }
    return mySource;
  }

  vtkPolyData*
  TPolyDataHolder
  ::GetPolyDataOutput()
  {
    return GetSource().GetPointer();
  }

  unsigned long int
  TPolyDataHolder
  ::GetMemorySize()
  {
    if(vtkDataSet* anOutput = GetPolyDataOutput()){
      anOutput->Update();
      return anOutput->GetActualMemorySize() * 1024;
    }
    if(myIsDone){
      size_t aNbPoints = GetNumberOfPoints(myNbCells);
      return GetDataSetSize(aNbPoints, myNbCells, myCellsSize, false);
    }
    throw std::runtime_error("TUnstructuredGridHolder::GetMemorySize - myIsDone == false !!!");
    return 0;
  }

  //---------------------------------------------------------------
  TUnstructuredGridHolder
  ::TUnstructuredGridHolder()
  {}

  const PUnstructuredGrid&
  TUnstructuredGridHolder
  ::GetSource() const
  {
    if(!mySource.GetPointer()){
      mySource = vtkUnstructuredGrid::New();
      mySource->Delete();
    }
    return mySource;
  }

  vtkUnstructuredGrid*
  TUnstructuredGridHolder
  ::GetUnstructuredGridOutput()
  {
    return GetSource().GetPointer();
  }

  unsigned long int
  TUnstructuredGridHolder
  ::GetMemorySize()
  {
    if(vtkDataSet* anOutput = GetUnstructuredGridOutput()){
      anOutput->Update();
      return anOutput->GetActualMemorySize() * 1024;
    }
    if(myIsDone){
      size_t aNbPoints = GetNumberOfPoints(myNbCells);
      return GetDataSetSize(aNbPoints, myNbCells, myCellsSize, false);
    }
    throw std::runtime_error("TUnstructuredGridHolder::GetMemorySize - myIsDone == false !!!");
    return 0;
  }

  //---------------------------------------------------------------
  unsigned long int
  TMemoryCheckIDMapper
  ::GetMemorySize()
  {
    if(myIsVTKDone){
      if(vtkDataSet* anOutput = GetOutput()){
        anOutput->Update();
        return anOutput->GetActualMemorySize() * 1024;
      }
    }
    throw std::runtime_error("TMemoryCheckIDMapper::GetMemorySize - myIsVTKDone == false !!!");
    return 0;
  }


  //---------------------------------------------------------------
  TAppendFilterHolder
  ::TAppendFilterHolder()
  {}

  const PAppendFilter&
  TAppendFilterHolder
  ::GetFilter() const
  {
    if(!myFilter.GetPointer()){
      myFilter = VISU_AppendFilter::New();
      myFilter->Delete();
      myFilter->SetMappingInputs(true);
    }
    return myFilter;
  }

  vtkUnstructuredGrid*
  TAppendFilterHolder
  ::GetUnstructuredGridOutput()
  {
    GetFilter()->Update();
    return GetFilter()->GetOutput();
  }

  //---------------------------------------------------------------
  TAppendPolyDataHolder
  ::TAppendPolyDataHolder()
  {}

  const PAppendPolyData&
  TAppendPolyDataHolder
  ::GetFilter() const
  {
    if(!myFilter.GetPointer()){
      myFilter = VISU_AppendPolyData::New();
      myFilter->SetMappingInputs(true);
      myFilter->Delete();
    }
    return myFilter;
  }

  vtkPolyData*
  TAppendPolyDataHolder
  ::GetPolyDataOutput()
  {
    GetFilter()->Update();
    return GetFilter()->GetOutput();
  }


  //---------------------------------------------------------------
  TMergeFilterHolder
  ::TMergeFilterHolder()
  {}

  const PMergeFilter&
  TMergeFilterHolder
  ::GetFilter() const
  {
    if(!myFilter.GetPointer()){
      myFilter = VISU_MergeFilter::New();
      myFilter->Delete();
    }
    return myFilter;
  }

  vtkDataSet*
  TMergeFilterHolder
  ::GetOutput()
  {
    GetFilter()->Update();
    return GetFilter()->GetOutput();
  }


  //---------------------------------------------------------------
  TMeshImpl
  ::TMeshImpl():
    myNbPoints(0)
  {}

  vtkIdType
  TMeshImpl::
  GetNbPoints() const
  {
    return myNbPoints;
  }

  vtkIdType
  TMeshImpl::
  GetDim() const
  {
    return myDim;
  }

  vtkPointSet*
  TMeshImpl::
  GetPointSet()
  {
    return myNamedPointCoords->GetPointSet();
  }


  //---------------------------------------------------------------
  TSubProfileImpl::TSubProfileImpl():
    myStatus(eNone),
    myGeom(eNONE)
  {}


  vtkIdType
  TSubProfileImpl
  ::GetElemObjID(vtkIdType theID) const
  {
    if ( !mySubMeshID.empty() )
      return mySubMeshID[theID];

    return theID;
  }


  vtkIdType
  TSubProfileImpl
  ::GetElemVTKID(vtkIdType theID) const
  {
    if ( !mySubMeshID.empty() )
      for ( size_t anId = 0; anId < mySubMeshID.size(); anId++ )
        if ( mySubMeshID[ anId ] == theID )
          return anId;

    return theID;
  }

  unsigned long int
  TSubProfileImpl
  ::GetMemorySize()
  {
    size_t aSize = TUnstructuredGridHolder::GetMemorySize();
    aSize += sizeof(vtkIdType) * mySubMeshID.size();
    return aSize;
  }

  bool 
  TSubProfileImpl
  ::isDefault() const
  {
    return strcmp(myName.c_str(),"") == 0;
  }
  
  //---------------------------------------------------------------
  bool
  operator<(const PSubProfile& theLeft, const PSubProfile& theRight)
  {
    PSubProfileImpl aLeft(theLeft), aRight(theRight);

    if(aLeft->myGeom != aRight->myGeom)
      return aLeft->myGeom < aRight->myGeom;

    if(aLeft->myStatus != aRight->myStatus)
      return aLeft->myStatus < aRight->myStatus;

    return aLeft->myName < aRight->myName;
  }


  //---------------------------------------------------------------
  TProfileImpl
  ::TProfileImpl():
    myIsAll(true),
    myMeshOnEntity(NULL)
  {}

  vtkIdType
  TProfileImpl
  ::GetNodeObjID(vtkIdType theID) const
  {
    return myNamedPointCoords->GetObjID(theID);
  }

  vtkIdType
  TProfileImpl
  ::GetNodeVTKID(vtkIdType theID) const
  {
    return myNamedPointCoords->GetVTKID(theID);
  }

  vtkFloatingPointType*
  TProfileImpl
  ::GetNodeCoord(vtkIdType theObjID)
  {
    if(myIsAll)
      return myMeshOnEntity->GetNodeCoord(theObjID);

    vtkIdType aVtkID = GetNodeVTKID(theObjID);
    return GetFilter()->GetOutput()->GetPoint(aVtkID);
  }

  vtkIdType
  TProfileImpl
  ::GetElemObjID(vtkIdType theID) const
  {
    return VISU::GetElemObjID(GetFilter()->GetOutput(), theID);
  }

  vtkIdType
  TProfileImpl
  ::GetElemVTKID(vtkIdType theID) const
  {
    return VISU::GetElemVTKID(GetFilter()->GetOutput(), theID);
  }

  vtkCell*
  TProfileImpl
  ::GetElemCell(vtkIdType theObjID)
  {
    if(myIsAll)
      return myMeshOnEntity->GetElemCell(theObjID);

    vtkIdType aVtkID = GetElemVTKID(theObjID);
    return GetFilter()->GetOutput()->GetCell(aVtkID);
  }

  vtkUnstructuredGrid*
  TProfileImpl
  ::GetUnstructuredGridOutput()
  {
    const PAppendFilter& anAppendFilter = GetFilter();
    return anAppendFilter->GetOutput();
  }

  unsigned long int
  TProfileImpl
  ::GetMemorySize()
  {
    size_t aSize = TAppendFilterHolder::GetMemorySize();
    aSize += myNamedPointCoords->GetMemorySize();
    aSize += myElemObj2VTKID.size() * 2 * sizeof(vtkIdType);
    TGeom2SubProfile::const_iterator anIter = myGeom2SubProfile.begin();
    TGeom2SubProfile::const_iterator anIterEnd = myGeom2SubProfile.end();
    for(; anIter != anIterEnd; anIter++){
      const PSubProfileImpl& aSubProfile = anIter->second;
      aSize += aSubProfile->GetMemorySize();
      aSize += sizeof(EGeometry);
    }
    return aSize;
  }

  std::string
  TProfileImpl
  ::GetNodeName(vtkIdType theObjID) const
  {
    return myNamedPointCoords->GetNodeName(theObjID);
  }

  std::string
  TProfileImpl
  ::GetElemName(vtkIdType theObjID) const
  {
    return myMeshOnEntity->GetElemName(theObjID);
  }


  //---------------------------------------------------------------
  TUnstructuredGridIDMapperImpl
  ::TUnstructuredGridIDMapperImpl()
  {
    if ( !myCommonCellsFilter.GetPointer() ) {
      myCommonCellsFilter = VISU_CommonCellsFilter::New();
      myCommonCellsFilter->Delete();
    }
  }

  vtkIdType
  TUnstructuredGridIDMapperImpl
  ::GetNodeObjID(vtkIdType theID) const
  {
    return myIDMapper->GetNodeObjID(theID);
  }

  vtkIdType
  TUnstructuredGridIDMapperImpl
  ::GetNodeVTKID(vtkIdType theID) const
  {
    return myIDMapper->GetNodeVTKID(theID);
  }

  vtkFloatingPointType*
  TUnstructuredGridIDMapperImpl
  ::GetNodeCoord(vtkIdType theObjID)
  {
    return myIDMapper->GetNodeCoord(theObjID);
  }

  vtkIdType
  TUnstructuredGridIDMapperImpl
  ::GetElemObjID(vtkIdType theID) const
  {
    return myIDMapper->GetElemObjID(theID);
  }

  vtkIdType
  TUnstructuredGridIDMapperImpl
  ::GetElemVTKID(vtkIdType theID) const
  {
    return myIDMapper->GetElemVTKID(theID);
  }

  vtkCell*
  TUnstructuredGridIDMapperImpl
  ::GetElemCell(vtkIdType theObjID)
  {
    return myIDMapper->GetElemCell(theObjID);
  }

  void
  TUnstructuredGridIDMapperImpl
  ::SetReferencedMesh( const PNamedIDMapper& theNamedIDMapper )
  {
    myCommonCellsFilter->SetCellsUG( theNamedIDMapper->GetUnstructuredGridOutput() );
  }

  void
  TUnstructuredGridIDMapperImpl
  ::Build()
  {
    if ( !myFilter.GetPointer() ) {
      const PAppendFilter& anAppendFilter = myIDMapper->GetFilter();

      vtkUnstructuredGrid* aGeometry = anAppendFilter->GetOutput();
      const PUnstructuredGrid& aSource = mySource.GetSource();
      vtkUnstructuredGrid* aDataSet = aSource.GetPointer();
      aDataSet->ShallowCopy( aGeometry );

      const PMergeFilter& aFilter = GetFilter();
      aFilter->SetGeometry( aGeometry );
      aFilter->SetScalars( aDataSet );
      aFilter->SetVectors( aDataSet );
      aFilter->AddField( "VISU_FIELD", aDataSet );
      aFilter->AddField( "VISU_FIELD_GAUSS_MIN", aDataSet );
      aFilter->AddField( "VISU_FIELD_GAUSS_MAX", aDataSet );
      aFilter->AddField( "VISU_FIELD_GAUSS_MOD", aDataSet );
      aFilter->AddField( "VISU_CELLS_MAPPER", aDataSet );
      aFilter->AddField( "ELNO_FIELD", aDataSet );
      aFilter->AddField( "ELNO_COMPONENT_MAPPER", aDataSet );
      aFilter->AddField( "VISU_POINTS_MAPPER", aDataSet );

      myCommonCellsFilter->SetProfileUG( aFilter->GetUnstructuredGridOutput() );
    }
  }

  vtkUnstructuredGrid*
  TUnstructuredGridIDMapperImpl
  ::GetUnstructuredGridOutput()
  {
    Build();
    return myCommonCellsFilter->GetOutput();
  }

  vtkDataSet*
  TUnstructuredGridIDMapperImpl
  ::GetOutput()
  {
    return GetUnstructuredGridOutput();
  }

  PUnstructuredGrid
  TUnstructuredGridIDMapperImpl
  ::GetSource()
  {
    Build();
    return mySource.GetSource();
  }

  unsigned long int
  TUnstructuredGridIDMapperImpl
  ::GetMemorySize()
  {
    size_t aSize = myIDMapper->GetMemorySize();

    aSize += mySource.GetMemorySize();

    if ( vtkUnstructuredGrid* anOutput = myCommonCellsFilter->GetOutput() )
      aSize += anOutput->GetActualMemorySize() * 1024;

    return aSize;
  }

  //---------------------------------------------------------------
  vtkIdType
  TPolyDataIDMapperImpl
  ::GetNodeObjID(vtkIdType theID) const
  {
    return myIDMapper->GetNodeObjID(theID);
  }

  vtkIdType
  TPolyDataIDMapperImpl
  ::GetNodeVTKID(vtkIdType theID) const
  {
    return myIDMapper->GetNodeVTKID(theID);
  }

  vtkFloatingPointType*
  TPolyDataIDMapperImpl
  ::GetNodeCoord(vtkIdType theObjID)
  {
    return myIDMapper->GetNodeCoord(theObjID);
  }

  vtkIdType
  TPolyDataIDMapperImpl
  ::GetElemObjID(vtkIdType theID) const
  {
    return myIDMapper->GetElemObjID(theID);
  }

  vtkIdType
  TPolyDataIDMapperImpl
  ::GetElemVTKID(vtkIdType theID) const
  {
    return myIDMapper->GetElemVTKID(theID);
  }

  vtkCell*
  TPolyDataIDMapperImpl
  ::GetElemCell(vtkIdType theObjID)
  {
    return myIDMapper->GetElemCell(theObjID);
  }

  void
  TPolyDataIDMapperImpl
  ::Build()
  {
    if ( !myFilter.GetPointer() ) {
      const PAppendPolyData& anAppendFilter = myIDMapper->GetFilter();
      vtkPolyData* aGeometry = anAppendFilter->GetOutput();

      const PPolyData& aSource = mySource.GetSource();
      vtkPolyData* aDataSet = aSource.GetPointer();
      aDataSet->ShallowCopy( aGeometry );

      const PMergeFilter& aFilter = GetFilter();
      aFilter->SetGeometry( aGeometry );
      aFilter->SetScalars( aDataSet );
      aFilter->SetVectors( aDataSet );
      aFilter->AddField( "VISU_FIELD", aDataSet );
      aFilter->AddField( "VISU_FIELD_GAUSS_MIN", aDataSet );
      aFilter->AddField( "VISU_FIELD_GAUSS_MAX", aDataSet );
      aFilter->AddField( "VISU_FIELD_GAUSS_MOD", aDataSet );
      aFilter->AddField( "VISU_CELLS_MAPPER", aDataSet );
      aFilter->AddField( "VISU_POINTS_MAPPER", aDataSet );
    }
  }

  vtkPolyData*
  TPolyDataIDMapperImpl
  ::GetPolyDataOutput()
  {
    Build();
    return myFilter->GetPolyDataOutput();
  }

  vtkDataSet*
  TPolyDataIDMapperImpl
  ::GetOutput()
  {
    return GetPolyDataOutput();
  }

  PPolyData
  TPolyDataIDMapperImpl
  ::GetSource()
  {
    Build();
    return mySource.GetSource();
  }

  unsigned long int
  TPolyDataIDMapperImpl
  ::GetMemorySize()
  {
    size_t aSize = myIDMapper->GetMemorySize();
    aSize += mySource.GetMemorySize();
    return aSize;
  }


  //---------------------------------------------------------------
  TGaussImpl
  ::TGaussImpl():
    myGeom(EGeometry(-1)),
    myNbPoints(0)
  {}

  void
  TGaussImpl
  ::LessThan(const PGaussImpl& theGauss,
             bool& theResult) const
  {
    theResult = false;
  }


  //---------------------------------------------------------------
  TGaussSubMeshImpl
  ::TGaussSubMeshImpl():
    myPointCoords(new TPointCoords()),
    myStatus(eNone),
    myStartID(0)
  {}

  TGaussPointID
  TGaussSubMeshImpl
  ::GetObjID(vtkIdType theID) const
  {
    TCellID aCellID = myStartID + theID / myGauss->myNbPoints;
    TLocalPntID aLocalPntID = theID % myGauss->myNbPoints;

    return TGaussPointID(aCellID, aLocalPntID);
  }

  vtkIdType
  TGaussSubMeshImpl
  ::GetVTKID(const TGaussPointID& theID) const
  {
    vtkIdType aResult = -1;

    TCellID aCellID = theID.first;
    TLocalPntID aLocalPntID = theID.second;

    vtkIdType aNbPoints = myGauss->myNbPoints;
    if ( aLocalPntID >= aNbPoints )
      return aResult;

    return ( aCellID - myStartID ) * aNbPoints + aLocalPntID;
  }

  vtkIdType
  VISU::TGaussSubMeshImpl
  ::GetElemObjID(vtkIdType theID) const
  {
    return mySubProfile->GetElemObjID( theID );
  }


  vtkIdType
  VISU::TGaussSubMeshImpl
  ::GetElemVTKID(vtkIdType theID) const
  {
    return mySubProfile->GetElemVTKID( theID );
  }

  vtkIdType
  TGaussSubMeshImpl
  ::GetGlobalID(vtkIdType theID) const
  {
    return myStartID + theID;
  }

  unsigned long int
  TGaussSubMeshImpl
  ::GetMemorySize()
  {
    size_t aSize = TPolyDataHolder::GetMemorySize();
    aSize += myPointCoords->GetMemorySize();
    return aSize;
  }

  //---------------------------------------------------------------
  bool
  operator<(const PGaussSubMesh& theLeft, const PGaussSubMesh& theRight)
  {
    PGaussSubMeshImpl aLeft(theLeft), aRight(theRight);
    const PGaussImpl& aGaussLeft = aLeft->myGauss;
    const PGaussImpl& aGaussRight = aRight->myGauss;

    if(aGaussLeft->myGeom != aGaussRight->myGeom)
      return aGaussLeft->myGeom < aGaussRight->myGeom;

    if(aLeft->mySubProfile != aRight->mySubProfile)
      return aLeft->mySubProfile < aRight->mySubProfile;

    bool aResult;
    aGaussLeft->LessThan(aGaussRight,aResult);

    return aResult;
  }


  //---------------------------------------------------------------
  TGaussMeshImpl
  ::TGaussMeshImpl():
    myParent(NULL)
  {}

  TGaussPointID
  TGaussMeshImpl
  ::GetObjID(vtkIdType theID) const
  {
    const PAppendPolyData& aFilter = GetFilter();
    return VISU::GetObjID(aFilter->GetOutput(), theID);
  }

  vtkIdType
  TGaussMeshImpl
  ::GetVTKID(const TGaussPointID& theID) const
  {
    vtkIdType aResult = -1;

    TCellID aCellID = theID.first;

    vtkIdType aVTKCellId = GetParent()->GetElemVTKID( aCellID );
    if ( aVTKCellId < 0 )
      return aResult;

    vtkCell* aCell = GetParent()->GetElemCell( aCellID );
    if ( !aCell )
      return aResult;

    EGeometry aVGeom = VISU::VTKGeom2VISU( aCell->GetCellType() );
    if ( aVGeom < EGeometry(0) )
      return aResult;

    TGeom2GaussSubMesh::const_iterator anIter = myGeom2GaussSubMesh.find( aVGeom );
    if ( anIter == myGeom2GaussSubMesh.end() )
      return aResult;

    size_t aSubMeshEnd = myGaussSubMeshArr.size();
    const PGaussSubMeshImpl& aGaussSubMesh = anIter->second;
    for ( size_t aSubMeshId = 0; aSubMeshId < aSubMeshEnd; aSubMeshId++ ) {
      const PGaussSubMeshImpl& aSubMesh = myGaussSubMeshArr[aSubMeshId];
      if ( aGaussSubMesh.get() == aSubMesh.get() ) {
        return aGaussSubMesh->GetVTKID(theID);
      }
    }

    return aResult;
  }

  vtkPolyData*
  TGaussMeshImpl
  ::GetPolyDataOutput()
  {
    return TAppendPolyDataHolder::GetPolyDataOutput();
  }

  unsigned long int
  TGaussMeshImpl
  ::GetMemorySize()
  {
    size_t aSize = TAppendPolyDataHolder::GetMemorySize();
    TGeom2GaussSubMesh::const_iterator anIter = myGeom2GaussSubMesh.begin();
    TGeom2GaussSubMesh::const_iterator anIterEnd = myGeom2GaussSubMesh.end();
    for(; anIter != anIterEnd; anIter++){
      const PGaussSubMeshImpl& aGaussSubMesh = anIter->second;
      aSize += aGaussSubMesh->GetMemorySize();
      aSize += sizeof(EGeometry);
    }
    return aSize;
  }

  TNamedIDMapper*
  TGaussMeshImpl
  ::GetParent() const
  {
    return myParent;
  }


  //---------------------------------------------------------------
  TGaussPointID
  TGaussPtsIDFilter
  ::GetObjID(vtkIdType theID) const
  {
    return myGaussPtsIDMapper->GetObjID(theID);
  }

  vtkIdType
  TGaussPtsIDFilter
  ::GetVTKID(const TGaussPointID& theID) const
  {
    return myGaussPtsIDMapper->GetVTKID(theID);
  }

  TNamedIDMapper*
  TGaussPtsIDFilter
  ::GetParent() const
  {
    return myGaussPtsIDMapper->GetParent();
  }

  vtkPolyData*
  TGaussPtsIDFilter
  ::GetPolyDataOutput()
  {
    return TPolyDataIDMapperImpl::GetPolyDataOutput();
  }

  vtkDataSet*
  TGaussPtsIDFilter
  ::GetOutput()
  {
    return GetPolyDataOutput();
  }


  //---------------------------------------------------------------
  TSubMeshImpl
  ::TSubMeshImpl():
    myStartID(0)
  {}

  void
  TSubMeshImpl
  ::CopyStructure( PStructured theStructured )
  {
    TStructured::CopyStructure( theStructured );

    if ( PMeshImpl aMesh = theStructured )
      GetSource()->ShallowCopy( aMesh->GetPointSet() );
  }

  vtkIdType
  TSubMeshImpl
  ::GetElemObjID(vtkIdType theID) const
  {
    return myStartID + theID;
  }

  std::string
  TSubMeshImpl
  ::GetElemName(vtkIdType theObjID) const
  {
    return "";
  }

  unsigned long int
  TSubMeshImpl
  ::GetMemorySize()
  {
    size_t aSize = TUnstructuredGridHolder::GetMemorySize();
    for(size_t anId = 0; anId < myCell2Connect.size(); anId++){
      const TConnect& aConnect = myCell2Connect[anId];
      aSize += aConnect.size() * sizeof(vtkIdType);
    }
    return aSize;
  }

  //---------------------------------------------------------------
  TMeshOnEntityImpl
  ::TMeshOnEntityImpl()
  {
    const PAppendFilter& anAppendFilter = GetFilter();
    anAppendFilter->SetMappingInputs(true);
  }

  void
  TMeshOnEntityImpl
  ::CopyStructure( PStructured theStructured )
  {
    TStructured::CopyStructure( theStructured );

    if ( PMeshImpl aMesh = theStructured )
      myNamedPointCoords = aMesh->myNamedPointCoords;
  }

  vtkIdType
  TMeshOnEntityImpl
  ::GetNodeVTKID(vtkIdType theID) const
  {
    return myNamedPointCoords->GetVTKID(theID);
  }

  vtkIdType
  TMeshOnEntityImpl
  ::GetNodeObjID(vtkIdType theID) const
  {
    return myNamedPointCoords->GetObjID(theID);
  }

  vtkIdType
  TMeshOnEntityImpl
  ::GetElemVTKID(vtkIdType theID) const
  {
    return VISU::GetElemVTKID(GetFilter()->GetOutput(), theID);
  }

  vtkIdType
  TMeshOnEntityImpl
  ::GetElemObjID(vtkIdType theID) const
  {
    return VISU::GetElemObjID(GetFilter()->GetOutput(), theID);
  }

  std::string
  TMeshOnEntityImpl
  ::GetNodeName(vtkIdType theObjID) const
  {
    return myNamedPointCoords->GetNodeName(theObjID);
  }

  std::string
  TMeshOnEntityImpl
  ::GetElemName(vtkIdType theObjID) const
  {
    TInputCellID anInputCellID = VISU::GetInputCellID(GetFilter()->GetOutput(), theObjID);
    const PSubMeshImpl& aSubMesh = mySubMeshArr[anInputCellID.first];
    return aSubMesh->GetElemName(anInputCellID.second);
  }

  vtkUnstructuredGrid*
  TMeshOnEntityImpl
  ::GetUnstructuredGridOutput()
  {
    return TAppendFilterHolder::GetUnstructuredGridOutput();
  }

  unsigned long int
  TMeshOnEntityImpl
  ::GetMemorySize()
  {
    size_t aSize = TAppendFilterHolder::GetMemorySize();
    aSize += myNamedPointCoords->GetMemorySize();
    aSize += myElemObj2VTKID.size() * 2 * sizeof(vtkIdType);
    TGeom2SubMesh::const_iterator anIter = myGeom2SubMesh.begin();
    TGeom2SubMesh::const_iterator anIterEnd = myGeom2SubMesh.end();
    for(; anIter != anIterEnd; anIter++){
      const PSubMeshImpl& aSubMesh = anIter->second;
      aSize += aSubMesh->GetMemorySize();
      aSize += sizeof(EGeometry);
    }
    return aSize;
  }

  //---------------------------------------------------------------
  void
  TFamilyImpl
  ::CopyStructure( PStructured theStructured )
  {
    TStructured::CopyStructure( theStructured );

    if ( PMeshImpl aMesh = theStructured ) {
      myNamedPointCoords = aMesh->myNamedPointCoords;
      GetSource()->ShallowCopy( aMesh->GetPointSet() );
    }
  }

  vtkIdType
  TFamilyImpl
  ::GetElemVTKID(vtkIdType theID) const
  {
    if(myElemObj2VTKID.empty())
      return theID;
    else{
      TID2ID::const_iterator anIter = myElemObj2VTKID.find(theID);
      if(anIter != myElemObj2VTKID.end())
        return anIter->second;
    }
    return -1;
  }

  vtkIdType
  TFamilyImpl
  ::GetElemObjID(vtkIdType theID) const
  {
    return myMeshID[theID];
  }

  vtkIdType
  TFamilyImpl
  ::GetNodeObjID(vtkIdType theID) const
  {
    return myNamedPointCoords->GetObjID(theID);
  }

  vtkIdType
  TFamilyImpl
  ::GetNodeVTKID(vtkIdType theID) const
  {
    return myNamedPointCoords->GetVTKID(theID);
  }

  vtkUnstructuredGrid*
  TFamilyImpl
  ::GetUnstructuredGridOutput()
  {
    return TUnstructuredGridHolder::GetUnstructuredGridOutput();
  }

  unsigned long int
  TFamilyImpl
  ::GetMemorySize()
  {
    size_t aSize = TUnstructuredGridHolder::GetMemorySize();
    aSize += myNamedPointCoords->GetMemorySize();
    aSize += myElemObj2VTKID.size() * 2 * sizeof(vtkIdType);
    aSize += myMeshID.size() * sizeof(vtkIdType);
    TGeom2SubMeshID::const_iterator anIter = myGeom2SubMeshID.begin();
    TGeom2SubMeshID::const_iterator anIterEnd = myGeom2SubMeshID.end();
    for(; anIter != anIterEnd; anIter++){
      const TSubMeshID& aSubMeshID = anIter->second;
      aSize += aSubMeshID.size() * sizeof(vtkIdType);
      aSize += sizeof(EGeometry);
    }
    return aSize;
  }


  //---------------------------------------------------------------
  void
  TGroupImpl
  ::CopyStructure( PStructured theStructured )
  {
    TStructured::CopyStructure( theStructured );

    if ( PMeshImpl aMesh = theStructured )
      myNamedPointCoords = aMesh->myNamedPointCoords;
  }

  TNbASizeCells
  TGroupImpl
  ::GetNbASizeCells() const
  {
    vtkIdType aNbCells = 0, aCellsSize = 0;
    TFamilySet::const_iterator anIter = myFamilySet.begin();
    for(; anIter != myFamilySet.end(); anIter++){
      PFamilyImpl aFamily = (*anIter).second;
      aNbCells += aFamily->myNbCells;
      aCellsSize += aFamily->myCellsSize;
    }
    return std::make_pair(aNbCells,aCellsSize);
  }

  vtkIdType
  TGroupImpl
  ::GetElemVTKID(vtkIdType theID) const
  {
    if(myElemObj2VTKID.empty())
      return theID;
    else{
      TID2ID::const_iterator anIter = myElemObj2VTKID.find(theID);
      if(anIter != myElemObj2VTKID.end())
        return anIter->second;
    }
    return -1;
  }

  vtkIdType
  TGroupImpl
  ::GetElemObjID(vtkIdType theID) const
  {
    return VISU::GetElemObjID(GetFilter()->GetOutput(), theID);
  }

  vtkIdType
  TGroupImpl
  ::GetNodeObjID(vtkIdType theID) const
  {
    return myNamedPointCoords->GetObjID(theID);
  }

  vtkIdType
  TGroupImpl
  ::GetNodeVTKID(vtkIdType theID) const
  {
    return myNamedPointCoords->GetVTKID(theID);
  }

  vtkUnstructuredGrid*
  TGroupImpl
  ::GetUnstructuredGridOutput()
  {
    return TAppendFilterHolder::GetUnstructuredGridOutput();
  }

  unsigned long int
  TGroupImpl
  ::GetMemorySize()
  {
    size_t aSize = TAppendFilterHolder::GetMemorySize();
    aSize += myNamedPointCoords->GetMemorySize();
    aSize += myElemObj2VTKID.size() * 2 * sizeof(vtkIdType);
    for(size_t anId = 0; anId < myFamilyArr.size(); anId++){
      const PFamilyImpl& aFamily = myFamilyArr[anId];
      aSize += aFamily->GetMemorySize();
    }
    return aSize;
  }



  //---------------------------------------------------------------
  TFieldImpl
  ::TFieldImpl()
    : myDataSize( 0 )
    , myDataType( 0 )
  {}

  void
  TFieldImpl
  ::Init(vtkIdType theNbComp,
         vtkIdType theDataType)
  {
    myNbComp = theNbComp;
    myDataType = theDataType;
    myCompNames.resize(theNbComp);
    myUnitNames.resize(theNbComp);

    myMetric2Comp2MinMax.resize(3);
    myMetric2Comp2AverageMinMax.resize(3);
    myMetric2Comp2Group2MinMax.resize(3);
    myMetric2Comp2Group2AverageMinMax.resize(3);
    for(int aGaussMetric = (int)VISU::AVERAGE_METRIC; aGaussMetric <= (int)VISU::MAXIMUM_METRIC; aGaussMetric++){
      TComp2MinMax& aComp2MinMax = myMetric2Comp2MinMax[aGaussMetric];
      TComp2MinMax& aComp2AverageMinMax = myMetric2Comp2AverageMinMax[aGaussMetric];
      TComp2Group2MinMax& aComp2Group2MinMax = myMetric2Comp2Group2MinMax[aGaussMetric];
      TComp2Group2MinMax& aComp2Group2AverageMinMax = myMetric2Comp2Group2AverageMinMax[aGaussMetric];

      aComp2MinMax.resize(theNbComp + 1);
      aComp2AverageMinMax.resize(theNbComp + 1);
      aComp2Group2MinMax.resize(theNbComp + 1);
      aComp2Group2AverageMinMax.resize(theNbComp + 1);
      for(vtkIdType iComp = 0; iComp <= theNbComp; iComp++){
        TMinMax& aMinMax = aComp2MinMax[iComp];
        aMinMax.first = VTK_LARGE_FLOAT;
        aMinMax.second = -VTK_LARGE_FLOAT;
        TMinMax& anAverageMinMax = aComp2AverageMinMax[iComp];
        anAverageMinMax.first = VTK_LARGE_FLOAT;
        anAverageMinMax.second = -VTK_LARGE_FLOAT;
      }
    }
  }

  vtkIdType
  TFieldImpl
  ::GetDataType() const
  {
    return myDataType;
  }

  TMinMax
  TFieldImpl
  ::GetMinMax(vtkIdType theCompID, const TNames& theGroupNames, TGaussMetric theGaussMetric)
  {
    TMinMax aMinMax;
    bool anIsMinMaxInitialized = false;
    if( !theGroupNames.empty() ) {
      aMinMax.first = VTK_LARGE_FLOAT;
      aMinMax.second = -VTK_LARGE_FLOAT;

      const TGroup2MinMax& aGroup2MinMax = myMetric2Comp2Group2MinMax[theGaussMetric][theCompID];
      TNames::const_iterator aNameIter = theGroupNames.begin();
      for( ; aNameIter != theGroupNames.end(); aNameIter++ ) {
        TGroup2MinMax::const_iterator aGroup2MinMaxIter = aGroup2MinMax.find( *aNameIter );
        if( aGroup2MinMaxIter != aGroup2MinMax.end() ) {
          const TMinMax& aGroupMinMax = aGroup2MinMaxIter->second;
          aMinMax.first = std::min( aMinMax.first, aGroupMinMax.first );
          aMinMax.second = std::max( aMinMax.second, aGroupMinMax.second );
          anIsMinMaxInitialized = true;
        }
      }
    }

    if( !anIsMinMaxInitialized )
      aMinMax = myMetric2Comp2MinMax[theGaussMetric][theCompID];

    return aMinMax;
  }


  TMinMax
  TFieldImpl
  ::GetAverageMinMax(vtkIdType theCompID, const TNames& theGroupNames, TGaussMetric theGaussMetric)
  {
    TMinMax aMinMax;
    bool anIsMinMaxInitialized = false;
    if( !theGroupNames.empty() ) {
      aMinMax.first = VTK_LARGE_FLOAT;
      aMinMax.second = -VTK_LARGE_FLOAT;

      const TGroup2MinMax& aGroup2MinMax = myMetric2Comp2Group2AverageMinMax[theGaussMetric][theCompID];
      TNames::const_iterator aNameIter = theGroupNames.begin();
      for( ; aNameIter != theGroupNames.end(); aNameIter++ ) {
        TGroup2MinMax::const_iterator aGroup2MinMaxIter = aGroup2MinMax.find( *aNameIter );
        if( aGroup2MinMaxIter != aGroup2MinMax.end() ) {
          const TMinMax& aGroupMinMax = aGroup2MinMaxIter->second;
          aMinMax.first = std::min( aMinMax.first, aGroupMinMax.first );
          aMinMax.second = std::max( aMinMax.second, aGroupMinMax.second );
          anIsMinMaxInitialized = true;
        }
      }
    }

    if( !anIsMinMaxInitialized )
      aMinMax = myMetric2Comp2AverageMinMax[theGaussMetric][theCompID];

    return aMinMax;
  }

  //----------------------------------------------------------------------------
  const PMeshValue&
  TGeom2Value
  ::GetMeshValue(EGeometry theGeom) const
  {
    TGeom2MeshValue::const_iterator anIter = myGeom2MeshValue.find(theGeom);
    if(anIter == myGeom2MeshValue.end())
      EXCEPTION(std::runtime_error,"TGeom2Value::GetMeshValue - myGeom2MeshValue.find(theGeom) fails");
    return anIter->second;
  }

  PMeshValue&
  TGeom2Value
  ::GetMeshValue(EGeometry theGeom)
  {
    return myGeom2MeshValue[theGeom];
  }


  //----------------------------------------------------------------------------
  TGeom2MeshValue&
  TGeom2Value
  ::GetGeom2MeshValue()
  {
    return myGeom2MeshValue;
  }

  const TGeom2MeshValue&
  TGeom2Value
  ::GetGeom2MeshValue() const
  {
    return myGeom2MeshValue;
  }

  PMeshValue
  TGeom2Value
  ::GetFirstMeshValue() const
  {
    if(myGeom2MeshValue.size() == 1)
      return myGeom2MeshValue.begin()->second;
    return PMeshValue();
  }


  //---------------------------------------------------------------
  TValForTimeImpl
  ::TValForTimeImpl():
    myGaussPtsIDFilter(new TGaussPtsIDFilter()),
    myUnstructuredGridIDMapper(new TUnstructuredGridIDMapperImpl()),
	myIsFilled(false)
  {}

  const PMeshValue&
  TValForTimeImpl
  ::GetMeshValue(EGeometry theGeom) const
  {
    return myGeom2Value.GetMeshValue(theGeom);
  }

  PMeshValue&
  TValForTimeImpl
  ::GetMeshValue(EGeometry theGeom)
  {
    return myGeom2Value.GetMeshValue(theGeom);
  }

  TGeom2MeshValue&
  TValForTimeImpl
  ::GetGeom2MeshValue()
  {
    return myGeom2Value.GetGeom2MeshValue();
  }

  const TGeom2MeshValue&
  TValForTimeImpl
  ::GetGeom2MeshValue() const
  {
    return myGeom2Value.GetGeom2MeshValue();
  }

  PMeshValue
  TValForTimeImpl
  ::GetFirstMeshValue() const
  {
    return myGeom2Value.GetFirstMeshValue();
  }

  int
  TValForTimeImpl
  ::GetNbGauss(EGeometry theGeom) const
  {
    TGeom2NbGauss::const_iterator anIter = myGeom2NbGauss.find(theGeom);
    if(anIter == myGeom2NbGauss.end()){
      return 1;
    }
    return anIter->second;
  }

  int
  TValForTimeImpl
  ::GetMaxNbGauss() const
  {
    int aNbGauss = 1;
    TGeom2NbGauss::const_iterator anIter = myGeom2NbGauss.begin();
    for(; anIter != myGeom2NbGauss.end(); anIter++){
      aNbGauss = std::max<int>(aNbGauss, anIter->second);
    }
    return aNbGauss;
  }

  unsigned long int
  TValForTimeImpl
  ::GetMemorySize()
  {
    size_t aSize = sizeof(TValForTimeImpl);
    const TGeom2MeshValue& aGeom2MeshValue = GetGeom2MeshValue();
    TGeom2MeshValue::const_iterator anIter = aGeom2MeshValue.begin();
    TGeom2MeshValue::const_iterator anIterEnd = aGeom2MeshValue.end();
    for(; anIter != anIterEnd; anIter++){
      const PMeshValue& aMeshValue = anIter->second;
      aSize += aMeshValue->GetMemorySize();
      aSize += sizeof(EGeometry);
    }
    return aSize;
  }

  //---------------------------------------------------------------
}
