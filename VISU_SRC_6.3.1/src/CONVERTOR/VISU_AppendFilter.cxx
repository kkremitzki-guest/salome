// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  SALOME OBJECT : kernel of SALOME component
//  File   : VISU_GeometryFilter.cxx
//  Author : 
//  Module : SALOME
//  $Header: /home/server/cvs/VISU/VISU_SRC/src/CONVERTOR/VISU_AppendFilter.cxx,v 1.3.2.1.6.1.8.1 2011-06-02 06:00:16 vsr Exp $
//
#include "VISU_AppendFilter.hxx"

#include <vtkObjectFactory.h>

//---------------------------------------------------------------
vtkCxxRevisionMacro(VISU_AppendFilter, "$Revision: 1.3.2.1.6.1.8.1 $");
vtkStandardNewMacro(VISU_AppendFilter);


//---------------------------------------------------------------
VISU_AppendFilter
::VISU_AppendFilter():
  TAppendFilterHelper(this)
{}


//---------------------------------------------------------------
VISU_AppendFilter
::~VISU_AppendFilter()
{}


//---------------------------------------------------------------
int
VISU_AppendFilter
::RequestData(vtkInformation *theRequest,
              vtkInformationVector **theInputVector,
              vtkInformationVector *theOutputVector)
{
  bool anIsExecuted = false;
  if( IsMergingInputs() || IsMappingInputs() )
    anIsExecuted = VISU::UnstructuredGridRequestData(theInputVector,
                                                     GetNumberOfInputConnections( 0 ),
                                                     theOutputVector,
                                                     GetSharedPointSet(),
                                                     IsMergingInputs(),
                                                     IsMappingInputs());
  if( !anIsExecuted )
    anIsExecuted = Superclass::RequestData(theRequest,
                                           theInputVector,
                                           theOutputVector);

  return anIsExecuted;
}
