# Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
# See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#

#  VISU OBJECT : interactive object for VISU entities implementation
#  File   : Makefile.am
#  Module : VISU
#
include $(top_srcdir)/adm_local/unix/make_common_starter.am

lib_LTLIBRARIES = libVisuConvertor.la

salomeinclude_HEADERS= \
	VISU_TypeList.hxx \
	VISU_VTKTypeList.hxx \
	VISU_IDMapper.hxx \
	VISU_ConvertorDef.hxx \
	VISU_Structures.hxx \
	VISU_Convertor.hxx \
	VISU_ConvertorDef_impl.hxx \
	VISU_Structures_impl.hxx \
	VISU_MeshValue.hxx \
	VISU_ElnoMeshValue.hxx \
	VISU_PointCoords.hxx \
	VISU_Convertor_impl.hxx \
	VISU_ConvertorUtils.hxx \
	VISU_MergeFilter.hxx \
	VISU_MergeFilterUtilities.hxx \
	VISU_AppendFilter.hxx \
	VISU_AppendPolyData.hxx \
	VISU_AppendFilterUtilities.hxx \
	VISU_ExtractUnstructuredGrid.hxx \
	VISU_UsedPointsFilter.hxx \
	VISU_CommonCellsFilter.hxx \
	VISUConvertor.hxx \
	VISU_MedConvertor.hxx \
	VISU_GaussMergeFilter.hxx \
	VISU_TableReader.hxx \
	VISU_Vtk2MedConvertor.hxx

dist_libVisuConvertor_la_SOURCES= \
	VISU_IDMapper.cxx \
	VISU_Structures.cxx \
	VISU_Convertor.cxx \
	VISU_Structures_impl.cxx \
	VISU_MeshValue.cxx \
	VISU_PointCoords.cxx \
	VISU_Convertor_impl.cxx \
	VISU_MedConvertor.cxx \
	VISU_ConvertorUtils.cxx \
	VISU_ExtractUnstructuredGrid.cxx \
	VISU_AppendFilter.cxx \
	VISU_AppendPolyData.cxx \
	VISU_AppendFilterUtilities.cxx \
	VISU_MergeFilter.cxx \
	VISU_MergeFilterUtilities.cxx \
	VISU_UsedPointsFilter.cxx \
	VISU_CommonCellsFilter.cxx \
	VISU_GaussMergeFilter.cxx \
	VISU_TableReader.cxx \
	VISU_Vtk2MedConvertor.cxx

libVisuConvertor_la_CPPFLAGS= \
	-ftemplate-depth-32 \
	$(VTK_INCLUDES) \
	$(HDF5_INCLUDES) \
	$(QT_INCLUDES) \
	@CAS_CPPFLAGS@ \
	@CAS_CXXFLAGS@ \
	@KERNEL_CXXFLAGS@ \
	@MED_CXXFLAGS@ \
	@GUI_CXXFLAGS@ \
	$(BOOST_CPPFLAGS) \
	$(MED_CPPFLAGS)


libVisuConvertor_la_LDFLAGS= \
	$(MED2_LIBS) \
	$(BOOST_LIB_THREAD) \
	$(BOOST_LIB_DATE_TIME) \
	$(VTK_LIBS) \
	$(QT_LIBS) \
	$(KERNEL_LDFLAGS) -lSALOMEBasics \
	$(CAS_KERNEL) \
	$(CAS_MATH) \
	$(MED_LDFLAGS) -lMEDWrapper -lMEDWrapper_V2_2 -lMEDWrapper_V2_1 -lMEDWrapperBase \
	$(GUI_LDFLAGS) -lVTKViewer

# Executables targets
bin_PROGRAMS= VISUConvertor
dist_VISUConvertor_SOURCES= VISUConvertor.cxx
VISUConvertor_CPPFLAGS=$(libVisuConvertor_la_CPPFLAGS)
VISUConvertor_LDADD=$(libVisuConvertor_la_LDFLAGS) \
	-lMEDWrapper_V2_2 -lMEDWrapper_V2_1 -lMEDWrapperBase -lMEDWrapper -lVTKViewer -lsuit -lqtx libVisuConvertor.la
