// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File:
//  Author:
//  Module : VISU
//
#include "VISU_PointCoords.hxx"
#include "VISU_ConvertorUtils.hxx"

#include <vtkUnstructuredGrid.h>
#include <vtkPointData.h>
#include <vtkIntArray.h>

#ifdef _DEBUG_
static int MYDEBUG = 0;
#else
static int MYDEBUG = 0;
#endif

namespace VISU
{
  //---------------------------------------------------------------
  void
  TCoordHolderBase
  ::Init(vtkIdType theNbPoints,
         vtkIdType theDim)
  {
    myDim = theDim;
    myNbPoints = theNbPoints;
  }

  vtkIdType
  TCoordHolderBase
  ::GetNbPoints() const
  {
    return myNbPoints; 
  }

  vtkIdType
  TCoordHolderBase
  ::GetDim() const
  {
    return myDim; 
  }

  size_t
  TCoordHolderBase
  ::size() const
  {
    return GetNbPoints() * GetDim(); 
  }

  unsigned long int
  TCoordHolderBase
  ::GetMemorySize()
  {
    return sizeof(TCoord) * size();
  }


  //---------------------------------------------------------------
  TPointCoords
  ::TPointCoords():
    myPointSet(vtkUnstructuredGrid::New())
  {
    vtkPoints* aPoints = vtkPoints::New();
    myPointSet->SetPoints(aPoints);
    aPoints->SetDataType(VTK_DOUBLE);
    aPoints->Delete();

    myPointSet->Delete();
  }

  void
  TPointCoords
  ::Init(const PCoordHolder& theCoord)
  {
    myPointSet->GetPoints()->SetNumberOfPoints(theCoord->GetNbPoints());
    myCoord = theCoord;
  }

  vtkIdType
  TPointCoords
  ::GetNbPoints() const
  {
    return myCoord->GetNbPoints(); 
  }

  vtkIdType
  TPointCoords
  ::GetDim() const
  {
    return myCoord->GetDim(); 
  }

  TCCoordSlice 
  TPointCoords
  ::GetCoordSlice(vtkIdType theNodeId) const
  {
    return myCoord->GetCoordSlice(theNodeId);
  }
  
  TCoordSlice 
  TPointCoords
  ::GetCoordSlice(vtkIdType theNodeId)
  {
    return myCoord->GetCoordSlice(theNodeId);
  }

  vtkIdType
  TPointCoords
  ::GetObjID(vtkIdType theID) const
  {
    return theID;
  }

  vtkIdType
  TPointCoords
  ::GetVTKID(vtkIdType theID) const
  {
    return theID;
  }

  void 
  TPointCoords
  ::SetVoidArray() const
  {
    vtkDataArray* aDataArray = myPointSet->GetPoints()->GetData();
    aDataArray->SetVoidArray(myCoord->GetValuePtr(), myCoord->size(), true);
  }

  vtkPointSet*
  TPointCoords
  ::GetPointSet() const
  { 
    if(!myIsVTKDone){
      TTimerLog aTimerLog(MYDEBUG,"TPointCoords::GetPoints()");
      vtkIdType aNbPoints = GetNbPoints();
      vtkIdType aDim = GetDim();

      INITMSG(MYDEBUG,"TPointCoords::GetPoints - aNbPoints = "<<aNbPoints<<
              "; aDim = "<<aDim<<
              endl);
      
      if(GetDim() == 3){
        INITMSG(MYDEBUG,"TPointCoords::GetPoints - SetVoidArray()"<<endl);
        SetVoidArray();
      }else{
        vtkPoints* aPoints = myPointSet->GetPoints();
        for(vtkIdType aPointId = 0; aPointId < aNbPoints; aPointId++){
          TCCoordSlice aSlice = GetCoordSlice(aPointId);
      
          vtkFloatingPointType aCoords[3] = {0.0, 0.0, 0.0};
          for(vtkIdType aDimId = 0; aDimId < aDim; aDimId++)
            aCoords[aDimId] = aSlice[aDimId];

          aPoints->SetPoint(aPointId, aCoords);
        }
      }

      myIsVTKDone = true;
    }
    
    return myPointSet.GetPointer();
  }

  unsigned long int
  TPointCoords
  ::GetMemorySize()
  {
    size_t aSize = myCoord->GetMemorySize();
    aSize += myPointSet->GetActualMemorySize() * 1024;
    return aSize;
  }


  //---------------------------------------------------------------
  void
  TNamedPointCoords
  ::Init(const PCoordHolder& theCoord)
  {
    TPointCoords::Init(theCoord);
    myPointsDim.resize(theCoord->GetDim());
  }

  std::string&
  TNamedPointCoords
  ::GetName(vtkIdType theDim)
  {
    return myPointsDim[theDim];
  }

  const std::string&
  TNamedPointCoords
  ::GetName(vtkIdType theDim) const
  {
    return myPointsDim[theDim];
  }

  std::string 
  TNamedPointCoords
  ::GetNodeName(vtkIdType theObjID) const
  {
    return "";
  }


  //---------------------------------------------------------------
  enum ECoordName{eX, eY, eZ, eNoneCoord};
  typedef VISU::TCoord (*TGetCoord)(const VISU::TCCoordSlice& theCoordSlice);
  
  template<ECoordName TCoordId>
  VISU::TCoord 
  GetCoord(const VISU::TCCoordSlice& theCoordSlice)
  {
    return theCoordSlice[TCoordId];
  }
  
  template<>
  VISU::TCoord 
  GetCoord<eNoneCoord>(const VISU::TCCoordSlice& theCoordSlice)
  {
    return 0.0;
  }
  
  
  TGetCoord aXYZGetCoord[3] = {
    &GetCoord<eX>, 
    &GetCoord<eY>, 
    &GetCoord<eZ>
  };
  
  
  TGetCoord aXYGetCoord[3] = {
    &GetCoord<eX>, 
    &GetCoord<eY>, 
    &GetCoord<eNoneCoord>
  };
  
  TGetCoord aYZGetCoord[3] = {
    &GetCoord<eNoneCoord>,
    &GetCoord<eX>, 
    &GetCoord<eY>
  };
  
  TGetCoord aXZGetCoord[3] = {
    &GetCoord<eX>, 
    &GetCoord<eNoneCoord>,
    &GetCoord<eY>
  };
  
  
  TGetCoord aXGetCoord[3] = {
    &GetCoord<eX>, 
    &GetCoord<eNoneCoord>,
    &GetCoord<eNoneCoord>
  };
  
  TGetCoord aYGetCoord[3] = {
    &GetCoord<eNoneCoord>,
    &GetCoord<eX>, 
    &GetCoord<eNoneCoord>
  };

  TGetCoord aZGetCoord[3] = {
    &GetCoord<eNoneCoord>,
    &GetCoord<eNoneCoord>,
    &GetCoord<eX>
  };

  
  class TCoordHelper{
    TGetCoord* myGetCoord;
  public:
    TCoordHelper(TGetCoord* theGetCoord):
      myGetCoord(theGetCoord)
    {}

    virtual
    ~TCoordHelper()
    {}

    VISU::TCoord 
    GetCoord(VISU::TCCoordSlice& theCoordSlice, 
             int theCoordId)
    {
      return (*myGetCoord[theCoordId])(theCoordSlice);
    }
  };
  typedef std::auto_ptr<TCoordHelper> TCoordHelperPtr;
  

  //---------------------------------------------------------------
  vtkPointSet*
  TNamedPointCoords
  ::GetPointSet() const
  { 
    if(!myIsVTKDone){
      TTimerLog aTimerLog(MYDEBUG,"TNamedPointCoords::GetPoints()");
      TCoordHelperPtr aCoordHelperPtr;
      bool anIsDimPresent[3] = {false, false, false};
      for(int iDim = 0; iDim < GetDim(); iDim++){
        // PAL16857(SMESH not conform to the MED convention) ->
        // 1D - always along X
        // 2D - always in XOY plane
        anIsDimPresent[iDim] = iDim < GetDim();
//      std::string aName = GetName(iDim);
//      if ( aName.size() > 1 ) // PAL13021 (PAL12148), aName has size 8 or 16
//        aName = aName.substr(0,1);
//      if(aName == "x" || aName == "X")
//        anIsDimPresent[eX] = true;
//      else if(aName == "y" || aName == "Y")
//        anIsDimPresent[eY] = true;
//      else if(aName == "z" || aName == "Z")
//        anIsDimPresent[eZ] = true;
      }
      
      switch(GetDim()){
      case 3:
        aCoordHelperPtr.reset(new TCoordHelper(aXYZGetCoord));
        break;
      case 2:
        if(anIsDimPresent[eY] && anIsDimPresent[eZ])
          aCoordHelperPtr.reset(new TCoordHelper(aYZGetCoord));
        else if(anIsDimPresent[eX] && anIsDimPresent[eZ])
          aCoordHelperPtr.reset(new TCoordHelper(aXZGetCoord));
        else
          aCoordHelperPtr.reset(new TCoordHelper(aXYGetCoord));
        break;
      case 1:
        if(anIsDimPresent[eY])
          aCoordHelperPtr.reset(new TCoordHelper(aYGetCoord));
        else if(anIsDimPresent[eZ])
          aCoordHelperPtr.reset(new TCoordHelper(aZGetCoord));
        else
          aCoordHelperPtr.reset(new TCoordHelper(aXGetCoord));
        break;
      }
      
      INITMSG(MYDEBUG,"TNamedPointCoords::GetPoints - aNbPoints = "<<GetNbPoints()<<
              "; aDim = "<<GetDim()<<
              endl);
      
      if(anIsDimPresent[eX] && anIsDimPresent[eY] && anIsDimPresent[eZ]){
        INITMSG(MYDEBUG,"TNamedPointCoords::GetPoints - SetVoidArray()"<<endl);
        SetVoidArray();
      }else{
        vtkPoints* aPoints = myPointSet->GetPoints();
        for(vtkIdType aNodeId = 0; aNodeId < GetNbPoints(); aNodeId++){ 
          TCCoordSlice aCoordSlice = GetCoordSlice(aNodeId);
          aPoints->SetPoint(aNodeId,
                            aCoordHelperPtr->GetCoord(aCoordSlice,eX),
                            aCoordHelperPtr->GetCoord(aCoordSlice,eY),
                            aCoordHelperPtr->GetCoord(aCoordSlice,eZ));
        }
      }
      
      {
        vtkIdType aNbTuples = GetNbPoints();
        int anEntity = int(VISU::NODE_ENTITY);
        vtkIntArray *aDataArray = vtkIntArray::New();
        aDataArray->SetName("VISU_POINTS_MAPPER");
        aDataArray->SetNumberOfComponents(2);
        aDataArray->SetNumberOfTuples(aNbTuples);
        int *aPtr = aDataArray->GetPointer(0);
        for(vtkIdType aTupleId = 0; aTupleId < aNbTuples; aTupleId++){
          vtkIdType anObjID = GetObjID(aTupleId);
          *aPtr++ = anObjID;
          *aPtr++ = anEntity;
        }
        myPointSet->GetPointData()->AddArray(aDataArray);
        aDataArray->Delete();
      }

      myIsVTKDone = true;
    }
    
    return myPointSet.GetPointer();
  }

  unsigned long int
  TNamedPointCoords
  ::GetMemorySize()
  {
    return TPointCoords::GetMemorySize();
  }


  //---------------------------------------------------------------
}
