// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
// File:    VISU_MapperHolder.cxx
// Author:  Alexey PETROV
// Module : VISU
//
#include "VISU_MapperHolder.hxx"
#include "VISU_PipeLine.hxx"

#include "VISU_PipeLineUtils.hxx"

#include <vtkDataSet.h>

#ifdef _DEBUG_
static int MYDEBUG = 0;
#else
static int MYDEBUG = 0;
#endif


//----------------------------------------------------------------------------
VISU_MapperHolder
::VISU_MapperHolder():
  myPipeLine(NULL)
{
  if(MYDEBUG) 
    MESSAGE("VISU_MapperHolder::VISU_MapperHolder - "<<this);
}


//----------------------------------------------------------------------------
VISU_MapperHolder
::~VISU_MapperHolder()
{
  if(MYDEBUG)
    MESSAGE("VISU_MapperHolder::~VISU_MapperHolder - "<<this);
}


//----------------------------------------------------------------------------
void 
VISU_MapperHolder
::ShallowCopy(VISU_MapperHolder *theMapperHolder,
              bool theIsCopyInput)
{
  if(theIsCopyInput)
    SetIDMapper(theMapperHolder->GetIDMapper());

  VISU::CopyMapper(GetMapper(), 
                   theMapperHolder->GetMapper(), 
                   theIsCopyInput);
}


//----------------------------------------------------------------------------
unsigned long int
VISU_MapperHolder
::GetMemorySize()
{
  unsigned long int aSize = 0;

  if(myMapper.GetPointer())
    if(vtkDataSet* aDataSet = myMapper->GetInput())
      aSize = aDataSet->GetActualMemorySize() * 1024;
  
  if(myIDMapper)
    aSize += myIDMapper->GetMemorySize();
  
  return aSize;
}


//----------------------------------------------------------------------------
unsigned long int 
VISU_MapperHolder
::GetMTime()
{
  unsigned long int aTime = Superclass::GetMTime();

  if(myIDMapper)
    if(vtkDataSet* aDataSet = myIDMapper->GetOutput())
      aTime = std::max(aTime, aDataSet->GetMTime());

  if(myMapper.GetPointer())
    aTime = std::max(aTime, myMapper->GetMTime());

  return aTime;
}


//----------------------------------------------------------------------------
void
VISU_MapperHolder
::SetPipeLine(VISU_PipeLine* thePipeLine)
{
  myPipeLine = thePipeLine;
}


//----------------------------------------------------------------------------
void
VISU_MapperHolder
::SetIDMapper(const VISU::PIDMapper& theIDMapper)
{
  if(myIDMapper == theIDMapper)
    return;

  myIDMapper = theIDMapper;

  if(myPipeLine && GetInput())
    if(!GetMapper()->GetInput()){
      myPipeLine->Build();
      myPipeLine->Init();
      myPipeLine->Update();
    }

  Modified();
}


//----------------------------------------------------------------------------
const VISU::PIDMapper&  
VISU_MapperHolder
::GetIDMapper()
{
  return myIDMapper;
}


//----------------------------------------------------------------------------
vtkDataSet* 
VISU_MapperHolder
::GetInput()
{
  if(myIDMapper)
    return myIDMapper->GetOutput();

  return NULL;
}


//----------------------------------------------------------------------------
vtkMapper* 
VISU_MapperHolder
::GetMapper()
{
  if(!myMapper.GetPointer())
    OnCreateMapper();

  return myMapper.GetPointer();
}


//----------------------------------------------------------------------------
vtkDataSet* 
VISU_MapperHolder
::GetOutput()
{
  if(myMapper.GetPointer())
    return myMapper->GetInput();

  return NULL;
}


//----------------------------------------------------------------------------
void 
VISU_MapperHolder
::Update()
{
  if(myMapper.GetPointer())
    return myMapper->Update();
}


//----------------------------------------------------------------------------
void
VISU_MapperHolder
::SetMapper(vtkMapper* theMapper)
{
  myMapper = theMapper;
}


//----------------------------------------------------------------------------
vtkIdType 
VISU_MapperHolder
::GetNodeObjID(vtkIdType theID)
{
  return myIDMapper->GetNodeObjID(theID);
}

//----------------------------------------------------------------------------
vtkIdType 
VISU_MapperHolder
::GetNodeVTKID(vtkIdType theID)
{
  return myIDMapper->GetNodeVTKID(theID);
}

//----------------------------------------------------------------------------
vtkFloatingPointType* 
VISU_MapperHolder
::GetNodeCoord(vtkIdType theObjID)
{
  return myIDMapper->GetNodeCoord(theObjID);
}


//----------------------------------------------------------------------------
vtkIdType 
VISU_MapperHolder
::GetElemObjID(vtkIdType theID)
{
  return myIDMapper->GetElemObjID(theID);
}

//----------------------------------------------------------------------------
vtkIdType
VISU_MapperHolder
::GetElemVTKID(vtkIdType theID)
{
  return myIDMapper->GetElemVTKID(theID);
}

//----------------------------------------------------------------------------
vtkCell* 
VISU_MapperHolder
::GetElemCell(vtkIdType  theObjID)
{
  return myIDMapper->GetElemCell(theObjID);
}


//----------------------------------------------------------------------------
