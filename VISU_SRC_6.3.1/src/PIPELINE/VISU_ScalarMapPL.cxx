// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File:    VISU_ScalarMapPL.cxx
//  Author:  Alexey PETROV
//  Module : VISU
//
#include "VISU_ScalarMapPL.hxx"
#include "VISU_DataSetMapperHolder.hxx"
#include "VISU_ElnoAssembleFilter.hxx"
#include "VISU_Extractor.hxx"
#include "VISU_FieldTransform.hxx"

#include "VISU_AppendFilter.hxx"
#include "VISU_MergeFilter.hxx"
#include "VISU_ConvertorUtils.hxx"

#include <vtkDataSet.h>
#include <vtkPointSet.h>
#include <vtkUnstructuredGrid.h>

#include <vtkDataSetMapper.h>
#include <vtkObjectFactory.h>


//----------------------------------------------------------------------------
vtkStandardNewMacro(VISU_ScalarMapPL);


//----------------------------------------------------------------------------
VISU_ScalarMapPL
::VISU_ScalarMapPL():
  VISU_UnstructuredGridPL(this),
  myElnoAssembleFilter( VISU_ElnoAssembleFilter::New() ),
  myAppendFilter(VISU_AppendFilter::New()),
  myMergeFilter(VISU_MergeFilter::New())
{
  SetIsShrinkable(true);
  SetIsFeatureEdgesAllowed(true);

  SetElnoDisassembleState( false );

  myElnoAssembleFilter->Delete();

  myAppendFilter->SetMergingInputs(true);
  myAppendFilter->Delete();

  myMergeFilter->SetMergingInputs(true);
  myMergeFilter->Delete();
}


//----------------------------------------------------------------------------
VISU_ScalarMapPL
::~VISU_ScalarMapPL()
{}


//----------------------------------------------------------------------------
unsigned long int
VISU_ScalarMapPL
::GetMTime()
{
  unsigned long int aTime = Superclass::GetMTime();

  aTime = std::max(aTime, myAppendFilter->GetMTime());
  aTime = std::max(aTime, myMergeFilter->GetMTime());

  return aTime;
}


//----------------------------------------------------------------------------
void
VISU_ScalarMapPL
::OnCreateMapperHolder()
{
  VISU_UnstructuredGridPL::OnCreateMapperHolder();
}


//----------------------------------------------------------------------------
void
VISU_ScalarMapPL
::Build()
{
  Superclass::Build();

  SetSourceGeometry();

  myMergeFilter->SetGeometry(myAppendFilter->GetOutput());

  vtkDataSet* aDataSet = GetClippedInput();
  myMergeFilter->SetScalars(aDataSet);
  myMergeFilter->SetVectors(aDataSet);

  myMergeFilter->RemoveFields();
  myMergeFilter->AddField("VISU_FIELD", aDataSet);
  myMergeFilter->AddField("VISU_FIELD_GAUSS_MIN", aDataSet);
  myMergeFilter->AddField("VISU_FIELD_GAUSS_MAX", aDataSet);
  myMergeFilter->AddField("VISU_FIELD_GAUSS_MOD", aDataSet);
  myMergeFilter->AddField("VISU_CELLS_MAPPER", aDataSet);
  myMergeFilter->AddField("VISU_POINTS_MAPPER", aDataSet);
  myMergeFilter->AddField("ELNO_POINT_COORDS", aDataSet);

  myElnoAssembleFilter->SetInput( InsertCustomPL() );

  GetDataSetMapper()->SetInput( myElnoAssembleFilter->GetOutput() );
}


//----------------------------------------------------------------------------
void
VISU_ScalarMapPL
::Update()
{
  Superclass::Update();
  //{
  //  std::string aFileName = std::string(getenv("HOME"))+"/"+getenv("USER")+"-aScalarsOutput.vtk";
  //  VISU::WriteToFile(GetFieldTransformFilter()->GetUnstructuredGridOutput(), aFileName);
  //}
  //{
  //  std::string aFileName = std::string(getenv("HOME"))+"/"+getenv("USER")+"-aGeomDataSet.vtk";
  //  VISU::WriteToFile(myAppendFilter->GetOutput(), aFileName);
  //}
  //{
  //  std::string aFileName = std::string(getenv("HOME"))+"/"+getenv("USER")+"-myMergeFilter.vtk";
  //  VISU::WriteToFile(myMergeFilter->GetUnstructuredGridOutput(), aFileName);
  //}
}


//----------------------------------------------------------------------------
void
VISU_ScalarMapPL
::DoShallowCopy(VISU_PipeLine *thePipeLine,
                bool theIsCopyInput)
{
  Superclass::DoShallowCopy(thePipeLine, theIsCopyInput);
  VISU_MergedPL::DoShallowCopy(thePipeLine, theIsCopyInput);

  if(VISU_ScalarMapPL* aPipeLine = dynamic_cast<VISU_ScalarMapPL*>(thePipeLine))
    SetGaussMetric(aPipeLine->GetGaussMetric());
}


//----------------------------------------------------------------------------
void
VISU_ScalarMapPL
::SetElnoDisassembleState( bool theIsShrunk )
{
  GetDataSetMapperHolder()->SetElnoDisassembleState( theIsShrunk );
  myElnoAssembleFilter->SetElnoAssembleState( theIsShrunk );
}


//----------------------------------------------------------------------------
void
VISU_ScalarMapPL
::SetSourceGeometry()
{
  if(IsExternalGeometryUsed()){
    ClearGeometry();
    myAppendFilter->AddInput(GetClippedInput());
  }
}


//----------------------------------------------------------------------------
int
VISU_ScalarMapPL
::AddGeometry(vtkDataSet* theGeometry, const VISU::TName& theGeomName)
{
  // rnv: to fix issue 0020167 (AddMeshOnGroup is not fully taken into account)
  // clear all inpust of the this->myAppendFilter in case if presentation
  // constructed on the entire mesh.
  if(!IsExternalGeometryUsed())
    ClearGeometry();
  AddGeometryName(theGeomName);
  myAppendFilter->AddInput(theGeometry);
  return GetNumberOfGeometry();
}


//----------------------------------------------------------------------------
vtkDataSet*
VISU_ScalarMapPL
::GetGeometry(int theGeomNumber, VISU::TName& theGeomName)
{
  theGeomName = GetGeometryName(theGeomNumber);
  return vtkDataSet::SafeDownCast(myAppendFilter->GetInput(theGeomNumber));
}


//----------------------------------------------------------------------------
int
VISU_ScalarMapPL
::GetNumberOfGeometry()
{
  return myAppendFilter->GetNumberOfInputConnections(0);
}


//----------------------------------------------------------------------------
bool
VISU_ScalarMapPL
::IsExternalGeometryUsed()
{
  return myAppendFilter->GetInput() != GetClippedInput();
}


//----------------------------------------------------------------------------
void
VISU_ScalarMapPL
::ClearGeometry()
{
  ClearGeometryNames();
  myAppendFilter->RemoveAllInputs();
}


//----------------------------------------------------------------------------
void
VISU_ScalarMapPL
::GetSourceRange(vtkFloatingPointType theRange[2])
{
  if(!IsExternalGeometryUsed())
    Superclass::GetSourceRange(theRange);
  else
    GetMergedInput()->GetScalarRange( theRange );
}


//----------------------------------------------------------------------------
void
VISU_ScalarMapPL
::SetGaussMetric(VISU::TGaussMetric theGaussMetric)
{
  if(GetGaussMetric() == theGaussMetric)
    return;

  GetExtractorFilter()->SetGaussMetric(theGaussMetric);
}


//----------------------------------------------------------------------------
VISU::TGaussMetric
VISU_ScalarMapPL
::GetGaussMetric()
{
  return GetExtractorFilter()->GetGaussMetric();
}


//----------------------------------------------------------------------------
vtkDataSet*
VISU_ScalarMapPL
::InsertCustomPL()
{
  return GetMergedInput();
}


//----------------------------------------------------------------------------
vtkPointSet*
VISU_ScalarMapPL
::GetMergedInput()
{
  if(myMergeFilter->GetInput())
    myMergeFilter->Update();
  return myMergeFilter->GetOutput();
}


//----------------------------------------------------------------------------
