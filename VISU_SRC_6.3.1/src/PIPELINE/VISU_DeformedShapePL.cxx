// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
// File:    VISU_PipeLine.cxx
// Author:  Alexey PETROV
// Module : VISU
//
#include "VISU_DeformedShapePL.hxx"
#include "VISU_PipeLineUtils.hxx"
#include "VTKViewer_Transform.h"

#include <vtkWarpVector.h>


//----------------------------------------------------------------------------
vtkStandardNewMacro(VISU_DeformedShapePL);


//----------------------------------------------------------------------------
VISU_DeformedShapePL
::VISU_DeformedShapePL():
  myScaleFactor(0.0),
  myMapScaleFactor(1.0)
{
  SetIsShrinkable(true);
  SetIsFeatureEdgesAllowed(true);

  myWarpVector = vtkWarpVector::New();
  myCellDataToPointData = VISU_CellDataToPointData::New();
}


//----------------------------------------------------------------------------
VISU_DeformedShapePL
::~VISU_DeformedShapePL()
{
  myWarpVector->Delete();

  myCellDataToPointData->Delete();
}


//----------------------------------------------------------------------------
unsigned long int 
VISU_DeformedShapePL
::GetMTime()
{
  unsigned long int aTime = Superclass::GetMTime();

  aTime = std::max(aTime, myWarpVector->GetMTime());
  aTime = std::max(aTime, myCellDataToPointData->GetMTime());

  return aTime;
}


//----------------------------------------------------------------------------
void
VISU_DeformedShapePL
::DoShallowCopy(VISU_PipeLine *thePipeLine,
                bool theIsCopyInput)
{
  Superclass::DoShallowCopy(thePipeLine, theIsCopyInput);

  if(VISU_DeformedShapePL *aPipeLine = dynamic_cast<VISU_DeformedShapePL*>(thePipeLine)){
    SetScale(aPipeLine->GetScale());
  }
}


//----------------------------------------------------------------------------
vtkFloatingPointType
VISU_DeformedShapePL
::GetScaleFactor(vtkDataSet* theDataSet)
{
  if(!theDataSet)
    return 0.0;

  theDataSet->Update();

  int aNbCells = theDataSet->GetNumberOfCells();
  int aNbPoints = theDataSet->GetNumberOfPoints();
  int aNbElem = aNbCells? aNbCells: aNbPoints;

  vtkFloatingPointType* aBounds = theDataSet->GetBounds();
  vtkFloatingPointType aVolume = 1, aVol, idim = 0;
  for(int i = 0; i < 6; i += 2){
    aVol = fabs(aBounds[i+1] - aBounds[i]);
    if(aVol > 0) {
      idim++;
      aVolume *= aVol;
    }
  }
  if( aNbElem == 0 || fabs(idim) < 1.0 / VTK_LARGE_FLOAT )
    return 0.0; // to avoid division by zero
  aVolume /= aNbElem;
  return pow(aVolume, vtkFloatingPointType(1.0/idim));
}


//----------------------------------------------------------------------------
vtkFloatingPointType
VISU_DeformedShapePL
::GetDefaultScale(VISU_ScalarMapPL* theScalarMapPL)
{
  vtkFloatingPointType aSourceRange[2];
  theScalarMapPL->GetSourceRange(aSourceRange);
  
  static vtkFloatingPointType EPS = 1.0 / VTK_LARGE_FLOAT;
  if(fabs(aSourceRange[1]) > EPS){
    vtkDataSet* aDataSet = theScalarMapPL->GetMergedInput();
    vtkFloatingPointType aScaleFactor = VISU_DeformedShapePL::GetScaleFactor(aDataSet);
    return aScaleFactor / aSourceRange[1];
  }
  return 0.0;
}


//----------------------------------------------------------------------------
void
VISU_DeformedShapePL
::SetScale(vtkFloatingPointType theScale) 
{
  if(VISU::CheckIsSameValue(myWarpVector->GetScaleFactor(), theScale))
    return;
  
  myWarpVector->SetScaleFactor(theScale*myMapScaleFactor);
  myScaleFactor = theScale;
}


//----------------------------------------------------------------------------
vtkFloatingPointType
VISU_DeformedShapePL
::GetScale() 
{
  return myScaleFactor;
}


//----------------------------------------------------------------------------
void
VISU_DeformedShapePL
::Init()
{
  Superclass::Init();

  SetScale(VISU_DeformedShapePL::GetDefaultScale(this));
}


//----------------------------------------------------------------------------
void
VISU_DeformedShapePL
::Update()
{
  Superclass::Update();
  //{
  //  std::string aFileName = std::string(getenv("HOME"))+"/"+getenv("USER")+"-myWarpVector.vtk";
  //  VISU::WriteToFile(myWarpVector->GetUnstructuredGridOutput(), aFileName);
  //}
}


//----------------------------------------------------------------------------
vtkDataSet* 
VISU_DeformedShapePL
::InsertCustomPL()
{
  VISU::CellDataToPoint(myWarpVector,
                        myCellDataToPointData,
                        GetMergedInput());

  return myWarpVector->GetOutput();
}


//----------------------------------------------------------------------------
unsigned long int
VISU_DeformedShapePL
::GetMemorySize()
{
  unsigned long int aSize = Superclass::GetMemorySize();

  if(myWarpVector->GetInput())
    if(vtkDataSet* aDataSet = myWarpVector->GetOutput())
      aSize += aDataSet->GetActualMemorySize() * 1024;
  
  if(myCellDataToPointData->GetInput())
    if(vtkDataSet* aDataSet = myCellDataToPointData->GetOutput())
      aSize += aDataSet->GetActualMemorySize() * 1024;

  return aSize;
}


//----------------------------------------------------------------------------
void
VISU_DeformedShapePL
::SetMapScale(vtkFloatingPointType theMapScale)
{
  myMapScaleFactor = theMapScale;
  Superclass::SetMapScale(theMapScale);

  vtkFloatingPointType aMapScale = myScaleFactor * theMapScale;
  if(VISU::CheckIsSameValue(myWarpVector->GetScaleFactor(), aMapScale))
    return;

  myWarpVector->SetScaleFactor( aMapScale );
}


//----------------------------------------------------------------------------
