// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
// File:    VISU_MergedPL.hxx
// Author:  Alexey PETROV
// Module : VISU
//
#ifndef VISU_MergedPL_HeaderFile
#define VISU_MergedPL_HeaderFile

#include "VISUPipeline.hxx"
#include "VISU_Structures.hxx"

class vtkDataSet;
class vtkPointSet;
class VISU_PipeLine;
 

//----------------------------------------------------------------------------
struct VISU_PIPELINE_EXPORT VISU_MergedPL
{
  virtual  
  void  
  SetSourceGeometry() = 0;

  virtual
  int
  AddGeometry( vtkDataSet* theGeometry, const VISU::TName& theGeomName ) = 0;

  virtual
  vtkDataSet*
  GetGeometry( int theGeomNumber, VISU::TName& theGeomName ) = 0;

  virtual
  int
  GetNumberOfGeometry() = 0;

  virtual
  bool 
  IsExternalGeometryUsed() = 0;

  virtual
  void
  ClearGeometry() = 0;

  virtual 
  vtkPointSet* 
  GetMergedInput() = 0;

  virtual
  void
  DoShallowCopy(VISU_PipeLine *thePipeLine,
                bool theIsCopyInput);

  virtual
  void
  AddGeometryName(const VISU::TName& theGeomName);

  virtual
  VISU::TName
  GetGeometryName( int theGeomNumber ) const;

  virtual
  const VISU::TNames&
  GetGeometryNames() const;

  virtual
  void
  ClearGeometryNames();

  VISU::TNames myGeometryNames;
};
  
#endif
