// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
// File:    VISU_StreamLinesPL.hxx
// Author:  Alexey PETROV
// Module : VISU
//
#ifndef VISU_StreamLinesPL_HeaderFile
#define VISU_StreamLinesPL_HeaderFile

#include "VISUPipeline.hxx"
#include "VISU_DeformedShapePL.hxx"

#include <vtkStreamer.h>

class vtkDataSet;
class vtkPointSet;
class VTKViewer_CellCenters;
class VTKViewer_GeometryFilter;
class VISU_MaskPointsFilter;
class vtkStreamLine;


//----------------------------------------------------------------------------
class VISU_PIPELINE_EXPORT VISU_StreamLinesPL : public VISU_DeformedShapePL
{
public:
  vtkTypeMacro(VISU_StreamLinesPL, VISU_DeformedShapePL);

  static
  VISU_StreamLinesPL*
  New();

  virtual
  unsigned long int 
  GetMTime();

  //----------------------------------------------------------------------------
  virtual
  size_t
  SetParams(vtkFloatingPointType theIntStep,
            vtkFloatingPointType thePropogationTime,
            vtkFloatingPointType theStepLength,
            vtkPointSet* theSource,
            vtkFloatingPointType thePercents,
            int theDirection = VTK_INTEGRATE_BOTH_DIRECTIONS);

  virtual
  vtkPointSet*
  GetSource();

  virtual
  vtkFloatingPointType
  GetUsedPoints();

  virtual
  vtkFloatingPointType 
  GetIntegrationStep();

  virtual
  vtkFloatingPointType
  GetPropagationTime();

  virtual
  vtkFloatingPointType
  GetStepLength();

  virtual
  int
  GetDirection();

  virtual
  vtkDataSet* 
  GetStreamerSource();

  virtual
  vtkFloatingPointType 
  GetVelocityCoeff();

  virtual
  vtkFloatingPointType
  GetMaxIntegrationStep();

  virtual
  vtkFloatingPointType
  GetMinIntegrationStep();

  virtual
  vtkFloatingPointType
  GetMinStepLength();

  virtual
  vtkFloatingPointType
  GetMaxStepLength();

  virtual
  vtkFloatingPointType
  GetMinPropagationTime();

  virtual
  vtkFloatingPointType
  GetMaxPropagationTime();

  virtual
  vtkFloatingPointType
  GetBasePropagationTime();

public:
  virtual
  vtkDataSet* 
  InsertCustomPL();

  virtual
  void
  Init();

  virtual
  void
  Build();

  virtual
  void
  Update();

  //! Gets memory size used by the instance (bytes).
  virtual
  unsigned long int
  GetMemorySize();

  virtual
  void
  SetMapScale(vtkFloatingPointType theMapScale = 1.0);

public:
  static
  vtkFloatingPointType
  GetMaxIntegrationStep(vtkDataSet* theDataSet);

  static
  vtkFloatingPointType
  GetMinIntegrationStep(vtkDataSet* theDataSet, 
                        vtkFloatingPointType thePercents);
  static
  vtkFloatingPointType
  GetBaseIntegrationStep(vtkDataSet* theDataSet, 
                         vtkFloatingPointType thePercents);
  
  static 
  vtkFloatingPointType
  GetMinPropagationTime(vtkDataSet* theDataSet, 
                        vtkFloatingPointType thePercents);

  static
  vtkFloatingPointType
  GetMaxPropagationTime(vtkDataSet* theDataSet);

  static
  vtkFloatingPointType
  GetBasePropagationTime(vtkDataSet* theDataSet);

  static
  vtkFloatingPointType
  GetMinStepLength(vtkDataSet* theDataSet, 
                   vtkFloatingPointType thePercents);

  static
  vtkFloatingPointType
  GetMaxStepLength(vtkDataSet* theDataSet);

  static
  vtkFloatingPointType
  GetBaseStepLength(vtkDataSet* theDataSet, 
                    vtkFloatingPointType thePercents);

  static
  vtkFloatingPointType
  GetVelocityCoeff(vtkDataSet* theDataSet);

  static
  size_t
  IsPossible(vtkPointSet* theDataSet);

protected:
  VISU_StreamLinesPL();

  virtual
  ~VISU_StreamLinesPL();

  virtual
  void
  DoShallowCopy(VISU_PipeLine *thePipeLine,
                bool theIsCopyInput);

  static 
  vtkFloatingPointType
  GetNecasseryMemorySize(vtkIdType theNbOfPoints, 
                         vtkFloatingPointType theStepLength, 
                         vtkFloatingPointType thePropogationTime, 
                         vtkFloatingPointType thePercents);

  static
  size_t
  FindPossibleParams(vtkDataSet* theDataSet, 
                     vtkFloatingPointType& theStepLength, 
                     vtkFloatingPointType& thePropogationTime, 
                     vtkFloatingPointType& thePercents);
  
  static 
  vtkFloatingPointType
  CorrectIntegrationStep(vtkFloatingPointType theStep, 
                         vtkDataSet* theDataSet, 
                         vtkFloatingPointType thePercents);

  static 
  vtkFloatingPointType
  CorrectPropagationTime(vtkFloatingPointType thePropagationTime, 
                         vtkDataSet* theDataSet, 
                         vtkFloatingPointType thePercents);

  static
  vtkFloatingPointType
  CorrectStepLength(vtkFloatingPointType theStep, 
                    vtkDataSet* theDataSet, 
                    vtkFloatingPointType thePercents);

  static
  vtkFloatingPointType
  GetUsedPointsDefault();

  vtkStreamLine* myStream;
  vtkPointSet* mySource;
  VTKViewer_CellCenters* myCenters;
  VTKViewer_GeometryFilter *myGeomFilter;
  VISU_MaskPointsFilter *myPointsFilter;
  vtkFloatingPointType myPercents;

private:
  VISU_StreamLinesPL(const VISU_StreamLinesPL&);  // Not implemented.
  void operator=(const VISU_StreamLinesPL&);  // Not implemented.
};


#endif
 
