// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
// File:    VISU_StreamLinesPL.cxx
// Author:  Alexey PETROV
// Module : VISU
//
#include "VISU_MaskPointsFilter.hxx"

#include <vtkObjectFactory.h>
#include <vtkPointSet.h>
#include <vtkPointData.h>
#include <vtkCellData.h>
#include <vtkPoints.h>
#include <vtkIdList.h>

vtkStandardNewMacro(VISU_MaskPointsFilter);

VISU_MaskPointsFilter::VISU_MaskPointsFilter(){
  PercentsOfUsedPoints = 1.0;
}

VISU_MaskPointsFilter::~VISU_MaskPointsFilter(){}

void VISU_MaskPointsFilter::Execute(){
  vtkPointSet *anInput = this->GetInput(), *anOutput = this->GetOutput();
  anOutput->GetPointData()->CopyAllOff();
  anOutput->GetCellData()->CopyAllOff();
  anOutput->CopyStructure(anInput);

  vtkPoints* aPoints = vtkPoints::New();
  vtkIdList *anIdList = vtkIdList::New();
  vtkIdType iEnd = anInput->GetNumberOfPoints();
  for(vtkIdType i = 0; i < iEnd; i++){
    anInput->GetPointCells(i,anIdList);
    if(anIdList->GetNumberOfIds() > 0)
      aPoints->InsertNextPoint(anInput->GetPoint(i));
  }
  vtkPoints* aNewPoints = vtkPoints::New();
  iEnd = aPoints->GetNumberOfPoints();
  if (PercentsOfUsedPoints > 0){
    vtkIdType anOffset = vtkIdType(1.0/PercentsOfUsedPoints);
    if(anOffset < 1) anOffset = 1;
    for(vtkIdType i = 0; i < iEnd; i += anOffset)
      aNewPoints->InsertNextPoint(aPoints->GetPoint(i));
  }
  anOutput->SetPoints(aNewPoints);
  aNewPoints->Delete();
  aPoints->Delete();
}
