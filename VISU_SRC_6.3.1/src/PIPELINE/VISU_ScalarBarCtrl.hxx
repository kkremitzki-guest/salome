// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
// File:    VISU_ScalarBarCtrl.hxx
// Author:  Peter KURNEV
// Module : VISU
//
#ifndef VISU_ScalarBarCtrl_HeaderFile
#define VISU_ScalarBarCtrl_HeaderFile

#include "VISUPipeline.hxx"

#include <vtkObject.h>

#include "VTKViewer.h"

class vtkRenderer;
class VISU_LookupTable;
class VISU_ScalarBarActor;

class VISU_PIPELINE_EXPORT VISU_ScalarBarCtrl :  public vtkObject
{
public:
  vtkTypeMacro(VISU_ScalarBarCtrl, vtkObject);
  static
  VISU_ScalarBarCtrl* 
  New();

  enum EMode {eSimple, eGlobal, eLocal};

  // Mode
  void
  SetMode(EMode theMode);

  EMode 
  GetMode() const;

  // Ranges
  void
  SetRangeGlobal(vtkFloatingPointType theMin,
                 vtkFloatingPointType theMax);
  void 
  SetRangeGlobal(vtkFloatingPointType *theRange);

  void
  SetGlobalRangeIsDefined(bool theIsDefined);
  
  void
  SetRangeLocal(vtkFloatingPointType theMin,
                vtkFloatingPointType theMax);
  void 
  SetRangeLocal(vtkFloatingPointType *theRange);
  
  // Selectors
  VISU_ScalarBarActor* 
  GetLocalBar();

  VISU_LookupTable* 
  GetLocalTable();

  VISU_ScalarBarActor* 
  GetGlobalBar();

  VISU_LookupTable* 
  GetGlobalTable();
  //
  // Renderer
  void
  AddToRender(vtkRenderer* theRenderer); 

  void
  RemoveFromRender(vtkRenderer* theRenderer);
  //
  // Visibility
  void
  SetVisibility(int theFlag);

  int
  GetVisibility() const;

  // Visibility
  void
  SetCtrlVisibility(int theFlag);

  int
  GetCtrlVisibility() const;

  // Build
  void Update();

  // Position
  void
  SetWidth(const vtkFloatingPointType theWidth);

  vtkFloatingPointType
  GetWidth() const;

  void  
  SetHeight(const vtkFloatingPointType theHeight);

  vtkFloatingPointType
  GetHeight() const;

  void
  SetPosition(const vtkFloatingPointType* thePosition);
  
  const vtkFloatingPointType* 
  GetPosition() const;
  //
  // Spacing
  void
  SetSpacing(const vtkFloatingPointType theSpacing);

  vtkFloatingPointType
  GetSpacing() const;
  //
  // Rainbow/bicolor
  bool
  GetBicolor() const;

  void
  SetBicolor(const bool theBicolor);
  //
  // Misc
  void  
  SetMarkValue(const vtkFloatingPointType theValue);

  vtkFloatingPointType
  GetMarkValue() const;

  void
  SetIsMarked(const bool theFlag);

  bool
  GetIsMarked()const;
  //
protected:
  VISU_ScalarBarCtrl();

  virtual
  ~VISU_ScalarBarCtrl();
  
  void
  UpdateForBicolor();

  void
  UpdateForColor();

  void
  UpdateMarkValue();

  void
  PrepareTables();
  
  void
  PrepareTables(VISU_ScalarBarActor* theScalarBarActor,
                VISU_LookupTable *theLookupTable,
                vtkIdType theId);
  
protected:
  EMode myMode;
  bool myGlobalRangeIsDefined;

  vtkFloatingPointType myDistance;
  vtkFloatingPointType myPosition[2];
  bool  myBicolor;
  int   myCtrlVisibility;

  unsigned char myBlack[3];
  unsigned char myGrey[3];
  //
  bool  myMarked;
  vtkFloatingPointType myMarkedValue;
  //
  VISU_ScalarBarActor *myGlobalScalarBar;
  VISU_LookupTable *myGlobalLookupTable; 

  VISU_ScalarBarActor *myLocalScalarBar;
  VISU_LookupTable *myLocalLookupTable; 
};

#endif
