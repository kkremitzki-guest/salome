// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
// File:    VISU_DeformedShapeAndScalarMapPL.hxx
// Author:  Eugeny Nikolaev
// Module : VISU
//
#ifndef VISU_DeformedShapeAndScalarMapPL_HeaderFile
#define VISU_DeformedShapeAndScalarMapPL_HeaderFile

#include "VISUPipeline.hxx"
#include "VISU_ScalarMapPL.hxx"

class VISU_MergeFilter;
class vtkWarpVector;
class vtkUnstructuredGrid;
class VISU_CellDataToPointData;
class vtkPointDataToCellData;
class VISU_ElnoDisassembleFilter;
class SALOME_ExtractGeometry;
class vtkImplicitFunction;


//----------------------------------------------------------------------------
class VISU_PIPELINE_EXPORT VISU_DeformedShapeAndScalarMapPL : public VISU_ScalarMapPL
{
public:
  vtkTypeMacro(VISU_DeformedShapeAndScalarMapPL, VISU_ScalarMapPL);

  static 
  VISU_DeformedShapeAndScalarMapPL* 
  New();

  virtual
  void
  SetScale(vtkFloatingPointType theScale);

  virtual
  vtkFloatingPointType
  GetScale();

  virtual
  int
  GetScalarMode();

  virtual
  void
  SetScalarMode(int theScalarMode = 0);

  virtual
  void
  SetScaling(int theScaling);
  
  virtual
  void
  SetScalarRange(vtkFloatingPointType theRange[2]);

  virtual
  vtkFloatingPointType* 
  GetScalarRange();

  virtual
  void
  GetSourceRange(vtkFloatingPointType theRange[2]);

  virtual
  void
  SetGaussMetric(VISU::TGaussMetric theGaussMetric);
  
  virtual
  VISU::TGaussMetric
  GetGaussMetric();

  virtual
  void
  SetScalars(vtkDataSet *theScalars);

  virtual
  vtkDataSet* 
  GetScalars();

  virtual
  void 
  RemoveAllClippingPlanes();

  virtual
  void
  RemoveClippingPlane(vtkIdType theID);

  virtual
  bool
  AddClippingPlane(vtkPlane* thePlane);

  virtual
  void
  SetImplicitFunction(vtkImplicitFunction *theFunction);

  virtual
  vtkImplicitFunction* 
  GetImplicitFunction();

public:
  //! Redefined method for initialization of the pipeline.
  virtual
  void
  Init();

  //! Redefined method for building the pipeline.
  virtual
  void
  Build();

  //! Redefined method for updating the pipeline.
  virtual
  void
  Update();

  //! Gets memory size used by the instance (bytes).
  virtual
  unsigned long int
  GetMemorySize();

  //! Update scalars.
  void
  UpdateScalars();
  
  virtual
  void
  SetMapScale(vtkFloatingPointType theMapScale = 1.0);
  
protected:
  VISU_DeformedShapeAndScalarMapPL();
  
  virtual
  ~VISU_DeformedShapeAndScalarMapPL();
  
  virtual
  vtkDataSet* 
  InsertCustomPL();

  virtual
  void
  DoShallowCopy(VISU_PipeLine *thePipeLine,
                bool theIsCopyInput);

private:
  VISU_DeformedShapeAndScalarMapPL(const VISU_DeformedShapeAndScalarMapPL&);

  vtkFloatingPointType myScaleFactor;
  vtkFloatingPointType myMapScaleFactor;
  vtkWarpVector  *myWarpVector;
  VISU_MergeFilter *myScalarsMergeFilter;
  vtkSmartPointer<vtkUnstructuredGrid> myScalars;
  VISU_CellDataToPointData* myCellDataToPointData;
  VISU_FieldTransform* myScalarsFieldTransform;
  VISU_Extractor* myScalarsExtractor;
  VISU_ElnoDisassembleFilter* myScalarsElnoDisassembleFilter;
  SALOME_ExtractGeometry* myExtractGeometry;
};

#endif
