// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
// File:    VISU_PipeLine.cxx
// Author:  Alexey PETROV
// Module : VISU
//
#include "VISU_CutPlanesPL.hxx"
#include "VISU_FieldTransform.hxx"
#include "VISU_PipeLineUtils.hxx"
#include "VTKViewer_GeometryFilter.h"
#include "VISU_MapperHolder.hxx"
#include "VISU_DeformationPL.hxx"

#include <vtkAppendPolyData.h>
#include <vtkCutter.h>
#include <vtkPlane.h>

//#include <vtkUnstructuredGrid.h>

static vtkFloatingPointType EPS = 1.0E-3;

#ifdef _DEBUG_
static int MYDEBUG = 0;
#else
static int MYDEBUG = 0;
#endif


//----------------------------------------------------------------------------
vtkStandardNewMacro(VISU_CutPlanesPL);


//----------------------------------------------------------------------------
VISU_CutPlanesPL
::VISU_CutPlanesPL():
  VISU_OptionalDeformationPL()
{
  if(MYDEBUG) MESSAGE("VISU_CutPlanesPL()::VISU_CutPlanesPL() - "<<this);
  
  SetIsShrinkable(false);
  SetIsFeatureEdgesAllowed(false);

  SetElnoDisassembleState( true );

  myAppendPolyData = vtkAppendPolyData::New();

  myNbParts = 10;

  myBasePlane[0] = YZ;
  myBasePlane[0] = XY;

  myDisplacement[0] = myDisplacement[1] = 0.5;

  myAng[0][0] = myAng[0][1] = myAng[0][2] = 0.0;
  myAng[1][0] = myAng[1][1] = myAng[1][2] = 0.0;
  UseDeformation(false);
}


//----------------------------------------------------------------------------
VISU_CutPlanesPL
::~VISU_CutPlanesPL()
{
  if(MYDEBUG) MESSAGE("VISU_CutPlanesPL()::~VISU_CutPlanesPL() - "<<this);
  myAppendPolyData->Delete();
  myAppendPolyData = NULL;
}


//----------------------------------------------------------------------------
unsigned long int 
VISU_CutPlanesPL
::GetMTime()
{
  unsigned long int aTime = Superclass::GetMTime();
  
  if(IsDeformed()) {
    aTime = std::max(aTime, VISU_OptionalDeformationPL::GetMTime());
  }
  
  aTime = std::max(aTime, myAppendPolyData->GetMTime());

  return aTime;
}


//----------------------------------------------------------------------------
void
VISU_CutPlanesPL
::DoShallowCopy(VISU_PipeLine *thePipeLine,
                bool theIsCopyInput)
{
  Superclass::DoShallowCopy(thePipeLine, theIsCopyInput);

  if(VISU_CutPlanesPL *aPipeLine = dynamic_cast<VISU_CutPlanesPL*>(thePipeLine)){

    SetOrientation(aPipeLine->GetPlaneOrientation(),
                   aPipeLine->GetRotateX(),
                   aPipeLine->GetRotateY());

    SetDisplacement(aPipeLine->GetDisplacement());

    SetNbParts(aPipeLine->GetNbParts());
    for (int i = 0, iEnd = GetNbParts(); i < iEnd; i++)
      if(!aPipeLine->IsPartDefault(i))  
        SetPartPosition(i, aPipeLine->GetPartPosition(i));
  }
}


//----------------------------------------------------------------------------
void
VISU_CutPlanesPL
::Init()
{
  Superclass::Init();
  SetNbParts(10);
  myBasePlane[0] = YZ;
  myDisplacement[0] = 0.5;
  myAng[0][0] = myAng[0][1] = myAng[0][2] = 0.0;
  SetScale(VISU_DeformationPL::GetDefaultScaleFactor(this));
}


//----------------------------------------------------------------------------
vtkDataSet* 
VISU_CutPlanesPL
::InsertCustomPL()
{
  return GetWarpVectorOutput();
}


//----------------------------------------------------------------------------
void
VISU_CutPlanesPL
::Update()
{
  vtkDataSet* aMergedInput = GetMergedInput();
  if(VISU::IsQuadraticData(aMergedInput)) // Bug 0020123, note 0005343
    throw std::runtime_error("Impossible to build presentation");

  ClearAppendPolyData(myAppendPolyData);


  if(!myVectorialField || !IsDeformed()){
    SetMergeFilterInput(aMergedInput,aMergedInput);
  }
  

  if(VISU::IsDataOnCells(aMergedInput))
    GetMapper()->SetScalarModeToUseCellData();
  else
    GetMapper()->SetScalarModeToUsePointData();

  SetPartPosition();
  
  vtkFloatingPointType aDir[3];
  GetDir(aDir, 
         myAng[0], 
         myBasePlane[0]);
  
  vtkFloatingPointType aBounds[6];

  vtkDataSet* aFilterOutput = GetMergeFilterOutput();
  
  aFilterOutput->GetBounds(aBounds);

  CutWithPlanes(myAppendPolyData,
                aFilterOutput,
                myNbParts,
                aDir,
                aBounds,
                myPartPosition, 
                myPartCondition, 
                myDisplacement[0]);

  

  SetWarpVectorInput(myAppendPolyData->GetOutput());
  Superclass::Update();
}


//----------------------------------------------------------------------------
unsigned long int
VISU_CutPlanesPL
::GetMemorySize()
{
  unsigned long int aSize = Superclass::GetMemorySize();

  if(vtkDataSet* aDataSet = myAppendPolyData->GetOutput())
    aSize += aDataSet->GetActualMemorySize() * 1024;
  
  int anEnd = myAppendPolyData->GetNumberOfInputConnections(0);
  for(int anId = 0; anId < anEnd; anId++)
    if(vtkDataSet* aDataSet = myAppendPolyData->GetInput(anId))
      aSize += aDataSet->GetActualMemorySize() * 1024;

  return aSize;
}


//----------------------------------------------------------------------------
void
VISU_CutPlanesPL
::SetPartPosition(int theNum)
{
  for(int i = 0; i < myNbParts; i++)
    myPartPosition[i] = GetPartPosition(i,theNum);
}

void
VISU_CutPlanesPL
::ClearAppendPolyData(vtkAppendPolyData *theAppendPolyData)
{
  theAppendPolyData->RemoveAllInputs();
}


//----------------------------------------------------------------------------
vtkFloatingPointType* 
VISU_CutPlanesPL::
GetRx(vtkFloatingPointType theRx[3][3], 
      vtkFloatingPointType thaAng)
{
  theRx[0][0] = 1.0;            theRx[0][1] = 0.0;            theRx[0][2] = 0.0;
  theRx[1][0] = 0.0;            theRx[1][1] = cos(thaAng);    theRx[1][2] = -sin(thaAng);
  theRx[2][0] = 0.0;            theRx[2][1] = sin(thaAng);    theRx[2][2] = cos(thaAng);
  return theRx[0];
}



//----------------------------------------------------------------------------
vtkFloatingPointType* 
VISU_CutPlanesPL
::GetRy(vtkFloatingPointType theRy[3][3], 
        vtkFloatingPointType thaAng)
{
  theRy[0][0] = cos(thaAng);    theRy[0][1] = 0.0;            theRy[0][2] = sin(thaAng);
  theRy[1][0] = 0.0;            theRy[1][1] = 1.0;            theRy[1][2] = 0.0;
  theRy[2][0] = -sin(thaAng);   theRy[2][1] = 0.0;            theRy[2][2] = cos(thaAng);
  return theRy[0];
}


//----------------------------------------------------------------------------
vtkFloatingPointType* 
VISU_CutPlanesPL
::GetRz(vtkFloatingPointType theRz[3][3], 
        vtkFloatingPointType thaAng)
{
  theRz[0][0] = cos(thaAng);    theRz[0][1] = -sin(thaAng);   theRz[0][2] = 0.0;
  theRz[1][0] = sin(thaAng);    theRz[1][1] = cos(thaAng);    theRz[1][2] = 0.0;
  theRz[2][0] = 0.0;            theRz[2][1] = 0.0;            theRz[2][2] = 1.0;
  return theRz[0];
}


//----------------------------------------------------------------------------
void
VISU_CutPlanesPL
::CorrectPnt(vtkFloatingPointType thePnt[3], 
             const vtkFloatingPointType BoundPrj[6])
{
  for(int i = 0, j = 0; i < 3; ++i, j=2*i){
    if(thePnt[i] < BoundPrj[j]) thePnt[i] = BoundPrj[j];
    if(thePnt[i] > BoundPrj[j+1]) thePnt[i] = BoundPrj[j+1];
  }
}


//----------------------------------------------------------------------------
void
VISU_CutPlanesPL
::GetBoundProject(vtkFloatingPointType BoundPrj[3], 
                  const vtkFloatingPointType BoundBox[6], 
                  const vtkFloatingPointType Dir[3])
{
  vtkFloatingPointType BoundPoints[8][3] = { {BoundBox[0],BoundBox[2],BoundBox[4]},
                                             {BoundBox[1],BoundBox[2],BoundBox[4]},
                                             {BoundBox[0],BoundBox[3],BoundBox[4]},
                                             {BoundBox[1],BoundBox[3],BoundBox[4]},
                                             {BoundBox[0],BoundBox[2],BoundBox[5]},
                                             {BoundBox[1],BoundBox[2],BoundBox[5]},
                                             {BoundBox[0],BoundBox[3],BoundBox[5]},
                                             {BoundBox[1],BoundBox[3],BoundBox[5]}};
  BoundPrj[0] = vtkMath::Dot(Dir,BoundPoints[0]), BoundPrj[1] = BoundPrj[0];
  for(int i = 1; i < 8; i++){
    vtkFloatingPointType tmp = vtkMath::Dot(Dir,BoundPoints[i]);
    if(BoundPrj[1] < tmp) BoundPrj[1] = tmp;
    if(BoundPrj[0] > tmp) BoundPrj[0] = tmp;
  }
  BoundPrj[2] = BoundPrj[1] - BoundPrj[0];
  BoundPrj[1] = BoundPrj[0] + (1.0 - EPS)*BoundPrj[2];
  BoundPrj[0] = BoundPrj[0] + EPS*BoundPrj[2];
  BoundPrj[2] = BoundPrj[1] - BoundPrj[0];
}


//----------------------------------------------------------------------------
void
VISU_CutPlanesPL
::SetOrientation(const VISU_CutPlanesPL::PlaneOrientation& theOrient,
                 vtkFloatingPointType theXAng, 
                 vtkFloatingPointType theYAng, 
                 int theNum)
{
  myBasePlane[theNum] = theOrient;
  switch(myBasePlane[theNum]){
  case XY: myAng[theNum][0] = theXAng; break;
  case YZ: myAng[theNum][1] = theXAng; break;
  case ZX: myAng[theNum][2] = theXAng; break;
  }
  switch(myBasePlane[theNum]){
  case XY: myAng[theNum][1] = theYAng; break;
  case YZ: myAng[theNum][2] = theYAng; break;
  case ZX: myAng[theNum][0] = theYAng; break;
  }
}


//----------------------------------------------------------------------------
const VISU_CutPlanesPL::PlaneOrientation& 
VISU_CutPlanesPL
::GetPlaneOrientation(int theNum)
{
  return myBasePlane[theNum];
}

vtkFloatingPointType 
VISU_CutPlanesPL
::GetRotateX(int theNum)
{
  switch(myBasePlane[theNum]){
  case XY: return myAng[theNum][0];
  case YZ: return myAng[theNum][1];
  case ZX: return myAng[theNum][2];
  }
  return 0;
}


//----------------------------------------------------------------------------
vtkFloatingPointType 
VISU_CutPlanesPL
::GetRotateY(int theNum)
{
  switch(myBasePlane[theNum]){
  case XY: return myAng[theNum][1];
  case YZ: return myAng[theNum][2];
  case ZX: return myAng[theNum][0];
  }
  return 0;
}


//----------------------------------------------------------------------------
vtkFloatingPointType 
VISU_CutPlanesPL
::GetDisplacement(int theNum)
{
  return myDisplacement[theNum];
}


//----------------------------------------------------------------------------
void
VISU_CutPlanesPL
::SetDisplacement(vtkFloatingPointType theDisp, 
                  int theNum) 
{
  if(VISU::CheckIsSameValue(myDisplacement[theNum], theDisp))
    return;

  myDisplacement[theNum] = theDisp;
  Modified();
}


//----------------------------------------------------------------------------
void
VISU_CutPlanesPL
::SetNbParts(int theNbParts) 
{
  if(theNbParts > 0 && GetNbParts() != theNbParts){
    myPartPosition.resize(theNbParts);
    myPartCondition.resize(theNbParts, 1);
    myNbParts = theNbParts;
    Modified();
  }
}


//----------------------------------------------------------------------------
int
VISU_CutPlanesPL
::GetNbParts() 
{
  return myPartPosition.size();
}


//----------------------------------------------------------------------------
void
VISU_CutPlanesPL
::SetPartPosition(int thePartNumber, 
                  vtkFloatingPointType thePartPosition)
{
  if(thePartNumber >= myNbParts) 
    return;

  bool anIsSameValue = VISU::CheckIsSameValue(myPartPosition[thePartNumber], thePartPosition);
  anIsSameValue &= VISU::CheckIsSameValue(myPartCondition[thePartNumber], 0);
  if(anIsSameValue)
    return;

  myPartPosition[thePartNumber] = thePartPosition;
  myPartCondition[thePartNumber] = 0;
  Modified();
}


//----------------------------------------------------------------------------
vtkFloatingPointType 
VISU_CutPlanesPL
::GetPartPosition(int thePartNumber, 
                  int theNum)
{
  if(thePartNumber >= myNbParts) 
    return 0;

  vtkFloatingPointType aPosition = myPartPosition[thePartNumber];
  if(myPartCondition[thePartNumber]){
      vtkFloatingPointType aDir[3], aBounds[6], aBoundPrj[3];
      if(!IsDeformed()) 
        GetMergedInput()->GetBounds(aBounds);
      else
        GetMergeFilterOutput()->GetBounds(aBounds);


      GetDir(aDir,
             myAng[theNum],
             myBasePlane[theNum]);

      GetBoundProject(aBoundPrj,
                      aBounds,
                      aDir);

      if(myNbParts > 1){
        vtkFloatingPointType aDBoundPrj = aBoundPrj[2]/(myNbParts - 1);
        vtkFloatingPointType aDisplacement = aDBoundPrj * myDisplacement[theNum];
        vtkFloatingPointType aStartPosition = aBoundPrj[0] - 0.5*aDBoundPrj + aDisplacement;
        aPosition = aStartPosition + thePartNumber*aDBoundPrj;
      }else
        aPosition = aBoundPrj[0] + aBoundPrj[2]*myDisplacement[theNum];
  }

  return aPosition;
}


//----------------------------------------------------------------------------
void
VISU_CutPlanesPL
::SetPartDefault(int thePartNumber)
{
  if(thePartNumber >= myNbParts) 
    return;

  bool anIsSameValue = VISU::CheckIsSameValue(myPartPosition[thePartNumber], GetPartPosition(thePartNumber));
  anIsSameValue &= VISU::CheckIsSameValue(myPartCondition[thePartNumber], 1);
  if(anIsSameValue)
    return;

  myPartPosition[thePartNumber] = GetPartPosition(thePartNumber);
  myPartCondition[thePartNumber] = 1;
  Modified();
}


//----------------------------------------------------------------------------
int
VISU_CutPlanesPL
::IsPartDefault(int thePartNumber)
{
  if(thePartNumber >= myNbParts) 
    return 1;

  return myPartCondition[thePartNumber];
}


//----------------------------------------------------------------------------
void
VISU_CutPlanesPL
::GetDir(vtkFloatingPointType theDir[3],
         const vtkFloatingPointType theAng[3],
         const PlaneOrientation& theBasePlane)
{
  int iPlane = 0;
  vtkFloatingPointType aRx[3][3], aRy[3][3], aRz[3][3], aRotation[3][3];
  switch(theBasePlane){
  case XY:
    if(fabs(theAng[0]) > EPS) GetRx(aRx,theAng[0]); else vtkMath::Identity3x3(aRx);
    if(fabs(theAng[1]) > EPS) GetRy(aRy,theAng[1]); else vtkMath::Identity3x3(aRy);
    vtkMath::Multiply3x3(aRx,aRy,aRotation);
    iPlane = 2;
    break;
  case YZ:
    if(fabs(theAng[1]) > EPS) GetRy(aRy,theAng[1]); else vtkMath::Identity3x3(aRy);
    if(fabs(theAng[2]) > EPS) GetRz(aRz,theAng[2]); else vtkMath::Identity3x3(aRz);
    vtkMath::Multiply3x3(aRy,aRz,aRotation);
    iPlane = 0;
    break;
  case ZX:
    if(fabs(theAng[2]) > EPS) GetRz(aRz,theAng[2]); else vtkMath::Identity3x3(aRz);
    if(fabs(theAng[0]) > EPS) GetRx(aRx,theAng[0]); else vtkMath::Identity3x3(aRx);
    vtkMath::Multiply3x3(aRz,aRx,aRotation);
    iPlane = 1;
    break;
  }

  for(int i = 0; i < 3; i++)
    theDir[i] = aRotation[i][iPlane];
}


//----------------------------------------------------------------------------
void
VISU_CutPlanesPL
::CutWithPlane(vtkAppendPolyData* theAppendPolyData,
               vtkDataSet* theDataSet,
               vtkFloatingPointType theDir[3], 
               vtkFloatingPointType theOrig[3])
{
  vtkCutter *aCutPlane = vtkCutter::New();
  aCutPlane->SetInput(theDataSet);
  vtkPlane *aPlane = vtkPlane::New();
  aPlane->SetOrigin(theOrig);

  aPlane->SetNormal(theDir);
  aCutPlane->SetCutFunction(aPlane);
  aPlane->Delete();
  theAppendPolyData->AddInput(aCutPlane->GetOutput());
  aCutPlane->Delete();
}


//----------------------------------------------------------------------------
void
VISU_CutPlanesPL
::CutWithPlanes(vtkAppendPolyData* theAppendPolyData, 
                vtkDataSet* theDataSet,
                int theNbPlanes, 
                vtkFloatingPointType theDir[3], 
                vtkFloatingPointType theBounds[6],
                const std::vector<vtkFloatingPointType>& thePlanePosition,
                const std::vector<int>& thePlaneCondition,
                vtkFloatingPointType theDisplacement)
{
  vtkFloatingPointType aBoundPrj[3], aOrig[3], aPosition;
  GetBoundProject(aBoundPrj, theBounds, theDir);
  if(theNbPlanes > 1){
    vtkFloatingPointType aDBoundPrj = aBoundPrj[2]/(theNbPlanes - 1);
    vtkFloatingPointType aDisplacement = aDBoundPrj*theDisplacement;
    vtkFloatingPointType aStartPosition = aBoundPrj[0] - 0.5*aDBoundPrj + aDisplacement;
    for (int i = 0; i < theNbPlanes; i++){
      aPosition = aStartPosition + i*aDBoundPrj;
      if(thePlaneCondition[i]){
        aPosition = aStartPosition + i*aDBoundPrj;
      }else
        aPosition = thePlanePosition[i];
      VISU::Mul(theDir,aPosition,aOrig);
      CutWithPlane(theAppendPolyData,theDataSet,theDir,aOrig);
    }
  }else{
    if(thePlaneCondition[0])
      aPosition = aBoundPrj[0] + aBoundPrj[2]*theDisplacement;
    else
      aPosition = thePlanePosition[0];
    VISU::Mul(theDir,aPosition,aOrig);
    CutWithPlane(theAppendPolyData,theDataSet,theDir,aOrig);
  }
  vtkPolyData *aPolyData = theAppendPolyData->GetOutput();
  aPolyData->Update();
  theAppendPolyData->Update();
}


//----------------------------------------------------------------------------
void
VISU_CutPlanesPL::SetVectorialField(VISU::PUnstructuredGridIDMapper theMapper)
{  
  if(myVectorialField == theMapper)
    return;

  if(CheckCanDeformate(theMapper->GetOutput())){
    myVectorialField = theMapper;
    
    SetMergeFilterInput(GetMergedInput(),theMapper->GetOutput());
  }
  else
    UseDeformation(false);
  
  Modified();
}

//----------------------------------------------------------------------------
VISU::PUnstructuredGridIDMapper VISU_CutPlanesPL::
getVectorialField()
{
  return myVectorialField;
}

//----------------------------------------------------------------------------
void VISU_CutPlanesPL::SetMapScale(vtkFloatingPointType theMapScale){
  Superclass::SetMapScale(theMapScale);
  if(IsDeformed())
    VISU_OptionalDeformationPL::SetMapScale(theMapScale);
}
