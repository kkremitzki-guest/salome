// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  File   : VISU_Tools.cxx
//  Author : Oleg UVAROV
//  Module : VISU
//
#include "VISU_Tools.h"

#include "VISU_Gen_i.hh"
#include "VISU_Table_i.hh"
#include "VISU_ViewManager_i.hh"

#include <VISU_ActorBase.h>

#include <LightApp_Displayer.h>

#include <SalomeApp_Study.h>
#include <SalomeApp_Application.h>
#include <SalomeApp_Module.h>

#include <SPlot2d_ViewModel.h>
#include <Plot2d_ViewFrame.h>
#include <Plot2d_ViewManager.h>

#include <SUIT_ResourceMgr.h>
#include <SUIT_Session.h>

//=============================================================================
namespace VISU
{
  //------------------------------------------------------------
  // Internal function used by several public functions below
  void
  UpdateCurve(VISU::Curve_i* theCurve,
              Plot2d_ViewFrame* thePlot,
              SPlot2d_Curve* plotCurve,
              int theDisplaying)
  {
    if ( theDisplaying == VISU::eErase ) {
      if ( plotCurve && thePlot )
        thePlot->eraseCurve( plotCurve, false );
    }
    else if ( theDisplaying == VISU::eDisplay || theDisplaying == VISU::eDisplayOnly ) {
      if ( plotCurve ) {
        plotCurve->setHorTitle( theCurve->GetHorTitle().c_str() );
        //plotCurve->setVerTitle( ( theCurve->GetVerTitle().c_str() ) );
        plotCurve->setVerTitle( theCurve->GetName().c_str() );
        plotCurve->setHorUnits( theCurve->GetHorUnits().c_str() );
        plotCurve->setVerUnits( theCurve->GetVerUnits().c_str() );
        double* xList = 0;
        double* yList = 0;
        QStringList zList;
        int     nbPoints = theCurve->GetData( xList, yList, zList );
        if ( nbPoints > 0 && xList && yList ) {
          plotCurve->setData( xList, yList, nbPoints, zList );
        }
        if ( !theCurve->IsAuto() ) {
          plotCurve->setLine( (Plot2d::LineType)theCurve->GetLine(), theCurve->GetLineWidth() );
          plotCurve->setMarker( (Plot2d::MarkerType)theCurve->GetMarker() );
          SALOMEDS::Color color = theCurve->GetColor();
          plotCurve->setColor( QColor( (int)(color.R*255.), (int)(color.G*255.), (int)(color.B*255.) ) );
        }
        plotCurve->setAutoAssign( theCurve->IsAuto() );
        if( thePlot )
          thePlot->displayCurve( plotCurve, false );
      }
      else {
        Plot2d_Curve* crv = theCurve->CreatePresentation();
        if ( crv ) {
          if( thePlot )
            thePlot->displayCurve( crv, false );
          theCurve->SetLine( (VISU::Curve::LineType)crv->getLine(), crv->getLineWidth() );
          theCurve->SetMarker( (VISU::Curve::MarkerType)crv->getMarker());
          SALOMEDS::Color newColor;
          newColor.R = crv->getColor().red()/255.;
          newColor.G = crv->getColor().green()/255.;
          newColor.B = crv->getColor().blue()/255.;
          theCurve->SetColor( newColor );
          crv->setAutoAssign( theCurve->IsAuto() );
        }
      }
    }
  }

  //------------------------------------------------------------
  void
  PlotTable(SalomeApp_Study* theStudy,
            Plot2d_ViewFrame* thePlot,
            VISU::Table_i* table,
            int theDisplaying)
  {
    if ( !thePlot )
      return;

    if ( theDisplaying == VISU::eDisplayOnly )
      thePlot->EraseAll();
    QList<Plot2d_Curve*> clist;
    thePlot->getCurves( clist );
    _PTR(Study) aStudy = theStudy->studyDS();
    _PTR(SObject) TableSO = aStudy->FindObjectID( table->GetEntry() );
    if ( TableSO ) {
      _PTR(ChildIterator) Iter = aStudy->NewChildIterator( TableSO );
      for ( ; Iter->More(); Iter->Next() ) {
        CORBA::Object_var childObject = VISU::ClientSObjectToObject( Iter->Value() );
        if( !CORBA::is_nil( childObject ) ) {
          CORBA::Object_ptr aCurve = VISU::Curve::_narrow( childObject );
          if( !CORBA::is_nil( aCurve ) ) {
            VISU::Curve_i* theCurve = dynamic_cast<VISU::Curve_i*>(VISU::GetServant(aCurve).in());
            SPlot2d_Curve* plotCurve = 0;
            SPlot2d_Curve* tmpCurve;
            for ( int i = 0; i < clist.count(); i++ ) {
              tmpCurve = dynamic_cast<SPlot2d_Curve*>( clist.at( i ) );
              if (tmpCurve && tmpCurve->hasIO() &&
                  theCurve->GetEntry() == tmpCurve->getIO()->getEntry()) {
                plotCurve = tmpCurve;
                break;
              }
            }

            UpdateCurve( theCurve, thePlot, plotCurve, theDisplaying );
	    CurveVisibilityChanged(theCurve, theDisplaying, true, false, true);

            if ( theDisplaying == VISU::eErase && plotCurve ) {
              clist.removeAll(plotCurve );
            }
          }
        }
      }
      thePlot->Repaint();
      SetVisibilityState(table->GetEntry(),GetStateByDisplaying(theDisplaying));
    }
    
  }

  //------------------------------------------------------------
  void
  PlotCurve(Plot2d_ViewFrame* thePlot,
            VISU::Curve_i* theCurve,
            int theDisplaying)
  {
    if ( !thePlot )
      return;

//  if ( theDisplaying == VISU::eDisplayOnly )
//    thePlot->EraseAll();
    QList<Plot2d_Curve*> clist;
    thePlot->getCurves( clist );
    SPlot2d_Curve* plotCurve = 0;
    SPlot2d_Curve* tmpCurve;
    if(theDisplaying == VISU::eErase) {
      // 23.06.2008 skl for IPAL17672
      for (int i = 0; i < clist.count(); i++) {
        tmpCurve = dynamic_cast<SPlot2d_Curve*>(clist.at(i));
        if (tmpCurve && tmpCurve->hasIO() &&
            theCurve->GetEntry() == tmpCurve->getIO()->getEntry()) {
          plotCurve = tmpCurve;
          thePlot->eraseCurve(clist.at(i));
          break;
        }
      }
      UpdateCurve(theCurve, thePlot, plotCurve, theDisplaying);
    }
    else {
      for (int i = 0; i < clist.count(); i++) {
        tmpCurve = dynamic_cast<SPlot2d_Curve*>(clist.at(i));
        if (tmpCurve && tmpCurve->hasIO() &&
            theCurve->GetEntry() == tmpCurve->getIO()->getEntry()) {
          plotCurve = tmpCurve;
        }
        else if (theDisplaying == VISU::eDisplayOnly) {
          thePlot->eraseCurve(clist.at(i));
        }
      }
      UpdateCurve(theCurve, thePlot, plotCurve, theDisplaying);
    }

    thePlot->Repaint();

    SetVisibilityState(theCurve->GetEntry(),GetStateByDisplaying(theDisplaying));
    CurveVisibilityChanged(theCurve,theDisplaying,false, true, true);
  }

  //------------------------------------------------------------
  void
  PlotRemoveCurve(SalomeApp_Application* theApp,
                  VISU::Curve_i* pCrv)
  {
    QString anEntry = pCrv->GetEntry().c_str();
    ViewManagerList pvm_list;
    theApp->viewManagers( SPlot2d_Viewer::Type(), pvm_list );
    ViewManagerList::Iterator pvm_it = pvm_list.begin();
    for( ; pvm_it != pvm_list.end(); pvm_it++ ){
      Plot2d_ViewManager* pvm = dynamic_cast<Plot2d_ViewManager*>( *pvm_it );
      if( pvm ){
        SPlot2d_Viewer* aSPlot2d = dynamic_cast<SPlot2d_Viewer*>( pvm->getViewModel() );
        if( aSPlot2d ){
          Plot2d_ViewFrame* thePlot = aSPlot2d->getActiveViewFrame();
          if(thePlot){
            QList<Plot2d_Curve*> clist;
            thePlot->getCurves( clist );
            for (int i = 0; i < clist.count(); i++) {
              if(SPlot2d_Curve* plotCurve = dynamic_cast<SPlot2d_Curve*>(clist[i]))
                if(plotCurve->hasIO() && (plotCurve->getIO()->getEntry() == anEntry))
                  thePlot->eraseCurve(clist[i]);
            }
          }
        }
      }
    }
  }
  
  //------------------------------------------------------------
  // Internal function used by the function below
  SPlot2d_Curve* GetCurveByIO( const Handle(SALOME_InteractiveObject)& theIObject,
                               Plot2d_ViewFrame* thePlot )
  {
    if ( !theIObject.IsNull() && thePlot ) {
      CurveDict aCurves = thePlot->getCurves();
      CurveDict::Iterator it = aCurves.begin();
      for( ; it != aCurves.end(); ++it ) {
        SPlot2d_Curve* aCurve = dynamic_cast<SPlot2d_Curve*>( it.value() );
        if(aCurve) {
          if ( aCurve->hasIO() && aCurve->getIO()->isSame( theIObject ) )
            return aCurve;
        }
      }
    }
    return NULL;
  }

  //------------------------------------------------------------
  void
  PlotContainer(Plot2d_ViewFrame* thePlot,
                VISU::Container_i* container,
                int theDisplaying)
  {
    if ( !thePlot || !container)
      return;

    if ( theDisplaying == VISU::eDisplayOnly )
      thePlot->EraseAll();
    QList<Plot2d_Curve*> clist;
    thePlot->getCurves( clist );
    if ( container->GetNbCurves() > 0 ) {
      int nbCurves = container->GetNbCurves();
      SetVisibilityState(container->GetEntry(), GetStateByDisplaying(theDisplaying));
      for ( int k = 1; k <= nbCurves; k++ ) {
        VISU::Curve_i* theCurve = container->GetCurve( k );
        if ( theCurve && theCurve->IsValid() ) {
          SPlot2d_Curve* plotCurve = GetCurveByIO(new SALOME_InteractiveObject(theCurve->GetEntry().c_str(), "", ""), thePlot);

          UpdateCurve( theCurve, thePlot, plotCurve, theDisplaying );

          if ( plotCurve && theDisplaying == VISU::eErase ) {
            clist.removeAll( plotCurve );
          }
	  CurveVisibilityChanged(theCurve, theDisplaying, true, true, true);
        }
      }
    }
    
    thePlot->Repaint();
    if(GetResourceMgr()->booleanValue("VISU","automatic_fit_all",false)){
      thePlot->fitAll();
    }
    
    qApp->processEvents();
  }

  //------------------------------------------------------------
  void
  CreatePlot(VISU_Gen_i* theVisuGen,
             Plot2d_ViewFrame* thePlot,
             _PTR(SObject) theTableSO)
  {
    _PTR(GenericAttribute) anAttr;
    if ( theTableSO &&
         ( theTableSO->FindAttribute( anAttr, "AttributeTableOfInteger" ) ||
           theTableSO->FindAttribute( anAttr, "AttributeTableOfReal" ) ) ) {
      CORBA::Object_var aTable = VISU::ClientSObjectToObject(theTableSO);
      CORBA::Object_var aContainer = theVisuGen->CreateContainer();

      if ( !CORBA::is_nil( aTable ) && !CORBA::is_nil( aContainer ) ) {
        VISU::Table_i*     pTable     = dynamic_cast<VISU::Table_i*>(VISU::GetServant(aTable).in());
        VISU::Container_i* pContainer = dynamic_cast<VISU::Container_i*>(VISU::GetServant(aContainer).in());

        if ( pContainer && pTable ) {
          for ( int i = 2; i <= pTable->GetNbRows(); i++ ) {
            CORBA::Object_var aNewCurve = theVisuGen->CreateCurve( pTable->_this(), 1, i );
            if( !CORBA::is_nil( aNewCurve ) ) {
              VISU::Curve_i* pCrv = dynamic_cast<VISU::Curve_i*>( VISU::GetServant(aNewCurve).in() );
              if ( pCrv ) {
                pContainer->AddCurve( pCrv->_this() );
              }
            }
          }
          PlotContainer( thePlot, pContainer, VISU::eDisplay );

          QString anEntry = pContainer->GetEntry().c_str();
          _PTR(Study) aStudy = theTableSO->GetStudy();
          _PTR(SObject) aContainerSO = aStudy->FindObjectID(anEntry.toLatin1().data());
          _PTR(SObject) aParentSO = aContainerSO->GetFather();
        }
      }
    }
  }

  //------------------------------------------------------------
  void SetVisibilityState(std::string entry, Qtx::VisibilityState state) {
    if(entry.empty())
      return;

    if( SUIT_Session* aSession = SUIT_Session::session() )
      if( SalomeApp_Application* anApp = dynamic_cast<SalomeApp_Application*>(aSession->activeApplication()) )
	if( SalomeApp_Study* aStudy =  dynamic_cast<SalomeApp_Study*>(anApp->activeStudy()) )
	  aStudy->setVisibilityState(entry.c_str(), state);

  }

  //------------------------------------------------------------
  void SetVisibilityState(SALOME_Actor *theActor, Qtx::VisibilityState state) {
    if(!theActor || !theActor->hasIO() || !theActor->getIO()->hasEntry())
      return;
    SetVisibilityState(theActor->getIO()->getEntry(), state);
  }

  void CurveVisibilityChanged(VISU::Curve_i* theCurve, 
			      int theDisplaying,
			      bool updateCurve,
			      bool updateTable,
			      bool updateContainers) {
    
    SUIT_Session* aSession = SUIT_Session::session();
    if (!aSession) return;
    
    SalomeApp_Application* anApp = dynamic_cast<SalomeApp_Application*>(aSession->activeApplication());
    if ( !anApp ) return;

    SalomeApp_Study* aStudy = dynamic_cast<SalomeApp_Study*>( anApp->activeStudy() );
    if ( !aStudy ) return;

    SalomeApp_Module* aModule = dynamic_cast<SalomeApp_Module*>( anApp->module( anApp->moduleTitle( "VISU" ) ) );
    if ( !aModule ) return;

    LightApp_Displayer* aDisplayer = aModule->displayer();

    SUIT_ViewManager* aManager = anApp->activeViewManager();

    Qtx::VisibilityState state = ( aManager && aManager->getType() == SPlot2d_Viewer::Type() ) ?
      GetStateByDisplaying(theDisplaying) : Qtx::UnpresentableState;
    
    if ( updateCurve )
      SetVisibilityState( theCurve->GetEntry(), state );
    
    if ( updateTable ) {
      Table_i* aTable = theCurve->getTable();
      if ( aTable && !(aStudy->visibilityState( aTable->GetEntry().c_str() ) == state) ) {
	_PTR(SObject) TableSO = aStudy->studyDS()->FindObjectID( aTable->GetEntry() );
	if ( TableSO ) {
	  bool isTableVisible = false;
	  if ( aDisplayer && state != Qtx::UnpresentableState ) {
	    _PTR(ChildIterator) Iter = aStudy->studyDS()->NewChildIterator( TableSO );
	    for ( ; Iter->More() && !isTableVisible ; Iter->Next() ) {
	      CORBA::Object_var childObject = VISU::ClientSObjectToObject( Iter->Value() );
	      if ( CORBA::is_nil( childObject ) ) continue;
	      VISU::Curve_i* aCurve = dynamic_cast<VISU::Curve_i*>( VISU::GetServant( childObject.in() ).in() );
	      isTableVisible = aCurve && aDisplayer->IsDisplayed( aCurve->GetEntry().c_str() );
	    }
	  } // if ( aDisplayer ... )
	  if ( state != Qtx::UnpresentableState )
	    SetVisibilityState( aTable->GetEntry(), ( isTableVisible ? Qtx::ShownState : Qtx::HiddenState ) );
	  else 
	    SetVisibilityState( aTable->GetEntry(), state );
	} // if ( TableSO )
      } // if ( aTable )
    } // if ( updateTable )

    if ( updateContainers ) {
      ContainerSet aContainers = theCurve->getContainers();
      ContainerSet::ConstIterator it = aContainers.begin();
      for ( ; it != aContainers.end(); it++ ) {
	//Check that state of container is not set already
	if(aStudy->visibilityState(*it) == state) continue;
	_PTR(SObject) aSObject = aStudy->studyDS()->FindObjectID( (*it).toLatin1().data() );
	if ( !aSObject ) continue;
	bool isContainerDisplayed = false;
	if ( aDisplayer && state != Qtx::UnpresentableState ) {
	  CORBA::Object_var anObj = VISU::ClientSObjectToObject( aSObject );
	  if ( CORBA::is_nil( anObj ) ) continue;
	  VISU::Container_i* aContainer = dynamic_cast<VISU::Container_i*>( VISU::GetServant( anObj.in() ).in() );
	  if ( !aContainer ) continue;
	  int nbCurves = aContainer->GetNbCurves();
	  for ( int k = 1; k <= nbCurves && !isContainerDisplayed; k++ ) {
	    VISU::Curve_i* aCurve = aContainer->GetCurve( k );
	    isContainerDisplayed = aCurve && aDisplayer->IsDisplayed( aCurve->GetEntry().c_str() );
	  }
	} // if ( aDisplayer ... )
	if ( state != Qtx::UnpresentableState )
	  SetVisibilityState( (*it).toLatin1().constData(), ( isContainerDisplayed ? Qtx::ShownState : Qtx::HiddenState ) );
	else {
	  SetVisibilityState( (*it).toLatin1().constData(), state );
	}
      } // for ( ; it != aContainers.end(); it++ )
    } //updateContainers    
  }
  
  Qtx::VisibilityState GetStateByDisplaying(int theDisplaying)
  {
    Qtx::VisibilityState state = Qtx::UnpresentableState;
    if(theDisplaying == eDisplayAll || 
       theDisplaying == eDisplay    || 
       theDisplaying == eDisplayOnly ) {
      state = Qtx::ShownState;
      
    } else if (theDisplaying == eErase || theDisplaying == eEraseAll) {
      state = Qtx::HiddenState;
    }
    return state;
  }

  void updateContainerVisibility(VISU::Container_i* theContainer)
  {
    if ( !theContainer ) return;
    
    SUIT_Session* aSession = SUIT_Session::session();
    if (!aSession) return;
    
    SalomeApp_Application* anApp = dynamic_cast<SalomeApp_Application*>(aSession->activeApplication());
    if ( !anApp ) return;

    SalomeApp_Study* aStudy = dynamic_cast<SalomeApp_Study*>( anApp->activeStudy() );
    if ( !aStudy ) return;

    SalomeApp_Module* aModule = dynamic_cast<SalomeApp_Module*>( anApp->module( anApp->moduleTitle( "VISU" ) ) );
    if ( !aModule ) return;

    SUIT_ViewManager* aManager = anApp->activeViewManager();
    int nbCurves = theContainer->GetNbCurves();
    
    Qtx::VisibilityState state = ( aManager && aManager->getType() == SPlot2d_Viewer::Type() && nbCurves > 0 ) ?
      Qtx::HiddenState : Qtx::UnpresentableState;
    
    LightApp_Displayer* aDisplayer = aModule->displayer();

    if ( nbCurves > 0 && aDisplayer ) {
      for ( int k = 1; k <= nbCurves; k++ ) {
	VISU::Curve_i* aCurve = theContainer->GetCurve( k );
	if ( aCurve && aDisplayer->IsDisplayed( aCurve->GetEntry().c_str() ) ) {
	  state = Qtx::ShownState;
	  break;
	} 	      
      }
    }
    aStudy->setVisibilityState( theContainer->GetEntry().c_str(), state );
  }
}
