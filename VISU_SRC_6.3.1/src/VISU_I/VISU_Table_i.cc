// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : VISU_Table_i.cc
//  Author : Vadim SANDLER
//  Module : VISU
//
#include "VISU_Table_i.hh"

#include "VISU_Tools.h"
#include "VISU_CutLinesBase_i.hh"
#include "VISU_CutSegment_i.hh"
#include "VISU_Result_i.hh"
#include "VISU_ViewManager_i.hh"

#include "SALOME_Event.h"
#include "SPlot2d_Curve.h"

#include "VISU_TableReader.hxx"
#include "VISU_ConvertorUtils.hxx"

#include "utilities.h"

#include <Basics_Utils.hxx>

using namespace std;

#ifdef _DEBUG_
static int MYDEBUG = 0;
#else
static int MYDEBUG = 0;
#endif
//----------------------------------------------------------------
//                      Table Object
//----------------------------------------------------------------
int VISU::Table_i::myNbPresent = 0;
const string VISU::Table_i::myComment  = "TABLE";
/*!
  Generate unique name
*/
QString VISU::Table_i::GenerateName()
{
  return VISU::GenerateName( "Table", ++myNbPresent );
}
/*!
  Gets comment string
*/
const char* VISU::Table_i::GetComment() const
{
  return myComment.c_str();
}
/*!
  Constructor
*/
VISU::Table_i::Table_i( SALOMEDS::Study_ptr theStudy, const char* theObjectEntry )
     : PrsObject_i(theStudy)
{
  MESSAGE("Table_i::Table_i - "<<this);
  mySObj = theStudy->FindObjectID(theObjectEntry);
  myOrientation = VISU::Table::HORIZONTAL;
}
/*!
  Destructor
*/
VISU::Table_i::~Table_i()
{
  MESSAGE("Table_i::~Table_i - "<<this);
}

//----------------------------------------------------------------------------
void
VISU::Table_i
::SetTitle( const char* theTitle )
{
  SetName( theTitle, true );
}

//----------------------------------------------------------------------------
char*
VISU::Table_i
::GetTitle()
{
  return CORBA::string_dup( GetName().c_str() );
}

//----------------------------------------------------------------------------
void
VISU::Table_i
::SetOrientation( VISU::Table::Orientation theOrientation )
{
  myOrientation = theOrientation;
}

//----------------------------------------------------------------------------
VISU::Table::Orientation
VISU::Table_i
::GetOrientation()
{
  return myOrientation;
}


//----------------------------------------------------------------------------
void
VISU::Table_i
::SortRow(CORBA::Long theRow, VISU::SortOrder theSortOrder, VISU::SortPolicy theSortPolicy)
{
  SALOMEDS::SObject_var SO = mySObj;
  SALOMEDS::StudyBuilder_var Builder = GetStudyDocument()->NewBuilder();
  if ( !SO->_is_nil() ) {
    SALOMEDS::GenericAttribute_var anAttr;
    if ( Builder->FindAttribute( SO, anAttr, "AttributeTableOfInteger" ) ) {
      SALOMEDS::AttributeTableOfInteger_var anInt = SALOMEDS::AttributeTableOfInteger::_narrow( anAttr );
      anInt->SortRow( theRow, (SALOMEDS::AttributeTable::SortOrder)theSortOrder,
                      (SALOMEDS::AttributeTable::SortPolicy)theSortPolicy );
    }
    else if ( Builder->FindAttribute( SO, anAttr, "AttributeTableOfReal" ) ) {
      SALOMEDS::AttributeTableOfReal_var aReal = SALOMEDS::AttributeTableOfReal::_narrow( anAttr );
      aReal->SortRow( theRow, (SALOMEDS::AttributeTable::SortOrder)theSortOrder,
                      (SALOMEDS::AttributeTable::SortPolicy)theSortPolicy );
    }
    UpdateCurves( std::map<int, int>() );
  }
}

//----------------------------------------------------------------------------
void
VISU::Table_i
::SortColumn(CORBA::Long theColumn, VISU::SortOrder theSortOrder, VISU::SortPolicy theSortPolicy)
{
  SALOMEDS::SObject_var SO = mySObj;
  SALOMEDS::StudyBuilder_var Builder = GetStudyDocument()->NewBuilder();
  if ( !SO->_is_nil() ) {
    SALOMEDS::GenericAttribute_var anAttr;
    if ( Builder->FindAttribute( SO, anAttr, "AttributeTableOfInteger" ) ) {
      SALOMEDS::AttributeTableOfInteger_var anInt = SALOMEDS::AttributeTableOfInteger::_narrow( anAttr );
      anInt->SortColumn( theColumn, (SALOMEDS::AttributeTable::SortOrder)theSortOrder,
                         (SALOMEDS::AttributeTable::SortPolicy)theSortPolicy );
    }
    else if ( Builder->FindAttribute( SO, anAttr, "AttributeTableOfReal" ) ) {
      SALOMEDS::AttributeTableOfReal_var aReal = SALOMEDS::AttributeTableOfReal::_narrow( anAttr );
      aReal->SortColumn( theColumn, (SALOMEDS::AttributeTable::SortOrder)theSortOrder,
                         (SALOMEDS::AttributeTable::SortPolicy)theSortPolicy );
    }
    UpdateCurves( std::map<int, int>() );
  }
}

//----------------------------------------------------------------------------
void
VISU::Table_i
::SortByRow(CORBA::Long theRow, VISU::SortOrder theSortOrder, VISU::SortPolicy theSortPolicy)
{
  SALOMEDS::SObject_var SO = mySObj;
  SALOMEDS::StudyBuilder_var Builder = GetStudyDocument()->NewBuilder();
  if ( !SO->_is_nil() ) {
    SALOMEDS::GenericAttribute_var anAttr;
    if ( Builder->FindAttribute( SO, anAttr, "AttributeTableOfInteger" ) ) {
      SALOMEDS::AttributeTableOfInteger_var anInt = SALOMEDS::AttributeTableOfInteger::_narrow( anAttr );
      anInt->SortByRow( theRow, (SALOMEDS::AttributeTable::SortOrder)theSortOrder,
                        (SALOMEDS::AttributeTable::SortPolicy)theSortPolicy );
    }
    else if ( Builder->FindAttribute( SO, anAttr, "AttributeTableOfReal" ) ) {
      SALOMEDS::AttributeTableOfReal_var aReal = SALOMEDS::AttributeTableOfReal::_narrow( anAttr );
      aReal->SortByRow( theRow, (SALOMEDS::AttributeTable::SortOrder)theSortOrder,
                        (SALOMEDS::AttributeTable::SortPolicy)theSortPolicy );
    }
    UpdateCurves( std::map<int, int>() );
  }
}

//----------------------------------------------------------------------------
void
VISU::Table_i
::SortByColumn(CORBA::Long theColumn, VISU::SortOrder theSortOrder, VISU::SortPolicy theSortPolicy)
{
  SALOMEDS::SObject_var SO = mySObj;
  SALOMEDS::StudyBuilder_var Builder = GetStudyDocument()->NewBuilder();
  if ( !SO->_is_nil() ) {
    SALOMEDS::LongSeq_var aRowIndices;
    SALOMEDS::GenericAttribute_var anAttr;
    if ( Builder->FindAttribute( SO, anAttr, "AttributeTableOfInteger" ) ) {
      SALOMEDS::AttributeTableOfInteger_var anInt = SALOMEDS::AttributeTableOfInteger::_narrow( anAttr );
      aRowIndices = anInt->SortByColumn( theColumn, (SALOMEDS::AttributeTable::SortOrder)theSortOrder,
                                         (SALOMEDS::AttributeTable::SortPolicy)theSortPolicy );
    }
    else if ( Builder->FindAttribute( SO, anAttr, "AttributeTableOfReal" ) ) {
      SALOMEDS::AttributeTableOfReal_var aReal = SALOMEDS::AttributeTableOfReal::_narrow( anAttr );
      aRowIndices = aReal->SortByColumn( theColumn, (SALOMEDS::AttributeTable::SortOrder)theSortOrder,
                                         (SALOMEDS::AttributeTable::SortPolicy)theSortPolicy );
    }
    std::map<int, int> aMixData;
    for ( int i = 0, n = aRowIndices->length(); i < n; i++ )
      aMixData[ aRowIndices[i] ] = i+1;
    UpdateCurves( aMixData );
  }
}

//----------------------------------------------------------------------------
void
VISU::Table_i
::UpdateCurves(std::map<int,int> theMixData)
{
  SALOMEDS::SObject_var SO = mySObj;
  SALOMEDS::StudyBuilder_var Builder = GetStudyDocument()->NewBuilder();
  SALOMEDS::ChildIterator_var CI = GetStudyDocument()->NewChildIterator( SO );
  for ( CI->InitEx( true ); CI->More(); CI->Next() ) {
    CORBA::Object_var anObj = SObjectToObject( CI->Value() );
    VISU::Curve_var aCurve = VISU::Curve::_narrow( anObj );
    if ( !aCurve->_is_nil() ) {
      if ( VISU::Curve_i* pCurve = dynamic_cast<VISU::Curve_i*>( GetServant( aCurve ).in() ) ) {
        int aHRow = pCurve->GetHRow(), aVRow = pCurve->GetVRow();
        if ( theMixData.find( aHRow ) != theMixData.end() )
          pCurve->SetHRow( theMixData[ aHRow ] );
        if ( theMixData.find( aVRow ) != theMixData.end() )
          pCurve->SetVRow( theMixData[ aVRow ] );
        UpdatePlot2d( pCurve, eUpdateData );
      }
    }
  }
}

//----------------------------------------------------------------------------
SALOMEDS::SObject_var
VISU::Table_i
::GetSObject() const
{
  return mySObj;
}

//----------------------------------------------------------------------------
std::string
VISU::Table_i
::GetObjectEntry() 
{
  CORBA::String_var anEntry = mySObj->GetID();
  return anEntry.in(); 
}

//----------------------------------------------------------------------------
/*!
  Gets number of rows in table
*/
CORBA::Long VISU::Table_i::GetNbRows()
{
  SALOMEDS::SObject_var SO = mySObj;
  SALOMEDS::StudyBuilder_var Builder = GetStudyDocument()->NewBuilder();
  if ( !SO->_is_nil() ) {
    SALOMEDS::GenericAttribute_var        anAttr;
    if ( Builder->FindAttribute( SO, anAttr, "AttributeTableOfInteger" ) ) {
      SALOMEDS::AttributeTableOfInteger_var anInt =  SALOMEDS::AttributeTableOfInteger::_narrow( anAttr );
	return anInt->GetNbRows();
    }
    else if ( Builder->FindAttribute( SO, anAttr, "AttributeTableOfReal" ) ) {
      SALOMEDS::AttributeTableOfReal_var aReal =  SALOMEDS::AttributeTableOfReal::_narrow( anAttr );
      return aReal->GetNbRows();
    }
  }
  return 0;
}
/*!
  Gets number of columns in table
*/
CORBA::Long VISU::Table_i::GetNbColumns()
{
  SALOMEDS::SObject_var SO = mySObj;
  SALOMEDS::StudyBuilder_var Builder = GetStudyDocument()->NewBuilder();
  if ( !SO->_is_nil() ) {
    SALOMEDS::GenericAttribute_var        anAttr;
    if ( Builder->FindAttribute( SO, anAttr, "AttributeTableOfInteger" ) ) {
      SALOMEDS::AttributeTableOfInteger_var anInt =  SALOMEDS::AttributeTableOfInteger::_narrow( anAttr );
      return anInt->GetNbColumns();
    }
    else if ( Builder->FindAttribute( SO, anAttr, "AttributeTableOfReal" ) ) {
      SALOMEDS::AttributeTableOfReal_var aReal =  SALOMEDS::AttributeTableOfReal::_narrow( anAttr );
      return aReal->GetNbColumns();
    }
  }
  return 0;
}
/*!
  Creates table object
*/
VISU::Storable* VISU::Table_i::Create()
{
  // generate name ...
  SetName(GetTableTitle().toLatin1().data(), false);

  // mpv (PAL 5357): if name attribute already exist at this label, use it as name of table
  if ( GetName() == "" )
    if ( !mySObj->_is_nil() ) {
      CutLinesBase_i* pCutLines = NULL;
      CORBA::Object_var anObj = SObjectToObject(mySObj);
      if(!CORBA::is_nil(anObj)){
	VISU::CutLinesBase_var aCutLines = VISU::CutLinesBase::_narrow(anObj);
	  if(!aCutLines->_is_nil())
	    pCutLines = dynamic_cast<CutLinesBase_i*>(GetServant(aCutLines).in());
	}
      if (!pCutLines)
	if (mySObj->GetName()) SetName(mySObj->GetName(), false);
    }

  if ( GetName() == "" )
    SetName(GenerateName().toLatin1().data(), false);
  // ... and build the object
  return Build( false );
}
/*!
  Builds presentation of table
*/
VISU::Storable* VISU::Table_i::Build( int theRestoring )
{

  // look for reference SObject with table attribute
  SALOMEDS::SObject_var SO = mySObj;

  if ( !SO->_is_nil() ) {
    CutLinesBase_i* pCutLines = NULL;
    CORBA::Object_var anObj = SObjectToObject(SO);
    if(!CORBA::is_nil(anObj)){
      VISU::CutLinesBase_var aCutLines = VISU::CutLinesBase::_narrow(anObj);
      if(!aCutLines->_is_nil())
        pCutLines = dynamic_cast<CutLinesBase_i*>(GetServant(aCutLines).in());
    }
    SALOMEDS::StudyBuilder_var Builder = GetStudyDocument()->NewBuilder();
    SALOMEDS::GenericAttribute_var anAttr;
    // look for component
    if ( !theRestoring ) {
      SALOMEDS::SComponent_var SComponent = VISU::FindOrCreateVisuComponent( GetStudyDocument() );
      // create SObject and set attributes
      QString aComment;
      if(pCutLines)
        aComment.sprintf("myComment=%s;mySourceId=CutLines",GetComment());
      else{
        aComment.sprintf("myComment=%s;mySourceId=TableAttr",GetComment());
        SALOMEDS::SObject_var aFatherSObject = SO->GetFather();
        if(aFatherSObject->FindAttribute(anAttr,"AttributeString")){
          SALOMEDS::AttributeString_var aCommentAttr =
            SALOMEDS::AttributeString::_narrow(anAttr);
          CORBA::String_var aValue = aCommentAttr->Value();
          Storable::TRestoringMap aMap;
          Storable::StringToMap(aValue.in(),aMap);
          bool anIsExist;
          QString aMethodName = VISU::Storable::FindValue(aMap,"myComment",&anIsExist);
          if(anIsExist){
            if( aMethodName == "ImportTables" ){
              aComment.sprintf("myComment=%s;mySourceId=TableFile",GetComment());
            }
          }
        }
      }

      string anEntry = CreateAttributes( GetStudyDocument(),
                                         SO->GetID(),//SComponent->GetID(),
                                         "ICON_TREE_TABLE",
                                         GetID(),
                                         GetName(),
                                         "",
                                         aComment.toLatin1().data(),
                                         pCutLines );
      // create SObject referenced to real table object
      mySObj = SALOMEDS::SObject::_duplicate(GetStudyDocument()->FindObjectID( anEntry.c_str() ));
      if(pCutLines) {
        bool isCutSegment = dynamic_cast<CutSegment_i*>(pCutLines);
        pCutLines->BuildTableOfReal(mySObj, isCutSegment);
      }
      // mpv (PAL5357): reference attributes are unnecessary now
      //SALOMEDS::SObject_var refSO = Builder->NewObject( mySObj );
      //Builder->Addreference( refSO, SO );
    }

    return this;
  }
  return NULL;
}
/*!
  Restores table object from stream
*/
VISU::Storable* VISU::Table_i::Restore( const Storable::TRestoringMap& theMap, SALOMEDS::SObject_ptr SO)
{
  if(MYDEBUG) MESSAGE(GetComment());
  SetName(VISU::Storable::FindValue(theMap,"myName").toLatin1().data(), false);
  myTitle = VISU::Storable::FindValue(theMap,"myTitle").toLatin1().data();
  myOrientation = ( VISU::Table::Orientation )( VISU::Storable::FindValue(theMap,"myOrientation").toInt() );
  mySObj = SALOMEDS::SObject::_duplicate(SO);
  return Build( true );
}
/*!
  Flushes table data into stream
*/
void VISU::Table_i::ToStream( std::ostringstream& theStr )
{
  Storable::DataToStream( theStr, "myName",        GetName().c_str() );
  Storable::DataToStream( theStr, "myTitle",       myTitle.c_str() );
  Storable::DataToStream( theStr, "myOrientation", myOrientation );
}
/*!
  Called from engine to restore table from the file
*/
VISU::Storable* VISU::Table_i::StorableEngine(SALOMEDS::SObject_ptr theSObject,
					      const Storable::TRestoringMap& theMap,
					      const std::string& thePrefix,
					      CORBA::Boolean theIsMultiFile)
{
  SALOMEDS::Study_var aStudy = theSObject->GetStudy();
  VISU::Table_i* pResent = new VISU::Table_i( aStudy, "" );
  return pResent->Restore( theMap, theSObject);
}
/*!
  Gets title for the original table object
*/
QString VISU::Table_i::GetTableTitle()
{
  SALOMEDS::SObject_var SO = mySObj;
  SALOMEDS::StudyBuilder_var Builder = GetStudyDocument()->NewBuilder();
  SALOMEDS::GenericAttribute_var        anAttr;
  SALOMEDS::AttributeTableOfInteger_var anInt;
  SALOMEDS::AttributeTableOfReal_var    aReal;
  if ( !SO->_is_nil() ) {
    if ( Builder->FindAttribute( SO, anAttr, "AttributeTableOfInteger" ) ) {
      anInt = SALOMEDS::AttributeTableOfInteger::_narrow( anAttr );
      CORBA::String_var aString = anInt->GetTitle();
      return aString.in();
    }
    else if ( Builder->FindAttribute( SO, anAttr, "AttributeTableOfReal" ) ) {
      aReal = SALOMEDS::AttributeTableOfReal::_narrow( anAttr );
      CORBA::String_var aString = aReal->GetTitle();
      return aString.in();
    }
  }
  return "";
}

//---------------------------------------------------------------
void VISU::Table_i::RemoveFromStudy()
{
  struct TRemoveFromStudy: public SALOME_Event
  {
    VISU::Table_i* myRemovable;
    TRemoveFromStudy(VISU::Table_i* theRemovable):
      myRemovable(theRemovable)
    {}
    
    virtual
    void
    Execute()
    {
      VISU::RemoveFromStudy(myRemovable->GetSObject(),false);
    }
  };

  // Remove the table with all curves
  ProcessVoidEvent(new TRemoveFromStudy(this));
}

//----------------------------------------------------------------
//                      Curve Object
//----------------------------------------------------------------
/*!
  Restores table object from the stream [ static ]
*/
static VISU::Table_i* GetTable( SALOMEDS::Study_ptr theStudy, SALOMEDS::SObject_ptr theSO ) {
  CORBA::Object_var anObject = VISU::SObjectToObject( theSO );
  if( !CORBA::is_nil( anObject ) ) {
    CORBA::Object_ptr aTable = VISU::Table::_narrow( anObject );
    if( !CORBA::is_nil( aTable ) )
      return dynamic_cast<VISU::Table_i*>(VISU::GetServant(aTable).in());
  }
  return NULL;
}

int VISU::Curve_i::myNbPresent = 0;
const string VISU::Curve_i::myComment  = "CURVE";
/*!
  Generate unique name
*/
QString VISU::Curve_i::GenerateName()
{
  return VISU::GenerateName( "Curve", ++myNbPresent ).toLatin1().data();
}
/*!
  Gets comment string
*/
const char* VISU::Curve_i::GetComment() const
{
  return myComment.c_str();
}
/*!
  Constructor
  NB : theHRow, theVRow are the indexes of rows in the Table object and numbered from the 1 to GetNbRows()
*/
VISU::Curve_i::Curve_i( SALOMEDS::Study_ptr theStudy, Table_i* theTable,
		        CORBA::Long theHRow, CORBA::Long theVRow,
			CORBA::Long theZRow, CORBA::Boolean theIsV2 )
: PrsObject_i(theStudy), myTable( theTable ), myHRow( theHRow ),
  myVRow( theVRow ), myZRow( theZRow ), myIsV2( theIsV2 )
{
  myAuto = true;
  myLine = VISU::Curve::SOLIDLINE;
  myLineWidth = 0;
  myMarker = VISU::Curve::CIRCLE;
  myColor.R = 0.0; myColor.G = 0.0; myColor.B = 0.0;
}
/*!
  Destructor
*/
VISU::Curve_i::~Curve_i()
{
  MESSAGE("Curve_i::~Curve_i");
}

//----------------------------------------------------------------------------
void
VISU::Curve_i
::SetTitle( const char* theTitle )
{
  SetName( theTitle, true );
}

//----------------------------------------------------------------------------
char*
VISU::Curve_i
::GetTitle()
{
  return CORBA::string_dup( GetName().c_str() );
}

//----------------------------------------------------------------------------
void
VISU::Curve_i
::SetColor( const SALOMEDS::Color& theColor )
{
  myColor = theColor; 
  myAuto = false;
}

//----------------------------------------------------------------------------
SALOMEDS::Color
VISU::Curve_i
::GetColor()
{
  return myColor;
}

//----------------------------------------------------------------------------
void
VISU::Curve_i
::SetMarker( VISU::Curve::MarkerType theType )
{
  myMarker = theType; 
  myAuto = false;
}

//----------------------------------------------------------------------------
VISU::Curve::MarkerType
VISU::Curve_i
::GetMarker()
{
  return myMarker;
}

//----------------------------------------------------------------------------
void
VISU::Curve_i
::SetLine( VISU::Curve::LineType theType, CORBA::Long theWidth )
{
  myLine = theType; 
  myLineWidth = theWidth; 
  myAuto = false;
}

//----------------------------------------------------------------------------
VISU::Curve::LineType
VISU::Curve_i
::GetLine()
{
  return myLine;
}

//----------------------------------------------------------------------------
CORBA::Long
VISU::Curve_i
::GetLineWidth()
{
  return myLineWidth;
}

//----------------------------------------------------------------------------
/*!
  Creates curve object
*/
VISU::Storable* VISU::Curve_i::Create()
{
  // generate name ...
  SetName(GetVerTitle(), false);
  if ( GetName() == "" )
    SetName(GenerateName().toLatin1().data(), false);
  // ... and build the object
  return Build( false );
}
/*!
  Builds presentation of curve
*/
VISU::Storable* VISU::Curve_i::Build(int theRestoring )
{
  if ( myTable != NULL ) {
    // getting table SObject by it's entry
    int nbRows = myTable->GetNbRows();
    if ( myHRow > 0 && myHRow <= nbRows && myVRow > 0 && myVRow <= nbRows ) {
      if ( !theRestoring ) {
	// look for component
	SALOMEDS::SComponent_var SComponent = VISU::FindOrCreateVisuComponent( GetStudyDocument() );
	// create SObject and set attributes
	QString aComment;
	aComment.sprintf("myComment=%s",GetComment());
	string anEntry = CreateAttributes( GetStudyDocument(),
					  myTable->GetObjectEntry(),
					  "",
					  GetID(),
					  GetName(),
					  "",
					  aComment.toLatin1().data(),
					  true );
	// create SObject referenced to real table object
	mySObj = SALOMEDS::SObject::_duplicate(GetStudyDocument()->FindObjectID(anEntry.c_str()));

	// Set icon
	SALOMEDS::StudyBuilder_var aStudyBuilder = GetStudyDocument()->NewBuilder();
	SALOMEDS::GenericAttribute_var anAttr;
	SALOMEDS::AttributePixMap_var  aPixmap;
	anAttr  = aStudyBuilder->FindOrCreateAttribute( mySObj, "AttributePixMap" );
	aPixmap = SALOMEDS::AttributePixMap::_narrow( anAttr );
	aPixmap ->SetPixMap("ICON_TREE_CURVE");
      }
      return this;
    }
  }
  return NULL;
}

/*!
  Returns CORBA::True if curve refers to valid table data
*/
CORBA::Boolean VISU::Curve_i::IsValid()
{
  // getting table SObject by it's entry
  SALOMEDS::SObject_var SO = GetStudyDocument()->FindObjectID(myTable->GetObjectEntry().c_str());
  SALOMEDS::StudyBuilder_var Builder = GetStudyDocument()->NewBuilder();
  SALOMEDS::GenericAttribute_var        anAttr;
  SALOMEDS::AttributeTableOfInteger_var anInt;
  SALOMEDS::AttributeTableOfReal_var    aReal;
  if ( !SO->_is_nil() ) {
    if ( Builder->FindAttribute( SO, anAttr, "AttributeTableOfInteger" ) ) {
      anInt = SALOMEDS::AttributeTableOfInteger::_narrow( anAttr );
      if ( myHRow > 0 && myHRow <= anInt->GetNbRows() && myVRow > 0 && myVRow <= anInt->GetNbRows() ) {
	return true;
      }
    }
    else if ( Builder->FindAttribute( SO, anAttr, "AttributeTableOfReal" ) ) {
      aReal = SALOMEDS::AttributeTableOfReal::_narrow( anAttr );
      if ( myHRow > 0 && myHRow <= aReal->GetNbRows() && myVRow > 0 && myVRow <= aReal->GetNbRows() ) {
	return true;
      }
    }
  }
  return false;
}
/*!
  Returns hor.axis title
*/
string VISU::Curve_i::GetHorTitle()
{
  string title;
  // getting table SObject by it's entry
  SALOMEDS::SObject_var SO = GetStudyDocument()->FindObjectID(myTable->GetObjectEntry().c_str());
  SALOMEDS::StudyBuilder_var Builder = GetStudyDocument()->NewBuilder();
  SALOMEDS::GenericAttribute_var        anAttr;
  SALOMEDS::AttributeTableOfInteger_var anInt;
  SALOMEDS::AttributeTableOfReal_var    aReal;
  if ( !SO->_is_nil() ) {
    if ( Builder->FindAttribute( SO, anAttr, "AttributeTableOfInteger" ) ) {
      anInt = SALOMEDS::AttributeTableOfInteger::_narrow( anAttr );
      SALOMEDS::StringSeq_var rowTitles = anInt->GetRowTitles();
      if ( rowTitles->length() > 0 && myHRow > 0 && myHRow <= anInt->GetNbRows() ) {
	title = rowTitles[ myHRow-1 ];
      }
    }
    else if ( Builder->FindAttribute( SO, anAttr, "AttributeTableOfReal" ) ) {
      aReal = SALOMEDS::AttributeTableOfReal::_narrow( anAttr );
      SALOMEDS::StringSeq_var rowTitles = aReal->GetRowTitles();
      if ( rowTitles->length() > 0 && myHRow > 0 && myHRow <= aReal->GetNbRows() ) {
	title = rowTitles[ myHRow-1 ];
      }
    }
  }
  return title;
}
/*!
  Returns ver.axis title
*/
string VISU::Curve_i::GetVerTitle()
{
  string title;
  // getting table SObject by it's entry
  SALOMEDS::SObject_var SO = GetStudyDocument()->FindObjectID(myTable->GetObjectEntry().c_str());
  SALOMEDS::StudyBuilder_var Builder = GetStudyDocument()->NewBuilder();
  SALOMEDS::GenericAttribute_var        anAttr;
  SALOMEDS::AttributeTableOfInteger_var anInt;
  SALOMEDS::AttributeTableOfReal_var    aReal;
  if ( !SO->_is_nil() ) {
    if ( Builder->FindAttribute( SO, anAttr, "AttributeTableOfInteger" ) ) {
      anInt = SALOMEDS::AttributeTableOfInteger::_narrow( anAttr );
      SALOMEDS::StringSeq_var rowTitles = anInt->GetRowTitles();
      if ( rowTitles->length() > 0 && myVRow > 0 && myVRow <= anInt->GetNbRows() )
	title = rowTitles[ myVRow-1 ];
    }
    else if ( Builder->FindAttribute( SO, anAttr, "AttributeTableOfReal" ) ) {
      aReal = SALOMEDS::AttributeTableOfReal::_narrow( anAttr );
      SALOMEDS::StringSeq_var rowTitles = aReal->GetRowTitles();
      if ( rowTitles->length() > 0 && myVRow > 0 && myVRow <= aReal->GetNbRows() )
	title = rowTitles[ myVRow-1 ];
    }
  }
  return title;
}
/*!
  Returns hor.axis units
*/
string VISU::Curve_i::GetHorUnits()
{
  string units;
  // getting table SObject by it's entry
  SALOMEDS::SObject_var SO = GetStudyDocument()->FindObjectID(myTable->GetObjectEntry().c_str());
  SALOMEDS::StudyBuilder_var Builder = GetStudyDocument()->NewBuilder();
  SALOMEDS::GenericAttribute_var        anAttr;
  SALOMEDS::AttributeTableOfInteger_var anInt;
  SALOMEDS::AttributeTableOfReal_var    aReal;
  if ( !SO->_is_nil()  ) {
    if ( Builder->FindAttribute( SO, anAttr, "AttributeTableOfInteger" ) ) {
      anInt = SALOMEDS::AttributeTableOfInteger::_narrow( anAttr );
      SALOMEDS::StringSeq_var rowUnits = anInt->GetRowUnits();
      if ( rowUnits->length() > 0 && myHRow > 0 && myHRow <= anInt->GetNbRows() )
	units = rowUnits[ myHRow-1 ];
    }
    else if ( Builder->FindAttribute( SO, anAttr, "AttributeTableOfReal" ) ) {
      aReal = SALOMEDS::AttributeTableOfReal::_narrow( anAttr );
      SALOMEDS::StringSeq_var rowUnits = aReal->GetRowUnits();
      if ( rowUnits->length() > 0 && myHRow > 0 && myHRow <= aReal->GetNbRows() )
	units = rowUnits[ myHRow-1 ];
    }
  }
  return units;
}
/*!
  Returns ver.axis units
*/
string VISU::Curve_i::GetVerUnits()
{
  string units;
  // getting table SObject by it's entry
  SALOMEDS::SObject_var SO = GetStudyDocument()->FindObjectID(myTable->GetObjectEntry().c_str());
  SALOMEDS::StudyBuilder_var Builder = GetStudyDocument()->NewBuilder();
  SALOMEDS::GenericAttribute_var        anAttr;
  SALOMEDS::AttributeTableOfInteger_var anInt;
  SALOMEDS::AttributeTableOfReal_var    aReal;
  if ( !SO->_is_nil() ) {
    if ( Builder->FindAttribute( SO, anAttr, "AttributeTableOfInteger" ) ) {
      anInt = SALOMEDS::AttributeTableOfInteger::_narrow( anAttr );
      SALOMEDS::StringSeq_var rowUnits = anInt->GetRowUnits();
      if ( rowUnits->length() > 0 && myVRow > 0 && myVRow <= anInt->GetNbRows() )
	units = rowUnits[ myVRow-1];
    }
    else if ( Builder->FindAttribute( SO, anAttr, "AttributeTableOfReal" ) ) {
      aReal = SALOMEDS::AttributeTableOfReal::_narrow( anAttr );
      SALOMEDS::StringSeq_var rowUnits = aReal->GetRowUnits();
      if ( rowUnits->length() > 0 && myVRow > 0 && myVRow <= aReal->GetNbRows() )
	units = rowUnits[ myVRow-1 ];
    }
  }
  return units;
}
/*!
  Gets curve data
*/
int VISU::Curve_i::GetData( double*& theHorList, double*& theVerList, QStringList& zList )
{
  theHorList = 0; theVerList = 0;
  // getting table SObject by it's entry
  SALOMEDS::SObject_var SO = GetStudyDocument()->FindObjectID(myTable->GetObjectEntry().c_str());
  SALOMEDS::StudyBuilder_var Builder = GetStudyDocument()->NewBuilder();
  SALOMEDS::GenericAttribute_var        anAttr;
  SALOMEDS::AttributeTableOfInteger_var anInt;
  SALOMEDS::AttributeTableOfReal_var    aReal;

  QString tip = "%1: %2", z_data;

  if ( !SO->_is_nil() ) {
    if ( Builder->FindAttribute( SO, anAttr, "AttributeTableOfInteger" ) ) {
      anInt = SALOMEDS::AttributeTableOfInteger::_narrow( anAttr );
      int nbCols = anInt->GetNbColumns(), nbRows = anInt->GetNbRows();
      if ( nbCols > 0 && myHRow > 0 && myHRow <= anInt->GetNbRows() && myVRow > 0 && myVRow <= anInt->GetNbRows() ) {
	int nbPoints = 0;
	for ( int j = 1; j <= nbCols; j++ ) {
	  if ( anInt->HasValue( myHRow, j ) && anInt->HasValue( myVRow, j ) )
	    nbPoints++;
	}
	if ( nbPoints > 0 ) {
	  theHorList = new double[ nbPoints ];
	  theVerList = new double[ nbPoints ];
	  int k = 0;

	  SALOMEDS::StringSeq_var rowTitles = anInt->GetRowTitles();

	  for ( int j = 1; j <= nbCols; j++ ) {
	    if ( anInt->HasValue( myHRow, j ) && anInt->HasValue( myVRow, j ) ) {
	      theHorList[k] = anInt->GetValue( myHRow, j );
	      theVerList[k] = anInt->GetValue( myVRow, j );

	      z_data = tip.arg( GetHorTitle().c_str() ).arg( theHorList[k] ) + "\n";
	      z_data += tip.arg( GetVerTitle().c_str() ).arg( theVerList[k] );

	      if( myZRow>0 && myZRow<=nbRows && anInt->HasValue( myZRow, j ) )
	      {
		string title;
		title = rowTitles[ myZRow-1 ];
		z_data += "\n" + tip.arg( title.c_str() ).arg( anInt->GetValue( myZRow, j ) );
	      }
	      zList.append( z_data );
	      k++;
	    }
	  }
	}
	return nbPoints;
      }
    }
    else if ( Builder->FindAttribute( SO, anAttr, "AttributeTableOfReal" ) ) {
      aReal = SALOMEDS::AttributeTableOfReal::_narrow( anAttr );
      int nbCols = aReal->GetNbColumns(), nbRows = aReal->GetNbRows();
      if ( nbCols > 0 && myHRow > 0 && myHRow <= aReal->GetNbRows() && myVRow > 0 && myVRow <= aReal->GetNbRows() ) {
	int nbPoints = 0;
	for ( int j = 1; j <= nbCols; j++ ) {
	  if ( aReal->HasValue( myHRow, j ) && aReal->HasValue( myVRow, j ) )
	    nbPoints++;
	}
	if ( nbPoints > 0 ) {
	  theHorList = new double[ nbPoints ];
	  theVerList = new double[ nbPoints ];
	  int k = 0;

	  SALOMEDS::StringSeq_var rowTitles = aReal->GetRowTitles();

	  for ( int j = 1; j <= nbCols; j++ ) {
	    if ( aReal->HasValue( myHRow, j ) && aReal->HasValue( myVRow, j ) ) {
	      theHorList[k] = aReal->GetValue( myHRow, j );
	      theVerList[k] = aReal->GetValue( myVRow, j );

	      z_data = tip.arg( GetHorTitle().c_str() ).arg( theHorList[k] ) + "\n";
	      z_data += tip.arg( GetVerTitle().c_str() ).arg( theVerList[k] );

	      if( myZRow>0 && myZRow<=nbRows && aReal->HasValue( myZRow, j ) )
	      {
		string title;
		title = rowTitles[ myZRow-1 ];
		z_data += "\n" + tip.arg( title.c_str() ).arg( aReal->GetValue( myZRow, j ) );
	      }
	      zList.append( z_data );
	      k++;
	    }
	  }
	}
	return nbPoints;
      }
    }
  }
  return 0;
}
/*!
  Creates curve Plot2d presentation object
*/
SPlot2d_Curve* VISU::Curve_i::CreatePresentation()
{
  SPlot2d_Curve* crv = new SPlot2d_Curve();
  crv->setYAxis( myIsV2 ? QwtPlot::yRight : QwtPlot::yLeft );
  crv->setHorTitle( GetHorTitle().c_str() );
  string tlt = GetTitle();
  if ( tlt.length() <= 0 )
    tlt = GetVerTitle();
  //crv->setVerTitle( strdup( GetVerTitle().c_str() ) );
  //crv->setVerTitle( strdup( GetName() ) );
  crv->setVerTitle( tlt.c_str() );
  crv->setHorUnits( GetHorUnits().c_str() );
  crv->setVerUnits( GetVerUnits().c_str() );
  double* xList = 0;
  double* yList = 0;
  QStringList zList;
  int     nbPoints = GetData( xList, yList, zList );
  if ( nbPoints > 0 && xList && yList ) {
    crv->setData( xList, yList, nbPoints, zList );
  }
  //cout << "********** Number of points: " << nbPoints <<endl;
  //for ( int i =0 ; i < nbPoints; i++ ) {
  //  cout << i<<"\t"<<xList[i] << "\t"<< yList[i] << endl;
  //}
  crv->setLine( (Plot2d::LineType)GetLine(), GetLineWidth() );
  crv->setMarker( (Plot2d::MarkerType)GetMarker() );
  SALOMEDS::Color color = GetColor();
  crv->setColor( QColor( (int)(color.R*255.), (int)(color.G*255.), (int)(color.B*255.) ) );
  crv->setAutoAssign( IsAuto() );
  CORBA::String_var aString = mySObj->GetID();
  crv->setIO(new SALOME_InteractiveObject(aString.in(), "VISU", GetName().c_str()));
  if ( myTable )
    crv->setTableIO(new SALOME_InteractiveObject(myTable->GetObjectEntry().c_str(), "VISU", myTable->GetName().c_str()));

  if(!myContainers.isEmpty())     
    crv->addOwners(myContainers);
  return crv;
}
/*!
  Restores curve object from stream
*/
VISU::Storable* VISU::Curve_i::Restore( const Storable::TRestoringMap& theMap, SALOMEDS::SObject_ptr theSO)
{
  if(MYDEBUG) MESSAGE(GetComment());
  mySObj = SALOMEDS::SObject::_duplicate(theSO);
  SetName(VISU::Storable::FindValue(theMap,"myName").toLatin1().data(), false);
  myHRow = VISU::Storable::FindValue(theMap,"myHRow").toInt();
  myVRow = VISU::Storable::FindValue(theMap,"myVRow").toInt();
  bool ok = false;
  QString z_str = VISU::Storable::FindValue(theMap,"myZRow", &ok);
  myZRow = ok ? z_str.toInt() : 0;
  ok = false;
  QString v2_str = VISU::Storable::FindValue(theMap,"myIsV2", &ok);
  myIsV2 = ok ? v2_str.toInt() : false;

  myColor.R = VISU::Storable::FindValue(theMap,"myColor.R").toDouble();
  myColor.G = VISU::Storable::FindValue(theMap,"myColor.G").toDouble();
  myColor.B = VISU::Storable::FindValue(theMap,"myColor.B").toDouble();
  myMarker = ( VISU::Curve::MarkerType )( VISU::Storable::FindValue(theMap,"myMarker").toInt() );
  myLine = ( VISU::Curve::LineType )( VISU::Storable::FindValue(theMap,"myLine").toInt() );
  myLineWidth = VISU::Storable::FindValue(theMap,"myLineWidth").toInt();
  myAuto = VISU::Storable::FindValue(theMap,"myAuto").toInt();
  return Build( true );
}
/*!
  Flushes curve data into stream
*/
void VISU::Curve_i::ToStream( std::ostringstream& theStr )
{
  Storable::DataToStream( theStr, "myName",      GetName().c_str() );
  Storable::DataToStream( theStr, "myHRow",      myHRow );
  Storable::DataToStream( theStr, "myVRow",      myVRow );
  Storable::DataToStream( theStr, "myZRow",      myZRow );
  Storable::DataToStream( theStr, "myIsV2",      myIsV2 );
  Storable::DataToStream( theStr, "myColor.R",   myColor.R );
  Storable::DataToStream( theStr, "myColor.G",   myColor.G );
  Storable::DataToStream( theStr, "myColor.B",   myColor.B );
  Storable::DataToStream( theStr, "myMarker",    myMarker );
  Storable::DataToStream( theStr, "myLine",      myLine );
  Storable::DataToStream( theStr, "myLineWidth", myLineWidth );
  Storable::DataToStream( theStr, "myAuto",      myAuto );
}
/*!
  Gets reference table's entry
*/
std::string VISU::Curve_i::GetTableID() {
  return myTable->GetObjectEntry();
}
/*!
  Called from engine to restore curve from the file
*/
VISU::Storable* VISU::Curve_i::StorableEngine(SALOMEDS::SObject_ptr theSObject,
					      const Storable::TRestoringMap& theMap,
					      const std::string& thePrefix,
					      CORBA::Boolean theIsMultiFile)
{
  SALOMEDS::Study_var aStudy = theSObject->GetStudy();
  VISU::Table_i* pTable = GetTable(aStudy, theSObject->GetFather());
  if( pTable != NULL ) {
    VISU::Curve_i* pResent = new VISU::Curve_i( aStudy, pTable, 0, 0, 0, false );
    return pResent->Restore( theMap, theSObject);
  }
  return NULL;
}

void VISU::Curve_i::RemoveFromStudy()
{
  
  //remove curve from containers
  PortableServer::POA_ptr aPOA = GetPOA();
  ContainerSet::ConstIterator it = myContainers.begin();
  for ( ; it != myContainers.end(); it++ ) {    
    SALOMEDS::SObject_var aSObject =  GetStudyDocument()->FindObjectID( (*it).toLatin1().data() );
    if ( aSObject->_is_nil() ) continue;
    
    CORBA::Object_var anObj = VISU::SObjectToObject( aSObject );
    if ( CORBA::is_nil( anObj ) ) continue;
    VISU::Container_i* aContainer = dynamic_cast<VISU::Container_i*>( VISU::GetServant( anObj.in() ).in() );
    if ( !aContainer ) continue;
    aContainer->RemoveCurve(VISU::Curve::_narrow(aPOA->servant_to_reference(this)));
  }

  struct TRemoveFromStudy: public SALOME_Event
  {
    VISU::Curve_i* myRemovable;
    TRemoveFromStudy(VISU::Curve_i* theRemovable):
      myRemovable(theRemovable)
    {}
    
    virtual
    void
    Execute()
    {
      VISU::DeleteActors(myRemovable);
      VISU::RemoveFromStudy(myRemovable->GetSObject(),false);
    }
  };

  ProcessVoidEvent(new TRemoveFromStudy(this));
}

SALOMEDS::SObject_var VISU::Curve_i::GetSObject()
{
  return mySObj;
}


/*!
  Add container.
  id  - owner of the curve
*/
void VISU::Curve_i::addContainer(const QString& id) {
  myContainers.insert(id);
}

/*!
  Remove Container
  id  - entry of the container
*/
void VISU::Curve_i::removeContainer(const QString& id) {
  myContainers.insert(id);
}

/*!
  Get all owners of the curve.
  \return owners of the curve.
*/
VISU::ContainerSet VISU::Curve_i::getContainers() const {
  return myContainers;
}


//----------------------------------------------------------------
//                      Container Object
//----------------------------------------------------------------
int VISU::Container_i::myNbPresent = 0;
const string VISU::Container_i::myComment  = "CONTAINER";
/*!
  Generate unique name
*/
QString VISU::Container_i::GenerateName()
{
  return VISU::GenerateName( "Plot2DView", ++myNbPresent ).toLatin1().data();
}
/*!
  Gets comment string
*/
const char* VISU::Container_i::GetComment() const
{
  return myComment.c_str();
}
/*!
  Constructor
*/
VISU::Container_i::Container_i( SALOMEDS::Study_ptr theStudy )
     : PrsObject_i( theStudy )
{
}
/*!
  Destructor
*/
VISU::Container_i::~Container_i()
{
  MESSAGE("Container_i::~Container_i");
  myCurves.clear();
}
/*!
  Inserts curve into the container
*/
void VISU::Container_i::AddCurve( Curve_ptr theCurve )
{
  if ( GetStudyDocument()->_is_nil() )
    return;
  SALOMEDS::SObject_var mySO = GetStudyDocument()->FindObjectID( GetEntry().c_str() );
  if ( mySO->_is_nil() )
    return;
  PortableServer::POA_ptr aPOA = GetPOA();
  Curve_i* pCurve = dynamic_cast<Curve_i*>( aPOA->reference_to_servant( theCurve ) );
  if( pCurve ) {
    QString entry( pCurve->GetEntry().c_str() );
    SALOMEDS::SObject_var SO = GetStudyDocument()->FindObjectID( entry.toLatin1().data() );
    if ( !SO->_is_nil() && myCurves.indexOf( entry ) == -1 ) {
      myCurves.append( entry );
      SALOMEDS::StudyBuilder_var Builder = GetStudyDocument()->NewBuilder();
      SALOMEDS::SObject_var newSO = Builder->NewObject( mySO );
      Builder->Addreference( newSO, SO );
      pCurve->addContainer(GetEntry().c_str());
    }
  }
}
/*!
  Removes curve from the container
*/
void VISU::Container_i::RemoveCurve( Curve_ptr theCurve )
{
  if ( GetStudyDocument()->_is_nil() )
    return;
  SALOMEDS::SObject_var mySO = GetStudyDocument()->FindObjectID( GetEntry().c_str() );
  if ( mySO->_is_nil() )
    return;
  PortableServer::POA_ptr aPOA = GetPOA();
  Curve_i* pCurve = dynamic_cast<Curve_i*>( aPOA->reference_to_servant( theCurve ) );
  if( pCurve ) {
    QString entry( pCurve->GetEntry().c_str() );
    if ( myCurves.indexOf( entry ) != -1 ) {
      // found !!!
      myCurves.removeAll( entry );
      SALOMEDS::StudyBuilder_var Builder = GetStudyDocument()->NewBuilder();
      SALOMEDS::ChildIterator_var CI = GetStudyDocument()->NewChildIterator( mySO );
      for ( ; CI->More(); CI->Next() ) {
	SALOMEDS::SObject_var childSO = CI->Value();
	SALOMEDS::SObject_var refSO;
	if ( childSO->ReferencedObject( refSO ) && !refSO->_is_nil() && entry == QString( refSO->GetID() ) ) {
	  Builder->RemoveObject( childSO );
	}
      }
      pCurve->removeContainer(GetEntry().c_str());
    }
  }
}
/*!
  Gets number of curves in the container
*/
CORBA::Long VISU::Container_i::GetNbCurves()
{
  //  Update();
  return myCurves.count();
}
/*!
  Clears container
*/
void VISU::Container_i::Clear()
{
  if ( GetStudyDocument()->_is_nil() )
    return;
  SALOMEDS::SObject_var mySO = GetStudyDocument()->FindObjectID( GetEntry().c_str() );
  if ( mySO->_is_nil() )
    return;
  QStringList toDelete;
  SALOMEDS::ChildIterator_var CI = GetStudyDocument()->NewChildIterator( mySO );
  for ( ; CI->More(); CI->Next() ) {
    toDelete.append( CI->Value()->GetID() );
  }
  SALOMEDS::StudyBuilder_var Builder = GetStudyDocument()->NewBuilder();
  for ( int i = 0; i < toDelete.count(); i++ ) {
    SALOMEDS::SObject_var SO = GetStudyDocument()->FindObjectID( toDelete[i].toLatin1().data() );
    Builder->RemoveObject( SO );
  }
  myCurves.clear();
}
/*!
  Creates container object
*/
VISU::Storable* VISU::Container_i::Create()
{
  // generate name ...
  SetName(GenerateName().toLatin1().data(), false);
  // ... and build the object
  return Build( false );
}
/*!
  Builds presentation of container
*/
VISU::Storable* VISU::Container_i::Build( int theRestoring )
{
  if ( !theRestoring ) {
    // looking for component
    SALOMEDS::SComponent_var SComponent = VISU::FindOrCreateVisuComponent( GetStudyDocument() );
    // create SObject and set attributes
    QString aComment;
    aComment.sprintf("myComment=%s",GetComment());
    string anEntry = CreateAttributes( GetStudyDocument(),
				       SComponent->GetID(),
				       "",
				       GetID(),
				       GetName(),
				       "",
				       aComment.toLatin1().data(),
				       true );
    mySObj = SALOMEDS::SObject::_duplicate(GetStudyDocument()->FindObjectID(anEntry.c_str()));

    // Set icon
    SALOMEDS::StudyBuilder_var aStudyBuilder = GetStudyDocument()->NewBuilder();
    SALOMEDS::GenericAttribute_var anAttr;
    SALOMEDS::AttributePixMap_var  aPixmap;
    anAttr  = aStudyBuilder->FindOrCreateAttribute( mySObj, "AttributePixMap" );
    aPixmap = SALOMEDS::AttributePixMap::_narrow( anAttr );
    aPixmap ->SetPixMap("ICON_TREE_CONTAINER");
  }
  return this;
}
/*!
  Updates presentation of container
*/
void VISU::Container_i::Update()
{
  if ( GetStudyDocument()->_is_nil() )
    return;
  SALOMEDS::StudyBuilder_var Builder = GetStudyDocument()->NewBuilder();
  SALOMEDS::SObject_var mySO = GetStudyDocument()->FindObjectID( GetEntry().c_str() );
  SALOMEDS::GenericAttribute_var anAttr;
  if ( !mySO->_is_nil() ) {
    QStringList toDelete;
    int i;
    for ( i = 0; i < myCurves.count(); i++ ) {
      SALOMEDS::SObject_var SO = GetStudyDocument()->FindObjectID( myCurves[i].toLatin1().data() );
      if ( !SO->_is_nil() && Builder->FindAttribute( SO, anAttr, "AttributeIOR" ) ) {
	// if real Curve Object still exists
	SALOMEDS::ChildIterator_var CI = GetStudyDocument()->NewChildIterator( mySO );
	bool bFound = false;
	for ( ; CI->More(); CI->Next() ) {
	  SALOMEDS::SObject_var childSO = CI->Value();
	  SALOMEDS::SObject_var refSO;
	  if ( childSO->ReferencedObject( refSO ) && !refSO->_is_nil() && myCurves[i] == QString( refSO->GetID() ) ) {
	    bFound = true; break;
	  }
	}
	if (! bFound ) {
	  // create SObject referenced to real curve object if is not yet added
	  SALOMEDS::SObject_var newSO = Builder->NewObject( mySO );
	  Builder->Addreference( newSO, SO );
	}
      }
      else {
	// real Curve Object doesn't exist (might be removed)
	toDelete.append( myCurves[i] );
      }
    }
    for ( i = 0; i < toDelete.count(); i++ ) {
      myCurves.removeAll( toDelete[i] );
    }
    toDelete.clear();
    SALOMEDS::ChildIterator_var CI = GetStudyDocument()->NewChildIterator( mySO );
    for ( ; CI->More(); CI->Next() ) {
      SALOMEDS::SObject_var childSO = CI->Value();
      SALOMEDS::SObject_var refSO;
      if ( childSO->ReferencedObject( refSO ) && ( refSO->_is_nil() || !Builder->FindAttribute( refSO, anAttr, "AttributeIOR" ) ||
						   myCurves.indexOf( refSO->GetID() ) == -1 ) ) {
	toDelete.append( childSO->GetID() );
      }
    }
    for ( i = 0; i < toDelete.count(); i++ ) {
      SALOMEDS::ChildIterator_var CI = GetStudyDocument()->NewChildIterator( mySO );
      for ( ; CI->More(); CI->Next() ) {
	SALOMEDS::SObject_var childSO = CI->Value();
	if ( toDelete[i] == CI->Value()->GetID() ) {
	  Builder->RemoveObject( childSO );
	}
      }
    }
  }
}
/*!
  Gets curve from container by index
  NB : curves are numbered from 1
*/
VISU::Curve_i* VISU::Container_i::GetCurve( CORBA::Long theIndex )
{
  if ( theIndex > 0 && theIndex <= myCurves.count()  ) {
    SALOMEDS::StudyBuilder_var Builder = GetStudyDocument()->NewBuilder();
    SALOMEDS::GenericAttribute_var anAttr;
    SALOMEDS::SObject_var SO = GetStudyDocument()->FindObjectID(myCurves[ theIndex-1 ].toLatin1().data() );
    CORBA::Object_var anObject = VISU::SObjectToObject( SO );
    if( !CORBA::is_nil( anObject ) ) {
      // if real Curve Object exists
      CORBA::Object_ptr aCurve = VISU::Curve::_narrow( anObject );
      if( !CORBA::is_nil( aCurve ) )
      return dynamic_cast<VISU::Curve_i*>(VISU::GetServant(aCurve).in());
    }
  }
  return NULL;
}
/*!
  Restores container data from the stream
*/
VISU::Storable* VISU::Container_i::Restore( const Storable::TRestoringMap& theMap, SALOMEDS::SObject_ptr SO )
{
  if(MYDEBUG) MESSAGE(GetComment());
  mySObj = SALOMEDS::SObject::_duplicate(SO);
  SetName(VISU::Storable::FindValue( theMap, "myName" ).toLatin1().data(), false);
  QString val = VISU::Storable::FindValue( theMap, "myCurves" );
  myCurves = val.split( "*", QString::SkipEmptyParts );
  return Build( true );
}
/*!
  Flushes container data into the stream
*/
void VISU::Container_i::ToStream( std::ostringstream& theStr )
{
  Storable::DataToStream( theStr, "myName",   GetName().c_str() );
  Storable::DataToStream( theStr, "myCurves", myCurves.join( QString( "*" ) ) );
//  theStr<<" myName "<<myName;
//  theStr<<" myCurves "<<myCurves.join( QString( "*" ) ).latin1()<<"* ";
}
/*!
  Called from engine to restore container from the file
*/
VISU::Storable* VISU::Container_i::StorableEngine(SALOMEDS::SObject_ptr theSObject,
						  const Storable::TRestoringMap& theMap,
						  const std::string& thePrefix,
						  CORBA::Boolean theIsMultiFile)
{
  SALOMEDS::Study_var aStudy = theSObject->GetStudy();
  VISU::Container_i* pResent = new VISU::Container_i( aStudy );
  return pResent->Restore( theMap, theSObject );
}

void VISU::Container_i::RemoveFromStudy()
{
  struct TRemoveFromStudy: public SALOME_Event
  {
    VISU::Container_i* myRemovable;
    TRemoveFromStudy(VISU::Container_i* theRemovable):
      myRemovable(theRemovable)
    {}
    
    virtual
    void
    Execute()
    {
      VISU::RemoveFromStudy(myRemovable->GetSObject(),false);
    }
  };

  ProcessVoidEvent(new TRemoveFromStudy(this));
}

SALOMEDS::SObject_var VISU::Container_i::GetSObject()
{
  return mySObj;
}

SALOMEDS::SObject_var
VISU::ImportTables(const char* theFileName, SALOMEDS::Study_ptr theStudy,
                   bool theFirstStrAsTitle)
{
  // Set "C" numeric locale to import numbers correctly
  Kernel_Utils::Localizer loc;

  TTableContainer aContainer;
  ImportTables( theFileName, aContainer, theFirstStrAsTitle );
  if ( aContainer.empty() ) 
    return SALOMEDS::SObject::_nil();

  SALOMEDS::StudyBuilder_var aStudyBuilder = theStudy->NewBuilder();
  SALOMEDS::SComponent_var theSComponent = VISU::FindOrCreateVisuComponent(theStudy);
  SALOMEDS::SObject_var aFileObject = aStudyBuilder->NewObject(theSComponent);
  SALOMEDS::GenericAttribute_var anAttr =
    aStudyBuilder->FindOrCreateAttribute(aFileObject, "AttributeName");
  SALOMEDS::AttributeName_var aName = SALOMEDS::AttributeName::_narrow(anAttr);
  QFileInfo aFileInfo(theFileName);
  aName->SetValue( aFileInfo.fileName().toLatin1().data());
  anAttr = aStudyBuilder->FindOrCreateAttribute(aFileObject, "AttributeString");
  SALOMEDS::AttributeString_var aComment = SALOMEDS::AttributeString::_narrow(anAttr);
  QString aString;
  aString.sprintf("myComment=ImportTables;myFileName=%s;myFirstStrAsTitle=%d",
		  aFileInfo.absoluteFilePath().toLatin1().data(),theFirstStrAsTitle);
  aComment->SetValue(aString.toLatin1().data());
  for(int i = 0, iEnd = aContainer.size(); i < iEnd; i++) {
    PTableIDMapper aTableIDMapper = aContainer[i];
    const TTable2D& aTable2D = *aTableIDMapper;
    SALOMEDS::SObject_var aRealObject = aStudyBuilder->NewObject(aFileObject);
    anAttr = aStudyBuilder->FindOrCreateAttribute(aRealObject, "AttributeName");
    aName = SALOMEDS::AttributeName::_narrow(anAttr);
    if(MYDEBUG) MESSAGE("aTable2D.myTitle = "<<aTable2D.myTitle);
    if ( aTable2D.myTitle != "" ) {
      aName->SetValue(aTable2D.myTitle.c_str());
    }
    else {
      QString aNewName;
      aNewName.sprintf("Table:%d",i);
      aName->SetValue(aNewName.toLatin1().data());
    }

    anAttr = aStudyBuilder->FindOrCreateAttribute(aRealObject, "AttributeTableOfReal");
    SALOMEDS::AttributeTableOfReal_var aTableOfReal = SALOMEDS::AttributeTableOfReal::_narrow(anAttr);
    aTableOfReal->SetTitle(aTable2D.myTitle.c_str());
    TTable2D aNewTable2D;
    aTable2D.getColumns(aNewTable2D);
    int kEnd = aNewTable2D.myRows[0].myValues.size();
    // check empty columns
    TColStd_MapOfInteger EmptyColumns;
    for(int j = 0, jEnd = aNewTable2D.myRows.size(); j < jEnd; j++) {
      bool hasVal = false;
      for(int k = 0; k < kEnd; k++) {
	QString aVal = aNewTable2D.myRows[j].myValues[k].c_str();
	bool anIsOk = false;
	double aValue = aVal.toDouble(&anIsOk);
        if(anIsOk) {
          hasVal = true;
          break;
        }
      }
      if(!hasVal) {
        EmptyColumns.Add(j);
      }
    }
    // create table of real
    aTableOfReal->SetNbColumns( kEnd - EmptyColumns.Extent() );
    int currNum = -1;
    for(int j = 0, jEnd = aNewTable2D.myRows.size(); j < jEnd; j++) {
      if( EmptyColumns.Contains(j) ) continue;
      currNum++;
      if(MYDEBUG) MESSAGE("j = "<<j<<"; kEnd = "<<kEnd);
      for(int k = 0; k < kEnd; k++) {
	QString aVal = aNewTable2D.myRows[j].myValues[k].c_str();
	bool anIsOk = false;
	double aValue = aVal.toDouble(&anIsOk);
	if( anIsOk && !aVal.contains("NAN",Qt::CaseInsensitive) &&
            !aVal.contains("INF",Qt::CaseInsensitive) ) {
	  aTableOfReal->PutValue(aValue,currNum+1,k+1);
        }
      }
      aTableOfReal->SetRowTitle(currNum+1,aNewTable2D.myRows[j].myTitle.c_str());
      aTableOfReal->SetRowUnit(currNum+1,aNewTable2D.myRows[j].myUnit.c_str());
    }
    for(int k = 0; k < kEnd; k++)
      aTableOfReal->SetColumnTitle(k+1,aNewTable2D.myColumnTitles[k].c_str());
  }
  return aFileObject;
}


//=======================================================================
//function : updateStrForCSV
//purpose  : auxilary for ExportTableToFile
//=======================================================================
void updateStrForCSV(QString& aStr, const char aSep)
{
  int index = aStr.indexOf('"');
  while(index>=0) {
    aStr.insert(index,'"');
    if( index+2 >= aStr.size() ) break;
    index = aStr.indexOf('"',index+2);
  }
  index = aStr.indexOf(aSep);
  if(index>=0) {
    // current string contains separator => need to use "..."
    aStr.insert(0,'"');
    aStr.push_back('"');
  }
}


//=======================================================================
//function : ExportTableToFile
//purpose  : 
//=======================================================================
template<class TTableAttr> bool ExportTableToFile(const TTableAttr& aTabAttr,
						  const char* theFileName)
{
  if (CORBA::is_nil(aTabAttr))
    return false;

  // Set "C" numeric locale to save numbers correctly
  Kernel_Utils::Localizer loc;

  QFile aFile(theFileName);
  aFile.open(QIODevice::WriteOnly);

  /* extract the table info and write it into file */

  QString aTitle(aTabAttr->GetTitle()); /*Table title*/
  int aRowsNb = aTabAttr->GetNbRows();
  int aColNb  = aTabAttr->GetNbColumns();

  SALOMEDS::StringSeq_var aRowTitles = aTabAttr->GetRowTitles();
  SALOMEDS::StringSeq_var aRowUnits = aTabAttr->GetRowUnits();
  SALOMEDS::StringSeq_var aColumnTitles = aTabAttr->GetColumnTitles();

  //--------------------------------------------------
  //    write as *.csv file if it is needed
  //--------------------------------------------------
  QString tmp(theFileName);
  tmp = tmp.trimmed();
  tmp = tmp.right(3).trimmed();
  if( tmp == QString("csv") ) {
    const char aSep = ',';
    // write column titles
    QString aLine(aRowTitles[0]);
    updateStrForCSV(aLine,aSep);
    for(int i=1; i<aRowsNb; i++) {
      aLine += aSep;
      QString aTmp(aRowTitles[i]);
      updateStrForCSV(aTmp,aSep);
      aLine += aTmp;
    }
    aLine += "\n";
    aFile.write(aLine.toLatin1() );
    // write table data
    QString aValue;
    for (int j = 1; j <= aColNb; j++) {
      QString aLine = "";
      if(aTabAttr->HasValue(j,1)) {
        aLine = aValue.sprintf("%.16g",(double)aTabAttr->GetValue(1,j));
      }
      for (int i = 2; i <= aRowsNb; i++) {
        if(aTabAttr->HasValue(i,j)) {
          aLine += aSep + aValue.sprintf("%.16g",(double)aTabAttr->GetValue(i,j));
        }
        else aLine += aSep;
      }
      aLine += "\n";
      aFile.write(aLine.toLatin1() );
    }

    aFile.close();
    return true;
  }
  //--------------------------------------------------
  //       end of writing as *.csv file
  //--------------------------------------------------

  /* The given table is rare (some cells is empty) or not? */
  bool isRareTable = false;
  for (int i = 1; i <= aRowsNb; i++)
    for (int j = 1; j <= aColNb && !isRareTable; j++)
      isRareTable = !aTabAttr->HasValue(i,j);

  QString aLine;
  if (isRareTable) {
    /* Separate the given table to 2D tables and write these ones to the file */
    QString anAbscissTitle(aRowTitles[0]); /*Absciss row title (X coord)*/
    anAbscissTitle.trimmed();
    QString anAbscissUnit(aRowUnits[0]);
    anAbscissUnit.trimmed();
    if (aRowsNb > 2 && aTitle.length() )  aTitle = aTitle + " - ";

    for (int i = 2; i <= aRowsNb; i++ )
      {
	/* TITLE */
	QString anOrdinate(aRowTitles[i-1]), aTail;
	anOrdinate.trimmed();

	aLine = "#TITLE: " + aTitle +
	  ((anOrdinate.length())?  anOrdinate :
	                          (aRowsNb>2)? aTail.sprintf("%d",i-1) : aTail.sprintf("") ) + "\n";
	aFile.write(aLine.toLatin1() );

	/* COLUMN_TITLES */
	if ( anAbscissTitle.length() || anOrdinate.length() ) {
	  aLine = "#COLUMN_TITLES: " + anAbscissTitle + " | " + anOrdinate;
          int tmpind = aLine.indexOf("\n");
          while(tmpind>=0) {
            aLine.remove(tmpind,1);
            tmpind = aLine.indexOf("\n");
          }
	  aLine += "\n";
	  aFile.write(aLine.toLatin1() );
	}

	/* COLUMN_UNITS */
	aLine = anAbscissUnit + " " +aRowUnits[i-1];
	if (!aLine.trimmed().isEmpty()) {
	  aLine = "#COLUMN_UNITS: " + aLine  + "\n";
	  aFile.write(aLine.toLatin1() );
	}

	/* CURVE COORDINATES */
	for (int j = 1; j <= aColNb; j++)
	  {
	    if ( aTabAttr -> HasValue(i,j) &&  aTabAttr -> HasValue(1, j)) {
	      aLine = aLine.sprintf("%.16g %.16g",
				    (double)(aTabAttr->GetValue(1,j)),
				    (double)(aTabAttr->GetValue(i,j)));  /* aTabAttr->GetValue(1,j) - X coord */
	      if ( !aLine.trimmed().isEmpty() ) {
		QString aColTitle(aColumnTitles[j-1]);
		if ( !aColTitle.trimmed().isEmpty() )
		  aLine = aLine + "  #TITLE: " + aColTitle ;
		aFile.write(QString(aLine + "\n").toLatin1() );
	      }
	    }
	  }
	aFile.write("\n", 1);
      }
  }//end of if (isRareTable)
  else {
    /* Write the table in the file without separating */
    /* TITLE */
    aLine = "#TITLE: " + aTitle + "\n";
    aFile.write(aLine.toLatin1());

    /* COLUMN_TITLES  and COLUMN_UNITS */
    QString aTitlesSep = "";
    QString aUnitsSep  = "";
    QString aTitlesStr = "#COLUMN_TITLES: ";
    QString aUnitsStr  = "#COLUMN_UNITS: ";
    for (int i = 1; i <= aRowsNb; i++) {
      if (!QString(aRowTitles[i-1]).trimmed().isEmpty()) {
        aTitlesStr += (aTitlesSep + aRowTitles[i-1]);
        if (aTitlesSep.isEmpty()) aTitlesSep = " | ";
      }
      if (!QString(aRowUnits[i-1]).trimmed().isEmpty()) {
        aUnitsStr += (aUnitsSep + aRowUnits[i-1]);
        if (aUnitsSep.isEmpty()) aUnitsSep = " ";
      }
    }
    int tmpind = aTitlesStr.indexOf("\n");
    while(tmpind>=0) {
      aTitlesStr.remove(tmpind,1);
      tmpind = aTitlesStr.indexOf("\n");
    }
    aTitlesStr += "\n";
    aUnitsStr  += "\n";
    aFile.write(aTitlesStr.toLatin1());
    aFile.write(aUnitsStr.toLatin1());

    /* CURVE COORDINATES */
    QString aSep, aValue, aColTitle;
    for (int j = 1; j <= aColNb; j++) {
      aLine = ""; aSep  = "";
      for (int i = 1; i <= aRowsNb; i++) {
        aLine += (aSep + aValue.sprintf("%.16g", (double)(aTabAttr->GetValue(i,j))));
        if (aSep.isEmpty()) aSep = " ";
      }
      if (!aLine.trimmed().isEmpty()) {
        aColTitle = aColumnTitles[j-1];
        if (!aColTitle.trimmed().isEmpty())
          aLine = aLine + "  #TITLE: " + aColTitle;
        aLine += "\n";
        aFile.write(aLine.toLatin1());
      }
    }
  } //end of else

  aFile.close();
  return true;
}

bool VISU::ExportTableToFile(SALOMEDS::SObject_ptr theTable, const char* theFileName)
{
  //Find table
  SALOMEDS::GenericAttribute_var anAttr ;
  if (theTable->FindAttribute(anAttr, "AttributeTableOfReal")) {
    SALOMEDS::AttributeTableOfReal_var aTabAttr =
      SALOMEDS::AttributeTableOfReal ::_narrow(anAttr);
    return ExportTableToFile ( aTabAttr , theFileName);
  }
  else if (theTable->FindAttribute(anAttr, "AttributeTableOfInteger")) {
    SALOMEDS::AttributeTableOfInteger_var aTabAttr =
      SALOMEDS::AttributeTableOfInteger ::_narrow(anAttr);
    return ExportTableToFile ( aTabAttr , theFileName);
  }
  return false;
}
