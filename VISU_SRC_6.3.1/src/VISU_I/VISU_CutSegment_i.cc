// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  File   : VISU_CutSegment_i.cc
//  Author : Oleg UVAROV
//  Module : VISU
//
#include "VISU_CutSegment_i.hh"
#include "VISU_Prs3dUtils.hh"
#include "VISU_Result_i.hh"

#include "VISU_Actor.h"
#include "VISU_CutSegmentPL.hxx"
#include "VISU_Convertor.hxx"
#include "VISU_PipeLineUtils.hxx"

#include "SUIT_ResourceMgr.h"
#include "SALOME_Event.h"

#include <vtkAppendPolyData.h>

#ifdef _DEBUG_
static int MYDEBUG = 0;
#else
static int MYDEBUG = 0;
#endif

using namespace std;

//---------------------------------------------------------------
size_t
VISU::CutSegment_i
::IsPossible(Result_i* theResult, 
	     const std::string& theMeshName, 
	     VISU::Entity theEntity, 
	     const std::string& theFieldName, 
	     CORBA::Long theTimeStampNumber, 
	     bool theIsMemoryCheck)
{
  return TSuperClass::IsPossible(theResult,
				 theMeshName,
				 theEntity,
				 theFieldName,
				 theTimeStampNumber,
				 theIsMemoryCheck);
}

//---------------------------------------------------------------
int VISU::CutSegment_i::myNbPresent = 0;

//---------------------------------------------------------------
QString 
VISU::CutSegment_i::GenerateName() 
{ 
  return VISU::GenerateName("CutSegment",myNbPresent++);
}

//---------------------------------------------------------------
const string VISU::CutSegment_i::myComment = "CUTSEGMENT";

//---------------------------------------------------------------
const char* 
VISU::CutSegment_i
::GetComment() const 
{ 
  return myComment.c_str();
}

//----------------------------------------------------------------------------
const char*
VISU::CutSegment_i
::GetIconName()
{
  if (!IsGroupsUsed())
    return "ICON_TREE_CUT_SEGMENT";
  else
    return "ICON_TREE_CUT_SEGMENT_GROUPS";
}

//---------------------------------------------------------------
VISU::CutSegment_i::
CutSegment_i(EPublishInStudyMode thePublishInStudyMode) :
  ColoredPrs3d_i(thePublishInStudyMode),
  ScalarMap_i(thePublishInStudyMode),
  CutLinesBase_i(thePublishInStudyMode),
  myCutSegmentPL(NULL)
{}


//---------------------------------------------------------------
void
VISU::CutSegment_i
::SameAs(const Prs3d_i* theOrigin)
{
  TSuperClass::SameAs(theOrigin);
}


//---------------------------------------------------------------
VISU::Storable* 
VISU::CutSegment_i
::Create(const std::string& theMeshName, 
	 VISU::Entity theEntity,
	 const std::string& theFieldName, 
	 CORBA::Long theTimeStampNumber)
{
  return TSuperClass::Create(theMeshName,theEntity,theFieldName,theTimeStampNumber);
}


//---------------------------------------------------------------
VISU::Storable* 
VISU::CutSegment_i
::Restore(SALOMEDS::SObject_ptr theSObject,
	  const Storable::TRestoringMap& theMap)
{
  if(!TSuperClass::Restore(theSObject, theMap))
    return NULL;

  SetPoint1(VISU::Storable::FindValue(theMap,"myPoint1[0]").toDouble(),
	    VISU::Storable::FindValue(theMap,"myPoint1[1]").toDouble(),
	    VISU::Storable::FindValue(theMap,"myPoint1[2]").toDouble());
  SetPoint2(VISU::Storable::FindValue(theMap,"myPoint2[0]").toDouble(),
	    VISU::Storable::FindValue(theMap,"myPoint2[1]").toDouble(),
	    VISU::Storable::FindValue(theMap,"myPoint2[2]").toDouble());

  return this;
}


//---------------------------------------------------------------
void
VISU::CutSegment_i
::ToStream(std::ostringstream& theStr)
{
  TSuperClass::ToStream(theStr);

  double aCoord[3];
  GetPoint1( aCoord[0], aCoord[1], aCoord[2] );
  Storable::DataToStream( theStr, "myPoint1[0]",       aCoord[0] );
  Storable::DataToStream( theStr, "myPoint1[1]",       aCoord[1] );
  Storable::DataToStream( theStr, "myPoint1[2]",       aCoord[2] );

  GetPoint2( aCoord[0], aCoord[1], aCoord[2] );
  Storable::DataToStream( theStr, "myPoint2[0]",       aCoord[0] );
  Storable::DataToStream( theStr, "myPoint2[1]",       aCoord[1] );
  Storable::DataToStream( theStr, "myPoint2[2]",       aCoord[2] );
}


//---------------------------------------------------------------
VISU::CutSegment_i
::~CutSegment_i()
{
  if(MYDEBUG) MESSAGE("CutSegment_i::~CutSegment_i()");
}


//---------------------------------------------------------------
struct TSetPoint1Event: public SALOME_Event 
{
  VISU_CutSegmentPL* myCutSegmentPL;
  CORBA::Double myX, myY, myZ;
  TSetPoint1Event(VISU_CutSegmentPL* theCutSegment, 
	 CORBA::Double theX, 
	 CORBA::Double theY,
	 CORBA::Double theZ):
    myCutSegmentPL(theCutSegment), 
    myX(theX), 
    myY(theY),
    myZ(theZ)
  {}

  virtual
  void
  Execute()
  {
    myCutSegmentPL->SetPoint1(myX, myY, myZ);
  }
};

void
VISU::CutSegment_i
::SetPoint1(CORBA::Double theX,
	    CORBA::Double theY,
	    CORBA::Double theZ)
{
  VISU::TSetModified aModified(this);

  ProcessVoidEvent(new TSetPoint1Event(myCutSegmentPL, theX, theY, theZ));
}


//---------------------------------------------------------------
void
VISU::CutSegment_i
::GetPoint1(CORBA::Double& theX,
	    CORBA::Double& theY,
	    CORBA::Double& theZ)
{
  myCutSegmentPL->GetPoint1(theX, theY, theZ);
}


//---------------------------------------------------------------
struct TSetPoint2Event: public SALOME_Event 
{
  VISU_CutSegmentPL* myCutSegmentPL;
  CORBA::Double myX, myY, myZ;
  TSetPoint2Event(VISU_CutSegmentPL* theCutSegment, 
	 CORBA::Double theX, 
	 CORBA::Double theY,
	 CORBA::Double theZ):
    myCutSegmentPL(theCutSegment), 
    myX(theX), 
    myY(theY),
    myZ(theZ)
  {}

  virtual
  void
  Execute()
  {
    myCutSegmentPL->SetPoint2(myX, myY, myZ);
  }
};

void
VISU::CutSegment_i
::SetPoint2(CORBA::Double theX,
	    CORBA::Double theY,
	    CORBA::Double theZ)
{
  VISU::TSetModified aModified(this);

  ProcessVoidEvent(new TSetPoint2Event(myCutSegmentPL, theX, theY, theZ));
}


//---------------------------------------------------------------
void
VISU::CutSegment_i
::GetPoint2(CORBA::Double& theX,
	    CORBA::Double& theY,
	    CORBA::Double& theZ)
{
  myCutSegmentPL->GetPoint2(theX, theY, theZ);
}


//---------------------------------------------------------------
void
VISU::CutSegment_i
::CreatePipeLine(VISU_PipeLine* thePipeLine)
{
  if(!thePipeLine){
    myCutSegmentPL = VISU_CutSegmentPL::New();
  }else
    myCutSegmentPL = dynamic_cast<VISU_CutSegmentPL*>(thePipeLine);

  TSuperClass::CreatePipeLine(myCutSegmentPL);
}


//----------------------------------------------------------------------------
bool
VISU::CutSegment_i
::CheckIsPossible() 
{
  return IsPossible(GetCResult(),GetCMeshName(),GetEntity(),GetCFieldName(),GetTimeStampNumber(),true);
}


//---------------------------------------------------------------
VISU_Actor* 
VISU::CutSegment_i
::CreateActor()
{
  if(VISU_Actor* anActor = TSuperClass::CreateActor()){
    anActor->SetVTKMapping(true);
    SUIT_ResourceMgr* aResourceMgr = VISU::GetResourceMgr();
    int aDispMode = aResourceMgr->integerValue("VISU", "cut_segment_represent", 2);
    anActor->SetRepresentation(aDispMode);
    return anActor;
  }
  return NULL;
}
