// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : VISU_PrsObject_i.hxx
//  Author : Alexey PETROV
//  Module : VISU
//
#ifndef VISU_CutLines_i_HeaderFile
#define VISU_CutLines_i_HeaderFile

#include "VISU_I.hxx"
#include "VISU_CutLinesBase_i.hh"

class VISU_CutLinesPL;

namespace VISU
{
  //----------------------------------------------------------------------------
  class VISU_I_EXPORT CutLines_i : public virtual POA_VISU::CutLines,
                                   public virtual CutLinesBase_i
  {
    static int myNbPresent;
    CutLines_i(const CutLines_i&);

  public:
    //----------------------------------------------------------------------------
    typedef CutLinesBase_i TSuperClass;
    typedef VISU::CutLines TInterface;

    explicit
    CutLines_i(EPublishInStudyMode thePublishInStudyModep);

    virtual
    ~CutLines_i();

    virtual
    VISU::VISUType
    GetType() 
    {
      return VISU::TCUTLINES;
    }

    virtual
    void
    SetOrientation(VISU::CutPlanes::Orientation theOrient,
		   CORBA::Double theXAngle, 
		   CORBA::Double theYAngle);
    
    virtual
    void
    SetOrientation2(VISU::CutPlanes::Orientation theOrient,
		    CORBA::Double theXAngle, 
		    CORBA::Double theYAngle);

    virtual 
    VISU::CutPlanes::Orientation 
    GetOrientationType();

    virtual
    VISU::CutPlanes::Orientation 
    GetOrientationType2();

    virtual 
    CORBA::Double 
    GetRotateX();

    virtual
    CORBA::Double 
    GetRotateY();

    virtual
    CORBA::Double 
    GetRotateX2();

    virtual 
    CORBA::Double 
    GetRotateY2();

    virtual
    void 
    SetDisplacement(CORBA::Double theDisp);

    virtual
    CORBA::Double 
    GetDisplacement();

    virtual
    void 
    SetDisplacement2(CORBA::Double theDisp);

    virtual
    CORBA::Double 
    GetDisplacement2();

    virtual
    void 
    SetBasePlanePosition(CORBA::Double thePlanePosition);

    virtual
    CORBA::Double 
    GetBasePlanePosition();

    virtual
    void 
    SetLinePosition(CORBA::Long thePlaneNumber, 
		    CORBA::Double thePlanePosition);

    virtual
    CORBA::Double
    GetLinePosition(CORBA::Long thePlaneNumber);

    virtual
    void 
    SetDefault();

    virtual 
    CORBA::Boolean 
    IsDefault();

    virtual
    void 
    SetDefaultPosition(CORBA::Long thePlaneNumber);

    virtual
    CORBA::Boolean 
    IsDefaultPosition(CORBA::Long thePlaneNumber);

    VISU_CutLinesPL* 
    GetSpecificPL() const
    { 
      return myCutLinesPL; 
    }
    
  protected:
    //! Extends VISU_ColoredPrs3d_i::CreatePipeLine
    virtual 
    void
    CreatePipeLine(VISU_PipeLine* thePipeLine);

    //! Extends VISU_ColoredPrs3d_i::CheckIsPossible
    virtual 
    bool 
    CheckIsPossible();

    virtual 
    const char* 
    GetIconName();

    VISU_CutLinesPL *myCutLinesPL;

  public:
    //! Extends VISU_ColoredPrs3d_i::IsPossible
    static
    size_t
    IsPossible(Result_i* theResult, 
	       const std::string& theMeshName, 
	       VISU::Entity theEntity,
	       const std::string& theFieldName, 
	       CORBA::Long theTimeStampNumber,
	       bool theIsMemoryCheck);

    //! Extends VISU_ColoredPrs3d_i::Create
    virtual 
    Storable* 
    Create(const std::string& theMeshName, 
	   VISU::Entity theEntity,
	   const std::string& theFieldName, 
	   CORBA::Long theTimeStampNumber);

    //! Extends VISU_ColoredPrs3d_i::ToStream
    virtual 
    void
    ToStream(std::ostringstream& theStr);

    //! Extends VISU_ColoredPrs3d_i::Restore
    virtual
    Storable* 
    Restore(SALOMEDS::SObject_ptr theSObject,
	    const Storable::TRestoringMap& theMap);

    virtual 
    void
    SameAs(const Prs3d_i* theOrigin);

    //! Extends VISU_ColoredPrs3d_i::CreateActor
    virtual 
    VISU_Actor* 
    CreateActor();

    static const std::string myComment;

    virtual
    const char* 
    GetComment() const;

    virtual
    QString 
    GenerateName();
  };
}

#endif
