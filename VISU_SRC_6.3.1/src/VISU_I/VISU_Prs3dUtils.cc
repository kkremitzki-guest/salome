// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : VISU_Prs3dUtils.cc
//  Author : Alexey PETROV
//  Module : VISU
//
#include "VISU_Prs3dUtils.hh"
#include "VISU_Prs3d_i.hh"
#include "VISU_PointMap3d_i.hh"
#include "SalomeApp_Study.h"
#include "SALOME_Event.h"


namespace VISU
{
  //----------------------------------------------------------------------------
  TSetModified
  ::TSetModified(VISU::PrsObject_i* thePrsObject):
    myPrsObject(thePrsObject)
  {
    this->Modified();
  }


  //----------------------------------------------------------------------------
  TSetModified
  ::~TSetModified()
  {
    struct TEvent: public SALOME_Event
    {
      VISU::TSetModified* mySetModified;
      TEvent(VISU::TSetModified* theSetModified):
	mySetModified(theSetModified)
      {}
    
      virtual
      void
      Execute()
      {
	VISU::PrsObject_i* aPrsObject = mySetModified->myPrsObject;
	if(!aPrsObject)
		return;

	VISU::Prs3d_i* aPrs3d;
	VISU::PointMap3d_i* aPntMap;
	SalomeApp_Study* aStudy;
	unsigned long int time;
	if( (aPrs3d = dynamic_cast<VISU::Prs3d_i*>(aPrsObject)) && aPrs3d->GetActorEntry() != "" ) {
		aStudy = aPrs3d->GetGUIStudy();
		time = aPrs3d->GetMTime();
	} else if ( aPntMap = dynamic_cast<VISU::PointMap3d_i*>(aPrsObject) ) {
		aStudy = aPntMap->GetGUIStudy();
		time = aPntMap->GetMTime();
	} else 
		return;
	
	if(time > mySetModified->GetMTime()){
	  if(aStudy)
	    aStudy->Modified();
	}
	  }
	};

    ProcessVoidEvent(new TEvent(this));
  }

  //----------------------------------------------------------------------------

  std::string ToFormat( const int thePrec )
  {
    // "%-#6.3g"
    char str[ 255 ];
    sprintf( str, "%%-#.%dg", thePrec );
    return str;
  }

  //----------------------------------------------------------------------------

  int ToPrecision( const char* theFormat )
  {
    int N = strlen( theFormat );
    int k = -1;
    char str[ 255 ];
    bool isOk = false;
    for ( int i = 0; i < N; i++ )
    {
      if ( theFormat[ i ] ==  '.' )
        k = 0;
      else if ( theFormat[ i ] == 'g' )
      {
        str[ k ] = 0;
        isOk = true;
        break;
      }
      else if ( k >= 0 )
        str[ k++ ] = theFormat[ i ];
    }

    int res = 0;
    if ( isOk )
      res = atoi( str );

    return res;
  }
};

















