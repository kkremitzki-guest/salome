// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : VISU_PrsObject_i.hxx
//  Author : Alexey PETROV
//  Module : VISU
//
#ifndef VISU_Vectors_i_HeaderFile
#define VISU_Vectors_i_HeaderFile

#include "VISU_DeformedShape_i.hh"

class VISU_VectorsPL;

namespace VISU
{
  //----------------------------------------------------------------------------
  class VISU_I_EXPORT Vectors_i : public virtual POA_VISU::Vectors,
				  public virtual DeformedShape_i
  {
    static int myNbPresent;
    Vectors_i(const Vectors_i&);

  public:
    //----------------------------------------------------------------------------
    typedef DeformedShape_i TSuperClass;
    typedef VISU::Vectors TInterface;

    explicit
    Vectors_i(EPublishInStudyMode thePublishInStudyModep);

    virtual
    void
    SameAs(const Prs3d_i* theOrigin);

    virtual
    ~Vectors_i();

    virtual
    VISU::VISUType 
    GetType() 
    { 
      return VISU::TVECTORS;
    }

    virtual
    void 
    SetLineWidth(CORBA::Double theWidth);

    virtual
    CORBA::Double
    GetLineWidth();

    virtual 
    void 
    SetGlyphType(VISU::Vectors::GlyphType theType);

    virtual
    VISU::Vectors::GlyphType 
    GetGlyphType();

    virtual 
    void 
    SetGlyphPos(VISU::Vectors::GlyphPos thePos);

    virtual 
    VISU::Vectors::GlyphPos 
    GetGlyphPos();

    VISU_VectorsPL* 
    GetSpecificPL() const
    { 
      return myVectorsPL; 
    }
    
  protected:
    //! Redefines VISU_ColoredPrs3d_i::CreatePipeLine
    virtual
    void
    CreatePipeLine(VISU_PipeLine* thePipeLine);

    //! Redefines VISU_ColoredPrs3d_i::CheckIsPossible
    virtual 
    bool 
    CheckIsPossible();

    virtual
    VISU_PipeLine* 
    GetActorPipeLine();

    VISU_VectorsPL *myVectorsPL;
    float myLineWidth;

  public:
    //! Redefines VISU_ColoredPrs3d_i::IsPossible
    static 
    size_t 
    IsPossible(Result_i* theResult, 
	       const std::string& theMeshName, 
	       VISU::Entity theEntity,
	       const std::string& theFieldName, 
	       CORBA::Long theTimeStampNumber,
	       bool theIsMemoryCheck);

    //! Redefines VISU_ColoredPrs3d_i::Create
    virtual
    Storable* 
    Create(const std::string& theMeshName, 
	   VISU::Entity theEntity,
	   const std::string& theFieldName, 
	   CORBA::Long theTimeStampNumber);

    //! Redefines VISU_ColoredPrs3d_i::ToStream
    virtual
    void
    ToStream(std::ostringstream& theStr);

    //! Redefines VISU_ColoredPrs3d_i::Restore
    virtual
    Storable* 
    Restore(SALOMEDS::SObject_ptr theSObject,
	    const Storable::TRestoringMap& theMap);

    static const std::string myComment;

    //! Redefines VISU_ColoredPrs3d_i::GetComment
    virtual
    const char* 
    GetComment() const;

    //! Redefines VISU_ColoredPrs3d_i::GenerateName
    virtual
    QString
    GenerateName();

    virtual
    const char* 
    GetIconName();

    //! Redefines VISU_ColoredPrs3d_i::CreateActor
    virtual
    VISU_Actor* 
    CreateActor();

    //! Redefines VISU_ColoredPrs3d_i::UpdateActor
    virtual
    void
    UpdateActor(VISU_ActorBase* theActor) ;
  };
}

#endif
