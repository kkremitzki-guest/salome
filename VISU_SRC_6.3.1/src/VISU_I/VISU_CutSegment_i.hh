// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  File   : VISU_CutSegment_i.hh
//  Author : Oleg UVAROV
//  Module : VISU
//
#ifndef VISU_CutSegment_i_HeaderFile
#define VISU_CutSegment_i_HeaderFile

#include "VISU_I.hxx"
#include "VISU_CutLinesBase_i.hh"

class VISU_CutSegmentPL;

namespace VISU
{
  //----------------------------------------------------------------------------
  class VISU_I_EXPORT CutSegment_i : public virtual POA_VISU::CutSegment,
                                     public virtual CutLinesBase_i
  {
    static int myNbPresent;
    CutSegment_i(const CutSegment_i&);

  public:
    //----------------------------------------------------------------------------
    typedef CutLinesBase_i TSuperClass;
    typedef VISU::CutSegment TInterface;

    explicit
    CutSegment_i(EPublishInStudyMode thePublishInStudyModep);

    virtual
    ~CutSegment_i();

    virtual
    VISU::VISUType
    GetType() 
    {
      return VISU::TCUTSEGMENT;
    }

    virtual
    void
    SetPoint1(CORBA::Double theX,
	      CORBA::Double theY,
	      CORBA::Double theZ );

    virtual
    void
    GetPoint1(CORBA::Double& theX,
	      CORBA::Double& theY,
	      CORBA::Double& theZ );

    virtual
    void
    SetPoint2(CORBA::Double theX,
	      CORBA::Double theY,
	      CORBA::Double theZ );

    virtual
    void
    GetPoint2(CORBA::Double& theX,
	      CORBA::Double& theY,
	      CORBA::Double& theZ );

    VISU_CutSegmentPL* 
    GetSpecificPL() const
    { 
      return myCutSegmentPL; 
    }
    
  protected:
    //! Extends VISU_ColoredPrs3d_i::CreatePipeLine
    virtual 
    void
    CreatePipeLine(VISU_PipeLine* thePipeLine);

    //! Extends VISU_ColoredPrs3d_i::CheckIsPossible
    virtual 
    bool 
    CheckIsPossible();

    virtual 
    const char* 
    GetIconName();

    VISU_CutSegmentPL *myCutSegmentPL;

  public:
    //! Extends VISU_ColoredPrs3d_i::IsPossible
    static
    size_t
    IsPossible(Result_i* theResult, 
	       const std::string& theMeshName, 
	       VISU::Entity theEntity,
	       const std::string& theFieldName, 
	       CORBA::Long theTimeStampNumber,
	       bool theIsMemoryCheck);

    //! Extends VISU_ColoredPrs3d_i::Create
    virtual 
    Storable* 
    Create(const std::string& theMeshName, 
	   VISU::Entity theEntity,
	   const std::string& theFieldName, 
	   CORBA::Long theTimeStampNumber);

    //! Extends VISU_ColoredPrs3d_i::ToStream
    virtual 
    void
    ToStream(std::ostringstream& theStr);

    //! Extends VISU_ColoredPrs3d_i::Restore
    virtual
    Storable* 
    Restore(SALOMEDS::SObject_ptr theSObject,
	    const Storable::TRestoringMap& theMap);

    virtual 
    void
    SameAs(const Prs3d_i* theOrigin);

    //! Extends VISU_ColoredPrs3d_i::CreateActor
    virtual 
    VISU_Actor* 
    CreateActor();

    static const std::string myComment;

    virtual
    const char* 
    GetComment() const;

    virtual
    QString 
    GenerateName();
  };
}

#endif
