// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : VISU_PointMap3d_i.hh
//  Author : 
//  Module : VISU
//
#ifndef VISU_PointMap3d_i_HeaderFile
#define VISU_PointMap3d_i_HeaderFile

#include "VISU_PrsObject_i.hh"
#include "SALOME_GenericObj_i.hh"

#include "VISU_TableReader.hxx"
#include "VISU_PointMap3dActor.h"
#include "VISU_Table_i.hh"
#include "VISU_ActorFactory.h"
#include "VISU_DeformedGridPL.hxx"

#include <vtkTimeStamp.h>
#include <vtkSmartPointer.h>

namespace VISU
{

  struct TActorFactory;

  //==============================================================================
  class VISU_I_EXPORT PointMap3d_i : public virtual POA_VISU::PointMap3d,
				     public virtual SALOME::GenericObj_i,
				     public virtual TActorFactory,
				     public virtual Table_i
  {
    static int myNbPresent;
    PointMap3d_i( const PointMap3d_i& );
  public:
    PointMap3d_i( SALOMEDS::Study_ptr theStudy, const char* theObjectEntry);

    virtual ~PointMap3d_i();
    virtual VISU::VISUType GetType() { return VISU::TPOINTMAP3D;};

    virtual void SetTitle( const char* theTitle );
    virtual char* GetTitle();

    virtual void RemoveFromStudy();
    virtual void UpdateActor(VISU_ActorBase* thePointMap3dActor);
    virtual void UpdateActors();

    virtual void Update();

    virtual Handle(SALOME_InteractiveObject) GetIO();

    //----------- override Prs3d methods -----------------------------

    //! Move the 3D presentation according to the given offset parameters
    virtual void SetOffset(CORBA::Float theDx, CORBA::Float theDy, CORBA::Float theDz);

    //! Gets offset parameters for the 3D presentation
    virtual void GetOffset(CORBA::Float& theDx, CORBA::Float& theDy, CORBA::Float& theDz);

    //! Do nothing, just for compilability
    virtual void SetMarkerStd(VISU::MarkerType, VISU::MarkerScale);
    virtual void SetMarkerTexture(CORBA::Long);
    virtual VISU::MarkerType GetMarkerType();
    virtual VISU::MarkerScale GetMarkerScale();
    virtual CORBA::Long GetMarkerTexture();

    //! Gets memory size actually used by the presentation (Mb).
    virtual CORBA::Float GetMemorySize();

    //----------- override ColoredPrs3dBase methods ------------------

    virtual CORBA::Double GetMin();
    virtual CORBA::Double GetMax();
    virtual CORBA::Double GetMinTableValue();
    virtual CORBA::Double GetMaxTableValue();
    virtual void SetRange(CORBA::Double theMin, CORBA::Double theMax);

    virtual CORBA::Double GetSourceMin();
    virtual CORBA::Double GetSourceMax();
    virtual void SetSourceRange();

    virtual CORBA::Boolean IsRangeFixed();

    virtual void SetPosition(CORBA::Double X, CORBA::Double Y);
    virtual CORBA::Double GetPosX();
    virtual CORBA::Double GetPosY();

    virtual void SetSize(CORBA::Double theWidth, CORBA::Double theHeight);

    virtual void SetRatios(CORBA::Long theTitleSize, CORBA::Long theLabelSize, 
			   CORBA::Long theBarWidth, CORBA::Long theBarHeight);

    virtual CORBA::Double GetWidth();
    virtual CORBA::Double GetHeight();

    virtual void SetNbColors(CORBA::Long theNbColors);
    virtual CORBA::Long GetNbColors();

    virtual void SetLabels(CORBA::Long theNbLabels);
    virtual CORBA::Long GetLabels();

    virtual void  SetLabelsFormat(const char* theFormat);
    virtual char* GetLabelsFormat();
    
    virtual void SetBarOrientation(VISU::ColoredPrs3dBase::Orientation theOrientation);
    virtual VISU::ColoredPrs3dBase::Orientation GetBarOrientation();

    void UseFixedRange(bool theUseFixedRange);

    // ScaledMap Methods

    virtual VISU::Scaling GetScaling();
    virtual void SetScaling(VISU::Scaling theScaling);

    bool IsPositiveTable();

    // Plot3dBase methods

    void SetScaleFactor (CORBA::Double theScaleFactor);
    CORBA::Double GetScaleFactor();

    void SetContourPrs (CORBA::Boolean theIsContourPrs );
    CORBA::Boolean GetIsContourPrs();

    void SetNbOfContours (CORBA::Long);
    CORBA::Long GetNbOfContours();

    VISU_DeformedGridPL* GetSpecificPL() const { return myTablePL; }

    // TActorFactory Methods

    //! Gets know whether the factory instance can be used for actor management or not
    virtual bool GetActiveState();

    virtual void SetActiveState(bool theState);

    //! Return modified time of the factory
    virtual unsigned long int GetMTime();

    //! To unregister the actor
    virtual void RemoveActor(VISU_ActorBase* theActor);
    virtual void RemoveActors();

    //------ Text Properties & Label Properties ------------------------

    virtual bool IsBoldTitle();
    virtual void SetBoldTitle(bool isBold);
    virtual bool IsItalicTitle();
    virtual void SetItalicTitle(bool isItalic);
    virtual bool IsShadowTitle();
    virtual void SetShadowTitle(bool isShadow);
    virtual int  GetTitFontType();
    virtual void SetTitFontType(int theType);
    virtual void GetTitleColor(vtkFloatingPointType& theR, 
		 vtkFloatingPointType& theG, 
		 vtkFloatingPointType& theB);
    virtual void SetTitleColor(vtkFloatingPointType theR, 
		 vtkFloatingPointType theG, 
		 vtkFloatingPointType theB);    

    virtual bool IsBoldLabel();
    virtual void SetBoldLabel(bool isBold);
    virtual bool IsItalicLabel();
    virtual void SetItalicLabel(bool isItalic);
    virtual bool IsShadowLabel();
    virtual void SetShadowLabel(bool isShadow);
    virtual int  GetLblFontType();
    virtual void SetLblFontType(int theType);
    virtual void GetLabelColor(vtkFloatingPointType& theR, 
		 vtkFloatingPointType& theG, 
		 vtkFloatingPointType& theB);
    virtual void SetLabelColor(vtkFloatingPointType theR, 
		 vtkFloatingPointType theG, 
		 vtkFloatingPointType theB);

    //-------------------------------------------------------------------
    virtual CORBA::Long GetTitleSize();
    virtual CORBA::Long GetLabelSize();
    virtual CORBA::Long GetBarWidth();
    virtual CORBA::Long GetBarHeight();

    virtual void SetUnitsVisible(CORBA::Boolean isVisible);
    virtual CORBA::Boolean IsUnitsVisible();

    //-------------------------------------------------------------------

  protected:
    Storable* Build(int theRestoring);

    VISU::Table::Orientation myOrientation;
    VISU::ColoredPrs3dBase::Orientation myBarOrientation;
    std::string              myTitle;
    std::string              myScalarBarTitle;
    bool                     myIsUnits;
    SALOMEDS::SObject_var    mySObj;
    int                      myNumberOfLabels;
    std::string              myLabelsFormat;
    vtkFloatingPointType     myPosition[2],
                             myWidth, myHeight,
                             myTitleSize,
                             myLabelSize, 
                             myBarWidth, myBarHeight;
    bool                     myIsFixedRange;
    CORBA::Float             myOffset[3];
    bool                     myIsActiveState;
    vtkTimeStamp             myParamsTime;
    vtkTimeStamp             myUpdateTime;

    Handle(SALOME_InteractiveObject) myIO;


    //Font management
    bool myIsBoldTitle;
    bool myIsItalicTitle;
    bool myIsShadowTitle;
    int  myTitFontType;
    vtkFloatingPointType myTitleColor[3];

    bool myIsBoldLabel;
    bool myIsItalicLabel;
    bool myIsShadowLabel;
    int  myLblFontType;
    vtkFloatingPointType myLabelColor[3];

    boost::signal0<void> myUpdateActorsSignal;
    boost::signal0<void> myRemoveActorsFromRendererSignal;
    vtkSmartPointer<vtkActorCollection> myActorCollection;

  public:
    //    virtual Storable* Create( const VISU::PTableIDMapper& theTableIDMapper );
    virtual Storable*       Create();
    VISU_PointMap3dActor*   CreateActor();
    VISU::PTableIDMapper    GetTableIDMapper();

    SALOMEDS::SObject_var GetSObject() const;
    VISU_DeformedGridPL*  myTablePL;

    virtual Storable* Restore( const Storable::TRestoringMap& theMap,
			       SALOMEDS::SObject_ptr SO);

    static Storable* StorableEngine(SALOMEDS::SObject_ptr theSObject,
				    const Storable::TRestoringMap& theMap,
				    const std::string& thePrefix,
				    CORBA::Boolean theIsMultiFile);

    virtual void ToStream( std::ostringstream& theStr );
    static const std::string myComment;
    virtual const char* GetComment() const;
    virtual QString GenerateName();
    virtual QString GetTableTitle();

    virtual std::string GetObjectEntry();
  };
}

#endif
