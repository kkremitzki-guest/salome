// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : VISU_DeformedShapeAndScalarMap_i.cc
//  Author : Eugeny Nikolaev
//  Module : VISU
//
#include "VISU_DeformedShapeAndScalarMap_i.hh"
#include "VISU_Prs3dUtils.hh"
#include "VISU_Result_i.hh"

#include "VISU_ScalarMapAct.h"
#include "VISU_DeformedShapeAndScalarMapPL.hxx"
#include "VISU_Convertor.hxx"

#include "SUIT_ResourceMgr.h"
#include "SALOME_Event.h"

#include <vtkUnstructuredGrid.h>
#include <vtkProperty.h>
#include <vtkMapper.h>

#ifdef _DEBUG_
static int MYDEBUG = 0;
#else
static int MYDEBUG = 0;
#endif

static int INCMEMORY = 4+12;

using namespace std;

//---------------------------------------------------------------
size_t
VISU::DeformedShapeAndScalarMap_i
::IsPossible(Result_i* theResult, 
	     const std::string& theMeshName, 
	     VISU::Entity theEntity,
	     const std::string& theFieldName, 
	     CORBA::Long theTimeStampNumber,
	     bool theIsMemoryCheck)
{
  size_t aResult = 0;
  try{
    aResult = TSuperClass::IsPossible(theResult,
				      theMeshName,
				      theEntity,
				      theFieldName,
				      theTimeStampNumber,
				      false);
    if(theIsMemoryCheck && aResult){
      VISU::Result_i::PInput anInput = theResult->GetInput(theMeshName,
							   theEntity,
							   theFieldName,
							   theTimeStampNumber);
      VISU::PField aField = anInput->GetField(theMeshName,
					      (VISU::TEntity)theEntity,
					      theFieldName);
      if(aField->myNbComp <= 1)
	return 0;

      bool anIsEstimated = true;
      size_t aSize = anInput->GetTimeStampOnMeshSize(theMeshName,
						     (VISU::TEntity)theEntity,
						     theFieldName,
						     theTimeStampNumber,
						     anIsEstimated);
      if(anIsEstimated)
	aSize *= INCMEMORY;
      aResult = VISU_PipeLine::CheckAvailableMemory(aSize);
      if(MYDEBUG) 
	MESSAGE("DeformedShapeAndScalarMap_i::IsPossible - CheckAvailableMemory = "<<float(aSize)<<"; aResult = "<<aResult);
    }
  }catch(std::exception& exc){
    INFOS("Follow exception was occured :\n"<<exc.what());
  }catch(...){
    INFOS("Unknown exception was occured!");
  }
  return aResult;
}

//---------------------------------------------------------------
int VISU::DeformedShapeAndScalarMap_i::myNbPresent = 0;

//---------------------------------------------------------------
QString
VISU::DeformedShapeAndScalarMap_i
::GenerateName() 
{
  return VISU::GenerateName("ScalarDef.Shape",myNbPresent++);
}

//---------------------------------------------------------------
const string VISU::DeformedShapeAndScalarMap_i::myComment = "SCALARMAPONDEFORMEDSHAPE"; // It is obsolete. Use "DEFORMEDSHAPEANDSCALARMAP" instead.

//---------------------------------------------------------------
const char* 
VISU::DeformedShapeAndScalarMap_i
::GetComment() const
{ 
  return myComment.c_str();
}

//----------------------------------------------------------------------------
const char*
VISU::DeformedShapeAndScalarMap_i
::GetIconName()
{
  if (!IsGroupsUsed())
    return "ICON_TREE_SCALAR_MAP_ON_DEFORMED_SHAPE";
  else
    return "ICON_TREE_SCALAR_MAP_ON_DEFORMED_SHAPE_GROUPS";
}

//---------------------------------------------------------------
VISU::DeformedShapeAndScalarMap_i
::DeformedShapeAndScalarMap_i(EPublishInStudyMode thePublishInStudyMode) :
  ColoredPrs3d_i(thePublishInStudyMode),
  ScalarMap_i(thePublishInStudyMode),
  myDeformedShapeAndScalarMapPL(NULL),
  myScalarTimeStampNumber(1)
{}


//---------------------------------------------------------------
VISU::Storable* 
VISU::DeformedShapeAndScalarMap_i
::Create(const std::string& theMeshName, 
	 VISU::Entity theEntity,
	 const std::string& theFieldName, 
	 CORBA::Long theTimeStampNumber)
{
  TSuperClass::Create(theMeshName,
			     theEntity,
			     theFieldName,
			     theTimeStampNumber);
  SetScalarField(theEntity,
		 theFieldName.c_str(),
		 theTimeStampNumber);
  return this;
}


//---------------------------------------------------------------
VISU::Storable*
VISU::DeformedShapeAndScalarMap_i
::Restore(SALOMEDS::SObject_ptr theSObject,
	  const Storable::TRestoringMap& theMap)
{
  if(!TSuperClass::Restore(theSObject, theMap))
    return NULL;

  QString aMeshName = VISU::Storable::FindValue(theMap,"myScalarMeshName");
  VISU::Entity anEntity = VISU::Entity(VISU::Storable::FindValue(theMap,"myScalarEntity").toInt());

  QString aFieldName = VISU::Storable::FindValue(theMap,"myScalarFieldName");
  int aTimeStampNumber = VISU::Storable::FindValue(theMap,"myScalarIteration").toInt();

  SetScalarField(anEntity,
		 aFieldName.toLatin1().constData(),
		 aTimeStampNumber);

  SetScale(VISU::Storable::FindValue(theMap,"myFactor").toDouble());
  
  return this;
}


//---------------------------------------------------------------
void
VISU::DeformedShapeAndScalarMap_i
::ToStream(std::ostringstream& theStr)
{
  TSuperClass::ToStream(theStr);
  
  Storable::DataToStream( theStr, "myScalarEntity",    int(myScalarEntity));
  Storable::DataToStream( theStr, "myScalarFieldName", myScalarFieldName.c_str());
  Storable::DataToStream( theStr, "myScalarIteration", int(myScalarTimeStampNumber));

  Storable::DataToStream( theStr, "myFactor", GetScale() );
}


//---------------------------------------------------------------
VISU::DeformedShapeAndScalarMap_i
::~DeformedShapeAndScalarMap_i()
{
  if(MYDEBUG) MESSAGE("DeformedShapeAndScalarMap_i::~DeformedShapeAndScalarMap_i()");
}


//---------------------------------------------------------------
void
VISU::DeformedShapeAndScalarMap_i
::SameAs(const Prs3d_i* theOrigin)
{
  TSuperClass::SameAs(theOrigin);
  
  if(const DeformedShapeAndScalarMap_i* aPrs3d = dynamic_cast<const DeformedShapeAndScalarMap_i*>(theOrigin)){
    DeformedShapeAndScalarMap_i* anOrigin = const_cast<DeformedShapeAndScalarMap_i*>(aPrs3d);
    CORBA::String_var aFieldName = anOrigin->GetScalarFieldName();
    
    SetScalarField(anOrigin->GetScalarEntity(),
		   aFieldName,
		   anOrigin->GetScalarTimeStampNumber());
    SetTitle(anOrigin->GetTitle()); //fix of 20094 issue
    SetScalarMode(anOrigin->GetScalarMode()); //
    if(!IsRangeFixed() && IsPipeLineExists()) //fix of 20107 issue
      SetSourceRange();                       //
    Update();
  }
}


//---------------------------------------------------------------
void
VISU::DeformedShapeAndScalarMap_i
::SetScale(CORBA::Double theScale) 
{ 
  VISU::TSetModified aModified(this);

  ProcessVoidEvent(new TVoidMemFun1ArgEvent<VISU_DeformedShapeAndScalarMapPL, vtkFloatingPointType>
		   (GetSpecificPL(), &VISU_DeformedShapeAndScalarMapPL::SetScale, theScale));
}


//---------------------------------------------------------------
CORBA::Double
VISU::DeformedShapeAndScalarMap_i
::GetScale()
{
  return myDeformedShapeAndScalarMapPL->GetScale();
}


//---------------------------------------------------------------
void
VISU::DeformedShapeAndScalarMap_i
::CreatePipeLine(VISU_PipeLine* thePipeLine)
{
  if(!thePipeLine){
    myDeformedShapeAndScalarMapPL = VISU_DeformedShapeAndScalarMapPL::New();
  }else
    myDeformedShapeAndScalarMapPL = dynamic_cast<VISU_DeformedShapeAndScalarMapPL*>(thePipeLine);

  myDeformedShapeAndScalarMapPL->GetMapper()->SetScalarVisibility(1);

  TSuperClass::CreatePipeLine(myDeformedShapeAndScalarMapPL);
}


//---------------------------------------------------------------
bool
VISU::DeformedShapeAndScalarMap_i
::CheckIsPossible() 
{
  return IsPossible(GetCResult(),GetCMeshName(),GetEntity(),GetCFieldName(),GetTimeStampNumber(),true);
}


//---------------------------------------------------------------
VISU_Actor* 
VISU::DeformedShapeAndScalarMap_i
::CreateActor() 
{
  VISU_Actor* anActor = TSuperClass::CreateActor(true);
  SUIT_ResourceMgr* aResourceMgr = VISU::GetResourceMgr();
  int  aDispMode = aResourceMgr->integerValue("VISU", "scalar_def_represent", 2);
  bool toShrink  = aResourceMgr->booleanValue("VISU", "scalar_def_shrink", false);
  anActor->SetRepresentation(aDispMode);
  if (toShrink) 
    anActor->SetShrink();
  anActor->SetVTKMapping(false);
  return anActor;
}


//---------------------------------------------------------------
void
VISU::DeformedShapeAndScalarMap_i
::UpdateActor(VISU_Actor* theActor) 
{
  if(VISU_ScalarMapAct* anActor = dynamic_cast<VISU_ScalarMapAct*>(theActor)){
    anActor->SetBarVisibility(true);
    GetPipeLine()->GetMapper()->SetScalarVisibility(1);
  }
  TSuperClass::UpdateActor(theActor);
}


//---------------------------------------------------------------
void
VISU::DeformedShapeAndScalarMap_i
::SetField(VISU::PField theField)
{
  TSuperClass::SetField(theField);
  
  if(!myScalarField)
    myScalarField = theField;
}


//---------------------------------------------------------------
void
VISU::DeformedShapeAndScalarMap_i
::SetScalarField(VISU::Entity theEntity,
		 const char* theFieldName,
		 CORBA::Long theTimeStampNumber)
{
  bool anIsModified = false;

  if(!anIsModified)
    anIsModified |= myScalarEntity != theEntity;

  if(!anIsModified)
    anIsModified |= myScalarFieldName != theFieldName;

  if(!anIsModified)
    anIsModified |= myScalarTimeStampNumber != theTimeStampNumber;
  
  if(!anIsModified)
    return;

  VISU::TSetModified aModified(this);

  VISU::TEntity aEntity = VISU::TEntity(theEntity);
  VISU::Result_i::PInput anInput = GetCResult()->GetInput(GetCMeshName(),
							  theEntity,
							  theFieldName,
							  theTimeStampNumber);
  myScalarField = anInput->GetField(GetCMeshName(), aEntity, theFieldName);

  VISU::PUnstructuredGridIDMapper anIDMapper = 
    anInput->GetTimeStampOnMesh(GetCMeshName(),
				aEntity,
				theFieldName,
				theTimeStampNumber);

  vtkUnstructuredGrid* anOutput = anIDMapper->GetUnstructuredGridOutput();
  if(myDeformedShapeAndScalarMapPL && anOutput)
    ProcessVoidEvent(new TVoidMemFun1ArgEvent<VISU_DeformedShapeAndScalarMapPL, vtkDataSet*>
		     (GetSpecificPL(), &VISU_DeformedShapeAndScalarMapPL::SetScalars, anOutput));
  
  myScalarEntity = theEntity;
  myScalarFieldName = theFieldName;
  myScalarTimeStampNumber = theTimeStampNumber;

  SetTitle(theFieldName);
  SetScalarMode(GetScalarMode());
  if(!IsRangeFixed() && IsPipeLineExists())
    SetSourceRange();

  myParamsTime.Modified();
}


//---------------------------------------------------------------
VISU::Entity 
VISU::DeformedShapeAndScalarMap_i
::GetScalarEntity()
{
  return myScalarEntity;
}


//---------------------------------------------------------------
char* 
VISU::DeformedShapeAndScalarMap_i
::GetScalarFieldName()
{
  return CORBA::string_dup(myScalarFieldName.c_str());
}


//---------------------------------------------------------------
CORBA::Long 
VISU::DeformedShapeAndScalarMap_i
::GetScalarTimeStampNumber()
{
  return myScalarTimeStampNumber;
}


//---------------------------------------------------------------
VISU::PField
VISU::DeformedShapeAndScalarMap_i
::GetScalarField()
{
  return myScalarField;
}
