// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : VISU_MeshAct.hxx
//  Author : Laurent CORNABE with the help of Nicolas REJNERI
//  Module : VISU
//  $Header: /home/server/cvs/VISU/VISU_SRC/src/OBJECT/VISU_ScalarMapAct.cxx,v 1.12.2.7.6.3.4.1 2011-06-02 06:00:17 vsr Exp $
//
#include "VISU_ScalarMapAct.h"
#include "VISU_LookupTable.hxx"
#include "VISU_ScalarBarActor.hxx"
#include "VISU_PipeLine.hxx"

#include "VISU_OpenGLPointSpriteMapper.hxx"
#include "VISU_GaussPtsDeviceActor.h"
#include "VISU_DeformedShapePL.hxx"
#include "VISU_PipeLineUtils.hxx"


#include <SUIT_ResourceMgr.h>
#include <SUIT_Session.h>

#include <SALOME_ExtractGeometry.h>
#include <SALOME_ExtractPolyDataGeometry.h>

#include <vtkObjectFactory.h>
#include <vtkRenderer.h>
#include <vtkProperty.h>
#include <vtkMatrix4x4.h>
#include <vtkMapper.h>
#include <vtkDataSetMapper.h>

#include <vtkRenderWindowInteractor.h>
#include <vtkCallbackCommand.h>
#include <vtkRenderWindow.h>
#include <vtkImageData.h>


//============================================================================
class VISU_PointsDeviceActor: public VISU_GaussDeviceActorBase
{
 public:
  vtkTypeMacro(VISU_PointsDeviceActor, VISU_GaussDeviceActorBase);

  static 
  VISU_PointsDeviceActor* 
  New();
 

  //----------------------------------------------------------------------------
  virtual
  void
  SetInput(vtkDataSet* theDataSet)
  {
    myGeomFilter->SetInput( theDataSet );
  }


  //----------------------------------------------------------------------------
  void
  SetInteractor(vtkRenderWindowInteractor* theInteractor)
  {
    if(theInteractor == myInteractor)
      return;

    if(myInteractor)
      myInteractor->RemoveObserver(myEventCallbackCommand);
    
    if(theInteractor)
      theInteractor->AddObserver(vtkCommand::CharEvent, 
                                 myEventCallbackCommand, 
                                 0.0);

    myInteractor = theInteractor;
  }


  //----------------------------------------------------------------------------
  void
  DoMapperShallowCopy( vtkMapper* theMapper,
                       bool theIsCopyInput )
  {
    Superclass::DoMapperShallowCopy( theMapper, theIsCopyInput );

    vtkDataSet* aDataSet = theMapper->GetInput();
    vtkFloatingPointType aScaleFactor = VISU_DeformedShapePL::GetScaleFactor( aDataSet );

    GetPointSpriteMapper()->SetAverageCellSize( aScaleFactor );
  }


  //----------------------------------------------------------------------------
  void
  DeepCopy( VISU_PointsDeviceActor *theActor )
  {
    VISU::CopyPointSpriteDataMapper( GetPointSpriteMapper(), theActor->GetPointSpriteMapper(), false );
  }

 protected:
  //----------------------------------------------------------------------------
  VISU_PointsDeviceActor():
    myGeomFilter( VTKViewer_GeometryFilter::New() ),
    myEventCallbackCommand( vtkCallbackCommand::New() ),
    myInteractor( NULL )
  {
    myGeomFilter->SetInside(true);

    VISU_OpenGLPointSpriteMapper* aMapper = VISU_OpenGLPointSpriteMapper::New();
    aMapper->SetInput( myGeomFilter->GetOutput() );

    std::string aRootDir( getenv( "VISU_ROOT_DIR") );
    std::string aMainTexture = aRootDir + "/share/salome/resources/visu/sprite_texture.bmp";
    std::string anAlphaTexture = aRootDir + "/share/salome/resources/visu/sprite_alpha.bmp";
    VISU::TTextureValue aTextureValue = VISU::GetTexture( aMainTexture, anAlphaTexture );
    aMapper->SetImageData( aTextureValue.GetPointer() );

    aMapper->SetUseLookupTableScalarRange(true);
    aMapper->SetColorModeToMapScalars();
    aMapper->SetScalarVisibility(true);

    SetPointSpriteMapper( aMapper );

    aMapper->Delete();

    myEventCallbackCommand->SetClientData( this );
    myEventCallbackCommand->SetCallback( VISU_PointsDeviceActor::ProcessEvents );
  }


  //----------------------------------------------------------------------------
  ~VISU_PointsDeviceActor()
  {
    SetInteractor( NULL );
    myGeomFilter->Delete();
    myEventCallbackCommand->Delete();
  }


  //----------------------------------------------------------------------------
  static
  void
  ProcessEvents(vtkObject* theObject, 
                unsigned long theEvent,
                void* theClientData, 
                void* theCallData)
  {
    if ( VISU_PointsDeviceActor* self = reinterpret_cast<VISU_PointsDeviceActor*>( theClientData ) )
      self->OnInteractorEvent( theEvent );
  }


  //----------------------------------------------------------------------------
  void
  OnInteractorEvent(unsigned long theEvent)
  {
    switch ( theEvent ) {
    case vtkCommand::CharEvent: {
      switch( myInteractor->GetKeyCode() ) {
      case 'M' :
      case 'm' : {
        if ( !GetVisibility() )
          return;

        static vtkFloatingPointType anIncrement = 2;
        vtkFloatingPointType aMagnification = GetPointSpriteMapper()->GetPointSpriteMagnification();
        vtkFloatingPointType coefficient = myInteractor->GetShiftKey() ? anIncrement : 1 / anIncrement;

        GetPointSpriteMapper()->SetPointSpriteMagnification( aMagnification * coefficient );

        myInteractor->CreateTimer(VTKI_TIMER_UPDATE);
        break;
      }
      default:
        return;
      }
      break;
    }
    default:
      return;
    }
  }


  //----------------------------------------------------------------------------
  vtkCallbackCommand* myEventCallbackCommand;
  vtkRenderWindowInteractor* myInteractor;
  VTKViewer_GeometryFilter* myGeomFilter;

 private:
  VISU_PointsDeviceActor(const VISU_PointsDeviceActor&); // Not implemented
  void operator=(const VISU_PointsDeviceActor&); // Not implemented
};

vtkStandardNewMacro(VISU_PointsDeviceActor);

//----------------------------------------------------------------------------
vtkStandardNewMacro(VISU_ScalarMapAct);
static vtkFloatingPointType EPS = 1.0 / VTK_LARGE_FLOAT;

//----------------------------------------------------------------------------
VISU_ScalarMapAct
::VISU_ScalarMapAct()
{
  myScalarBar = VISU_ScalarBarActor::New();

  vtkProperty* aProperty = GetProperty();
  aProperty->SetAmbient(1.0); 
  aProperty->SetDiffuse(0.0);
  aProperty->SetSpecular(0.0);
  
  myProperty->DeepCopy(aProperty);

  vtkMatrix4x4 *aMatrix = vtkMatrix4x4::New();

  mySurfaceActor= SVTK_DeviceActor::New();
  mySurfaceActor->SetRepresentation(SVTK::Representation::Surface);
  mySurfaceActor->SetProperty(aProperty);
  mySurfaceActor->SetUserMatrix(aMatrix);

  myEdgeActor = SVTK_DeviceActor::New();
  myEdgeActor->SetRepresentation(SVTK::Representation::Wireframe);
  myEdgeActor->SetUserMatrix(aMatrix);
  myEdgeActor->GetProperty()->SetColor(255.,255.,255.);

  myPointsActor = SVTK_DeviceActor::New();
  myPointsActor->SetRepresentation(SVTK::Representation::Points);
  myPointsActor->SetProperty(aProperty);
  myPointsActor->SetUserMatrix(aMatrix);

  myPointSpriteActor = VISU_PointsDeviceActor::New();
  myPointSpriteActor->SetProperty(aProperty);
  myPointSpriteActor->SetUserMatrix(aMatrix);

  aMatrix->Delete();

  myIsPointSpriteMode = false;

  SUIT_ResourceMgr* aResourceMgr = SUIT_Session::session()->resourceMgr();
  //Quadratic 2D elements representation
  if(aResourceMgr) {
    //----------------------------------------------------------------------------
    int aQuadraticAngle = aResourceMgr->integerValue( "VISU", "max_angle", 2);
    mySurfaceActor->SetQuadraticArcAngle(aQuadraticAngle);
    myEdgeActor->SetQuadraticArcAngle(aQuadraticAngle);

    int anElem0DSize = aResourceMgr->integerValue( "VISU", "elem0d_size", 5);
    mySurfaceActor->GetProperty()->SetPointSize(anElem0DSize);
  }
}

//----------------------------------------------------------------------------
VISU_ScalarMapAct
::~VISU_ScalarMapAct()
{
  myScalarBar->Delete();

  myPointsActor->Delete();
  myPointSpriteActor->Delete();

  mySurfaceActor->Delete();
  myEdgeActor->Delete();
}

void
VISU_ScalarMapAct
::ShallowCopyPL(VISU_PipeLine* thePipeLine)
{
  VISU_DataSetActor::ShallowCopyPL( thePipeLine );

  myEdgeActor->GetMapper()->ScalarVisibilityOff();

  myPointSpriteActor->DoMapperShallowCopy( thePipeLine->GetMapper(), false );

  VISU::CopyMapper( myPointsActor->GetMapper(), thePipeLine->GetMapper(), false );
  VISU::CopyMapper( mySurfaceActor->GetMapper(), thePipeLine->GetMapper(), false );
}

//----------------------------------------------------------------------------
void
VISU_ScalarMapAct
::SetMapperInput(vtkDataSet* theDataSet)
{
  Superclass::SetMapperInput( theDataSet );

//    myPointsActor->SetInput( theDataSet );
//    mySurfaceActor->SetInput( theDataSet );
//    myEdgeActor->SetInput( theDataSet );

  if (theDataSet->IsA("vtkPolyData")) {
    myPointSpriteActor->SetInput( myPolyDataExtractor->GetOutput() );
    myPointsActor->SetInput( myPolyDataExtractor->GetOutput() );
    mySurfaceActor->SetInput( myPolyDataExtractor->GetOutput() );
    myEdgeActor->SetInput( myPolyDataExtractor->GetOutput() );
  } else {
    myPointSpriteActor->SetInput( myExtractor->GetOutput() );
    myPointsActor->SetInput( myExtractor->GetOutput() );
    mySurfaceActor->SetInput( myExtractor->GetOutput() );
    myEdgeActor->SetInput( myExtractor->GetOutput() );
  }
}

//----------------------------------------------------------------------------
void
VISU_ScalarMapAct
::SetTransform(VTKViewer_Transform* theTransform)
{
  Superclass::SetTransform(theTransform);

  myPointsActor->SetTransform(theTransform);
  myPointSpriteActor->SetTransform(theTransform);

  mySurfaceActor->SetTransform(theTransform);
  myEdgeActor->SetTransform(theTransform);
}

//----------------------------------------------------------------------------
vtkProperty* 
VISU_ScalarMapAct
::GetEdgeProperty()
{
  return myEdgeActor->GetProperty();
}

//----------------------------------------------------------------------------
void
VISU_ScalarMapAct
::SetShrinkable(bool theIsShrinkable)
{
  Superclass::SetShrinkable(theIsShrinkable);

  mySurfaceActor->SetShrinkable(theIsShrinkable);
}

void
VISU_ScalarMapAct
::SetShrinkFactor(vtkFloatingPointType theValue)
{
  Superclass::SetShrinkFactor(theValue);

  mySurfaceActor->SetShrinkFactor(theValue);
}

//----------------------------------------------------------------------------
void
VISU_ScalarMapAct
::SetShrink()
{
  if(myRepresentation == VTK_POINTS)
    return;

  Superclass::SetShrink();

  mySurfaceActor->SetShrink();
}

void
VISU_ScalarMapAct
::UnShrink()
{
  Superclass::UnShrink();

  mySurfaceActor->UnShrink();
}

//----------------------------------------------------------------------------
void
VISU_ScalarMapAct
::SetFeatureEdgesAllowed(bool theIsFeatureEdgesAllowed)
{
  Superclass::SetFeatureEdgesAllowed(theIsFeatureEdgesAllowed);

  mySurfaceActor->SetFeatureEdgesAllowed(theIsFeatureEdgesAllowed);
}

void
VISU_ScalarMapAct
::SetFeatureEdgesAngle(vtkFloatingPointType theValue)
{
  Superclass::SetFeatureEdgesAngle(theValue);

  mySurfaceActor->SetFeatureEdgesAngle(theValue);
}

void
VISU_ScalarMapAct
::SetFeatureEdgesFlags(bool theIsFeatureEdges,
                       bool theIsBoundaryEdges,
                       bool theIsManifoldEdges,
                       bool theIsNonManifoldEdges)
{
  Superclass::SetFeatureEdgesFlags(theIsFeatureEdges,
                                   theIsBoundaryEdges,
                                   theIsManifoldEdges,
                                   theIsNonManifoldEdges);

  mySurfaceActor->SetFeatureEdgesFlags(theIsFeatureEdges,
                                       theIsBoundaryEdges,
                                       theIsManifoldEdges,
                                       theIsNonManifoldEdges);
}

void
VISU_ScalarMapAct
::SetFeatureEdgesColoring(bool theIsColoring)
{
  Superclass::SetFeatureEdgesColoring(theIsColoring);

  mySurfaceActor->SetFeatureEdgesColoring(theIsColoring);
}

//----------------------------------------------------------------------------
void
VISU_ScalarMapAct
::SetFeatureEdgesEnabled(bool theIsFeatureEdgesEnabled)
{
  if(theIsFeatureEdgesEnabled && myRepresentation == VTK_POINTS)
    return;

  Superclass::SetFeatureEdgesEnabled(theIsFeatureEdgesEnabled);

  mySurfaceActor->SetFeatureEdgesEnabled(theIsFeatureEdgesEnabled);
}

//----------------------------------------------------------------------------
void
VISU_ScalarMapAct
::SetOpacity(vtkFloatingPointType theValue)
{
  mySurfaceActor->GetProperty()->SetOpacity(theValue);

  vtkFloatingPointType aPointSpriteOpacity = theValue > 0.0 ? 1.0 : 0.0;
  myPointSpriteActor->GetPointSpriteMapper()->SetPointSpriteOpacity(aPointSpriteOpacity);
}

vtkFloatingPointType
VISU_ScalarMapAct
::GetOpacity()
{
  return mySurfaceActor->GetProperty()->GetOpacity();
}

//----------------------------------------------------------------------------
void
VISU_ScalarMapAct
::SetLineWidth(vtkFloatingPointType theLineWidth)
{
  mySurfaceActor->GetProperty()->SetLineWidth(theLineWidth);
}

vtkFloatingPointType
VISU_ScalarMapAct::GetLineWidth()
{
  return mySurfaceActor->GetProperty()->GetLineWidth();
}

//----------------------------------------------------------------------------
void
VISU_ScalarMapAct
::DeepCopy(VISU_Actor *theActor)
{
  if(VISU_ScalarMapAct* anActor = dynamic_cast<VISU_ScalarMapAct*>(theActor)){
    Superclass::DeepCopy(theActor);
    SetBarVisibility(anActor->GetBarVisibility());
    myPointSpriteActor->DeepCopy( anActor->myPointSpriteActor );
    SetShading(anActor->IsShading());
  }
}


//----------------------------------------------------------------------------
void
VISU_ScalarMapAct
::AddToRender(vtkRenderer* theRenderer)
{
  Superclass::AddToRender(theRenderer);

  myPointSpriteActor->SetInteractor( myInteractor );

  if(myScalarBar)
    theRenderer->AddActor2D(myScalarBar);
}

//----------------------------------------------------------------------------
void
VISU_ScalarMapAct
::RemoveFromRender(vtkRenderer* theRenderer)
{
  if(myScalarBar)
    theRenderer->RemoveActor(myScalarBar);

  if ( vtkWindow* aWindow = theRenderer->GetRenderWindow() ) {
    myPointSpriteActor->ReleaseGraphicsResources( aWindow );
    myPointsActor->ReleaseGraphicsResources( aWindow );
    mySurfaceActor->ReleaseGraphicsResources( aWindow );
    myEdgeActor->ReleaseGraphicsResources( aWindow );
  }

  Superclass::RemoveFromRender(theRenderer);
}

//----------------------------------------------------------------------------
void
VISU_ScalarMapAct
::SetVisibility(int theMode)
{
  Superclass::SetVisibility( theMode );

  myPointSpriteActor->SetVisibility( theMode );
  myPointsActor->SetVisibility( theMode );

  if(myScalarBar) 
    myScalarBar->SetVisibility(myBarVisibility && theMode);
}

//----------------------------------------------------------------------------
int 
VISU_ScalarMapAct
::GetBarVisibility()
{
  return myBarVisibility;
}

//----------------------------------------------------------------------------
VISU_ScalarBarActor*
VISU_ScalarMapAct
::GetScalarBar()
{
  return myScalarBar;
}

//----------------------------------------------------------------------------
void
VISU_ScalarMapAct
::SetBarVisibility(bool theMode)
{
  myBarVisibility = theMode;
  if(myScalarBar) 
    myScalarBar->SetVisibility(myBarVisibility && GetVisibility());
}


//----------------------------------------------------------------------------
void
VISU_ScalarMapAct
::SetRepresentation(int theMode) 
{ 
  bool anIsShanding = IsShading();

  Superclass::SetRepresentation(theMode);

  if(theMode == SVTK::Representation::Surfaceframe)
    mySurfaceActor->SetRepresentation(SVTK::Representation::Surface);
  else
    mySurfaceActor->SetRepresentation(theMode);

  myPointSpriteActor->SetProperty( mySurfaceActor->GetProperty() );
  myPointsActor->SetProperty( mySurfaceActor->GetProperty() );

  SetShading(anIsShanding);
}


//----------------------------------------------------------------------------
void
VISU_ScalarMapAct
::SetShading(bool theOn)
{
  vtkProperty* aProperty = mySurfaceActor->GetProperty();

  if (theOn) {
    aProperty->SetAmbient(0.0); 
    aProperty->SetDiffuse(1.0);
  } else {
    aProperty->SetAmbient(1.0); 
    aProperty->SetDiffuse(0.0);
  }
}

//----------------------------------------------------------------------------
bool
VISU_ScalarMapAct
::IsShading()
{
  vtkProperty* aProperty = mySurfaceActor->GetProperty();
  
  return (fabs(aProperty->GetAmbient()) < EPS && fabs(aProperty->GetDiffuse() - 1.) < EPS);
}

int
VISU_ScalarMapAct
::RenderOpaqueGeometry(vtkViewport *ren)
{
  GetMatrix(myEdgeActor->GetUserMatrix());
  GetMatrix(mySurfaceActor->GetUserMatrix());
  GetMatrix(myPointsActor->GetUserMatrix());
  GetMatrix(myPointSpriteActor->GetUserMatrix());

  using namespace SVTK::Representation;
  switch ( GetRepresentation() ) {

  case Surfaceframe:
    mySurfaceActor->SetAllocatedRenderTime(this->AllocatedRenderTime/2.0,ren);
    mySurfaceActor->RenderOpaqueGeometry(ren);

    myEdgeActor->SetAllocatedRenderTime(this->AllocatedRenderTime/2.0,ren);
    myEdgeActor->RenderOpaqueGeometry(ren);
    break;

  case Points:
    if( myIsPointSpriteMode ) {
      myPointSpriteActor->SetAllocatedRenderTime(this->AllocatedRenderTime,ren);
      myPointSpriteActor->RenderOpaqueGeometry(ren);
    } else {
      myPointsActor->SetAllocatedRenderTime(this->AllocatedRenderTime,ren);
      myPointsActor->RenderOpaqueGeometry(ren);
    }
    break;

  default:
    mySurfaceActor->SetAllocatedRenderTime(this->AllocatedRenderTime,ren);
    mySurfaceActor->RenderOpaqueGeometry(ren);
  }

  return 1;
}

int
VISU_ScalarMapAct
#if (VTK_XVERSION < 0x050100)
::RenderTranslucentGeometry(vtkViewport *ren)
#else
::RenderTranslucentPolygonalGeometry(vtkViewport *ren)
#endif
{
  GetMatrix(myEdgeActor->GetUserMatrix());
  GetMatrix(mySurfaceActor->GetUserMatrix());

  using namespace SVTK::Representation;
  switch ( GetRepresentation() ) {

  case Surfaceframe:
    mySurfaceActor->SetAllocatedRenderTime(this->AllocatedRenderTime,ren);
#if (VTK_XVERSION < 0x050100)
    mySurfaceActor->RenderTranslucentGeometry(ren);
#else
    mySurfaceActor->RenderTranslucentPolygonalGeometry(ren);
#endif

    myEdgeActor->SetAllocatedRenderTime(this->AllocatedRenderTime/2.0,ren);
#if (VTK_XVERSION < 0x050100)
    myEdgeActor->RenderTranslucentGeometry(ren);
#else
    myEdgeActor->RenderTranslucentPolygonalGeometry(ren);
#endif
    break;

  case Points:
    if( myIsPointSpriteMode ) {
      myPointSpriteActor->SetAllocatedRenderTime(this->AllocatedRenderTime,ren);
#if (VTK_XVERSION < 0x050100)
      myPointSpriteActor->RenderTranslucentGeometry(ren);
#else
      myPointSpriteActor->RenderTranslucentPolygonalGeometry(ren);
#endif
    } else {
      myPointsActor->SetAllocatedRenderTime(this->AllocatedRenderTime,ren);
#if (VTK_XVERSION < 0x050100)
      myPointsActor->RenderTranslucentGeometry(ren);
#else
      myPointsActor->RenderTranslucentPolygonalGeometry(ren);
#endif
    }
    break;

  default:
    mySurfaceActor->SetAllocatedRenderTime(this->AllocatedRenderTime,ren);
#if (VTK_XVERSION < 0x050100)
    mySurfaceActor->RenderTranslucentGeometry(ren);
#else
    mySurfaceActor->RenderTranslucentPolygonalGeometry(ren);
#endif
  }

  return 1;
}

//----------------------------------------------------------------------------
#if (VTK_XVERSION >= 0x050100)
int
VISU_ScalarMapAct
::HasTranslucentPolygonalGeometry()
{
  int result = 0; 

  using namespace SVTK::Representation;
  switch ( GetRepresentation() ) {
  case Surfaceframe:
    result |= mySurfaceActor->HasTranslucentPolygonalGeometry();
    result |= myEdgeActor->HasTranslucentPolygonalGeometry();
    break;
  case Points:
    if( myIsPointSpriteMode )
      result |= myPointSpriteActor->HasTranslucentPolygonalGeometry();
    else
      result |= myPointsActor->HasTranslucentPolygonalGeometry();
    break;
  default:
    result |= mySurfaceActor->HasTranslucentPolygonalGeometry();
  }

  return result;
}
#endif

//----------------------------------------------------------------------------
unsigned long int
VISU_ScalarMapAct
::GetMemorySize()
{
  unsigned long int aSize = Superclass::GetMemorySize();

  aSize += myPointSpriteActor->GetMemorySize();

  return aSize;
}

//----------------------------------------------------------------------------

VISU_Actor::EQuadratic2DRepresentation 
VISU_ScalarMapAct
::GetQuadratic2DRepresentation() const
{
  bool mode = (mySurfaceActor->GetQuadraticArcMode() && myEdgeActor->GetQuadraticArcMode());
  if(mode){
    return VISU_Actor::eArcs;
  }
  else
    return VISU_Actor::eLines;
}
  
void VISU_ScalarMapAct::SetQuadratic2DRepresentation( VISU_Actor::EQuadratic2DRepresentation theMode )
{
  Superclass::SetQuadratic2DRepresentation( theMode );
  switch(theMode) {
  case VISU_Actor::eArcs:
    mySurfaceActor->SetQuadraticArcMode(true);
    myEdgeActor->SetQuadraticArcMode(true);
    break;
  case VISU_Actor::eLines:
    mySurfaceActor->SetQuadraticArcMode(false);
    myEdgeActor->SetQuadraticArcMode(false);
    break;
  default:
    break;
  }
}

void VISU_ScalarMapAct::SetMarkerStd( VTK::MarkerType theMarkerType, VTK::MarkerScale theMarkerScale )
{
  Superclass::SetMarkerStd( theMarkerType, theMarkerScale );
  myPointsActor->SetMarkerStd( theMarkerType, theMarkerScale );

  myIsPointSpriteMode = theMarkerType == VTK::MT_POINT_SPRITE;
}

void VISU_ScalarMapAct::SetMarkerTexture( int theMarkerId, VTK::MarkerTexture theMarkerTexture )
{
  Superclass::SetMarkerTexture( theMarkerId, theMarkerTexture );
  myPointsActor->SetMarkerTexture( theMarkerId, theMarkerTexture );

  myIsPointSpriteMode = false;
}

/**
 * Set size of the 0D elements.
 */
void VISU_ScalarMapAct::Set0DElemSize(vtkFloatingPointType theValue) {
  mySurfaceActor->GetProperty()->SetPointSize(theValue);
}

/**
 * Get size of the 0D elements.
 */
vtkFloatingPointType VISU_ScalarMapAct::Get0DElemSize() {
  return mySurfaceActor->GetProperty()->GetPointSize();
}

