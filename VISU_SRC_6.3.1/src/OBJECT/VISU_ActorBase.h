// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : 
//  Author : 
//  Module : VISU
//  $Header: /home/server/cvs/VISU/VISU_SRC/src/OBJECT/VISU_ActorBase.h,v 1.2.16.3.2.2 2011-06-02 06:00:17 vsr Exp $
//
#ifndef VISU_ACTOR_BASE_H
#define VISU_ACTOR_BASE_H

#include "VISU_OBJECT.h"
#include "SALOME_Actor.h"
#include "VISU_BoostSignals.h"
#include <boost/bind.hpp>

class VTKViewer_ShrinkFilter;

//----------------------------------------------------------------------------
namespace VISU 
{ 
  struct TActorFactory;
}


//! This class defines an abstaract interface to manage actors
class VISU_OBJECT_EXPORT VISU_ActorBase: public SALOME_Actor,
                                    public boost::signalslib::trackable
{
 public:
  vtkTypeMacro(VISU_ActorBase, SALOME_Actor);

  //----------------------------------------------------------------------------
  VISU::TActorFactory* GetFactory();
  virtual void SetFactory(VISU::TActorFactory* theActorFactory);
  
  //----------------------------------------------------------------------------
  virtual void UpdateFromFactory();

  //----------------------------------------------------------------------------
  virtual void SetTransform(VTKViewer_Transform* theTransform); 

  virtual void SetLineWidth(vtkFloatingPointType theLineWidth);
  virtual vtkFloatingPointType GetLineWidth();

  virtual void SetShrink();
  virtual void UnShrink(); 
  virtual bool IsShrunkable();
  virtual bool IsShrunk();
  virtual void SetShrinkable(bool theIsShrinkable);

  virtual void SetShrinkFactor(vtkFloatingPointType theFactor = 0.8); 
  virtual vtkFloatingPointType GetShrinkFactor();
  
  virtual void SetRepresentation(int theMode);

  virtual void RemoveFromRender(vtkRenderer* theRenderer);
  virtual void RemoveFromRender();

  virtual void ConnectToFactory(boost::signal0<void>& , boost::signal0<void>&);

  //----------------------------------------------------------------------------
  //! Return pointer to the dataset, which used to calculation of the bounding box of the actor
  //! Redefined from VTKViewer_Actor
  virtual vtkDataSet* GetHighlightedDataSet();
  

 protected:
  VISU_ActorBase();
  virtual  ~VISU_ActorBase();

  VISU::TActorFactory*                 myActorFactory;
  vtkTimeStamp                         myUpdateFromFactoryTime;
  boost::signal1<void,VISU_ActorBase*> myDestroySignal;

  boost::signalslib::connection        myUpdateActorsConnection;
  boost::signalslib::connection        myRemoveFromRendererConnection;

  vtkSmartPointer<VTKViewer_ShrinkFilter> myShrinkFilter;

  bool myIsShrinkable;
  bool myIsShrunk;
};

#endif //VISU_ACTOR_BASE_H
