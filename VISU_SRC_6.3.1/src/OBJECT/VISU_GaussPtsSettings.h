// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : 
//  Author : 
//  Module : VISU
//  $Header: /home/server/cvs/VISU/VISU_SRC/src/OBJECT/VISU_GaussPtsSettings.h,v 1.6.16.1.8.1 2011-06-02 06:00:17 vsr Exp $
//
#ifndef VISU_GaussPtsSettings_HeaderFile
#define VISU_GaussPtsSettings_HeaderFile

#include "VISU_OBJECT.h"

#include <vtkObject.h>
#include <vtkCommand.h>

#include "VTKViewer.h"

class vtkImageData;

#include "VISU_Actor.h"

//============================================================================
namespace VISU
{
  const vtkIdType UpdateFromSettingsEvent        = vtkCommand::UserEvent + 100; 
  const vtkIdType UpdateInsideSettingsEvent      = vtkCommand::UserEvent + 101; 
  const vtkIdType UpdateOutsideSettingsEvent     = vtkCommand::UserEvent + 102; 
}


//! Base class of Gauss Points settings.
class VISU_OBJECT_EXPORT VISU_GaussPtsSettings : public vtkObject
{
 public:
  vtkTypeMacro( VISU_GaussPtsSettings, vtkObject );

  VISU_GaussPtsSettings();
  virtual ~VISU_GaussPtsSettings();

  static
  VISU_GaussPtsSettings*
  New();

  vtkSetMacro( Initial, bool );
  vtkGetMacro( Initial, bool );

  vtkSetMacro( PrimitiveType, int );
  vtkGetMacro( PrimitiveType, int );

  vtkSetMacro( Clamp, vtkFloatingPointType );
  vtkGetMacro( Clamp, vtkFloatingPointType );

  vtkSetMacro( Texture, vtkImageData* );
  vtkGetMacro( Texture, vtkImageData* );

  vtkSetMacro( AlphaThreshold, vtkFloatingPointType );
  vtkGetMacro( AlphaThreshold, vtkFloatingPointType );

  vtkSetMacro( Resolution, int );
  vtkGetMacro( Resolution, int );

  vtkSetMacro( Magnification, vtkFloatingPointType );
  vtkGetMacro( Magnification, vtkFloatingPointType );

  vtkSetMacro( Increment, vtkFloatingPointType );
  vtkGetMacro( Increment, vtkFloatingPointType );

 protected:
  bool                Initial;

  int                 PrimitiveType;
  vtkFloatingPointType Clamp;
  vtkImageData*       Texture;
  vtkFloatingPointType AlphaThreshold;
  int                 Resolution;
  vtkFloatingPointType Magnification;
  vtkFloatingPointType Increment;
};


//! Class of Inside Cursor Gauss Points settings.
/*!
 * Contains information about the point sprite parameters:
 * Clamp, Texture, Alpha threshold, Const size and Color.
 * Used by Gauss Points Actor.
 */
class VISU_OBJECT_EXPORT VISU_InsideCursorSettings : public VISU_GaussPtsSettings
{
 public:
  vtkTypeMacro( VISU_InsideCursorSettings, vtkObject );

  VISU_InsideCursorSettings();
  virtual ~VISU_InsideCursorSettings();

  static
  VISU_InsideCursorSettings*
  New();

  vtkSetMacro( MinSize, vtkFloatingPointType );
  vtkGetMacro( MinSize, vtkFloatingPointType );

  vtkSetMacro( MaxSize, vtkFloatingPointType );
  vtkGetMacro( MaxSize, vtkFloatingPointType );

 protected:
  vtkFloatingPointType MinSize;
  vtkFloatingPointType MaxSize;
};


//============================================================================
//! Class of Outside Cursor Gauss Points settings.
/*!
 * Contains information about the point sprite parameters:
 * Clamp, Texture, Alpha threshold, Const size and Color.
 * Used by Gauss Points Actor.
 */
class VISU_OBJECT_EXPORT VISU_OutsideCursorSettings : public VISU_GaussPtsSettings
{
 public:
  vtkTypeMacro( VISU_OutsideCursorSettings, vtkObject );

  VISU_OutsideCursorSettings();
  virtual ~VISU_OutsideCursorSettings();

  static
  VISU_OutsideCursorSettings*
  New();

  vtkSetMacro( Size, vtkFloatingPointType );
  vtkGetMacro( Size, vtkFloatingPointType );

  vtkSetMacro( Uniform, bool );
  vtkGetMacro( Uniform, bool );

  vtkSetVector3Macro( Color, vtkFloatingPointType );
  vtkGetVector3Macro( Color, vtkFloatingPointType );

 protected:
  vtkFloatingPointType Size;
  bool                Uniform;
  vtkFloatingPointType Color[3];
};

#endif
