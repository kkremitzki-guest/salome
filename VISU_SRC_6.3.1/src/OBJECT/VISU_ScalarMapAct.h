// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : VISU_ScalarMapAct.h
//  Author : Laurent CORNABE with help of Nicolas REJNERI
//  Module : VISU
//  $Header: /home/server/cvs/VISU/VISU_SRC/src/OBJECT/VISU_ScalarMapAct.h,v 1.8.2.3.6.3.4.1 2011-06-02 06:00:17 vsr Exp $
//
#ifndef VISU_ScalarMapAct_HeaderFile
#define VISU_ScalarMapAct_HeaderFile

#include "VISU_OBJECT.h"
#include "VISU_DataSetActor.h"

namespace VTK
{
  const MarkerType MT_POINT_SPRITE = MT_USER + 1;
}

class VISU_ScalarBarActor;
class VISU_PointsDeviceActor;

//----------------------------------------------------------------------------
class VISU_OBJECT_EXPORT VISU_ScalarMapAct : public VISU_DataSetActor 
{
 public:
  vtkTypeMacro(VISU_ScalarMapAct,VISU_DataSetActor);

  static
  VISU_ScalarMapAct* 
  New();

  ~VISU_ScalarMapAct();

  //! Copies all properties from the given actor
  virtual
  void
  DeepCopy(VISU_Actor *theActor);

  virtual
  void
  ShallowCopyPL(VISU_PipeLine* thePipeLine);

  //! Apply view transformation
  virtual
  void
  SetTransform(VTKViewer_Transform* theTransform); 

  virtual
  vtkProperty* 
  GetEdgeProperty(); 

  virtual
  void
  SetShrinkable(bool theIsShrinkable);

  virtual
  void
  SetShrinkFactor(vtkFloatingPointType theFactor = 0.8); 

  virtual
  void
  SetShrink(); 

  virtual
  void
  UnShrink(); 

  virtual
    EQuadratic2DRepresentation GetQuadratic2DRepresentation() const;
  
  virtual void 
    SetQuadratic2DRepresentation( EQuadratic2DRepresentation theMode );

  
  virtual
  void
  SetFeatureEdgesAllowed(bool theIsFeatureEdgesAllowed);

  virtual
  void
  SetFeatureEdgesEnabled(bool theIsFeatureEdgesEnabled);

  virtual
  void
  SetFeatureEdgesAngle(vtkFloatingPointType theAngle = 30.0); 

  virtual
  void
  SetFeatureEdgesFlags(bool theIsFeatureEdges,
                       bool theIsBoundaryEdges,
                       bool theIsManifoldEdges,
                       bool theIsNonManifoldEdges);

  virtual
  void
  SetFeatureEdgesColoring(bool theIsColoring);

  virtual
  void
  SetOpacity(vtkFloatingPointType theValue);

  virtual
  vtkFloatingPointType
  GetOpacity();

  virtual
  void
  SetLineWidth(vtkFloatingPointType theLineWidth);

  virtual
  vtkFloatingPointType
  GetLineWidth();

  virtual
  void
  AddToRender(vtkRenderer* theRenderer); 

  virtual
  int
  RenderOpaqueGeometry(vtkViewport *ren);

  virtual
  int
#if (VTK_XVERSION < 0x050100)
  RenderTranslucentGeometry(vtkViewport *ren);
#else
  RenderTranslucentPolygonalGeometry(vtkViewport *ren);

  virtual
  int
  HasTranslucentPolygonalGeometry();
#endif

  virtual
  void
  RemoveFromRender(vtkRenderer* theRenderer);

  virtual
  void
  SetVisibility(int theMode);

  virtual
  int
  GetBarVisibility();

  virtual
  vtkFloatingPointType 
  Get0DElemSize();

  virtual
  VISU_ScalarBarActor* 
  GetScalarBar();

  virtual
  void
  SetBarVisibility(bool theMode);

  virtual
  void
  SetRepresentation(int theMode);

  virtual
  void
  SetShading(bool theOn = true);

  virtual
  bool
  IsShading();

  //! Gets memory size used by the instance (bytes).
  virtual
  unsigned long int
  GetMemorySize();

  virtual
  void
  SetMarkerStd( VTK::MarkerType, VTK::MarkerScale );

  virtual
  void
  SetMarkerTexture( int, VTK::MarkerTexture );

  virtual
  void
  Set0DElemSize(vtkFloatingPointType theValue);


 protected:
  VISU_ScalarMapAct();

  virtual 
  void
  SetMapperInput(vtkDataSet* theDataSet);

  bool myBarVisibility;
  VISU_ScalarBarActor* myScalarBar;

  VISU_PointsDeviceActor* myPointSpriteActor;
  SVTK_DeviceActor*    myPointsActor;
  SVTK_DeviceActor*    mySurfaceActor;
  SVTK_DeviceActor*    myEdgeActor;

  bool myIsPointSpriteMode;
};
//----------------------------------------------------------------------------


#endif
