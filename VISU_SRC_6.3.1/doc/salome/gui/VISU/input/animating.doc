/*!

\page animating_page Animation

\image html animatdef.jpg "Example of Animation presentations for deformed shape"

\n In \b Post-Pro module you can generate real-time animation of
fields. In comparison with sweeping, animations are created on the
base of frames, displaying field presentations generated on \b EVERY
timestamp of your field. So, the frame displaying the field
presentation created on the base of the first timestamp will begin
your animation and the frame displaying the field presentation created
on the base of the last timestamp will end it. In sweeping <b>ONLY
ONE</b> timestamp is used for its generation.

In the case of animation of several fields <b>Parallel Animation</b>
will display the animations simultaneously, while <b>Successive
Animation</b> will display them one after another.
\n In the case of <b>Parallel Animation</b> you can set different
kinds of presentations for each of the selected fields. This animation
type also requires that the number of timestamps > 1 and is the same
for all animated fields.
\n For <b>Successive Aimation</b> the kind of presentation should be
the same for all selected fields, which means that it is not possible
to select a field from the list.  It is also necessary that the number
of timestamps > 0 and the number of components is the same for all
animated fields.

\ref slider_page functionality also allows to create real-time
animation on every time stamp of the field. This is another,
somewhat more user-friendly, implementation of animation on fields.

<em>To generate an Animation:</em>
\par
<ol>
<li>Right-click on the field in the Object Browser.</li>

<li>From the pop-up menu select \b Parallel or <b>Successive
Animation</b>.
The following dialog box will appear:

\image html animation.png

\b Animation dialog box is destined for management of your animation.
</li>
<li>Click <b>Setup Animation</b> button. The following dialog box
allowing to set the parameters of your animation will appear.

\image html setupanimation1.png

&ndash; <b>Use range of time stamps</b> check box allows to select the range
of time stamps, which will be used for generating your animation.

\image html setupanimation2.png

&ndash; <b>Use sequence of time stamps</b> check box allows to select
the time stamps, which will be used for generating your presentation
from the list of time stamps

&ndash; \b Fields: list of fields for animation.

&ndash; \b Properties: Here you can adjust properties of each field
selected from the list:
<ul>
<li>Select the \b type of field presentations, which will be used as
frames in animation.</li>
<li>Set properties of the <b>Scalar Bar</b> (\ref scalar_map_page "see also").</li>
<li>Adjust additional \b properties of the selected type of field presentation.</li>
</ul>
<br>
</li>
<li>Click \b OK to validate your parameters. You will return back to
\b Animation dialog box.</li>

<li>Click <b>Generate frames</b> button to generate the frames for
your animation based on the parameters adjusted in <b>Setup
Animation</b> dialog box.</li>
</ol>

<br><em>Running animations:</em>
\par
In the main \b Animation dialog box there are several button intended
for management and running your animation:

\image html run.jpg
<center><b>Running</b> your animation</center>

\image html previous.jpg
<center><b>Previous</b> frame (time step)</center>

\image html next.jpg
<center><b>Next</b> frame (time step)</center>

\image html begining.jpg
<center><b>To the beginning</b> of the animation.</center>

\image html end.jpg
<center><b>To the end</b> of the animation.</center>

<br><em>Additional options of the animation:</em>
\par
You can <b>increase/decrease</b> the speed of your animation by moving
the \b Speed wheel \b Up/Down:

\image html speed.jpg

\par
<b>Cycled animation</b> check box: this option allows to start a
cycled animation of your presentation.
<br><br>
<b>Use proportional timing</b> check box: this option allows to render
your animation with proportional periods of time between every frame
(not depending on the time stamps).
<br><br>
<b>Time stamp frequency</b> spin box: this option is available if
<b>Save animation to AVI file</b> mode is turned on. It provides a
possibility to capture only timestamps with indices divisible by the
specified frequency, which allows to generate less voluminous films.
<br><br>
<b>Clean memory at each frame</b> - this option allows to optimize the
performance of the operation.

\note <b>Save animation to AVI file</b> works only if there is \b
jpeg2yuv utility installed on your computer. If this third-party
product is missing, the check-box will not be selectable. This utility
is a part of \b mjpegtool package, which can be downloaded from
http://mjpeg.sourceforge.net/.  Run "which jpeg2yuv" to check whether
this tool has been installed and the path to it was properly added to
your PATH variable. If not, you need to activate \b mjpegtools
indicating its location in the variable PATH. The syntax of the
command should be as follows: export
PATH=${INSTALL_ROOT}/mjpegtools-1.8.0/bin:${PATH}.

\par
<b>Save Animation</b> - saves changes made in the published
animation. If you have already published a study and press <b>Publish
to study</b> again you get another animation in the Object Browser.
<br><br>
<b>Publish to Study</b> - saves your animation in the study and
presents it in the Object Browser.

<br><b>See Also</b> a sample script of using
\ref tui_animation_page "Animation" via TUI.

*/
