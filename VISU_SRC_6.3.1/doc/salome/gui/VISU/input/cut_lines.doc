/*!

\page cut_lines_page Cut Lines presentation
\image html cutlinesprsnt.png "Example of Cut Lines presentation"

\n <b>Cut Lines</b> is a type of presentation which displays colored
cells with applied scalar values on the mesh where lines are placed.
\n The procedure of construction of <b>Cut Lines</b> presentation
reuses the algorithm of creation <b>Cut Planes</b> presentation and
consists of two steps:
<ol>
<li>From <b>Cut Planes</b> presentation one plane is taken and it is
used as base plane for construction of cut lines.</li>
<li>This plane is cut by a regular array of planes. The result of this
operation is a regular array of lines in space,  belonging to the same
plane and having the same orientation. They are located inside or on
the mesh.</li>
</ol>

<em>To create a Cut Lines presentation:</em>
\par
&ndash; Right-click on one of the time stamps of the field in the
Object browser and from the pop-up menu choose <b>Cut Lines</b>, or
\n &ndash; Click on one of the time stamps of the field in the Object
browser and select from the main menu <b>Visualization > Cut
Lines</b>, or click <em>"Cut Lines"</em> icon in the <b>Visualization
Toolbar</b>.

\image html cutlinesicn.jpg
<center><em>"Cut Lines" icon</em></center>

\image html cutlines.png

\par
<b>Plane of lines:</b> this tab of the dialog box contains the
parameters of the base plane, on which the cut lines will be located.
<ul>
<li><b>Orientation</b> check boxes allow to set the orientation of cut
planes in 3D space. You should select two of three axis (XY, YZ, or
ZX) in which your planes will be located in 3D space.</li>
<li><b>Rotations</b> of the planes in 3d space around the axes of the
selected orientation. (The angle of rotation is set in degrees)</li>
<li><b>Base plane position:</b> position of the base plane. This value
can be set by default or entered manually.</li>
<li><b>Displacement</b> of the plane. This parameter defines position
of the base plane in 3d space.</li>
<li><b>Show preview</b> check box allows to edit the parameters of the
presentation and simultaneously observe the preview of this
presentation in the viewer.</li>
<li><b>Invert all curves</b> check box allows to invert the resulting
curves.</li>
<li><b>Use absolute length</b> check box allows to see the real length
of the line, instead of [0,1] interval.</li>
<li><b>Generate Data Table:</b> If this check box is marked, Post-Pro
will automatically generate a data table on the basis of your Cut
Lines presentation. This table will be created in the structure of the
study.</li>
<li><b>Generate Curves:</b> If this check box is marked, Post Pro
will automatically generate curve lines on the basis of values taken
from the generated data table. These curves will be created in the
structure of the study and can be visualized in a XY plot.</li>
</ul>
See more about table presentations and curve lines 
\ref table_presentations_page "here".

\par
<b>Cut Planes:</b> this tab of the dialog box contains the parameters of cut
planes, which will be used for construction of the cut lines.
<br><br>
<b>Scalar Bar</b> tab allows to define the parameters of the scalar bar
displayed with this presentation (\ref scalar_map_page "see also").

After you have finished with setting these parameters, click \b
OK. Your presentation with scalar bar will be immediately displayed in
the viewer:

<b>Tip:</b> From <b>Cut Lines</b> presentation you can create a
<b>data table</b>. This table will consist of the field scalar values
located on each cut line of the constructed presentation (see also:
\ref creating_tables_from_cut_lines "Creating tables from CutLines presentations").
After that your data table can be used for
construction of a 2d plot of curves based on the scalar values from
the table (see also: \ref creating_curves_page "Creating curves" and 
\ref creating_plot2d_page "Creating Plot 2D presentation").

<br><b>See Also</b> a sample TUI Script of  
\ref tui_cut_lines_page "Cut Lines" presentation operation.

*/
