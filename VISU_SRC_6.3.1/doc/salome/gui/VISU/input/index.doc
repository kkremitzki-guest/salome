/*!

\mainpage Introduction to Post-Pro

\image html visufullscreen.png

<b>Post-Pro</b> module of Salome is used for analysis and post-processing  of numerical simulation results. 
 
\n The major part of \b Post-Pro functionality is dedicated to post-processing of files in MED format.
A MED file contains the description of a  \subpage med_object_page "MED object", which represents a \b mesh and a set of \b physical \b values corresponding to mesh cells.

\n For compability with other solver solutions \b Post-Pro supports
 \subpage table_presentations_page "Data Table Objects",
 two-dimensional data arrays, as input type for visualization. 

\n The Python interface of \b Post-Pro module is described in \subpage
idl_interface_page "Access to Post-Pro module functionality" page.
\n Sample Python scripts can be found in \subpage python_examples_page chapter.

*/
