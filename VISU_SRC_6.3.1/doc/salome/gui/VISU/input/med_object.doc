/*!

\page med_object_page MED Objects

\b MED \b Object is a specific twofold data structure
with consists of \b  Mesh \b Data and \b Physical \b Data.

- \b Mesh \b Data includes geometrical information: list of nodes, edges, faces and cells
of the mesh, and information on the interpolation in accordance with resulting
elements, i.e. the mesh should be compatible with the chosen interpolation.
- \b Physical \b Data contains initial and boundary conditions: scalar
or vector values applied to the mesh cells.

It is not possible to create and edit \b MED \b Objects in the \b
Post-Pro module. You can only \subpage importing_med_objects_page
"import a MED object" from Salome MED module or from a MED file.

\b MED Objects can be visualized and explored with various types of
\subpage a3d_presentations_page "3D presentations".

\b Post-Pro visualization engine bases on VTK and OpenGL
technologies, which provide useful tools for 
\subpage a3d_management_page "Management of presentations".

As it can be seen, the description of a MED Object is written in a tree-like form,
containing three main folders: \ref families_anchor "Families", 
\ref groups_anchor "Groups", and \ref fields_anchor "Fields".

\image html snapfgf.png

<br>
\anchor families_anchor
<h2>Families</h2>

A \b Family is a user-defined submesh composed of cells of a
definite type  - Nodes, Edges, Faces or Cells. The
presentation created on the basis of a particular \b family will be
composed of geometrical elements, corresponding to the type of cells
of this \b family.

<center>
<table>
<tr>
<td><b>Type of cells (entities) of the family</b></td>
<td><b>Geometrical element</b></td>
</tr>
<tr>
<td>Nodes</td>
<td>Points</td>
</tr>
<tr>
<td>Edges</td>
<td>Segments</td>
</tr>
<tr>
<td>Faces</td>
<td>Triangles, quadrangles</td>
</tr>
<tr>
<td>Cells</td>
<td>Any</td>
</tr>
</table>
<em>Table of correspondence</em>
</center>

In the \b Families  folder all families of the mesh are put into subfolders defining the type of cells of the mesh: \b onNodes,
\b onEdges, \b onFaces, \b onCells.
\n The subfolders also represent submeshes of the mesh, which are
composed of all cells of the mesh of this type.

<br>
\anchor groups_anchor
<h2>Groups</h2>

\b Groups combine families independently on the type of
cells, of which they are composed.

<br>
\anchor fields_anchor
<h2>Fields</h2>

\b Fields represent the results of calculations (scalar or vector
values), grouped under one physical concept.
\n These values are applied to the cells of a definite submesh, which
is indicated in the Object Browser in the subfolder of the \b field. 
\n Most often the calculations are performed during some period of
time. That's why \b fields include <b>Time Stamps</b>, representing
the results of calculations in one definite moment. 

*/
