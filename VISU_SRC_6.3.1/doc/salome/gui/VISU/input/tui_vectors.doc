/*!

\page tui_vectors_page Vectors

Create Vectors on the field of the imported MED file:

\code
import os

import salome

import VISU
import visu_gui

# Directory containing MED files
datadir = os.getenv("DATA_DIR")

# Get VISU engine
myVisu = visu_gui.myVisu

# Import MED file
medFile = os.path.join(datadir,"MedFiles","fra.med")
myResult = myVisu.ImportFile(medFile)

# Create vectors for the first timestamp of 'VITESSE' field
meshName = 'LE VOLUME'
fieldEntity = VISU.NODE
fieldName = 'VITESSE'
myVectors = myVisu.VectorsOnField(myResult, meshName, fieldEntity, fieldName, 1)

# Set scale factor
myVectors.SetScale(0.1)

# Set magnitude coloring
myVectors.ShowColored(True)

# Set line width
myVectors.SetLineWidth(2)

# Set the type of representation of the vector head (arrow)
myVectors.SetGlyphType(VISU.Vectors.ARROW)

# Set the position of the vector head (center)
myVectors.SetGlyphPos(VISU.Vectors.CENTER)

# Update object browser
salome.sg.updateObjBrowser(1)

# Display just created vectors
myViewManager = myVisu.GetViewManager()
myView = myViewManager.Create3DView()

myView.DisplayOnly(myVectors)
myView.FitAll()
\endcode

<br>Please, see \ref VISU.Vectors "Vectors interface reference documentation" 
for more details.

*/