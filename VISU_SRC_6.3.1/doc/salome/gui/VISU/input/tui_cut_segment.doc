/*!

\page tui_cut_segment_page Cut Segment

Create <b>Cut Segment</b> presentation on a field of the imported MED file:

\code
import os

import salome

import VISU
import visu_gui

# The directory containing MED files
datadir = os.getenv("DATA_DIR")

# Get VISU engine
myVisu = visu_gui.myVisu

# Import a MED file
medFile = os.path.join(datadir,"MedFiles","fra.med")
myResult = myVisu.ImportFile(medFile)

# Create a cut segment for the first timestamp of 'VITESSE' field
meshName = 'LE VOLUME'
fieldEntity = VISU.NODE
fieldName = 'VITESSE'
myCutSegment = myVisu.CutSegmentOnField(myResult, meshName, fieldEntity, fieldName, 1)

# Set the first point of the segment
myCutSegment.SetPoint1(0, 0, 0)

# Set the second point of the segment
myCutSegment.SetPoint2(0.41, 0.0685, 1.082)

# Invert the resulting curve
myCutSegment.SetAllCurvesInverted(True)

# Update the object browser
salome.sg.updateObjBrowser(1)

# Display the newly created cut segment
myViewManager = myVisu.GetViewManager()
myView = myViewManager.Create3DView()

myView.DisplayOnly(myCutSegment)
myView.FitAll()
\endcode

<br>Please, see \ref VISU.CutSegment "CutSegment interface reference documentation" 
for more details.

*/
