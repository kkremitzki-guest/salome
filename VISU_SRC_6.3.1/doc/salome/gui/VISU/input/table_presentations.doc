/*!

\page table_presentations_page DataTable object

<b>DataTable object</b> is a way to represent a two-dimensional data array
(data arranged in rows and columns) in Salome. Each column groups values expressed in certain measurement \b units.
All \b columns and <b>rows</b>  have  <b>titles</b>. Tables can be \b
rarefied, which means that some cells may be empty, without any
values.

<b>DataTable object</b> can be created directly, with the usage of a
special programming API (this problem lies out of the scope of Salome
GUI Help), or \subpage importing_exporting_tables_page "imported"
from properly defined ACSII files. 

\anchor creating_tables_from_cut_lines

\note Alternatively, it is possible to  create a table from
the scalar values applied to the cells forming a \ref cut_lines_page "Cut Lines"
or a \ref cut_segment_page "Cut Segment" presentation. Simply right-click on the 
presentation in the Object Browser or in the viewer and select <b>Create Table</b>
from the pop-up menu.

Tables are not displayed automatically. To view the imported table,
right-click on it in the Object Brower and select <b>Show Table</b>
in the context menu.

\image html viewtable.png

When the <b>Enable editing</b> option is checked, the table contents are editable.
\note At the current moment this mode only allows to sort table data.
The sorting is performed by clicking on the header of the column, by which the data
is sorted.

<b>Sort policy</b> option allows to specify how the empty cells will be processed
during the sort procedure. The following options are available:
<ul>
<li><b>Empty cells are considered as lowest values</b></li>
<li><b>Empty cells are considered as highest values</b></li>
<li><b>Empty cells are always first</b></li>
<li><b>Empty cells are always last</b></li>
<li><b>Empty cells are ignored</b> (this means that the positions of empty cells
will not change after the sorting)</li>
</ul>

<b>Adjust Cells</b> button allows to adjust the size of table cells according
to their contents.

It is also possible to create 
\subpage table_3d_page "Table 3D presentation" basing on the table
data and display it in the VTK viewer.

Another way of exploiting table data is 
\subpage creating_curves_page "creating curves" and
 \subpage creating_plot2d_page "displaying them in the Plot 2D viewer".
Plot 2D viewer is described in the documentation on GUI module. 
 


*/
