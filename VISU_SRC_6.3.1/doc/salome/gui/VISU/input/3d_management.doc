/*!

\page a3d_management_page Managing 3D Presentations

After the 3D presentation is created, it can be edited or submitted to
various post-processing operations. 

The operations connected with the presentation-handling in the VTK
Viewer and in the Object Browser via context menus are described in
\subpage viewing_3d_presentations_page "Viewing 3D presentations" section.

Some more specific functionalities are available via dockable windows
accessed from the Main menu <b>View -> Windows </b>

\image html presentation1.png

<ul>
<li><b> Object Browser</b> - hides and displays the Object browser
used for management of objects created or imported into Salome application</li>
<li><b> Python Console</b> - hides and displays the Python console,
which is a window for Python interpreter.</li>
<li> \subpage slider_page - used for animation of presentations created on
all timestamps of a field.</li>
<li> \subpage sweeping_page "Sweep" -  used for pseudo-animation of presentations created on
one timestamp of a field.</li>
<li> \subpage clipping_page "Clipping Planes" - allows to create cross-section of
the presentation.</li> 
<li> \subpage selection_info_page "Selection" - allows to view the
attributes of elements displayed in the viewer.</li>
<li> \subpage feature_edges_page "Feature Edges" - allows to choose the
edges selectable for <b>Feature Edges</b> functionality.</li>
</ul>

Other important presentation management functionalities characteristic for Post-Pro
module are:
<ul>
<li> \subpage animating_page - another way of animation of
presentations on fields. </li>
<li> \subpage evolution_page - tracing of temporal evolution of a
variable at a given point.</li>
<li> \subpage point_marker_page - allows to change the representation
of points, using standard or custom markers.</li>
<li> \subpage translate_presentation_page - displacement of
presentations in the viewer.</li>
<li> \subpage recording_page - allows to dump user actions in an AVI
file. </li>
<li> \subpage navigation_page - about advanced user operation modes in the
VTK viewer (using keyboard only, mouse only or spacemouse). </li> 
<li> \subpage perspective_view_page and the possibility to set camera
position and focal point of the scene manually provide flexibility of
viewing the presentation.</li> 
</ul>


*/

