/*!

\page navigation_page Navigation in the VTK viewer

\n VTK viewer in Post-pro module provides two ways of user style
navigation modes: \b Standard and \b Advanced. Switching between them
is allowed with <b>Interaction Style Switch</b> button from Viewer Toolbar:

<br><h2>Navigation using the mouse</h2>

<h3>Standard</h3> user interaction style supports processing of mouse
and keyboard events with the following assignments:

<table>
<tr>
<td><center><b>Action</b></center></td><td><center><b>Command</b></center></td><td><center><b>Movement</b></center></td>
</tr>
<tr>
<td>Dynamic rotation</td>
<td><center><b>[CTRL] key + Right Mouse Button</b></center></td>
<td>\image html image46.gif 
<center>Rotation about the focal point</center>
</td>
</tr>
<tr>
<td>Dynamic panning</td>
<td><center><b>[CTRL] key + Central Mouse button</b></center></td>
<td>\image html image47.gif
<center>Translation in any direction</center>
</td>
</tr>
<tr>
<td>Dynamic zoom</td>
<td><center><b>[CTRL] key + Left Mouse Button</b></center></td>
<td>\image html image48.gif
<center>Increase or decrease of zoom ratio</center>
</td>
</tr>
</table>

<h3>Advanced or "keyboard free"</h3> style makes it possible to manipulate objects without
keyboard keys, 

<table>
<tr>
<td><center><b>Action</b></center></td><td><center><b>Command</b></center></td><td><center><b>Movement</b></center></td>
</tr>
<tr>
<td>Dynamic rotation</td>
<td><center><b>Right Mouse Button</b></center></td>
<td>\image html image46.gif 
<center>Rotation about the focal point</center>
</td>
</tr>
<tr>
<td>Dynamic panning</td>
<td><center><b>Central Mouse button</b></center></td>
<td>\image html image47.gif
<center>Translation in any direction</center>
</td>
</tr>
<tr>
<td>Dynamic zoom</td>
<td><center><b>Left Mouse Button + Central Mouse button</b></center></td>
<td>\image html image49.gif
<center>Increase or decrease of zoom ratio</center>
</td>
</tr>
</table>

\ref selection_info_page  also depends on the user interaction
style. With the \b Standard style Selection is activated by pressing
left mouse button, while with the \b Advanced style selection is done
by pressing \b S key.

It is possible to customize the sencibility of each view operation
(rotation, panning and zooming) by pressing \b + or \b - keys to
increase or decrease the speed.   



<br><h2>Navigation using the keyboard</h2>

VTK viewer also allows to manipulate objects using keyboard keys.

<table>
<tr>
<td><center><b>Action</b></center></td><td><center><b>Key</b></center></td><td><center><b>Movement</b></center></td>
</tr>
<tr>
<td>Dynamic rotation</td>
<td><center><b>[CTRL] key + \n Left Arrow, Right Arrow , \n Up Arrow, Down Arrow</b></center></td>
<td>\image html image46.gif 
<center>Rotation about the focal point</center>
</td>
</tr>
<tr>
<td>Dynamic panning</td>
<td><center><b>Left Arrow, Right Arrow, \n Up Arrow, Down Arrow</b></center></td>
<td>\image html image47.gif
<center>Translation in any direction</center>
</td>
</tr>
<tr>
<td>Dynamic zoom</td>
<td><center><b>[PageUp], \n [PageDn]</b></center></td>
<td><center>Increase or decrease of zoom ratio</center></td>
</tr>
<tr>
<td>Speed increment</td>
<td><center><b>[ + ], \n [ - ]</b></center></td>
<td><center>Increase or decrease by 1 of the speed increment for the previously defined movement.</center></td>
</tr>
</table>

<br><h2>Navigation using the spacemouse</h2>

VTK viewer also allows to manipulate objects using a spacemouse.

The spacemouse introduces a local coordinate system and 6 degrees of
freedom, which are independent from the 3D view coordinate system.

\image html image50.gif

The movement of 3D models in the screen depends on the user
manipulations with the controller head.

<table>
<tr>
<td><center><b>Action</b></center></td><td><center><b>Move</b></center></td><td><center><b>Description</b></center></td>
</tr>
<tr>
<td>Dynamic rotation \n about axis X</td>
<td>\image html image51.gif</td>
<td><center><b>Tilt</b> the controller head forward and backward to rotate the object about axis X.</center></td>
</tr>
<tr>
<td>Dynamic rotation \n about axis Y</td>
<td>\image html image52.gif</td>
<td><center><b>Rotate</b> the controller head clockwise and counterclockwise to rotate the model about axis Y.</center> 
</td>
</tr>
<tr>
<td>Dynamic panning</td>
<td>\image html image53.gif</td>
<td><center><b>Pull</b> up and <b>push</b> down to move the model up and down.
\n <b>Move left</b> and <b>right</b> to move the model left and right.</center>
</td>
</tr>
<tr>
<td>Dynamic zoom</td>
<td>\image html image54.gif</td>
<td><center><b>Push forward</b> or <b>pull backward</b> to respectively decrease or increase the zoom ratio.</center>
</td>
</tr>
</table>

Buttons of the spacemouse are also used to change the magnification
for Gauss points:

\image html image55.gif

By default, buttons are defined as follows, however the user can
change them in the preferences:

<table>
<tr> 
<td><center><b>Button</b></center></td>
<td><center><b>Description</b></center></td>
</tr>
<tr>
<td><center>Button 1</center></td>
<td>Decrease by 1 speed increment used for the keyboard (the same as [-] key)</td>
</tr>
<tr>
<td><center>Button 2</center></td>
<td>Increase by 1 speed increment used for the keyboard (the same as [+] key)</td>
</tr>
<tr>
<td><center>Button 10</center></td>
<td>Divide the current magnification by the magnification ratio</td>
</tr>
<tr>
<td><center>Button 11</center></td>
<td>Multiply the current magnification by the magnification ratio</td>
</tr>
<tr>
<td><center>Button Star</center></td>
<td>Toggle button to switch to dominant or combined movements</td>
</tr>
</table>

*/