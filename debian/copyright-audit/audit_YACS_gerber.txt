Audit files of Salome YACS module
Number of files: 1579

Statements are found in audit_C_STATEMENTS_gerber.txt

==== BINARI:
./src/batch/*.wav
./src/genericgui/resources/*.png
./src/wrappergen/src/HXX2SALOME_GENERIC_CLASS_NAME_SRC/resources/*.png
./src/pyqt/gui/icons/*.png, *.gif
./src/salomegui/resources/*.png

==== EMPTY:
./AUTHORS
./ChangeLog
./src/wrappergen/src/HXX2SALOME_GENERIC_CLASS_NAME_SRC/AUTHORS
./src/wrappergen/src/HXX2SALOME_GENERIC_CLASS_NAME_SRC/ChangeLog
./src/wrappergen/src/HXX2SALOME_GENERIC_CLASS_NAME_SRC/README
./src/wrappergen/src/HXX2SALOME_GENERIC_CLASS_NAME_SRC/NEWS
./src/wrappergen/bin/Cpp_Template__SRC/NEWS
./doc/ref/*.png
./doc/images/*.png, *.jpg

==== NO LICENCE:
./INSTALL
./README
./src/batch/*.py
./src/batch/*.xml
./src/batch/*.txt
./src/genericgui/Message.hxx
./src/genericgui/YACSGuiLoader.cxx
./src/genericgui/*.ui
./src/genericgui/journal
./src/genericgui/YACSGuiLoader.hxx
./src/salomeloader/ElementTree.py
./src/wrappergen/src/HXX2SALOME_GENERIC_CLASS_NAME_SRC/adm_local/unix/config_files/README
./src/wrappergen/src/HXX2SALOME_GENERIC_CLASS_NAME_SRC/resources/config
./src/wrappergen/src/HXX2SALOME_GENERIC_CLASS_NAME_SRC/resources/HXX2SALOME_GENERIC_CLASS_NAME_en.ps
./src/wrappergen/src/HXX2SALOME_GENERIC_CLASS_NAME_SRC/doc/dev_guide.txt
./src/wrappergen/src/HXX2SALOME_GENERIC_CLASS_NAME_SRC/bin/VERSION
./src/wrappergen/bin/HXX2SALOME_GENERIC_CLASS_NAME_SRC/adm_local/unix/config_files/README
./src/wrappergen/bin/HXX2SALOME_GENERIC_CLASS_NAME_SRC/resources/config
./src/wrappergen/bin/HXX2SALOME_GENERIC_CLASS_NAME_SRC/resources/HXX2SALOME_GENERIC_CLASS_NAME_en.ps
./src/wrappergen/bin/HXX2SALOME_GENERIC_CLASS_NAME_SRC/doc/dev_guide.txt
./src/wrappergen/bin/HXX2SALOME_GENERIC_CLASS_NAME_SRC/bin/VERSION
./src/wrappergen/bin/Deprecated
./src/wrappergen/bin/Cpp_Template__SRC/root_clean
./src/wrappergen/bin/Cpp_Template__SRC/AUTHORS
./src/wrappergen/bin/Cpp_Template__SRC/archive
./src/wrappergen/bin/Cpp_Template__SRC/ChangeLog
./src/wrappergen/bin/Cpp_Template__SRC/README
./src/wrappergen/bin/Cpp_Template__SRC/rfind
./src/yacsloader/samples/stream2.xml
./src/yacsloader/samples/stream3.xml
./src/yacsloader/samples/f.data
./src/yacsloader/samples/setports.xml
./src/yacsloader/samples/stream1.xml
./src/yacsloader/samples/optimizer1.xml
./src/yacsloader/samples/stream4.xml
./src/yacsloader/samples/optimizer2.xml
./src/yacsloader/schema.xsd
./src/bases/chrono.hxx
./src/bases/chrono.cxx
./src/yacsloader_swig/Test/testRefcount.py
./src/salomegui/Yacsgui_Resource.cxx
./src/salomegui/resources/YACSSchemaCatalog.xml
./src/salomegui/resources/YACS_icons.ts
./src/yacsorb/README.txt
./NEWS
./doc/*
./Misc/doxy2swig.py
./build_cmake.bat

==== STATEMENT-1:
./adm/unix/config_files/libtool.m4

==== STATEMENT-7:
./src/wrappergen/src/HXX2SALOME_GENERIC_CLASS_NAME_SRC/INSTALL


==== STATEMENT-10:
./src/wrappergen/src/HXX2SALOME_GENERIC_CLASS_NAME_SRC/doc/rst.css

==== STATEMENT-11:
./adm/unix/config_files/ltversion.m4
./adm/unix/config_files/lt~obsolete.m4

==== STATEMENT-12:
./adm/unix/config_files/ltoptions.m4
./adm/unix/config_files/ltsugar.m4

==== GPL2:
./src/wrappergen/src/HXX2SALOME_GENERIC_CLASS_NAME_SRC/adm_local/unix/py-compile
./src/wrappergen/src/HXX2SALOME_GENERIC_CLASS_NAME_SRC/COPYING: GPL 2 License
./src/wrappergen/src/HXX2SALOME_GENERIC_CLASS_NAME_SRC/py-compile
./COPYING: GPL 2 License

==== Files not mentioned above contain LGPL 2.1 statement:
./Makefile.am
./src/salomegui_swig/*
./src/hmi/*
./src/genericgui/*.hxx, *.cxx
./src/salomeloader/*
./src/wrappergen/src/*
./src/yacsloader/*
./src/bases/*
./src/pyqt/gui/*
./src/yacsloader_swig/*
./src/salomegui/*
./src/engine/*
./src/runtime/*
./src/yacsorb/*
./clean_configure
./idl/*
./adm/*
./build_configure
./mkinstalldirs
./configure.in.base
./build_cmake
./rfind
./doc/Doxyfile
./doc/Makefile.am
./doc/Doxyfile
./Demo/*
./py-compile
