<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="en_US">
<context>
    <name>@default</name>
    <message>
        <source>MEN_FILE</source>
        <translation>&amp;File</translation>
    </message>
    <message>
        <source>MEN_FILE_HELLO</source>
        <translation>Hello</translation>
    </message>
    <message>
        <source>MEN_GET_BANNER</source>
        <translation>Get banner</translation>
    </message>
    <message>
        <source>MEN_HELLO</source>
        <translation>HELLO</translation>
    </message>
    <message>
        <source>MEN_MY_NEW_ITEM</source>
        <translation>My menu item</translation>
    </message>
    <message>
        <source>STS_GET_BANNER</source>
        <translation>Get HELLO banner</translation>
    </message>
    <message>
        <source>STS_MY_NEW_ITEM</source>
        <translation>Call my menu item</translation>
    </message>
    <message>
        <source>TLT_GET_BANNER</source>
        <translation>Get HELLO banner</translation>
    </message>
    <message>
        <source>TLT_MY_NEW_ITEM</source>
        <translation>My menu item</translation>
    </message>
    <message>
        <source>TOOL_HELLO</source>
        <translation>HELLO</translation>
    </message>
</context>
<context>
    <name>HELLOGUI</name>
    <message>
        <source>BUT_OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <source>INF_HELLO_BANNER</source>
        <translation>HELLO Information</translation>
    </message>
    <message>
        <source>INF_HELLO_MENU</source>
        <translation>This is just a test</translation>
    </message>
    <message>
        <source>QUE_HELLO_LABEL</source>
        <translation>Name Import</translation>
    </message>
    <message>
        <source>QUE_HELLO_NAME</source>
        <translation>Please, Enter your name</translation>
    </message>
</context>
</TS>
