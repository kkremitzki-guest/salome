<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr_FR">
<context>
    <name>@default</name>
    <message>
        <source>MEN_FILE</source>
        <translation>&amp;Fichier</translation>
    </message>
    <message>
        <source>MEN_FILE_HELLO</source>
        <translation>Hello</translation>
    </message>
    <message>
        <source>MEN_GET_BANNER</source>
        <translation>Recevoir la bannière</translation>
    </message>
    <message>
        <source>MEN_HELLO</source>
        <translation>HELLO</translation>
    </message>
    <message>
        <source>MEN_MY_NEW_ITEM</source>
        <translation>Ma commande de menu</translation>
    </message>
    <message>
        <source>STS_GET_BANNER</source>
        <translation>Recevoir la bannière HELLO</translation>
    </message>
    <message>
        <source>STS_MY_NEW_ITEM</source>
        <translation>Evoquer ma commande de menu</translation>
    </message>
    <message>
        <source>TLT_GET_BANNER</source>
        <translation>Recevoir la bannière HELLO</translation>
    </message>
    <message>
        <source>TLT_MY_NEW_ITEM</source>
        <translation>Ma commande de menu</translation>
    </message>
    <message>
        <source>TOOL_HELLO</source>
        <translation>HELLO</translation>
    </message>
</context>
<context>
    <name>HELLOGUI</name>
    <message>
        <source>BUT_OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <source>INF_HELLO_BANNER</source>
        <translation>Information HELLO</translation>
    </message>
    <message>
        <source>INF_HELLO_MENU</source>
        <translation>Ce n&apos;est qu&apos;un test</translation>
    </message>
    <message>
        <source>QUE_HELLO_LABEL</source>
        <translation>Nom d&apos;Import</translation>
    </message>
    <message>
        <source>QUE_HELLO_NAME</source>
        <translation>Indiquez votre nom</translation>
    </message>
</context>
</TS>
