#ifndef __HXX2SALOME_H
#define __HXX2SALOME_H

#include <QtGui/QDialog>
#include "hxx2salome_ui.h"

class HXX2Salome : public QDialog, private Ui::HXX2Salome
{
  Q_OBJECT

public:
  HXX2Salome();
  ~HXX2Salome();

private:
  void retrieve();
  void dump();

private slots:
  void on_CloseButton_clicked();
  void on_SourceTreeButton_clicked();
  void on_IncludeButton_clicked();
  void on_LibraryButton_clicked();
  void on_EnvFileButton_clicked();
  void on_OutputTreeButton_clicked();
  void on_GenerateButton_clicked();
};

#endif // __HXX2SALOME_H
