#include <QtGui/QApplication>

#include "hxx2salome.h"

int main( int argc, char ** argv )
{
  QApplication a( argc, argv );

  HXX2Salome mw;
  mw.show();
  
  return a.exec();
}
