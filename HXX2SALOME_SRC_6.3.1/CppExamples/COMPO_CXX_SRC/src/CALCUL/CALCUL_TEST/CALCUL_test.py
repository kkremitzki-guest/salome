from os import getenv
if getenv("SALOMEPATH"):
    import salome
    import CALCUL_ORB
    myCalc = salome.lcc.FindOrLoadComponent("FactoryServer", "CALCUL")
    IN_SALOME_GUI = 1
else:
    import CALCULSWIG
    myCalc=CALCULSWIG.CALCUL()
pass
#
#
print "Test Program of CALCUL component"
print myCalc.add(10,15)
print myCalc.mul(10,15)


