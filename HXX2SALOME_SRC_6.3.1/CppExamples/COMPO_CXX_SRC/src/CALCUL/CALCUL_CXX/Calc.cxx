#include "Calc.hxx"

int CALCUL::add(int i1, int i2)
{
    return i1+i2;
}

int CALCUL::mul(int i1, int i2)
{
    return i1*i2;
}

unsigned CALCUL::fact(unsigned n)
{
    int factorielle=1;
    for (unsigned i=n; i!=1; --i)
	factorielle*=i;
    return factorielle;
}

