#ifndef _CALCUL_HXX_
#define _CALCUL_HXX_


class CALCUL
{
// M�thodes publiques
public:
    int add(int i1, int i2);
    int mul(int i1, int i2);
    unsigned fact(unsigned n);
};

#endif
