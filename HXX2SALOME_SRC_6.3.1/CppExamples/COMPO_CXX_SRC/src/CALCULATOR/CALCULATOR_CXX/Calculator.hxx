#ifndef _CALCULATOR_HXX_
#define _CALCULATOR_HXX_

// forward declaration
namespace MEDMEM {
    class MESH;
    template <class T> class FIELD;
}

class CALCULATOR
{
// M�thodes publiques
public:
    CALCULATOR();
    ~CALCULATOR();
    MEDMEM::FIELD<double>* createConstField(double value);
    double norm2(const MEDMEM::FIELD<double>& field);
    double normMax(const MEDMEM::FIELD<double>& field);
    MEDMEM::FIELD<double>* add(const MEDMEM::FIELD<double>& field1, const MEDMEM::FIELD<double>& field2);
    void printField(const MEDMEM::FIELD<double>* myField);
private:
    MEDMEM::MESH* myMesh_;
    MEDMEM::FIELD<double>* myField_;
};

#endif
