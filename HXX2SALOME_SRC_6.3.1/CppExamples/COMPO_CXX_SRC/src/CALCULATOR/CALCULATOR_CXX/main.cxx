#include "Calculator.hxx"
#include "MEDMEM_Field.hxx"
#include "LocalTraceCollector.hxx"

using namespace std;
int main(int argc, char ** argv)
{
    LocalTraceCollector::instance();
    CALCULATOR myCalc;
    MEDMEM::FIELD<double>* f1 = myCalc.createConstField(2.0);
    MEDMEM::FIELD<double>* f2 = myCalc.createConstField(3.0);
    MEDMEM::FIELD<double>* f = myCalc.add(*f1,*f2);
    myCalc.printField(f);
    delete f1;
    delete f2;
    delete f;
}
