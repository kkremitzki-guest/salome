#include "Calculator.hxx"
#include "MEDMEM_Field.hxx"
#include "MEDMEM_Mesh.hxx"
#include <string>

using namespace std;
using namespace MED_EN;

CALCULATOR::CALCULATOR()
{
    string ressourceDir(getenv("MED_ROOT_DIR"));
    ressourceDir+="/share/salome/resources/";
    const string FileName(ressourceDir+"pointe.med");
    const string FieldName("fieldcelldouble");
    const string MeshName("maa1");
    try 
    {
	myMesh_ = new MEDMEM::MESH(MEDMEM::MED_DRIVER,FileName.c_str(),MeshName.c_str());
	cout<<"Lecture du Maillage Source : " << myMesh_->getName() << endl << flush; 
	cout<<"Construction du support    : "<< endl << flush; 
	MEDMEM::SUPPORT* fromSupport=new MEDMEM::SUPPORT(myMesh_,"XsupportX",MED_CELL);
	myField_ = new MEDMEM::FIELD<double>(fromSupport,MEDMEM::MED_DRIVER,FileName.c_str(),FieldName.c_str());
	cout<<"Lecture du champ Source    : " << myField_->getName() << endl << flush;
	cout<<"Fin Constructeur TESTMED"<<endl<<flush;
    } 
    catch (MEDMEM::MEDEXCEPTION& ex)
    {
	cout << ex.what() ;
    }
}

CALCULATOR::~CALCULATOR()
{
    if (myMesh_)
	delete myMesh_;
    if (myField_)
	delete myField_;
}

MEDMEM::FIELD<double>* CALCULATOR::createConstField(double value)
{
    MEDMEM::FIELD<double>* myField=new MEDMEM::FIELD<double>(myField_->getSupport(),1); 
    for(int i=0; i!=myField_->getNumberOfValues();++i)
        myField->setValueIJ(i+1,1,value);
    myField->setName("Const field");
    return myField;
}

double CALCULATOR::norm2(const MEDMEM::FIELD<double>& field)
{
    return field.norm2();
}

double CALCULATOR::normMax(const MEDMEM::FIELD<double>& field)
{
    return field.normMax();
}

MEDMEM::FIELD<double>* CALCULATOR::add(const MEDMEM::FIELD<double>& field1, const MEDMEM::FIELD<double>& field2)
{
    return new MEDMEM::FIELD<double>(field1+field2);
}

void CALCULATOR::printField(const MEDMEM::FIELD<double>* myField)
{
    const MEDMEM::SUPPORT * mySupport = myField->getSupport();
    std::cout << "\n------------------ Field "<< myField->getName() << " : " <<myField->getDescription() << "------------------" <<  std::endl ;
    int NumberOfComponents = myField->getNumberOfComponents() ;
    std::cout << "- Type : " << mySupport->getEntity() << std::endl;
    std::cout << "- Nombre de composantes : "<< NumberOfComponents << std::endl ;
    std::cout << "- Nombre de valeurs     : "<< myField->getNumberOfValues() << std::endl ;
    for (int i=1; i<NumberOfComponents+1; i++) {
	std::cout << "  - composante "<<i<<" :"<<std::endl ;
	std::cout << "      - nom         : "<<myField->getComponentName(i)<< std::endl;
	std::cout << "      - description : "<<myField->getComponentDescription(i) << std::endl;
	std::cout << "      - units       : "<<myField->getMEDComponentUnit(i) << std::endl;
    }
    std::cout << "- iteration :" << std::endl ;
    std::cout << "    - numero : " << myField->getIterationNumber()<< std::endl  ;
    std::cout << "    - ordre  : " << myField->getOrderNumber()<< std::endl  ;
    std::cout << "    - temps  : " << myField->getTime()<< std::endl  ;
    std::cout << "- Type : " << myField->getValueType()<< std::endl;
    
    std::cout << "- Valeurs :"<<std::endl;
    int NumberOf = mySupport->getNumberOfElements(MED_ALL_ELEMENTS);

    //mySupport->isOnAllElements()
    bool displayNode = mySupport->isOnAllElements() && mySupport->getEntity()==MED_NODE;
    bool displayBary = mySupport->isOnAllElements() && mySupport->getEntity()==MED_CELL;
    int dim_space = mySupport->getMesh()->getSpaceDimension();
    const double * coord = mySupport->getMesh()->getCoordinates(MED_FULL_INTERLACE);

    MEDMEM::FIELD<double>* barycenter(0);
    if(displayBary)
	barycenter = mySupport->getMesh()->getBarycenter(mySupport);

    for (int i=1; i<NumberOf+1; i++) {
	const double * value = myField->getValueI(MED_FULL_INTERLACE,i) ;
	if(displayNode)
	{
	    int N=(i-1)*dim_space;
	    std::cout << std::setw(5) << coord[N] << " " << std::setw(5) << coord[N+1]<<  " " << std::setw(5) << coord[N+2] << "  : " ;
	}
	if(displayBary)
	    std::cout << std::setw(5) << barycenter->getValueIJ(i,1) << " " << std::setw(5) << barycenter->getValueIJ(i,2) 
		 <<  " " << std::setw(5) << barycenter->getValueIJ(i,3) << "  : " ;
	for (int j=0; j<NumberOfComponents; j++)
	    std::cout << value[j]<< " ";
	std::cout<<std::endl;
    }
    std::cout << std::endl;
    std::cout << "Norme euclidienne : " << myField->norm2() << std::endl;
    std::cout << "Norme max         : " << myField->normMax() << std::endl;
    std::cout << "------------------------------------------------------------------------" << std::endl << std::endl << std::flush;
}

