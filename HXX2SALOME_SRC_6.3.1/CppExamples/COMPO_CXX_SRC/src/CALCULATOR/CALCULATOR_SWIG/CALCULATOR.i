%module CALCULATORSWIG

%{
#include "Calculator.hxx"
#include "LocalTraceCollector.hxx"
%}

/*
   Initialisation block due to the LocalTraceCollector mechanism

 */

%init %{
    LocalTraceCollector::instance();
%}

%include "Calculator.hxx"


