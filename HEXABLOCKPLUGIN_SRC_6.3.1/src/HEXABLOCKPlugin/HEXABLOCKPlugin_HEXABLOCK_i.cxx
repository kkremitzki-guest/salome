// Copyright (C) 2009-2011  CEA/DEN, EDF R&D
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  SMESH SMESH_I : idl implementation based on 'SMESH' unit's calsses
//  File   : HEXABLOCKPlugin_HEXABLOCK_i.cxx
//  Author : Lioka RAZAFINDRAZAKA (CEA)
//  Module : HEXABLOCKPlugin
//  $Header: /home/server/cvs/HEXABLOCKPLUGIN/HEXABLOCKPLUGIN_SRC/src/HEXABLOCKPlugin/Attic/HEXABLOCKPlugin_HEXABLOCK_i.cxx,v 1.1.2.1.6.1 2011-06-02 14:10:37 vsr Exp $
//
#include "HEXABLOCKPlugin_HEXABLOCK_i.hxx"
#include "SMESH_Gen.hxx"
#include "HEXABLOCKPlugin_HEXABLOCK.hxx"

#include "utilities.h"

using namespace std;

//=============================================================================
/*!
 *  HEXABLOCKPlugin_HEXABLOCK_i::HEXABLOCKPlugin_HEXABLOCK_i
 *
 *  Constructor
 */
//=============================================================================

HEXABLOCKPlugin_HEXABLOCK_i::HEXABLOCKPlugin_HEXABLOCK_i (PortableServer::POA_ptr thePOA,
                                          int                     theStudyId,
                                          ::SMESH_Gen*            theGenImpl )
     : SALOME::GenericObj_i( thePOA ), 
       SMESH_Hypothesis_i( thePOA ), 
       SMESH_Algo_i( thePOA ),
       SMESH_3D_Algo_i( thePOA )
{
  MESSAGE( "HEXABLOCKPlugin_HEXABLOCK_i::HEXABLOCKPlugin_HEXABLOCK_i" );
  myBaseImpl = new ::HEXABLOCKPlugin_HEXABLOCK (theGenImpl->GetANewId(),
                                        theStudyId,
                                        theGenImpl );
}

//=============================================================================
/*!
 *  HEXABLOCKPlugin_HEXABLOCK_i::~HEXABLOCKPlugin_HEXABLOCK_i
 *
 *  Destructor
 */
//=============================================================================

HEXABLOCKPlugin_HEXABLOCK_i::~HEXABLOCKPlugin_HEXABLOCK_i()
{
  MESSAGE( "HEXABLOCKPlugin_HEXABLOCK_i::~HEXABLOCKPlugin_HEXABLOCK_i" );
}

//=============================================================================
/*!
 *  HEXABLOCKPlugin_HEXABLOCK_i::GetImpl
 *
 *  Get implementation
 */
//=============================================================================

::HEXABLOCKPlugin_HEXABLOCK* HEXABLOCKPlugin_HEXABLOCK_i::GetImpl()
{
  MESSAGE( "HEXABLOCKPlugin_HEXABLOCK_i::GetImpl" );
  return ( ::HEXABLOCKPlugin_HEXABLOCK* )myBaseImpl;
}

