# Copyright (C) 2006-2011  CEA/DEN, EDF R&D
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
# See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#

# --
# Author : Anthony Geay (CEA)
# --
# -----------------------------------------------------------------------------
#
AC_INIT([Salome2 Project], [6.3.1], [paul.rascle@edf.fr], [SalomeYacs])
AM_INIT_AUTOMAKE([tar-pax])
AC_CONFIG_HEADER(yacs_config.h)

# set up MODULE_NAME variable for dynamic construction of directories (resources, etc.)
MODULE_NAME=yacs
AC_SUBST(MODULE_NAME)

SHORT_VERSION=`echo $VERSION | awk -F. '{printf("%d.%d",$1,$2)}'`
AC_SUBST(SHORT_VERSION)
XVERSION=`echo $VERSION | awk -F. '{printf("0x%02x%02x%02x",$1,$2,$3)}'`
AC_SUBST(XVERSION)

RELEASE=$VERSION
AC_SUBST(RELEASE)

# This function return on stdout the absolute path of the filename in
# argument. Exemple:
# $ filename="../KERNEL_SRC/configure
# $ absfilename=`absolute_path $filename`
function absolute_path {
    filename=$1
    here=`pwd`
    apath=`dirname $filename`
    cd $apath
    apath=`pwd`
    cd $here
    echo $apath
}

# Build directory, where the configure script is executed.
ROOT_BUILDDIR=`pwd`
# Source directory, where the configure script is located.
ROOT_SRCDIR=`absolute_path $0`

AC_SUBST(ROOT_SRCDIR)
AC_SUBST(ROOT_BUILDDIR)

echo
echo Source root directory : $ROOT_SRCDIR
echo Build  root directory : $ROOT_BUILDDIR

# -----------------------------------------------------------------------------

AC_ENABLE_DEBUG(yes)
AC_DISABLE_PRODUCTION

#Linker options
AC_CANONICAL_BUILD
AC_LINKER_OPTIONS

#Mandatory products
AC_PROG_CXX
AM_PROG_LIBTOOL

AC_CHECK_LIB(dl,dlopen)

ENABLE_PTHREADS

AM_PATH_PYTHON(2.3)
AC_PYTHON_DEVEL
python_ok=yes
CHECK_THREAD
AC_CHECK_OMNIORB
AC_CHECK_EXPAT
AC_CHECK_LIBXML
CHECK_BOOST

#Optional products
AC_PROG_SWIG(1.3.17)
SWIG_ENABLE_CXX
SWIG_PYTHON
CHECK_CPPUNIT
AC_CHECK_KERNEL
#CHECK_MSG2QM
CHECK_HTML_GENERATORS
CHECK_SPHINX

CHECK_GUI(SALOME_Session_Server,SALOME_Session_Server)

I2_CHECK_QT4
I2_CHECK_QSCINTILLA

AM_CONDITIONAL([PYTHON_API], [test "x$swig_ok" == "xyes"])
AM_CONDITIONAL([SALOME_KERNEL], [test "x$KERNEL_ROOT_DIR" != "x"])
AM_CONDITIONAL([DSC_PORTS], [test -f $KERNEL_ROOT_DIR/idl/salome/DSC_Engines.idl])

AM_CONDITIONAL(WINDOWS, [ test ])

echo
echo
echo
echo "------------------------------------------------------------------------"
echo "$PACKAGE $VERSION"
echo "------------------------------------------------------------------------"
echo
echo "Configuration Options Summary:"
echo
echo "Mandatory products:"
echo "  Threads ................ : $thread_ok"
echo "  Boost .................. : $boost_ok"
echo "  OmniOrb (CORBA) ........ : $omniORB_ok"
echo "  OmniOrbpy (CORBA) ...... : $omniORBpy_ok"
echo "  Python ................. : $python_ok"
echo "  Expat .................. : $expat_ok"
echo "  libxml ................. : $libxml_ok"
echo
echo "Optional products:"
echo "  swig (python wrapper)... : $swig_ok"
echo "  SALOME KERNEL .......... : $Kernel_ok"
echo "  DSC extension .......... : $dsc_ok"
echo "  Cppunit (make check).... : $cppunit_ok"
echo "  Qt4 (graphic interface)  : $qt_ok"
echo "  QScintilla ............. : $qscintilla_ok"
#echo "  msq2qm ................. : $msg2qm_ok"
echo "  SALOME GUI ............. : $SalomeGUI_ok"
echo "  Sphinx (user doc) ...... : $sphinx_ok"
echo
echo "------------------------------------------------------------------------"
echo

if test "x$thread_ok" = "xno"; then
  AC_MSG_ERROR([Thread is required],1)
fi
if test "x$omniORB_ok" = "xno"; then
  AC_MSG_ERROR([OmniOrb is required],1)
fi
if test "x$python_ok" = "xno"; then
  AC_MSG_ERROR([Python is required],1)
fi
if test "x$omniORBpy_ok" = "xno"; then
  AC_MSG_ERROR([OmniOrbpy is required],1)
fi
if test "x$expat_ok" = "xno"; then
  AC_MSG_ERROR([Expat is required],1)
fi
if test "x$libxml_ok" = "xno"; then
  AC_MSG_ERROR([Libxml is required],1)
fi


AC_OUTPUT([ \
  Makefile \
  Demo/Makefile \
  doc/Makefile \
  idl/Makefile \
  src/Makefile \
  src/bases/Makefile \
  src/bases/Test/Makefile \
  src/engine/Makefile \
  src/engine/Plugin/Makefile \
  src/engine/Test/Makefile \
  src/engine_swig/Makefile \
  src/hmi/Makefile \
  src/pyqt/Makefile \
  src/runtime/Makefile \
  src/runtime/Test/Makefile \
  src/runtime_swig/Makefile \
  src/genericgui/Makefile \
  src/salomewrap/Makefile \
  src/salomegui/Makefile \
  src/salomegui_swig/Makefile \
  src/salomeloader/Makefile \
  src/salomeloader/testSalomeLoader.py \
  src/wrappergen/Makefile \
  src/wrappergen/src/Makefile \
  src/yacsloader/Makefile \
  src/yacsloader/Test/Makefile \
  src/yacsloader/Test/YacsLoaderTest.sh \
  src/yacsloader/Test/YacsLoaderInSessionTest.sh \
  src/yacsloader/Test/display.sh \
  src/yacsloader/Test/config_appli.xml \
  src/yacsloader_swig/Makefile \
  src/yacsloader_swig/Test/Makefile \
  src/yacsloader_swig/Test/YacsLoaderTest.sh \
  src/yacsloader_swig/Test/YacsLoaderInSessionTest.sh \
  src/yacsloader_swig/Test/config_appli.xml \
  src/yacsorb/Makefile \
  src/salomegui/resources/YACSCatalog.xml \
  doc/Doxyfile \
  doc/conf.py \
])

