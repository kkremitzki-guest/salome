// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  SALOME SUPERVGraph : build Supervisor viewer into desktop
//  File   : SUPERVGraph.cxx
//  Author : Nicolas REJNERI
//  Module : SALOME
//  $Header: /home/server/cvs/GUI/GUI_SRC/src/SUPERVGraph/SUPERVGraph.cxx,v 1.7.22.1.12.1 2011-06-01 13:53:39 vsr Exp $
//
#include "SUPERVGraph.h"
#include "SUPERVGraph_ViewFrame.h"

#include "SUIT_Desktop.h"
#include "SUIT_ViewWindow.h"

using namespace std;

/*!
  Creates view
*/
SUIT_ViewWindow* SUPERVGraph::createView(SUIT_Desktop* parent)
{
  return new SUPERVGraph_ViewFrame( parent ); 
}

extern "C"
{
  /*!
    Creates view
  */
  SUIT_ViewWindow* createView(SUIT_Desktop* parent)
  {
    return SUPERVGraph::createView(parent);
  }
}
