/*!

\page running_salome_page Running SALOME

<em>To launch SALOME:</em>
<ol>
<li>Install the SALOME package into a definite directory (ex. \b SALOME)
on your hard disk. It is preferable if you use the special
installation procedure allowing to install the  SALOME platform and
all corresponding applications.</li>
<li>The installation shell script will create a special file:
<b>salome.csh</b> (CShell file) in your SALOME/KERNEL directory. This file
contains all environment variables necessary for launching SALOME
platform with other application products provided with SALOME
installation package. You have a possibility to add one of them into
your profile if you enter in the command console the following: <br><br>
<tt>source salome.csh</tt>

<b>Tip:</b> During the installation procedure you have a possibility to set your profile automatically.</li>

<li> Launch SALOME platform, using the following Python script located
in the <b>SALOME/KERNEL/bin/salome</b> directory:<br><br> 
<ul>
<li>\b runSalome.py [command line options]
</ul>
</li>
</ol>

\anchor batch_mode_run

<table>
<tr>
<td><h2>Options</h2></td>
<td><h2>Description</h2></td>
</tr>
<tr>
<td>--\b help or -\b h</td>
<td>print this help</td>
</tr>
<tr>
<td>--\b gui or -\b g</td>
<td>launch with GUI</td>
</tr>
<tr>
<td> --\b terminal or -\b t</td>
<td>launch without GUI in batch mode</td>
</tr>
<tr>
<td>--\b logger or -\b l</td>
<td>redirects log messages in the file <em>logger.log</em></td>
</tr>
<tr>
<td>--\b file=<b>\<FILE\></b> or -\b f=<b>\<FILE\></b></td>
<td>redirects  log messages in a custom file</td>
</tr>
<tr>
<td>--\b xterm or -\b x</td>
<td>the servers open an xterm window and log messages are displayed in this window</td>
</tr>
<tr>
<td>--\b modules=\b module1,\b module2,... or -\b m=\b module1,\b module2,...</td>
<td>list of SALOME modules which will be loaded into the module catalogue</td>
</tr>
<tr>
<td>--\b embedded=<b>registry,study,moduleCatalog,cppContainer</b>,
or -\b e=<b>registry,study,moduleCatalog,cppContainer</b></td>
<td>embedded CORBA servers (default: registry,study,moduleCatalog,cppContainer)
note that logger,pyContainer,supervContainer can't be embedded</td>
</tr>
<tr>
<td>--\b standalone=<b>registry,study,moduleCatalog,cppContainer,pyContainer,supervContainer</b>, or
-\b s=<b>registry,study,moduleCatalog,cppContainer,pyContainer,supervContainer</b></td>
<td>standalone CORBA servers (default: pyContainer,supervContainer)</td>
</tr>
<tr>
<td>--\b containers=<b>cpp,python,superv</b>
or -\b c=<b>cpp,python,superv</b></td>
<td>launch of cpp, python and supervision containers</td>
</tr>
<tr>
<td>--\b portkill or -\b p</td>
<td>kill SALOME launched with the current port</td>
</tr>
<tr>
<td>--\b killall or -\b k</td>
<td>kill SALOME</td>
</tr>
<tr>
<td>--<b>interp</b>=<b>n</b> or -<b>i</b>=<b>n</b></td>
<td>number of additional xterm to open, with session environment</td>
</tr>
<tr>
<td>-\b z</td>
<td>display splash screen</td>
</tr>
<tr>
<td>-\b r</td>
<td>disable centralized exception handling mechanism</td>
</tr>
</table>

<b>Tip:</b> If the \b runSalome.py script is launched without prompting
any options, they will be taken by default from the file <b>SalomeApp.xml
(SALOME/GUI/share/salome/resources/SalomeApp.xml)</b>. If you are
constantly launching SALOME with some specific options which are
different from the defaults, you can edit this file according to your
requirements. So the next time you run SALOME, you won't have to enter
these numerous command console options.

*/