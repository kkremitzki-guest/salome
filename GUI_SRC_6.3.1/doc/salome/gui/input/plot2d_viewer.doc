/*!

\page plot2d_viewer_page Plot 2D viewer

The functionalities of Plot2d viewer are available via its Viewer
Toolbar. Buttons marked with small downward triangles have extended
functionality which can be accessed by locking on them with left mouse
button.

\image html plot2dviewer_toolbar.png "Viewer Toolbar"
<hr>

\image html plot2d_camera_dump.png
<br><center><b>Dump View</b> - exports an object from the viewer in bmp,
png, jpg or jpeg image format.</center>
<hr>

\image html plot2d_fitall.png
<br><center><b>Fit all</b> - scales the display to show the entire
scene. Use this to resize the scene so that it could fit within the
Viewer boundary.</center>
<hr>

\image html plot2d_fitarea.png
<br><center><b>Fit area</b> - resizes the view to place in the visible
area only the contents of a frame drawn with pressed left mouse
button.</center>
<hr>

\image html plot2d_zoom.png
<br><center><b>Zoom</b> - allows to zoom in and out.</center>
<hr>

\image html plot2d_pan.png
<br><center>\b Panning - if the represented objects are greater that the
visible area and you don't wish to use Fit all functionality, click on
this button and you'll be able to drag the scene to see its remote
parts.</center>
<hr>

\image html plot2d_glpan.png
<br><center><b>Global panning</b> - allows to define the center of the
scene presenting all displayed objects in the visible area.</center>
<hr>

\image html plot2d_points.png
<br><center><b>Draw Points</b> - switch display mode to the
<b>Points</b> mode. In this mode, each curve is displayed as a set of
points.</center>
<hr>

\image html plot2d_lines.png
<br><center><b>Draw Lines</b> - switch display mode to the
<b>Lines</b> mode. In this mode, each curve is represented as a set of
the plain line segments.</center>

\image html plot2d_splines.png
<br><center><b>Draw Splines</b> - switch display mode to the
<b>Splines</b> mode. In this mode, each curve is represented as a
spline.</center>
<hr>

\image html plot2d_linear.png
<br>
\image html plot2d_log.png
<br><center>These buttons allow to switch horizontal axis scale to the
linear or logarithmic mode.<br>Note that the logarithmic mode of the
horizontal axis scale is allowed only if the minimum value of abscissa
component of displayed points is greater than zero.</center>
<hr>

\image html plot2d_linear_y.png
<br>
\image html plot2d_log_y.png
<br><center>These buttons allow to switch vertical axis scale to the
linear or logarithmic mode.<br>Note that the logarithmic mode of the
vertical axis scale is allowed only if the minimum value of ordinate
component of displayed points is greater than zero.</center>
<hr>

\image html plot2d_legend.png
<br><center><b>Show Legend</b> - Shows / hides information about the
displayed objects in a legend.</center>
<hr>

\image html plot2d_settings.png
<br><center>\b Settings - provides an access to the settings dialog
box, that allows to specify advanced parameters for the Plot 2d
Viewer.</center>
<br>

\anchor settings
\image html plot2d_view_settings.png
<br>

The options are as follows:
- <b>Main title</b> the title of the XY plot. By default, it will
consist of the names of the tables, on the basis of which the curve
lines have been constructed.
- <b>Curve type</b> you can select from \b Points, \b Lines and \b Spline.
- <b>Show legend</b> here you can define the position of the
description table on the XY plot (to the \b Left, to the \b Right, on
\b Top or on \b Bottom).
- <b>Marker size</b> - size of the points (markers) forming curve lines.
- <b>Background color</b> of the XY plot.
- <b>Scale mode</b> here you can select the type of scaling (\b
Linear or \b Logarithmic) for <b>X (Horizontal)</b> or <b>Y
(Vertical)</b> axes separately. Note that the \b Logarithmic scale can
be used only if the minimum value of corresponding component (abscissa
or ordinate) of all points displayed in the viewer is greater than zero.
- <b>Axis ...</b> options group allows to specify additional settings for each axis separately:
  - <b>Axis title</b> - an axis title
  - <b>Grid / Axes marks</b>  here you can define the maximum number of major
  and minor scale divisions for a specified axis. The real number of
  intervals fits to {1,2,5}*10^N, where N is a natural number, and
  doesn't exceed the maximum.
- <b>Save settings as default</b> If this check box is marked, all
XY plots will be displayed with these defined properties.
<hr>

\image html plot2d_clone.png
<br><center><b>Clone view</b> - opens a new duplicate scene.</center>
<hr>

\image html plot2d_print.png
<br><center><b>Print view</b> - print view scene.</center>
<hr>

*/
